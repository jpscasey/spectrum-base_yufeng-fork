#!/usr/bin/env bash

# THIS SCRIPT LOGINS TO AWS.

access_id=$1;
access_key=$2;

# Remove previous auth if exits
rm -f ~/.docker/config.json

# AWS configuration
if [[ ! -e ~/.aws/config || ! -e ~/.aws/credentials ]]; then
aws configure set aws_access_key_id ${access_id}\
    && aws configure set aws_secret_access_key ${access_key} \
    && aws configure set region eu-west-1 \
    && aws configure set output text
fi

# AWS login
$(aws ecr get-login --no-include-email --region eu-west-1)

# Update auth file
awk '{gsub(/328140101896.dkr.ecr/,"https://328140101896.dkr.ecr");print}' ~/.docker/config.json > temp.json && mv -f temp.json ~/.docker/config.json
