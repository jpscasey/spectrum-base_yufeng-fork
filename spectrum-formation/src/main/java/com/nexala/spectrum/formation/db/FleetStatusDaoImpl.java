package com.nexala.spectrum.formation.db;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import com.google.inject.Inject;
import com.nexala.spectrum.formation.Configuration;
import com.nexala.spectrum.formation.SpectrumFormation;
import com.nexala.spectrum.formation.db.beans.FleetStatus;
import com.nexala.spectrum.formation.db.beans.Formation;

public class FleetStatusDaoImpl extends NamedParameterJdbcDaoSupport {
    
    private final Configuration conf;
    
    @Inject
    public FleetStatusDaoImpl (Configuration conf) {
        this.conf = conf;
    }
    
    private ResultSetExtractor<List<FleetStatus>> fleetStatusExtractor = new ResultSetExtractor<List<FleetStatus>>() {
        @Override
        public List<FleetStatus> extractData(ResultSet rs) throws SQLException, DataAccessException {
            List<FleetStatus> result = new ArrayList<>();
            while (rs.next()) {
                result.add(new FleetStatus(
                        rs.getInt("ID"),
                        rs.getString("Headcode"), 
                        rs.getString("Setcode"), 
                        rs.getString("Diagram"), 
                        rs.getInt("UnitID"), 
                        rs.getInt("UnitPosition"), 
                        rs.getString("UnitList"), 
                        rs.getInt("FleetFormationID")));
            }
            return result;
        }
    };
    
    /**
     * Returns an active FleetStatus object for a specific unit number
     * @param unitNumber
     * @return
     */
    public List<FleetStatus> getFleetStatusByUnitNumber (String unitNumber) {
        List<FleetStatus> result = new ArrayList<>();
        
        String query = conf.getQuery(SpectrumFormation.fleetStatusFromUnitNumber);
        SqlParameterSource params = new MapSqlParameterSource().addValue("unitNumber", unitNumber);
        result = this.getNamedParameterJdbcTemplate().query(query, params, fleetStatusExtractor);
        
        return result;
    }
    
    /**
     * 
     * @param fleetStatuses
     */
    public void closeFleetStatusList (List<FleetStatus> fleetStatuses) {
        for (FleetStatus fleetStatus: fleetStatuses) {
            closeFleetStatus(fleetStatus);
        }
    }
    
    public void closeFleetStatus (FleetStatus fleetStatus) {
        String query = conf.getQuery(SpectrumFormation.closeFleetStatus);
        
        SqlParameterSource params = new MapSqlParameterSource().addValue("fleetStatusID", fleetStatus.getId());
        this.getNamedParameterJdbcTemplate().update(query, params);
    }
    
    
    
    public void insertNewFleetStatus (String unitNumber, Formation newFormation) {
        String query = conf.getQuery(SpectrumFormation.insertFleetStatus);
        
        SqlParameterSource params = new MapSqlParameterSource()
                .addValue("setcode", null)
                .addValue("diagram", null)
                .addValue("headcode", newFormation.getHeadcode())
                .addValue("loading", null)
                .addValue("unitList", newFormation.getUnitNumberListAsString())
                .addValue("locationIDHeadcodeStart", null)
                .addValue("locationIDHeadcodeEnd", null)
                .addValue("unitNumber", unitNumber)
                .addValue("unitPosition", newFormation.getUnitNumberList().indexOf(unitNumber) + 1)
                .addValue("unitOrientation", 1)
                .addValue("fleetFormationID", newFormation.getId())
                .addValue("activeCab", newFormation.getActiveCab());
        this.getNamedParameterJdbcTemplate().update(query, params);
    }

}
