package com.nexala.spectrum.formation;

public class SpectrumFormation {
    
    // FleetFormation queries
    public static final String formationIdByUnitList = "getFormationIdByUnitList";
    public static final String currentFormations = "getCurrentFormations";
    public static final String insertFormation = "insertFormation";
    public static final String updateFormation = "updateFormation";
    public static final String closeFormation = "closeFormation";
    public static final String insertSingleUnitFormation = "insertSingleUnitFormation";
    
    // FleetStatus queries
    public static final String fleetStatusFromUnitNumber = "getFleetStatusFromUnitNumber";
    public static final String closeFleetStatus = "closeFleetStatus";
    public static final String insertFleetStatus = "insertFleetStatus";

}
