package com.nexala.spectrum.formation.db.beans;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Builder pattern to facilitate constructing the Formation object with different optional fields
 * @author BRedaha
 *
 */
public class FormationBuilder {
    Integer id;
    
    Date validFrom;
    Date validTo;
    
    List<Unit> unitList;
    
    List<String> unitNumberList;
    List<Integer> unitIdList;
    
    String headcode;
    String activeCab;
    
    public FormationBuilder() {
        
    }
    
    public FormationBuilder id(Integer id) {
        this.id = id;
        return this;
    }
    
    public FormationBuilder unitList(List<Unit> unitList) {
        this.unitList = unitList;
        this.unitNumberList = new ArrayList<>();
        this.unitIdList = new ArrayList<>();
        
        for (Unit unit : unitList) {
            unitNumberList.add(unit.getUnitNumber());
            unitIdList.add(unit.getUnitId());
        }
        
        return this;
    }
    
    public FormationBuilder unitNumberList(List<String> unitNumberList) {
        this.unitNumberList = unitNumberList;
        return this;
    }
    
    public FormationBuilder unitNumberListString(String unitNumberList) {
        this.unitNumberList = new ArrayList<>();
        if (unitNumberList != null) {
            String[] unitNumberStr = unitNumberList.split(",");
            for (String unitNumber : unitNumberStr) {
                this.unitNumberList.add(unitNumber);
            }
        }
        return this;
    }
    
    public FormationBuilder unitIdList(List<Integer> unitIdList) {
        this.unitIdList = unitIdList;
        return this;
    }
    
    public FormationBuilder unitIdListString(String unitIdList) {
        String[] unitIdStrList = unitIdList.split(",");
        this.unitIdList = new ArrayList<Integer>();
        
        for (String unitId : unitIdStrList) {
            if (!unitId.isEmpty()) {
                this.unitIdList.add(Integer.parseInt(unitId));
            }
        }
        return this;
    }
    
    public FormationBuilder validFrom(Date validFrom) {
        this.validFrom = validFrom;
        return this;
    }
    
    public FormationBuilder validTo(Date validTo) {
        this.validTo = validTo;
        return this;
    }
    
    public FormationBuilder headcode(String headcode) {
        this.headcode = headcode;
        return this;
    }
    
    public FormationBuilder activeCab(String activeCab) {
        this.activeCab = activeCab;
        return this;
    }
    
    public Formation build() {
        return new Formation(this);
    }
}


