package com.nexala.spectrum.formation.db;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import com.google.inject.Inject;
import com.nexala.spectrum.formation.Configuration;
import com.nexala.spectrum.formation.SpectrumFormation;
import com.nexala.spectrum.formation.db.beans.Formation;
import com.nexala.spectrum.formation.db.beans.FormationBuilder;

public class FormationDaoImpl extends NamedParameterJdbcDaoSupport {
    
    private final Configuration conf;
    
    @Inject
    public FormationDaoImpl (Configuration conf) {
        this.conf = conf;
    }
            
    private ResultSetExtractor<Integer> idExtractor = new ResultSetExtractor<Integer>() {
        @Override
        public Integer extractData(ResultSet rs) throws SQLException, DataAccessException {
            while (rs.next()) {
                return rs.getInt(1);
            }
            return null;
        }
    };
    
    private ResultSetExtractor<List<Formation>> formationExtractor = new ResultSetExtractor<List<Formation>>() {
        @Override
        public List<Formation> extractData(ResultSet rs) throws SQLException, DataAccessException {
            List<Formation> result = new ArrayList<>();
            while (rs.next()) {
                result.add(
                    new FormationBuilder()
                            .id(rs.getInt("ID"))
                            .unitIdListString(rs.getString("UnitIDList"))
                            .unitNumberListString(rs.getString("UnitNumberList"))
                            .validFrom(rs.getDate("ValidFrom"))
                            .validTo(rs.getDate("ValidTo"))
                            .build()
                );
            }
            return result;
        }
    };
    
    
    /**
     * Checks the database to see if a formation already exists with this list of units
     * @see com.nexala.spectrum.formation.db.beans.Formation
     * @param datasource - javax.sql.Datasource objects configured for the fleet database
     * @param unitNumberList
     * @return the formation ID if found, else null
     */
    public Integer getFormationIdByUnitList (String unitNumberList) {
        
        String query = conf.getQuery(SpectrumFormation.formationIdByUnitList);
        SqlParameterSource ffIdParams = new MapSqlParameterSource().addValue("unitNumberList", unitNumberList);
        Integer ffId = this.getNamedParameterJdbcTemplate().query(query, ffIdParams, idExtractor);
        
        return ffId;
    }
    
    
    /**
     * Gets all active formations that currently contain units from the new formation
     * @param unitNumbers - a list of the units from a new formations
     * @return currentFormations - list of active formations
     */
    public List<Formation> getCurrentFormations (List<String> unitNumbers) {
        List<Formation> currentFormations = new ArrayList<Formation>();
        
        String query = conf.getQuery(SpectrumFormation.currentFormations);
        for (String unitNumber : unitNumbers) {
            SqlParameterSource params = new MapSqlParameterSource().addValue("unitNumber",  "%" + unitNumber + "%");
            List<Formation> temp = this.getNamedParameterJdbcTemplate().query(query, params, formationExtractor);
            currentFormations.addAll(temp);
        }
        
        return currentFormations;
    }
    
    
    /**
     * Inserts a Formation object into the database
     * @param datasource - javax.sql.Datasource objects configured for the fleet database
     * @param formation - the Formation to insert
     */
    public Integer insertFormation (Formation formation) {
        String query = conf.getQuery(SpectrumFormation.insertFormation);
        
        SqlParameterSource ffParams = new MapSqlParameterSource()
                .addValue("unitIdList", formation.getUnitIdListAsString())
                .addValue("unitNumberList", formation.getUnitNumberListAsString());
        Integer newFfId = this.getNamedParameterJdbcTemplate().query(query, ffParams, idExtractor);
        
        return newFfId;
    }
    
    
    /**
     * Updates an existing formation in the database
     * @param datasource - javax.sql.Datasource objects configured for the fleet database
     * @param formation - the Formation to update
     */
    public void updateFormation (Formation formation) {
        
        
    }
    
    
    /**
     * Close all the formations in a list
     * @param formations
     */
    public void closeFormations (List<Formation> formations) {
        for (Formation formation : formations) {
            closeFormation(formation);
        }
    }
    
    
    /**
     * Close a single formation
     * @param formation
     */
    public void closeFormation (Formation formation) {
        String query = conf.getQuery(SpectrumFormation.closeFormation);
        
        SqlParameterSource params = new MapSqlParameterSource().addValue("formationId", formation.getId());
        this.getNamedParameterJdbcTemplate().update(query, params);
        
    }
    
    /**
     * Creates single-unit formations for the units remaining from closed formations not in the new formation
     * @param formation
     */
    public void cleanUpLooseUnits (List<String> looseUnitIds) {
        for (String unitId : looseUnitIds) {
            insertSingleUnitFormation(unitId);
        }
    }
    
    /**
     * Inserts a single-unit formation for a unit leftover from a closed 
     * @param formation
     */
    public void insertSingleUnitFormation(String unitId) {
        String query = conf.getQuery(SpectrumFormation.insertSingleUnitFormation);
        
        SqlParameterSource params = new MapSqlParameterSource().addValue("unitNumber", unitId);
        this.getNamedParameterJdbcTemplate().update(query, params);
    }
}
