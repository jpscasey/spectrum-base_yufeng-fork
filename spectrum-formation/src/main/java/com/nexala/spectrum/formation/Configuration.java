package com.nexala.spectrum.formation;

import com.google.inject.Singleton;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

@Singleton
public class Configuration {
    private final Config conf = ConfigFactory.load();
    
    public Config get() {
        return conf;
    }
    
    public String getQuery(String queryId) {
        return conf.getString("formation.queries." + queryId);
    }

}
