package com.nexala.spectrum.formation.db.beans;

import java.util.Comparator;

public class Unit {
    private int unitId;
    private String unitNumber;
    private int formationPosition;
    
    /**
     * Comparator to sort units by formation position
     */    
    public static Comparator<Unit> UnitPositionComparator = new Comparator<Unit>() {
        public int compare(Unit unit1, Unit unit2) {
            Integer unit1Pos = unit1.getFormationPosition();
            Integer unit2Pos = unit2.getFormationPosition();
            
            return unit1Pos.compareTo(unit2Pos);
        }
    };

    public int getUnitId() {
        return unitId;
    }

    public void setUnitId(int unitId) {
        this.unitId = unitId;
    }

    public String getUnitNumber() {
        return unitNumber;
    }

    public void setUnitNumber(String unitNumber) {
        this.unitNumber = unitNumber;
    }

    public int getFormationPosition() {
        return formationPosition;
    }

    public void setFormationPosition(int formationPosition) {
        this.formationPosition = formationPosition;
    }

}
