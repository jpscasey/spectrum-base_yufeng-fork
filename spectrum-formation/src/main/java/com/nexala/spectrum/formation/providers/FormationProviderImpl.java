package com.nexala.spectrum.formation.providers;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import com.google.inject.Inject;
import com.nexala.spectrum.formation.db.FleetStatusDaoImpl;
import com.nexala.spectrum.formation.db.FormationDaoImpl;
import com.nexala.spectrum.formation.db.beans.FleetStatus;
import com.nexala.spectrum.formation.db.beans.Formation;

public class FormationProviderImpl implements FormationProvider {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(FormationProviderImpl.class);
    
    @Inject
    private FormationDaoImpl formationDao;
    
    @Inject 
    private FleetStatusDaoImpl fleetStatusDao;
    
    private PlatformTransactionManager txManager;
    
    private DataSource datasource;
    
    public FormationProvider setDataSource(DataSource datasource) {
        this.datasource = datasource;
        formationDao.setDataSource(datasource);
        fleetStatusDao.setDataSource(datasource);
        return this;
    }
    
    /**
     * @inheritDoc
     */
    @Override
    public void processFormation(Formation formation) throws Exception {

        txManager = new DataSourceTransactionManager(this.datasource);
        
        // Start transaction
        DefaultTransactionDefinition def = new DefaultTransactionDefinition();
        def.setName("processFormation");
        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
        
        TransactionStatus txStatus = txManager.getTransaction(def);
        
        try {
            List<Formation> newFormations = updateFleetFormation(formation);        
            updateFleetStatus(newFormations);
            
            // Commit the transaction
            txManager.commit(txStatus);;
        } catch (Exception e) {
            LOGGER.error("Unable to update the formation, rolling back changes.", e);
            // Rollback the transaction
            txManager.rollback(txStatus);
            throw e;
        }
    }
    
    /**
     * @inheritDoc
     */
    @Override
    public List<Formation> updateFleetFormation(Formation formation) throws Exception {
        
        Integer formationId = formationDao.getFormationIdByUnitList(formation.getUnitNumberListAsString());
        
        if (formationId == null) {
            // Get active formations for all units in the new formation
            List<Formation> currentFormations = formationDao.getCurrentFormations(formation.getUnitNumberList());
            
            // Close all active formations containing the units from the new formation
            formationDao.closeFormations(currentFormations);
            
            // Insert new formation from message
            formationId = formationDao.insertFormation(formation);
            formation.setId(formationId);
            
            // Create new formations for all remaining units from closed formations
            List<String> looseUnits = getLooseUnits(formation, currentFormations);
            formationDao.cleanUpLooseUnits(looseUnits);
            
            // Return list of new formations to be processed into FleetStatus
            List<Formation> newFormations = formationDao.getCurrentFormations(looseUnits);
            newFormations.add(formation);
                        
            return newFormations;
            
        } else {
            // Exact formation exists, do nothing
            // TODO: should we update formation if new units are added to the same formation? why not just create a new one...
            LOGGER.info("Formation is already active.");
            formation.setId(formationId);
            List<Formation> result =  new ArrayList<Formation>();
            result.add(formation);
            return result;
        }
    }
    
    
    /**
     * @inheritDoc
     */
    @Override
    public void updateFleetStatus(List<Formation> newFormations) throws Exception {
        if (newFormations.isEmpty()) {
            return;
        }        
        
        List<FleetStatus> result = new ArrayList<>();
        for (Formation formation : newFormations) {
            for (String unitNumber : formation.getUnitNumberList()) {
                if (unitNumber != null) {
                    result.addAll(fleetStatusDao.getFleetStatusByUnitNumber(unitNumber));
                    fleetStatusDao.insertNewFleetStatus(unitNumber, formation);
                }
            }
        }
        
        fleetStatusDao.closeFleetStatusList(result);

        return;
    }
    
    /**
     * Gets the unitNumbers for all units in newly-closed formations that are not in the new formation
     * @param newFormation - the formation created by the message
     * @param closedFormations - the list of current formations containing units in the new formation
     * @return - list of strings - all unit numbers left loose by the closed formations
     */
    private List<String> getLooseUnits(Formation newFormation, List<Formation> closedFormations) {
        List<String> affectedUnits = new ArrayList<>();
        
        for (Formation closedFormation : closedFormations) {
            affectedUnits.addAll(closedFormation.getUnitNumberList());
        }
        
        affectedUnits.removeAll(newFormation.getUnitNumberList());
        
        return affectedUnits;
    }
    
}
