package com.nexala.spectrum.formation.providers;

import java.util.List;

import javax.sql.DataSource;

import com.nexala.spectrum.formation.db.beans.Formation;

public interface FormationProvider {
    
    /**
     * Before using the provider the datasource must be set to the correct data source for the formation in question.
     * @param datasource - a basic javax.sql.Datasource object for the data source required
     */
    public FormationProvider setDataSource(DataSource datasource);
    
    /**
     * Process a Formation object: close all active formations for the units in the unit list, create this formation as well as
     * formations for any leftover units form the closed formations, close all affected FleetStatus records, insert new ones.
     * All database changes are wrapped in a transaction and rolled back if any exceptions are thrown.
     * 
     * @param formation - a Formation object configured with all available fields
     * @throws Exception
     */
    public void processFormation(Formation formation) throws Exception;
    
    
    /**
     * Takes a new formation object, close all active formations for all units in the new formation. Inserts the new formation to 
     * the FleetFormation table, and creates new rows for the units left over form the closed formations.
     * Returns the list of new formations
     * 
     * @param formation
     * @return list of Formation objects for all the newly created formations
     * @throws Exception
     */
    public List<Formation> updateFleetFormation(Formation formation) throws Exception;
    
    
    /**
     * Processes a list of formations, closing old rows and inserting new rows in FleetStatus
     */
    public void updateFleetStatus(List<Formation> newFormations) throws Exception;

}
