/******************************************************************************
** Description
*
* Use the following code to generate a list of columns to be placed  in all 
* stored procedures returning the full channel list. 
* Uncomment only one of the commented lines accordingly to what you wish to 
* generate (list of input parameters, list of columns, list of parameters)
*******************************************************************************
** 
*******************************************************************************
**  Author: Fulvio
**  Date: 31/05/2017
*******************************************************************************/

DECLARE @channelId int = 1
DECLARE @channelsPerLine int = 10
DECLARE @output varchar(max)
DECLARE @first bit = 1
DECLARE @maxChannelId int = (SELECT MAX(ID) FROM Channel)

WHILE (@channelId <= @maxChannelId) 
BEGIN

    SET @output = ''

    WHILE((@channelId % @channelsPerLine != 0 OR @first = 1) AND @channelId <= @maxChannelId)
    BEGIN
        
        IF (@first = 1)
        BEGIN
            SET @first = 0
            SET @output = '      '
        END
         
        IF NOT EXISTS (SELECT 1 FROM Channel WHERE ID = @channelId)
            SET @channelId = (SELECT MIN(ID) FROM Channel WHERE ID > @channelId)
        
        -- Generate input parameter list - e.g. "@Col1 Decimal(9,6) = NULL, @Col2 Decimal(9,6) = NULL,"
        -- SET @output = @output + '@Col' + CONVERT(varchar, @channelId) + ' ' + (SELECT DataType FROM Channel WHERE ID = @channelId) + ' = NULL, '
        
        -- Generate create table column list - e.g. "[Col1] [Decimal(9,6)] NULL, [Col2] [Decimal(9,6)] NULL,"
        -- SET @output = @output + '[Col' + CONVERT(varchar, @channelId) + '] [' + (SELECT DataType FROM Channel WHERE ID = @channelId) + '] NULL, '
        
        -- Generate table columns list - e.g. "[Col1], [Col2], [Col3],"
        -- SET @output = @output + '[Col' + CONVERT(varchar, @channelId) + '], '

        -- Generate select columns list - e.g. "Col1, Col2, Col3,"
        -- SET @output = @output + 'Col' + CONVERT(varchar, @channelId) + ', '
        
        -- Generate parameters list - e.g. "@Col1, @Col2, @Col3,"
        -- SET @output = @output + '@Col' + CONVERT(varchar, @channelId) + ', '
        
        SET @channelId = @channelId + 1
    
    END
    
    PRINT @output

    SET @first = 1
        
END
GO
