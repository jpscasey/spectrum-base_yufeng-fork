# R2M TypeSafe Configuration Documentation

R2M uses various typesafe configuration files to configure the application.

## Event Bean
| Name              | Component | Type   | Description                                    | Example                                                     | Image |
| ----------------- | --------- | ------ | ---------------------------------------------- | ----------------------------------------------------------- | ----- |
| r2m.event         | Event     | Array  | List of properties for the Event bean          | event: [{id: "eventCode", db: "FaultCode", type: "string"}] |       |
| r2m.event.db      | Event     | String | Database column for property                   | db: "FaultCode"                                             |       |
| r2m.event.id      | Event     | String | Property name/id                               | id: "eventCode"                                             |       |
| r2m.event.screens | Event     | Array  | List of screens this property will be used for | screens: ["eventHistory", "eventDetail"]                    |       |
| r2m.event.type    | Event     | String | Data type of property                          | type: "int"                                                 |       ||

## Event Analysis
| Name                                              | Component | Type   | Description | Example | Image |
| ------------------------------------------------- | --------- | ------ | ----------- | ------- | ----- |
| r2m.eventAnalysis.filters                         |           |        |             |         |       |
| r2m.eventAnalysis.filters.displayName             |           |        |             |         |       |
| r2m.eventAnalysis.filters.name                    |           |        |             |         |       |
| r2m.eventAnalysis.filters.type                    |           |        |             |         |       |
| r2m.eventAnalysis.filters.visible                 |           |        |             |         |       |
| r2m.eventAnalysis.filters.faultCodeAsReference    | Event Analysis | Boolean | If true, records are filtered by FaultCode. If false, by FaultMetaID | faultCodeAsReference:true |       ||


## Event Detail
| Name                                              | Component                   | Type    | Description | Example | Image |
| ------------------------------------------------- | --------------------------- | ------- | ----------- | ------- | ----- |
| r2m.eventDetail.acknowledgeFaultEnabled           |                             |         |             |         |       |
| r2m.eventDetail.acknowledgeFaultRequiresComment   |                             |         |             |         |       |
| r2m.eventDetail.dataTabFields                     |                             |         |             |         |       |
| r2m.eventDetail.dataTabFields.columns             |                             |         |             |         |       |
| r2m.eventDetail.dataTabFields.columns.displayName |                             |         |             |         |       |
| r2m.eventDetail.dataTabFields.columns.format      |                             |         |             |         |       |
| r2m.eventDetail.dataTabFields.columns.handler     |                             |         |             |         |       |
| r2m.eventDetail.dataTabFields.columns.name        |                             |         |             |         |       |
| r2m.eventDetail.dataTabFields.columns.visible     |                             |         |             |         |       |
| r2m.eventDetail.dataTabFields.displayName         |                             |         |             |         |       |
| r2m.eventDetail.dataTabFields.name                |                             |         |             |         |       |
| r2m.eventDetail.dataTabFields.type                |                             |         |             |         |       |
| r2m.eventDetail.dataTabFields.linkable            | Event Detail                | Boolean | Make URLs be auto-detected (working for textareas only atm) | linkable: true |       |
| r2m.eventDetail.deactivateFaultEnabled            |                             |         |             |         |       |
| r2m.eventDetail.deactivateFaultRequiresComment    |                             |         |             |         |       |
| r2m.eventDetail.displayVehicleTabs                |                             |         |             |         |       |
| r2m.eventDetail.staticSiblings	                | Event Detail                | Boolean | If true (default) the vehicle tabs are populated using the current formation. If false, the formation at the time of the event is used | staticSiblings: false |       |
| r2m.eventDetail.eventDataTabComments              |                             |         |             |         |       |
| r2m.eventDetail.eventDataTabEnabled               |                             |         |             |         |       |
| r2m.eventDetail.fields                            |                             |         |             |         |       |
| r2m.eventDetail.fields.displayName                |                             |         |             |         |       |
| r2m.eventDetail.fields.format                     |                             |         |             |         |       |
| r2m.eventDetail.fields.name                       |                             |         |             |         |       |
| r2m.eventDetail.help                      		| Event Detail popup          | Boolean | If true, help icon is available on the top right.            |         |       |
| r2m.eventDetail.mapEnabled                        |                             |         |             |         |       |
| r2m.eventDetail.mouseOverHeaderEnabled            |                             |         |             |         |       |
| r2m.eventDetail.renderer.file                     |                             |         |             |         |       |
| r2m.eventDetail.renderer.name                     |                             |         |             |         |       |
| r2m.eventDetail.toolbar                           |                             |         |             |         |       |
| r2m.eventDetail.toolbar.actionsEnabled            |                             |         |             |         |       |
| r2m.eventDetail.toolbar.actions                   |                             |         |             |         |       |
| r2m.eventDetail.toolbar.actions.name              |                             |         |             |         |       |
| r2m.eventDetail.toolbar.actions.displayName       |                             |         |             |         |       |
| r2m.eventDetail.toolbar.actions.opensPopup        | Event Detail                | Boolean | If true, the button opens a popup | opensPopup: true |       |
| r2m.eventDetail.toolbar.actions.popupHeight       | Event Detail                | Int     | sets height of popup dialog | popupHeight: 500 |       |
| r2m.eventDetail.toolbar.actions.popupWidth        | Event Detail                | Int     | sets width of popup dialog  | popupWidth: 400 |       |
| r2m.eventDetail.toolbar.actions.isLicenced        | Event Detail                | Boolean | If true, the button is licenced | isLicenced: true |       |
| r2m.eventDetail.toolbar.actions.popupFields       | Event Detail                | Array   | List of field configs for popup | {displayName: "Comment", name: "comment", type: "text_area", editable: true} |       |
| r2m.eventDetail.toolbar.navigationEnabled         |                             |         |             |         |       |
| r2m.eventDetail.toolbar.navigation                |                             |         |             |         |       |
| r2m.eventDetail.toolbar.navigation.id             |                             |         |             |         |       |
| r2m.eventDetail.toolbar.navigation.displayName    |                             |         |             |         |       |
| r2m.eventDetail.toolbar.navigation.openUnitSummary   |              |         |             |         |       |
| r2m.eventDetail.toolbar.navigation.unitSummaryTab    |              |         |             |         |       |
| r2m.eventDetail.toolbar.navigation.alternativeAction |              |         |             |         |       ||
| r2m.eventDetail.showSlider                        | Event Detail                | Boolean | If it is true it shows time slider in eventdetail                                              |   |
| r2m.eventDetail.defaultScrollValue			    | Event Detail                | Integer | Value is defaulted to 3, so position of slider will be -30 seconds always if its not saved     |   |
| r2m.eventDetail.periodsListEvent  			    | Event Detail                | List    | This has list of values between -5 minutes to +5 minutes, that used for slider to be displayed |   |

## Event History
| Name                                               | Component | Type   | Description | Example | Image |
| -------------------------------------------------- | --------- | ------ | ----------- | ------- | ----- |
| r2m.eventHistory.columns                           |           |        |             |         |       |
| r2m.eventHistory.columns.displayName               |           |        |             |         |       |
| r2m.eventHistory.columns.exportable                |           |        |             |         |       |
| r2m.eventHistory.columns.format                    |           |        |             |         |       |
| r2m.eventHistory.columns.name                      |           |        |             |         |       |
| r2m.eventHistory.columns.resizable                 |           |        |             |         |       |
| r2m.eventHistory.columns.sortable                  |           |        |             |         |       |
| r2m.eventHistory.columns.type                      |           |        |             |         |       |
| r2m.eventHistory.columns.visible                   |           |        |             |         |       |
| r2m.eventHistory.columns.width                     |           |        |             |         |       |
| r2m.eventHistory.createFaultEnabled                |           |        |             |         |       |
| r2m.eventHistory.createFaultRecoverButton          |           |        |             |         |       |
| r2m.eventHistory.definedFilters                    |           |        |             |         |       |
| r2m.eventHistory.definedFilters.name               |           |        |             |         |       |
| r2m.eventHistory.definedFilters.searchFields       |           |        |             |         |       |
| r2m.eventHistory.definedFilters.searchFields.name  |           |        |             |         |       |
| r2m.eventHistory.definedFilters.searchFields.type  |           |        |             |         |       |
| r2m.eventHistory.definedFilters.searchFields.value |           |        |             |         |       |
| r2m.eventHistory.descMaxCharacters                 |           |        |             |         |       |
| r2m.eventHistory.eventsList                        |           |        |             |         |       |
| r2m.eventHistory.filters                           |           |        |             |         |       |
| r2m.eventHistory.filters.displayName               |           |        |             |         |       |
| r2m.eventHistory.filters.name                      |           |        |             |         |       |
| r2m.eventHistory.filters.type                      |           |        |             |         |       |
| r2m.eventHistory.isDefinedFiltersVisible           |           |        |             |         |       ||

## Current Events Panel
| Name                                                 | Component | Type   | Description | Example | Image |
| ---------------------------------------------------- | --------- | ------ | ----------- | ------- | ----- |
| r2m.liveAndRecentPanel.columns                       |           |        |             |         |       |
| r2m.liveAndRecentPanel.columns.format                |           |        |             |         |       |
| r2m.liveAndRecentPanel.columns.name                  |           |        |             |         |       |
| r2m.liveAndRecentPanel.columns.visible               |           |        |             |         |       |
| r2m.liveAndRecentPanel.columns.width                 |           |        |             |         |       |
| r2m.liveAndRecentPanel.createFaultEnabled            |           |        |             |         |       |
| r2m.liveAndRecentPanel.createFaultRecoverButton      |           |        |             |         |       |
| r2m.createFaultSearch           				|  			| Config  | Configuration for unit search button   |     				|       |
| r2m.createFaultSearch.name 			|  			| String  | Property name/id    									| id: "unitnumber"                                     								|		|
| r2m.createFaultSearch.db 			|  			| String  | Database column for property, as called in the query    | db: "UnitNumber"                               									|		|
| r2m.createFaultSearch.type 			|  			| String  | Data type of property    								| type: "String"               														|		|
| r2m.liveAndRecentPanel.createPopupFields             |           |        |             |         |       |
| r2m.liveAndRecentPanel.createPopupFields.displayName |           |        |             |         |       |
| r2m.liveAndRecentPanel.createPopupFields.idType      |           |        |             |         |       |
| r2m.liveAndRecentPanel.createPopupFields.name        |           |        |             |         |       |
| r2m.liveAndRecentPanel.createPopupFields.width       |           |        |             |         |       |
| r2m.liveAndRecentPanel.descMaxCharacters             |           |        |             |         |       |
| r2m.liveAndRecentPanel.eventsLimit                   |           |        |             |         |       |
| r2m.liveAndRecentPanel.filters                       |           |        |             |         |       |
| r2m.liveAndRecentPanel.fleetSummary.bottom           |           |        |             |         |       |
| r2m.liveAndRecentPanel.panels                        |           |        |             |         |       |
| r2m.liveAndRecentPanel.panels.criteria               |           |        |             |         |       |
| r2m.liveAndRecentPanel.panels.criteria.defaultValue  |           |        |             |         |       |
| r2m.liveAndRecentPanel.panels.criteria.label         |           |        |             |         |       |
| r2m.liveAndRecentPanel.panels.criteria.name          |           |        |             |         |       |
| r2m.liveAndRecentPanel.panels.criteria.type          |           |        |             |         |       |
| r2m.liveAndRecentPanel.panels.label                  |           |        |             |         |       |
| r2m.liveAndRecentPanel.panels.name                   |           |        |             |         |       |
| r2m.liveAndRecentPanel.rowMouseOver                  |           |        |             |         |       |
| r2m.liveAndRecentPanel.rowMouseOver.format           |           |        |             |         |       |
| r2m.liveAndRecentPanel.rowMouseOver.label            |           |        |             |         |       |
| r2m.liveAndRecentPanel.rowMouseOver.value            |           |        |             |         |       ||

## Fleet Location
| Name                                                  | Component         | Type   | Description                                                      | Example                    | Image |
| ----------------------------------------------------- | ----------------- | ------ | ---------------------------------------------------------------- | -------------------------- | ----- |
| r2m.fleetLocation.filters				                |                   |        |                                                                  |                            |       |
| r2m.fleetLocation.filters.id                			| Settings Panel    |String  | Filter id                                                        | id: "Trains"               |       |
| r2m.fleetLocation.filters.label	          			| Settings Panel    |String  | Name to be displayed on Settings Panel                     		| label: "Show Trains"       |       | 
| r2m.fleetLocation.filters.type				        | Settings Panel    |String	 | Filter type: checkBox, textBox									| type: "checkBox"	         |		 |
| r2m.fleetLocation.filters.operator          			| Settings Panel    |String    |  controls the logical operation of the filter   | or: "||"   |       |
| r2m.fleetLocation.filters.showOnUnitLocation			| Settings Panel    |Boolean | if true, the filter is shown in Unit Location                    | showOnUnitLocation: true   |       |
| r2m.fleetLocation.filters.enableByDefault		        | Settings Panel    |Boolean | Default value if filter does not exist on UserConfiguration table| enableByDefault: true	     |		 |
| r2m.fleetLocation.filters.filterChecked		        | Settings Panel    |Boolean | Whether location checked filter should be compared to unit filter| filterChecked: true	     |		 |
| r2m.fleetLocation.defaultView.displayName             |                   |        |                                                                  |                            |       |
| r2m.fleetLocation.defaultView.id                      |                   |        |                                                                  |                            |       |
| r2m.fleetLocation.defaultView.northWest.latitude      |                   |        |                                                                  |                            |       |
| r2m.fleetLocation.defaultView.northWest.longitude     |                   |        |                                                                  |                            |       |
| r2m.fleetLocation.defaultView.southEast.latitude      |                   |        |                                                                  |                            |       |
| r2m.fleetLocation.defaultView.southEast.longitude     |                   |        |                                                                  |                            |       |
| r2m.fleetLocation.defaultView.type                    |                   |        |                                                                  |                            |       |
| r2m.fleetLocation.details.defaultDetailView.latitude  |                   |        |                                                                  |                            |       |
| r2m.fleetLocation.details.defaultDetailView.longitude |                   |        |                                                                  |                            |       |
| r2m.fleetLocation.details.defaultDetailView.type      |                   |        |                                                                  |                            |       |
| r2m.fleetLocation.details.defaultDetailView.zoom      |                   |        |                                                                  |                            |       |
| r2m.fleetLocation.details.fields                      |                   |        |                                                                  |                            |       |
| r2m.fleetLocation.details.fields.displayName          |                   |        |                                                                  |                            |       |
| r2m.fleetLocation.details.fields.format               |                   |        |                                                                  |                            |       |
| r2m.fleetLocation.details.fields.name                 |                   |        |                                                                  |                            |       |
| r2m.fleetLocation.details.fields.referencedField      |                   |        |                                                                  |                            |       |
| r2m.fleetLocation.extraLocationDropdown               |                   |        | Allowes to add extra dropdown widget near "Select a station" menu    |                            |       |
| r2m.fleetLocation.extraLocationDropdown.displayName   |                   |String  | Contains the i18n keyword for displaying the placeholder for the first element in the dropdown widget |                            | "SelectStation" (must exists in i18n files)     |
| r2m.fleetLocation.extraLocationDropdown.locations     |                   |Object  | Contains the new locations to display on the map                 |                            |       |
| r2m.fleetLocation.extraLocationDropdown.locations.text|                   |String  | Contains the name of the location                                | "Amsterdam"                |       |
| r2m.fleetLocation.extraLocationDropdown.locations.latitude|               |Double  | Contains the latitude                                            |                            |       |
| r2m.fleetLocation.extraLocationDropdown.locations.longitude|              |Double  | Contains the longitude                                            |                            |       |
| r2m.fleetLocation.extraViews                          |                   |        |                                                                  |                            |       |
| r2m.fleetLocation.extraViews.displayName              |                   |        |                                                                  |                            |       |
| r2m.fleetLocation.extraViews.id                       |                   |        |                                                                  |                            |       |
| r2m.fleetLocation.extraViews.northWest.latitude       |                   |        |                                                                  |                            |       |
| r2m.fleetLocation.extraViews.northWest.longitude      |                   |        |                                                                  |                            |       |
| r2m.fleetLocation.extraViews.southEast.latitude       |                   |        |                                                                  |                            |       |
| r2m.fleetLocation.extraViews.southEast.longitude      |                   |        |                                                                  |                            |       |
| r2m.fleetLocation.extraViews.type                     |                   |        |                                                                  |                            |       |
| r2m.fleetLocation.markers                             |                   |        |                                                                  |                            |       |
| r2m.fleetLocation.markers.enabledByDefault            |                   |        |                                                                  |                            |       |
| r2m.fleetLocation.markers.jsInclude                   |                   |        |                                                                  |                            |       |
| r2m.fleetLocation.markers.jsObject                    |                   |        |                                                                  |                            |       |
| r2m.fleetLocation.markers.label                       |                   |        |                                                                  |                            |       |
| r2m.fleetLocation.markers.redrawOnViewChange          |                   |        |                                                                  |                            |       |
| r2m.fleetLocation.markers.type                        |                   |        |                                                                  |                            |       |
| r2m.fleetLocation.markers.z-index                     |                   |        |                                                                  |                            |       |
| r2m.fleetLocation.maxInactiveTime                     |                   |        |                                                                  |                            |       |
| r2m.fleetLocation.parentPlugin.cssList                |                   |        |                                                                  |                            |       |
| r2m.fleetLocation.parentPlugin.jsList                 |                   |        |                                                                  |                            |       |
| r2m.fleetLocation.parentPlugin.name                   |                   |        |                                                                  |                            |       |
| r2m.fleetLocation.plugins                             |                   |        |                                                                  |                            |       |
| r2m.fleetLocation.plugins.jsList                      |                   |        |                                                                  |                            |       |
| r2m.fleetLocation.plugins.name                        |                   |        |                                                                  |                            |       |
| r2m.fleetLocation.routes                              |                   |        |                                                                  |                            |       |
| r2m.fleetLocation.routes.file                         |                   |        |                                                                  |                            |       |
| r2m.fleetLocation.routes.fleetId                      |                   |        |                                                                  |                            |       |
| r2m.fleetLocation.showFleetFullNames					|                   |        |                                                                  |                            |       |
| r2m.fleetLocation.showClearApplyButtons               | Settings Panel    |Boolean | Clear and Apply buttons are displayed in settings panel 			|                            |       |
| r2m.fleetLocation.showTrainCheckBox               	| Settings Panel    |Boolean | If true, Show Trains check box is shown in setting panel. 		|                            |       |
| r2m.fleetLocation.showTrainLabelCheckBox              | Settings Panel    |Boolean | If true, Show Train Labels check box is shown in setting panel. 	|                            |       |
| r2m.fleetLocation.showAerialCheckBox               	| Settings Panel    |Boolean | If true, Aerial check box is shown in setting panel. 			|                            |       |
| r2m.fleetLocation.stations                            |                   |        |                                                                  |                            |       |
| r2m.fleetLocation.stations.file                       |                   |        |                                                                  |                            |       |
| r2m.fleetLocation.stations.fleetId                    |                   |        |                                                                  |                            |       |
| r2m.fleetLocation.tabName                             |                   |        |                                                                  |                            |       |
| r2m.fleetLocation.trainIcon                           |                   |        |                                                                  |                            |       |
| r2m.fleetLocation.trainIcon.image                     |                   |        |                                                                  |                            |       |
| r2m.fleetLocation.trainIcon.name                      |                   |        |                                                                  |                            |       |
| r2m.fleetLocation.trainIcon.z-index                   |                   |        |                                                                  |                            |       |
| r2m.fleetLocation.trainLabel.height                   |                   |        |                                                                  |                            |       |
| r2m.fleetLocation.trainLabel.pointer                  |                   |        |                                                                  |                            |       |
| r2m.fleetLocation.trainLabel.width                    |                   |        |                                                                  |                            |       |
| r2m.fleetLocation.trainLabel.x-offset                 |                   |        |                                                                  |                            |       |
| r2m.fleetLocation.trainLabel.y-offset                 |                   |        |                                                                  |                            |       |
                                                                                                                                                                                     
## Fleet Summary
| Name                                                     | Component              | Type    | Description                                                         | Example                      | Image |
| -------------------------------------------------------- | ---------------------- | ------  | ------------------------------------------------------------------- | ---------------------------- | ----- |
| r2m.fleetGridStyles.blueYellowDataGroup                  |                        |         |                                                                     |                              |       |
| r2m.fleetGridStyles.blueYellowDataGroup.channelCategory  |                        |         |                                                                     |                              |       |
| r2m.fleetGridStyles.blueYellowDataGroup.style            |                        |         |                                                                     |                              |       |
| r2m.fleetGridStyles.blueYellowGroup                      |                        |         |                                                                     |                              |       |
| r2m.fleetGridStyles.blueYellowGroup.channelCategory      |                        |         |                                                                     |                              |       |
| r2m.fleetGridStyles.blueYellowGroup.style                |                        |         |                                                                     |                              |       |
| r2m.fleetGridStyles.common                               |                        |         |                                                                     |                              |       |
| r2m.fleetGridStyles.common.channelCategory               |                        |         |                                                                     |                              |       |
| r2m.fleetGridStyles.common.style                         |                        |         |                                                                     |                              |       |
| r2m.fleetGridStyles.fleetActiveCab                       |                        |         |                                                                     |                              |       |
| r2m.fleetGridStyles.fleetActiveCab.channelCategory       |                        |         |                                                                     |                              |       |
| r2m.fleetGridStyles.fleetActiveCab.style                 |                        |         |                                                                     |                              |       |
| r2m.fleetGridStyles.fleetHeadcode                        |                        |         |                                                                     |                              |       |
| r2m.fleetGridStyles.fleetHeadcode.channelCategory        |                        |         |                                                                     |                              |       |
| r2m.fleetGridStyles.fleetHeadcode.style                  |                        |         |                                                                     |                              |       |
| r2m.fleetGridStyles.fleetLocation                        |                        |         |                                                                     |                              |       |
| r2m.fleetGridStyles.fleetLocation.channelCategory        |                        |         |                                                                     |                              |       |
| r2m.fleetGridStyles.fleetLocation.style                  |                        |         |                                                                     |                              |       |
| r2m.fleetGridStyles.group                                |                        |         |                                                                     |                              |       |
| r2m.fleetGridStyles.group.channelCategory                |                        |         |                                                                     |                              |       |
| r2m.fleetGridStyles.group.style                          |                        |         |                                                                     |                              |       |
| r2m.fleetGridStyles.headCodeStyle                        |                        |         |                                                                     |                              |       |
| r2m.fleetGridStyles.headCodeStyle.channelCategory        |                        |         |                                                                     |                              |       |
| r2m.fleetGridStyles.headCodeStyle.style                  |                        |         |                                                                     |                              |       |
| r2m.fleetGridStyles.nonValidated                         |                        |         |                                                                     |                              |       |
| r2m.fleetGridStyles.nonValidated.channelCategory         |                        |         |                                                                     |                              |       |
| r2m.fleetGridStyles.nonValidated.style                   |                        |         |                                                                     |                              |       |
| r2m.fleetGridStyles.timestamp                            |                        |         |                                                                     |                              |       |
| r2m.fleetGridStyles.timestamp.channelCategory            |                        |         |                                                                     |                              |       |
| r2m.fleetGridStyles.timestamp.style                      |                        |         |                                                                     |                              |       |
| r2m.fleetGridStyles.timestampGreyFault                   |                        |         |                                                                     |                              |       |
| r2m.fleetGridStyles.timestampGreyFault.channelCategory   |                        |         |                                                                     |                              |       |
| r2m.fleetGridStyles.timestampGreyFault.style             |                        |         |                                                                     |                              |       |
| r2m.fleetGridStyles.timestampOrangeFault                 |                        |         |                                                                     |                              |       |
| r2m.fleetGridStyles.timestampOrangeFault.channelCategory |                        |         |                                                                     |                              |       |
| r2m.fleetGridStyles.timestampOrangeFault.style           |                        |         |                                                                     |                              |       |
| r2m.fleetGridStyles.timestampPurpleFault                 |                        |         |                                                                     |                              |       |
| r2m.fleetGridStyles.timestampPurpleFault.channelCategory |                        |         |                                                                     |                              |       |
| r2m.fleetGridStyles.timestampPurpleFault.style           |                        |         |                                                                     |                              |       |
| r2m.fleetGridStyles.timestampRedFault                    |                        |         |                                                                     |                              |       |
| r2m.fleetGridStyles.timestampRedFault.channelCategory    |                        |         |                                                                     |                              |       |
| r2m.fleetGridStyles.timestampRedFault.style              |                        |         |                                                                     |                              |       |
| r2m.fleetGridStyles.unitActive                           |                        |         |                                                                     |                              |       |
| r2m.fleetGridStyles.unitActive.channelCategory           |                        |         |                                                                     |                              |       |
| r2m.fleetGridStyles.unitActive.style                     |                        |         |                                                                     |                              |       |
| r2m.fleetSummary.ShowFleetFullNames                      |                        |         |                                                                     |                              |       |
| r2m.fleetSummary.ShowSingleFleetTab                      |                        |         |                                                                     |                              |       |
| r2m.fleetSummary.mouseOverHeaderEnabled                  | Fleet Summary          | Boolean | If true (default), on mouseover heading header title is displayed as tooltip  | mouseOverHeaderEnabled: false                             |       |
| r2m.fleetSummary.drillDown                               |                        |         |                                                                     |                              |       |
| r2m.fleetSummary.drillDown.displayUnitRow                | FleetSummary Drilldown | Boolean | If true (default), displays row of unit numbers. Hides row if false | displayUnitRow: true         |       |
| r2m.fleetSummary.plugins                                 |                        |         |                                                                     |                              |       |
| r2m.fleetSummary.plugins.cssList                         |                        |         |                                                                     |                              |       |
| r2m.fleetSummary.plugins.jsList                          |                        |         |                                                                     |                              |       |
| r2m.fleetSummary.plugins.name                            |                        |         |                                                                     |                              |       |
| r2m.fleets.{fleet}.columnUnitIdentifier                  |                        |         |                                                                     |                              |       |
| r2m.fleets.{fleet}.columns                               |                        |         |                                                                     |                              |       |
| r2m.fleets.{fleet}.columns.columns                       |                        |         |                                                                     |                              |       |
| r2m.fleets.{fleet}.columns.columns.displayName           |                        |         |                                                                     |                              |       |
| r2m.fleets.{fleet}.columns.columns.format                |                        |         |                                                                     |                              |       |
| r2m.fleets.{fleet}.columns.columns.handler               |                        |         |                                                                     |                              |       |
| r2m.fleets.{fleet}.columns.columns.licence               |                        |         |                                                                     |                              |       |
| r2m.fleets.{fleet}.columns.columns.name                  |                        |         |                                                                     |                              |       |
| r2m.fleets.{fleet}.columns.columns.sortable              |                        |         |                                                                     |                              |       |
| r2m.fleets.{fleet}.columns.columns.style                 |                        |         |                                                                     |                              |       |
| r2m.fleets.{fleet}.columns.columns.type                  |                        |         |                                                                     |                              |       |
| r2m.fleets.{fleet}.columns.columns.width                 |                        |         |                                                                     |                              |       |
| r2m.fleets.{fleet}.columns.cssClass                      |                        |         |                                                                     |                              |       |
| r2m.fleets.{fleet}.columns.displayName                   |                        |         |                                                                     |                              |       |
| r2m.fleets.{fleet}.columns.licence                       |                        |         |                                                                     |                              |       |
| r2m.fleets.{fleet}.columns.name                          |                        |         |                                                                     |                              |       |
| r2m.fleets.{fleet}.columns.type                          |                        |         |                                                                     |                              |       |
| r2m.fleets.{fleet}.columns.visible                       |                        |         |                                                                     |                              |       |
| r2m.fleets.{fleet}.datasource                            |                        |         |                                                                     |                              |       |
| r2m.fleets.{fleet}.defaultSortingColumn                  |                        |         |                                                                     |                              |       |
| r2m.fleets.{fleet}.defaultSortingDescending              |                        |         |                                                                     |                              |       |
| r2m.fleets.{fleet}.filters                               | Filters                |         |                   |                              |       |
| r2m.fleets.{fleet}.filters.columns                       | Filters                | List of Strings | Columns that the filter will check against | columns: ["Speed"]|   |
| r2m.fleets.{fleet}.filters.id                            | Filters                | String  | Filter id | id: "speedMinFilter"            |       | 
| r2m.fleets.{fleet}.filters.label                         | Filters                | String  | Display name of the filter             | label: "Speed Min"          |       |
| r2m.fleets.{fleet}.filters.type                          | Filters                | String  | Filter type: textBox, minTextBox, maxTextBox, checkBox | type: "minTextBox"     |       |
| r2m.fleets.{fleet}.filters.optional                      | Filters                | Boolean     | If at least one "optional: true" filter matches, the row is displayed. If false or absent, the filter must match for the row to display. | optional: true  |       |
| r2m.fleets.{fleet}.name                                  |                        |         |                                                                     |                              |       |
| r2m.fleets.{fleet}.tabOrder                              |                        |         |                                                                     |                              |       |
| r2m.fleets.{fleet}.vehicleLevel                          |                        |         |                                                                     |                              |       ||
                                                                                                                                                                                          

## Channel Data
| Name                                                       | Component   |  Type   | Description                          | Example                        | Image |
| -----------------------------------------------            | ----------- | ------- | -------------------------------------| -----------------------------  | ----- |
| r2m.channelData.columns               		             |             |         |                                      |                                |       |
| r2m.channelData.columns.nonStockColumns                    |             |         |                                      |                                |       |
| r2m.channelData.columns.nonStockColumns.rowMouseOver.value |Unit Summary | String  | Field to display as channel tooltip  | value: "description"			 |       |
| r2m.channelData.columns.stockColumn                        |             |         |                                      |                                |       |
| r2m.channelData.screen			  			             |             |         |                                      |                                |       |
| r2m.channelData.timestampGroupName                         |Unit Summary - Channel Data | String | Add timestamp row to the group specified. If absent, no channel is added | timestampGroupName: "SR2GeneralGroup"    |       |

                                                                                                                                                                                       

## Data View
| Name                                            | Component |  Type   | Description                                                                                 | Example | Image |
| ----------------------------------------------- | --------- | ------- | ------------------------------------------------------------------------------------------- | ------- | ----- |
| r2m.dataView.maxDescriptionLength               |           |         |                                                                                             |         |       |
| r2m.dataView.virtualScrollBar 				  |           |         |                                                                                             |         |       |
| r2m.dataView.isSystemChartsIncluded			  |           |         |                                                                                             |         |       |
| r2m.dataView.fleets.{fleet}.isChartEnabled  	  | Data View | Boolean | If true, Graph view is available on Data view tab. If false, only grid view is available.   |         |       |
| r2m.dataView.channelGroups.label.label  	  	  | Data View | String  | The name of the channel field to be used for the label display name                         |         |       |
| r2m.dataView.channelGroups.label.value  	      | Data View | String  | The name of the channel field to be used for the label mouseover text                       |         |       |
| r2m.dataView.chartChannelLabel                  | Data View | String  | The channel field to use for the chart label - name or description                		  |         |       |
| r2m.dataView.tableChannel.label.label           | Data View | String  | The channel field to use for the table header - name or description                		  |         |       |
| r2m.dataView.tableChannel.label.value           | Data View | String  | The channel field to use for the table header mouseover text - name or description          |         |       |


## Schematic

| Name                                            | Component |  Type   | Description                                                                                 | Example | Image |
| ----------------------------------------------- | --------- | ------- | ------------------------------------------------------------------------------------------- | ------- | ----- |
| r2m.schematic               					  | Schematic | Object  | Object which contains a list of items representing the schematic files with related 		  |         |       |
|												  |			  |			| configuration options and extra plugins required by JavaScript classes					  |         |       |
| r2m.schematic.items               			  | Schematic | Array   |             																				  |         |       |
| r2m.schematic.items.name             			  | Schematic | String  | Contains the name of the schematic which works as an identifier for each schematic. Will	  |name:"class91_1"	|
|												  |			  |			| be used as value for HTML select tag and is used as name of the div which contains the	  |			|		|
| 												  |			  |			| Raphael.js canvas object.            														  |         |       |
| r2m.schematic.items.displayName                 | Schematic | String  | Used as value to be displayed in HTML select tag when user selects the schematic to display.|displayName:"Class 91 - Schematic 1" 
| r2m.schematic.items.javaScriptConstructor 	  | Schematic | String  | This value represents the name of the Constructor of the JavaScript class containing the 	  |			|		|
|												  |			  |			| Schematic diagram. | For example if the configuration contains: 							  |			|		|
|												  |			  |			| `javaScriptConstructor: "Class91Schematic1"` the file `raphael.schematic.class91.1.js` 	  |			|		|
|												  |			  |			| is the class which display schematic using Raphael, the constructor for *schematic class91* |			|		|
|												  |			  |			| is the one contained in **javaScriptcConstructor**. 										  |			|		|
| r2m.schematic.items.javaScriptClassPath         | Schematic | String  | The path where the JavaScript schematic is placed.  |javaScriptClassPath:"js/schematic/raphael.schematic.class91.1.js"
| r2m.schematic.items.canvas_size 				  | Schematic | Object  | Contains the width and height for the div which contains the Raphael.js Schematic 		  |         |       |
| r2m.schematic.items.canvas_size.width			  | Schematic | Integer |  																							  | width: 700      |
| r2m.schematic.items.canvas_size.height 		  | Schematic | Integer |																							  | width: 575      |
| r2m.schematic.items.raphael_size 				  | Schematic | Object  | Contains the width and height required to create a Raphael.js object 						  |         |       |
| r2m.schematic.items.raphael_size.width 		  | Schematic | Integer |  																							  | width: 700      |
| r2m.schematic.items.raphael_size.height 		  | Schematic | Integer |  																							  | width: 565      |
| r2m.schematic.items.schematic_params 			  | Schematic | Object  | Contains the xOffset and yOffest and scale which are values required by each JavaScript Schematic class 		|
| r2m.schematic.items.schematic_params.xoffset 	  | Schematic | Integer |   																						  | 		|       |
| r2m.schematic.items.schematic_params.yoffset    | Schematic | Integer |   																						  |   	 	|       |
| r2m.schematic.items.schematic_params.scale 	  | Schematic | Double  |   																						  | 		|       |
| r2m.schematic.schematicdata	           		  | Schematic | Array   | Contains all the channels for the schematic view											  |         |       |
| r2m.schematic.schematicdata.name	        	  | Schematic | Array   | 																							  |         |       |
| r2m.schematic.schematicdata.channelId           | Schematic | String  | 																							  |         |       |
| r2m.schematic.schematicdata.type	           	  | Schematic | String  | 																							  |         |       |
| r2m.schematic.schematicdata.default	          | Schematic | String  | Default value of the channel (optional)													  |         |       |
| r2m.schematic.schematicdata.unitOfMeasurement	  | Schematic | String  | Unit of measurement of the channel (optional)												  |         |       |
| r2m.schematic.schematicdata.view	           	  | Schematic | Array   | List of schematic views the schematic channel belongs to, must match the items.name		  | ["class91_1","class91_2"]


## Recovery
| Name                              | Component | Type   | Description                                         | Example | Image |
| --------------------------------- | --------- | ------ | --------------------------------------------------- | ------- | ----- |
| r2m.recovery.faultColumns			| Recovery  | Array  | List of columns displayed in Fault Summary table    |         |       ||


## Unit Summary
| Name                                   | Component    |  Type    | Description                                                                                                |Example| Image |
| -------------------------------------- | ------------ | -------- | ---------------------------------------------------------------------------------------------------------- | ----- | ----- |
| r2m.unitSummary.combinedHeader         | Unit Summary | Boolean  | If true, the unitnumbers will be displayed as combined unit in the unitsummary header (ie "82225 - 91126") | true  |       |

## Unit Detail                                                        
| Name                                                                       | Component   | Type   | Description                                                                     | Example                                     | Image |
| ------------------------------------------------------                     | ---------   | ------ | ------------------------------------------------------------------------------  | ------------------------------------------- | ----- |
| r2m.unitDetail.showEventHistoryButton                                      | Unit Detail |Boolean | If true, quick link button to Event History with pre-defined search is displayed|                                             |       |
| r2m.unitDetail.headerFields                                                |             |        |                                                                                 |                                             |       |
| r2m.unitDetail.headerFields.displayName                                    |             |        |                                                                                 |                                             |       |
| r2m.unitDetail.headerFields.format                                         |             |        |                                                                                 |                                             |       |
| r2m.unitDetail.headerFields.name                                           |             |        |                                                                                 |                                             |       |
| r2m.unitDetail.plugins.name                                                | Unit Detail | String |                                                                                 |                                             |       |
| r2m.unitDetail.plugins.sources                                             | Unit Detail | List   | List of js files to be added to the screen                                                                                |                                             |       |
| r2m.unitDetail.plugins.stylesheets                                         | Unit Detail | List   | List of css files to be added to the screen                                                                                |                                             |       |
| r2m.unitDetail.udPanel.defaultStockTab                                     | Unit Detail | Config | Configuration for the stock tab displayed by default (can be missing) 		  | defaultStockTab: {type: "formation", displayName: "All"} |       |
| r2m.unitDetail.udPanel.defaultStockTab.type                                | Unit Detail | String | Stock level displayed when the default tab is selected (can be formation, unit or vehicle) | type: "formation" |       |
| r2m.unitDetail.udPanel.defaultStockTab.displayName                         | Unit Detail | String | Name of the default stock tab                                                   | displayName: "All" |       | 
| r2m.unitDetail.udPanel.stockTabsType                                       | Unit Detail | String | Stock level of the other tabs (can be formation, unit or vehicle)               | stockTabsType: "unit" |       |
| r2m.unitDetail.udPanel.rows                                                |             |        |                                                                                 |                                             |       |
| r2m.unitDetail.udPanel.rows.cells                                          |             |        |                                                                                 |                                             |       |
| r2m.unitDetail.udPanel.rows.cells.columns                                  |             |        |                                                                                 |                                             |       |
| r2m.unitDetail.udPanel.rows.cells.columns.displayName                      |             |        |                                                                                 |                                             |       |
| r2m.unitDetail.udPanel.rows.cells.columns.format                           |             |        |                                                                                 |                                             |       |
| r2m.unitDetail.udPanel.rows.cells.columns.handler                          |             |        |                                                                                 |                                             |       |
| r2m.unitDetail.udPanel.rows.cells.columns.name                             |             |        |                                                                                 |                                             |       |
| r2m.unitDetail.udPanel.rows.cells.columns.sortByColumn                     |             |        |                                                                                 |                                             |       |
| r2m.unitDetail.udPanel.rows.cells.columns.sortable                         |             |        |                                                                                 |                                             |       |
| r2m.unitDetail.udPanel.rows.cells.columns.type                             |             |        |                                                                                 |                                             |       |
| r2m.unitDetail.udPanel.rows.cells.columns.visible                          |             |        |                                                                                 |                                             |       |
| r2m.unitDetail.udPanel.rows.cells.columns.width                            |             |        |                                                                                 |                                             |       |
| r2m.unitDetail.udPanel.rows.cells.columns.columnMouseOver                  | Unit Detail | Array  | Adds a mouseover to the column. This object is parsed as a MouseOverText java object      |       |       |
| r2m.unitDetail.udPanel.rows.cells.columns.columnMouseOver.label            | Unit Detail | String | Prefixes the mouseover with a label.                                            | label: "Category"                                             |       |
| r2m.unitDetail.udPanel.rows.cells.columns.columnMouseOver.value            | Unit Detail | String | DB column name of the mouseover                                                 | value: "faultUnitID"                                              |       |
| r2m.unitDetail.udPanel.rows.cells.columns.columnMouseOver.format           | Unit Detail | String | Format of the mouseover                                                         | format: "{timeAgo}"                                            |       |
| r2m.unitDetail.udPanel.rows.cells.columns.columnMouseOver.constant         | Unit Detail | String | Constant value for the mouseover                                                | constant: "Fault"                                            |       |
| r2m.unitDetail.udPanel.rows.cells.filters                                  | Unit Detail | Array  | List of filters for the related table                                           | filters: [{name: "categoryId", label: "Category", type: "multiple_select", defaultOptions: []}] |       |
| r2m.unitDetail.udPanel.rows.cells.filters.name                             | Unit Detail | String | Filter name, needs to match the field you want to filter                        | name: "categoryId"                          |       |
| r2m.unitDetail.udPanel.rows.cells.filters.label                            | Unit Detail | String | Filter label                                                                    | label: "Category"                           |       |
| r2m.unitDetail.udPanel.rows.cells.filters.type                             | Unit Detail | String | Filter type                                                                     | type: "multiple_select"                     |       |
| r2m.unitDetail.udPanel.rows.cells.filters.fleets            				 | Unit Detail | List of Strings | If null, filter added for all fleets. If not null, filter added only to fleets in the list                                                              | fleets: ["class90","321"]                     |       |
| r2m.unitDetail.udPanel.rows.cells.filters.defaultOptions                   | Unit Detail | Array  | Filter options not coming from database                                         | defaultOptions: [{id: "", displayName: ""}] |       |
| r2m.unitDetail.udPanel.rows.cells.filters.defaultOptions.id                | Unit Detail | String | Option id                                                                       | id: "all"                                   |       |
| r2m.unitDetail.udPanel.rows.cells.filters.defaultOptions.displayName       | Unit Detail | String | Option label                                                                    | id: "All"  
| r2m.unitDetail.udPanel.rows.cells.filters.defaultOptions.checkedAsDefault  | Unit Detail | Boolean | Is checkbox filter checked by default                                          | id: "All"                                   |       |
| r2m.unitDetail.udPanel.rows.cells.filters.defaultOptions.include values    | Unit Detail | Boolean | Does checkbox filter include the values from default options, or exclude them? | id: "All"                                   |       |
| r2m.unitDetail.udPanel.rows.cells.mode                                     |             |        |                                                                                 |                                             |       |
| r2m.unitDetail.udPanel.rows.cells.name                                     |             |        |                                                                                 |                                             |       |
| r2m.unitDetail.udPanel.rows.cells.title                                    |             |        |                                                                                 |                                             |       |
| r2m.unitDetail.udPanel.rows.position                                       |             |        |                                                                                 |                                             |       |
| r2m.unitDetail.maximoURL                                                   | Unit Detail | List   | List of maximo types (NOTE: NS only)                                            |        |       |
| r2m.unitDetail.maximoURL.maximoType                                        | Unit Detail | String | Maximo type, either work order or service request | maximoType: "Work Order"    |       |
| r2m.unitDetail.maximoURL.url                                               | Unit Detail | String | Url to maximo for the given type                                                |        |       |
| r2m.unitDetail.workOrder                                                   | Unit Detail | Array  | List of properties for the Work Order bean                                      | workOrder: [{id: "description", db: "Description", type: "string"}] |       |
| r2m.unitDetail.workOrder.id                                                | Unit Detail | String | Property id                                                                     | id: "description"                           |       |
| r2m.unitDetail.workOrder.database                                          | Unit Detail | String | Database column name for property 											  | db: "Description"                           |       |
| r2m.unitDetail.workOrder.type                                              | Unit Detail | String | Data type for property                                                          | type: "string"                              |       |
                                                                                                                                                                                                                             
## Channel Definition
| Name                                      | Component | Type   | Description | Example | Image |
| ----------------------------------------- | --------- | ------ | ----------- | ------- | ----- |
| r2m.channelDefinition.columns             |           |        |             |         |       |
| r2m.channelDefinition.columns.displayName |           |        |             |         |       |
| r2m.channelDefinition.columns.handler     |           |        |             |         |       |
| r2m.channelDefinition.columns.name        |           |        |             |         |       |
| r2m.channelDefinition.columns.sortable    |           |        |             |         |       |
| r2m.channelDefinition.columns.type        |           |        |             |         |       |
| r2m.channelDefinition.columns.visible     |           |        |             |         |       |
| r2m.channelDefinition.columns.width       |           |        |             |         |       |
| r2m.channelDefinition.details             |           |        |             |         |       |
| r2m.channelDefinition.details.displayName |           |        |             |         |       |
| r2m.channelDefinition.details.editable    |           |        |             |         |       |
| r2m.channelDefinition.details.maxLength   |           |        |             |         |       |
| r2m.channelDefinition.details.min         |           |        |             |         |       |
| r2m.channelDefinition.details.name        |           |        |             |         |       |
| r2m.channelDefinition.details.type        |           |        |             |         |       ||

## Downloads
| Name                             	                             | Component | Type   	| Description                                                                                           | Example                    | Image |
| -------------------------------- 	                             | --------- | ------ 	| --------------------------------------------------------                                              | -------                    | ----- |
| r2m.download.VehicleSelector     	                             |           |Boolean 	| If true, Vehicle search field is shown		                                                        | VehicleSelector: true      |       |
| r2m.download.isRetrievable	   	                             |           |Boolean 	| If true, button 'Retrieve' is shown	                                                                | isRetrievable: false       |       |
| r2m.download.thresholdTime	   	                             |           |Integer	| If popupfilters endTime and startTime are in place, max time difference in seconds	                | thresholdTime: 1800        |       |
| r2m.download.gapTime			  	                             |           |Integer	| When the startTime is set to a value, the endTime is automatically set to startTime + gapTime	seconds | timeGap: 60                |       |
| r2m.download.columns.displayName 	                             |           |String  	|                                                                                                       |                            |       |
| r2m.download.columns.name        	                             |           |String  	|                                                                                                       |                            |       |
| r2m.download.columns.sortable    	                             |           |Boolean    |			                                                                                            |                            |       |
| r2m.download.columns.style       	                             |           |String     | 				                                                                                        |                            |       |
| r2m.download.columns.type        	                             |           |String     | 														                                                |                            |       |
| r2m.download.columns.width       	                             |           |Integer    |                                                                                                      |                            |       |
| r2m.download.fields.id       	   	                             |           |Integer    |                                                                                                      |                            |       |
| r2m.download.fields.db       	   	                             |           |String     | Database column for property, as called in the query                                                 | db: "unitNumber"           |       |
| r2m.download.fields.type       	                             |           |String     | Data type of property                                                                                | type: "string"             |       |
| r2m.download.popupFilters.displayName                          |           |String     |                                                                   					                |                            |       |
| r2m.download.popupFilters.name       	                         |           |String     |                                                                   					                |                            |       |
| r2m.download.popupFilters.type       	                         |           |String     |                                                                   					                |                            |       |
| r2m.download.popupFilters.disabledCondition			         |           |String     | List of conditions that should match to disable the filter       					                |                            |       |
| r2m.download.popupFilters.disabledCondition.filterName         |           |String     | Filter name to be checked                 					                						|filterName: "fileTypeName", |       |
| r2m.download.popupFilters.disabledCondition.operator       	 |           |String     | The logical operation of the condition                                                               |operator: "==",             |       |
| r2m.download.popupFilters.disabledCondition.value       	     |           |String     | FilterName's value to be checked                                                                     |value: "Event Recorder"     |       |



## Editor
| Name                                             | Component           | Type    | Description                                                                      | Example          | Image |
|--------------------------------------------------|---------------------|---------|----------------------------------------------------------------------------------|----------------- |-------|
| r2m.editor.packages                              |                     |         |                                                                                  |                  |       |
| r2m.editor.packages.display-name                 |                     |         |                                                                                  |                  |       |
| r2m.editor.packages.name                         |                     |         |                                                                                  |                  |       |
| r2m.editor.packages.parent-path                  |                     |         |                                                                                  |                  |       |
| r2m.editor.packages.type                         |                     |         |                                                                                  |                  |       |
| r2m.editor.recovery.key                          |                     |         |                                                                                  |                  |       |
| r2m.editor.recovery.url                          |                     |         |                                                                                  |                  |       |
| r2m.editor.variables                             |                     |         |                                                                                  |                  |       |
| r2m.editor.variables.description                 |                     |         |                                                                                  |                  |       |
| r2m.editor.variables.name                        |                     |         |                                                                                  |                  |       |
| r2m.editor.variables.db                          | Email configuration | String  | Variable name to be catched from event fields.		                              | db: "category"   |       |
| r2m.editor.variables.type                        |                     |         |                                                                                  |                  |       |
| r2m.editor.faultMetaExtraFields                  | Fault configuration | Array   | Array of extra fields for a fault definition                                     |                  |       |
| r2m.editor.faultMetaExtraFields.i18nDisplayName  | Fault configuration | String  | Key of the label on the translation file (.properties)                           |                  |       |
| r2m.editor.faultMetaExtraFields.name             | Fault configuration | String  | Name of the field (will be the name in FaultMetaExtraField table)                |                  |       |
| r2m.editor.faultMetaExtraFields.type             | Fault configuration | String  | Valid values : text_area, input_text, input_number, select                       |                  |       |
| r2m.editor.faultMetaExtraFields.min              | Fault configuration | Integer | The min value for a input_number field                                           |                  |       |
| r2m.editor.faultMetaExtraFields.max              | Fault configuration | Integer | The max value for a input_number field                                           |                  |       |
| r2m.editor.faultMetaExtraFields.values           | Fault configuration | Array   | The list of valid values for a select field (see r2m.dropdowns.xxx)              |                  |       ||
| r2m.editor.overrideFields           			   | Fault configuration | Array   | Array of override fields for a fault definition.               				  |                  |       |
| r2m.editor.overrideFields.i18nDisplayName  	   | Fault configuration | String  | Key of the label on the translation file (.properties)                           |                  |       |
| r2m.editor.overrideFields.name             	   | Fault configuration | String  | Name of the field                 											      |                  |       |
| r2m.editor.overrideFields.type             	   | Fault configuration | String  | Valid values : text_area, input_text, input_number, select                       |                  |       |
| r2m.editor.overrideFields.min              	   | Fault configuration | Integer | The min value for a input_number field                                           |                  |       |
| r2m.editor.overrideFields.max              	   | Fault configuration | Integer | The max value for a input_number field                                           |                  |       |
| r2m.editor.overrideFields.values           	   | Fault configuration | Array   | The list of valid values for a select field (see r2m.dropdowns.xxx)              |                  |       |

## Screens
| Name                  | Component |  Type   | Description                                                           | Example | Image |
| --------------------- | --------- | ------- | --------------------------------------------------------------------- | ------- | ----- |
| r2m.screens           |           |         |                                                                       |         |       |
| r2m.screens.external  |           |         |                                                                       |         |       |
| r2m.screens.groupName |           |         |                                                                       |         |       |
| r2m.screens.grouped   |           |         |                                                                       |         |       |
| r2m.screens.licence   |           |         |                                                                       |         |       |
| r2m.screens.name      |           |         |                                                                       |         |       |
| r2m.screens.path      |           |         |                                                                       |         |       |
| r2m.screens.position  |           |         |                                                                       |         |       |
| r2m.screens.view      |           |         |                                                                       |         |       |
| r2m.screens.visible   |           |         |                                                                       |         |       |
| r2m.screens.help	    |           | Boolean | If true, help section is available on the screen                      |         |       ||

## Stream Analysis
| Name                                                                             | Component | Type | Description                                                     | Example | Image |
|----------------------------------------------------------------------------------|-----------|------|-----------------------------------------------------------------|---------|-------|
| r2m.stream-analysis.config.spectrum.rule.engine.faults.retracted.externally      |           |      |                                                                 |         |       |
| r2m.stream-analysis.config.spectrum.rule.engine.faults.retracted.startup         |           |      |                                                                 |         |       |
| r2m.stream-analysis.config.spectrum.rule.engine.gap.enable                       |           |      |                                                                 |         |       |
| r2m.stream-analysis.config.spectrum.rule.engine.gap.interval                     |           |      |                                                                 |         |       |
| r2m.stream-analysis.config.spectrum.rule.engine.gap.maxTime                      |           |      |                                                                 |         |       |
| r2m.stream-analysis.config.spectrum.rule.engine.keep.active.rule.enabled         |           |      |                                                                 |         |       |
| r2m.stream-analysis.config.spectrum.rule.engine.maxNbClassloader                 |           |      |                                                                 |         |       |
| r2m.stream-analysis.config.spectrum.rule.engine.reset.rule                       |           |      |                                                                 |         |       |
| r2m.{fleet}.stream-analysis.config.spectrum.rule.engine.faults.retracted.startup |           |      |                                                                 |         |       |
| r2m.{fleet}.stream-analysis.config.spectrum.rule.engine.session.expire           |           |      |                                                                 |         |       |
| r2m.{fleet}.stream-analysis.dataSp                                               |           |      |                                                                 |         |       |
| r2m.{fleet}.stream-analysis.dataTesterSp                                         |           |      |                                                                 |         |       |
| r2m.{fleet}.stream-analysis.eventDataLatestSinceSp                               |           |      |                                                                 |         |       |
| r2m.{fleet}.stream-analysis.eventDataSp                                          |           |      |                                                                 |         |       |
| r2m.{fleet}.stream-analysis.eventDataTesterLatestSinceSp                         |           |      |                                                                 |         |       |
| r2m.{fleet}.stream-analysis.eventDataTesterSp                                    |           |      |                                                                 |         |       |
| r2m.{fleet}.stream-analysis.extra-channels                                       |           |      |                                                                 |         |       |
| r2m.{fleet}.stream-analysis.extra-channels.name                                  |           |      |                                                                 |         |       |
| r2m.{fleet}.stream-analysis.extra-channels.type                                  |           |      |                                                                 |         |       |
| r2m.{fleet}.stream-analysis.keyChannel                                           |           |      |                                                                 |         |       |
| r2m.{fleet}.stream-analysis.maxEventChannelTimeDifferenceMinutes                 |           |      |                                                                 |         |       |
| r2m.{fleet}.stream-analysis.maxJoinDistance                                      |           |      | The max distance between 2 records in milliseconds to join them |         |       |
| r2m.{fleet}.stream-analysis.ruleEngineDelay                                      |           |      | The no. of seconds to delay processing each record by           |         |       |

## Global/System
| Name                                      | Component     | Type    | Description                                         | Example            | Image |
|-------------------------------------------|---------------|---------|-----------------------------------------------------|--------------------|-------|
| r2m.globalScreenRefreshRateInMilliseconds |               |         |                                                     |                    |       |
| r2m.branding                              | top left logo | String  | The path to the logo image                          | img/nexala-r2m.png |       |
| r2m.brandingStyle                         | top left logo | String  | The css style to apply to the logo                  | padding-top:5px    |       |
| r2m.footer                                | footer text   | String  | The footer text displayed at the bottom             |                    |       |
| r2m.fleetFormationCacheInterval           |               | Integer | How long the fleet formation cache is valid (in ms) |                    |       |
| r2m.fleetFormationSeparator               |               | String  | The separator used on the fleetformation table      |                    |       |


## Database Queries
| Name         | Component | Type   | Description                         | Example                                    | Image |
| ------------ | --------- | ------ | ----------------------------------- | ------------------------------------------ | ----- |
| r2m.queries  | Database  | Config | Configuration containing db queries | queries: { getUnits: "", getVehicles: "" } |       ||

## Dropdowns
| Name                    | Component | Type   | Description                               | Example                                         | Image |
| ----------------------- | --------- | ------ | ----------------------------------------- | ----------------------------------------------- | ----- |
| r2m.dropdowns           | Dropdowns | Config | Configuration containing R2M dropdowns    | dropdowns: {unitOfMeasure:[], systemCode:[]}    |       |
| r2m.dropdowns.xxx       | Dropdowns | List   | List of options for the dropdwown xxx     | xxx: [{value:"01", label:"Option 1"}]           |       |
| r2m.dropdowns.xxx.value | Dropdowns | String | Code of an option for the dropdown xxx    | value:"01"                                      |       |
| r2m.dropdowns.xxx.label | Dropdowns | String | Label of an option for the dropdown xxx   | label:"Option 1"                                |       ||

## Unit Search
| Name                    				| Component | Type    | Description                               				| Example                                         									| Image |
| ----------------------- 				| --------- | ------  | ----------------------------------------- 				| ----------------------------------------------- 									| ----- |
| r2m.unitSearch           				|  			| Config  | Configuration for unit search button    				|     																				|       |
| r2m.unitSearch.headers       			|  			| Array   | List of default headers for unit search     			| headers: [{id: "id", db: "id", type: "String", displayName: "ID", visible: false}]|		|
| r2m.unitSearch.headers.name 			|  			| String  | Property name/id    									| id: "unitnumber"                                     								|		|
| r2m.unitSearch.headers.db 			|  			| String  | Database column for property, as called in the query    | db: "UnitNumber"                               									|		|
| r2m.unitSearch.headers.type 			|  			| String  | Data type of property    								| type: "String"               														|		|
| r2m.unitSearch.headers.displayName 	|  			| String  | Header name displayed in unit search results    		|  displayName: "Unit Number"                										|		|
| r2m.unitSearch.headers.visible 		|  			| Boolean | If true, header is visible on, and used for, unit search|  visible: true                                  									|		|
| r2m.unitSearch.maximumResults 		|  			| Integer | Max number of results shown in unit search    			| maximumResults: 15                                     							|       |
| r2m.unitSearch.panelWidth 		|  			| Integer | Width of the text box for the search. Determines the width of the records too. Default 250   			| panelWidth: 300                                     							|       |


