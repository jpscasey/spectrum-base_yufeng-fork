<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%
    // Hack to get around bug in tomcat:
    // https://issues.apache.org/bugzilla/show_bug.cgi?id=3839
//    if (session.isNew()) { // if a user has bookmarker /j_security_check, this redirects them to the context root
//        response.sendRedirect(response.encodeRedirectURL(request.getContextPath()));
//        return;
//    }

    response.setHeader("Cache-Control", "no-cache, must-revalidate");
    response.setHeader("Pragma", "no-cache");
%>

<html>
    <head>
        <title>Nexala R2M Login Error</title>
        
        <link rel="stylesheet" type="text/css" href="/css/nexala.login.css" />
    </head>

    <body class="loginpage">
        <div class="loginbox">
            <div class="loginform">
                <div class="logo"></div>
                
                <form name="spectrumLoginForm" method="POST" action="j_security_check">
                    <fieldset>
                        <label for="username">Username:</label>
                        <input type="text" name="j_username" id="username" class="logintext" value="<%=request.getParameter("j_username")==null?"":request.getParameter("j_username")%>" />
                    </fieldset>
                    <fieldset>
                        <label for="password">Password:</label>
                        <input type="password" name="j_password" id="password" class="logintext" />
                    </fieldset>
                    <fieldset>
                    	<div class="link">
                        	<a href="/lostPassword">forgot your password?</a>
                        </div>
                        <input type="submit" class="loginButton" name="login" value="Login">
                    </fieldset>
                </form>
                <span style='color:red; font-size: 12px'>Invalid Username or Password. Please try again.</span>
            </div>
        </div>
        
        <script type="text/javascript" language="JavaScript">
            document.spectrumLoginForm.j_username.focus();
        </script>
    </body>
</html>
