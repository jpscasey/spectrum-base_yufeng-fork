<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%
    response.setHeader("Cache-Control", "no-cache, must-revalidate");
    response.setHeader("Pragma", "no-cache");
%>

<html>
    <head>
        <title>Nexala R2M Login Unauthorised Access</title>
        
        <link rel="stylesheet" type="text/css" href="/css/nexala.login.css" />
    </head>

    <body class="loginpage">
        <div class="loginbox">
            <div class="loginform">
                <p>User <%=request.getUserPrincipal().getName()%> is not authorised to use this application.</p>
                <p>
                    <a href="logout.jsp">Click here to logout and login as a different user.</a>
                </p>
            </div>
        </div>
    </body>
</html>
