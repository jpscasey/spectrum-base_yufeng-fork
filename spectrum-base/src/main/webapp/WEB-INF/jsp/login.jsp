<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%
    response.setHeader("Cache-Control", "no-cache, must-revalidate");
    response.setHeader("Pragma", "no-cache");
%>

<html>
    <head>
        <title>Nexala R2M Login</title>
        <meta http-equiv="X-UA-Compatible" content="IE=8" >
        
        <link rel="stylesheet" type="text/css" href="/css/nexala.login.css" />
    </head>
    
    <body class="loginpage">
        <div class="loginbox">
            <div class="loginform">
                <div class="logo"></div>
            
                <form name="spectrumLoginForm" method="POST" action="j_security_check">
                    <fieldset>
                        <label for="username">Username:</label>
                        <input type="text" name="j_username" id="username" class="logintext" value="" />
                    </fieldset>
                    <fieldset>
                        <label for="password">Password:</label>
                        <input type="password" name="j_password" id="password" class="logintext" value="" />
                    </fieldset>
                    <fieldset>
                    	<div class="link">
                        	<a href="/lostPassword">Forgot your password?</a>
                        </div>
                        <input type="submit" class="loginButton" name="login" value="Login">
                    </fieldset>
                </form>
            </div>
        </div>
        
        <script type="text/javascript" language="JavaScript">
            document.spectrumLoginForm.j_username.focus();
        </script>
    </body>
</html>