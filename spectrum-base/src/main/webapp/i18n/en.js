var translationEn = {
	"Unit search (Ctrl+S)" : "Unit search (Ctrl+S)",
	"Profile": "Profile",
	"Manage users": "Manage users",
	"Logout": "Logout",
	"Fleet Summary": "Fleet Summary",
	"Unit Summary": "Unit Summary",
	"Fleet Location": "Fleet Location",
	"Event History": "Event History",
	"Event Analysis": "Event Analysis",
	"Rules Editor": "Rules Editor",
	"Channel Definition": "Channel Definition",
	"Administration": "Administration",
	"Data View": "Data View",
	"Unit No": "Unit No",
	"$1 {{PLURAL:$1|year|years}} ago": "$1 {{PLURAL:$1|year|years}} ago",
	"$1 {{PLURAL:$1|month|months}} ago": "$1 {{PLURAL:$1|month|months}} ago",
	"$1 {{PLURAL:$1|week|weeks}} ago": "$1 {{PLURAL:$1|week|weeks}} ago",
	"$1 {{PLURAL:$1|day|days}} ago": "$1 {{PLURAL:$1|day|days}} ago",
	"$1 {{PLURAL:$1|hour|hours}} ago": "$1 {{PLURAL:$1|hour|hours}} ago",
	"$1 {{PLURAL:$1|minute|minutes}} ago": "$1 {{PLURAL:$1|minute|minutes}} ago",
	"$1 {{PLURAL:$1|second|seconds}} ago": "$1 {{PLURAL:$1|second|seconds}} ago",
	"Unit": "Unit",
	"Vehicle": "Vehicle",
	"Time": "Time",
	"Location": "Location",
	"Description": "Description",
	"Type": "Type",
	"Channel Type": "Type",
	"Category": "Category",
	"LIVE EVENTS": "LIVE EVENTS",
	"RECENT EVENTS": "RECENT EVENTS",
	"ACKNOWLEDGED": "ACKNOWLEDGED",
	"NOT ACKNOWLEDGED": "NOT ACKNOWLEDGED",
	"Fleet": "Fleet",
	"Priority": "Priority",
	"Clear": "Clear",
	"Apply": "Apply",
	"Filters": "Filters",
	"Service Info": "Service Info",
	"Unit 1": "Unit 1",
	"Unit 2": "Unit 2",
	"Unit 3": "Unit 3",
	"Last Update Time": "Last Update Time",
	"GPS Coordinates": "GPS Coordinates",
	"Speed": "Speed",
	"Event": "Event",
	"Send to E2M": "Send to E2M",
	"&lt; Prev": "&lt; Prev",
	"Next &gt;": "Next &gt;",
	"Error - Unable to send fault": "Error - Unable to send fault",
	"Event Detail": "Event Detail",
	"Event Location": "Event Location",
	"Date / Time": "Date / Time",
	"End Time": "End Time",
	"Latitude": "Latitude",
	"Longitude": "Longitude",
	"Event Code": "Event Code",
	"Event Description": "Event Description",
	"Event Category": "Event Category",
	"Position Code": "Position Code",
	"Severity": "Severity",
	"URL": "URL",
	"Acknowledged": "Acknowledged",
	"Not Acknowledged": "Not Acknowledged",
	"Has Recovery": "Has Recovery",
	"Event Summary": "Event Summary",
	"Additional Info": "Additional Info",
	"Comment History": "Comment History",
	"Comment Entry": "Comment Entry",
	"Navigation": "Navigation",
	"Action": "Action",
	"Info": "Info",
	"Error": "Error",
	"Event status saved": "Event status saved",
	"Error updating event": "Error updating event",
	"Submit Comment": "Submit Comment",
	"Acknowledge": "Acknowledge",
	"Please enter a comment first.": "Please enter a comment first.",
	"Can't save the change, another user did changes to this event.": "Can't save the change, another user did changes to this event.",
	"Can't send the request, another user did changes to this event.": "Can't send the request, another user did changes to this event.",
	"Deactivate": "Deactivate",
	"Train Number": "Train Number",
	"Operations": "Operations",
	"Traction": "Traction",
	"Brakes": "Brakes",
	"Doors": "Doors",
	"Safety": "Safety",
	"HVAC": "HVAC",
    "Communication": "Communication",
    "Toilets": "Toilets",
    "High Voltage": "High Voltage",
    "Low Voltage": "Low Voltage",
    "Air Supply": "Air Supply",
	"Other": "Other",
    "Last Updated Time": "Last Updated Time",
    "Diagram": "Diagram",
    "Formation": "Formation",
    "Leading Vehicle": "Leading Vehicle",
    "Unit Detail": "Unit Detail",
	"Channel Data": "Channel Data",
	"Data view": "Data view",
	"Cab": "Cab",
	"Schematic": "Schematic",
	"Events": "Events",
	"Counters": "Counters",
	"TimeStamp": "TimeStamp",
	"timestamp": "TimeStamp",
	"Diagram": "Diagram",
	"Unit Number": "Unit Number",
	"LeadingVehicle": "LeadingVehicle",
	"Faults": "Faults",
	"Warnings": "Warnings",
	"Code": "Code",
	"Date": "Date",
	"Not Acknowledged Events": "Not Acknowledged Events",
	"Acknowledged Events": "Acknowledged Events",
	"All Except Test": "All Except Test",
	"All": "All",
	"Hide Additional": "Hide Additional",
	"Show Additional": "Show Additional",
	"Chart Name": "Chart Name",
	"This configuration will be permanently deleted. Are you sure?": "This configuration will be permanently deleted. Are you sure?",
	"Please enter chart name": "Please enter chart name",
	"System charts may not be modified.": "System charts may not be modified.",
	"Save/Rename": "Save/Rename",
	"Delete": "Delete",
	"Error": "Error",
	"Delete": "Delete",
	"Rename": "Rename",
	"Save As": "Save As",
	"Yes": "Yes",
	"No": "No",
	"groupLead": "groupLead",
	"grouped": "grouped",
	"Save": "Save",
	"Cancel": "Cancel",
	"Show Table": "Show Table",
	"Show Charts": "Show Charts",
	"Refresh Charts": "Refresh Charts",
	"Export Chart Data to PDF": "Export Chart Data to PDF",
	"Export Chart Data to CSV": "Export Chart Data to CSV",
	"{{PLURAL:$1|year|years}}": "{{PLURAL:$1|year|years}}",
	"{{PLURAL:$1|month|months}}": "{{PLURAL:$1|month|months}}",
	"{{PLURAL:$1|week|weeks}}": "{{PLURAL:$1|week|weeks}}",
	"{{PLURAL:$1|day|days}}": "{{PLURAL:$1|day|days}}",
	"{{PLURAL:$1|hour|hours}}": "{{PLURAL:$1|hour|hours}}",
	"{{PLURAL:$1|minute|minutes}}": "{{PLURAL:$1|minute|minutes}}",
	"{{PLURAL:$1|second|seconds}}": "{{PLURAL:$1|second|seconds}}",
	"PAUSE": "PAUSE",
	"PLAY": "PLAY",
	"LIVE": "LIVE",
	"Overview": "Overview",
	"Coordinates": "Coordinates",
	"Live": "Live",
	"Recent": "Recent",
	"Last Updated Time": "Last Updated Time",
	"Formation": "Formation",
	"Leading Vehicle": "Leading Vehicle",
	"GPS": "GPS",
	"Details": "Details",
	"Select a station": "Select a station",
	"Settings": "Settings",
	"Show Trains": "Show Trains",
	"Show Train Labels": "Show Train Labels",
	"Show Detectors": "Show Detectors",
	"Aerial": "Aerial",
	"Show": "Show",
	"Show Tracks": "Show Tracks",
	"Show Stations": "Show Stations",
	"Fault Code": "Fault Code",
	"Summary": "Summary",
	"Current": "Current",
	"Reporting Only": "Reporting Only",
	"Advanced Export": "Advanced Export",
	"Advanced Export Events to CSV": "Advanced Export Events to CSV",
	"Export Events to CSV": "Export Events to CSV",
	"Show Advanced Options": "Show Advanced Options",
	"Expand": "Expand",
	"Hide Advanced Options": "Hide Advanced Options",
	"Show predefined filters": "Show predefined filters",
	"Predefined Filters": "Predefined Filters",
	"Search Events": "Search Events",
	"Search": "Search",
	"Live w/ Recovery": "Live w/ Recovery",
	"SR Last 8 Hours": "SR Last 8 Hours",
	"SR Created By Rule Last 8 Hours": "SR Created By Rule Last 8 Hours",
	"Live Ack Without SR Last 8 hours": "Live Ack Without SR Last 8 Hours",
	"Live w/ Maximo": "Live w/ Maximo",
	"Has Maximo SR": "Has Maximo SR",
	"Export": "Export",
	"Vehicle Number": "Vehicle Number",
	"From": "From",
	"To": "To",
	"Not live": "Not live",
	"Keyword": "Keyword",
	"Hide Filters": "Hide Filters",
	"Show Filters": "Show Filters",
	"Collapse": "Collapse",
	"Notification": "Notification",
	"The search returned no results.": "The search returned no results.",
	"The export returned no results.": "The export returned no results.",
	"Period": "Period",
	"Any": "Any",
	"Count": "Count",
	"Total Count": "Total Count",
	"Total Duration": "Total Duration",
	"Average Duration": "Average Duration",
	"Fault Category": "Fault Category",
	"Fault Code/Desc": "Fault Code/Desc",
	"Fault Type": "Fault Type",
	"Create Time": "Create Time",
	"Headcode": "Headcode",
	"Event Data": "Event Data",
	"ID": "ID",
	"Name": "Name",
	"Group": "Group",
	"Reference": "Reference",
	"Unit of Measure": "Unit of Measure",
	"Display Order": "Display Order",
	"Always Displayed": "Always Displayed",
	"Rules Status": "Rules Status",
	"Add rule": "Add rule",
	"Remove rule": "Remove rule",
	"Channel Definition Details": "Channel Definition Details",
	"Rules": "Rules",
	"Status": "Status",
	"Active": "Active",
	"Comment": "Comment",
	"CommentSave": "Comment",
	"Validations": "Validations",
	"Add validation": "Add validation",
	"Remove validation": "Remove validation",
	"type": "Type",
	"date": "Date",
	"endDate": "End Date",
	"Event Group": "Event Group",
	"unitNumber": "Unit Number",
	"vehicleNumber" : "Vehicle Number",
	"headcode": "Headcode",
	"vehicle": "Vehicle",
	"location": "Location",
	"latitude": "Latitude",
	"longitude": "Longitude",
	"faultCode": "Fault Code",
	"speed" : "Speed",
	"category": "Category",
	"hasRecovery": "Has Recovery",
	"description": "Description",
	"summary": "Summary",
	"additionalInfo": "Additional Info",
	"current": "Current",
	"reportingOnly": "Reporting Only",
	"acknowledged the event": "acknowledged the event",
	"deactivated the event": "deactivated the event",
	"issued a service request": "issued a service request",
	"commented": "commented",
	"Show Bridges": "Show Bridges",
	"setCode" : "Set Code",
	"Show Event History": "Show Event History",
	"RTM-O only": "RTM-O only",
	"Exclude closed WO/SR": "Exclude closed WO/SR",
	"isAcknowledged": "Is Acknowledged",
	"Active Cab": "Active Cab",
	"Show WSP": "Show WSP",
	"Show Sanding": "Show Sanding",
	"Live Events": "Live Events",
	"Recent Events": "Recent Events",
	"You do not have permission to access this.": "You do not have permission to access this.",
	"Rule Name": "Rule Name",
	"Date Occurred": "Date Occurred",
	"Show time controller": "Show time controller",
	"endTime": "End Time",
	"recoveryStatus": "Recovery Status",
	"Two Events needed to create Event Group.": "Two Events needed to create Event Group.",
	"No lead event selected. Please select an event to flag as the lead of this group.": "No lead event selected. Please select an event to flag as the lead of this group.",
	"Create": "Create",
	"Cancel": "Cancel",
	"Event Group was created.": "Event Group was created."
	
		
};