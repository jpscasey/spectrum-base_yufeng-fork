/**
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 * @author Paul O'Riordan
 */
(function( $ ) {
    $.fn.fadeTo = function(imgSrc, settings) {
        
        var settings = $.extend({
            fadeOutTime: 1000,
            fadeInTime: 1000
        }, settings);
        
        // The image needs to be wrapped in a relatively
        // positioned div. This frame will be created
        // if it does not exist.
        var frame = $(this).parent('div.frame');

        if (frame.length < 1) { 
            frame = $(this).wrap(
                $('<div>').
                    addClass('frame')
                    .css({
                        position: 'relative',
                        display: 'inline-block'
                    })
            ).parent();
            
            $(this).css('float', 'left');
        }
        
        // In order to transition cleanly to the new image,
        // we create a 'background' image, whose src will be
        // set to the current image's src. We then hide the
        // actual image, change its src to the newSrc;
        // fadeIn the actual image and fadeOut the background
        var bg = frame.children('img.bg');
        
        if (bg.length < 1) {
            bg = $('<img/>').css({
                position: 'absolute',
                top: 0,
                left: 0
            })
            .addClass('bg');
            frame.append(bg);
        }

        var img = $(this);
        var stime = new Date().getTime();
        
        bg.stop();
        img.stop().css('opacity', 1);
        
        /*
        bg.one('load', function() {
            bg.css('opacity', 1);
            img.css('opacity', 0);
            img.one('load', function() {
                var etime = new Date().getTime();
                img.animate({opacity: 1}, settings.fadeOutTime);
                bg.animate({opacity: 0}, settings.fadeInTime);
            }).attr('src', imgSrc);
        }).attr('src', img.attr('src'));*/
        
        
        if (bg.attr('src') != img.attr('src')) {
            bg.one('load', function() {
                bg.css('opacity', 1);
                img.css('opacity', 0);
                img.one('load', function() {
                    var etime = new Date().getTime();
                    img.animate({opacity: 1}, settings.fadeOutTime);
                    bg.animate({opacity: 0}, settings.fadeInTime);
                })
                img.attr('src', imgSrc);
            });
            
            bg.attr('src', img.attr('src'));
        } else {
            img.one('load', function() {
                var etime = new Date().getTime();
                img.animate({opacity: 1}, settings.fadeOutTime);
                bg.animate({opacity: 0}, settings.fadeInTime);
            })
            img.attr('src', imgSrc);
        }
    };
})(jQuery);