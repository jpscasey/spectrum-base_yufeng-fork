var nx = nx || {};
nx.unit = nx.unit || {};

nx.unit.DataPlots = (function() {
    
    function ctor(us) {
        nx.unit.TimeAware.call(this, us);
        
        var me = this;
        this.configIdx = 0;
        this.__fleetId = us.getFleetId(); // the selected fleet
        this.__endOfPage = false;
        this.__topOfPage = false;
        this.__selectedConfig = null; // the selected configuration
        this.__chartConfigs = new Array(); // the different configuration
        this.__maxNumberRows = 3000;
        this.__showValuesTable = false; //flag when the table is shown
        this.__previousStatePaused = us.getToggleButton().isPaused(); //flag previous state of timer
        this.__showValuesButton = null; //Button to access to the table
        this.__hideValuesButton = null; //Button to access to the chart
        this.__toggleButton = $('#playPauseButton');
        this.__numberOfRows = 25; //number of rows per pagination
        this.__lastFirtRow = 0;
        this.__scrollerTimer; //Function to timeOut the scroll event
        this.__timestampTimer; //Function to timeOut the time popup
        this.__valuesGrid; //Table of values
        this.__dv = $('#dvMain');
        this.__timePositionDiv;
        this.__controllerDiv = null; //Div where the controller tool is placed
        this.__controllerWidth = 265;
        this.__accordion = this.__dv.find('#dvChannelAccordion');
        this.__configurations = this.__dv.find('#dvConfigurations div select');
        this.__dvSaveButton = this.__dv.find('#dvSaveButton').button({
            icons: {
                primary: "ui-icon-disk"
            },
            text: false
        });
        //this.__dvRenameButton = this.__dv.find('#dvRenameButton').button();
        this.__dvDeleteButton = this.__dv.find('#dvDeleteButton').button({
            icons: {
                primary: "ui-icon-closethick"
            },
            text: false
        });
        this.__renameField = this.__dv.find('#renameField');
        this.__saveField = this.__dv.find('#saveField');
        this.__selectedChannels = [];
        this.__stockSub = null;
        this.__lastChartWidth = null;
        
        
        // Set the titles
        $('#dvSaveButton').attr('title', $.i18n('Save/Rename'));
        $('#dvDeleteButton').attr('title', $.i18n('Delete'));
        $('#system-warning').attr('title', $.i18n('Error'));
        $('#emptyfield-warning').attr('title', $.i18n('Error'));
        $('#config-delete').attr('title', $.i18n('Delete'));
        $('#rename-config').attr('title', $.i18n('Rename'));
        $('#save-config').attr('title', $.i18n('Save As'));
        
        nx.rest.chart.configuration(function(result){
        	me._initPeriods(result.defaultPeriod, result.periods, result.refreshPeriod);
        	
        	me.__exportPDF = result.exportPDF;
        	
            me.__isVirtualScrollBar = result.virtualScrollBar;
            
            me.__isSystemChartsIncluded = result.isSystemChartsIncluded;
        	
        	me.__refreshNotLive = result.refreshNotLive;
        	
        	me.__chartEnabledByFleet = result.chartEnabledByFleet;
        	
            me.__addRangeSelector();
            
            me.__addClickHandlers();
            
            me.__subscribeToEvents();
            
            me.__channelGroupLabelConfig = result.channelGroupLabelConfig;
            
            me.__tableChannelLabelConfig = result.tableChannelLabelConfig;
            
            me._initAccordion();
        });       
        
        // TODO - should add resize method to unit tabs.
        $(window).resize(function() {
            me.__accordion.accordion('resize');
            if (me.__isVirtualScrollBar) {
                me.__refreshCharts();
                
                var endRow = Math.round(($('.dvTable').height() - 160) / 21);
                me.__numberOfRows = endRow;

                me.__refreshTable();
            }       
        });
        
        this.stockTabs = new nx.unit.StockTabs(
                this.__dv.find('.vehicleTabs'),
                true,
                false);
        
        this.__configurations.change(function(evt) {
            var configId = me.__configurations.val();
            
            if (!!configId) {
                // Update the selected channels..
                me.__setChartConfiguration(configId, function() {
                    me.__refreshCharts();
                    me.__redrawTable();
                });
            }
            
            if (me.__isVirtualScrollBar) {
                for(var i = 0; i < me.__chartConfigs.length; i++) {
                    if (me.__chartConfigs[i].id ==configId) {
                        var pane = $(me.stockTabs.getSelectedTabEl());
                        me.__selectConfig(me.__chartConfigs[i], $(pane.find('#scroller table tbody tr td')[i]), i, false);
                    }
                }
            }
            
        });
        
    };
    
    /** Extend nx.unit.Tab */
    ctor.prototype = new nx.unit.Tab();

    /** Extend nx.unit.TimeAware */
    ctor.prototype = new nx.unit.TimeAware();
    

    ctor.prototype._onHide = function() {
        this.__unitsummary.getTimePicker().setCustomAmount(null);
        if (this.__stockSub != null) {
            this.__stockSub.unsubscribe();
            this.__stockSub = null;
        }
        //show again the Play / Pause button
        this.__toggleButton.show();
    };
    
    
    /**
     *  Initialize this.__period with the period selected by default
     *  Initialize this.__periodValues with a table of periods 
     *  that can be selected
     *  times are in milliseconds.
     */
    ctor.prototype._initPeriods = function(firstPeriodPosition, periodValues, refreshPeriod) {
        this.__firstPeriodPosition = firstPeriodPosition;
        this.__periodValues = periodValues;
        this.__period = periodValues[firstPeriodPosition];
        this.__refreshPeriod = refreshPeriod;
    };
    
    ctor.prototype._initAccordion = function() {
        var me = this;
        this.__accordion.accordion({
            autoHeight: false,
            animated: false, // workaround to an IE8 bug
            
            /**
             * On change, if the group has not been populated. Query
             * its channels and add them to the group
             */
            change: function(event, ui) {
                var panel = ui.newContent;
                var groupId = ui.newHeader.find('a').attr('href');
                groupId = groupId.replace(/#/, '');
    
                if (!me.__isChannelGroupPopulated(panel)) {
                    nx.rest.group.channels(me.__fleetId, groupId, function(channels) {
                        me.__addChannelsToGroup(panel, channels);
                        
                    });
                }
            }
        });
        
    };
    
    /**
     * Return the amount of time to add or remove when going forward/reverse
     */
    ctor.prototype._timePickerAmountOfTime = function() {
        var me = this;
        this.__unitsummary.getTimePicker().setCustomAmount(function() {
            return (me.__period/2);
        });
        
    };
    
    ctor.prototype._onShow = function() {
        var me = this;
        
        // Change the default behavior of fast forward/rewind buttons
        this._timePickerAmountOfTime();
        
        this.__stockSub = nx.hub.stockChanged.subscribe(function(evt, stockId, objEl) {
            me.__stockChanged(stockId, objEl);
        });
        
        
        
        this.__accordion.accordion('resize');
        //if the table is shown hide the Play / Pause button
        if (this.__showValuesTable) {
            this.__toggleButton.hide();
        }
    };
    
    ctor.prototype.__reloadChartConfigurations = function(id) {
        var me = this;
        
        if (!!id) {
            this.__loadChartConfigurations(function() {
                me.__setChartConfiguration(id, function() {
                    me.__enableDeleteButton();
                    me.__refreshCharts();
                });
            });
        } else {
            // Select the first item.
            this.__loadChartConfigurations(function() {
                me.__selectFirstConfiguration();
            });
        }
    };
    
    ctor.prototype.__loadChartConfigurations = function(callback) {
        var me = this;
        
        if (me.__fleetId != null){
            // Get the system chart configurations
            nx.rest.chart.system(me.__fleetId, function(confs) {
                var sysconfs = confs; 

                // Get the user configurations.
                nx.rest.chart.user(me.__fleetId, function(userConfs) {
                    
                    me.__configurations.empty();
                    me.__chartConfigs = new Array();
                    
                    if (me.__isSystemChartsIncluded == true) {
                    	me.__appendConfigurations(sysconfs, true);
                    }
                    me.__appendConfigurations(userConfs, false);
                    
                    $.uniform.update(me.__configurations);
                    
                    if (!!callback) {
                        callback();
                    }
                });
            });
        };
    };
    
    ctor.prototype.__addClickHandlers = function() {
        var me = this;
        
        var yes = $.i18n('Yes');
        var no = $.i18n('No');
        var cancel = $.i18n('Cancel');
        var save = $.i18n('Save');
        var rename = $.i18n('Rename');
        
        this.__dvDeleteButton.click(function() {
            var option = me.__getSelectedChartOption();
            var chartId = option.val();
            
            if (option.data('system')) {
                me.__systemModificationWarning();
            } else {
            	
            	var deleteButtons = {};

                deleteButtons[no] = function() {
                    $(this).dialog('close');
                };

                deleteButtons[yes] = function() {
                    me.__delete(chartId);
                    $(this).dialog('close');
                };
            	
                $("#config-delete").dialog({
                    resizable : false,
                    modal : true,
                    buttons : deleteButtons
                });
            }
        });

        this.__dvSaveButton.click(function() {
            var option = me.__getSelectedChartOption();
            var chartId = option.val();
            var name = option.text();
            
            me.__saveField.val(name);

            saveChartFields = null;

            if (option.data('system')) {
                saveChartFields = function() {
                    if (name != me.__saveField.val()) {

                        if (me.__saveField.val().trim() == "") {
                            me.__emptyFieldWarning();
                        } else {
                            me.__save(me.__fleetId, chartId, me.__saveField.val());
                            saveChartFields = null;
                            $("#save-config").dialog('close');
                        }
                    } else {
                        me.__systemModificationWarning();
                    }
                }

                var saveButtons = [
                	{
                		text: save,
                		id: "dialogSave",
                		click: saveChartFields
                	},
                	{
                		text: cancel,
                		id: "dialogCancel",
                		click: function() {
                            $(this).dialog('close');
                        }
                    }
                ];
                
                $("#save-config").dialog({
                    resizable : false,
                    modal : true,
                    buttons : saveButtons,
                    close : function() {
                        me.__saveField.val('');
                    }
                });
                
                $('#dialogSave').addClass('ui-button-primary');
                $('#dialogCancel').addClass('ui-button-secondary');
                
            } else {
                saveChartFields = function() {
                    if (me.__saveField.val().trim() == "") {
                        me.__emptyFieldWarning();
                    } else {
                        me.__save(me.__fleetId, chartId, me.__saveField.val());
                        saveChartFields = null;
                        $("#save-config").dialog('close');
                    }
                }
            	
                var saveButtonsNotSystem = [
                	{
                		text: save,
                		id: "dialogSave",
                		click: saveChartFields
                	},
                	{
                		text: rename,
                		id: "dialogRename",
                		click: function() {
                            if (me.__saveField.val().trim() == "") {
                                me.__emptyFieldWarning();
                            } else {
                                me.__rename(me.__fleetId, chartId, me.__saveField.val());
                                $("#save-config").dialog('close');
                            }
                        }
                    },
                    {
                        text: cancel,
                        id: "dialogCancel",
                        click: function() {
                            $(this).dialog('close');
                        }
                    }
                ];
                
                $("#save-config").dialog({
                    resizable : false,
                    modal : true,
                    buttons : saveButtonsNotSystem,
                    close : function() {
                        me.__saveField.val('');
                    }
                });
                
                $('#dialogSave').addClass('ui-button-primary');
                $('#dialogRename').addClass('ui-button-secondary');
                $('#dialogCancel').addClass('ui-button-secondary');
            }

            $('#saveFieldForm').on('keyup keypress', function(e) {
                var keyCode = e.keyCode || e.which;

                // if enter is pressed after entering the name of the chart
                // then save the chart without submitting the form and
                // reloading the page
                if (keyCode === 13 && !!saveChartFields) {
                    saveChartFields();

                    e.preventDefault();
                    return false;
                }
            });
        });
        var chartDiv = $('#chartPane');
        
        var exportCSVPosition;
        if(this.__chartEnabledByFleet[this.__fleetId] || typeof this.__chartEnabledByFleet[this.__fleetId] == "undefined") {
        	exportCSVPosition = 236;
        } else {
        	exportCSVPosition = 207;
        }
        
        // Export Button
        var exportButton = $("<button></button>").button({
            icons : {
                primary: 'ui-icon-arrowthickstop-1-s'
            },
            text: false
        }).click(function(e) {
            me.__download();
        }).css({'left': exportCSVPosition + 'px'})
        .attr('title',$.i18n('Export Chart Data to CSV'));
        
        // Export PDF Button
        var exportPdfButton = $("<button></button>").click(function(e) {
            me.__downloadPdf();
        }).css({'left': exportCSVPosition + 29 + 'px'})
        .attr('title',$.i18n('Export Chart Data to PDF'))
        .attr('type','button')
        .addClass('exportPdf');
        
        // Values table Button
        me.__showValuesButton = $("<button></button>").button({
            icons : {
                primary: 'ui-icon-calculator'
            },
            text: false 
        }).click(function(e) {
            me.__showValues();
            me.__redrawTable();
        }).css({'left': '207px'})
        .attr('title', $.i18n('Show Table'));
        
        me.__hideValuesButton = $('<button></button>').button({
            icons : {
                primary: 'ui-icon-image'
            },
            text: false
        }).click(function(e) {
            me.__hideValues(false);
            me.__refreshCharts();
        }).css({'left': '207px'})
        .attr('title', $.i18n('Show Charts'));
        
        // Refresh Button
        me.__reloadButton = $('<button></button>').button({
            icons : {
                primary: 'ui-icon-refresh'
            },
            label: 'Refresh'
        }).click(function(e) {
        	me.__refreshCharts();
        }).css({
        	'left': this.__controllerWidth - 95 + 'px',
        	'width': '90px',
        	'line-height': '13px'})
        .attr('title',$.i18n('Refresh Charts'));
        
        
        //Add the buttons to the cotroller tool box
        if (me.__chartEnabledByFleet[me.__fleetId] || typeof this.__chartEnabledByFleet[this.__fleetId] == "undefined") {
            me.__controllerDiv.append(me.__showValuesButton);
            me.__controllerDiv.append(me.__hideValuesButton);
            me.__hideValuesButton.hide();
            
            me.__hideValues(false);
            me.__refreshCharts();
        } else {
        	me.__showValues();
        	me.__redrawTable();
        }
        me.__controllerDiv.append(exportButton);
        me.__controllerDiv.append(me.__reloadButton);
        me.__reloadButton.hide();

        if (this.__exportPDF) {
        	this.__controllerDiv.append(exportPdfButton);
        }
    };
    
    ctor.prototype.__delete = function(chartId) {
        var me = this;
        
        nx.rest.chart.remove(me.__fleetId, chartId, function() {
           // select first configuration.
            me.__reloadChartConfigurations();
        });
    };
    
    ctor.prototype.__save = function(fleetId, chartId, name) {
        var me = this;
        var channels = this.__selectedChannels;
        var oldChartId = chartId;
        
        nx.rest.chart.save(fleetId, chartId, name, channels, function(newConf) {
            me.__reloadChartConfigurations(!!newConf ? newConf.id : oldChartId);
        });
    };
    
    ctor.prototype.__rename = function(fleetId, chartId, newName) {
        var me = this;
        
        nx.rest.chart.rename(fleetId, chartId, newName, function(newConf) {
            me.__reloadChartConfigurations(chartId);
        });
    };
    
    ctor.prototype.__systemModificationWarning = function() {
        $("#system-warning").dialog({
            resizable : false,
            modal : true,
            buttons : {
                'Ok' : function() {
                    $(this).dialog('close');
                }
            }
        });
    };

    ctor.prototype.__emptyFieldWarning = function() {
        $("#emptyfield-warning").dialog({
            resizable : false,
            modal : true,
            buttons : {
                'Ok' : function() {
                    $(this).dialog('close');
                }
            }
        });
    };
    
    ctor.prototype.__getSelectedChartOption = function() {
        return this.__configurations.children('option:selected');
    };
    
    ctor.prototype.__appendConfigurations = function(confs, system) {
        for (var i = 0, l = confs.length; i < l; i++) {
            var conf = confs[i];
            
            var option = $('<option/>')
                .val(conf.id)
                .text(conf.name);
            
            if (system) {
                option.addClass('system');
                this.__chartConfigs.push(confs[i]);
            }
            
            option.data('system', system);
            
            this.__configurations.append(option);
        }
    };
    
    /** Adds a jQueryUI slider to the vehicle tabs header */
    ctor.prototype.__addRangeSelector = function() {
        var tabheader = this.__dv.find('.vehicleTabs').children('ul');
        
        if (this.__exportPDF) {
        	this.__controllerWidth += 29;
        }
        
        if (!this.__chartEnabledByFleet[this.__fleetId] && typeof this.__chartEnabledByFleet[this.__fleetId] != "undefined") {
        	this.__controllerWidth -= 29;
        }
        
        if (this.__refreshPeriod != null) {
        	this.__controllerWidth += 95;
        }
        
        this.__controllerDiv = $('<div>')
            .css({
                'float': 'left',
                'height': '33px',
                'width': this.__controllerWidth + 'px',
                'margin-top': '-8px'
            }).addClass('ui-state-default')
            .addClass('ui-corner-all');
        
        var div = $('<div>').css({
            'position': 'absolute',
            'top': '-2px',
            'left': '6px',
            'height': '20px',
            'width': '200px',
            'box-sizing': 'border-box',
            'padding-left': '10px',
            'padding-right': '10px',
            'padding-top': '4px',
            'direction': 'ltr'
        });
        
        var slider = $('<div>').css({
            'width': '100%'
        });
        
        var value = $('<div>').css({
            'position': 'absolute',
            'top': '9px',
            'left': '82px',
            'height': '20px',
            'line-height': '16px',
            'width': '110px',
            'direction': 'ltr',
            'text-align': 'left',
            'padding-top': '2px'
        });
        
        div.append(slider);
        this.__controllerDiv.append(div);
        this.__controllerDiv.append(value);
        tabheader.append(this.__controllerDiv);
        
        var me = this;
        
        value.text(nx.util.timestampToHumanReadable(me.__periodValues[me.__firstPeriodPosition]));
        
        slider.slider({
            min: 0,
            max: me.__periodValues.length - 1,
            value: me.__firstPeriodPosition,
            slide: function(event, ui) {
                me.__period = me.__periodValues[ui.value];
                value.text(nx.util.timestampToHumanReadable(me.__period));
                if (!!me.__valuesGrid && me.__isVirtualScrollBar) {
                    me.__valuesGrid.getScroller().scrollTop(0);
                    me.__valuesGrid.getScroller().find(".tableContainer").height((me.__period / 1000) *21);
                }
                
                if (me.__refreshPeriod != null && me.__period >= me.__refreshPeriod) {
             	   $(me.__reloadButton).show();
             	   me.getUnitSummary().setNotLive();
             	   me.getUnitSummary().__dm.stop();
             	   
                } else {
                   if (!me.__refreshNotLive || $('.nowButton').hasClass('ui-state-active')) { 
             	       $(me.__reloadButton).hide();
                   }    
                }
            },
            stop: function(event, ui) {
                me.__refreshCharts();
                me.__refreshTable();
            }
        });
    };

    ctor.prototype.__disableDeleteButton = function() {
        this.__dvDeleteButton.button('disable');
    };
    
    ctor.prototype.__disableSaveButton = function() {
        this.__dvSaveButton.button('disable');
    };
    
    ctor.prototype.__enableDeleteButton = function() {
        this.__dvDeleteButton.button('enable');
    };
    
    ctor.prototype.__enableSaveButton = function() {
        this.__dvSaveButton.button('enable');
    };
    
    ctor.prototype.__enableDisableSaveButton = function() {
		if(this.__isSelectedChannelsEmpty())
			this.__disableSaveButton(); 
		else this.__enableSaveButton();
    };
	
	ctor.prototype.__isSelectedChannelsEmpty = function() {
		return (this.__selectedChannels.length == 0);
	}
    
    ctor.prototype.__isChannelGroupPopulated = function(groupEl) {
        if (groupEl.children(".channelGroup").length > 0) {
            return true;
        } else {
            return false;
        }
    };
    
    /** Selects the chart configuration by given id */
    ctor.prototype.__selectConfigurationById = function(configId) {
        var me = this;
        var options = $('#dvConfigurations select').children();
        
        for (var i = 0, l = options.length; i < l; i++) {
        	var option = $(options[i]);
            if (!!option.val() && option.val() == configId) {
            	/** Select the configuration */
            	option.attr({selected: 'selected'});
                this.__selectedConfig = configId;
                me.__configurations.val(configId);
                $.uniform.update(me.__configurations);
                break;
            }
        }
    };
    
    /** Selects the first chart configuration in the dropdown */
    ctor.prototype.__selectFirstConfiguration = function() {
        var me = this;
        var options = $('#dvConfigurations select').children();
        
        if (options.length > 0) {
            /** Select the first configuration */
            $(options[0]).attr({selected: 'selected'});
            this.__configurations.change();

            var option = $(options[0]);
            var configId = option.val();
            //var system = option.data('system');
            
            this.__setChartConfiguration(configId, function() {
                me.__refreshCharts();
                me.__enableDeleteButton();
            });
        } else {
            //an empty option list prevent uniform.update to refresh the component
            this.__configurations.append($("<option />"));
            $.uniform.update(this.__configurations);
            this.__configurations.empty();

            //unselect all channels
            this.__unselectChannels();

            //disable the buttons
            this.__disableDeleteButton();
            this.__disableSaveButton();

            //Clear all charts
            $("#dvMain #chartPane .ui-tabs-panel").empty();
        }
    };
    
    /** Fetches the channels for the first open channel group [accordion] */
    ctor.prototype.__populateInitialGroup = function() {
        // Get the initial element..
        var groupId = this.__accordion
            .find('.ui-state-active')
            .find('a')
            .attr('href');
        
        var groupEl = this.__accordion
            .find('.ui-accordion-content-active');
        
        groupId = groupId.replace(/#/, '');
        
        if (!this.__isChannelGroupPopulated(groupEl)) {
            var me = this;
            nx.rest.group.channels(me.__fleetId, groupId, function(channels) {
            	if (!me.__isChannelGroupPopulated(groupEl)) {
            		me.__addChannelsToGroup(groupEl, channels);
            	}
            });
        }
    };
    
    /** Adds a list of channels to the specified channel group */
    ctor.prototype.__addChannelsToGroup = function(groupEl, channels) {
        var me = this;
        
        var chanGroup = jQuery('<div/>').addClass('channelGroup');
        
        var position = 0;
        for (var i = 0, l = channels.length; i < l; i++) {
            var channel = channels[i];
            var chanDiv = jQuery('<div/>');
            if(channel.type != "TIMESTAMP" && channel.type != "TEXT") {
                if (position % 2 == 1) {
                    chanDiv.addClass('channel even');
                } else {
                    chanDiv.addClass('channel');
                }
                position ++;
                
                
                var descDiv = jQuery('<div/>', {
                    title: $.i18n(channel[me.__channelGroupLabelConfig.value]),
                    text: $.i18n(channel[me.__channelGroupLabelConfig.label])
                }).addClass('desc');
                
                chanDiv.append(descDiv);
                
                var checkBoxDiv = jQuery('<div/>').addClass('checkbox');
                
                var checkBox = jQuery('<input/>', {
                   type: 'checkbox',
                   value: channel.id
                }).addClass('dvChannelCheckbox');
                
                if (this.__isSelected(channel.id)) {
                    checkBox.attr('checked', 'checked');
                }
                
                checkBoxDiv.append(checkBox);
                
                checkBox.uniform().click(function(el) {
                    var checked = $(this).attr('checked');
                    var channelId = $(this).attr('value');
                    
                    if (!!checked) {
                        me.__selectChannel(channelId);
                    } else {
                        me.__unselectChannel(channelId);
                    }
                    
                    $(this).attr('checked', !!checked);
					me.__enableDisableSaveButton();
                    me.__refreshCharts();
                    me.__redrawTable();
                });
                
                chanDiv.append(checkBoxDiv);
                
                chanGroup.append(chanDiv);
            }
        }
        
        groupEl.append(chanGroup);
        
    };
    
    /** Unselect all channels */
    ctor.prototype.__unselectChannels = function() {
        this.__dv.find('.dvChannelCheckbox').each(function() {
            $(this).prop('checked', false);
            $.uniform.update($(this));
        });
        
        this.__selectedChannels = [];
    };
    
    ctor.prototype.__isSelected = function(id) {
        var arr = this.__selectedChannels;
        
        for (var i = 0, l = arr.length; i < l; i++) {
            if (id == arr[i]) {
                return true;
            } 
        }
        
        return false;
    };
     
    ctor.prototype.__selectChannel = function(id) {
        var arr = this.__selectedChannels;
        
        for (var i = 0, l = arr.length; i < l; i++) {
            if (id == arr[i]) {
                // already selected.
                return false;
            }
        }
        
        arr.push(id);
        return true;
    };
    
    ctor.prototype.__unselectChannel = function(id) {
        var arr = this.__selectedChannels;
        
        for (var i = 0, l = arr.length; i < l; i++) {
            if (id == arr[i]) {
                arr.splice(i, 1);
                break;
            }
        }
    };
    
    ctor.prototype.__refreshCharts = function() {
		var tabSelected = this.stockTabs.getSelectedTabEl();
		var pane = $(tabSelected);
		var chartSpan = pane.find('.dvChart');

        if (!this.__showValuesTable && !this.__isSelectedChannelsEmpty()) {
			if (chartSpan.length > 0) {
				var chartDiv = chartSpan[0];
				$(chartDiv).show();
			}
            var timestamp = this.getUnitSummary().getTimestamp();
            
            this.__loadChart(
                    this.stockTabs.getSelectedStockId(),
                    this.stockTabs.getSelectedTabEl(),
                    timestamp - this.__period,
                    timestamp
            );
        }else if(this.__isSelectedChannelsEmpty()){
			if (chartSpan.length > 0) {
				var chartDiv = chartSpan[0];
				$(chartDiv).hide();
			}
		}
    };
    
    /** Loads a charts channels */
    ctor.prototype.__loadChart = function(stockId, objEl, startTime, endTime) {
        if (stockId == null) {
            return;
        }
        var pane = $(objEl);
        var me = this;
        
        var chartSpan = pane.find('.dvChart');
        var channels = this.__selectedChannels;
        
        if (channels.length < 1) {
            return;
        }
        
        if (chartSpan.length != 1) {
            // Should already be empty, but clear it anyway.
            chartSpan.empty();
            
            /*
             * We are rendering that :
             * <div id = 'pane' style = 'position: relative; height: 100%; width: 100%'>   
             * <div id = 'chartBody'></div> 
             * <div id = 'scroller'>
             *      <table>
             *          <tbody></tbody>
             *      </table>    
             * </div>         
             * </div>         
             */
            
            var paneScroller = $('<div id = \'pane\' style = \'position: relative; height: 100%; width: 100%\'>');
           
               // FIXME virtual scrollbar is very messy. 
            if (this.__isVirtualScrollBar) {
                var scroller = $('<div id = \'scroller\'>');
                scroller.append('<table><tbody></tbody></table>');

                pane.unbind('mousewheel');
                pane.bind('mousewheel', function(e, d) {

                    var pan = $(".vehicleTabs div.ui-tabs-panel:not(.ui-tabs-hide)")[0];

                    // if there is no scroll bar
                    var noScroll = pan.scrollHeight - pan.offsetHeight  < 5;

                    var endScroll = pan.scrollHeight - (pan.scrollTop + pan.offsetHeight) <5;
                    var topScroll = pan.scrollTop == 0;


                    if (noScroll  
                            ||(me.__endOfPage == true && endScroll && d <0) // scroll down
                            ||(me.__topOfPage == true && topScroll && d >0)) { // scroll up
                        var newIdx = me.configIdx + d;

                        if (d > 0) {
                            newIdx = me.configIdx - d;
                        } else {
                            newIdx = me.configIdx -d ;
                        }

                        if (newIdx > -1 && newIdx < me.__chartConfigs.length) {

                            var config = me.__chartConfigs[newIdx];
                            var el = $(pane.find('#scroller table tbody tr td')[newIdx]);

                            me.__selectConfig(config, el, newIdx, true);
                        }

                    }

                    me.__endOfPage = endScroll;
                    me.__topOfPage = topScroll;

                });

            }
            
            // Create the frame for the charts..
            var div = $('<div>')
                .addClass('dvChart')
                .css('opacity', 0);
            
            pane.append(paneScroller);
            paneScroller.append(div);
            
            if (this.__isVirtualScrollBar) {
                paneScroller.append(scroller);


                var configurations = new Array();

                for (var i = 0, l = this.__chartConfigs.length; i < l; i++) {
                    var conf = this.__chartConfigs[i];

                    var abrConf = "";

                    if(!!conf.name) {
                        var words = conf.name.split(" ");
                        for (var iw = 0; iw < words.length; iw++) {
                            if (words[iw].length > 0) {
                                if('(' != words[iw].charAt(0)) {
                                    abrConf += words[iw].charAt(0);
                                }
                            }
                        }

                        configurations.push({id:conf.id, name:conf.name, abbr:abrConf});
                    }
                }

                var scrollTable = pane.find('#scroller table tbody');

                scrollTable.empty();
                
                var lines = 1 ;

                for(var idx = 0; idx < configurations.length; idx += lines) {

                    var row = $('<tr>');

                    for (var idLn = 0; idLn < lines;idLn++) {
                        var line = null;
                        if (idx+idLn < configurations.length) {
                            var pos = idx+idLn;
                            var config = configurations[pos];
                            line = $('<td><div title="'+ config.name+'">' + config.abbr + '</div></td>');

                            $(line).click(function(config, pos) {
                                return function() {
                                    me.__selectConfig(config, $(this), pos, true);
                                };
                            }(config, pos));
                        } else {
                            line = $('<td>');
                        }

                        row.append(line);

                    }

                    scrollTable.append(row);

                }
                
                if(configurations.length > 0) {
                    this.__selectConfig(configurations[0], $(pane.find('#scroller table tbody tr td')[0]), 0, true);
                }

                
            }
            
            var src = this.__requestChart(stockId, startTime, endTime, pane, channels);
            
            var img = $('<img>')
                .attr('id', 'imgDataplot')
                .attr('alt', 'dataplot')
                .attr('src', src)
                .addClass('dataplot');

            div.append(img);//.appendTo(paneScroller);


            img.one('load', function() {
                div.animate({'opacity': 1}, 1000);
            });
        } else {
            
            clearTimeout(this.__idRequest);
            
            this.__idRequest =  setTimeout(function(){
                
                var dvChart = $('.dvChart');
                
                var img = dvChart.find('img.dataplot');

                var newSrc = me.__requestChart(stockId, startTime, endTime, pane, channels);

                $(img).css("height", "auto");
                $(img).attr('src', newSrc);
                
            }, 300);
            
        }
    };
    
    /* retrieve the element after a timout*/
    // XXX remove startTime
    ctor.prototype.__requestChart = function(stockId, startTime, endTime, pane, channels) {
        
        var newSrc = nx.rest.chart.chart(this.__fleetId,
                stockId,
                this.__period / 1000,
                 endTime,
                 encodeURIComponent(jstz.determine().name().replace('/', '-')),
                null,
                parseInt(pane.find('#pane').width() - pane.find('#scroller').outerWidth(true)),
                channels);
        
        return newSrc;
    };
    
    ctor.prototype.__selectConfig = function(config, el, pos, updateDropdown) {
        if (updateDropdown) {
            this.__configurations.val(config.id);
            this.__configurations.change();
            $.uniform.update(this.__configurations);
        }

        this.configIdx = pos;

        $('.selected').removeClass('selected');
        $(el).addClass('selected');
        
    };
    
    /** Loads a chart configuration and selects its channels */
    ctor.prototype.__setChartConfiguration = function(configId, callback) {
        var me = this;
        
        this.__selectedConfig = configId;
        
        me.__configurations.val(configId);
        $.uniform.update(me.__configurations);
        
        nx.rest.chart.channels(me.__fleetId, configId, function(channels) {
            me.__unselectChannels();
            
            for (var i = 0, l = channels.length; i < l; i++) {
                var channel = channels[i];
                var checkbox = me.__dv.find('input:checkbox[value="' +
                        channel + '"]');
                
                checkbox.prop('checked', true);
                $.uniform.update(checkbox);
                
                me.__selectChannel(channel);
            }
            
            if (callback != null) {
                callback();
            }
        });
    };
    
    ctor.prototype._logging = function() {
        //logging unit summary - data plots access
        nx.rest.logging.unitDataPlots();
    };
    
    ctor.prototype._update = function(data) {
        var unitId = data.unitId;
        var timestamp = data.timestamp;
        var me = this;
        
        if (unitId != null && data.fleetId != null) {

            if (this.__fleetId == null) {
                this.__fleetId = data.fleetId;
                this.__loadChartConfigurations(function() {
                    me.__selectFirstConfiguration();
                    //me.__populateInitialGroup(); 
                });
            }
            
            this.__fleetId = data.fleetId;
            
            if (data.unitChanged()) {

                // Reload channel categories
                
                nx.rest.group.all(data.fleetId,function(groups) {
                    
                	var activeTab = 0;
                	if(!!data.sameFormation) {
                		activeTab = me.__accordion.accordion('option', 'active');
                	}
                	
                	me.__accordion.accordion("destroy");
                    me.__accordion.empty();
                    
                    var toAdd = '';
                    
                    for (var i = 0; i < groups.length; i++) {
                        var grp = groups[i];
                        toAdd += '<h3><a href="'+grp.id;
                        toAdd += '">'+$.i18n(grp.description)+'</a></h3>';
                        toAdd += '<div></div>';
                    }
                    
                    me.__accordion.append(toAdd);
                    
                    me._initAccordion();
                    me.__accordion.accordion('option', 'active', activeTab);
                    me.__populateInitialGroup();
                    
               });
                
                
                nx.rest.unit.stock(data.unitId, data.fleetId, function(stockList) {
                    me.stockTabs.setStock(stockList);
                    
                    me.__loadChartConfigurations(function() {
                        // delete panel
                        var tabSelected = me.stockTabs.getSelectedTabEl();
                        var pane = $(tabSelected);
                        var chartSpan = pane.find('#pane');
                        if (chartSpan.length > 0) {
                            var chartDiv = chartSpan[0];
                            $(chartDiv).remove();
                        }
                        
                        me.__refreshCharts();
                        me.__redrawTable();
                        if (!data.sameFormation) {
                        	me.__selectFirstConfiguration();
                        } else {
                        	me.__selectConfigurationById(me.__selectedConfig);
                        }
                    });
                    
                });
            } else {
            	if ((me.__refreshPeriod == null || me.__period < me.__refreshPeriod)
            			&& !$(me.__reloadButton).is(":visible")){
                    me.__refreshCharts();
                    me.__refreshTable();
            	}
            }
        }
    };
    
    ctor.prototype.__stockChanged = function(evt, stockId, objEl) {
        if(!!this.__chartEnabledByFleet && (this.__chartEnabledByFleet[this.__fleetId] || typeof this.__chartEnabledByFleet[this.__fleetId] == "undefined")) {
        	this.__hideValues(false);
        }
        this.__refreshCharts();
        this.__refreshTable();
    };

    ctor.prototype._hasTimeController = function() {
        return true;
    };
    
    /** Export the chart values to csv file */
    ctor.prototype.__download = function() {
        
        if (this.__selectedChannels.length == 0) {
            return;
        }
        
        var timestamp = this.getUnitSummary().getTimestamp();
        
        var stockId = this.stockTabs.getSelectedStockId();
        var startTime = timestamp - this.__period;
        var endTime = timestamp;
        var channels = this.__selectedChannels;

        if(stockId == null) {
            return;
        }
        var url = nx.rest.chart.chartExport(
            this.__fleetId,
            stockId,
            this.__period / 1000,
            endTime,
            channels,
            encodeURIComponent(jstz.determine().name().replace('/', '-'))
        );
        nx.util.download(url);
    };
    
    /** Export the chart values to pdf file */
    ctor.prototype.__downloadPdf = function() {
    	
        if (this.__selectedChannels.length == 0) {
            return;
        }
        
        var unitNumber = $(".selectedFormationUnit").html();
        var vehicleID = this.stockTabs.getSelectedStockId();
        var vehicleNumber = "";
        $.each(this.stockTabs.stockList, function( index, stock ) {
        	if(stock.id == vehicleID) {
        		vehicleNumber = stock.description;
        	}
        });
        
        timestamp = this.getUnitSummary().getTimestamp();
        var time = new Date(timestamp);
        var month = time.getMonth() + 1;
        var minutes = time.getMinutes();
        var min = "";
        if (minutes <= 9) {
        	var min = "0" + minutes; 
        } else {
        	min = minutes;
        }
        var seconds = time.getSeconds();
        var sec = "";
        if (seconds <= 9) {
        	sec = "0" + seconds; 
        } else {
        	sec = seconds;
        }
        var date = time.getDate() + "/" + month + "/" + time.getFullYear() + 
            " " + time.getHours() + ":" + min + ":" + sec;
        var tableContent = $("<table>")
        .append(
            $('<tr>')
            .append(
                $('<td>')
                .text("Unit Number : " + unitNumber)),
            $('<tr>')
            .append(
                $('<td>')
                .text("Vehicle Number : " + vehicleNumber)),
            $('<tr>')
            .append(
                $('<td>')
                .text("Date : " + date)),
            $('<tr>')
            .append(
                $('<td>')
                .text("")));
        var dataTable = "<table></table><table>" + tableContent.html() + "</table>";
    	var pane = $(this.stockTabs.getSelectedTabEl());
    	var type = "dataView";
    	
    	var width = this.__lastChartWidth;
    	if (!this.__showValuesTable) {
    	    width = parseInt(pane.find('#pane').width() - pane.find('#scroller').outerWidth(true));
    	}
    	
    	var url = nx.rest.chart.exportaspdf(
    	    this.__fleetId,
       	    unitNumber,
    	    vehicleID,
    	    this.__period / 1000,
    	    timestamp,
    	    encodeURIComponent(jstz.determine().name().replace('/', '-')),
    	    null,
    	    width,
    	    this.__selectedChannels,
    	    type);
    	
    	nx.util.downloadPostImageAndData(url, null, dataTable);
    };
    
    /** Show Values table */
    ctor.prototype.__showValues = function() {
        
        this.__hideValuesButton.show();
        this.__showValuesButton.hide();
        
        this.__showValuesTable = true;
        
        var tabSelected = this.stockTabs.getSelectedTabEl();
        var pane = $(tabSelected);
        this.__lastChartWidth = parseInt(pane.find('#pane').width() - pane.find('#scroller').outerWidth(true));
        var chartSpan = pane.find('#pane');
        if (chartSpan.length > 0) {
            var chartDiv = chartSpan[0];
            $(chartDiv).remove();
            
        }
        
        if (this.__isVirtualScrollBar) {
            // unbind mousewheel 
            pane.unbind('mousewheel');
        }
        
        //TODO put here the code to create the table
        this.__redrawTable();
    };
    
    /** Hide values table and show charts 
     * @param togglePlay if we should toggle play
     */
    ctor.prototype.__hideValues = function(togglePlay) {
        var me = this;
        
        this.__hideValuesButton.hide();
        this.__showValuesButton.show();
        
        this.__showValuesTable = false;
        
        // FIXME should not call publish.
        if (togglePlay) {
            this.getUnitSummary().getToggleButton().play(function() {
                me.getUnitSummary().getTimePicker().setLive();
                nx.hub.play.publish();
            });
        }
        
        this.__toggleButton.show();
        
        var tabSelected = this.stockTabs.getSelectedTabEl();
        var pane = $(tabSelected);
        var tableSpan = pane.find('.dvTable');
        
        if (tableSpan.length > 0) {
            var tableDiv = tableSpan[0];
            $(tableDiv).hide();
        }
        
        var chartSpan = pane.find('.dvChart');
        
        if (chartSpan.length > 0) {
            var chartDiv = chartSpan[0];
            $(chartDiv).show();
        }
    };
    
    /** Create and Load the values table */ 
    ctor.prototype.__loadChannelsValues = function() {
        var me = this;
        // Pause the time controller
        me.__previousStatePaused = this.getUnitSummary()
                .getToggleButton().isPaused();
        
        me.getUnitSummary().getToggleButton().pause(function() {
            nx.hub.pause.publish();
            me.getUnitSummary().getTimePicker().setNotLive();
        });
        
        me.__toggleButton.hide(); //Hide the Play/Pause button
        
        var stockId = this.stockTabs.getSelectedStockId();
        var tabSelected = this.stockTabs.getSelectedTabEl();
        
        //Calculates the time range
        var timestamp = me.getUnitSummary().getTimestamp();
        var startTime = timestamp - this.__period;
        var endTime = timestamp;
        
        var channels = this.__selectedChannels;
        
        var pane = $(tabSelected);
        
        if (channels.length < 1) {
            return;
        }
        
        if(stockId == null) {
            return;
        }
        
        me.__valuesGrid = null;
        
        //Update or create the table
        var grid = null;
        var tableSpan = pane.find('.dvTable');
        if (tableSpan.length < 1) {
            
            // Create the frame for the table.
            grid = $('<div>')
                .addClass('dvTable');
            grid.appendTo(pane);
            
            //Time div
            me.__timePositionDiv = $('<div>')
                .addClass('ui-corner-all')
                .attr('id','tableTimeDiv')
                .attr('text','timestamp');
            me.__timePositionDiv.appendTo(pane);
            me.__timePositionDiv.hide();
        } else {
            grid = $(tableSpan[0]);
            grid.show();
        }
        
        //Calculates the number of rows per page
        var startRow = 0;
        var endRow = this.__maxNumberRows;
        this.__numberOfRows = this.__maxNumberRows;
        if (this.__isVirtualScrollBar) {
            endRow = Math.round((grid.height() - 160) / 21);
            this.__numberOfRows = endRow;
        }
        var scrollSize = me.__period / 1000;
        nx.rest.chart.values(
            me.__fleetId,
            stockId,
            me.__period / 1000,
            endTime,
            endRow,
            channels,  function(response) {
                div = me.__createTable(grid, response.columns,scrollSize);
                me.__fillValuesTable(response.columns,response.data);
            }
        );
        
        //Hide the chart
        var chartSpan = pane.find('.dvChart');
        if (chartSpan.length > 0) {
            var chartDiv = chartSpan[0];
            $(chartDiv).hide();
        }
    };
    
    /** Fills the values table */
    ctor.prototype.__fillValuesTable = function(columns,data) {
        var gridData = [];
        for (var i = 0; i < data.length; i++) {
        	gridData.push({id: i, data: this.__getRow(columns, data[i].channels)});
        }
        this.__valuesGrid.__internalSetData(gridData);
    };
    
    /** Creates the values table */
    ctor.prototype.__createTable = function(grid, columns, totalRows) {
        var me = this;
        
        grid.html('<table class = "ui-corner-all"></table>');

        if (!columns) {
            return;
        }

        var cols = [];
        var text, mouseOver;
        
        for (var i = 0, l = columns.length; i < l; i++) {
            var col = columns[i];
            
            text = this.__tableChannelLabelConfig.label === 'name' ? col.header : col.mouseOverHeader;
            mouseOver = this.__tableChannelLabelConfig.value === 'name' ? col.header : col.mouseOverHeader;
            
            cols.push({
                'name': col.name,
                'text': text,
                'width': col.width + 'px',
                'sortable': col.sortable,
                'styles': col.styles,
                'mouseOverHeader': mouseOver,
                'formatter': function(el, value) {
                    return me.__columnFormatter(el, value);
                }
            });
        }
        
        // Values table
        me.__valuesGrid = grid.tabular({
            'columns': [cols],
            'scrollbar': true,
            'fixedHeader': true,
            'renderOnlyVisibleRows': this.__isVirtualScrollBar,
            'tableHeight': 160 + (totalRows  * 21), //21 height of each rows
            'scrollHandler': (function(me) {
                    return function(obj) {
                        return me.__scrollHandler(obj);
                    };
                })(me)
        });
    };
    
    /** Manages the Scroll Event */
    ctor.prototype.__scrollHandler = function(objEvent) {
        var me = this;
        
        if (me.__showValuesTable) {
            if (!!me.__valuesGrid) {
                var startPosition = me.__valuesGrid.getScroller().scrollTop();
                var startRow = Math.round(me.__valuesGrid.getScroller().scrollTop()/21);
                
                if((startRow != me.__lastFirtRow) || startPosition == 0) {
                    me.__lastFirtRow = startRow;
                    
                    me.__refreshTable(startRow);
                }
            }
        }
    };
    
    ctor.prototype.__refreshTable = function(startRow) {
        var me = this;
        
        if (me.__showValuesTable) {
            var timestamp = me.getUnitSummary().getTimestamp();
            var endTime = timestamp;
            var startTime = timestamp - me.__period;
            startTime = startTime + ((startRow != null? startRow: 0) * 1000);
            
            var stockId = me.stockTabs.getSelectedStockId();
            var rowNumber = me.__numberOfRows;
            var channels = me.__selectedChannels;
            if(me.__timePositionDiv) {
            	me.__timePositionDiv.show();
            	me.__timePositionDiv.text($.format("{date}", startTime));
            }
            clearTimeout(me.__scrollerTimer);
            me.__scrollerTimer = setTimeout( function (){
                nx.rest.chart.values(
                    me.__fleetId,
                    stockId,
                    (endTime - startTime) / 1000,
                    endTime,
                    rowNumber,
                    channels, function(response) {
                        me.__fillValuesTable(
                                response.columns,response.data);
                        var scrollPos = me.__valuesGrid.getScroller()
                        .scrollTop();
                        me.__valuesGrid.getTable().css("top",
                                scrollPos + "px");
                        clearTimeout(me.__timestampTimer);
                        me.__timestampTimer = setTimeout( function (){
                            me.__timePositionDiv.hide(300);
                        },2000);
                    }
                );
            }, 100 );
        }
    };
    
    ctor.prototype.__redrawTable = function() {
        var me = this;
        
        if (me.__showValuesTable) {
            me.__loadChannelsValues();
        }
    };

    /** Creates the rows for the table from channels values */
    ctor.prototype.__getRow = function(columns, channels) {
    	
        var row = [];
        
        for (var i = 0, l = columns.length; i < l; i++) {
            var channel = channels[columns[i].name];
            if (!!channel) {
                if (i == 0) {// TimeStamp
                    channel[0] = $.format("{date}", channel[0]);
                }
                row.push(channel);
            } else {
                row.push([null, 0]);
            }
        }
        return row;
    };
    
    ctor.prototype.__columnFormatter = function(column, value) {
        var cellText = '&nbsp;';
        if (!!value && value[0] != null && value[0] != 'null') {
            cellText = value[0];
        }
        
        var cellCat = !!value ? value[1] || null : null;
        var className = !!cellCat ? column.styles[nx.util.CATEGORIES[cellCat]]
                : 'channeldata_default';

        return "<div class = '" + className + "'>" + cellText + "</div>";
    };
    
    ctor.prototype.__subscribeToEvents = function() {
    	var me = this;
    	
    	nx.hub.play.subscribe(function(e) {
    		if (me.__refreshNotLive && me.__period < me.__refreshPeriod) {
     	        $(me.__reloadButton).hide();
    		}    
    	});
    	
    	nx.hub.pause.subscribe(function(e) {
    		if (me.__refreshNotLive && me.__period < me.__refreshPeriod) {
      	        $(me.__reloadButton).show();
    		}    
     	});
    	
    	nx.hub.timeChanged.subscribe(function(e) {
    		if (me.__refreshNotLive && me.__period < me.__refreshPeriod) {
      	        $(me.__reloadButton).show();
    		}    
     	});
    	
    	nx.hub.startLive.subscribe(function(e) {
    		if (me.__refreshNotLive && me.__period < me.__refreshPeriod) {
      	        $(me.__reloadButton).hide();
    		}    
     	});
    	
    	nx.hub.stopLive.subscribe(function(e) {
    		if (me.__refreshNotLive && me.__period < me.__refreshPeriod) {
      	        $(me.__reloadButton).show();
    		}    
     	});
    };

    return ctor;
}());
