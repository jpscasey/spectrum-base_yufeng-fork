var nx = nx || {};
nx.event = nx.event || {}; 

nx.event.EventTableDetail = (function() {
    function ctor() {
        var me = this;
    };
    
    
    ctor.prototype.render = function (rootPane, configuration, event) {
        
    	var me = this;
    	
    	var header = this.__createHeader();
        var tableContainer = this.__createTableContainer();
        
        nx.rest.event.eventDetailExtension(event.fleetId, event.id, function(eventDetailMap) {
        	tableContainer.append(me.__createTable(eventDetailMap));
        });
        
        rootPane.empty();
        rootPane.append(header);
        rootPane.append(tableContainer);
    };
    
    
    
    ctor.prototype.__createHeader = function () {
    	return $('<div>').attr('id', 'eventTableDetailHeader').addClass("ui-widget-header").text("Additionnal Event Detail");
    };
    
    
    
    ctor.prototype.__createTableContainer = function () {
    	return $('<div>').attr('id', 'eventTableDetailPane');
    };
    
    
    
    ctor.prototype.__createTable = function (eventDetailMap) {
        var extDetailTable = $('<table>').attr('id', 'eventTableDetail');
        
        $.each(eventDetailMap, function (key, value) {
        	var tr = $('<tr>');
        	var val = value.value;
        	
        	if(value.columnType == "TIMESTAMP") {
        	    val = $.format("{date}", val);
        	}
        	
        	$('<td class="eventLabel">'+value.label+'</td>').appendTo(tr)
        	$('<td>'+val+'</td>').appendTo(tr)
        	extDetailTable.append(tr);
        });
        
        return extDetailTable;
    };
    
    
    
    return ctor;
}());