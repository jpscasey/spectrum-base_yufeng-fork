var nx = nx || {};
nx.cab = nx.cab || {};

nx.cab.Uncouple = (function () {
    var ctor = function (el, component, settings) {
        nx.cab.Widget.call(this);

        this._value = false;
        this._settings = $.extend({
            cx: component.x+component.width / 2,
            cy: component.y+component.height / 2,
            radius: Math.min(component.width, component.height) / 2
        }, settings);

        this._paper = el;
        this._dial = this._draw();
    };
    
    ctor.prototype = new nx.cab.Widget();

    ctor.prototype._draw = function() {
        var cx = this._settings.cx;
        var cy = this._settings.cy;
        var radius = this._settings.radius;

        return this._paper.set(
            this._paper.path([
                'M', cx - radius + (radius / 20), cy - radius / 16,
                'L', cx, cy - radius / 3.8,
                'L', cx + radius - (radius / 20), cy - radius / 16,
                'L', cx + radius - (radius / 20), cy + radius / 16,
                'L', cx, cy + radius / 3.8,
                'L', cx - radius + (radius / 20), cy + radius / 16,
                'Z'
            ].join(',')).attr({
                fill: '#E60000',
                stroke: 'none'
            }),

            this._paper.rect(
                cx - radius + (radius / 20),
                cy - radius / 12,
                (radius - (radius / 20)) * 2,
                radius / 6,
                radius / 12).attr({
                fill: '90-#FF0000-#e60000',
                stroke: '#900',
                'stroke-width': 1
            }),

            this._paper.circle(cx, cy, radius / 3).attr({
                fill: '90-#FF0000-#FF0000-#e60000',
                stroke: '#900'
            }),

            this._paper.circle(cx, cy, radius / 5).attr({
                fill: '#777'
            }),

            this._paper.circle(cx, cy, radius / 8).attr({
                fill: '#444'
            })
        );
    };
    
    ctor.prototype._reset = function() {
        this.val(false);
    };

    ctor.prototype._update = function(channelName, channelValue, channelCategory) {
        if (channelValue != null) {
            var cx = this._settings.cx;
            var cy = this._settings.cy;
            this._dial.transform('R' + (channelValue ? 0 : -90) + ',' +  cx + ',' + cy);
            this._value = channelValue;
        }
    };

	ctor.prototype.val = function() {
        if (arguments.length > 0) {
            var newValue = arguments[0];

            if (newValue != this._value) {
                var cx = this._settings.cx;
                var cy = this._settings.cy;
                this._dial.transform('R' + (newValue ? 0 : -90) + ',' +  cx + ',' + cy);
                this._value = newValue;
            }
        } else {
            return this._value;
        }
    };

    return ctor;
}());
