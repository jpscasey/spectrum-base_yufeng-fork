(function( $ ) {
    var DateRangePicker = function(el, config) {
        this.__el = el;
        this.__config = config;

        this.__dateTo = null;
        this.__dateFrom = null;

        this.__datePickerField = $('<div>').addClass('dateRangePickerField').appendTo(this.__el);

        this.__datePickerInterval = $('<select>');
        $('<div>').addClass('dateRangePickerInterval').append(this.__datePickerInterval).appendTo(this.__el);

        this.__datePickerCalendar = $('<div>').addClass('dateRangePickerCalendar').appendTo(this.__el);

        this.__initializeInterval();
        this.__initializeCalendar();
    };

    DateRangePicker.prototype.__initializeInterval = function() {
        var me = this;

        $('<option />', {
            'val': '30',
            'text': 'Last month'
        })
        .prop('selected', true)
        .appendTo(this.__datePickerInterval);

        $('<option />', {
            'val': '7',
            'text': 'Last week'
        })
        .appendTo(this.__datePickerInterval);

        $('<option />', {
            'val': '1',
            'text': 'Yesterday'
        })
        .appendTo(this.__datePickerInterval);

        $('<option />', {
            'val': '0',
            'text': 'Custom'
        })
        .appendTo(this.__datePickerInterval);

        this.__datePickerInterval.uniform();

        this.__datePickerInterval.change(function() {
            var interval = $(this).val();
            if (interval > 0) {
                var to = new Date();
                var from = new Date(to.getTime() - (interval * 86400000));

                me.__setFieldValue(from, to);
                me.__setCalendarValue(from, to);
            }
        });
    };

    DateRangePicker.prototype.__initializeCalendar = function() {
        var me = this;

        var to = new Date();
        var from = new Date(to.getTime() - (30 * 24 * 60 * 60 * 1000));

        this.__datePickerCalendar.DatePicker({
            'inline': true,
            'date': [from, to],
            'calendars': 3,
            'mode': 'range',
            'onChange': function(dates, el) {
                me.__datePickerInterval.val(0).change();
                me.__setFieldValue(dates[0], dates[1]);
            }
        });

        this.__setFieldValue(from, to);
    };

    DateRangePicker.prototype.__setFieldValue = function(dateFrom, dateTo) {
        this.__dateFrom = dateFrom;
        this.__dateTo = dateTo;

        this.__datePickerField.text(
                dateFrom.getDate() + " " +
                dateFrom.getMonthName(true) + ", " +
                dateFrom.getFullYear() + " - " +
                dateTo.getDate() + " " +
                dateTo.getMonthName(true) + ", " +
                dateTo.getFullYear()
        );
    };

    DateRangePicker.prototype.__setCalendarValue = function(dateFrom, dateTo) {
        this.__datePickerCalendar.DatePickerSetDate([dateFrom, dateTo], true);
    };

    DateRangePicker.prototype.getDateFrom = function() {
        return this.__dateFrom;
    };

    DateRangePicker.prototype.getDateTo = function() {
        return this.__dateTo;
    };

    $.fn.daterangepicker = function(config) {
        var dateRangePicker = new DateRangePicker(this, config);
        return dateRangePicker;
    };
})(jQuery);
