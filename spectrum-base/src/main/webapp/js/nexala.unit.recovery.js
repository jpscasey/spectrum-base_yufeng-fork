var nx = nx || {};
nx.unit = nx.unit || {};
nx.unit.Recovery = nx.unit.Recovery || {};

nx.unit.Recovery = (function() {
    function ctor(us) {
    	nx.unit.Tab.call(this, us);
    	
        this.__faultId = us.__faultId;
        this.__fleetId = us.__fleetId;
        this.__contextId = null;
        this.__event = null;
        this.__cl90UnitId = null;
        
        this.__recoveryColumns = [];        
        this.__rowIndex = 0;
        this.__recoveryTable = null;
        this.__recoveryGrid = null;       
        this.__currentRecoveryRow = null;
        this.__currentAnswers = [];
        this.__selectedAnswer = null;
        this.__answerButtonIds = [];
        this.__responseButtonIds = [];
        this.__recoveryNotesId = null;
        this.__recoveryCompleted = false;
        this.__globalFaultTime = null;
        
        this.__initializeFaultTable();
        this.__initializeRecoveryTable();
        this.__initializeTimer();

    };
    
    /** Extend nx.unit.Tab */
    ctor.prototype = new nx.unit.Tab();
    
    ctor.prototype._onHide = function() {
        $('#timer').hide();
    };
    
    ctor.prototype._onShow = function() {
		$('#timer').show();
    }
    
    ctor.prototype._update = function(data){ 

    	// On first load, store the unitId from data to create a unique Id for this fault
    	// (used by getFaultData for class90 to reload the fault if the user switches vehicles)
    	if(this.__cl90UnitId == null){
    		this.__cl90UnitId = data.unitId;
    	}
    	
    	if(data.unitId && data.unitChanged()){
    		if(!!this.__event && this.__event.sourceId != data.unitId && this.__cl90UnitId != data.unitId){
     			this.__clearFaultData();
     			this.__clearRecoveryData();
    		} else {
    			this.__getFaultData();
    		}
    	}
    }

    ctor.prototype._logging = function() {
    };
    
    ctor.prototype._hasTimeController = function() {
        return false;
    };
    
    ctor.prototype.__initializeFaultTable = function() {
    	
    	this.__faultTable = $("#faultSummaryTable");
    	this.__faultTable.html('<table class = "ui-corner-all"></table>');
        this.__columns = []; 
    	
        var me = this;
        
    	nx.rest.recovery.columns(function(response) {
    		for (var i = 0; i < response.length; i++) {
                var column = response[i];
    		    
                me.__columns.push({
                    'name': column.name,
                    'text': column.header,
                    'width': column.width + 'px',
                    'type': column.type,
                    'visible': column.visible,
                    'sortable': column.sortable,
                    'format': column.format,
                    'handler': column.handler
                });
            }
    		
    		me.__faultGrid = me.__faultTable.tabular({
    			columns: me.__columns
    		});
    	})
    	
    	var restartButton = $('<button id = "restartRecoveryButton">' + $.i18n('Restart Recovery') + '</button>')
	    .button({	
	    }).click(function(e) {
	    	me.__restartRecovery();
        }).addClass("ui-state-disabled").attr('disabled', 'disabled');
	    $('#faultSummary').append(restartButton);
    }
    
    ctor.prototype.__initializeTimer = function() {
        
        var me = this;
        $('#unitSummaryHeader').append('<div id = "timer"></div>');
        
        window.setInterval(function() {
            if (!!me.__globalFaultTime) {
                var faultTimeStamp = me.__globalFaultTime;
                faultTimeStamp = (faultTimeStamp-(faultTimeStamp % 1000)) / 1000;
                var currentTimeStamp = (new Date).getTime();
                currentTimeStamp = (currentTimeStamp-(currentTimeStamp % 1000)) / 1000;
                var timeDifference = currentTimeStamp - faultTimeStamp;
                var minutes = Math.floor(timeDifference / 60);
                var seconds = timeDifference - minutes * 60;
                if (seconds < 10) {
                    seconds = '0' + seconds;
                }
                
                $("#timer").text(minutes + ":" + seconds);
            }
        }, 1000);
    }
    
    ctor.prototype.__initializeRecoveryTable = function() {
    	var me = this;
    	
    	this.__recoveryTable = $("#recoveryDetailsTable");
    	this.__recoveryTable.html('<table id = "recoveryDetailsGrid" class = "ui-corner-all"></table>');
    	
    	this.__addRecoveryColumn("question", "Question", 80);
    	this.__addRecoveryColumn("answer", "Answer", 80);
    	this.__addRecoveryColumn("action", "Action", 80);
    	this.__addRecoveryColumn("worked", "", 80);
    	this.__addRecoveryColumn("notes", "Recovery Notes", 80);
    	
    	this.__recoveryTable.tabular({
    		columns: this.__recoveryColumns
    	});
    	
    	this.__recoveryGrid = $("#recoveryDetailsGrid");
    	
    	var completeButton = $('<button id = "completeRecoveryButton">' + $.i18n('Complete Recovery') + '</button>')
	    .button({
	    }).click(function(e) {
	    	me.__completeRecovery();
        }).addClass("ui-state-disabled").attr('disabled', 'disabled');
    	
    	$("#recovery").append(completeButton);
    }
    
    ctor.prototype.__addRecoveryColumn = function(name, header, width) {
    	this.__recoveryColumns.push({
            'name': name,
            'text': header,
            'width': width + 'px',
            'type': 'text',
            'visible': true,
            'sortable': false,
            'format': null,
            'handler': null
        });
    }
    
    ctor.prototype.__getFaultData = function() {
    	
    	if (!this.__faultId || !this.__fleetId) {
    		return;
    	}
    	
    	var me = this;
    	
    	nx.rest.event.eventDetail(this.__fleetId, this.__faultId, function(response) {
    		me.__event = response.fields;
    		var tableData = [];
    		var recoveryStatus = me.__event.recoveryStatus;
    		var eventCreateTime = new Date(me.__event.createTime).toLocaleString('en-GB').replace(/\,/g,'');
    		
    		if (!!recoveryStatus) {
    			var current = "N";
    			if (!!me.__event.current) {
    				current = "Y";
    			}
    			
    			tableData.push({
                	row: 0,
                    id: 0, 
                    data: [
                           me.__event.headcode,
                           eventCreateTime,
                           me.__event.description,
                           me.__event.eventCode,
                           me.__event.category,                           
                           current
                           ]
                });
    			me.__globalFaultTime = me.__event.createTime;
    		}
    		
    		me.__faultGrid.setData(tableData);
    		
    		if (recoveryStatus == "Completed") {
    			me.__recoveryCompleted = true;
    		}
    		
    		if (!!recoveryStatus) {
    			me.__isRecoveryActive();
    		}
    	});
    }
    
    // To be used when the unit is changed in the unit formation
    ctor.prototype.__clearFaultData = function(){
    	var me = this;
    	var tableData = [];
    	
    	tableData.push({
        	row: 0,
            id: 0, 
            data: [
                   '-',
                   null,
                   '-',
                   '-',
                   '-',                           
                   '-'
                   ]
        });
    	
    	document.getElementById('worstCaseScenarioBox').value = '';
    	me.__faultGrid.setData(tableData);
    }
    
    ctor.prototype.__clearRecoveryData = function(){       
        // clear recovery body & deactivate buttons
        this.__rowIndex = 0;
    	$("#recoveryDetailsGrid").children("tbody").remove();
    	this.__deactivateButton("#completeRecoveryButton");
    	this.__deactivateButton("#restartRecoveryButton");
    	$("#timer").remove();
    }
    
    ctor.prototype.__startRecovery = function(event) { 	
    	var me = this;
    	
    	nx.rest.recovery.execute(this.__fleetId, event.eventCode, event.id, function(response) {
    		me.__contextId = response;
    		me.__getNextStep();
    	});
    }
    
    ctor.prototype.__getNextStep = function() {
    	var me = this;
    	
    	nx.rest.recovery.nextStep(this.__fleetId, this.__contextId, function(recoveryStep) {
        	me.__rowIndex++;
        	
            if(recoveryStep.type == 1) {
                me.__createOutcomeRow(recoveryStep.outcomeAction);
            } else {        	
            	nx.rest.recovery.worstCaseScenario(me.__fleetId, me.__contextId, function(response){
            		me.__setWorstCaseScenario(response);
            	});
                
                me.__createRecoveryDetailRow(recoveryStep);
            }
    	});
    }
    
    ctor.prototype.__setWorstCaseScenario = function(wcs) {
    	document.getElementById('worstCaseScenarioBox').value = wcs;
    }
    
    ctor.prototype.__createRecoveryDetailRow = function(recoveryStep) {
    	var me = this;
    	
    	this.__currentAnswers = [];
    	this.__selectedAnswer = null;
    	this.__answerButtonIds = [];
    	this.__recoveryNotesId = null;
    	this.__currentRecoveryRow = "<tr><td>";
    	
    	var questionBox = "";
    	var url = recoveryStep.questionUrl;
    	if (url != null && url != "") {
    	    questionBox = "<textarea class = 'recoveryDetailsBox' readonly style='color:#0000EE; text-decoration:underline;'>" + recoveryStep.question + "</textarea>";
    		questionBox = "<a href=\"" + url + "\" target=\"_blank\">" + questionBox + "</a>";
    	} else {
    	    questionBox = "<textarea class = 'recoveryDetailsBox' readonly >" + recoveryStep.question + "</textarea>";
    	}
    	
    	this.__currentRecoveryRow += questionBox;
    	
    	this.__currentRecoveryRow += "</td><td>";
    	$.each(recoveryStep.answers, function(index, currentAnswer) {
    		var answer = currentAnswer.answer;
    		me.__currentAnswers.push(currentAnswer);
    		var buttonId = "answerButton" + me.__rowIndex + "_" + index;
    		me.__answerButtonIds.push(buttonId);
    		var button = "<div id = " + buttonId + " class='ui-widget-header recoveryDetailsButton'><span>" +
    		    answer + "</span></div>";
    		me.__currentRecoveryRow += button;
    	});
    	this.__currentRecoveryRow += "</td><td></td><td></td>";
    	
    	this.__recoveryNotesId = "recoveryNotes" + me.__rowIndex;
    	this.__currentRecoveryRow += "<td><textarea id = " + this.__recoveryNotesId +
    	        " class = 'recoveryDetailsBox'></textarea></td></tr>";
    	
    	this.__recoveryGrid.append(this.__currentRecoveryRow);
    	
    	$.each(this.__answerButtonIds, function(index, buttonId) {
    		$('#' + buttonId).click(function(e) {
    			me.__selectedAnswer = me.__currentAnswers[index];
    			me.__disableButtons(me.__answerButtonIds);
    			me.__setButtonColor(buttonId);
    			if (me.__rowIndex == 1) {
    				me.__activateButton("#restartRecoveryButton");
    			}
                me.__onAnswerButtonExecute(me.__selectedAnswer);
            });
    	});
    }
    
    ctor.prototype.__createOutcomeRow = function(outcome) {
    	
    	this.__currentRecoveryRow = "<tr>";
    	var outcomeBox = "<td><textarea class = 'recoveryDetailsBox' readonly>" + outcome + "</textarea></td>";
    	this.__currentRecoveryRow += outcomeBox;
    	this.__currentRecoveryRow += "<td></td><td></td><td></td><td></td></tr>";
    	this.__recoveryGrid.append(this.__currentRecoveryRow);
    	
    	this.__setWorstCaseScenario(outcome);
    	
    	this.__activateButton("#completeRecoveryButton");
    }
    
    ctor.prototype.__onAnswerButtonExecute = function(answer) {
    	var me = this;
    	
    	if (!!answer.action) {
    		var actionBox = "<textarea class = 'recoveryDetailsBox' readonly>" + answer.action + "</textarea>";
    		var actionCell = document.createElement('div');
    		actionCell.innerHTML = actionBox;
    		var url = answer.actionUrl;
    		var action = actionCell;
    		if (url != null && url != "") {
    			var linkableActionCell = document.createElement('a');
    			linkableActionCell.setAttribute('href', url);
    			linkableActionCell.setAttribute('target', '_blank');
    			linkableActionCell.appendChild(actionCell);
    			action = linkableActionCell;
    		}
    		var currentRow = document.getElementById("recoveryDetailsGrid").rows[this.__rowIndex];
    		currentRow.cells[2].appendChild(action);
    		
    		this.__responseButtonIds = [];
    		var workedButton = document.createElement('div');
    		var workeButtonId = "workedButton" + this.__rowIndex;
    		this.__responseButtonIds.push(workeButtonId);
    		workedButton.id = workeButtonId;
    		workedButton.className = "ui-widget-header recoveryDetailsButton";
    		workedButton.innerHTML = $.i18n("Worked");
    		currentRow.cells[3].appendChild(workedButton);
    		var didntWorkButton = document.createElement('div');
    		var didntWorkButtonId = "didntWorkButton" + this.__rowIndex;
    		this.__responseButtonIds.push(didntWorkButtonId);
    		didntWorkButton.id = didntWorkButtonId;
    		didntWorkButton.className = "ui-widget-header recoveryDetailsButton";
    		didntWorkButton.innerHTML = $.i18n("Didn't Work");
    		currentRow.cells[3].appendChild(didntWorkButton);
    		
        	$.each(this.__responseButtonIds, function(index, buttonId) {
        		$('#' + buttonId).click(function(e) {
        			me.__disableButtons(me.__responseButtonIds);
        			me.__setButtonColor(buttonId);
                    me.__onResponseButtonExecute(index);                    
                });
        	});
    		
    	} else {
    		var notes = this.__getNotesText();
    		nx.rest.recovery.setStepOutcome(this.__fleetId, this.__contextId, this.__faultId, answer.answerId,
    				answer.answer, answer.action, false, notes, this.__recoveryCompleted, function(response){
    			me.__getNextStep();
        	});
    	}
    }
    
    ctor.prototype.__onResponseButtonExecute = function(index) {
    	var me = this;
    	
    	var worked = true;
    	if (index > 0) {
    		worked = false;
    	}
    	
    	var notes = this.__getNotesText();
    	nx.rest.recovery.setStepOutcome(this.__fleetId, this.__contextId, this.__faultId,
    			this.__selectedAnswer.answerId, this.__selectedAnswer.answer, this.__selectedAnswer.action,
    			worked, notes, this.__recoveryCompleted, function(response) {
			me.__getNextStep();
    	});
    }
    
    ctor.prototype.__disableButtons = function(buttonIds) {
    	var me = this;
    	
    	$.each(buttonIds, function(index, buttonId) {
    		$('#' + buttonId).unbind();                    
    	});
    }
    
    ctor.prototype.__setButtonColor = function(buttonId) {
    	$('#' + buttonId).addClass("orangeButton")
    	    .removeClass("ui-widget-header");  	
    }
    
    ctor.prototype.__completeRecovery = function() {
    	var me = this;
    	
    	nx.rest.recovery.completeRecovery(this.__fleetId, this.__contextId, this.__faultId, function(response) {
    		if (response > 0) {
    			me.__recoveryCompleted = true;
    			me.__deactivateButton("#completeRecoveryButton");
    		} else {
        		$('body').append("<div id='notificationPopup'><p>Impossible to complete recovery.</p></div>");
               	$('#notificationPopup').dialog({
           			autoOpen : true,
           			height : 100,
          			width : 280,
          			modal : true,
                	resizable: false,
                	title : 'Error - Unable to complete repository',
                	close : function() {
                	    $('#notificationPopup').remove();
                    }
                }); 
    		}
    	});
    }
    
    ctor.prototype.__resumeRecovery = function() {
    	var me = this;
    	
    	nx.rest.recovery.resumeRecovery(this.__fleetId, this.__contextId, function(response) {
        	$.each(response.workItems, function(index, workItem) {
        		var activity = response.activities[index];
        		var step = activity.recoveryStepConfig;
        		
        		if(!!step) {
        			me.__rowIndex++;
        			me.__createRecoveryDetailRow(step);
        			me.__setNotesText(workItem.notes);
        			$.each(step.answers, function(i, answer) {
        				if (answer.answerId == workItem.answerId) {
        					me.__replayAnswer(answer, workItem.worked);
        				}
        			});
        		}
        	});
    		
        	var lastActivity = response.activities[response.activities.length - 1];
        	if (!!lastActivity.recoveryStepConfig) {
        		me.__rowIndex++;
        		me.__createRecoveryDetailRow(lastActivity.recoveryStepConfig);
        	} else if (!!lastActivity.outcomeConfig) {
        		me.__rowIndex++;
        		me.__createOutcomeRow(lastActivity.outcomeConfig.action);
        	}
        	nx.rest.recovery.worstCaseScenario(me.__fleetId, me.__contextId, function(response){
        		me.__setWorstCaseScenario(response);
        	});
    	});
    }
    
    ctor.prototype.__replayAnswer = function(answer, worked) {
    	this.__selectedAnswer = answer;
    	this.__disableButtons(this.__answerButtonIds);
    	this.__setButtonColor(this.__answerButtonIds[answer.answerId - 1]);
    	if (this.__rowIndex == 1) {
    		this.__activateButton("#restartRecoveryButton");
    	}
    	if (!!answer.action) {
    		this.__onAnswerButtonExecute(answer);
    		this.__replayResponse(worked);
    	}
    }
    
    ctor.prototype.__replayResponse = function(worked) {
    	var buttonId = this.__responseButtonIds[0];
    	if (!worked) {
    		buttonId = this.__responseButtonIds[1];
    	}
    	this.__disableButtons(this.__responseButtonIds);
    	this.__setButtonColor(buttonId);
    }
    
    ctor.prototype.__isRecoveryActive = function() {
    	var me = this;
    	
    	nx.rest.recovery.isActive(this.__fleetId, this.__faultId, function(response) {
    		if (!!response) {
    			me.__contextId = response;
    			me.__resumeRecovery();
    		} else {
    			me.__startRecovery(me.__event);
    		}
    	});
    }
    
    ctor.prototype.__getNotesText = function() {
    	var text = $('#' + this.__recoveryNotesId).val();
    	var lastChar = text.substring(text.length - 1, text.length);
    	if (lastChar == "?") {
    		text = text.substring(0, text.length - 1);
    	}
    	return text;
    }
    
    ctor.prototype.__setNotesText = function(text) {
    	$('#' + this.__recoveryNotesId).attr("value", text);
    }
    
    ctor.prototype.__activateButton = function(buttonId) {
    	$(buttonId).removeClass("ui-state-disabled").removeAttr('disabled');
    }
    
    ctor.prototype.__deactivateButton = function(buttonId) {
    	$(buttonId).addClass("ui-state-disabled").attr('disabled', 'disabled');
    }
    
    ctor.prototype.__restartRecovery = function() {
    	this.__rowIndex = 0;
    	$("#recoveryDetailsGrid").children("tbody").remove();
    	this.__deactivateButton("#completeRecoveryButton");
    	this.__deactivateButton("#restartRecoveryButton");
    	this.__startRecovery(this.__event);
    }
    
    return ctor;
}());
