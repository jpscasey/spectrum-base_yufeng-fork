var nx = nx || {};
nx.unit = nx.unit || {};

nx.unit.StockTabs = (function() {
    function ctor(el, tabsRight, displaySingleTab) {
        var me = this;
        this.el = el;
        this.tabsRight = tabsRight || false;
        this.displaySingleTab = displaySingleTab;
        this.addingTabs = false;
        this.tabCount = 0;
        this.stockId = null;
        this.stockPanel = null;
        this.stockList = [];
        
        if (!!tabsRight) {
            this.el.addClass('ui-tabs-right');
        }
        
        this.el.empty().append('<ul>');
        this.el.tabs({
            'show': function(event, ui) {
                if (me.addingTabs == false) {
                    var hash = ui.tab.hash.slice(1);
                    var stockId = parseInt(hash);
                    me.stockId = stockId;
                    me.stockPanel = ui.panel;
                    nx.hub.stockChanged.publish(stockId, ui.panel);
                }
            },
            'tabTemplate': '<li id="tab#{label}"><a href="#{href}"><span>#{label}</span></a></li>'
        });
    };
    
    ctor.prototype.clear = function() {
        while ((stock = this.stockList.pop()) != undefined) {
            this.el.tabs('remove', '#' + stock.id);
        }
    };
    
    ctor.prototype.getSelectedStockId = function() {
        return this.stockId;
    };
    
    ctor.prototype.getSelectedTabEl = function() {
        return this.stockPanel;
    };

    ctor.prototype.setStock = function(stockList) {
        this.addingTabs = true;
        this.stockList = stockList;
        
        if (this.tabsRight) {
            stockList = stockList.reverse();
        }
        
        for (var i = 0; i < this.tabCount; i++) {
            this.el.tabs('remove', 0);
        }
        
        for (var i = 0, l = stockList.length; i < l; i++) {
            var stock = stockList[i];
            this.el.tabs('add', '#' + stock.id, stock.description);
        }
        if (stockList.length == 1 && !this.displaySingleTab) {
            $('#tab' + stockList[0].description).hide();
        }
        
        // Fix the tab height.. The default CSS (for full height tabs) assumes that
        // there will only be one row of tabs. Need to add one px here for some reason
        this.el.children('div').css('top', (this.el.children('ul').height() + 1) + 'px');
        
        //The div should have left to right direction (just the tabs are inverted)
        this.el.children('div').css('direction', 'ltr');
        
        this.tabCount = stockList.length;
        this.addingTabs = false;
        
        // Select the first tab
        if (stockList.length > 0) {
            var selected = this.tabsRight ? stockList.length - 1 : 0;
            this.stockId = stockList[selected].id;
            this.stockPanel = this.el.find('#' + this.stockId);
            
            this.el.tabs('select', selected);
        }
    };
    
    ctor.prototype.showAllTabs = function() {
    	for(index in this.stockList) {
    		$('#tab' + this.stockList[index].description).show();
    	}
    };
    
    return ctor;
})();