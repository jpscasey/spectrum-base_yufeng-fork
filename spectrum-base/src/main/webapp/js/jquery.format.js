/**
 * @author Paul O'Riordan
 */
(function( $ ) {
	var booleanFormat = function(value, format) {
    	if (format == null) {
    		return value.toString();
    	} else {
    		var values = format.split(/\|/);
    		return (value === true) ? $.trim($.i18n(values[0])) : $.trim($.i18n(values[1]));
    	}
    };
	
	var numberFormat = function(number, format) {
    	if (format == null) {
    		return number;
    	} else {
    		format = format.replace(/^#.*?(\.|$)/, '[0-9]+$1');
    		format = format.replace(/\./g, '\.');
    		format = format.replace(/#/g, '[0-9]');
    		format = new RegExp(format, 'g');
    		return String(number).match(format)[0];
    	}
    };
    
    var timeAgoFormat = function(timestamp) {
    	
    	return nx.util.timeAgoFormat(timestamp);
        
    };
    
    /**
     * Format a timestamp using the pattern passed in parameter`.
     * If no pattern is specified the default one is 'DD/MM/YYYY HH:mm:ss'
     * Here the list of supported pattern letters :
     * Y : year
     * M : month
     * D : day
     * H : hour
     * m : minute
     * s : second
     * S : milliseconds
     * 
     */
    var timestampFormat = function(timestamp, format) {
        
        if (format == null) {
            format = 'DD/MM/YYYY HH:mm:ss';
        }
        
        if (timestamp == null || timestamp == undefined) {
            return "";
        }
        
        var format2Digits = function(val) {
            if (val < 10) {
                return '0' + val;
            }
            
            return val;
        };

        var format3Digits = function(val) {
            var newVal = val;
            if (val < 100) {
                if (val < 10) {
                    newVal = '00' + newVal;
                }
                else {
                    newVal = '0' + newVal;
                }
            }
            
            return newVal;
        };

        var date = new Date(timestamp);
        
        var formats = [
                       [/YYYY/, function(date) { return date.getFullYear() }],
                       [/YY/, function(date) { var year = date.getFullYear().toString(); return year.substring(2) }],
                       [/MM/, function(date) { return format2Digits(date.getMonth()+1)}],
                       [/DD/, function(date) { return format2Digits(date.getDate())}],
                       [/HH/, function(date) { return format2Digits(date.getHours())}],
                       [/mm/, function(date) { return format2Digits(date.getMinutes())}],
                       [/ss/, function(date) { return format2Digits(date.getSeconds())}],
                       [/SSS/, function(date) { return format3Digits(date.getMilliseconds())}]
                   ];
        
        for (var k in formats) {
            format = format.replace(formats[k][0], formats[k][1](date));    
        }
                        
        return format;
    };
    
    var yesNoFormat = function(value) {
        return (value != null && value != false) ? "Y" : "N";
    };
	
	var stringFormat = function(value) {
		return $.i18n(value);    
	};
	
	jQuery.extend({
	    format: function() { 
	    	if (arguments.length <= 1) {
	    		if (arguments.length == 1) {
	    			return arguments[0] == null ? '' : arguments[0];
	    		} else {
	    			return '';
	    		}
	        }
	        
	        var re = /{(.*?)}/m,
	            match = null,
	            input = String(arguments[0]),
	            i = 1;
	        
	        while ((match = re.exec(input)) != null ) {
	            var fmt = $.trim(match[1]);
	            
	            var value = arguments[i];
	            // if only one parameter is passed and the format contains multiple {(.*?)} instances
	            // replace them all with the same parameter
	            if (value == null && arguments.length == 2) {
	                value = arguments[1];
	            }
	
	            if (fmt.length == 0) {
	                input = input.replace(re, arguments[i] || null);
	            } else {
	                var split = fmt.split(":");
	                var ftype = split[0] != null ?  $.trim(split[0]) : null;
	                var fstr = null;
	                if (split[1] != null) {
	                    split.shift();
	                    fstr = $.trim(split.join(':'));
	                }
	
	                if (ftype == "date") {
	                    value = timestampFormat(value, fstr);
	                } else if (ftype == "timeago") {
	                	value = timeAgoFormat(value);
	                } else if (ftype == "number") {
	                	value = numberFormat(value, fstr);
	                } else if (ftype == "boolean") {
	                	value = booleanFormat(value, fstr);
	                } else if (ftype == "yes/no") {
	                    value = yesNoFormat(value);
	                } else if (ftype == "text") {
	                    value = stringFormat(value);
	                }
	                
	                input = (!!value? input.replace(re, value || null) : value);
	            }
	            
	            i++;
	        }
	
	        return input;
	    }
	});
})(jQuery);
