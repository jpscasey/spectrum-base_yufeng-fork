var nx = nx || {};
nx.unit = nx.unit || {};

nx.unit.Maps = (function() {
    function ctor(us) {
        nx.unit.TimeAware.call(this, us);
        
        var me = this;
        
        this.__unitSummary = us;
        this.__conf = null;
        this.__frame = null;
        this.__inProgress = true;
        
        this.__unitId = null;
        this.__timestamp = null;
        this.__toolbarConf = null;
        
        $.when(mapReady).done(function() {
        	me.__init(us);
        });
    };
    
    /** Extend nx.unit.TimeAware */
    ctor.prototype = new nx.unit.TimeAware();
    
    /** Extend nx.unit.Tab */
    ctor.prototype = new nx.unit.Tab();

    ctor.prototype.__init = function(us) {
        var me = this;
        nx.rest.map.conf(function(response) {
            me.__conf = response;
            me.__initialize(me.__conf);
        });
    }
    
    ctor.prototype.__initialize = function(conf) {
        var me = this;
        
        var defaultView = this.__getDefaultView(conf);
        var detailDefaultView = this.__getDetailDefaultView(conf);
        var detailList = this.__getDetails(conf);
        var trainIcons = this.__getTrainIcons(conf);
        var trainLabel = this.__getTrainLabel(conf);

        this.__toolbarConf = {
            'container': $('#mapToolbar'),
            'route': { 'visible': true },
            'station': { 'visible': true },
            'train': { 'visible': true },
            'trainLabel': { 'visible': true },
            'searchUnit': false,
            'defaultView': defaultView,
            'filters': conf.filters,
            'clearApplyButtons': conf.showClearApplyButtons,
        };

        var mapConf = {
            //bing maps requires DOM element instead a JQuery Object
            'container': $('#map')[0],
            'defaultView': defaultView,
            'followMode': true,
            'mapMarkersProvider': conf.mapMarkersConfiguration,
            'trainIcons': trainIcons,
            'trainLabel': trainLabel
        };

        var detailConf = {
            'container': $('#mapInfoOverlay'),
            'defaultView': detailDefaultView,
            'maxHeight': $('#map').height(),
            'detailList': detailList
        };
        
        var frameConf = {
            'singleUnitMode': true,
            'timePicker': $('#timePicker')
        };
        
        this.__frame = new nx.maps.Frame(frameConf, this.__toolbarConf, mapConf, detailConf);

        this.__inProgress = false;
        
        this._update({
            'unitId': this.__unitId,
            'fleetId': this.__fleetId,
            'timestamp': this.__timestamp,
            'unitChanged': function() {
                return false;
            }
        });
        
      //loads the stations/routes
        window.setTimeout(function() {
            nx.rest.map.licencedConf(function(response) {
                var namedViews = me.__getNamedViews(response.namedViews);
                me.__frame.setNamedViews(namedViews);    
                
                var stations = me.__getStations(response.stations);
                me.__frame.setStations(stations);
                
                var routes = me.__getRoutes(response.routes);
                me.__frame.setRoutes(routes);              
            });
        }, 500);
    };

    ctor.prototype.__getDefaultView = function(conf) {
        return {
            'id': conf.defaultView.id,
            'type': conf.defaultView.mapType,
            'desc': conf.defaultView.description,
            'northWest': {
                'latitude': conf.defaultView.northWest.latitude, 
                'longitude': conf.defaultView.northWest.longitude
            },
            'southEast': {
                'latitude': conf.defaultView.southEast.latitude, 
                'longitude': conf.defaultView.southEast.longitude
            }
        };
    };

    ctor.prototype.__getDetailDefaultView = function(conf) {
        return {
            'latitude': conf.defaultDetailView.center.latitude,
            'longitude': conf.defaultDetailView.center.longitude,
            'zoom': conf.defaultDetailView.zoom
        };
    };
    
    ctor.prototype.__getStations = function(stations) {
        var result = [];
        
        for (var index in stations) {
            var station = stations[index];
            result.push({
                'name': station.name,
                'code': station.code,
                'coordinate' : {
                    'latitude': station.coordinate.latitude,
                    'longitude': station.coordinate.longitude
                },
                'type': station.type
            });
        }
        
        return result;
    };

    ctor.prototype.__getNamedViews = function(namedViews) {
        var result = [];
        
        for (var index in namedViews) {
            var namedView = namedViews[index];
            result.push({
                'id': namedView.id,
                'type': namedView.mapType,
                'desc': namedView.description,
                'northWest': {
                    'latitude': namedView.northWest.latitude,
                    'longitude': namedView.northWest.longitude
                },
                'southEast': {
                    'latitude': namedView.southEast.latitude,
                    'longitude': namedView.southEast.longitude
                }
            });
        }
        
        return result;
    };

    ctor.prototype.__getRoutes = function(routes) {
        var result = [];
        
        for (var index in routes) {
            var route = routes[index];

            var coordinates = [];
            for (var i in route.points) {
                var coordinate = route.points[i];
                coordinates.push({
                    'latitude': coordinate.latitude,
                    'longitude': coordinate.longitude
                });
            }
            
            result.push({
                'coordinate' : coordinates
            });
        }
        
        return result;
    };
    
    ctor.prototype.__getDetails = function(conf) {
        var details = [];
        
        for (var index in conf.details) {
            var detail = conf.details[index];
            
            details.push({
                'label': detail.label,
                'name': detail.name,
                'ref': detail.referencedField,
                'format': detail.formatter,
                'fleets': detail.fleets
            });
        }
        
        return details;
    };

    ctor.prototype.__getTrainIcons = function(conf) {
        var icons = [];
        
        for (var index in conf.trainIcons) {
            var icon = conf.trainIcons[index];
            
            icons.push({
                'severity': icon.severity,
                'image': icon.image,
                'name': icon.name,
                'zIndex': icon.zIndex
            });
        }
        
        return icons;
    };
    
    ctor.prototype.__getTrainLabel = function(conf) {
        var trainLabel = conf.trainLabel;
        
        var result = {
            'pointer': trainLabel.pointer,
            'xOffset': trainLabel.xOffset,
            'yOffset': trainLabel.yOffset,
            'height': trainLabel.height,
            'width': trainLabel.width
        }
        
        var title = trainLabel.title;
        if (!!title) {
            result['title'] = title;
        }
        
        var desc = trainLabel.description;
        if (!!desc && desc.length > 0) {
            description = [];
            
            for (var index in desc) {
                var d = desc[index];
                
                var item = {
                    'label': d.label
                }
                
                if (!!d.value) {
                    item['value'] = d.value;
                }
                
                if (!!d.format) {
                    item['format'] = d.format;
                }
                
                if (!!d.constant) {
                    item['constant'] = d.constant;
                }
                
                description.push(item);
            }
            
            result['description'] = description;
        }
        
        return result;
    }
    
    ctor.prototype.__refresh = function(data, unitChanged) {
        this.__frame.update(data, unitChanged);
    };

    ctor.prototype._logging = function() {
        //logs the unit summary - map access
        nx.rest.logging.unitMap();
    };
    
    ctor.prototype._hasTimeController = function() {
        return true;
    };
    ctor.prototype._onHide = function() {
        nx.main.removeSettingButton();
    };
    ctor.prototype._onShow = function() {
        if(nx.main.__settingsButton == null) {
            if(!!this.__frame) {
                this.__frame.__toolbar.addSettings(this.__frame, this.__toolbarConf, false);
            }
        }
    };
    ctor.prototype._update = function(data) {
        this.__unitId = data.unitId;
        this.__timestamp = data.timestamp;
        this.__fleetId = data.fleetId;

        if (!data.unitId || !data.timestamp || !data.fleetId) {
            return;
        }

        if (!this.__inProgress) {
            this.__inProgress = true;
            
            var me = this;
            
            var timestamp =  data.timestamp;
            
            // FIXME fleetId should be first parameter
            nx.rest.map.unit(data.fleetId, data.unitId, timestamp, function(response) {
                // FIXME does this work if there is no route map??
            
                me.__inProgress = false;
                var unitChanged = false;
                
                if (!$('#routeMap').is(":visible")) {
                    unitChanged = data.unitChanged();
                }
                
                me.__refresh(response, unitChanged);
                
                var obj = new Object();
                obj.data = [response];
                // Send an event to notify new datas
                $.event.trigger({
                    type: "mapUpdate",
                    message: obj
                });
            });
        }
    };

    return ctor;
})();

