var nx = nx || {};
nx.event = nx.event || {};
/**
 * nx.event
 * 
 * A collection of event utilities
 */
nx.event.Event = (function() {
    function ctor () {
        var me = this;
        me.__faultTable;
        me.__advanced = false;
        // the datatable column list
        me.__columnList = []; 
        // the column descriptions as they are received from the server.
        me.__columns = [];
        me.__advancedButton = $("#advancedButton");
        me.__faultTableDiv = $("#faultTable");
        me.__advancedSearch = $(".advancedSearch");
        me.__eventHeader = $(".eventHeader");
        me.__additionalFields = [];
        me.__backgroundClasses = new Object(); 
        me.__backgroundColors = 0;
        // Number of events per page
        me.__eventsPerPage = 30;
        me.__pageSelected = 0;
        me.__timePickerFrom = $('#timePickerFrom').timepicker();
        me.__timePickerTo = $('#timePickerTo').timepicker();
        // Predefined filters
        me.__isDefinedFiltersVisible = false;
        me.__showDefinedFilters = false;
        me.__definedFilterPanel;
        me.__definedFilters = [
        	
        ];
        // group button
        me.__isGroupButtonVisible = false;
        me.__checkedRecords = {};
        me.__groupFaultsInTable = false;
        me__defaultSortPosition=0;
        nx.rest.event.eventHistoryConf(function(response) {
            me.__searchLimit = response.searchLimit;
            
            me.__exportLimit = response.exportLimit;
        	
        	me.__initAdditionalFields(response.additionnalFields, response.additionalDropDownValues, me);
        	//load the selects
            me.__initDropdownValues(response.additionnalFields, response.additionalDropDownValues, me);
            
            me.__initEventColumns(response.eventColumns, me);
            
            // initialise defined filters menu
            me.__isDefinedFiltersVisible = response.definedFiltersVisible;
            if(!!me.__isDefinedFiltersVisible) {
            	me.__definedFilters = response.definedFilters;
            	me.__createDefinedFiltersPanel();
            }
            me__defaultSortPosition= response.defaultSortColumn;
            // initialise defined filters menu
            me.__isGroupButtonVisible = response.groupButtonVisible;
            me.__groupFaultsInTable = response.displayGroupsInBlock;

            me.__createButtons();

            $('.searchEventsButton').find('button').each(function () {
                $(this).button({disabled: false});
            });
            
            $('.exportEventsButton').find('button').each(function () {
                $(this).button({disabled: false});
            });
            
            nx.main.checkExtraActions();
        });
        
        
        //SearchInputs
        this.__startTimestamp = this.__timePickerFrom.data('obj');
        this.__endTimestamp = this.__timePickerTo.data('obj');
        this.__live = $("input[name='liveRadio']"); 
        this.__keyword = $('#keyWordEventSearch');
        this.__desc = $('#faultDesc');
        this.__initTimepickers();
        //this.__createButtons();
        this.__init();
        
    };
    
    ctor.prototype.__initTimepickers = function() {
    	var me = this;
    	var date = $.format('{date : YYYY/MM/DD}', new Date());
        me.__startTimestamp.setTime(new Date(date + " 00:00:00"));
        me.__endTimestamp.setTime(new Date(date + " 23:59:59"));
    }
    
    ctor.prototype.__numOfAdvancedSearchRows = function() {
    	return 4;
    }
    
    ctor.prototype.__initAdditionalFields = function(fields,dropdownfields, me) {
        me.__additionalFields = fields;
        var rows = 0;
        
        var addFieldsEl = $("#additionalSearchFields");
        
        var numberRows = this.__numOfAdvancedSearchRows();
        var currentDiv = null;
        for (var i = 0, l = fields.length; i < l; i++) {
            var field = fields[i];
            var input = null;
            
            var div = $('<div>')
                .addClass('labelledItem');
            
            var label = $('<label>')
                .addClass('searchLabel40')
                .addClass('labelSelect')
                .attr('for', 'uniform-'+field.name)
                .text($.i18n(field.label));
            
            if (rows % numberRows == 0 || (rows >= 2 && fields[i].type == "multiple_select")) {
            	rows = 0;
                addFieldsEl.append(currentDiv);
                currentDiv = $('<div>')
                    .addClass('fieldGroup');
            }

            switch (field.type)
            {
	            case "input_text" : { 
	            	input = $('<input>')
	                .attr('type', 'text')
	                .attr('size', field.width)
	                .attr('name', field.name)
	                .attr('id', field.name);
	            	rows ++;
	            }break;
	            case "select" : { 
	            	input = $('<select>')
	    	        .attr('id', field.name)
	    	        .attr('name', field.name)
	    	        .attr('width', field.width)
	    	        .append('<option />');
	            	rows++;
	            }break;
	            case "multiple_select" : { 
	            	input = $('<select multiple>')
	    	        .attr('id', field.name)
	    	        .attr('name', field.name)
	    	        .attr('width', field.width)
	    	        .attr('style', 'background-color: white !important; padding: 0px !important')
			        .append('<option style="background-color: white !important" />');
	            	rows = rows + 3;
	            }break;
	            case "autocomplete" : {
	            	input = $('<input>')
	            	.attr('type', 'text')
	                .attr('size', field.width)
	                .attr('name', field.name)
	                .attr('id', field.name);
	            	
	            	var values = dropdownfields[field.name];
	            	var labelValues = [];
	            	var labelSet = {};
	            	// add the labels only
	            	for (var val in values) {
	            		var labelVal = values[val].label;
	            		// filter out the duplicated value
		            	if (labelSet[labelVal]) {
							continue;
						}
						labelSet[labelVal] = true;
	            		labelValues.push(values[val].label);
	            	}
	            	
	            	input.autocomplete({
	            		delay: 0,
	            		source: labelValues
	            	});
	            	
	            	rows ++;
	            }break;
            }
            
            div.append(label).append(input); 
            currentDiv.append(div);
        
        }
        if (!!currentDiv && currentDiv.children().size() > 0) {
            addFieldsEl.append(currentDiv);
        }
            
    };
    
    ctor.prototype.__initEventColumns = function(columns, me) {
        me.__columns =  [];
        
        for (var i = 0; i < columns.length; i++) {
            var column = columns[i];
            
            me.__columns.push({
                'name': column.name,
                'text': column.header, // FIXME... if not for this property - conversion would not be needed
                'width': column.width + 'px',
                'type': column.type,
                'visible': column.visible,
                'sortable': column.sortable,
                'resizable' : column.resizable,
                'autosize' : column.autosize,
                'format': column.format,
                'handler': column.handler,
                'cssClass': column.cssClass
            });
        }

        me.__faultTable = $('#faultTable').tabular({
            'columns': me.__columns,
            'cellClickHandler': (function(me) {
                return function(cell, data, columnDef, target) {
                    if (columnDef.type == 'checkbox') {
                        var checkbox = cell.find('input');
                        if ($(target).is(cell)) {
                            // check the box
                            checkbox.prop('checked', !checkbox.prop('checked'));
                        }
                        var isChecked = checkbox.prop('checked');
                        me.__faultTable.setCellValue(data.id, columnDef.name, isChecked);
                        if (!!isChecked) {
                            checkbox.addClass('tickedCheckbox');
                            var record = me.__faultTable.getRecordById(data.id);
                            if (!!record) {
                                me.__checkedRecords[data.id] = record;
                            }
                        } else {
                            checkbox.removeClass('tickedCheckbox');
                            delete me.__checkedRecords[data.id];
                        }
                        // if fault groups are enabled, select all members of the group
                        if(!!data.faultGroupId) {
                        	me.__toggleGroupChecked(data, columnDef, isChecked);
                        }

                        me.__validateGroupButton();
                    } else {
                        me.__openDetails(cell, data, columnDef);
                    }
                };
            })(me),
            'cellMouseOverHandler': (function(me) {
                return function(cell, data, columnDef) {
                    me.__mouseoverHandler(cell, data, columnDef);
                };
            })(me),
            'scrollbar': true,
            'fixedHeader': true,
            'renderOnlyVisibleRows' : true
        });

        // override tabular sort to keep grouped events together
        me.__faultTable.__sortModel = function() {
        	var column = this.__columns[this.__sortPosition];
            
            var separatedLists = [];
            if (me.__groupFaultsInTable === true) {
            	separatedLists = this.removeGrouped(this.__model);
            	this.__model = separatedLists[0];
            }
            
            if (!column.type) {
            	this.__sortModelString();
            } else if (column.type == 'number' || column.type.toUpperCase() == 'ANALOGUE' || column.type.toUpperCase() == 'DIGITAL' || column.type == 'timestamp') {
            	this.__sortModelNumber();
            } else if (column.type == 'duration') {
            	this.__sortDuration();
            } else {
            	this.__sortModelString();
            }
            
            if (me.__groupFaultsInTable === true) {
            	// add any faults removed from the sort back into the results
            	for (var index in separatedLists[1]) {
            		var row = separatedLists[1][index];
            		var groupId = row.faultGroupId;
            		var groupLeadPresent = this.isGroupLeadPresent(groupId);
            		var groupLeadIndex = this.findGroupLead(groupId, groupLeadPresent);
            		
            		this.__model.splice(groupLeadIndex + 1, 0, row);
            	}
            }
            
            this.__rebuildIndex(this.__model);
    	};
    	
    	// helper function for override sort: 
    	// remove faults w/ group ID but not group lead (as long as group lead is present in the results)
        me.__faultTable.removeGrouped = function(model) {
        	var rowsToIgnore = [];
        	var rowsToSort = [];
        	var groupsFound = [];
        	
        	for (var index in model) {
        		var row = model[index];
        		var groupLeadPresent = this.isGroupLeadPresent(row.faultGroupId);
        		if (row.faultGroupId != null && (row.isGroupLead == 0 || row.isGroupLead == false) && groupLeadPresent == true) {
        			rowsToIgnore.push(row);
        		} else if (row.faultGroupId != null && (row.isGroupLead == 0 || row.isGroupLead == false) 
        					&& groupLeadPresent == false) {
        			if (groupsFound.indexOf(row.faultGroupId) == -1 ) {
	        			groupsFound.push(row.faultGroupId);
	        			rowsToSort.push(row);
        			} else if (groupsFound.indexOf(row.faultGroupId) > -1 ) {
        				rowsToIgnore.push(row);
        			}
        		} else {
        			rowsToSort.push(row);
        		}
        	}
        	
        	return [rowsToSort, rowsToIgnore];
    	};
    	
    	// helper function for override sort:
    	// get index of group lead to splice the others from the group back into the results AFTER sorting
    	me.__faultTable.findGroupLead = function(targetGroupId, groupLeadPresent) {
        	for (var i = 0; i < this.__model.length; i++) {
	    		var row = this.__model[i];
	    		if(groupLeadPresent == true && row.faultGroupId == targetGroupId && (row.isGroupLead == 1 || row.isGroupLead == true)) {
	    			return i;
	    		} else if (row.faultGroupId == targetGroupId && groupLeadPresent == false) {
	    			return i;
	    		}
	    	}
		};
		
		// helper function for override sort
		// check if the group lead is in the search results
		me.__faultTable.isGroupLeadPresent = function(targetGroupId) {
			var groupLeadIsPresent = false;
        	for (var i = 0; i < this.__model.length; i++) {
	    		var row = this.__model[i];

	    		if(row.faultGroupId == targetGroupId && (row.isGroupLead == 1 || row.isGroupLead == true)) {
	    			groupLeadIsPresent = true;
	    		}
	    	}

        	return groupLeadIsPresent;
		};
		
		me.__faultTable.initialSort = function() {
			  var me = this;
            //If no column to sort on has been selected, we default to createTime
            if (me.__sortPosition == null) {
                this.__sortPosition = me__defaultSortPosition;
                // perform the sort
                this.__sortDesc = true;
                this.__model = this.__fullDataList;
                this.__sortModel();
                this.__fullDataList = this.__model;
                this.redraw();
            }
		};
    };
    
    ctor.prototype.__initDropdownValues = function(fields, dropdownlists, me) {
    	
    	for (var i = 0, l = fields.length; i < l; i++) {
            var field = fields[i];
            var comboName = field.name;
            
            if (field.type == "select" || field.type == "multiple_select") {
            	for (var j in dropdownlists[comboName]) {
                    var option = dropdownlists[comboName][j];
                    if (option.code !== -1){
	                    $("#"+comboName).append($("<option style='background-color: white !important' />")
	                    	.val(option.code).text($.i18n(option.label)));
                    } else {
                    	$("#"+comboName).append($("<option style='background-color: white !important' />")
    	                    .val(option.label).text($.i18n(option.label)));
                    }
                }
        		$("#"+comboName).uniform({selectAutoWidth : false});
            }
            
    	}
    };
    
    ctor.prototype.__createButtons = function() {
        var me = this;
        
        // Search Button
        var searchButton = $('<button id="searchEvents" title = "'+$.i18n("Search Events")+'">'+$.i18n("Search")+'</button>').button({
        	icons: {
                secondary: 'ui-icon-search'
            },
            disabled: true
        }).addClass('ui-button-primary')
        .click(function(e) {
            me.__doSearch();
        });
        
        var idsToFilter = '';
        me.__additionalFields.forEach( function (arrayItem)
                {
                    idsToFilter += '#' + arrayItem.name + ', ';
        });
        idsToFilter += '#keyWordEventSearch';
        $(idsToFilter).keypress(function(e){
            if(e.which == 13){ //Enter key pressed with text box in focus
                me.__doSearch();
            }
        });
        
        // Export Button
        var exportButton = $('<button title = "'+$.i18n("Export Events to CSV")+'">'+$.i18n("Export")+'</button>').button({
            icons : {
                secondary: 'ui-icon-arrowthickstop-1-s'
            },
            disabled: true
        }).addClass('ui-button-secondary')
        .click(function(e) {
            me.__download();
        });
        
        // Advanced Export Button
        var advancedExportButton = $('<button title = "'+$.i18n('Advanced Export Events to CSV')+'">'+$.i18n('Advanced Export')+'</button>').button({
            icons : {
                secondary: 'ui-icon-arrowthickstop-1-s'
            }
        }).click(function(e) {
            me.__advancedDownload();
        });
        
        var expandButton = $('<button id="showAdvanced" title = "'+$.i18n('Show Advanced Options')+'">'+$.i18n('Expand')+'</button>').button({
            icons: {
                primary: 'ui-icon-triangle-1-s'
            },
            text: false
        }).toggle(function(e) {
            me.__showAdvancedOptions();
            
            $(this).attr("title", $.i18n("Hide Advanced Options"));
            $(this).button("option", {
                icons: { primary: "ui-icon-triangle-1-n" }
            });
            
            me.__keyword.prop('disabled', true);
            me.__keyword.addClass('disabled'); // Because of IE
        }, function() {
            me.__hideAdvancedOptions();
            
            $(this).attr("title", $.i18n('Show Advanced Options'));
            $(this).button("option", {
                icons: { primary: "ui-icon-triangle-1-s" }
            });
            
            me.__keyword.prop('disabled', false);
            me.__keyword.removeClass('disabled'); // Because of IE
        });
        
        // Predefined Filters button
        if(!!me.__isDefinedFiltersVisible) {
        	var header = $('#innerHeader');
        	var buttonContainer = $('<span>').addClass('definedFiltersButton').appendTo(header);
        	
        	var predefinedButton = $('<button title = "'+$.i18n('Show predefined filters')+'">'+$.i18n('Expand')+'</button>').button({
        		icons: {
        			primary: 'ui-icon-triangle-1-s'
        		},
        		text: false
        	}).toggle(function(e) {
        		me.__showPredefinedFilters();
        	}, function(e) {
        		me.__hidePredefinedFilters();
        	});
        	
        	$('.definedFiltersButton').append(predefinedButton);
        };
        
        // Group Button
        if(!!me.__isGroupButtonVisible) {
            var groupButton = $('<button id="groupEvents" title = "'+$.i18n("Group Events")+'">'+$.i18n("Group")+'</button>').button()
            .click(function(e) {
                me.__openGroupDialog();
            });
            
            groupButton.button("option", "disabled", true);
            
            nx.rest.event.getLicences(function(response) {
                me.__licences = response;
                groupLicence = me.__licences.group;
                if (groupLicence) {
                    $('.groupEventsButton').append(groupButton);
                }
            });
        }
        
        
        $('.searchEventsButton').append(searchButton);
        $('.exportEventsButton').append(exportButton);
        $('.advancedExportEventsButton').append(advancedExportButton);
        $('.expandButton').append(expandButton);
    };

    ctor.prototype.__init = function() {
        var me = this;
        
        var fullDate = new Date();
        
        $( ".inputFaultDate").datepicker({
            /*
             * If the fault create date is based in the train timestamp, if the train has an incorrect timestamp,
             * a fault in future could happen (FCC for instance). Search using future dates doesn't look like a problem
            */
            //maxDate: fullDate.getDate() + "/" + twoDigitMonth + "/" + fullDate.getFullYear(),
            date: fullDate,
            dateFormat: "dd/mm/yy"
        });
        
        $( ".inputFaultDate")
            .datepicker( "setDate" , new Date());
        
        $( ".inputFaultDate")
            .datepicker( "option", "onChangeMonthYear" , function(year, month, inst){
                $(this).datepicker("setDate", '1/' + month + '/' + year );
            });
        
        $(window).resize(function() {
            me.__resizeEventTable();
        });
        
        $("#advancedButton").on('click',function(){
            me.__advancedClick();
        });
        
        $( "#radioDiv" ).buttonset();
        
    };
    
    ctor.prototype.__createDefinedFiltersPanel = function() {
    	var me = this;
    	
    	var header = $('#innerHeader');
    	me.__definedFilterPanel = $('<div>')
    		.attr('id','predefinedFilterPanel')
    		.addClass('predefinedFilters')
    		.appendTo(header);
    	
    	me.__definedFilterPanel.append($('<div>').addClass('definedFiltersContainer ui-corner-all darkbg'));
    	$('.definedFiltersContainer').append($('<div>').addClass('definedFiltersHeader').html($.i18n('Predefined Filters')));
    	
    	for (filterName in me.__definedFilters) {
    		var filter = me.__definedFilters[filterName];
    		
    		var button = me.__createDefinedFilterButton(filterName, filter);
    		
    		$('.definedFiltersContainer').append(button);
    	}
    	
    };
    
    ctor.prototype.__createDefinedFilterButton = function(filterName, filterConf) {
    	var me = this;
    	
    	var buttonContainer = $('<div>').addClass('definedFilterButtonContainer');
    	
    	// create defined filter button
    	var label = $('<div>').html($.i18n(filterName)).addClass('filterLabel').appendTo(buttonContainer);
    	var buttonDiv = $('<div>').addClass('filterButton').appendTo(buttonContainer);
    	var button = $('<button title="'+$.i18n('Apply')+'">'+$.i18n('Apply')+'</button>').button({
    		text: true
	    	}).click(function(e) {
	    		// perform search using filter details - pass conf to generic applyFilter function
	    		me.__applyDefinedFilter(filterConf);
	    	})
	    	.appendTo(buttonDiv);
    	
    	return buttonContainer;
    };
    
    ctor.prototype.__applyDefinedFilter = function(filterConf) {
    	var me = this;
    	
    	me.__resetParameters();
    	// timePickerFrom ?
    	// timePickerTo ?
    	
    	for(index in filterConf) {
    		var conf = filterConf[index];
    		
    		if(me.__advanced == false) {
    			$('#showAdvanced').click();
    		}
    		
    		switch (conf.type) {
	    		case "select" : {
	    			$('#' + conf.name).val(conf.value).change();
	    		} break;
	    		case "input_text" : {
	    			$('#' + conf.name).val(conf.value);
	    		} break;
	    		case "radio" : {	    			
	    			$('label[for=' + conf.value + ']').trigger("click");	    			
	    		} break;
	    		case "multiple_select" : {
	    			var input = me.__splitInputString(conf.value);
	    			$('#' + conf.name).val(input);
	    		} break;
				case "timeOffset" : {
	    			this.__setTimepicker(conf);
	    		} break;
    		}
    		
    	}
    	
    	$('#searchEvents').click();
    };
    
    ctor.prototype.__splitInputString = function(string) {
    	return string.split(',');
    };
    
    /** Call the event detail Dialog */
    ctor.prototype.__openDetails = function(cell, data, columnDef) {        
        var eventId = data.data[0];
        var fleetId = data.data[1];
        var eventIdList = [];
        
        $.each(this.__faultTable.__model, function (index, anEvent) {
            eventIdList.push(anEvent.fleetId + "-" + anEvent.id);
        });
        
        nx.event.open(eventId, fleetId, eventIdList);
    };
    
    /** selects/deselects all the events in an event group when one is clicked*/
    ctor.prototype.__toggleGroupChecked = function(data, columnDef, isChecked) {
    	var me = this;
    	nx.rest.event.getEventsByGroup(data.fleetId, data.faultGroupId, function(response) {
    		$.each(response, function(index, groupEvent) {
    			var event = groupEvent.fields;
    			me.__faultTable.setCellValue(event.id, columnDef.name, isChecked);
    			
    			if (!!isChecked) {
                    //checkbox.addClass('tickedCheckbox');
                    var record = me.__faultTable.getRecordById(event.id);
                    if (!!record) {
                        me.__checkedRecords[event.id] = record;
                    } else {
                        // if a record is not displayed due to filters
                        // create a temporary row which will be then placed
                        // in array of records to be displayed in Event Group window 

                        var row  = {'id': event.id, 
                                'type': event.type,
                                'fleetId': event.fleetId,
                                'sourceId': event.sourceId,
                                'rowVersion': event.rowVersion,
                                'data': me.__getRow(event)};
                        
                        if (!!event.faultGroupId) {
                            row.faultGroupId = event.faultGroupId;
                            row.isGroupLead = event.isGroupLead;
                        }
                        
                        if (!!event.maximoId) {
                            row.serviceRequestId = event.maximoId;
                        }
                        
                        if (event.current) {
                            row.cssClass = me.__getLightenClass(event.typeColor);
                        }

                        me.__checkedRecords[event.id] = row;
                    }
                } else {
                    //checkbox.removeClass('tickedCheckbox');
                    delete me.__checkedRecords[event.id];
                }
    		});
    		me.__faultTable.redraw();
    		me.__validateGroupButton();
    	});
    };
    
    ctor.prototype.__validateGroupButton = function() {
        if (Object.keys(this.__checkedRecords).length > 0) {
            $('#groupEvents').button("enable");
        } else {
            $('#groupEvents').button("disable");
        }
    }
    
    ctor.prototype.__openGroupDialog = function() {
        var me = this;
        var fleetId = null;
        var sourceId = null;
        var serviceRequestId = null;
        var faultGroupId = null;
        var valid = true;
        
        $.each(this.__checkedRecords, function(id, record) {
        	if(faultGroupId == null && !!record.faultGroupId) {
        		faultGroupId = record.faultGroupId;
        	}
        });
        
        $.each(this.__checkedRecords, function(id, record) {
            if (!!fleetId && record.fleetId != fleetId) {
                $('<div></div>').dialog({
                    buttons: { 'Ok': function () { $(this).dialog('close'); } },
                    close: function (event, ui) { $(this).remove(); },
                    resizable: false,
                    title: $.i18n('NotificationPayAttention'),
                    modal: true
                }).text('Event from different fleets have been selected. Only events from the same fleet can be grouped together. Please only select events from the same fleet.');
                valid = false;
                return false;
            } else if (!!sourceId && record.sourceId != sourceId) {
            	$('<div></div>').dialog({
                    buttons: { 'Ok': function () { $(this).dialog('close'); } },
                    close: function (event, ui) { $(this).remove(); },
                    resizable: false,
                    title: $.i18n('NotificationPayAttention'),
                    modal: true
                }).text($.i18n('EventGroupSameUnitWarning'));
            	valid = false;
            	return false;
            }else if (!!serviceRequestId && !!record.serviceRequestId && record.serviceRequestId != serviceRequestId){
            	$('<div></div>').dialog({
                    buttons: { 'Ok': function () { $(this).dialog('close'); } },
                    close: function (event, ui) { $(this).remove(); },
                    resizable: false,
                    title: $.i18n('NotificationPayAttention'),
                    modal: true
                }).text('Event with different service requests have been selected. Only events with the same service request can be grouped together.');
            	valid = false;
            	return false;
            }else if (!!faultGroupId && !!record.faultGroupId && record.faultGroupId != faultGroupId) {
            	$('<div></div>').dialog({
                    buttons: { 'Ok': function () { $(this).dialog('close'); } },
                    close: function (event, ui) { $(this).remove(); },
                    resizable: false,
                    title: $.i18n('NotificationPayAttention'),
                    modal: true
                }).text('Event from different groups have been selected. Only events without a group can be added to an existing group.');
            	valid = false;
            	return false;
            }else if (serviceRequestId == null){
            	serviceRequestId = record.serviceRequestId;
            	if (fleetId == null && sourceId == null){
	        		fleetId = record.fleetId;
	        		sourceId = record.sourceId;
            	}
            }
        });
        
        if (valid) {
            selectedRows = [];
            ids = Object.keys(this.__checkedRecords).map(Number);
            ids.sort(function(a,b) {return b-a});
            $.each(ids, function(i, id) {
                selectedRows.push(me.__checkedRecords[id]);
            });
            nx.event.group(selectedRows, fleetId, this);
        }
    };
    
    
    ctor.prototype.__mouseoverHandler = function(cell, data, columnDef) {
    	if (columnDef.name == "typeColor") {
    		var faultType = data.type;
            cell.attr('title', faultType);
    	} else {
    		var text = cell[0].children[0].innerHTML;
    		if (text != '&nbsp;') {
    		    cell.attr('title', text);
    		}
    	}
        
    };

    /** Checks the strings data from a event to prevent null data*/
    ctor.prototype.__checkNulls = function(event) {
        for (var k in event) {
            event[k] = $.format(event[k]);
        }

        return event;
    };

    /**
     * An RGB to HSL conversion function based on the algorithm defined in
     * Samuel R. Buss's '3D Computer Graphics: A Mathematical Introduction with
     * OpenGL'
     * 
     * @See books.google.com/books?isbn=0521821037
     * 
     * @param r
     *            A value in the range 0..255
     * @param g
     *            A value in the range 0..255
     * @param b
     *            A value in the range 0..255
     * 
     * Returns an array [h, s, l] where h, s & l are in the range 0..1
     */
    ctor.prototype.__rgbToHsl = function(r, g, b) {
        r /= 255, g /= 255, b /= 255;
        
        var max = Math.max(r, g, b),
            min = Math.min(r, g, b),
            delta = max - min,
            hue = 0,
            sat = 0,
            lum = 0.5 * (max + min);
        
        if (delta != 0) {
            if (lum < 0.5) {
                sat = delta / (max + min);
            } else {
                sat = delta / (2 - max - min);
            }
            
            switch (max) {
                case r:
                    hue = (g - b) / delta + (g < b ? 6 : 0);
                    break;
                case g:
                    hue = 2 + (b - r) / delta;
                    break;
                default:
                    hue = 4 + (r - g) / delta;
            }
        }
        
        return [hue > 0 ? hue / 6 : 0, sat, lum];
    };

    /**
     * A HSL to RGB conversion function based on the algorithm defined at: http://easyrgb.com/index.php?X=MATH&H=19#text19
     *
     * @param h - A value in the range 0..1
     * @param s - A value in the range 0..1
     * @param l - A value in the range 0..1
     *
     * @return An array [r, g, b], where the values r, g & b are in the range 0..255
     * @author Paul
     */
    ctor.prototype.__hslToRgb = function(h, s, l) {
        var hueToRgb = function (v1, v2, vH) {
            if (vH < 0) vH += 1;
            if (vH > 1) vH -= 1;
            if (( 6 * vH ) < 1) return v1 + ( v2 - v1 ) * 6 * vH;
            if (( 2 * vH ) < 1) return v2;
            if (( 3 * vH ) < 2) return v1 + ( v2 - v1 ) * ( ( 2 / 3 ) - vH ) * 6;
            return v1;
        };
        
        if (s === 0) {
            var r = Math.round(l * 255);
            // If saturation is 0, then colour is greyscale
            return [r, r, r];
        } else {
            var v2 = (l < 0.5) ? l * ( 1 + s ) : ( l + s ) - ( s * l ),
                v1 = 2 * l - v2;
            
            return [
                Math.round(255 * hueToRgb( v1, v2, h + ( 1 / 3 ))),
                Math.round(255 * hueToRgb( v1, v2, h )),
                Math.round(255 * hueToRgb( v1, v2, h - ( 1 / 3 )))
            ];
        }
    };

    ctor.prototype.__hexToRgb = function(hex) {
        var s = (hex.length == 7 || hex.length == 4) ? 1 : 0,
            inc = (hex.length == 3) ? 1 : 2,
            rgb = [];

        for (var i = s, l = hex.length; i < l; i += inc) {
            rgb.push(parseInt(hex.slice(i, i + inc), 16));
        }
        
        return rgb;
    };

    ctor.prototype.__rgbToHex = function(r, g, b) {
        var hex = '#';
        
        for (var i = 0, l = arguments.length; i < l; i++) {
            hex += (('0' + arguments[i].toString(16)).slice(-2));
        }

        return hex;
    };

    ctor.prototype.__lighten = function(color) {
        var rgb = this.__hexToRgb(color);
        var hsl = this.__rgbToHsl(rgb[0], rgb[1], rgb[2]);
        var nrgb = this.__hslToRgb(hsl[0], hsl[1], 0.85);
        var lighter = this.__rgbToHex(nrgb[0], nrgb[1], nrgb[2]);
        return lighter;
    };
    
    
    ctor.prototype.__getLightenClass = function(color) {
        var className = this.__backgroundClasses[color];
        
        if (!className) {
            
            className = "rowBackgroundColor"+this.__backgroundColors++;
            
            var cssColor = this.__lighten(color);
            $("<style type='text/css'> ."+className+"{ background-color:"+cssColor+"!important;} </style>").appendTo("head");
            
            this.__backgroundClasses[color] = className;
            
        }
        
        return className;
    };
    
    
    ctor.prototype.__loadData = function(data) {
    	var me = this;
        var gridData = [];
        if (!!data && data.length > 0) {
            for (var id in data) {
                var event = data[id].fields;
                
                var row  = {'id': event.id, 
                        'type': event.type,
                        'fleetId': event.fleetId,
                        'sourceId': event.sourceId,
                        'rowVersion': event.rowVersion,
                        'data': this.__getRow(event)};
                
                if (!!event.faultGroupId) {
                    row.faultGroupId = event.faultGroupId;
                    row.isGroupLead = event.isGroupLead;
                }
                
                if (!!event.maximoId) {
                	row.serviceRequestId = event.maximoId;
                }
                
                if (event.current) {
                    row.cssClass = this.__getLightenClass(event.typeColor);
                }

                gridData.push(row);
            }
            
            if ( data.length >= ( me.__searchLimit ) ) {
            	$('<div></div>').dialog({
                    buttons: { 'Ok': function () { $(this).dialog('close'); } },
                    close: function (event, ui) { $(this).remove(); },
                    resizable: false,
                    title: $.i18n('NotificationPayAttention'),
                    modal: true
            	}).text($.i18n('Screen ' + me.__searchLimit + ' limit reached. Please refine search.'));
            }
            
            this.__faultTable.setData(gridData, true);

        } else {
            this.__faultTable.setData(gridData, true);
            $('<div></div>').dialog({
                buttons: { 'Ok': function () { $(this).dialog('close'); } },
                close: function (event, ui) { $(this).remove(); },
                resizable: false,
                title: $.i18n('Notification'),
                modal: true
            }).text($.i18n('The search returned no results.'));
        }
        
        // if displaying grouped events together, perform an initial sort by create time
        if (me.__groupFaultsInTable === true) {
        	me.__faultTable.initialSort();
        }
        
        this.__validateGroupButton();
        
    };
    
    ctor.prototype.__getRow = function(obj) {
        var row = [];
        
        for (var i in this.__columns) {
            var column = this.__columns[i];
            row.push(obj[column.name]);
        }
        
        return row;
    };
    
    ctor.prototype.__resizeEventTable = function() {
    	// Nothing to do
    };
    

    /** Call the rest search or Advanced search method*/
    ctor.prototype.__doSearch = function() {
        var me = this;
        this.__checkedRecords = {};
        var parameters = this.__getSearchParameters(false);
        
        me.__pageSelected = 0;
        $("body").addClass("wait");
        if (this.__advanced == false) {
            nx.rest.event.search(parameters, function(response) {
            		$("body").removeClass("wait");
                    return me.__loadData(response);
                }
            );
        } else {
            nx.rest.event.advancedSearch(parameters, function(response) {
            	$("body").removeClass("wait");
                return me.__loadData(response);
            });
        }
    };

    /**Read the input necessaries to do the search or download*/
    ctor.prototype.__getSearchParameters = function(limit) {
    	
        var me = this;
        var startDate = me.__startTimestamp.getTime();
        var endDate = me.__endTimestamp.getTime();
        var live = $("input[name='liveRadio']:checked").val();
        var limitSearch = 0;
        if (live == "") {
            live = null;
        };
        
        if (limit == false) {
        	limitSearch = me.__searchLimit;
        } else {
        	limitSearch = me.__exportLimit;
        }
        
        var params = {
        	'limitHistory' : limitSearch,
            'startDate': startDate,
            'endDate': endDate,
            'live': live        
            };
        
        if (me.__advanced == false) {
            var keyword = me.__keyword.val();
            params['keyword'] = keyword;
            
            for (var i = 0, l = this.__additionalFields.length; i < l; i++) {
                var field = this.__additionalFields[i];
                params[field.name] = keyword;
            }
        } else {
            
            // Add the additional fields 
            for (var i = 0, l = this.__additionalFields.length; i < l; i++) {
                var field = this.__additionalFields[i];
                switch (field.type)
                {
    	            case "input_text" : {
    	            	params[field.name] = $('input#' + field.name).val();
    	            }break;
    	            case "select" : {
    	            	params[field.name] = $('select#' + field.name).val();
    	            }break;
    	            case "multiple_select" : { 
    	            	params[field.name] = __getMultipleSelectValues(field);
    	            }break;
    	            case "autocomplete" : { 
    	            	params[field.name] = $('input#' + field.name).val();
    	            }break;
                }
                
            }
        }
        
        return params;
    };
    
    function __getMultipleSelectValues(field)
    {
        var fieldValues = "";
        
        $('select#' + field.name + ' :selected').each(function(i, selected){
            fieldValues = fieldValues + "," + $(selected).val();
        })

    	return fieldValues.replace(/^,*/,"").replace(/,*$/,"").replace(/\s+/g,"");
    }
    
    ctor.prototype.__resetParameters = function() {
    	var me = this;
    	
    	$('label[for=liveAll]').trigger("click");
    	$('#keywordEventSearch').val('');
    	
    	for(index in me.__additionalFields) {
    		var field = me.__additionalFields[index];
    		
    		switch(field.type)
    		{
	    		case "input_text" : { 
	            	$('#' + field.name).val('');
	            }break;
	    		case "select" : {
	    			$('#' + field.name).val('');
	    			$.uniform.update('#' + field.name);
	    		}break;
	    		case "multiple_select" : {
	    			$('#' + field.name).val('');
	    		}break;
    		}
    	}
    };

    /** Download a csv file with the results of the table */
    ctor.prototype.__download = function() {
    	var url = null;
        var parameters = null;
        var me = this;
        
        parameters = this.__getSearchParameters(false);
        
        me.__pageSelected = 0;
        $("body").addClass("wait");
    	
    	if (me.__advanced == false) {
    		 url = nx.rest.event.download();
    		nx.rest.event.searchCount(parameters, function(response) {
    			$("body").removeClass("wait");
    			if ( response > 0 ) {
    	    		if ( response >= ( me.__searchLimit ) ) {
    	            	$('<div></div>').dialog({
    	                    buttons: { 'Ok': function () { $(this).dialog('close'); } },
    	                    close: function (event, ui) { $(this).remove(); },
    	                    resizable: false,
    	                    title: $.i18n('Notification'),
    	                    modal: true
    	            	}).text($.i18n('Export ' + me.__searchLimit + ' limit reached. Please refine search.'));
    	            }                
    	            nx.util.downloadPostWithJson(url, parameters);
    			} else {
    				$('<div></div>').dialog({
    	                buttons: { 'Ok': function () { $(this).dialog('close'); } },
    	                close: function (event, ui) { $(this).remove(); },
    	                resizable: false,
    	                title: $.i18n('Notification'),
    	                modal: true
    				}).text($.i18n('The export returned no results.'));
    			}
    	    });
    	} else {
            url = nx.rest.event.advancedDownload();
    		nx.rest.event.searchAdvancedCount(parameters, function(response) {
        		$("body").removeClass("wait");
        		if ( response > 0 ) {
            		if ( response >= ( me.__searchLimit ) ) {
                    	$('<div></div>').dialog({
                            buttons: { 'Ok': function () { $(this).dialog('close'); } },
                            close: function (event, ui) { $(this).remove(); },
                            resizable: false,
                            title: $.i18n('Notification'),
                            modal: true
                    	}).text($.i18n('Export ' + me.__searchLimit + ' limit reached. Please refine search.'));
                    }                
                    nx.util.downloadPostWithJson(url, parameters);
        		} else {
        			$('<div></div>').dialog({
                        buttons: { 'Ok': function () { $(this).dialog('close'); } },
                        close: function (event, ui) { $(this).remove(); },
                        resizable: false,
                        title: $.i18n('Notification'),
                        modal: true
        			}).text($.i18n('The export returned no results.'));
        		}
            });
    	}
    };
    
    ctor.prototype.__advancedDownload = function() {
    	var url = null;
        var parameters = null;
        var me = this;
        me.__pageSelected = 0;
        $("body").addClass("wait");
    	parameters = this.__getSearchParameters(true);
    	nx.rest.event.searchAdvancedCount(parameters, function(response) {
    		$("body").removeClass("wait");
    		if ( response > 0 ) {
        		if ( response >= ( me.__exportLimit ) ) {
                	$('<div></div>').dialog({
                        buttons: { 'Ok': function () { $(this).dialog('close'); } },
                        close: function (event, ui) { $(this).remove(); },
                        resizable: false,
                        title: $.i18n('Notification'),
                        modal: true
                	}).text($.i18n('Export ' + me.__exportLimit + ' limit reached. Please refine search.'));
                }
            	url = nx.rest.event.advancedDownload();
            	nx.util.downloadPostWithJson(url, parameters);
    		} else {
    			$('<div></div>').dialog({
                    buttons: { 'Ok': function () { $(this).dialog('close'); } },
                    close: function (event, ui) { $(this).remove(); },
                    resizable: false,
                    title: $.i18n('Notification'),
                    modal: true
                }).text($.i18n('The export returned no results.'));
    		}
        });
    };

    /** Open or close the advanced search div */
    ctor.prototype.__showAdvancedOptions = function() {
        var me = this;
        
        if (this.__advanced == false) {
            this.__advancedSearch.show('slide', {direction: 'up'});
            this.__faultTableDiv.animate({top: 146 + this.__eventHeader.height()}, function(){
            	me.__faultTable.getScroller().scroll();
            });
            
            this.__advanced = true;
        }
    };
    
    ctor.prototype.__hideAdvancedOptions = function() {
        var me = this;
        
        if (this.__advanced == true) {
            this.__advancedSearch.hide('slide', {direction: 'up'});
            this.__faultTableDiv.animate({top: 40}, function(){
            	me.__faultTable.getScroller().scroll();
            });
            
            this.__advanced = false;
        }
    };
    
    /** Open or close the predefined filters div  */
    ctor.prototype.__showPredefinedFilters = function() {
    	var me = this;
    	
    	if (me.__showDefinedFilters == false) {
    		me.__definedFilterPanel.show('slide', {direction: 'up'});
    		
    		me.__showDefinedFilters = true;
    	}
    };
    
    ctor.prototype.__hidePredefinedFilters = function() {
    	var me = this;
    	
    	if (me.__showDefinedFilters == true) {
    		me.__definedFilterPanel.hide('slide', {direction: 'up'});
    		
    		me.__showDefinedFilters = false;
    	}
    };
	
	ctor.prototype.__setTimepicker = function(conf) {
    	var me = this;
		var date = new Date();
		date.setHours(date.getHours() + parseInt(conf.value));
		if(conf.name == "fromDate") {
			me.__startTimestamp.setTime(new Date(date));
		}
		else if(conf.name == "toDate") {
			me.__endTimestamp.setTime(new Date(date));
		}
    }

    return ctor;
    
}());

nx.rest.logging.event();

$(document).ready(function() {
    var event = new nx.event.Event();
});
