var nx = nx || {};

nx.rest.eventanalysis = (function() {
    return {
    	EVENTANALYSIS_DOWNLOAD: 'rest/eventanalysis/download',
    	
        EVENTANALYSIS_GROUPINGS: 'rest/eventanalysis/groupings',
        EVENTANALYSIS_FILTERS: 'rest/eventanalysis/searchFields',
        EVENTANALYSIS_DETAIL_COLUMNS: 'rest/eventanalysis/detailColumns',
        EVENTANALYSIS_DETAIL_DEFAULT_SORTER: 'rest/eventanalysis/detailDefaultSorter',
        
        EVENTANALYSIS_FILTERS_PER_ROW: 'rest/eventanalysis/filtersPerRow',
        
        EVENTANALYSIS_LOAD_FILTER_OPTIONS: 'rest/eventanalysis/filterOptions',

        EVENTANALYSIS_FAULT_TYPES: 'rest/eventanalysis/faultTypes',

        EVENTANALYSIS_UNIT_SEARCH: 'rest/eventanalysis/searchUnit/{fleetId}/{unitNumber}',
        EVENTANALYSIS_VEHICLE_SEARCH: 'rest/eventanalysis/searchVehicle',
        EVENTANALYSIS_FAULT_META_SEARCH: 'rest/eventanalysis/searchFaultMeta',

        EVENTANALYSIS_LOCATION_SEARCH: 'rest/eventanalysis/searchLocation/{fleetId}/{location}',
        EVENTANALYSIS_DATA_SEARCH: 'rest/eventanalysis/searchData',
        EVENTANALYSIS_DETAIL_DATA_SEARCH: 'rest/eventanalysis/searchDetailData',

        download: function() {
            return this.EVENTANALYSIS_DOWNLOAD;
        },
        
        getGroupings: function(success) {
            var params = {
                'cache': false,
                'success': success
            };

            $.ajax(this.EVENTANALYSIS_GROUPINGS, params);
        },
        
        getFilters: function(success) {
            var params = {
                'cache': false,
                'success': success
            };

            $.ajax(this.EVENTANALYSIS_FILTERS, params);
        },
        
        getFiltersPerRow: function(success) {
            var params = {
                'cache': false,
                'success': success
            };

            $.ajax(this.EVENTANALYSIS_FILTERS_PER_ROW, params);
        },

        getDetailColumns: function(success) {
            var params = {
                'cache': false,
                'success': success
            };
    
            $.ajax(this.EVENTANALYSIS_DETAIL_COLUMNS, params);
        },

        getDetailDefaultSorter: function(success) {
            var params = {
                'cache': false,
                'success': success
            };
    
            $.ajax(this.EVENTANALYSIS_DETAIL_DEFAULT_SORTER, params);
        },

        getFilterOptions: function(success) {
            var params = {
                'cache': false,
                'success': success
            };

            $.ajax(this.EVENTANALYSIS_LOAD_FILTER_OPTIONS, params);
        },

        getFaultTypes: function(success) {
            var params = {
                'cache': false,
                'success': success
            };

            $.ajax(this.EVENTANALYSIS_FAULT_TYPES, params);
        },

        searchUnit: function(fleetId, unitNumber, success) {
            var service = this.EVENTANALYSIS_UNIT_SEARCH
                .replace('{fleetId}', fleetId)
                .replace('{unitNumber}', unitNumber);

            var params = {
                'cache': false,
                'success': success
            };

            $.ajax(service, params);
        },

        searchVehicle: function(fleetId, vehicleType, unitId, vehicleNumber, success) {
            var params = {
                'fleetId': fleetId,
                'vehicleType': !!vehicleType ? vehicleType : null,
                'unitId': !!unitId ? unitId : null,
                'vehicleNumber': vehicleNumber
            };

            $.ajax(this.EVENTANALYSIS_VEHICLE_SEARCH, {
                'type': 'POST',
                'contentType': 'application/json; charset=utf-8',
                'data': JSON.stringify(params),
                'cache': false,
                'success': success
            });
        },

        searchFaultMeta: function(parameters, success) {
        	$.ajax(this.EVENTANALYSIS_FAULT_META_SEARCH, {
                'type': 'POST',
                'contentType': 'application/json; charset=utf-8',
                'data': JSON.stringify(parameters),
                'cache': false,
                'success': success
            });
        },

        searchLocation: function(fleetId, location, success) {
            var service = this.EVENTANALYSIS_LOCATION_SEARCH
                .replace('{fleetId}', fleetId)
                .replace('{location}', location);

            var params = {
                'cache': false,
                'success': success
            };

            $.ajax(service, params);
        },

        searchData: function(parameters, success) {
            $.ajax(this.EVENTANALYSIS_DATA_SEARCH, {
                'type': 'POST',
                'contentType': 'application/json; charset=utf-8',
                'data': JSON.stringify(parameters),
                'cache': false,
                'success': success
            });
        },

        searchDetailData: function(parameters, success) {
            $.ajax(this.EVENTANALYSIS_DETAIL_DATA_SEARCH, {
                'type': 'POST',
                'contentType': 'application/json; charset=utf-8',
                'data': JSON.stringify(parameters),
                'cache': false,
                'success': success
            });
        }
    };
}());