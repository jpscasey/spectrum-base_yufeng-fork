/**
 * Extension of DatePicker to get the date as UTC.
 */

var getDate = function () {
if (this.size() > 0) {
          return prepareDate($('#' + $(this).data('datepickerId')).data('datepicker'));
        }
};

var prepareDate = function (options) {
    var dates = null;
    if (options.mode == 'single') {
      if(options.date) { 
    	  var dt = new Date(options.date);
    	  var UTCtime = dt.getTime()-(dt.getTimezoneOffset()*60000);
    	  dt.setTime(UTCtime);
    	  dates = dt;
      }
    } else {
      dates = new Array();
      $(options.date).each(function(i, val){
    	 var dt = new Date(val);
    	 var UTCtime = dt.getTime()-(dt.getTimezoneOffset()*60000);
    	 dt.setTime(UTCtime);
        dates.push(dt);
      });
    }
    return [dates, options.el];
  };

  jQuery.fn.extend({
	    DatePickerGetDate: getDate
	  });
  

  /**
   * Extension of jquery ui datePicker to get the date as UTC.
   * FIXME could be broken with a different version of jquery ui than 1.8.23
   */

  $.extend($.datepicker, {

	  _getOldDateDatepicker : $.datepicker._getDateDatepicker,

	  _getDateDatepicker: function(target, noDefault) {
		  var dt = $.datepicker._getOldDateDatepicker(target, noDefault);
		  var UTCtime = dt.getTime()-(dt.getTimezoneOffset()*60000);
		  dt.setTime(UTCtime);
		  return dt;
	  }

  });

  
