/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
(function( $ ){

  $.fn.cleartextbox = function() {
      return this.each(function() {
          $(this).css({'border-width': '0px', 'outline': 'none'})
          .wrap('<div class="divcleartextbox"></div>')
          .parent().append('<a class="cleartextbox" href="#"><img src="img/close.png" /></a>');

          $('.cleartextbox')
              .attr('title', 'Click to clear')
              .click(function() {
                  $(this).prev().val('').focus();
              });
      });
  };
  
  
})( jQuery );