var nx = nx || {};
nx.ea = nx.ea || {};

nx.ea.ChartRenderer = (function() {
    function ctor(eventAnalysis) {
        var me = this;

        this.__eventAnalysis = eventAnalysis;

        this.__content = $('#eaReportChartContent');

        this.__plot = null;
        this.__data = null;
        this.__faultTypes = null;
        this.__ticks = null;

        nx.rest.eventanalysis.getFaultTypes(function(faultTypes) {
            me.__faultTypes = {};

            for (var iType in faultTypes) {
            	var faultType = faultTypes[iType];
            	me.__faultTypes[faultType.name] = faultType;
            }
            
            me.__initialize();
        });
    };

    ctor.prototype.__initialize = function() {
        var me = this;

        //attach resize function to the browser window
        $(window).resize(function() {
            if (!!me.__plot && !!me.__plot.replot) {
                me.__plot.replot( { resetAxes: true } );
            }
        });

        this.__updateContent();
    };

    ctor.prototype.show = function() {
        if (this.__content.css('display') == 'none') {
            this.__content.show();
        }
    }

    ctor.prototype.hide = function() {
        if (this.__content.css('display') != 'none') {
            this.__content.hide();
        }
    }

    ctor.prototype.update = function(data, grouping, filters) {
        this.show();

        if (!this.__faultTypes) {
            return;
        }

        this.__data = data;

        this.__updateContent();
    };

    ctor.prototype.__updateContent = function() {
        var me = this;

        this.__plot = null;

        this.__content.empty();

        this.__ticks = [" "];
        var dataSeries = [];
        var labelSeries = [];
        var counters = [];

        for (var index in this.__faultTypes) {
            var faultType = this.__faultTypes[index];
            dataSeries.push([0]);
            labelSeries.push({ 'label': $.i18n(faultType.name), 'name': faultType.name, 'color': faultType.color });
            counters.push({'name': faultType.name, 'data': []});
        }

        if (!!this.__data) {
            if (this.__data.records.length > 0) {
                this.__ticks = [];
                dataSeries = [];

                for (var iRecord in this.__data.records) {
                    var reportBean = this.__data.records[iRecord];

                    this.__ticks.push(reportBean.referenceValue);

                    for (var iCounter in counters) {
                        var counter = counters[iCounter];
                        counter.data.push(reportBean[counter.name + 'Count']);
                    }
                }

                for (var iCounter in counters) {
                    dataSeries.push(counters[iCounter].data);
                }
            }
        }

        this.__plot = $.jqplot('eaReportChartContent', dataSeries, {
            'stackSeries': true,
            'legend': { 'show': true },
            'seriesDefaults': {
                'renderer': $.jqplot.BarRenderer,
                'fillToZero': true,
                'useNegativeColors': false,
                'pointLabels': {
                    'show': true,
                    'stackedValue': true,
                    'formatString': "%.2f"
                }
            },
            'series': labelSeries,
            'axes': {
                'xaxis': {
                    'renderer': $.jqplot.CategoryAxisRenderer,
                    'ticks': this.__ticks,
                    'tickRenderer': $.jqplot.CanvasAxisTickRenderer,
                    'tickOptions': {
                        'fontFamily': "Arial Unicode MS, Arial, sans-serif",
                        'angle': -45,
                        'fontSize': "8pt"
                    }
                }
            }
        });

        this.__content.on('jqplotDataHighlight',
            function () {
                $('.jqplot-event-canvas').css( 'cursor', 'pointer' );
            }
        );

        this.__content.on('jqplotDataUnhighlight',
            function() {
                $('.jqplot-event-canvas').css('cursor', 'auto');
            }
        );

        this.__content.on('jqplotDataClick', 
            function (ev, seriesIndex, pointIndex, data) {
                var referenceKey = me.__data.reference[me.__ticks[pointIndex]];
                var faultTypeId = me.__faultTypes[labelSeries[seriesIndex].name].id;

                me.__eventAnalysis.openReportDetail(referenceKey, faultTypeId);
            }
        );
    };

    return ctor;
})();
