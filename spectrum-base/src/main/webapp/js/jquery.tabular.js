/**
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 * @author Paul O'Riordan
 */
(function( $ ) {
    var Tabular = function(el, config) {
        el.addClass('tabular');
        this.__el = el;
        if (this.__isResizable(config)) {
            config.fixedHeader = false;
            config.resizableHeader = true;
        }
        this.__config = config;
        this.__table = el.children('table');
        this.__thead = $('<thead class = "ui-widget-header">');
        this.__tbody = $('<tbody>'); 
        this.__table.append(this.__thead);
        this.__table.append(this.__tbody);
        this.__columns = [];
        this.__setColumns(config);
        this.__sortPosition = null;
        this.__sortDesc = false;
        this.__model = [];
        this.__recordIdToModelIndex = {};
        this.__filterFunctions = [];
        
        /* 
         * Used by the partial renderer to store the tabular model.
         * __model will contain only displayed data
         */
        this.__fullDataList = []; 
        
        
        var me = this;
        
        this.__table.delegate("td", "mouseover", function() {
            var cell = $(this);
                
            var colNumber = cell
                .parent()
                .children()
                .index(cell);
            
            //construct table of visible columns
            var visibleColumns = [];

            for (var i = 0; i < me.__columns.length; i++) {
                if (me.__columns[i].visible) {
                    visibleColumns[visibleColumns.length] = me.__columns[i];
                }
            }

            var col = visibleColumns[colNumber];

            //var col = me.__columns[colNumber];

            // Add "cursor:pointer" when user hovers over clickable cell.
            // XXX this requires the columns to have the "random" handler
            // attribute set. Need to review this.
            if (!!config.cellClickHandler && !!col.handler) {
                cell.addClass('clickable');
            }
            
            if (!!config.cellMouseOverHandler) {
                var rowNumber = cell
                    .parent()
                    .parent()
                    .children()
                    .index(cell.parent());
            
                config.cellMouseOverHandler(
                    cell,
                    me.__model[rowNumber],
                    col
                );
            }
        });

        this.__table.delegate("td", "mouseout", function() {
            $(this).removeClass('clickable');
        });

        if (!!config.cellClickHandler) {
            this.__table.delegate("td", "click", function(e) {
                var cell = $(this);

                var colNumber = cell
                    .parent()
                    .children()
                    .index(cell);
                
                //construct table of visible columns
                var visibleColumns = [];

                for (var i = 0; i < me.__columns.length; i++) {
                    if (me.__columns[i].visible) {
                        visibleColumns[visibleColumns.length] = me.__columns[i];
                    }
                }
                
                var col = visibleColumns[colNumber];

                var rowNumber = cell
                    .parent()
                    .parent()
                    .children()
                    .index(cell.parent());

                config.cellClickHandler(
                    cell,
                    me.__model[rowNumber],
                    col,
                    e.target
                );
            });
        }
    };
    
    Tabular.prototype.__isResizable = function(conf) {
        for(var i = 0, l = conf.columns.length; i < l; i++) {
            var columns = conf.columns[i];
            if (!!columns) {
                // is expected to be a 2d array - if not, it is converted into one.
                if (columns.length > 0 && !$.isArray(columns[0])) {
                    columns = [columns];
                }
                for(var j = 0, ll = columns.length; j < ll; j++) {
                    if (conf.columns[i][j].resizable) {
                        return true;
                    }
                }
            }
        }
    };

    Tabular.prototype.__createFixedHeader = function(el) {
        this.__thead = this.__table.children('thead');
        this.__tbody = this.__table.children('tbody');
        this.__fixedHeader = $('<div>').attr(
            'class', 'fixed'
        );
        this.__scroller = $('<div>').attr(
            'class', 'scroller'
        );
        this.__tableContainer = $('<div>').attr(
                'class', 'tableContainer'
        );
        this.__scrollWidth = this.__getScrollbarWidth();
        
        this.__fixedHeader.css('margin-right', this.__scrollWidth);
        
        // If fixedHeader..
        el.prepend(this.__scroller);
        el.prepend(this.__fixedHeader);
        if (this.__config.renderOnlyVisibleRows === true) {
            this.__initRenderOnlyVisibleRows();
        }
        this.__tableContainer.append(this.__table);
        this.__scroller.append(this.__tableContainer);
        this.__cloneHead(this.__fixedHeader);

        // Attach resize handler to window.
        var me = this; 
        var timer;
        var scrollLeft = -1;
        
        this.__scroller.scroll(function() {
            if (this.scrollLeft != scrollLeft) {
                scrollLeft = this.scrollLeft;
                me.__fixedHeader.css('margin-left', '-' + scrollLeft + 'px');
            }
        });
        
        $(window).resize(function() {
            clearTimeout(timer);
            
            if (timer != null) {
                window.clearTimeout(timer);
            }
            
            timer = setTimeout(function() {
                me.__scroller.scroll();
                me.adjustHeaderWidths();
                timer = null;
            }, 5);
        });
   
        me.__fixedHeader.find(".resizable").each(function(index, column) {
            var resizableColumn = $(column).removeClass("resizable").resizable({
                    handles: "e",
                    resize: function( event, ui ) {
                        me.adjustHeaderWidths();
                        $(event.target).removeClass("autosize");
                    }
                });
            var alsoResizeColumn = $(".scroller .resizable").eq(0);
            alsoResizeColumn.removeClass("resizable");
            resizableColumn.resizable("option", "alsoResize", alsoResizeColumn);
        });
    };
    
    /**
     * Clones the real table header add adds it to a table inside
     * a div, which is positioned above the table. After adding
     * the 'fake' header. The header width needs to be adjusted 
     */
    Tabular.prototype.__cloneHead = function(header) {
        var clone = this.__thead.clone();
        header.append($('<table>').append(clone));
    };
    
    Tabular.prototype.__createResizableHeader = function(el) {
        var me = this;
        
        me.__thead = me.__table.children('thead');
        me.__tbody = me.__table.children('tbody');
        me.__fixedHeader = $('<div>').attr(
                'class', 'resizable'
        );
        me.__scroller = $('<div>').attr(
                'class', 'scroller'
        );
        me.__tableContainer = $('<div>').attr(
                'class', 'tableContainer'
        );
        me.__scrollWidth = me.__getScrollbarWidth();
        
        me.__fixedHeader.css('margin-right', me.__scrollWidth);
        
        el.prepend(me.__scroller);
        el.prepend(me.__fixedHeader);
        if (me.__config.renderOnlyVisibleRows === true) {
            me.__initRenderOnlyVisibleRows();
        }
        
        me.__tableContainer.append(me.__table);
        me.__scroller.append(me.__tableContainer);
        me.__cloneHead(me.__fixedHeader);
        
        me.__fixedHeader.find(".resizable").each(function(index, column) {
            var resizableColumn = $(column).removeClass("resizable").resizable({
                    handles: "e",
                    resize: function( event, ui ) {
                        me.adjustHeaderWidths();
                        $(event.target).removeClass("autosize");
                    }
                });
            var alsoResizeColumn = $(".scroller .resizable").eq(0);
            alsoResizeColumn.removeClass("resizable");
            resizableColumn.resizable("option", "alsoResize", alsoResizeColumn);
        });
    };
    
    Tabular.prototype.__initRenderOnlyVisibleRows = function() {
        this.__table.css("position", "absolute");
        if (!!this.__config.scrollHandler) {
        	this.__scroller.scroll(this.__config.scrollHandler);
        } else {
        	this.__scroller.scroll(this, this.__renderOnlyVisibleRowsScrollHandler);
        }
        
        this.setTableHeight(this.__config.tableHeight);
    };
    
    
    /**
     * Calculate the height in pixels of a line, we assume that all lines have the same height.
     * @param model a table which contain data
     */
    Tabular.prototype.__calculateLineHeight = function(model) {

    	var tbody = ['<tbody class = "scrollContent">'];
        
        if (!!model[0]) {
            var row = model[0];
            tbody.push(this.__addRow(row));
        }
        
        tbody.push('</tbody>');
        this.__table.append(tbody.join(''));
        
        var height = this.__table.find("tbody > tr:first").height();
        
        this.__table[0].removeChild(
                this.__table.children('tbody')[0]
            );
        
        return height;
    };
    
    Tabular.prototype.__renderOnlyVisibleRowsScrollHandler = function(objEvent) {
    	
    	var me = objEvent.data;
    	
    	var startPosition = me.getScroller().scrollTop();

    	var lineHeight = me.__lineHeight;

    	var tableHeight = me.__el.height() - me.__thead.height();

    	var startRow = Math.floor(me.getScroller().scrollTop() / lineHeight);
    	
    	var remainder = me.getScroller().scrollTop() % lineHeight;

    	me.getTable().css("top", startPosition-remainder + "px");

    	// Calculate how many row can be displayed
    	var nbLines = Math.ceil(tableHeight / lineHeight);

    	var data = [];
    	if (me.__fullDataList.length < (startRow+nbLines)) {
    		data = me.__fullDataList.slice(startRow);
    	} else {
    		data = me.__fullDataList.slice(startRow, startRow+nbLines);
    	}

    	me.__internalSetData(data, true, false);

    };
    
    Tabular.prototype.setTableHeight = function(tableHeight) {
        this.__tableContainer.css("height", tableHeight + "px");
    };
    
    /**
     * Sets the width of the fixedHeader div to be the same width
     * as the scrollContainer.
     */
    Tabular.prototype.adjustHeaderWidths = function() {
        //var fake = this.__el.find('.fixed tr:last-child th > div');
        var real = this.__el.find('.scroller tr:last-child th > div');
        var undef = [];
        
        for (var i = 0, l = this.__columns.length; i < l; i++) {
            var column = this.__columns[i];
            if (column.width == null && column.colspan == null) {
                undef.push(i);
            }
        }

        var fheaders = this.__fixedHeader.find('tr:last-child th div:first-child');
        var rheaders = this.__thead.find('tr:last-child th div:first-child');
        if (undef.length > 1) {
            // Prevents layout/style recalculation occurring for each adjusted column.
            var widths = [];
            
            for (var i = 0, l = undef.length; i < l; i++) {
                var realTh = $(rheaders[undef[i]]);
                widths.push(realTh.width());
            }
            
            for (var i = 0, l = undef.length; i < l; i++) {
                var fakeTh = $(fheaders[undef[i]]);
                fakeTh.width(widths[i]);
            }
        } else if (undef.length == 0) {
            // Prevents layout/style recalculation occurring for each adjusted column.
            var widths = [];
            
            for (var i = 0, l = real.length; i < l; i++) {
                var realTh = $(rheaders[i]);
                widths.push(realTh.width());
            }
            
            for (var i = 0, l = widths.length; i < l; i++) {
                var fakeTh = $(fheaders[i]);
                fakeTh.width(widths[i]);
            }
        }
    };
    
    /**
     * Calculates the scrollbar width by creating an absolute
     * 100x100px div and stuffing another element with 100% height
     * & width inside it, causing the div to overflow. We then
     * calculate the width of the inner div with overflow:hidden
     * & overflow:auto on the parent. The different between these
     * two elements is the scrollbar width.
     */ 
    Tabular.prototype.__getScrollbarWidth = function() {
        var scroll = $('<div>').css({
            'position': 'absolute',
            'width': '100px',
            'height': '100px',
            'visibility': 'hidden',
            'overflow': 'hidden'
        });
        
        var div = $('<div>').css({
            'width': '100%',
            'height': '100%'
        });
          
        scroll.append(div).appendTo('body');
         
        var origWidth = div.width();
        scroll.css('overflow', 'scroll');
          
        var width = origWidth - div.width();
        $(scroll).remove();
        return width;
    };
     
    /**
     * Create the thead element from the list of user supplied
     * columns.
     *
     * @param columns a 2D array specifying the columns, e.g.
     * [
     *  [{text: 'g1', colspan: 3}],
     *  [{text: 'c1', width: '50px'}, {text: 'c2'}, {text: 'c3'}]
     * ]
     */
    Tabular.prototype.__setColumns = function(conf) {
        var me = this;
        var tr = ['<tr>'];
          
        this.__columns = [];

        // conf.columns is expected to be a 2d array - if not, it is converted
        // into one.
        var iterColumns = conf.columns;
        
        if (iterColumns.length > 0 && !$.isArray(iterColumns[0])) {
            iterColumns = [iterColumns];
        }
        
        for (var i in iterColumns) {
            var tr = $('<tr>');
            
            // Add lastRow class to the last row
            if (i == (iterColumns.length-1)) {
                tr.addClass('lastRow');
            }
            
            
            for (var j in iterColumns[i]) {
                var col = iterColumns[i][j];

                var span = $('<span>')
                    .text($.i18n(col.text));
                
                if (!!col.spanCssClass) {
                    span.addClass(col.spanCssClass);
                }
                
                var div = $('<div>')
                    .addClass('headerContent')
                    .width('100%');
                
                if (col.sortable) {
                    div.addClass('clickable');
                }
                
                var th = $('<th>')
                    .attr('colspan', col.colspan)
                    .attr('rowspan', col.rowspan);
                
                if (!!col.mouseOverHeader) {
                    th.attr('title', col.mouseOverHeader);
                }
                
                if(!!col.cssClass) {
                    th.addClass(col.cssClass);
                }
                    
                if (col.rowspan >= iterColumns.length) {
                    this.__columns.push(col);
                } else if (i == iterColumns.length - 1) {
                    this.__columns.push(col);
                }

                if (col.resizable == true) {
                    th.addClass("resizable");
                    
                    if (conf.resizableHeader === true) {
                        th.css('position', 'relative');
                    }
                }

                if (col.width != null) {
                    th.width(col.width);
                }

                if (col.visible == false) {
                    th.css('display', 'none');
                }

                if (col.autosize == true) {
                    th.addClass("autosize");
                }
 
                div.append(span);
                th.append(div);
                tr.append(th);
            }
            
            this.__thead.append(tr);
        }
        
        // save the bottom row of columns
        //this.__columns = conf.columns[conf.columns.length - 1];

        if (conf.fixedHeader === true) {
            this.__createFixedHeader(this.__el);
        } else if (conf.resizableHeader === true) {
            this.__createResizableHeader(this.__el);
        }
         
        // Change to tr:last..
        this.__el.find('thead .lastRow th').click(function(el) {
            var th = $(el.target).closest('th');
            
            var colNumber = th
                .parent()
                .children()
                .index(th);

            var column = me.__columns[colNumber];
            
            if (column.sortable && !!column.sortByColumn) {
            	me.sortColumnByOtherColumnName(column.name, column.sortByColumn);
            } else if (column.sortable) {
            	me.sort(el.target, conf.defSortDesc);
            }
        });
    };

    /**
     * Sorts the table by the specified column
     * @param el - the column to sort by.
     */
    Tabular.prototype.sort = function(el, defSortDesc) {
        
        // Figure out what column # this column is.
        el = $(el).closest('th')[0];
        
        var children = $(el).parent().children();
        var position = -1;
        var sortDesc = false;
        if (defSortDesc) {
        	sortDesc = true;
        }
        
        for (var i = 0, l = children.length; i < l; i++) {
            var child = children[i];
            var jqChild = $(child);
            
            if (child == el) {
                var sort = jqChild.children('div.sort');
                
                if (sort.length > 0) {
                    if (sort.hasClass('desc')) {
                        sort
                            .removeClass('desc')
                            .removeClass('ui-icon-triangle-1-s')
                            .addClass('asc')
                            .addClass('ui-icon-triangle-1-n');
                        sortDesc = false;
                    } else {
                        sort
                            .removeClass('asc')
                            .removeClass('ui-icon-triangle-1-n')
                            .addClass('desc')
                            .addClass('ui-icon-triangle-1-s');
                        sortDesc = true;
                    }
                } else {
                    // Add the sort icon to jqChild + position it in the middle
                    if (defSortDesc) {
                	    $('<div>')
                            .addClass('sort')
                            .addClass('desc')
                            .addClass('ui-icon')
                            .addClass('ui-icon-triangle-1-s')
                            .css(
                                'margin-bottom', ($(el).height() / 2 - 4) + 'px'
                            )
                            .appendTo(jqChild);
                    } else {
                    	$('<div>')
                            .addClass('sort')
                            .addClass('asc')
                            .addClass('ui-icon')
                            .addClass('ui-icon-triangle-1-n')
                            .css(
                                'margin-bottom', ($(el).height() / 2 - 4) + 'px'
                            )
                            .appendTo(jqChild);
                    }    
                }
                
                position = i;
            } else {
                jqChild.children('div.sort').remove();
            }
        }
        
        this.__sortPosition = position;
        this.__sortDesc = sortDesc;

        if ( this.__config.renderOnlyVisibleRows === true) {
        	this.__scroller.scrollTop(0);
        	
            // sort first
            if (this.__sortPosition != null) {
                // sort records
                this.__model = this.__fullDataList;
                this.__sortModel();
                this.__fullDataList = this.__model;
            }
            
            // then scroll to adjust the page accordingly
            this.__scroller.scroll();
        	
        } else {
        	// Now, do the sorting & redraw
            this.__sortModel();
            this.redraw();
        }
        
        
        if (!!this.__config.sortCallback) {
        	
        	// Get all the columns
        	var columnsEl = this.__el.find('thead tr th').find('.clickable');
        	
        	//Find the number of the column
        	for (var i = 0, l = columnsEl.length; i < l; i++) {
                var column = columnsEl[i];
                if ($(column).parent()[0] == el) {
                	this.__config.sortCallback(this.__columns[i].name, sortDesc);
                }
            }
        	
        }

    };
    
    Tabular.prototype.__sortModel = function() {
        var column = this.__columns[this.__sortPosition];
        
        try {
	        if (!column.type) {
	            this.__sortModelString();
	        } else if (column.type == 'number' || column.type.toUpperCase() == 'ANALOGUE' || column.type.toUpperCase() == 'DIGITAL' || column.type == 'timestamp') {
	            this.__sortModelNumber();
	        } else if (column.type == 'duration') {
	            this.__sortDuration();
	        } else {
	            this.__sortModelString();
	        }
	    } catch (e) {
	        if (e instanceof TypeError) {
                this.__sortModelString();
            }
	    }
        
        this.__rebuildIndex(this.__model);
    };

    Tabular.prototype.hasRecordId = function(id) {
    	
        if (!!this.__recordIdToModelIndex[id]) {
            return true;
        }
        return false;
    };

    Tabular.prototype.getRecordIds = function() {
        var keys = {};
        for (var k in this.__recordIdToModelIndex) {
            keys[k] = '1';
        }
        return keys;
    };

    Tabular.prototype.__sortModelNumber = function() {
        var me = this;
        
        this.__model = this.__model.sort(function(a, b) {
            var colA = me.__getCellValue(a, me.__sortPosition);
            var colB = me.__getCellValue(b, me.__sortPosition);
            
            // XXX some of the columns are coming in as strings..
            //var colA = a.data[me.__sortPosition];
            //var colB = b.data[me.__sortPosition];
            var valA = colA != null ? parseFloat(colA) : null;
            var valB = colB != null ? parseFloat(colB) : null;
            
            // Check if we are not sorting booleans
            if (colA != null && typeof(colA) === "boolean") {
            	valA = colA ? 1 : 0;
            }
            
            if (colB != null && typeof(colB) === "boolean") {
            	valB = colB ? 1 : 0;
            }
            
            // Check nulls first because they can behave weirdly if directly compared with numbers
            if (valA == null) {
                return 1;
            } else if (valB == null) {
                return -1;
            } else if (valA < valB) {
                return me.__sortDesc ? 1 : -1;
            } else if (valA > valB) {
                return me.__sortDesc ? -1 : 1;
            } else {
                return 0;
            }
        });
    };
    
    Tabular.prototype.__sortDuration = function () {
        var me = this;
        
        this.__model = this.__model.sort(function(a, b) {
            var colA = me.__getCellValue(a, me.__sortPosition);
            var colB = me.__getCellValue(b, me.__sortPosition);
            
            //remove all colons from string before parsing to a float
            var valA = colA != null ? parseFloat(colA.replace(/:/g,'')) : null;
            var valB = colB != null ? parseFloat(colB.replace(/:/g,'')) : null;
            
            // Check nulls first because they can behave weirdly if directly compared with numbers
            if (valA == null) {
                return 1;
            } else if (valB == null) {
                return -1;
            } else if (valA < valB) {
                return me.__sortDesc ? 1 : -1;
            } else if (valA > valB) {
                return me.__sortDesc ? -1 : 1;
            } else {
                return 0;
            }
        });
    };
    
    Tabular.prototype.__sortModelString = function() {
        var me = this;

        this.__model = this.__model.sort(function(a, b) {
            //var valA = a.data[me.__sortPosition] || "";
            //var valB = b.data[me.__sortPosition] || "";
            var valA = me.__getCellValue(a, me.__sortPosition) || "";
            var valB = me.__getCellValue(b, me.__sortPosition) || "";

            var compare;
            
            if (valA.length == 0) {
            	return 1;
            } else if (valB.length == 0) {
            	return -1;
            }
            
            if (jQuery.type(valA) == "string" && jQuery.type(valB) == "string") {
            	compare = valA.toUpperCase().localeCompare(valB.toUpperCase());
            } else {
            	compare = valA - valB
            }
            
            if (compare < 0) {
                return me.__sortDesc ? 1 : -1;
            } else if (compare > 0) {
                return me.__sortDesc ? -1 : 1;
            } else {
                return 0;
            }
            
            
        });
    };
    
    Tabular.prototype.sortByColumnName = function(columnName) {
        var position = null;
        for (var i = 0, l = this.__columns.length; i < l; i++) {
            var column = this.__columns[i];
            if (column.name == columnName) {
                position = i;
                break;
            }
        }

        var target = this.__el.find('thead tr th').find('.clickable')[position];
        
        this.sort(target);
    };
    
    Tabular.prototype.sortColumnByOtherColumnName = function(columnName, sortByColumnName) {
    	var position = null;
    	var sortPosition = null;
        for (var i = 0, l = this.__columns.length; i < l; i++) {
            var column = this.__columns[i];
            if (column.name == columnName) {
            	position = i;
            } else if (column.name == sortByColumnName) {
            	sortPosition = i;
            }
            if (!!position && !!sortPosition) {
            	break;
            }
        }

        var sortBy = this.__el.find('thead tr th')[sortPosition];
        var target = this.__el.find('thead tr th')[position];
        var jqTarget = $(target);
        
        this.sort(sortBy);
        
        var sort = $(sortBy).children('div.sort');
        
        if (sort.length > 0) {
            if (sort.hasClass('desc')) {
            	$('<div>')
                .addClass('sort')
                .addClass('desc')
                .addClass('ui-icon')
                .addClass('ui-icon-triangle-1-s')
                .css(
                    'margin-bottom', (jqTarget.height() / 2 - 4) + 'px'
                )
                .appendTo(jqTarget);
            } else {
            	$('<div>')
                .addClass('sort')
                .addClass('asc')
                .addClass('ui-icon')
                .addClass('ui-icon-triangle-1-n')
                .css(
                    'margin-bottom', (jqTarget.height() / 2 - 4) + 'px'
                )
                .appendTo(jqTarget);
            }
        }
    };
    
    Tabular.prototype.__getCellValue = function(rowObj, colNum) {
        if (!rowObj) {
            return null;
        } else if (!rowObj.data) {
            return null;
        } else {
            var value = rowObj.data[colNum];
            
            if ((!!value) && (typeof value == 'object')) {
                return value[0];
            } else {
                return value;
            }
        }
    };
    
    Tabular.prototype.__rebuildIndex = function(model) {
        this.__recordIdToModelIndex = {};
        
        for (var i in model) {
            var record = model[i];
            this.__recordIdToModelIndex[record.id] = i;
        }
    };
    
    
    
    Tabular.prototype.__internalSetData = function(data, bRedraw, bSort) {
    	this.__model = data;
    	
    	if (this.__sortPosition != null && (bSort === undefined || bSort)) {
            this.__sortModel();
        }
        
    	if (this.__config.renderOnlyVisibleRows === true) {
    		this.__rebuildIndex(this.__fullDataList);
    	} else {
    		this.__rebuildIndex(this.__model);
    	}
    	
        if (bRedraw === undefined || bRedraw) {
            this.redraw();
        }
    	
    };
    
    
    Tabular.prototype.setData = function(data, bRedraw) {

        if (this.__config.renderOnlyVisibleRows === true) {
        	this.__fullDataList = data;
        	
        	this.__lineHeight = this.__calculateLineHeight(data);
        	
        	var headerHeight = this.__thead.height();
        	
        	this.setTableHeight(headerHeight + data.length*this.__lineHeight);
            
        	var tableHeight = this.__el.height() - headerHeight;
        	
        	data = data.slice(0, Math.ceil(tableHeight/this.__lineHeight));
            
        }
        
        this.__internalSetData(data, bRedraw);
        
        if (this.__config.resizableHeader) {
            this.adjustHeaderWidths();
        }
        
        if (this.__config.renderOnlyVisibleRows === true) {
      	
            // sort first
            if (this.__sortPosition != null) {
                // sort records
                this.__model = this.__fullDataList;
                this.__sortModel();
                this.__fullDataList = this.__model;
            }
            
            // then scroll to adjust the page accordingly
            this.__scroller.scroll();
        }
        
        this.__autosize();
    };

    Tabular.prototype.updateData = function(data, bRedraw) {
    	if (this.__config.renderOnlyVisibleRows === true) {
    		this.__internalSetData(this.__fullDataList, false);
    	}
    	
    	
        for (var i in data) {
            var record = data[i];
            var position = this.__recordIdToModelIndex[record.id];
            
            if (position != null) {
                
                for (var k in record.data) {
                    var newValue = record.data[k];
                    
                    if (newValue !== undefined) {
                        this.__model[position].data[k] = record.data[k];
                    }
                }
            } else {
                this.__model.push(record);
            }
        }
        
        if (this.__sortPosition != null) {
            this.__sortModel();
        }

        
        if (this.__config.renderOnlyVisibleRows === true) {
        	this.__rebuildIndex(this.__fullDataList);
        	
        	// sort first
            if (this.__sortPosition != null) {
                // sort records
                this.__model = this.__fullDataList;
                this.__sortModel();
                this.__fullDataList = this.__model;
            }
            
            // then scroll to adjust the page accordingly
        	this.__scroller.scroll();
        } else {
        	this.__rebuildIndex(this.__model);
        	if (bRedraw === undefined || bRedraw) {
        		this.redraw();
        	}
        }
        
        this.__autosize();
    };

    Tabular.prototype.__autosize = function() {
        var me = this;
        if (!!me.__fixedHeader) {
            me.__fixedHeader.find("th").each(function(index, column) {
                if ($(column).hasClass("autosize")) {
                    var alsoResizeColumn = me.__scroller.find("th").eq(index);
                    
                    var longestColumnValue = me.__longestColumnValue(index);
                    var textWidth = Math.max(me.__getTextWidth(longestColumnValue) + 60, 20);
                    
                    column.style.width = textWidth + 'px';
                    alsoResizeColumn[0].style.width = textWidth + 'px';
                }
            });
        }
    };
    
    Tabular.prototype.__longestColumnValue = function(colIndex) {
        var longestValue = "";
        for (var i in this.__fullDataList) {
            var row = this.__fullDataList[i];
            if (row.data[colIndex].length > longestValue.length) {
                longestValue = row.data[colIndex]
            }
        }
        
        return longestValue;
    };
    
    Tabular.prototype.__getTextWidth = function getWidthOfText(text){
        // Create dummy span
        var fakeEl = $('<span>').hide().appendTo(document.body);
        fakeEl.text(text).css(this.__tbody.css('font'));
        
        // Get width
        var width = fakeEl.width();
        
        // Remove dummy span
        fakeEl.remove();
        
        return width;
    };
    
    Tabular.prototype.redraw = function() {
        var tbody = ['<tbody class = "scrollContent">'];
        
        for (var i in this.__model) {
            var row = this.__model[i];
            if (this.applyFilters(row)) {
                tbody.push(this.__addRow(row));
            } else {
            	tbody.push("<tr class='hidden'></tr>");
            }
        }
        
        tbody.push('</tbody>');

        this.__table.children('tbody').replaceWith(tbody.join(''));
        
        if (this.__config.fixedHeader === true) {
            this.adjustHeaderWidths();
        }
        
        this.tickCheckboxes();
    };

    Tabular.prototype.getRowData = function(rowNumber) {
        var row = this.__model[rowNumber];
        if (!!row) {
            return row.data;
        }
        return null;
    };
    
    Tabular.prototype.showRow = function(rowNumber) {
        this.__setRowVisibility(rowNumber, true);
    };

    Tabular.prototype.hideRow = function(rowNumber) {
        this.__setRowVisibility(rowNumber, false);
    };

    Tabular.prototype.showAllRows = function() {
        var tbody = this.__table.children('tbody');
        var trs = tbody.children('tr');

        for (var i = 0, l = trs.length; i < l; i++) {
            this.__setRowVisibility(i, true);
        }

        this.updateStriping();
    };

    Tabular.prototype.hideAllRows = function() {
        var tbody = this.__table.children('tbody');
        var trs = tbody.children('tr');

        for (var i = 0, l = trs.length; i < l; i++) {
            this.__setRowVisibility(i, false);
        }

        this.updateStriping();
    };

    Tabular.prototype.__setRowVisibility = function(rowNumber, visible) {
        var tbody = this.__table.children('tbody');
        var trs = tbody.children('tr');
        var tr = $(trs[rowNumber]);

        var classToAdd = 'visible';
        var classToRemove = 'nonVisible';
        if (!visible) {
            classToAdd = 'nonVisible';
            classToRemove = 'visible';
        }

        tr.removeClass(classToRemove);
        tr.addClass(classToAdd);
    };

    Tabular.prototype.updateStriping = function() {
        var tbody = this.__table.children('tbody');
        var trs = tbody.children('tr:visible');

        for (var i = 0, l = trs.length; i < l; i++) {
            var tr = $(trs[i]);

            if (i % 2 == 0) {
                tr.addClass('odd');
                tr.removeClass('even');
            } else {
                tr.addClass('even');
                tr.removeClass('odd');
            }
        }
    };

    Tabular.prototype.__addRow = function(row) {
    	var trClasses = "visible";
        
        if(!!row.cssClass) {
            trClasses += " " + row.cssClass;
        }
        
        var tr = ['<tr class="'+trClasses+'">'];
        
        
        var data = row.data;
        
        for (var i in data) {
            if (i >= this.__columns.length) {
                break;
            }
            
            // optional format string in column config
            var format = this.__columns[i].format;
            // optional format function - these take precedence over the format string above.
            var formatter = this.__columns[i].formatter;
            var visible = this.__columns[i].visible;
            var cssClass = this.__columns[i].cssClass;
            
            var colType = this.__columns[i].type;
            var colName = this.__columns[i].name;
            
            if (visible != false) {
                
                var classes = 'c'+i;
                if (!!cssClass) {
                   classes = classes+' '+cssClass; 
                }
                
                tr.push('<td class="' + classes + '">');
                
                
                
                if (colType == 'checkbox') {
                    var checkbox = '<input class="' + colName + '-checkbox';
                    if (!!row[colName]) {
                        checkbox += ' tickedCheckbox';
                    }
                    checkbox += '" type="checkbox"/>';
                    tr.push(checkbox);
                } else if (!!formatter) { // User defined formatter takes precedence over format string..
                    var formattedValue = formatter(this.__columns[i], data[i]);
                    tr.push(formattedValue);
                } else {
                    var value = !!format ? $.format(format, data[i]) : data[i];
                    var displayValue = (typeof value === 'object') && (value != null)? value[0] : value;
                    
                    // null or undefined
                    if (displayValue == null) {
                        tr.push('<div>&nbsp;</div>');
                    } else {
                        tr.push('<div>' + displayValue + '</div>');
                    }
                }
                
                tr.push('</td>');
            }
        }
        
        tr.push('</tr>');
        
        var html = tr.join('');
        return html;
    };

    $.fn.tabular = function(config) {
        var tabular = new Tabular(this, config);
        return tabular;
    };

    Tabular.prototype.getTable = function() {
        return this.__table;
    };

    Tabular.prototype.getTableRow = function(recordId) {
    	if (this.__config.renderOnlyVisibleRows === true) {
    		throw new Error("getTableRow(): Unsupported method with renderOnlyVisibleRows option.");
    		return null;
    	}
    	 
        var modelIndex = this.__recordIdToModelIndex[recordId];

        if (modelIndex != null) {
            var tr = this.__table.find('tbody tr')[modelIndex];
            if (!!tr) {
                return $(tr);
            }
        }

        return null;
    };

    Tabular.prototype.getScroller = function() {
        return this.__scroller;
    };
    
    Tabular.prototype.addFilterFunction = function(filterFunction) {
        this.__filterFunctions.push(filterFunction);
    };
    
    Tabular.prototype.clearFilterFunctions = function() {
        this.__filterFunctions = [];
    };
    
    Tabular.prototype.applyFilters = function(row) {
    	var result = true;
    	
    	/* If we have an optional filter, init visible to false,
    	 * the optional filters will set it to visible if it match
    	 */
    	for (var i in this.__filterFunctions) {
        	if (!!this.__filterFunctions[i].optional) {
        		result = false;
        		break;
        	}
    	}
    	
        for (var i in this.__filterFunctions) {
        	var filter = this.__filterFunctions[i];
            if (!filter.optional && filter.filter(row) == false) {
                return false;
            }
            
            if (!!filter.optional && filter.filter(row) == true ) {
            	result = true;
            }
        }
        
        return result;
    };
    
    Tabular.prototype.applyFilter = function(filters) {
        // FIXME - this method should not exist in tabular.js
        if (!!window.console) {
            console.log('jquery.tabular.js::applyFilter FIXME');
        }
       	
        if(filters != null) {
            for (var k in this.__model) {
                var row =  this.__model[k];
                var visible = true;
                for(var i in filters) {
                    var filter = filters[i];
                    var columns = filter.columns;
                    var value = filter.value;
                    var type = filter.type;
                    
                    visible = false;
                    for(index in columns) {
                        var column = columns[index];
                        
                        var columnNumber = null;
                        for (var j in this.__columns) {
                            columnObj = this.__columns[j];
                            if (columnObj.name == column) {
                                columnNumber = j;
                                break;
                            }
                        }

                        if(columnNumber != null) {
                            if (type == "checkBox") {
                                if (!!row.data[columnNumber][0]) {
                                    visible = true;
                                    break;
                                }
                            }
                            else if (type == "textBox") {
                                if(!!row.data[columnNumber][0]) {
                                    if(row.data[columnNumber][0].toUpperCase().indexOf(value.toUpperCase()) >= 0) {
                                        visible = true;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    if (!visible) {
                        break;
                    }
                }
                this.__setRowVisibility(k, visible);
            }
        } else {
            this.showAllRows();
        }

        this.adjustHeaderWidths();
    };
    
    Tabular.prototype.setCellValue = function(rowId, colName, value) {
        var record = this.getRecordById(rowId);
        if (!!record) {
            record[colName] = value;
        }
    };
    
    Tabular.prototype.getRecordById = function(recordId) {
        var pos = this.__recordIdToModelIndex[recordId];
        return this.__fullDataList[pos];
    };
    
    Tabular.prototype.tickCheckboxes = function() {
        $('.tickedCheckbox').each(function() {
            var checkbox = $(this);
            checkbox.prop('checked', true);
        });
    };
    
})(jQuery);
