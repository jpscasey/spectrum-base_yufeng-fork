var nx = nx || {};
nx.ea = nx.ea || {};

nx.ea.ReportDetailTable = (function() {
    function ctor(eventAnalysis, panel) {
        var me = this;

        this.__eventAnalysis = eventAnalysis;
        this.__panel = panel;
        this.__table = null;
        this.__columns = null;
        this.__defaultSorter = null;
        this.__data = null;
        
        $.extend( $.i18n.parser.emitter, {
            // Handle COUNT keywords
            count: function ( nodes ) {
                return nodes[0] + ' ' + $.i18n('Count');
            }
        } );

        nx.rest.eventanalysis.getDetailColumns(function(columns) {
            me.__setColumns(columns);

            nx.rest.eventanalysis.getDetailDefaultSorter(function(defaultSorter) {
                me.__defaultSorter = defaultSorter;
                me.__initialize();
            });
        });
        
    };

    ctor.prototype.__initialize = function() {
        var me = this;

        this.__panel
            .append(
                $("<div>")
                    .attr("id", "eaReportDetailReturn")
                    .on("click", (function(me) {
                        return function() {
                            me.hide();
                            me.__eventAnalysis.__eaExpandButton.show();
                        }
                    })(me))
                    .append($("<img>").attr("src", "img/back2.png"))
                    .append($("<span>").text($.i18n("Overview")))
            )
            .append(
                $("<div>")
                    .attr("id", "eaReportDetailTable")
                    .append($("<table>").addClass("ui-corner-all"))
            );

        this.__table = $('#eaReportDetailTable').tabular({
            'columns': [this.__columns],
            'cellClickHandler': (function(me) {
                return function(cell, data, columnDef) {
                    me.__clickHandler(cell, data, columnDef);
                }
            })(me),
            'cellMouseOverHandler': (function(me) {
                return function(cell, data, columnDef) {
                    me.__mouseoverHandler(cell, data, columnDef);
                }
            })(me),
            'scrollbar': true,
            'fixedHeader': true,
            'renderOnlyVisibleRows' : true
        });
    };

    ctor.prototype.__clickHandler = function(cell, data, columnDef) {
    	var me = this;
    	
        var fleetId = data.data[this.__getColumnPosition("fleetId")];
        var eventId = data.data[this.__getColumnPosition("id")];
        var eventIdList = me.__getEventIdList();
        
        nx.event.open(eventId, fleetId, eventIdList);
    };

    ctor.prototype.__mouseoverHandler = function(cell, data, columnDef) {
        if (columnDef.name == 'faultTypeColor') {
            var faultType = data.data[this.__getColumnPosition("faultType")];
            cell.attr('title', faultType);
        }
    };

    ctor.prototype.__getColumnPosition = function(columnName) {
        for (var i = 0; i < this.__columns.length; i++) {
            if (this.__columns[i].name.toLowerCase() == columnName.toLowerCase()) {
                return i;
            }
        }

        return null;
    };

    ctor.prototype.__setColumns = function(columns) {
        this.__columns = [];

        for (var index in columns) {
            var column = columns[index];

            this.__columns.push({
                'name': column.name,
                'text': column.header,
                'width': column.width + 'px',
                'type': column.type,
                'visible': column.visible,
                'sortable': column.sortable,
                'sortByColumn': column.sortByColumn,
                'format': column.format,
                'handler': column.handler,
                'formatter': this.__formatter
            });
        }
    };

    ctor.prototype.__formatter = function(column, value) {
        if (!!column.format) {
            value = $.format(column.format, value);
        }

        return !!value ? "<div align='center'>" + value + "</div>" : "&nbsp;";
    };

    ctor.prototype.show = function(data) {
        $('#eaReportChartContent').hide();
        $('#eaReportTablePanel').hide();
        this.__data = data;

        //this.__panel.show('slide', {direction: 'right'}, 500);
        this.__panel.show();
        this.__updateTable();
    };

    ctor.prototype.hide = function() {
        if (this.__panel.css('display') != 'none') {
            //this.__panel.hide('slide', {direction: 'right'}, 500);
            this.__panel.hide();
        }
        $('#eaReportChartContent').show();
        $('#eaReportTablePanel').show();
    };
    
    ctor.prototype.__getEventIdList = function() {
    	var me = this;
    	var idList = [];
    	
    	$.each(this.__table.__model, function (index, anEvent) {
    	    var fleetId = anEvent.data[me.__getColumnPosition("fleetId")];
    	    var eventId = anEvent.data[me.__getColumnPosition("id")];
            idList.push(fleetId + "-" + eventId);
        });
    	
    	return idList;
    };

    ctor.prototype.__updateTable = function() {
        if (!this.__data) {
            return;
        }

        var tabularData = [];

        for (var index in this.__data) {
            var reportBean = this.__data[index];

            var dataRow = [];
            for (var columnIndex in this.__columns) {
                var column = this.__columns[columnIndex];
                dataRow.push(reportBean[column.name]);
            }

            tabularData.push({'id': index, 'data': dataRow});
        }

        this.__table.setData(tabularData, true);

        this.__table.sortByColumnName(this.__defaultSorter.columnName);
        if (this.__defaultSorter.desc == true) {
            this.__table.sortByColumnName(this.__defaultSorter.columnName);
        }
    };

    return ctor;
})();
