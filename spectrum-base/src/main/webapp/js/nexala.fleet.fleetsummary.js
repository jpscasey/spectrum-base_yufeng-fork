var nx = nx || {};
nx.fleet = nx.fleet || {};

/** nx.fleet.FleetSummary : Fleet Summary screen */
nx.fleet.FleetSummary = (function() {
    function ctor () {
        var me = this;
        
        this.__ENTER = 13;
        this.__ESC = 27;
        
        this.__fleetConfiguration = null;
        this.__columns = null;
        this.__fleetGrid = $('#fleetGrid');
        this.__grid = null;
        this.__filtersOn = false;

        this.__timestamp = null;
        this.__lastEventsUpdateTime = null;
        this.__selectedFleetId = null;
        
        this.__updateAllAjax = null;

        this.__commonConfiguration = null;
        this.__refreshRate = null;

        var commonConfDfd = $.Deferred();
        var commonConfLoaded = commonConfDfd.promise();
        nx.rest.fleet.getCommonConfiguration(function(commonConfiguration) {
            me.__commonConfiguration = commonConfiguration;
            commonConfDfd.resolve();
        });

        var globalRefreshTimeDfd = $.Deferred()
        var globalRefreshTimeLoaded = globalRefreshTimeDfd.promise();
        nx.rest.system.globalRefreshRate(function(refreshRate) {
            me.__refreshRate = refreshRate;
            globalRefreshTimeDfd.resolve();
        });
        
        $.when(commonConfLoaded, globalRefreshTimeLoaded).done(function() {

            nx.rest.fleet.getFleetTabs(function(response) {
                var fleetInfoList = response;
        		
                if (fleetInfoList != null && (!!me.__commonConfiguration.showSingleFleetTab || fleetInfoList.length > 1)) {
                    
                	$("#fleetGrid").addClass("fleetGridTabs");

                    var ul = $("<ul>");
                    
                    $.each(fleetInfoList, function(index, fleetInfo) {
                        var fleetLabel = fleetInfo.code;
                        if (me.__commonConfiguration.showFleetFullNames) {
                        	fleetLabel = fleetInfo.name;
                        }
                    	var li = $("<li>")
    	                    .append($("<a>")
    	                            .attr("href", "#fleetGrid")
    	                            .attr("id", fleetInfo.code)
    	                            .text(fleetLabel));
                        ul.append(li);
                	});

                    $("#fleetContainer").prepend(ul);

                    $("#fleetContainer").tabs({
                        'show': function(event, ui) {
                            var fleetId = ui.tab.id;

                            me.__timestamp = null;
                            me.__lastEventsUpdateTime = null;

                            me.__fleetGrid.empty();
                            me.__fleetGrid.append("<table class='ui-corner-all'></table>");

                            $("#filtersButtom").remove();
                            $(".filtersPanel").remove();

                            me.__loadFleet(fleetId);

                            /*// FIXME
                                nx.rest.fleet.unitColumsIdentifier(fleetId, function(response) {
                                    me.__columnsUnitIdentifier = response;
                                });*/

                        },
                        
                        'cookie' : { expires: 2 }
                    });

                } else if(fleetInfoList != null && fleetInfoList.length > 0) {
                
                	me.__loadFleet(fleetInfoList[0].code);
                }
                
                nx.main.__eventsVisible = true;
                
                $(".left button.eventButton").hide();
                
                me.__scheduleUpdates();
        	});
        });

        
    };

    ctor.prototype.__loadFleet = function(fleetId) {
        var me = this;
        
        if (me.__updateAllAjax != null) {
            me.__updateAllAjax.abort();
            me.__updateAllAjax = null;
        }
        me.__selectedFleetId = fleetId;
        
        nx.rest.fleet.configuration(fleetId, function(response) {
            me.__fleetConfiguration = response;
            
            me.__userConfiguration = {
            		variable : "fleetConf"+fleetId,
            		data :  {}
            };
            
            if (!!response.userConfiguration) {
            	me.__userConfiguration.data = JSON.parse(response.userConfiguration)
            }
            
            me.__loadColumns(response.columns, response.mouseOverHeaderEnabled);
            me.__initTable();
            me.__loadInitialData();
            me.__addFilterComponent();
        });
    };

    ctor.prototype.__loadColumns = function(columnDefs, mouseOverHeaderEnabled) {
        var colsHeader = [];
        var cols = [];
        
        this.__columns = [];
        this.__headers = [];

        for (var index in columnDefs) {
            var colDef = columnDefs[index];
            var col = {};
            
            col.name = colDef.name;
            col.text = colDef.header;
            col.mouseOverHeader = mouseOverHeaderEnabled ? $.i18n(colDef.header) : null;
            col.formatter = (colDef.type == 'timestamp' ? this.__timeFormatter : this.__formatter);
            
            if (colDef.type == 'group') {
                col.type = 'group';
                col.colspan = colDef.columns.length;
            } else {
                if (colDef.type == 'analogue' || colDef.type == 'digital') {
                    col.type = 'number';
                }
                else {
                    col.type = colDef.type;
                }
                col.spanCssClass = 'tableSpanHeader';
                col.thCssClass = 'tableThHeader';
            }
            
            col.visible = colDef.visible;
            col.sortable = colDef.sortable;
            col.handler = colDef.handler;
            col.styles = colDef.styles;
            col.cssClass = colDef.cssClass;
            
            if (!!colDef.rowSpan) {
                col.rowspan = colDef.rowSpan;
            }
            
            if (!!colDef.colSpan) {
                col.colspan = colDef.colSpan;
            }
            
            if (colDef.width != null && colDef.width != undefined) {
                col.width = colDef.width + 'px';
            }
            
            if (col.type == 'group') {
                colsHeader.push(col);
            } else {
                cols.push(col);
            }
        }
        
        this.__headers = colsHeader;
        this.__columns.push(cols);
    };
    
    ctor.prototype.__initTable = function() {
        var me = this;
        
        var columns = [];
        
        if (this.__headers.length != 0) {
            columns.push(this.__headers);
        }
        
        columns.push(this.__columns[0]);
        
        this.__tableInit = false;
        
        this.__grid = this.__fleetGrid.tabular({
            'columns': columns,
            'cellClickHandler': (function(me) {
                return function(cell, data, columnDef) {
                    me.__clickHandler(cell, data, columnDef);
                };
            })(me),
            'cellMouseOverHandler': (function(me) {
                return function(cell, data, columnDef) {
                    me.__mouseoverHandler(cell, data, columnDef);
                };
            })(me),
            'scrollbar': true,
            'fixedHeader': true,
            'sortCallback':  (function(me) {
                return function(columnName, defSortDesc) {
                	if (!!me.__tableInit) {
	                	me.__userConfiguration.data.sort = {
	                			columnName: columnName,
	                			desc: defSortDesc
	                	};
	                	
	                    nx.rest.system.setUserConfiguration(me.__userConfiguration, function() {
	                    	
	                    });
                	}
                };
            })(me)
        });
        
        // Apply initial sorting to the table
        var sort;
        if (!!me.__userConfiguration.data.sort) {
        	sort = me.__userConfiguration.data.sort;
        } else {
        	sort = this.__fleetConfiguration.sorter;
        }

        this.__grid.sortByColumnName(sort.columnName);
        if (sort.desc == true) {
            me.__grid.sortByColumnName(sort.columnName);
        }
        
        this.__tableInit = true;
    };

    ctor.prototype.__loadInitialData = function() {
        var me = this;
        
        me.__updateAllAjax = nx.rest.fleet.all(this.__selectedFleetId, function(results) {
            me.__timestamp = results.timestamp;
            me.__setData(results.data); 
        }).done(function() {
            me.__updateAllAjax = null;
        });
    };

    ctor.prototype.__setData = function(data) {
        var gridData = [];
        
        for (var id in data) {
            var unit = data[id];
            var dataRow = [];
            
            for (var i = 0, l = this.__columns[0].length; i < l; i++) {
                var column = this.__columns[0][i];
                var channel = unit.channels[column.name];
                dataRow.push(channel);
            }
            
            gridData.push({'id': id, 'data': dataRow});
        }
        
        this.__grid.setData(gridData, true);
        
        if(this.__filtersOn) {
        	this.__grid.redraw();
        }
    };
    
    ctor.prototype.__scheduleUpdates = function() {
        var me = this;
        
        window.setInterval(function() {
            if(me.__updateAllAjax == null) {
            	 me.__updateAllAjax = nx.rest.fleet.all(me.__selectedFleetId, function(results) {
                     me.__timestamp = results.timestamp;
                     me.__setData(results.data); 
                 }).done(function() {
                     me.__updateAllAjax = null;
                 });
            }
        }, me.__refreshRate);
    };

    ctor.prototype.__hasNewFormation = function(data) {
        for (var id in data) {
            if (!this.__grid.hasRecordId(id)) {
                return true;
            }
        }
        
        var tableIds = this.__grid.getRecordIds();
        for (var id in tableIds) {
            if (!data[id]) {
                return true;
            }
        }
        
        return false;
    };

    ctor.prototype.__openDrillDown = function(unitIds, group) {
        if (!unitIds) {
            console.error("nexala.fleet.fleetsummary.js (openDrillDown) ::: selected unit ids not available");
            return;
        }

        if (!group) {
            console.error("nexala.fleet.fleetsummary.js (openDrillDown) ::: selected group not available");
            return;
        }

		// XXX fleetId should be first param
        new nx.fleet.DrillDown(group, unitIds, this.__selectedFleetId, this.__refreshRate);
    };

    ctor.prototype.__columnDefPosition = function(name) {
        for (var i = 0; i < this.__columns[this.__columns.length - 1].length; i++) {
            var col = this.__columns[this.__columns.length - 1][i];
            
            if (col.name.toLowerCase() == name.toLowerCase()) {
                return i;
                brake;
            }
        }
        
        return null;
    };

    ctor.prototype.__clickHandler = function(cell, data, columnDef) {
        if (columnDef.handler == 'openDrillDown') {
        	$("body").addClass("wait");
            var group = {'name': columnDef.name, 'header': columnDef.text };
            var formationColPos = this.__columnDefPosition('formation');
            var unitIds = data.data[formationColPos][0];
            
            this.__openDrillDown(unitIds, group);
        } else if (columnDef.handler == 'openUnitSummary') {
            var stockNumber = null;
            
            if (columnDef.type == 'stock') {
                stockNumber = data.data[this.__columnDefPosition(columnDef.name)][0];
            }
            if (stockNumber == null) {
                // otherwise find the first stock type column
                for (var i = 0, l = this.__columns[0].length; i < l; i++) {
                    var next = this.__columns[0][i];
                    
                    if (next.type == 'stock' && data.data[i][0] != null) {
                        stockNumber = data.data[i][0];
                        break;
                    }
                }
            }
            
            // FIXME should only publish stockId
            if (stockNumber == null) {
                nx.hub.unitChanged.publish({'id': data.id, 'fleetId': this.__selectedFleetId});
            } else {
                nx.hub.unitChanged.publish({'unitNumber': stockNumber, 'fleetId': this.__selectedFleetId});
            }
        }
    };

    ctor.prototype.__mouseoverHandler = function(cell, data, columnDef) {
        var me = this;
        if (columnDef.name.toLowerCase() == 'location') {
            var code = $.trim(cell.text());
             if (code == '' || code == ' ') {
                return;
            }
            
            nx.rest.location.byCode(this.__selectedFleetId, code, function(response) {
                if (response != null) {
                    var title = [$.i18n('Code') , ': ', response.code, '\n'];
                    title.push($.i18n('Name') , ': ', response.name, '\n'); 
                    if(response.code != response.tiploc) {
                        title.push($.i18n('Tiploc') , ': ', response.tiploc, '\n');
                    }
                    title.push($.i18n('Latitude') , ': ', response.latitude, '\n');
                    title.push($.i18n('Longitude') , ': ', response.longitude, '\n');
                    title.push($.i18n('Type') , ': ', response.type);
                   
                    cell.attr('title', title.join(''));
                }
            });
            
           
        }
        
        if (columnDef.name.toLowerCase() == 'headcode') {
            var code = $.trim(data.data[0][0]);
            if (code == '') {
                return;
            }
            var diagramColumn = me.__columns[0].length - 1;
            
            for(var i in me.__columns[0]) {
                var col = me.__columns[0][i];
                if(col.name == "Diagram") {
                    diagramColumn = i;
                }
            }
            var title = $.trim(data.data[diagramColumn][0]);
            if (!title || title == '') {
                title = 'No diagram';
            }
            
            cell.attr('title', title);
        }
        
        if (columnDef.name.toLowerCase() == 'vehiclenumberlisttrunc') {
            var vehicleNumberListColumn = -1;
            
            for (var i in me.__columns[0]) {
                var col = me.__columns[0][i];
                if (col.name.toLowerCase() == "vehiclenumberlist") {
                    vehicleNumberListColumn = i;
                }
            }
            
            var title = $.trim(data.data[vehicleNumberListColumn][0]);
            if (!title || title == '') {
                title = 'No vehicle list';
            }
            
            cell.attr('title', title);
        }
    };

    /**
     * Applies a style to a column based on the category and the specified
     * styles map (which maps categories to class names).
     */
    ctor.prototype.__formatter = function(column, values) {
        if (!!values) {
            
            var value = "&nbsp;";
            if (values[0] != null && values[0] !== '') {
                value = values[0];
            }
            var category = values[1];
            
            var className = !!column.styles ? column.styles[nx.util.CATEGORIES[category]] : null;
            
            if (!!className) {
                return '<div class = "' + className + '">' + value + '</div>';
            } else {
                return '<div class = "fleet_default">' + value + '</div>';
            }
        } else {
            return '<div class = "fleet_default">&nbsp;</div>';
        }
    };

    ctor.prototype.__timeFormatter = function(column, values) {
        if (!!values) {
            var value = values[0];
            
            if (value != null) {
                value = $.format("{date}", value);
            } else {
                value = '&nbsp;';
            }
            
            var category = values[1];
            var className = !!column.styles ? column.styles[nx.util.CATEGORIES[category]] : null;
            
            if (!!className) {
                return '<div class = "' + className + '">' + value + '</div>';
            } else {
                return '<div class = "fleet_default">' + value + '</div>';
            }
        } else {
            return '<div class = "fleet_default">&nbsp;</div>';
        }
    };
    
    ctor.prototype.__addFilterComponent = function() {
        var me = this;

        me.__filterList = this.__fleetConfiguration.fleetFilters;

        nx.main.removeFiltersButton();
        nx.main.addFilters(me.__filterList, me.__userConfiguration.data.filters);
        me.__applyFilters(false);
        me.__updateFilterIcon();

        $('#cancelFilterButton').click( function(e) {
            for (var i in me.__filterList) {
                var filter = me.__filterList[i];
                if (filter.type == 'textBox' || filter.type == 'minTextBox' || filter.type == 'maxTextBox'){
                    $('#' + filter.id).val("");
                }else{// checkbox
                    $('#' + filter.id).removeAttr("checked");
                }
            }
            $.uniform.update();
            me.__filtersOn = false;
        }).addClass('ui-button-secondary');

        $('#applyFilterButton').click( function(e) {
            me.__filtersOn = true;
            me.__applyFilters (true);
            me.__updateFilterIcon();
            nx.main.hideFilters();
        }).addClass('ui-button-primary');
        $('.filtersPanel').keydown(function(evt) {
            if (evt.which == me.__ENTER) {
                evt.preventDefault();
                // hide..
                me.__filtersOn = true;
                me.__applyFilters(true);
                nx.main.hideFilters();
            }

            if (evt.which == me.__ESC) {
                evt.preventDefault();
                // hide..
                for (var i in me.__filterList) {
                    var filter = me.__filterList[i];
                    if (filter.type == 'textBox' || filter.type == 'minTextBox' || filter.type == 'maxTextBox'){
                        $('#' + filter.id).val("");
                    }else{// checkbox
                        $('#' + filter.id).removeAttr("checked");
                    }
                }
                me.__filtersOn = false;
                me.__applyFilters(true);
                nx.main.hideFilters();
            }
        });
    };
    
    ctor.prototype.__applyFilters = function(saveFilters) {
        var me = this;
        
        // Reset Filters
        me.__grid.clearFilterFunctions();
        
        var userFilters = {};

        // Add Filters
        for (var i in me.__filterList) {
            var filter = me.__filterList[i];
                
            if (filter.type == 'textBox') {
                var valueFilter =  $('#' + filter.id).val();
                userFilters[filter.id] = valueFilter;
                
                if (!!valueFilter & valueFilter != "") {
                    var filterFunction = [];
                    filterFunction.push('return !!row && (');
                    
                    for (var j in filter.columnIds) {
                        var columnId = filter.columnIds[j];
                     
                        // If the user who is applying the filters has no licence 
                        // for this column this will prevent any error.
                        var pos = this.__columnDefPosition(columnId);
                        if (!pos && pos != 0) {
                            continue;
                        }
                        
                        if (j > 0) {
                            filterFunction.push(' || ');
                        }
                        
                        filterFunction.push('(!!row.data[' + pos + '][0] && String(row.data[' + pos + '][0]).toUpperCase().indexOf("' + valueFilter.toUpperCase() + '") >= 0)')
                    }
                    
                    filterFunction.push(');');

                    me.__grid.addFilterFunction({ 
                    	optional: filter.optional,
                    	filter:new Function('row', filterFunction.join(''))
                    });
                }
            } else if (filter.type == "checkBox") { 
                var valueFilter = $('#' + filter.id).attr("checked");
                userFilters[filter.id] = valueFilter;
                    
                if (!!valueFilter) {
                    var filterFunction = [];
                    filterFunction.push('return !!row && (');
                    
                    for (var j in filter.columnIds) {
                        var columnId = filter.columnIds[j];
                        
                        // If the user who is applying the filters has no licence 
                        // for this column this will prevent any error.
                        var pos = this.__columnDefPosition(columnId);
                        if (!pos && pos != 0) {
                            continue;
                        }
                        
                        if (j > 0) {
                            filterFunction.push(' || ');
                        }
                        
                        filterFunction.push('(!!row.data[' + pos + '][0])')
                    }
                    
                    filterFunction.push(');');

                    me.__grid.addFilterFunction({
                    	optional: filter.optional,
                    	filter:new Function('row', filterFunction.join(''))
                    });
                }
            } else { //must be either minTextBox or maxTextBox
                var operator;
                if(filter.type == 'minTextBox') {
                    operator = ">=";
                } else if (filter.type == 'maxTextBox') {
                    operator = "<=";
                }
                
                var valueFilter =  $('#' + filter.id).val();
                userFilters[filter.id] = valueFilter;
                
                if (!!valueFilter & valueFilter != "") {
                    var filterFunction = [];
                    filterFunction.push('return !!row && (');
                    
                    for (var j in filter.columnIds) {
                        var columnId = filter.columnIds[j];
                     
                        // If the user who is applying the filters has no licence 
                        // for this column this will prevent any error.
                        var pos = this.__columnDefPosition(columnId);
                        if (!pos && pos != 0) {
                            continue;
                        }
                        
                        if (j > 0) {
                            filterFunction.push(' || ');
                        }
                        
                        filterFunction.push('(!!row.data[' + pos + '][0] && (row.data[' + pos + '][0]) ' + operator + valueFilter + ')')
                    }
                    
                    filterFunction.push(');');

                    me.__grid.addFilterFunction({ 
                        optional: filter.optional,
                        filter:new Function('row', filterFunction.join(''))
                    });
                }
            } 
            
        }
        
        if (saveFilters) {
        	me.__userConfiguration.data.filters = userFilters;
            nx.rest.system.setUserConfiguration(me.__userConfiguration, function() {
            	
            });
        }
        
        // redraw will use these filters
        me.__grid.redraw();
    };
    
    ctor.prototype.__updateFilterIcon = function() {
    	var me = this;
    	
    	if(me.__checkForActiveFilters() == true) {
    		$('#filtersButton span.ui-icon').addClass('ui-icon-red');
    	} else {
    		$('#filtersButton span.ui-icon').removeClass('ui-icon-red');
    	}
    };
    
    ctor.prototype.__checkForActiveFilters = function() {
    	var me = this;
    	var filterActive = false;
    	
    	for (var i in me.__filterList) {
    		var filter = me.__filterList[i];
    		
    		if (filter.type == 'textBox' || filter.type == 'minTextBox' || filter.type == 'maxTextBox') {
                var valueFilter =  $('#' + filter.id).val();
                
                if (valueFilter != '') {
                	filterActive = true;
                }
                
                
            } else { // checkbox
                var valueFilter = $('#' + filter.id).is(":checked");
                    
                if (valueFilter == true) {
                	filterActive = true;
                }
            }
    	}
    	
    	return filterActive;
    };

    return ctor;
}());

//fleet summary access logging
nx.rest.logging.fleet();

$(document).ready(function() {
    var fleet = new nx.fleet.FleetSummary();
});
