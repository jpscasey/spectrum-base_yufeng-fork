var nx = nx || {};
nx.unit = nx.unit || {};

/** A UnitSummary tab which subscribes to time change information */
nx.unit.TimeAware = (function() {
    function ctor(us) {
        nx.unit.Tab.call(this, us);
        var me = this;
    };
    
    ctor.prototype = new nx.unit.Tab();
        
    return ctor;
})();