var nx = nx || {};

nx.hub = {
    unitChanged: {
        __event: 'unit-change-event',

        /**
         * Fires a unit-change-event, the unitId parameter is the id
         * of the newly selected unit, or null if the user has made an
         * invalid selection.
         * 
         * @param the id of the newly selected unit item or null if the
         *  user has made an invalid selection..
         */
        publish: function(unitId) {
            $.publish(this.__event, unitId);
        },
        
        /**
         * Subscribe to the unit-change-event. This is triggered, when
         * the user interacts with the unit selector.
         * 
         * @param callback this function is called when the
         *  unit-change-event is fired. The callback function should have
         *  one parameter, which will contain the unitId or null if the
         *  user has made an invalid selection.
         */
        subscribe: function(callback) {
            $.subscribe(this.__event, callback);
        },
        
        unsubscribe: function(args) {
            $.unsubscribe(this.__event, args);
        }
    },
    
    stockChanged: {
        __event: 'stock-change-event',
        
        /**
         * Fires a stock-change-event. This event is published when a 
         * stock (vehicle) tab is selected on the dataplots screen.
         * 
         * @param stockId - the newly selected stock
         * @param pane - the jQuery ui tab pane, which will hold the data. 
         */
        publish: function(stockId, pane) {
            $.publish(this.__event, [stockId, pane]);
        },
        
        /**
         * Subscribes to the stock change event.
         * 
         * @param callback this function is called when the
         *  stock-change-event os fired. The callback function should have
         *  two parameters, the first being the stockId selected; the
         *  second is the newly visible stock "tabpane".
         */
        subscribe: function(callback) {
            var me = this;
            
            $.subscribe(this.__event, callback);
            
            return {
                unsubscribe: function() {
                    $.unsubscribe(me.__event, callback);
                }
            };
        },
        
        unsubscribe: function(callback) {
            $.unsubscribe(this.__event);
        }
    },
    
    timeChanged: {
        __event: 'time-change-event',
        
        publish: function(timestamp) {
            $.publish(this.__event, timestamp);
        },
        
        subscribe: function(callback) {
            $.subscribe(this.__event, callback);
        }
    },
    
    startLive: {
        __event: 'live-event',
        
        publish: function() {
            $.publish(this.__event);
        },
        
        subscribe: function(callback) {
            $.subscribe(this.__event, callback);
        }
    },
    
    stopLive: {
        __event: 'stop-live-event',
        
        publish: function() {
            $.publish(this.__event);
        },
        
        subscribe: function(callback) {
            $.subscribe(this.__event, callback);
        }
    },
    
    play: {
        __event: 'play-event',
        
        publish: function() {
            $.publish(this.__event);
        },
        
        subscribe: function(callback) {
            $.subscribe(this.__event, callback);
        }
    },
    
    pause: {
        __event: 'pause-event',
        
        publish: function() {
            $.publish(this.__event);
        },
        
        subscribe: function(callback) {
            $.subscribe(this.__event, callback);
        }
    }
};