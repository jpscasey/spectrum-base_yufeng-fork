var nx = nx || {};
nx.cab = nx.cab || {};


/**
 * The switch component.
 */
nx.cab.Switch = (function () {

    var ctor = function (el, component, settings) {
        nx.cab.Widget.call(this);
        this._component = component;
        this.settings = $.extend({
            onAngle : 270,
            offAngle : 190
        }, settings);
        
        this._paper = el;
        this._draw();
    };
    
    ctor.prototype = new nx.cab.Widget();
    
    ctor.prototype._update = function(channelName, channelValue, channelCategory) {
        
        if(channelValue == true) {
            this._switch.animate({transform: 'R'+this.settings.onAngle+','+this._xCenter+','+this._yCenter}, 500);
        } else if (channelValue == false) {
            this._switch.animate({transform: 'R'+this.settings.offAngle+','+this._xCenter+','+this._yCenter}, 500);
        }

    };
    
    ctor.prototype._reset = function() {
    };
    
    ctor.prototype._draw = function() {
        
        var halfHeight = this._component.height/2;
        var halfWidth = this._component.height/2;
        
        var radius = Math.min(this._component.width, this._component.height)/2;
        this._xCenter = this._component.x + halfWidth;
        this._yCenter = this._component.y + halfHeight;
        
        
        this._paper.setStart();
        
        this._paper.circle(this._xCenter, this._yCenter, radius).attr({
            fill: '0-#999-#333-#999-#333-#333-#888'
        });
        
        var pointerHeight =  this._component.height/6;
        
        var offsetPointer = pointerHeight/2;
        
        this._paper.rect(this._component.x+halfWidth, this._component.y+halfHeight-offsetPointer, halfWidth, pointerHeight).attr({
            fill: '#fff'
        });
        
        this._switch = this._paper.setFinish();
        
        this._switch.attr({
            transform : 'R'+ this.settings.offAngle+','+this._xCenter+','+this._yCenter
        });
        
    };
     
    return ctor;
}());