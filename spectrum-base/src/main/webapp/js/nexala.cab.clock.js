var nx = nx || {};
nx.cab = nx.cab || {};

nx.cab.Clock = (function () {

    var __radiusRoundedCorner = 15;
    var __borderSize = 2;
    var __txtTime;

    var ctor = function (el, component, settings) {
        nx.cab.Widget.call(this);
        this._component = component;
        this.settings = $.extend({
            width: component.width,
            height: component.height
        }, settings);
        
        this._paper = el;
        this._draw();
    };
    
    ctor.prototype = new nx.cab.Widget();
    
    ctor.prototype._update = function(channelName, channelValue, channelCategory) {
        if (!!channelValue) {
            
            var dt = new Date();
            
            dt.setTime(channelValue);

            var hours = dt.getHours();
            var minutes = dt.getMinutes();

            if (hours < 10) 
             hours = '0' + hours;

            if (minutes < 10) 
             minutes = '0' + minutes;

            var time = hours + ":" + minutes;
            
            
            this.__txtTime.attr({
                text : time});
        }
    };
    
  
    
    ctor.prototype._reset = function() {
        this.__txtTime.attr({
            text : '00:00'});
    };
    
    ctor.prototype._draw = function() {
        var xOffset = this._component.x;
        var yOffset = this._component.y;
        
        // Draw clock
        this._paper.rect(xOffset,yOffset,
                this.settings.width,
                this.settings.height,
                __radiusRoundedCorner
            ).attr({
                fill: '90-#222-#333-#444',
                stroke: 'none'});
        
        // Draw border
        this._paper.rect(xOffset+__borderSize, yOffset+__borderSize,
                this.settings.width - (__borderSize*2) ,
                this.settings.height - (__borderSize*2),
                __radiusRoundedCorner).attr({
                    stroke: '#555555'});
        
        this.__txtTime = this._paper.text(xOffset+this.settings.width/2, yOffset+this.settings.height/2, '00:00')
        .attr({'fill': '#ddd',
               'font-weight': 'bold'});

    };
     
    return ctor;
}());