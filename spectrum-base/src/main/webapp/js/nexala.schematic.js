var nx = nx || {};
nx.unit = nx.unit || {};

/**
 * Class which requests configuration values to REST service
 * and generates the HTML code to fill items in combobox and creates
 * canvas for schematics using Raphael.js
 * @author Simon
 */

nx.unit.Schematic = (function() {
    function ctor(us, selectedSchematic, mainCanvas, mainSchematic) {
    	nx.unit.TimeAware.call(this, us);
    	var me = this;
    	
    	this.__schematic = $('#schematicMain');
        this.__schList = null;
        this.__schContainer = null;
        this.__itemValues = null;
        
        this.__data = null;
        
        this.__stockTabs = new nx.unit.StockTabs(
                this.__schematic.find('.vehicleTabs'),
                true,
                false);
        $('.vehicleTabs ul').addClass("ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all");
        
        this.__mainSchematic = mainSchematic;
        this.__mainCanvas = mainCanvas;
        this.__selectedSchematic = selectedSchematic;
        
        me._init();
    };
    
    /** Extend nx.unit.TimeAware */
    ctor.prototype = new nx.unit.TimeAware();
    
    /** Extend nx.unit.Tab */
    ctor.prototype = new nx.unit.Tab();
    
    ctor.prototype._onHide = function() {
        
    };
    
    // block is called every time a user click on the schematic tab
    ctor.prototype._onShow = function() {
    	var me = this;
    	
    	/*this.__unitChangeSub = nx.hub.unitChanged.subscribe(function(evt, unit, objEl) {
    		nx.rest.schematic.stock(unit.fleetId, unit.id, function(stockList) {
    			me.__updateTabs(stockList);
            });
        });*/
    };
    
    ctor.prototype._update = function(data) {
    	var me = this;
    };
    
    ctor.prototype.__updateTabs = function(stockList) {
    	var me = this;
    	me.__stockTabs.clear();
    	me.__stockTabs.setStock(stockList)
    };

    ctor.prototype._logging = function() {
        
    };
    
    ctor.prototype._hasTimeController = function() {
        return true;
    };
    
    // set data which will be shared with costumer javascript class
    ctor.prototype._setData = function(data) {
        var me = this;
    	
    	me.__data = data;
    };
    
    ctor.prototype._init = function() {
    	var me = this;
        // create the main div which will contain the select element
    	// and the div for the schematics   	  
    	$('#absoluteSchematic').show();
        var schematicContent = $("#schematicContent");

        schematicContent.empty();
        schematicContent.append("<select id=\"selectList\"></select>");
        schematicContent.append("<div id=\"schematicContainer\"></div>");
    	
    	me.__schList = $('#selectList');
        me.__schContainer = $('#schematicContainer');
    	
    	// get the list of schematic contained in configuration file    	
    	nx.rest.schematic.getItemsList(function(itemsList) {
    		me.__itemValues = itemsList;
    		
    		// include js code for each schematic
    		for (var i = 0; i < me.__itemValues.length; i++) {
    			var jsFile = me.__itemValues[i]['jsclassPath'];
    			
    			$.getScript(jsFile, function() {
    			    //console.log("successfully imported new js file!");
    			});
    		}
    		
    		// generate html list of items
    		me._displayList();
    		
    		// select the first item in list of schematic
    		me._selectSchematic(me.__itemValues[0]['name']);
    	});
    	
    	// check if user select a different item in list
    	me.__schList.on('change', function() {
    		me._selectSchematic($(this).val());
    	});
    };
    
    // method used to generate the html for the listbox which
    // will contain the list of schematics by name
    ctor.prototype._displayList = function() {
    	var me = this;
    	
    	var isFirst = true;
    	var option_html = '';
    	var canvas_html = '';
    	
    	for (i = 0; i < me.__itemValues.length; i++) {
    		option_html += '<option ' + ((isFirst) ? 'selected' : '') + ' value="' + me.__itemValues[i]['name'] + '">' + me.__itemValues[i]['displayName'] + '</option>';
    		
    		// create a div which will contain a canvas for each item
    		canvas_html += '<div id="'+ me.__itemValues[i]['name'] +'Canvas" style="display: '+ ((isFirst) ? 'block' : 'none') +';"></div>';
    		
    		// show only the first schematic and select only the first item in list
    		isFirst = false;
    	}
    	
    	this.__schContainer.html(canvas_html);
    	this.__schList.html(option_html);
    	
    	// apply the theme to the select tag using jquery.uniform.js
    	this.__schList.uniform();
    	this.__schContainer.uniform();
    };
    
    // method called every time an item from the list of schematic is selected
    // based on the value selected by the user a new raphael object will be instantiated
    // and the schematic will be redrawn
    ctor.prototype._selectSchematic = function(value) {
    	var me = this;
    	
    	// if canvas div hasn't been created yet stop drawing the
    	// raphael diagram on the page
    	if (document.getElementById(value + 'Canvas') == null) {
    		return;
    	}
    	
    	var valueIndex = 0;
    	
    	// hide all schematic and show only the one selected by the user
    	
    	for (i = 0; i < me.__itemValues.length; i++) {
    		$('#' + me.__itemValues[i]['name'] + 'Canvas').hide();
    		$('#' + me.__itemValues[i]['name'] + 'Canvas').html('');
    		
    		// save index of the value chosen
    		if (me.__itemValues[i]['name'] == value) {
    			valueIndex = i;
    		}
    	}
    	
    	$('#' + value + 'Canvas').show();
    	
    	// set width and height for the canvas div    	
        document.getElementById(value + 'Canvas').style.width = me.__itemValues[valueIndex]['canvasSize']['width'] + 'px';
        document.getElementById(value + 'Canvas').style.height = me.__itemValues[valueIndex]['canvasSize']['height'] + 'px';
        
        // instantiate a new raphael object
        me.__mainCanvas = Raphael(document.getElementById(value + 'Canvas'),
        		me.__itemValues[valueIndex]['raphaelSize']['width'],
        		me.__itemValues[valueIndex]['raphaelSize']['height']);
        
        var SchematicClass = window[me.__itemValues[valueIndex]['jsconstructor']];
        
        // create a new class dynamically passing the name of the constructor
        me.__mainSchematic = new SchematicClass(me.__mainCanvas,
        		me.__itemValues[valueIndex]['schematicParams']['xoffset'],
        		me.__itemValues[valueIndex]['schematicParams']['yoffset'],
        		me.__itemValues[valueIndex]['schematicParams']['scale']);
        
        me.__selectedSchematic = value;
    };

    return ctor;
}());