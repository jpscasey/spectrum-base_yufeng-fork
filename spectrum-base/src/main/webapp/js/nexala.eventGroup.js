var nx = nx || {};
nx.event = nx.event || {};

nx.event.EventGroup = (function() {
    function ctor() {
        var me = this;
        this.__groupDialog = $('<div>')
            .attr('id', 'eventGroupDialog');
        this.__groupDialog.append(this.__faultGroupTableDiv);
        this.__faultGroupTable = null;
        
        this.__fleetId = null;
        this.__selectedEventId = null;
        this.__eventPanel = null;
        this.__serviceRequestInList = 0;
        
        this.__existingGroupInList = false;
        this.__selectedGroupId = null;
        this.__selectedServiceRequestId = null;
    
        this.__tableColumns = null;
        
        nx.rest.event.eventGroupConfiguration(function(response) {
            me.__tableColumns = response.columns;
        });
        
        nx.event.eventGroupObj = this;
    };
    
    ctor.prototype.__createTable = function(columns) {
        var me = this;
        
        var faultGroupTableDiv = $('<div>')
            .attr('id', 'faultGroupTable')
            .append('<table cellpadding="0" cellspacing="0" border="0"></table>')
        
        // rebuild the div because if not, we lose the click handler for some reason...
        this.__groupDialog.empty();
        this.__groupDialog.append(faultGroupTableDiv);
        
        var tableColumns = [];
        
        for (var i = 0; i < columns.length; i++) {
            var column = columns[i];
            
            tableColumns.push({
                'name': $.i18n(column.name),
                'text': $.i18n(column.header),
                'width': column.width + 'px',
                'type': column.type,
                'visible': column.visible,
                'sortable': column.sortable,
                'resizable' : column.resizable,
                'autosize' : column.autosize,
                'format': column.format,
                'handler': column.handler,
                'cssClass': column.cssClass
            });
        }
        
        this.__faultGroupTable = faultGroupTableDiv.tabular({
            'columns': tableColumns,
            'cellClickHandler': (function(me) {
                return function(cell, data, columnDef) {
                	if (me.__existingGroupInList == false) {
	                	if((me.__serviceRequestInList > 1 && !!data.isGroupLead) 				// multiple events with SR: pick lead
	                			|| ( me.__serviceRequestInList == 1 && !!data.serviceRequestId) // only one events with SR: default as lead
	                			|| me.__serviceRequestInList == 0) 								// no events with SR, any can be lead
	                	{
	                		me.__selectedEventId = data.id
	                		if (!!data.serviceRequestId) {
	                			me.__selectedServiceRequestId = data.serviceRequestId;
	                		}
	                        $('tr.highlight').removeClass('highlight');
	                        cell.closest('tr').addClass("highlight");
	                	}
                	}
                };
            })(this),
            'cellMouseOverHandler': (function(me) {
                return function(cell, data, columnDef) {
                    me.__mouseoverHandler(cell, data, columnDef);
                };
            })(this),
            'scrollbar': true,
            'fixedHeader': true,
            'renderOnlyVisibleRows' : false
        });
    }
    
    ctor.prototype.__mouseoverHandler = function(cell, data, columnDef) {
        if (columnDef.name == "typeColor") {
            var faultType = data.type;
            cell.attr('title', faultType);
        } else {
            var text = cell[0].children[0].innerHTML;
            if (text != '&nbsp;') {
                cell.attr('title', text);
            }
        }
        
    };
    
    ctor.prototype.show = function(eventList, fleetId, eventPanel) {
        var me = this;
        this.__eventList = eventList;
        this.__fleetId = fleetId;
        this.__eventPanel = eventPanel;
        this.__serviceRequestInList = 0;
        this.__groupEventsLoaded = false;
        this.__existingGroupInList = false;
        this.__selectedGroupId = null;
        this.__selectedServiceRequestId = null;
        
        this.__createTable(this.__tableColumns);
        
        var gridData = [];
        if (!!eventList && eventList.length > 0) {
            for (var id in eventList) {
                gridData.push(eventList[id]);
                
                // scan events for unique service requests
                if (!!eventList[id].serviceRequestId && eventList[id].serviceRequestId != me.__selectedServiceRequestId) {
                	me.__serviceRequestInList++;
                	me.__selectedServiceRequestId = eventList[id].serviceRequestId;
                }
                
                if (!!eventList[id].faultGroupId && me.__selectedGroupId == null) {
                	me.__existingGroupInList = true;
                	me.__selectedGroupId = eventList[id].faultGroupId;
                }
            }
        }
        
        this.__faultGroupTable.setData(gridData, true);
        
        // auto-select lead event if there is only one option
        var newLead = false;
        if(this.__serviceRequestInList == 1) {
                
            $.each(this.__faultGroupTable.__model, function(index, row) {
                if (row.faultGroupId == null && row.serviceRequestId != null) {
                    var entry = me.__faultGroupTable.getTableRow(row.id);
                    me.__selectedEventId = row.id
                    me.__selectedServiceRequestId = row.serviceRequestId;
                    $('tr.highlight').removeClass('highlight');
                    $(entry).addClass("highlight");
                    newLead = true
                    return;
                }
            });
            $.each(this.__faultGroupTable.__model, function(index, row) {
                if (!!row.serviceRequestId && me.__existingGroupInList == false || me.__existingGroupInList == true && row.isGroupLead == true) {
                    var entry = me.__faultGroupTable.getTableRow(row.id);
                    me.__selectedEventId = row.id
                    me.__selectedServiceRequestId = row.serviceRequestId;
                    if (!newLead) {
                        $('tr.highlight').removeClass('highlight');
                        $(entry).addClass("highlight");
                    }
                    return;
                }
            });
        } else if (this.__serviceRequestInList == 0) {
            $.each(this.__faultGroupTable.__model, function(index, row) {
            	if(me.__existingGroupInList == true && row.isGroupLead == true) {
            		var entry = me.__faultGroupTable.getTableRow(row.id);
            		me.__selectedEventId = row.id
            		me.__selectedServiceRequestId = row.serviceRequestId;
                    $('tr.highlight').removeClass('highlight');
                    $(entry).addClass("highlight");
            		return;
            	}
            });
        }
        
        this.__groupDialog.dialog({
            'autoOpen': true,
            'width': 1000,
            'height': 600,
            'modal': true,
            'resizable': false,
            'title': $.i18n('Event Group'),
            'closeOnEscape': true,
            'buttons': [
				{
					text: $.i18n('Create'),
					click: function() {
						if (!!me.__selectedEventId) {
							var leadEventId = me.__selectedEventId;
							$(this).dialog('close');
							if (!!me.__selectedGroupId) {
								me.__doUpdate(leadEventId);
							} else {
								me.__doCreate(leadEventId);
							}
						} else {
							$('<div></div>').dialog({
								buttons: { 'Ok': function () { $(this).dialog('close'); } },
								close: function (event, ui) { $(this).remove(); },
								resizable: false,
								title: $.i18n('Notification'),
								modal: true
							}).text($.i18n('No lead event selected. Please select an event to flag as the lead of this group.'));
						}
					}
				},
				{
					text: $.i18n('Cancel'),
					click: function() {
						$(this).dialog('close');
					}
				}
            ],
            'close': function() {
                me.__selectedEventId = null;
                me.__groupDialog.remove();
            }
        });
    };
    
    ctor.prototype.__doCreate = function(leadEventId) {
        var me = this;
        var userName = '';
        
        nx.rest.system.user(function(user) {
            if(user.name != null && user.name.trim().length > 0) {
                userName = user.name;
            } else {
                userName = user.id;
            }
            
            var eventGroupParams = [];
            for (var i = 0; i < me.__eventList.length; i++) {
                eventGroupParams.push({
                    'faultId': me.__eventList[i].id,
                    'faultGroupId': me.__eventList[i].faultGroupId,
                    'isFaultGroupLead': me.__eventList[i].isGroupLead,
                    'faultRowVersion': me.__eventList[i].rowVersion
                });
            }
            
            $("body").addClass("wait");
            nx.rest.event.createEventGroup(me.__fleetId, eventGroupParams, leadEventId, userName, function(response) {
                $("body").removeClass("wait");

                $('<div></div>').dialog({
                    buttons: { 'Ok': function () { $(this).dialog('close'); } },
                    close: function (event, ui) { $(this).remove(); },
                    resizable: false,
                    title: $.i18n('Notification'),
                    modal: true
                }).text($.i18n('Event Group was created.'));
                
                me.__eventPanel.__doSearch();
            }, function(jqXHR, textStatus, errorThrown) {
                if (jqXHR.status == 409) {
                    $("body").removeClass("wait");
                    alert($.i18n("Event Group could not be created. One of the events was modified since the last search"));
                    
                    var eventIndex = me.__eventIdList.indexOf(me.__event.id);
                    me.show(me.__fleetId, me.__eventIdList[eventIndex], me.__eventIdList);
                }
            });
        });

    };
    
    ctor.prototype.__doUpdate = function(leadEventId) {
    	var me = this;
        var userName = '';
        
        nx.rest.system.user(function(user) {
            if(user.name != null && user.name.trim().length > 0) {
                userName = user.name;
            } else {
                userName = user.id;
            }
            
            var eventGroupParams = [];
            for (var i = 0; i < me.__eventList.length; i++) {
                eventGroupParams.push({
                    'faultId': me.__eventList[i].id,
                    'faultGroupId': me.__eventList[i].faultGroupId,
                    'isFaultGroupLead': me.__eventList[i].isGroupLead,
                    'faultRowVersion': me.__eventList[i].rowVersion
                });
            }
            
            $("body").addClass("wait");
            nx.rest.event.updateEventGroup(me.__fleetId, eventGroupParams, leadEventId, userName, function(response) {
                $("body").removeClass("wait");

                $('<div></div>').dialog({
                    buttons: { 'Ok': function () { $(this).dialog('close'); } },
                    close: function (event, ui) { $(this).remove(); },
                    resizable: false,
                    title: $.i18n('Notification'),
                    modal: true
                }).text($.i18n('Event Group was updated.'));
                
                me.__eventPanel.__doSearch();
            }, function(jqXHR, textStatus, errorThrown) {
                if (jqXHR.status == 409) {
                    $("body").removeClass("wait");
                    alert($.i18n("Event Group could not be updated. One of the events was modified since the last search"));
                    
                    var eventIndex = me.__eventIdList.indexOf(me.__event.id);
                    me.show(me.__fleetId, me.__eventIdList[eventIndex], me.__eventIdList);
                }
            });
        });
    };
    
    ctor.prototype.__isFaultInData = function(faultId, gridData) {
    	for (var i = 0; i < gridData.length; i++) {
    		if (gridData[i].id == faultId) {
    			return true;
    		}
    	}
    	return false;
    };
    
    ctor.prototype.__getRow = function(obj) {
        var row = [];
        
        for (var i in this.__tableColumns) {
            var column = this.__tableColumns[i];
            row.push($.i18n(obj[column.name]));
        }
        
        return row;
    };

    
    return ctor;
}());

nx.event.eventGroupObj = null;

nx.event.group = function(eventList, fleetId, eventPanel) {
    if (eventList.length > 1) {
        if (nx.event.eventGroupObj == null) {
            nx.event.eventGroupObj = new nx.event.EventGroup();
        }
    
        nx.event.eventGroupObj.show(eventList, fleetId, eventPanel);
    } else {
        $('<div></div>').dialog({
            buttons: { 'Ok': function () { $(this).dialog('close'); } },
            close: function (event, ui) { $(this).remove(); },
            resizable: false,
            title: $.i18n('Notification'),
            modal: true
        }).text($.i18n('Two Events needed to create Event Group.'));
    }
};
