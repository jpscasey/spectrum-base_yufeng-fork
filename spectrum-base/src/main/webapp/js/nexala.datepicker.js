var nx = nx || {};

nx.datepicker = function() {
    var to = new Date();
    var from = new Date(to.getTime() - 1000 * 60 * 60 * 24 * 30);

    $('.datepicker_calendar').DatePicker({
        inline: true,
        date: [from, to],
        calendars: 3,
        mode: 'range',
        current: new Date(to.getFullYear(), to.getMonth() - 1, 1),
        onChange: function(dates,el) {
            // update the range display
            $('.date_range_field span').text(dates[0].getDate()+' '+dates[0].getMonthName(true)+', '+
                    dates[0].getFullYear()+' - '+
                    dates[1].getDate()+' '+dates[1].getMonthName(true)+', '+dates[1].getFullYear());
        }
    });

    // initialize the special date dropdown field
    $('.date_range_field span').text(from.getDate()+' '+from.getMonthName(true)+', '+from.getFullYear()+' - '+
            to.getDate()+' '+to.getMonthName(true)+', '+to.getFullYear());

    // bind a click handler to the date display field, which when clicked
    // toggles the date picker calendar, flips the up/down indicator arrow,
    // and keeps the borders looking pretty
    $('.date_range_field').bind('click', function(){
        $('.datepicker_calendar').toggle();
        if($('.date_range_field a').text().charCodeAt(0) == 9660) {
            // switch to up-arrow
            $('.date_range_field a').html('&#9650;');
            $('.date_range_field').css({borderBottomLeftRadius:0, borderBottomRightRadius:0});
            $('.date_range_field a').css({borderBottomRightRadius:0});
        } else {
            // switch to down-arrow
            $('.date_range_field a').html('&#9660;');
            $('.date_range_field').css({borderBottomLeftRadius:5, borderBottomRightRadius:5});
            $('.date_range_field a').css({borderBottomRightRadius:5});
        }
        return false;
    });
};