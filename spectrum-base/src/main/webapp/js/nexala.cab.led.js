var nx = nx || {};
nx.cab = nx.cab || {};

nx.cab.Led = (function () {
    var COLOR1 = "#eabe3f";
    var COLOR2 = "#e8cf8a";
    var BORDER = "#666";
    var BLACK = "#000";
    var SECTIONS = 20;

    var ctor = function (el, component, settings) {
        nx.cab.Widget.call(this);
        this._component = component;
        this.settings = $.extend({
            onColor: 'red',
            offColor: 'green',
            invalidColor: 'gray',
            width: component.width,
            height: component.height
        }, settings);
        
        this._paper = el;
        this._draw();
    };
    
    ctor.prototype = new nx.cab.Widget();
    
    ctor.prototype._update = function(channelName, channelValue, channelCategory) {
        
        if(this.settings.channelCatDisplay) {
            // Change the color depending on the channel category
            var color = this.settings.categoryColor[channelCategory];
            if(color != null) {
                this.__button[0].attr({
                    fill: color
                });
            } else {
                this.__invalid();
            }
        } else {
            if (channelValue == null) {
                this.__invalid();
            } else if (channelValue == this.settings.onValue) {
                this.__on();
            } else {
                this.__off();
            }
        }
        
        
        
    };
    
    ctor.prototype._reset = function() {
        this.__invalid();
    };
    
    ctor.prototype._draw = function() {
        var xOffset = this._component.x;
        var yOffset = this._component.y;
        
        this.__button = this._paper.set(
            this._paper.rect(
                xOffset,
                yOffset,
                this.settings.width,
                this.settings.height,
                2
            ).attr({
                fill: this.settings.invalidColor,
                stroke: 'none'
            })
        );
    };
    
    ctor.prototype.__invalid = function() {
        this.__button[0].attr({
           fill: this.settings.invalidColor
        });
    };
    
    ctor.prototype.__on = function() {
        this.__button[0].attr({
            fill: this.settings.onColor
        });
    };
    
    ctor.prototype.__off = function() {
        this.__button[0].attr({
            fill: this.settings.offColor
        });
    };
    
    return ctor;
}());