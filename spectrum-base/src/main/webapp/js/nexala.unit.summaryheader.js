var nx = nx || {};
nx.unit = nx.unit || {};

nx.unit.summaryHeader = (function () {
    function ctor(stockSummary) {
        nx.rest.logging.unit();

        this.__stockId = null;
        this.__unitFormationSelector = $('<div>').attr('id', 'unitFormationSelector');
        $('#unitSummaryHeader #left').append(this.__unitFormationSelector);
        
    };
    
    ctor.prototype._update = function(data) {
        var me = this;
        
        var unitSummaryHeaderDfd = $.Deferred()
        var unitSummaryHeaderLoaded = unitSummaryHeaderDfd.promise();
        if (this.__stockId != data.unitId && typeof me.__conf != null) {
        	nx.rest.unitSummary.conf(data.fleetId, function(response) {
	        	me.__conf = response;
	        	this.__stockId = data.unitId;
	        	unitSummaryHeaderDfd.resolve();
	        });
        }
        
        if (this.__stockId != data.unitId) {
            this.__stockId = data.unitId;
        	$.when(unitSummaryHeaderLoaded).done(function() {
        		me.__updateFormation(data.fleetId, data.unitId);
        	});
        }
    };
    
    ctor.prototype.__updateFormation = function(fleetId, stockId) {
        if (!fleetId || !stockId) {
            return;
        }
        
        var me = this;
        
        this.__unitFormationSelector.html("");
        
        nx.rest.unit.formationUnits(stockId, fleetId, function(response) {
            for (var i = 0, l = response.length; i < l; i++) {
                var unit = response[i];
                var unitHeaderNumber = unit.unitNumber;
                if (me.__conf.unitSummaryHeaderCombined) {
	                if (unit.vehicles.length > 1) {
	                	unitHeaderNumber = unit.vehicles[0].vehicleNumber;
	                	for (var i = 1; i < unit.vehicles.length; i++) {
	                		var vehicle = unit.vehicles[i]; 
	                		unitHeaderNumber += (" - " + vehicle.vehicleNumber);
	                	}
                	}
                } 
                
                if (unit != null) {
                    $('<a>')
                        .data('unitId', unit.id)
                        .text(unitHeaderNumber)
                        .addClass('formationUnit')
                        .addClass(unit.id == stockId ? 'selectedFormationUnit' : '')
                        .click(
                            function (event) { 
                                event.preventDefault();
                                nx.hub.unitChanged.publish({id: $(this).data('unitId'), fleetId: fleetId, sameFormation : true});
                            }
                        )
                        .appendTo(me.__unitFormationSelector);
                }
            }
        });
    };

    return ctor;
})();
