var nx = nx || {};
nx.cab = nx.cab || {};

nx.cab.Caws = (function () {
    var ctor = function (el, component) {
        this._COLOR_RED_ON = '#E0091D';
        this._COLOR_RED_OFF = '#63010A';
        this._COLOR_GREEN_ON = '#13A106';
        this._COLOR_GREEN_OFF = '#042901';
        this._COLOR_YELLOW_ON = '#F5F70C';
        this._COLOR_YELLOW_OFF = '#61610A';
        
        nx.cab.Widget.call(this);
        this._leds = [];
        this._height = component.height;
        this._width = component.width;
        this._component = component; 
        this._paper = el;
        this._draw();
        
        
    };
    
    ctor.prototype = new nx.cab.Widget();

    ctor.prototype._draw = function() {
        
        var xOffset = this._component.x;
        var yOffset = this._component.y;
        
        var padding = this._height / 15,
            ledHeight = (this._height - padding) / 5,
            ledPadding = padding / 4;

         this._leds.push(
            this._led(xOffset, yOffset, this._width, ledHeight, this._COLOR_GREEN_ON, this._COLOR_GREEN_OFF)
        );
        
        this._leds.push(
            this._led(xOffset, yOffset + ledHeight + ledPadding, this._width, ledHeight, this._COLOR_YELLOW_ON, this._COLOR_YELLOW_OFF)
        );
        
        this._leds.push(
            this._led(xOffset, yOffset+ (ledHeight + ledPadding) * 2, this._width, ledHeight, this._COLOR_YELLOW_ON, this._COLOR_YELLOW_OFF)
        );
        
        this._leds.push(
            this._led(xOffset, yOffset + (ledHeight + ledPadding) * 3, this._width, ledHeight, this._COLOR_YELLOW_ON, this._COLOR_YELLOW_OFF)
        );
        
        this._leds.push(
            this._led(xOffset, yOffset + (ledHeight + ledPadding) * 4, this._width, ledHeight, this._COLOR_RED_ON, this._COLOR_RED_OFF)
        );
    };
    
    ctor.prototype._reset = function() {
        for (var i = 0, l = this._leds.length; i < l; i++) {
            var led = this._leds[i];
            this._off(led);
        }
    };
    
    ctor.prototype._update = function(channelName, channelValue, channelCategory) {
        // Turn off all leds
        for (var i = 0, l = this._leds.length; i < l; i++) {
            var led = this._leds[i];
            var bit = 1 << i;

            if ((channelValue & bit) == bit) {
                this._on(led);
            } else {
                this._off(led);
            }
        }
    };

    ctor.prototype._off = function(led) {
        led[2].hide();
        led[3].hide();
    };
    
    ctor.prototype._on = function(led) {
        led[2].show();
        led[3].show();
    };
    
    ctor.prototype._led = function(x, y, width, height, onColor, offColor) {
        
        var rect0 = this._paper.rect(x, y, width, height).attr({
                fill: 'silver'
        });
        
        /*
        var rect2 = this._paper.rect(
            x + height / 8,
            y + height / 8,
            width - (height / 4),
            height - (height / 4)
        ).attr({
            stroke: 'none',
            fill: offColor
        });*/
        
        var rect1 = this._paper.rect(
            x + height / 8 + 2,
            y + height / 8 + 2,
            width - height / 4 - 4,
            height - height / 4 - 4 
        ).attr({
            fill: offColor,
            stroke: 'rgba(0, 0, 0, 0.3)',
            'stroke-width': 3
        });
        
        var rect2 = this._paper.rect(
            x + height / 8,
            y + height / 8,
            width - (height / 4),
            height - (height / 4)
        ).attr({
            stroke: 'none',
            //fill: '180-' + onColor + '-' + offColor + '-' + onColor
            fill: onColor
        }).hide();
        
        var glow = rect2.glow({
            color: onColor,
            width: height / 4,
            opacity: 0.4
        }).hide();
        
        return this._paper.set(
            rect0,
            //rect2,
            rect1,
            rect2,
            glow
        );
    };

    return ctor;
}());
