var nx = nx || {};
nx.cab = nx.cab || {};

nx.cab.Slider = (function () {

    var ctor = function (el, component, settings) {
        nx.cab.Widget.call(this);
        this._component = component;
        this._settings = settings;

        this._handle = null;
        this._height = component.height;
        this._width = component.width;
        this._paper = el;
        this._draw();
        this._handle.hide();
        this._defaultPosition = 0;
    };
    
    ctor.prototype = new nx.cab.Widget();
    
    ctor.prototype._draw = function() {
        var compXOffset = this._component.x;
        var compYOffset = this._component.y;
        
        var yoffset = 4;
        var runnerWidth = this._width * 0.3;
        var labelsWidth = this._width;
        var height = this._height; 

        var labels = this._paper.rect(
                compXOffset, 
                compYOffset, 
                labelsWidth, 
                height).attr({
            fill: '#222',
            stroke: 'none',
            'stroke-width': 0
        });
        
        var runner = this._paper.rect(compXOffset + labelsWidth - runnerWidth - 3 , compYOffset + 3, runnerWidth, height - 6).attr({
            fill: '#000'
        });

        var min = this._settings.minValue;
        var max = this._settings.maxValue;
        
        var majorIncrement = this._settings.majorIncrement;
        var numMajorIncrements = (max - min) / majorIncrement;
        
        var minorIncrement = this._settings.minorIncrement;
        
        var usableHeight = height - yoffset * 2;
        var labelHeight = usableHeight - 6 
        
        var path = "";

        for (var i = 0, val = min; val <= max; i++, val += majorIncrement) {
            var x = compXOffset + labelsWidth - runnerWidth - 3;
            var y = compYOffset + yoffset + labelHeight - (i * labelHeight) / (numMajorIncrements) + 3;

            path += ['M', x, y, 'L', x + 9, y].join(',');
            
            var numMinorIncrements = majorIncrement / minorIncrement;
            var nextY = compYOffset + yoffset + labelHeight - ((i + 1) * labelHeight) / (numMajorIncrements);
            for (var j = 1, minorVal = val + minorIncrement; minorVal < val + majorIncrement && minorVal < max; j++, minorVal += minorIncrement) {
                var minorY = y - (((y - nextY) / numMinorIncrements) * j);
                path += ['M', x, minorY, 'L', x + 5, minorY].join(',');
            }

            
            var lbl = jQuery('<div/>', {
                css: {
                    right: (this._paper.width - x + 3)+'px',
                    top:  (y-7)+'px',
                    color: 'white',
                    'font-size': '9px'
                },

                text: val
            }).addClass('valueLabel');

            this._paper.container().append(lbl);

        }
        
        this._paper.path(path).attr({
            stroke: '#FFF',
            'stroke-width': 2,
            'text-anchor': 'start'
        });

        this._xoffset = this._width - 13;
        this._yoffset = yoffset;
        this._height = usableHeight;

        this._drawSlider();
    };

    ctor.prototype._drawSlider = function() {
        var compXOffset = this._component.x;
        var compYOffset = this._component.y;
        var x = this._xoffset+ compXOffset;
        var y = Math.round(this._yoffset+compYOffset + this._height - 3);
        var min = this._settings.minValue;
        var max = this._settings.maxValue;

        this._handle = this._paper.set(
            this._paper.path("M" + (x + 10) + " " + (y + 2) + " L" + x + " " + y + " " + (x + 10) + " " + (y - 2) + " Z").attr({
                stroke: '#FF0',
                fill: "#FF0",
                'stroke-width': 2
            })
        );
        
    };
    
    ctor.prototype._update = function(channelName, channelValue, channelCategory) {       
        if (channelValue == null) {
            this._handle.hide();
        } else {
            var minValue = this._settings.minValue;
            var maxValue = this._settings.maxValue;
            
            var minPosition = this._component.y + this._yoffset + this._height - 3;
            var maxPosition = this._component.y + this._yoffset + 3;
            
            var cy = minPosition - ((minPosition - maxPosition) * (channelValue - minValue) / (maxValue - minValue))
            
            cy = Math.max(cy, maxPosition);
            cy = Math.min(cy, minPosition);
            
            var moveY = cy - minPosition;
            
            this._handle.show();
            this._handle.animate({transform: 'T0,' + moveY}, 1000);
        }
    };
    
    ctor.prototype._reset = function() {
        this._handle.hide();
    }

    return ctor;
}());
