var nx = nx || {};
nx.cab = nx.cab || {};

nx.cab.SimpleLever = (function () {

    var COLOR1 = "#eabe3f";
    var COLOR2 = "#e8cf8a";
    var BORDER = "#666";
    var BLACK = "#000";
    var SECTIONS = 20;

    var ctor = function (el, component, settings) {
        nx.cab.Widget.call(this);
        this._component = component;
        this._handle = null;
        this._cx = this._component.x+ component.width / 2;
        this._cy = this._component.y+component.height / 2;
        this._radius = Math.min(component.width / 2, component.height / 2);
        this._paper = el;
        this._draw();
    };
    
    ctor.prototype = new nx.cab.Widget();
    
    ctor.prototype._draw = function() {
        this._paper.circle(
            this._cx,
            this._cy,
            this._radius
        ).attr({
            fill: '290-#c0c0c0-#555'
        });

        this._paper.circle(
            this._cx,
            this._cy,
            this._radius / 1.2
        ).attr({
            fill: 'silver',
            stroke: 'none'
        });

        this._paper.circle(
            this._cx,
            this._cy,
            this._radius / 1.5 
        ).attr({
            fill: '#777',
            stroke: 'none'
        });
        
        this._paper.circle(
            this._cx,
            this._cy,
            this._radius / 2.5 
        ).attr({
            fill: '#666',
            stroke: 'none'
        });

        this._drawHandle(this._cx, this._cy + 8, this._radius / 2);
    };

    ctor.prototype._drawHandle = function(x, y, r) {
        this._handle = this._paper.set(
            this._paper.circle(x, y, r).attr({
                fill: 'r#000-#222',
                stroke: '#333',
                'stroke-width': '1'
            }),

            this._paper.ellipse(x, y, r - r/5, r - r/20).attr({
                stroke: 'none',
                fill: 'r(.5, .1)#999-#999',
                opacity: 0
            })
        );
    };
    
    ctor.prototype._update = function(channelName, channelValue, channelCategory) {
        if (channelValue == 1) {
            this._on();
        } else {
            this._off();
        }
    };
    
    ctor.prototype._on = function() {
        this._handle.transform("t0, -16");
    };
    
    ctor.prototype._off = function() {
        this._handle.transform("t0, 0");
    };
    
    ctor.prototype._reset = function() {
        this._off();
    };

    return ctor;
}());
