var nx = nx || {};
nx.event = nx.event || {}; 

nx.event.EventChannelDetail = (function() {
    function ctor() {
        var me = this;

        this.__groupDfd = $.Deferred();
        this.__groupsLoaded = this.__groupDfd.promise();
        
        this.__configuration = null;
        
        this.__group = null;
        
        this.__event = null;
        
        this.__fleetId = null;
        this.__eventId = null;
        
        this.__eventGroupId = null;
        
        this.__stockTabs = null;
        
        this.__stockChangeSuscribed = false;
        
        this.__parent = null;
        this.__period =null; 
    };
    
    
    ctor.prototype.render = function(rootPane, configuration, event, parent) {
    	
    	var me = this;
    	this.__configuration = configuration;
        this.__fleetId = event.fleetId;
        this.__eventId = event.id;
        this.__eventTime = event.createTime;
        
        this.__event = event;
        
        this.__parent = parent;

    	this.__buildDomStructure(rootPane);
    	
    	this.__displayStockTabs = configuration.displayVehicleTabs[event.fleetId];
        this.__stockTabs = new nx.unit.StockTabs($('#eventStockTabs'), false, this.__displayStockTabs);
        
        if(me.__configuration.eventDataTabEnabled) {
        	me.__buildEventDataTab();
        }
        
        if(!this.__stockChangeSuscribed){
            nx.hub.stockChanged.subscribe(function(stockId) {
                me.__showChannelData();
            });
            this.__stockChangeSuscribed = true;
        }
        
        
        nx.rest.group.all(event.fleetId, function(groups) {
            me.__drawGroups(groups);
            me.__groupDfd.resolve();
        });
        
        $.when(this.__groupsLoaded).done(function() {
        	me.__populateChannelData(event, me.__configuration);
        });
        
    };
    
    ctor.prototype.__buildDomStructure = function(rootPane) {
    	var me = this;
    	
    	rootPane.empty();
    	
    	var channelGroupsPane = $('<div>').attr('id', 'eventChannelGroups');
    	
    	var channelsPane = $('<div>').attr('id', 'eventDetailChannels');
    	channelsPane.append($('<div>').attr('id', 'eventStockTabs').addClass('fullHeightTabs'));
    	
    	rootPane.append(channelsPane);
    	rootPane.append(channelGroupsPane);
    	rootPane.append($('<div>').attr('id', 'eventDetailLinks'));
    	
    	$('#eventChannelGroups').css('top', '29px');
    	$('#eventGroup').css('top', '28px');
        $('#eventDetailChannels').css('top', '30px');
    };
    
    ctor.prototype.__buildEventDataTab = function() {
    	var me = this;
    	var event = me.__event;
    	
    	var id = 'eventDataTab';
    	var label = 'Event Data';
    	
    	var newTab = $('<li id=' + id + '"_tab"><a href="#' + id + '">' + $.i18n(label) + '</a></li>').addClass('ui-state-default ui-corner-top');
    	$('#eventStockTabs').find(".ui-tabs-nav").append(newTab);
    	$('#eventStockTabs').append("<div id=" + id + "></div>");
    	$('#eventStockTabs').tabs("refresh");
    	
    	$('#eventDetailChannels').css('top', '0px');
    	
    	var dataTab = $('#eventDataTab');
    	
    	dataTab.append($('<div>').addClass('dataTabColumnLeft').attr('id', 'eventData'));
    	
    	if(!!me.__configuration.eventDataTabEventGroups && !!me.__event.faultGroupId) {
	    	nx.rest.event.getEventsByGroup(me.__fleetId, me.__event.faultGroupId, function(response) {
	    		$('#eventDetailData').append($('<div>').attr('id', 'eventGroup'));
	    		$('#eventGroup').css('top', '28px');
	    		me.__drawEventGroup(response);
	    	});
    	}
    	
    	if (me.__configuration.eventDataTabEnabled) {
        	me.__stockTabs.showAllTabs();
        }
    	
    	for(index in me.__configuration.dataTabFields) {
    		var field = me.__configuration.dataTabFields[index];
    		var format = field.formatter;
    		
    		var fieldDiv = $('<div>').addClass('eventDataField').appendTo($('#eventData'));
			$('<div>').addClass('eventDataTab_label').html($.i18n(field.label)).appendTo(fieldDiv);
			
			var value = !!format ? $.format(format, event[field.name]) : event[field.name];
    		switch (field.type) {
	    		case 'text_area':
    				if (!field.linkable) {
                        $('<textarea>')
                        .addClass('eventDataTab_textarea')
                        .attr('id', field.name)
                        .attr('readonly', 'readonly')
                        .val(value)
                        .appendTo(fieldDiv);
    				} else {
                        var textArea = $('<span>')
                            .addClass('eventDataTab_spantextarea')
                            .attr('id', field.name);
                        textArea.append(me.__linkifyText(value));
                        textArea.appendTo(fieldDiv);
                        break;
    				}
    				break;
    			case 'input_text':
    				$('<input type="text">')
    					.addClass('eventDataTab_textinput')
    					.attr('id', field.name)
    					.attr('readonly', 'readonly')
    					.val(value)
    					.appendTo(fieldDiv);
    				break;
    			case 'label':
    				var span = $('<span>')
					.addClass('eventDataTab_spantextarea')
    				$(value).appendTo(span);
    				span.appendTo(fieldDiv);
    				break;
    		}
    	}
    	
    	// draw comments section
    	if(me.__configuration.eventDataTabCommentsEnabled) {
    		var commentHistory = $('<div>').addClass('eventDataField')
    								.attr('id', 'commentHistory')
    								.appendTo('#eventData');
    		
    		commentHistory.append($('<div>').addClass('eventDataTab_label').html($.i18n('Comment History')));
    		commentHistory.append($('<span>')
    				.addClass('eventDataTab_spantextarea')
    				.attr('id', 'commentHistory_display'));
    		
    		// populate comment data
    		nx.rest.event.comments(me.__event.fleetId, me.__event.id, function(response) {
    			me.__showComments(response);
    		});
    	}
    };
    
    ctor.prototype.__linkifyText = function(text) {
        var me = this;
        var result = '';
        if (!!text && text.length > 0) {
            result = '<p>';
            text = text.replaceAll('\n','<br>');
            $.each(text.split(' '), function(index, string) {
                if (index > 0) {
                    result += ' ';
                }
                if (me.__isUrl(string)) {
                    // If the URL ends with one or more of the following characters, these will be ignored
                    invalidEndChars = ['.', ',', ':', ')'];
                    var url = string;
                    var removedChars = '';
                    while ($.inArray(url.slice(-1), invalidEndChars) > -1) {
                        removedChars = url.slice(-1) + removedChars;
                        url = url.slice(0, -1);
                    }
                    result += '<a href="' + url + '" target="_blank">' + url + '</a>' + removedChars;
                } else {
                    result += string;
                }
            });
            result += '</p>';
        }
        
        return result;
    };
    
    ctor.prototype.__isUrl = function(text) {
        // strings matching the format "http(s)://xxx.xxx" as well as "file:///X:/.../.../.../example.txt" are considered valid URLs
        var urlRegex = /(http|ftp|https|file):(\/\/|\/\/\/)(([\w_-]+(?:(?:\.[\w_-]+)+))|([a-zA-Z]:))([\w.,@?^=%&:/~+#-]*[\w@?^=%&/~+#-])?/;
        return urlRegex.test(text);
    };
    
    ctor.prototype.__drawEventGroup = function(groupList) {    	
        var me = this;
        
        var groupListTabPane = $('#eventGroup');
        var groupListTabs = $('<ul>').appendTo(groupListTabPane);
        var selectedEventIndex = null;
        var groupLeadIndex = null;
        
        $.each(groupList, function(index, event) {
           
            if(event.fields.id == me.__eventId) {
            	selectedEventIndex = index;
            }
            
            if(event.fields.isGroupLead) {
            	groupLeadIndex = index;
            }
            
            $('<li>').append(
                    $('<a>')
                    	.attr('href', '#eventGroup_' + event.fields.id)
                        .attr('id', 'event_' + event.fields.id)
                        .text(event.fields.eventCode)
                        .click(function() {
                        	$('.dropdown-menu').hide();
                        	me.__parent.__eventIdListBookmark = me.__fleetId + "-" + me.__eventId;
                        	nx.event.open(event.fields.id, me.__fleetId, me.__parent.__eventIdList);
                        })
            ).appendTo(groupListTabs);
            
            $('<div>').attr('id', 'eventGroup_' + event.fields.id).appendTo(groupListTabPane);
        });
        
        groupListTabPane.tabs({
            'show': function(event, ui) {
            	var groupEvent = ui.tab.id.substr(6);
            }
        }).addClass('ui-tabs-vertical')
        .addClass('ui-helper-clearfix');
        
        groupListTabs
            .children('li')
            .removeClass('ui-corner-top')
            .addClass('ui-corner-right');
        
        groupListTabs
        	.children('li')
        	.eq(groupLeadIndex)
        	.addClass('groupLead');

        groupListTabPane.children('div').hide();
        
        groupListTabPane.tabs("select", selectedEventIndex)
    	
    };
    
    ctor.prototype.__checkForComment = function() {
    	var comment = $.trim($('#commentSubmit_input').val());
    	if(comment.length == 0) {
    		return false;
    	} else {
    		return true;
    	}
    };
    
    ctor.prototype.__showComments = function(comments) {
    	var me = this;
    	
    	$('#commentHistory_display').empty();
		
		for(var index in comments) {
			var comment = comments[index];
			
			var actionLbl = "";
			if (comment.action == "ack") {
				actionLbl = $.i18n("acknowledged the event");
			} else if (comment.action == "deac") {
				actionLbl = $.i18n("deactivated the event");
			} else if (comment.action == "req") {
				actionLbl = $.i18n("issued a service request");
			} else if (comment.action == "com") {
				actionLbl = $.i18n("commented");
			}
			
			var commentString = me.__formatDate(comment.timestamp)
									+ ' - ' + comment.userName 
									+ ' ' + actionLbl
									+ ' : ' + comment.text;
			
			$('#commentHistory_display').append($('<p>').text(commentString));
		}	
	};
	
	ctor.prototype.__refreshComments = function() {
		var me = this;
		
		nx.rest.event.comments(me.__event.fleetId, me.__event.id, function(response) {
			me.__showComments(response);
		});
	};
    
    ctor.prototype.__formatDate = function(timestamp) {
    	if(timestamp == null) {
    		return '';
    	}
    	
    	var date = new Date(timestamp);
    	
    	var format2Digits = function(val) {
    		if(val < 10){
    			return '0' + val;
    		}
    		return val;
    	}
    	
    	var day = date.getDate();
    	var month = date.getMonth() + 1;
    	var year = date.getFullYear();
    	var hours = date.getHours();
    	var minutes = date.getMinutes();
    	var seconds = date.getSeconds();

    	
    	var formattedDate = format2Digits(day) + '/' + format2Digits(month) + '/' + year + ' ' + format2Digits(hours) + ':' + format2Digits(minutes) + ':' + format2Digits(seconds);
    	return formattedDate;

    }
    
    ctor.prototype.__drawGroups = function(groups) {
        var me = this;
        
        if(me.__configuration.showSlider) {
        	me.__addRangeSelector(toolbar);
        }
        var groupsTabPane = $('#eventChannelGroups');
        var groupsTabs = $('<ul>').appendTo(groupsTabPane);
        
        for (var i = 0, l = groups.length; i < l; i++) {
            var group = groups[i];
            
            $('<li>').append(
                    $('<a>')
                        .attr('href', '#eventDetailTab_' + group.id)
                        .attr('id', 'group_' + group.id)
                        .text($.i18n(group.description))
            ).appendTo(groupsTabs);
            
            $('<div>').attr('id', 'eventDetailTab_' + group.id).appendTo(groupsTabPane);
        }
        
        groupsTabPane.tabs({
            'show': function(event, ui) {
                me.__group = ui.tab.id.substr(6);
                me.__showChannelData();
            }
        }).addClass('ui-tabs-vertical')
        .addClass('ui-helper-clearfix');
        
        groupsTabs
            .children('li')
            .removeClass('ui-corner-top')
            .addClass('ui-corner-right');

        groupsTabPane.children('div').hide();
    };
    
    ctor.prototype.__reset = function() {

        this.__stockTabs.clear();
        $('#eventChannelGroups').empty();
        $('#eventChannelGroups').tabs('destroy');
        
    };
    
    ctor.prototype.__populateChannelData = function(event, configuration) {
        var me = this;
        
        var isIE8OrBelow = function() {
            return /MSIE\s/.test(navigator.userAgent) && parseFloat(navigator.appVersion.split("MSIE")[1]) < 9;
        }
        
        var fun = function() {
        	var srcNumber = event.sourceNumber.replace(/\//g, '\\/ '); // escape slashs
            $('#eventStockTabs').find('#tab' + srcNumber).addClass('tab' + event.type);
            $('#eventStockTabs').find('li').each(function(index, el){
                if (el.id == 'tab' + event.sourceNumber) {
                    $('#eventStockTabs').tabs('select', index);
                }
            });
            
            var shiftUp = 1;
            var tabHeight = Math.floor($('#eventStockTabs ul').height());
            if (isIE8OrBelow() && tabHeight > 0 ) {
            	shiftUp = 2;
            } else if (tabHeight == 0) {
            	shiftUp = -29;
            	$('#eventDetailChannels').css('top', '30px');
            }

            var topOffset = tabHeight - shiftUp + 'px';
            
            // Need to set the position of the channel groups..
            $('#eventChannelGroups').css({
                'top': topOffset
            });
            $('#eventGroup').css({
                'top': topOffset
            });
            
            if (configuration.buttonRows > 0) {
            	me.__addButtonSection(event, configuration);
            } else {
            	$('#eventDetailLinks').hide();
            }
            
            if (configuration.eventDataTabEnabled) {
            	me.__stockTabs.showAllTabs();
            }

            me.__showChannelData();
            
            $('#eventStockTabs').tabs("refresh");
            $('#eventStockTabs').tabs('select', 0);
        }
        
        var showStockTabs = function(stockList) {
            $('#eventDetailChannels').css('top', '0px');
            me.__stockTabs.setStock(stockList);
            fun();
        }
        
        if (!me.__displayStockTabs) {
            me.__stockTabs.setStock([{id: event.sourceId, description: event.sourceNumber}]);
            fun();
        } else if (!!me.__configuration.staticSiblings) {
            nx.rest.stock.siblings(event.fleetId, event.sourceId, function(stockList) {
                showStockTabs(stockList);
            });
        } else {
            nx.rest.stock.stocksByEvent(event.fleetId, event.id, function (stockList) {
                showStockTabs(stockList);
            }); 
        }
    };
    				
    ctor.prototype.__showChannelData = function() {
        var me = this;
        var stockId = this.__stockTabs.getSelectedStockId();
        var groupId = this.__group;
        var fleetId = this.__fleetId;
        var eventId = this.__eventId;
        var configuration = this.__configuration;
        var timestamp = this.__eventTime;
        var period = 0; 
        
        if(me.__configuration.showSlider){
        	
        	if (this.__period ==null)
        		period = me.__configuration.periodsListEvent[me.__configuration.defaultScrollValue];
        	else 
        		period=	this.__period;
        	
        }
        
        if (stockId == null || isNaN(stockId)) {
        	if(configuration.eventDataTabEnabled) {
        		$('#eventChannelGroups').hide();
        		$('#eventGroup').show();
        	} else {
        		$('#eventChannelGroups').show();
        		$('#eventGroup').hide();
        	}
            return;
        } else {
        	$('#eventChannelGroups').show();
        	$('#eventGroup').hide();
        }
        
        nx.rest.event.eventDetailData(fleetId, eventId, groupId, stockId, period ,timestamp, function(response) {
        	// changing tab quickly can mean the callback fires on the wrong tab, check stockId
        	var currentStockId = me.__stockTabs.getSelectedStockId();
        	if (currentStockId == null || isNaN(stockId) || currentStockId != stockId) {
                return;
            }
        	
            var tab = $(me.__stockTabs.getSelectedTabEl());
            var table = $('<div>')
                .addClass('edChannelData tabularVertical')
                .append($('<table>').addClass('ui-corner-all'));
            
            tab.empty().append(table);
            
            // Build the table
            var columns = [];
            for (var i = 0, l = response.columns.length; i < l; i++) {
                var next = response.columns[i];
                
                var headerMouseOverOutput = '';
                outer:
                for (var j = 0 ; j < configuration.headerMouseOver.length ; j++) {
                    if ($.i18n(next[configuration.headerMouseOver[j].value])) {
                    	if(headerMouseOverOutput != '') {
                    		headerMouseOverOutput += '\n';
                    	}
                        headerMouseOverOutput += $.i18n(next[configuration.headerMouseOver[j].value]);
                    }
                }
                
                // * FIXME tabular should be able to read the columns without conversion.
                columns.push({
                    name: next.name,
                    text: (!! next[configuration.headerDisplayField] ?  next[configuration.headerDisplayField] : ''), 
                    mouseOverHeader: ((configuration.mouseOverHeaderEnabled && !!next.mouseOverHeader) ? headerMouseOverOutput : $.i18n(next.header)), 
                    width: next.width + 'px',
                    sortable: next.sortable,
                    styles: next.styles,
                    formatter: function(column, value) {
                        // This looks wrong - but will leave for the moment....
                        var cellText = !!value ? value[0] || '&nbsp;' : '&nbsp;';
                        var cellCat = !!value ? value[1] || null : null;
                        var className = !!cellCat ? column.styles[nx.util.CATEGORIES[cellCat]] : 'channeldata_default';
                        
                        if (!!value && !!value[0] && column.name == "TimeStamp"){
                            cellText =  value = $.format("{date:DD/MM/YYYY HH:mm:ss.SSS}", cellText);
                            className = " eventTimeStampColumn"
                        }
                        
                        return "<div class = '" + className + "' title='" + column.text + "'>" + cellText + "</div>";
                    }
                });
            }
            
            var grid = table.tabular({
                columns: columns,
                scrollbar: true,
                fixedHeader: true,
                headerTooltip: true
            });
            
            // Set the data
            
            var data = response.data;
            var gridData = [];
            
            for (var i = 0, l = data.length; i < l; i++) {
                var channels = data[i].channels;
                var row = [];
                
                for (var j = 0; j < response.columns.length; j++) {
                    var value = channels[response.columns[j].name] || [null, 0];
                    row.push(value);
                }
                
                gridData.push({id: i, data: row});
            }

            grid.setData(gridData);
        });
    };
    
    ctor.prototype.__addButtonSection = function(event, configuration) {
    	var me = this;
    	
    	var rows = configuration.buttonRows;
    	var height = configuration.buttonHeight;
    	var width = configuration.buttonWidth;
    	
    	var shiftUp = (height + 5) * rows;
    	$('#eventDetailChannels').css('bottom', shiftUp + 'px');
    	
        $.each(configuration.buttons, function(index, button) {
        	var btn = $('<button id = "' + button.id + '">' + $.i18n(button.displayName) + '</button>')
    	    .button({	
    	    }).css({
    	    	'height': height + 'px',
    	    	'width': width + '%'
    	    }).click(function(e) {
                if (!!button.openUnitSummary) {
                	nx.util.openHistoricUnitSummary(event.stockId, event.fleetId,
                			button.unitSummaryTab, null, event.createTime, event.id);	
                } else if (!!button.alternativeAction) {
                	eval(button.alternativeAction);
                }
            }).addClass("eventDetailButton");
        	
        	$('#eventDetailLinks').append(btn);
        });
    	
    };
    
    ctor.prototype.__addRangeSelector = function(toolbar) {
        if (this.__refreshPeriod != null) {
        	this.__controllerWidth += 95;
        }
        var toolbar = $('#eventDetailDataSlider').css({
        		'border': 'solid transparent'
        	}).addClass('ui-widget-header');
        

        
        
        this.__controllerDiv = $('<div>')
            .css({
                'float': 'right',
                'height': '25px',
                'width': this.__controllerWidth + 'px',
                'margin-top': '-3px'
            }).addClass('ui-state-default')
            .addClass('ui-corner-all');
        
        var div = $('<div>').css({
            'position': 'right',
            'top': '-2px',
            'left': '6px',
            'height': '25px',
            'width': '200px',
            'box-sizing': 'border-box',
            'padding-left': '10px',
            'padding-right': '10px',
            'padding-top': '4px',
            'direction': 'ltr'
        });
        
        var slider = $('<div>').css({
            'width': '100%'
        });
        
        var value = $('<div>').css({
            'position': 'absolute',
            'top': '9px',
            'rigt': '82px',
            'height': '20px',
            'line-height': '16px',
            'width': '110px',
            'direction': 'ltr',
            'text-align': 'right',
            'padding-top': '5px',
            'font-size': '10px',
            'margin-left' : '20px'
        });
       
        var me = this;
        
        if(this.__event.anyPeriod != null ) {
        	value.text(nx.util.timestampToHumanReadable(this.__event.anyPeriod * 1000));
        	me.__period = me.__event.anyPeriod * 1000;
        	localStorage.setItem("selectedPeriod" , me.__period);
        }
        else {
            value.text(nx.util.timestampToHumanReadable(me.__configuration.periodsListEvent[me.__configuration.defaultScrollValue]));
        }
        

        localStorage.setItem("selectedPeriod" , me.__configuration.periodsListEvent[me.__configuration.defaultScrollValue]);
        
        div.append(slider);
        this.__controllerDiv.append(div);
        this.__controllerDiv.append(value);
        this.__controllerDiv.appendTo(toolbar);
        
        slider.slider({
            min: 0,
            max:me.__configuration.periodsListEvent.length - 1,
            value: this.__event.anyPeriod != null  ? me.__configuration.periodsListEvent.indexOf(me.__event.anyPeriod * 1000) : me.__configuration.defaultScrollValue,
            slide: function(event, ui) {
                me.__period = me.__configuration.periodsListEvent[ui.value];
                localStorage.setItem("selectedPeriod" , me.__period);
                value.text(nx.util.timestampToHumanReadable(me.__period));
                me.__showChannelData();

            },
            stop: function(event, ui) {
                
            }
        });
    };
    
    return ctor;
}());