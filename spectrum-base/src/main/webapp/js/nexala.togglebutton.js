var nx = nx || {};
nx.unit = nx.unit || {};

nx.unit.PlayPauseButton = (function() {
    function ctor(el, on) {
        this.__state = false;
        
        el
            .addClass('toggleButton')
            .addClass('ui-widget')
            .addClass('ui-widget-content')
            .addClass('ui-corner-all');
        
        this.__pause = $('<div>')
            .addClass('option')
            .append($('<a>').text($.i18n('PAUSE')));
        
        this.__play = $('<div>')
            .addClass('option')
            .append($('<a>').text($.i18n('PLAY')));
        
        this.__slider = $('<div>')
            .addClass('slider')
            .addClass('ui-widget-header')
            .addClass('ui-corner-all');
        
        this.__el = el;
        
        el.append(this.__slider);
        el.append(this.__play);
        el.append(this.__pause);
        
        if (on) {
            this.play();
        } else {
            this.pause();
        }

        var me = this;
        
        this.__el.click(function(e) {
            if (me.__state) {
                me.pause(function() {
                    nx.hub.pause.publish();
                });
            } else {
                me.play(function() {
                    nx.hub.play.publish();
                });
            }
        });
    };
    
    ctor.prototype.pause = function(event) {
        var me = this;
        
        if (this.__state == true) {
            this.__slider.animate({
                'left': 0
            }, 200, function() {
                me.__play.removeClass('ui-state-active');
                me.__pause.addClass('ui-state-active');
                me.__state = false;
                
                if (!!event) {
                    event();
                }
            });
        }
    };
    
    ctor.prototype.play = function(event) {
        var me = this;
        
        if (this.__state == false) {
            this.__slider.animate({
                'left': me.__el.outerWidth() / 2
            }, 200, function(time) {
                me.__pause.removeClass('ui-state-active');
                me.__play.addClass('ui-state-active');
                me.__state = true;
                
                if (!!event) {
                    event();
                }
            });
        }
    };
    
    ctor.prototype.isPaused = function() {
        return !this.__state;
    };

    return ctor;
})();

(function($) {
    $.fn.togglebutton = function(on) {
        return this.each(function() {
            var element = $(this);
            
            if (element.data('obj')) {
                return;
            }
            
            var toggleButton = new nx.unit.PlayPauseButton($(this), on);
            element.data('obj', toggleButton);
        });
    };
})(jQuery);