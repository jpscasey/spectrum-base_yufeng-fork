var nx = nx || {};

nx.Main = (function() {
    function ctor() {
        var me = this;
        this.__left = $('#footer .left');
        this.__right = $('#footer .right');
        
        this.__A_KEY = 65;
        this.__S_KEY = 83;
        this.__CTRL = 17;
        
        this.__unitSearchButton = $('#unitSearchButton');
        this.__unitSearch = $('#unitSearchPanel');
        this.__unitSearchVisible = false;
        
        this.__systemPanelButton = $('#systemPanelButton');
        this.__systemPanel = null;
        this.__systemPanelVisible = false;
        
        this.__eventColumns = [];
        this.__eventPanelBottom = false;
        this.__descriptionMaxCharacters = null;
        this.__eventRowMouseOver = [];
        this.__createFaultButton = false;
        this.__eventPanelsOnBottom = false;
        this.__createFaultRecoverButton = false;
        this.__alwaysDisplayLiveCount = null;
        this.__createFaultInput = [];
        this.__eventPanels = [];
        
        this.__hasEventsPanelLicence = true;

        this.__lastEventsUpdateTime = null;
        this.__currentEventsList = {};
        
        this.__settingsButton = null;
        this.__settingsPanel = null;
        this.__settingsVisible = false;
        
        this.__filtersButton = null;
        this.__filtersPanel = null;
        this.__filtersVisible = false;
        
        this.__eventsButton = null;
        this.__eventsVisible = false;
        this.__newEventIntervalId = null;
        
        this.__currentEventFilters = {};
        this.__tempEventFilters = {};
        
        this.__refreshRate = null;
        
        this.__helpPath = null;
        
        var screenName = window.location.pathname.replace(/^\/([^\/]*).*$/, '$1');
        
        this.__createUnitButton();
        
        // FIXME - should pass in a unitId instead of a number here.
        this.__unitChangeSub = nx.hub.unitChanged.subscribe(
                function(e, unit) {
            me.openUnitSummary(unit);
        });
        
        this.__createEventsButton();
        
        var spectrumConfigDfd = $.Deferred();
        this.__spectrumConfigured = spectrumConfigDfd.promise();
        nx.rest.system.getSpectrumConfiguration(function(response) {
        	// Help config
        	if($.parseJSON(response.helpConfig[screenName])) {
        		ctor.prototype.__createHelpButton();
        	}
        	
        	// User config
        	me.__user = response.user;
        	
        	// Search config
        	me.__unitSelector = $('.unitSelector').unitSelector(response.unitSearchConfiguration, function(select){
            	nx.hub.unitChanged.publish(
                		{id: select.id, unitNumber: select.unitnumber, fleetId: select.fleetId , userAction : true});
            }, function(){
            	me.hideUnitSearch();
            });
        	
        	// Panel licence config
        	me.__hasEventsPanelLicence = response.eventsPanelLicence;
        	
        	// Refresh rate config
        	me.__refreshRate = response.globalRefreshRate;
        	
        	spectrumConfigDfd.resolve();
        	
        });
        
        $.when(me.__spectrumConfigured).done(function() {
        	
        	me.__createSystemPanelButton();
        	
            if (!!me.__hasEventsPanelLicence) {
            	
            	// Get the current screen
            	var screen = $('#links').find('.link-active').find('a').attr('href');
            	var isCurrentScreenFleetSummary = false;
            	if (screen == "fleet") {
            		isCurrentScreenFleetSummary = true;
            	}

                // Get the list of column for the event notifications panel.
                nx.rest.event.panelConfiguration(isCurrentScreenFleetSummary, function(response) {
                    me.__eventColumns = response.columns;
                    me.__eventPanelBottom = response.bottom;
                    me.__descriptionMaxCharacters = response.descriptionMaxCharacters;
                    me.__eventRowMouseOver = response.rowMouseOver;
                    me.__createFaultButton = response.createFault;
                    me.__eventPanelsOnBottom = response.eventPanelsOnBottom;
                    me.__createFaultRecoverButton = response.createFaultRecoverButton;
                    me.__alwaysDisplayLiveCount = response.alwaysDisplayLiveCount;
                    me.__createFaultInput = response.createFaultInput;
                    me.__eventPanels = response.eventPanels;
                    if (!!response.userConfiguration) {
                    	me.__eventFiltersUserConfig = JSON.parse(response.userConfiguration);
                    }
                    
                    me.__initEventPanels();
                    me.__initEventButtons();
                    me.__getCurrentEvents(me.__eventPanels[0].name);
                    
                    nx.rest.event.liveCount(me.__currentEventFilters[currentActivePanel], this.__lastEventsUpdateTime, function(response) {
                        me.__eventUpdate(response, true);
                    }, me.onComplete());
                    
                    $(window).resize(function() {
                        me.__accordion.accordion("resize");
                    });
                    
                });
            }
            else {
                $('.left button.eventButton').remove();
                $('.eventContainer').remove();
                $('#fleetContainer').attr('style', 'height: 100% !important; width: 100% !important');
            }
        });
        
        $('html').click(function() {
            if (me.__settingsVisible == true) {
                me.__hideSettings();
            }
            
            if (me.__systemPanelVisible == true) {
                me.__hideSystemPanel();
            }
            
            if (me.__filtersVisible == true) {
                me.__hideFilters();
            }
            
            if (me.__unitSearchVisible == true) {
                me.hideUnitSearch();
            }
            
        });
    };

    ctor.prototype.onComplete = function() {
    	var me = this;
    	setTimeout(function () { me.refreshFilter(); }, me.__refreshRate);
    }
    
	ctor.prototype.refreshFilter = function() {
		var me = this;
		var queryParameters = {};
		if (!me.__alwaysDisplayLiveCount) {
			queryParameters = me.__currentEventFilters[currentActivePanel];
		} else {
			queryParameters = {
				isLive : "true"
			};
		}
		nx.rest.event.liveCount(queryParameters, me.__lastEventsUpdateTime,
				function(response) {
					me.__eventUpdate(response);
				}, me.onComplete());
	}
    
    ctor.prototype.onEventAccordionChange = function (event, ui) {
    	
    	if (!!this.__eventPanelBottom) {
    		return;
    	}
    	
    	ui.newContent.css("overflow", "auto");
        $(ui.newHeader[0]).find('.currentEventsFilterIcon').addClass('ui-icon ui-icon-volume-off');
        currentActivePanel = $(ui.newHeader)[0].id.substring(0, $(ui.newHeader)[0].id.indexOf('-header'));
        $(ui.oldHeader[0]).find('.currentEventsFilterIcon').removeClass('ui-icon ui-icon-volume-off');
        
        $.each(this.__eventPanels, function(index, eventPanel) {
            $('#' + eventPanel.name + '-filter').hide();
        });
    }

    // FIXME Is this used? 
    ctor.prototype.addEventList = function () {
        var me = this;

        nx.rest.event.liveCount(this.__currentEventFilters[currentActivePanel], me.__lastEventsUpdateTime, function(response) {
            me.__eventUpdate(response, true);
        }, me.onCompleteAddEventList());
    };

    ctor.prototype.startAddEventList = function () {
    	nx.rest.event.liveCount(this.__currentEventFilters[currentActivePanel], me.__lastEventsUpdateTime, function(response) {
            me.__eventUpdate(response);
        }, this.onCompleteAddEventList());
    }
    
    ctor.prototype.onCompleteAddEventList = function() {
    	setTimeout(function () { this.startAddEventList(); }, this.__refreshRate);
    }
    
    ctor.prototype.openUnitSummary = function(unit) {
	    nx.util.openUnitSummary(!!unit.id ? Math.abs(unit.id): null, unit.fleetId, null, unit.unitNumber);
    }
    
    ctor.prototype.__createUnitButton = function() {
        var me = this;
        var ctrl = false;
        
        $(document).keyup(function(e) {
            if (e.which == me.__CTRL)
                ctrl = false;
        }).keydown(function(e) {
            if (e.which == me.__CTRL) {
                ctrl = true;
            } else if (e.which == me.__S_KEY && ctrl == true) {
                e.preventDefault();
                
                if (me.__unitSearchVisible) {
                    me.hideUnitSearch();
                } else {
                    me.showUnitSearch();
                }
            }
        });
        
        var button = $('<button>')
            .button({icons: {primary: 'ui-icon-search'}})
            .click(function(e) {
                e.stopPropagation();
                
                if (me.__unitSearchVisible) {
                    me.hideUnitSearch();
                } else {
                    me.showUnitSearch();
                }
            })
            .attr('title',$.i18n('Unit search (Ctrl+S)'));
        
        this.__unitSearchButton.append(button);
    };
    
    ctor.prototype.__createHelpButton = function() {
        var me = this;
        
        var button = $('<button>')
            .button({icons: {primary: 'ui-icon-help'}})
            .click(function(e) {
            	var screen = window.location.pathname.replace(/[^a-zA-Z0-9]/g, '');
                nx.rest.help.config(function(response) {
                	var helpPath = response["helpDocumentPath"];
                	if (document.getElementById(screen + 'Tabs')) {
                		var tabName = $('#' + screen + 'Tabs').find('.ui-state-active').find('a').attr('data-i18n').replace(/[^a-zA-Z0-9]/g, '');
                		window.open(helpPath + '#' + screen + tabName, '_blank');
                	} else {
                		window.open(helpPath + '#' + screen, '_blank');
                	}
                });	
            });
        
        $('#helpButton').append(button);
       
    };
    
    ctor.prototype.__createSystemPanelButton = function() {
        var me = this;
        
        var button = $('<button>')
            .button({icons: {primary: 'ui-icon-person'}})
            .click(function(e) {
                e.stopPropagation();
                
                if (me.__systemPanelVisible) {
                    me.__hideSystemPanel();
                } else {
                    me.__showSystemPanel();
                }
            });
        
        this.__systemPanelButton.append(button);
        
        me.__createSystemPanel(me.__user);
        
    };

    ctor.prototype.__createSystemPanel = function(user) {
        var me = this;
        
        this.__systemPanel = $('<div>')
            .addClass('systemPanel');

        this.__systemPanel.append($('<div>').addClass('peak-up'));
        
        var content = $('<div>')
            .addClass('systemPanelContent')
            .addClass('ui-corner-all');
        
        var link = $('<a>').prop('href', user.accountUrl).prop('target', '_blank').addClass('systemPanelChangePassword');
        content.append(link);
        link.append($('<div>')
                .text(user.name)
                .addClass('systemPanelName'));
        
        link.append($('<div>')
                .text(user.login)
                .addClass('systemPanelLoginName'));

        content.append($('<br>'));
        
        content.append($('<button>')
                .text($.i18n('Profile'))
                .addClass('systemPanelProfileButton')
                .addClass('ui-button ui-widget ui-state-default ui-corner-all')
                .click(function() {
                	window.open(user.accountUrl, '_blank');
                }));
        
        if (!!user.userAdmin) {
        	content.append($('<button>')
                    .text($.i18n('Manage users'))
                    .addClass('systemPanelManagementButton')
                    .addClass('ui-button ui-widget ui-state-default ui-corner-all')
                    .click(function() {
                    	window.open(user.realmManagementUrl, '_blank');
                    }));
        }

        content.append($('<hr>'));
        
        content.append($('<button>')
                .text($.i18n('Logout'))
                .addClass('systemPanelLogoutButton')
                .addClass('ui-button ui-widget ui-state-default ui-corner-all')
                .click(function() {
                		window.location.replace(user.logoutUrl);
                }));
        
        
        
        this.__systemPanel.append(content);
        
        $('body').append(this.__systemPanel);
    };

    ctor.prototype.__showSystemPanel = function() {
        this.__systemPanel.show('slide', {direction: 'up'});
        this.__systemPanelVisible = true;
    };
    
    ctor.prototype.__hideSystemPanel = function() {
        this.__systemPanel.hide('slide', {direction: 'up'});
        this.__systemPanelVisible = false;
    };
    
    ctor.prototype.showUnitSearch = function() {
        var me = this;
        
        var onComplete = function() {
            var input = me.__unitSearch.find('input');
            input.select();
            input.focus();
        };
        
        this.__unitSearch.show('slide', {direction: 'up'}, 100, onComplete);
        
        this.__unitSearchVisible = true;
    };
    
    ctor.prototype.hideUnitSearch = function() {
    	if (this.__unitSearchVisible == false) {
    		return;
    	}
        this.__unitSearch.hide('slide', {direction: 'up'}, 100);
        this.__unitSearchVisible = false;
    };

    ctor.prototype.__createEventsButton = function() {
        var me = this;
        
        this.__eventsButton = $('<button>')
            .text(0)
            .appendTo(this.__left)
            .button({icons: {primary: 'ui-icon-alert'}})
            .toggle(function() {
                me.showEvents();
                me.__eventsVisible = true;
                me.__unsetNewEventState();
            }, function() {
                me.hideEvents();
                me.__eventsVisible = false;
            }).addClass('eventButton');
        $('<span>').addClass('alert').appendTo(this.__left).hide(); // FIXME So that IE8 doesn't keep reloading the icon
    };

    ctor.prototype.__eventUpdate = function(response, initialUpdate) {
        var me = this;
        var live = response.eventCount;
        var hasNew = response.newEvents;
        var hasNewFlag = sessionStorage.getItem('newEventsFlag');
        var previousEventUpdate = this.__lastEventsUpdateTime;
        
        if (initialUpdate) {
            this.__previousCount = live;
        }
        
        if (response.lastEvent > 0) {
            this.__lastEventsUpdateTime = response.lastEvent;
        }
        
        var diff = this.__previousCount != live;

        // Update the button label (i.e. # of live faults)
        this.__eventsButton.children('.ui-button-text').text(live);
        
        // If there are new faults the button should flash
        // red until the user clicks the button.
        if (this.__eventsVisible) {
            if (!!initialUpdate || hasNew || diff || hasNewFlag == 'true') {
                sessionStorage.setItem('newEventsFlag', 'false');
                $.each(this.__eventPanels, function(index, eventPanel) {
                    me.__getCurrentEvents(eventPanel.name);
                });
            } else {
                $.each(this.__eventPanels, function(index, eventPanel) {
                    me.__updateCurrentEvents(eventPanel.name, me.__currentEventsList);
                });
            }
        } else {
            if (hasNew || hasNewFlag == 'true') {
                sessionStorage.setItem('newEventsFlag', 'true');
                this.__setNewEventState();
            }
        }
        
        if (hasNew) {
            $.event.trigger({
                type: "newEvent",
                message: previousEventUpdate
            });
        }
        
        this.__previousCount = live;
    };

    ctor.prototype.__setNewEventState = function() {
        if (!!this.__newEventIntervalId) {
            return;
        }
        
        var me = this;
        
        this.__eventsButton.find('.ui-icon-alert').addClass('alert');

        this.__newEventIntervalId = window.setInterval(function() {
            var eb = me.__eventsButton.find('.ui-icon-alert');
            
            if (eb.hasClass('alert')) {
                eb.removeClass('alert');
            } else {
                eb.addClass('alert');
            }
        }, 1000);
    };

    ctor.prototype.__unsetNewEventState = function() {
        window.clearInterval(this.__newEventIntervalId);
        this.__newEventIntervalId = null;
        sessionStorage.setItem('newEventsFlag', 'false');
        this.__eventsButton.find('.ui-icon-alert').removeClass('alert');
    };
    
    ctor.prototype.showEvents = function() {
        var me = this;
        var header = $('#header');
        var footer = $('#footer');
        var headerOffset = $(header).offset();
        var headerHeight = $(header).height();
        var footerOffset = $(footer).offset();
        var height = footerOffset.top - headerOffset.top - headerHeight;
        var top = headerOffset.top + headerHeight;
        var popup = $('.eventContainer');
        var div = $("#relativeContent");
        var divOffset = div.offset();
        var divHeight = div.height();
        var divWidth = div.width();
        
        div.css({
            'position': 'absolute',
            'left': divOffset.left,
            'width': divWidth
        }).animate({'left': divOffset.left + popup.width() + 5});
        
        popup.css({
            visibility: 'visible',
            display: 'none'
        }).show('slide', function() {
            /*
             *  Resize method doesn't work on hidden div, 
             *  recalculate the size once accordion is visible
             */ 
            me.__accordion.accordion("resize");
        });

        $.each(this.__eventPanels, function(index, eventPanel) {
            me.__getCurrentEvents(eventPanel.name);
        });
    };
    
    ctor.prototype.__initEventPanels = function() {
    	var me = this;

    	$.each(me.__eventPanels, function(index, panel) {
    		me.__createEventPanel(index, panel);
    		me.__reinitFilters(panel.name, true);
    	});
    	
    	me.__accordion = $('.eventAccordion').accordion({
            fillSpace: true,
            icons: false,
            change: function(event, ui) { // No scrolls were shown in IE
            	me.onEventAccordionChange(event, ui);
            }
        });
    	
    };

    ctor.prototype.__toggleFilterPanel = function(panelName) {
        var filterPanel = $('#' + panelName + '-filter')
        if (filterPanel.is(':visible')) {
            filterPanel.hide('slide', {direction: 'up'});
            if(this.__checkDefaultFilterValues(panelName) == false) {
            	this.__clearFilters(panelName, false);
            }
        } else {
            filterPanel.show('slide', {direction: 'up'});
        }
    };

    ctor.prototype.__createEventPanel = function(index, panelConfig) {
        var me = this;
        
        // Header elements
        var header = $('<h3>').attr('id', panelConfig.name + '-header');
    	var a = $('<a>');
    	var titleDiv = $('<div>').addClass("currentEventsTitle");
    	var labelSpan = $('<span>').html($.i18n(panelConfig.label));
    	var filterIconSpan = $('<span>')
    	    .attr('id', panelConfig.name + '-filter-icon')
    	    .addClass('currentEventsFilterIcon')
    	    .click(function(e) {
    	        // Scroll to the top of the table so that the filters panel is visible
    	        $('#' + panelConfig.name + '-table').scrollTop(0);
                me.__toggleFilterPanel(panelConfig.name);
            });
        
        titleDiv.append(labelSpan)
            .append(filterIconSpan);
        a.append(titleDiv);
        header.append(a);
        
        if (me.__eventPanelsOnBottom) {
            filterIconSpan.addClass('ui-icon ui-icon-volume-off');
            if (index == 0) {
            	currentActivePanel = panelConfig.name;
            	labelSpan.attr('id', 'liveEventText');
            } else if (index == 1) {
            	$(labelSpan).attr('id', 'recentEventText');
            }
        } else if (index == 0) {
            filterIconSpan.addClass('ui-icon ui-icon-volume-off');
            currentActivePanel = panelConfig.name;
        }
        
        // Filter expanded panel elements
    	var filterDiv = $('<div>')
    	        .attr('id', panelConfig.name + "-filter")
    	        .addClass('currentEventsFilter')
    	        
    	var filterItemsContainer = $('<div>')
    	        .addClass('currentEventPanelFilters ui-corner-all darkbg')
    	        .append($('<div>').addClass('filtersHeader').html($.i18n('Filters')));

    	$.each(panelConfig.criteriaList, function(index, criteria) {
    		if (criteria.fieldType.toLowerCase() == 'checkbox') {
    		    filterItemsContainer.append(me.__createCheckboxFilter(panelConfig.name, criteria));
    		} else if (criteria.fieldType.toLowerCase() == 'multiple_select') {
    		    filterItemsContainer.append(me.__createMultiSelectFilter(panelConfig.name, criteria));
    		} else if (criteria.fieldType.toLowerCase() == 'select') {
    		    filterItemsContainer.append(me.__createSelectFilter(panelConfig.name, criteria));
    		}
    	});

        var buttonRow = $('<div>').addClass('filter');
        
        var buttonCancel = $('<button>').button()
            .text($.i18n('Clear'))
            .click(function(e) {
            	me.__clearFilters(panelConfig.name, false);
            })
            .addClass('ui-button-secondary');
            
        var buttonApply = $('<button>').button()
            .text($.i18n('Apply'))
            .click(function(e) {
            	me.__copyFilterCriteria(panelConfig.name);
            	var filterActive = me.__checkDefaultFilterValues(panelConfig.name);
                me._applyEventCriteriaChange(panelConfig.name, filterActive, true);
            })
            .addClass('ui-button-primary');
            
        buttonRow.append(buttonApply);
        buttonRow.append(buttonCancel);
        filterItemsContainer.append(buttonRow);
    	
    	filterDiv.append(filterItemsContainer);
    	
    	// Events list table
    	var tableDiv = $('<div>').attr('id', panelConfig.name + '-table').addClass(panelConfig.name).addClass('currentEvents');
    	tableDiv.append(filterDiv)
    	tableDiv.append('<table>');
    	
    	$('.eventAccordion').append(header).append(tableDiv);
    };
    
    ctor.prototype.__copyFilterCriteria = function(panelConf) {
    	var me = this;
    	$.each(me.__eventPanels, function(index, panel) {
    		$.each(panel.criteriaList, function(indx, criteria) {
    			if(criteria.fieldType.toLowerCase() != "invisible") {
    				me.__currentEventFilters[panel.name][criteria.name] = me.__tempEventFilters[panel.name][criteria.name];
    			};
    		});
    	});
    };
    
    ctor.prototype.__checkDefaultFilterValues = function(panelName) {
    	var filterActive = false;
    	var me = this;
        var panel = null;
        
        $.each(this.__eventPanels, function(index, eventPanel) {
            if (panelName == eventPanel.name) {
                panel = eventPanel;
                return false;
            }
        });
    	
        $.each(panel.criteriaList, function(index, criteria) {
            if (criteria.fieldType.toLowerCase() == 'checkbox') {
                var isChecked = criteria.defaultValues[0] == 'true';
                
                if (me.__currentEventFilters[panelName][criteria.name] != isChecked) {
                	filterActive = true;
                }
            } else if (criteria.fieldType.toLowerCase() == 'multiple_select') {
                var hasDefaultValue = false;
                
                // Get the list of selected values from user config.
                var selectedValues = criteria.defaultValues;
                if (!!me.__currentEventFilters[panelName][criteria.name]) {
                	selectedValues = me.__currentEventFilters[panelName][criteria.name].split(',');
                	if (selectedValues != criteria.defaultValues) {
                		filterActive = true;
                	}
                }                
            } else if (criteria.fieldType.toLowerCase() == 'select') {
                var select = $('#' + panelName + '-' + criteria.name + '-select');
                
                var value = criteria.defaultValues[0];
                
                if (value != me.__currentEventFilters[panelName][criteria.name]) {
                	filterActive = true;
                }                
            }
        });
        
    	return filterActive;
    };
    
    ctor.prototype.__createCheckboxFilter = function(panelName, criteria) {
        var me = this;
        
        var row = $('<div>').addClass('filter');
        var label = $('<div>')
            .addClass('label').html($.i18n(criteria.label));
        
        var input = $('<input>')
            .attr('type', 'checkbox')
            .attr('id', panelName + '-' + criteria.name + '-checkbox')
            .addClass('filter_checkbox')
            .change(function() {
            		me.__tempEventFilters[panelName][criteria.name] = $(this).is(':checked');
             });
        
        
        
        row.append(label);
        row.append(input);
        input.uniform();
        return row;
    };
    
    ctor.prototype.__createMultiSelectFilter = function(panelName, criteria) {
        var me = this;
        var criteriaDiv = $('<div>')
                .addClass('filter')
                .attr('style', 'height: auto')
                .append($('<label>')
                        .addClass('filterMultiSelectLabel')
                        .addClass('labelSelect')
                        .text($.i18n(criteria.label)));
        var multiSelect = $('<select multiple>')
                        .attr('id', panelName + '-' + criteria.name + '-multiselect')
                        .attr('name', panelName + '-' + criteria.name)
                        .addClass('filterMultiSelect')
                        .append('<option  />');
        
        $.each(criteria.options, function(index, option) {
        	var opt = $("<option />").val(option.id).text(option.displayName);
        	multiSelect.append(opt);
        });
        
        multiSelect.change(function() {
        	me.__tempEventFilters[panelName][criteria.name] = '';
        	
            $('#' + panelName + '-' + criteria.name + '-multiselect option:selected').each(function() {
            	if (me.__tempEventFilters[panelName][criteria.name].length > 0) {
                    me.__tempEventFilters[panelName][criteria.name] += ','
                }
                me.__tempEventFilters[panelName][criteria.name] += $(this).val();
            });

        });
        
        criteriaDiv.append(multiSelect);
        
        return criteriaDiv;
    };
    
    
    ctor.prototype.__createSelectFilter = function(panelName, criteria) {
        var me = this;
        var criteriaDiv = $('<div>')
                .addClass('filter')
                .attr('style', 'height: auto')
                .append($('<label>')
                        .addClass('filterSelectLabel')
                        .addClass('labelSelect')
                        .text($.i18n(criteria.label)));
        var select = $('<select>')
                        .attr('id', panelName + '-' + criteria.name + '-select')
                        .attr('name', panelName + '-' + criteria.name)
                        .attr('style', 'background-color: white !important; padding: 0px !important')
                        .addClass('filterMultiSelect')
                        .append('<option style="background-color: white !important" />');
        
        $.each(criteria.options, function(index, option) {
        	select.append($("<option style='background-color: white !important' />")
                            .val(option.id).text(option.displayName));
        });
        
        select.change(function() {
        	me.__tempEventFilters[panelName][criteria.name] = $('#' + panelName + '-' + criteria.name + '-select').val();
        });

        select.uniform({selectAutoWidth : false});
        
        criteriaDiv.append(select);
        
        return criteriaDiv;
    };
    
    ctor.prototype.__reinitFilters = function(panelName, userOverriden) {
        
        var me = this;
        var panel = null;
        var userDefined = false;
        
        $.each(this.__eventPanels, function(index, eventPanel) {
            if (panelName == eventPanel.name) {
                panel = eventPanel;
                return false;
            }
        });
        
        this.__currentEventFilters[panelName] = {};
        this.__tempEventFilters[panelName] = {};
        
        $.each(panel.criteriaList, function(index, criteria) {
            me.__currentEventFilters[panelName][criteria.name] = "";
            
            if (criteria.fieldType.toLowerCase() == 'invisible') {
                me.__currentEventFilters[panelName][criteria.name] = criteria.defaultValues[0];
            }
            // Init chekbox filters
            if (criteria.fieldType.toLowerCase() == 'checkbox') {
                var checkbox = $('#' + panelName + '-' + criteria.name + '-checkbox');
                
                var isChecked = criteria.defaultValues[0] == 'true';
                
                // Check if the default value is overriden by the user config
                if (userOverriden && !!me.__eventFiltersUserConfig && !!me.__eventFiltersUserConfig[panelName]) {
                	if (!(typeof me.__eventFiltersUserConfig[panelName][criteria.name] === 'undefined')) {
                		var userIsChecked = me.__eventFiltersUserConfig[panelName][criteria.name];
                		if (userIsChecked != isChecked) {
                			userDefined = true;
                		}
                		isChecked = userIsChecked;
                	}
                }
                
                checkbox.prop('checked', isChecked);
                
                if (isChecked) {
                    checkbox.parent().addClass('checked');
                } else {
                    checkbox.parent().removeClass('checked');
                }
                
                me.__currentEventFilters[panelName][criteria.name] = isChecked;
            
            } else if (criteria.fieldType.toLowerCase() == 'multiple_select') {
            	// Init multiple_select filters
            	
                $('#' + panelName + '-' + criteria.name + '-multiselect option:selected').prop('selected', false);
                
                var hasDefaultValue = false;
                
                // Get the list of selected values from user config.
                var selectedValues = criteria.defaultValues;
                if (userOverriden && !!me.__eventFiltersUserConfig && !!me.__eventFiltersUserConfig[panelName]
                	&& !!me.__eventFiltersUserConfig[panelName][criteria.name]) {
                	selectedValues = me.__eventFiltersUserConfig[panelName][criteria.name].split(',');
                	if (selectedValues != criteria.defaultValues) {
                		userDefined = true;
                	}
                }
                
                $.each(selectedValues, function(index, defaultValue) {
                    var criteriaOption = $('#' + panelName + '-' + criteria.name + '-multiselect option[value="' + defaultValue + '"]');
                    
                    // if criteriaOption exists
                    if (criteriaOption.length !== 0) {
                        if (me.__currentEventFilters[panelName][criteria.name].length > 0) {
                            me.__currentEventFilters[panelName][criteria.name] += ','
                        }
                        me.__currentEventFilters[panelName][criteria.name] += defaultValue;
                        
                        criteriaOption.prop('selected', true);
                        hasDefaultValue = true;
                    }
                });
                
				if (!hasDefaultValue) {
                	$('#' + panelName + '-' + criteria.name + '-multiselect option:first-child').prop("selected", true);
                }
                
            } else if (criteria.fieldType.toLowerCase() == 'select') {
                var select = $('#' + panelName + '-' + criteria.name + '-select');
                
                var value = criteria.defaultValues[0];
                
                if (userOverriden && !!me.__eventFiltersUserConfig && !!me.__eventFiltersUserConfig[panelName]
        			&& !!me.__eventFiltersUserConfig[panelName][criteria.name]) {
                	value = me.__eventFiltersUserConfig[panelName][criteria.name];
                	if (value != criteria.defaultValues[0]) {
                		userDefined = true;
                	}
                }
                
                if (!!value) {
                	select.val(value);
                }
                
                me.__currentEventFilters[panelName][criteria.name] = criteria.defaultValues[0];
            }
        });
        
        // clone the current filters to the temp filters object, not a reference
        $.extend(this.__tempEventFilters[panelName], this.__currentEventFilters[panelName]);
        
        this._applyEventCriteriaChange(panelName, userDefined, !userOverriden);
    };
    
ctor.prototype.__clearFilters = function(panelName, userOverriden) {
        
        var me = this;
        var panel = null;
        var userDefined = false;
        
        $.each(this.__eventPanels, function(index, eventPanel) {
            if (panelName == eventPanel.name) {
                panel = eventPanel;
                return false;
            }
        });
        
        this.__tempEventFilters[panelName] = {};
        
        $.each(panel.criteriaList, function(index, criteria) {
            me.__tempEventFilters[panelName][criteria.name] = "";
            
            if (criteria.fieldType.toLowerCase() == 'invisible') {
                me.__tempEventFilters[panelName][criteria.name] = criteria.defaultValues[0];
            }
            // Init chekbox filters
            if (criteria.fieldType.toLowerCase() == 'checkbox') {
                var checkbox = $('#' + panelName + '-' + criteria.name + '-checkbox');
                
                var isChecked = criteria.defaultValues[0] == 'true';
                
                // Check if the default value is overriden by the user config
                if (userOverriden && !!me.__eventFiltersUserConfig && !!me.__eventFiltersUserConfig[panelName]) {
                	if (!(typeof me.__eventFiltersUserConfig[panelName][criteria.name] === 'undefined')) {
                		var userIsChecked = me.__eventFiltersUserConfig[panelName][criteria.name];
                		if (userIsChecked != isChecked) {
                			userDefined = true;
                		}
                		isChecked = userIsChecked;
                	}
                }
                
                checkbox.prop('checked', isChecked);
                
                if (isChecked) {
                    checkbox.parent().addClass('checked');
                } else {
                    checkbox.parent().removeClass('checked');
                }
                
                me.__tempEventFilters[panelName][criteria.name] = isChecked;
            
            } else if (criteria.fieldType.toLowerCase() == 'multiple_select') {
            	// Init multiple_select filters
            	
                $('#' + panelName + '-' + criteria.name + '-multiselect option:selected').prop('selected', false);
                
                var hasDefaultValue = false;
                
                // Get the list of selected values from user config.
                var selectedValues = criteria.defaultValues;
                if (userOverriden && !!me.__eventFiltersUserConfig && !!me.__eventFiltersUserConfig[panelName]
                	&& !!me.__eventFiltersUserConfig[panelName][criteria.name]) {
                	selectedValues = me.__eventFiltersUserConfig[panelName][criteria.name].split(',');
                	if (selectedValues != criteria.defaultValues) {
                		userDefined = true;
                	}
                }
                
                $.each(selectedValues, function(index, defaultValue) {
                    var criteriaOption = $('#' + panelName + '-' + criteria.name + '-multiselect option[value="' + defaultValue + '"]');
                    
                    // if criteriaOption exists
                    if (criteriaOption.length !== 0) {
                        if (me.__tempEventFilters[panelName][criteria.name].length > 0) {
                            me.__tempEventFilters[panelName][criteria.name] += ','
                        }
                        me.__tempEventFilters[panelName][criteria.name] += defaultValue;
                        
                        criteriaOption.prop('selected', true);
                        hasDefaultValue = true;
                    }
                });
                
                if (!hasDefaultValue) {
                	$('#' + panelName + '-' + criteria.name + '-multiselect option:first-child').prop("selected", true);
                }
                
            } else if (criteria.fieldType.toLowerCase() == 'select') {
                var select = $('#' + panelName + '-' + criteria.name + '-select');
                
                var value = criteria.defaultValues[0];
                
                if (userOverriden && !!me.__eventFiltersUserConfig && !!me.__eventFiltersUserConfig[panelName]
        			&& !!me.__eventFiltersUserConfig[panelName][criteria.name]) {
                	value = me.__eventFiltersUserConfig[panelName][criteria.name];
                	if (value != criteria.defaultValues[0]) {
                		userDefined = true;
                	}
                }
                
                if (!!value) {
                	select.val(value);
                }
                
                me.__tempEventFilters[panelName][criteria.name] = criteria.defaultValues[0];
            }
        });
    };
    
    ctor.prototype._applyEventCriteriaChange = function(panelName, showRedIcon, sendConfig) {
        var filterIcon = $('#' + panelName + '-filter-icon');
        if (!filterIcon.hasClass('ui-icon-red') && showRedIcon) {
            filterIcon.addClass('ui-icon-red');
        } else if (filterIcon.hasClass('ui-icon-red') && !showRedIcon) {
            filterIcon.removeClass('ui-icon-red');
        }
        
        if (!!sendConfig) {
            this.__toggleFilterPanel(panelName);
        	// Persist user config
            var conf = {
                    variable: "eventFilters",
            		data: this.__currentEventFilters
            };
            
            nx.rest.system.setUserConfiguration(conf, function() {
            	
            });
        }
        
        this.__getCurrentEvents(panelName);
    };
    
    ctor.prototype.__initEventButtons = function() {
    	var me = this;
    	
    	if (!!this.__createFaultButton || true) {
    		var createFaultPopup = $('<div>').dialog({
    			autoOpen : false,
                height : 250,
                width : 300,
                modal : true,
                resizable : false,
                title : 'Create New Fault',
                buttons: [
                	{
                		text: 'Create',
                		click: function() {
                			saveNewFault(false);
                		}
                	},
                	{
                		text: 'Create & Recover',
                		id: 'createFaultRecoverButton',
                		click: function() {
                			saveNewFault(true);
                		}
                	},
                	{
                		text: 'Cancel',
                		click: function() {
                			$(this).dialog('close');
                		}
                	}
                ]
    		})
    		.html('<div id="createFaultPopupContent">')
    		.on('dialogclose', function() {
    			$('#newFaultWarningBox').empty();
            });
    		
    		if(!me.__createFaultRecoverButton) {
    			$('#createFaultRecoverButton').remove();
    		}
    		    		
    		var fields = me.__createFaultInput;
    		for(var i = 0; i < fields.length; i++) {
    			var field = fields[i];
    			var input = null;
    			    			
    			var div = $('<div>').addClass('createFaultInputContainer');
    			
    			var label = $('<label>').text(field.label);
    			
    			switch(field.idType)
    			{
	    			case "text" : {
	    				input = $('<input>')
	    				.attr('type', 'text')
	    				.attr('width', field.width)
	    				.attr('name', field.name)
	    				.attr('id', field.name)
	    				.addClass('createFaultTextInput')
	    				.addClass('createFaultInput');
	    			}break;
	    			case "select" : {
	    				input = $('<select>')
	    				.attr('id', field.name)
		    	        .attr('name', field.name)
		    	        .attr('width', field.width)
		    	        .append('<option />')
	    				.addClass('createFaultSelectInput')
	    				.addClass('createFaultInput');
	    			}break;
	    			case "timepicker" : {
	    				input = $('<div>')
	    				.addClass('timePicker');
	    				var inputDiv = $('<div>')
	    				.addClass('inputDiv');
	    				input.append(inputDiv);

	    				input.timepicker()
	    				.attr('id', field.name)
		    	        .attr('name', field.name)
		    	        .attr('width', field.width)
		    	        .addClass('createFaultDateInput')
		    	        .addClass('createFaultInput');
	    			}
    			}
    			
    			div.append(label).append(input);
    			
    			$('#createFaultPopupContent').append(div);
    			
    		}
    		$('#createFaultPopupContent').append('<div id="newFaultWarningBox">');
    		
    		me._units = {};
    		var fleets = [];
			nx.rest.fleet.getFleetList(function(response) {
				fleets = response;
				
	    		for(var i = 0; i < fleets.length; i++) {
	    			var option = fleets[i];
	    			$('#newFaultFleet').append($("<option style='background-color: white !important' />")
	    			.val(option.id).text(option.name));
	    		}
	    		$('#newFaultFleet').uniform({selectAutoWidth: true});
			});
    		
    		// input field for unit autocomplete
    		$('#newFaultUnitNumber').autocomplete({
    			source: function(request, response) {
    				var vehicleNumber = $('#newFaultUnitNumber').val();
    				if(vehicleNumber.length > 2) {
    					nx.rest.event.find(vehicleNumber, function(responseList) {
    						response($.map(responseList, function(item) {
    							me._units[item.fields.unitnumber] = {
    									fleetId: item.fields.fleetId,
                                        unitId: item.fields.unitId
    							};
    							
    							return {
    								label: item.fields.unitnumber,
                                    value: item.fields.unitnumber,
                                    shortName: item.fields.unitnumber,
                                    fleetId: item.fields.fleetId,
                                    unitId: item.fields.unitId
    							}
    						}));
    					});
    				}
    			},
    			
    			focus: function(event, ui) {
    				$('#newFaultUnitNumber').val(ui.item.value);
    				$('#newFaultUnitNumber').data('selectedId', [ui.item.fleetId, ui.item.unitId] );
    			},
    			
    			select: function(event, ui) {
    				$('#newFaultUnitNumber').val(ui.item.value);
    				$('#newFaultUnitNumber').data('selectedId', [ui.item.fleetId, ui.item.unitId] );
    			}
    		});
			
			var faultDropDowns = [];
			nx.rest.event.getCreateFaultDropDowns(function(response) {
				faultDropDowns = response;

				for(var optionName in faultDropDowns) {
					for(var i in faultDropDowns[optionName]) {
						var option = faultDropDowns[optionName][i];
						$('#'+optionName).append($("<option style='background-color: white !important' />")
								.val(option.label).text(option.label));
					}
					$("#"+optionName).uniform({selectAutoWidth : true});
				}
			});
			
			// ensure the date picked is not in the future
			var validateDate = function(date) {
				if(date > new Date()){
					$('#newFaultWarningBox').html("Error: date must be in the past");
					return false;
				} else {
					return true;
				}
			};
			
			var saveNewFault = function(recover) {
				$('#newFaultWarningBox').empty();
				var saveData = {};
				var validParams = true;
				var unitNumberField;
				for(field in fields) {
					if(fields[field].name == "newFaultDate") {
						var inputValue = $('#'+fields[field].name).data('obj').getTime();
						saveData[fields[field].name] = inputValue;
					} else if(fields[field].name == "newFaultUnitNumber") {
						var inputValue = $('#'+fields[field].name).data('selectedId');
						unitNumberField = $('#'+fields[field].name);
						if(inputValue == null) {
							var unitNumber = $('#'+fields[field].name).val();
							if (!!me._units[unitNumber]) {
								saveData['fleetId'] = me._units[unitNumber].fleetId;
								saveData['unitId'] = me._units[unitNumber].unitId;
							} else {
								validParams = false;
								$('#newFaultWarningBox').html("Invalid Unit Number");
							}
							
						} else {
							saveData['fleetId'] = inputValue[0];
							saveData['unitId'] = inputValue[1];
						}
					} else {
						var inputValue = $('#'+fields[field].name).val();
						saveData[fields[field].name] = inputValue;
						if (inputValue.length < 1) {
							validParams = false;
							$('#newFaultWarningBox').html("Fault Code must not be empty");
						}
					}
				}
				
				if(validateDate(saveData['newFaultDate']) && validParams) {
					nx.rest.event.createNewFault(
							saveData.fleetId,
							saveData.unitId,
							saveData.newFaultCode,
							saveData.newFaultDate,
							function(response) {
								if(response > 0) {
									unitNumberField.data('selectedId', null);
									createFaultPopup.dialog('close');
									if(recover) {
										// open the event in the recovery tab
										nx.rest.event.eventDetail(saveData.fleetId, response, function(event) {
											newEventData = event.fields;
											
											nx.util.openHistoricUnitSummary(
													newEventData.stockId, 
													saveData.fleetId, 
													'recoveryTab', 
													null, 
													saveData.newFaultDate, 
													response);
										});
									} 
								} else {
									$('#newFaultWarningBox').html("Error inserting fault");
								}
							});
				};

			};
    		    		
    		var button = $('<button id="createFaultButton">Create New</button>')
    		.button({	
    	    }).css({
    	    }).click(function() {
    	    	createFaultPopup.dialog('open')
            }).addClass("");
    		    		
    		if (me.__eventPanelsOnBottom) {
    			$("#liveEvents-header .currentEventsTitle").append(button);
    		} else {
        		$("#liveEventFilter").append(button);
    		}
    		
    	}
    };

    ctor.prototype.__getCurrentEvents = function(panelName) {
        var me = this;
        
        if (typeof this.__currentEventFilters === 'undefined') {
        	me.__reinitFilters(panelName, true);
        }
        // retrieve events for all fleets
        nx.rest.event.allCurrent(this.__currentEventFilters[panelName], function(events) {
        	me.__currentEventsList[panelName] = [];
        	$.each(events, function(index, event) {
        		me.__currentEventsList[panelName].push(event.fields);
        	});
            me.__updateCurrentEvents(panelName, me.__currentEventsList);
        });
    };
    
    ctor.prototype.__updateCurrentEvents = function(panelName, events) {
        var me = this;

        // Clear the live faults
        $('.' + panelName + ' table').empty();
        
        if (!!events && !!events[panelName] && events[panelName].length > 0) {
            if (me.__lastEventsUpdateTime < events[panelName][0].createTime) {
                me.__lastEventsUpdateTime = events[panelName][0].createTime;
            }
            
            for (var i = 0, l = events[panelName].length; i < l; i++) {
                var event = events[panelName][i];
                $('.' + panelName + ' table').append(me.__createRow(event));
            }
        }
    };
    
    /**
     * @deprecated Use nx.util.openUnitSummary instead
     * Remove if no more occurence of that function in dependent  projects depending on Spectrum
     */
    ctor.prototype.__openUnitSummaryById = function(id, fleetId) {
        nx.util.openUnitSummary(id,fleetId);
    };

    /**
     * @deprecated Use nx.util.openUnitSummary instead
     * Remove if no more occurence of that function in dependent  projects depending on Spectrum
     */
	// FIXME - this method should not exist. Unit Summary should only be opened by stock ID.
    ctor.prototype.__openUnitSummaryByUnitNumber = function(unitNumber, fleetId) {
        nx.util.openUnitSummary(null, fleetId, null, unitNumber);
    };
    
    ctor.prototype.setUnit = function(args) {
    	var me = this; 
    	
    	// Unit select only available after we got the configuration 
    	$.when(me.__spectrumConfigured).done(function() {
    		me.__unitSelector.setUnit(args);
    	});
        
    };

    ctor.prototype.__createRow = function(event) {
    	var me = this;
        var row = $('<tr>');
        
        for (var i = 0, l = this.__eventColumns.length; i < l; i++) {
            var columnDef = this.__eventColumns[i];
            
            if (columnDef.visible) {
                
                var value = event[columnDef.name];
                var type = event.type;
                
                var td = $('<td>')
                    .addClass('event')
                    .css('width', columnDef.width);
                
                if (value != null) {
                        
                    // If we were using Tabular here, this would be automatic.
                    if (!!columnDef.format && columnDef.format.search('eventIconTwoColor') === -1) {
                    	td.append($.format(columnDef.format, value));
                    } else if (!!columnDef.format && columnDef.format.search('eventIconTwoColor') > -1) {
                    	td.append($.format(columnDef.format, value.split(",")[0], value.split(",")[1]));
                    } else if (!!columnDef.constant) {
                    	td.append(columnDef.constant);
                    } else {
                    	td.append(value);
                    }
                    
                    if (columnDef.name == 'description' && this.__descriptionMaxCharacters != null && this.__descriptionMaxCharacters > 0){
                    	td[0].style.whiteSpace = 'normal';
                    	var trimmedString = td[0].textContent.length > this.__descriptionMaxCharacters ?
                    			td[0].textContent.substring(0, this.__descriptionMaxCharacters - 3) + "..." : td[0].textContent.substring(0, this.__descriptionMaxCharacters);
                    	td[0].textContent = trimmedString;

                    }
                    
                    if (columnDef.handler == 'openUnitSummary') {
                        td.click(function() {
                            nx.util.openUnitSummary(null, event.fleetId, null, event.unitNumber);
                        });
                    } else {
                        td.click(function() {
                            var panelName = $(this).closest('.currentEvents')[0].id.substr(0, ($(this).closest('.currentEvents')[0].id).indexOf('-table'));
                            var eventIds = [];
                            
                            $.each(me.__currentEventsList[panelName], function(index, e) {
                                eventIds.push(e.fleetId + "-" + e.id);
                            });

                            nx.event.open(event.id, event.fleetId, eventIds);
                        });
                    }
                    
                    var cellTitle = '';
                	$.each(columnDef.mouseOverContent, function(index, line) {
                		cellTitle = me.__getEventTitle(index, cellTitle, line, event);
                	});
                	
                	if (cellTitle != '') {
                		td.attr('title', cellTitle);
                	}
                    
                } else {
                	td.append("");
                }
                
                row.append(td);
            }
        };
        
        var rowTitle = '';
    	$.each(this.__eventRowMouseOver, function(index, line) {
    		rowTitle = me.__getEventTitle(index, rowTitle, line, event);
    	});
        
    	if (rowTitle != '') {
    		row.attr('title', rowTitle);
    	}
    	
        return row;
    };

    ctor.prototype.__getEventTitle = function(index, title, line, event) {
        
		if (index > 0) {
			title += '\n';
		}
    	
    	if (!!line.label) {
    		title += $.i18n(line.label) + ': ';
    	}
    	
    	if (!!line.value) {
    		var value = event[line.value];
    		if (!!line.format) {
    			value = $.format(line.format, value);
    		}
    		title += (value || '');
    	} else if (!!line.constant) {
    		title += line.constant;
    	}

        return title;
    }
    
    ctor.prototype.hideEvents = function() {
        var div = $("#relativeContent");
        
        // hide the faults panel..
        $(".eventContainer").hide('slide');
        
        // move the panel back..
        div.animate({'left': 0}, function() {
            div.css({
                'position': 'absolute',
                'left': 0,
                'width': '100%'
            });
        });
    };

    ctor.prototype.clearSettings = function() {
        var settings = $('div.settingsPanel');
        settings.hide('slide', {direction: 'down'}, function() {
            $(this).remove();
        });
    };
    
    ctor.prototype.__showSettings = function() {
        var offsetLeft = this.__settingsButton.offset().left;
        
        if (offsetLeft > 20) {
            this.__settingsPanel.css('left', offsetLeft - 20);
        } else {
            var peak = this.__settingsPanel.find('.peak');
            this.__settingsPanel.css('left', offsetLeft);
            peak.css('margin-left', 5);
        }
        
        this.__settingsPanel.show('slide', {direction: 'down'});
        this.__settingsVisible = true;
    };
    
    ctor.prototype.__hideSettings = function() {
        if(this.__settingsVisible) {
            this.__settingsPanel.hide('slide', {direction: 'down'});
            this.__settingsVisible = false;
        }
    };
    
    ctor.prototype.removeSettingButton = function(settingsList) {
        if(!!nx.main.__settingsButton) {
            this.__settingsButton.remove();
            this.__hideSettings();
            this.__settingsButton = null;
        }
    };
    
    ctor.prototype.addSettings = function(settingsList, conf, createSettingsPanel) {
    	
    	if((conf.clearApplyButtons && createSettingsPanel) || !conf.clearApplyButtons) {
    		this.__settingsPanel = $('<div>')
            .addClass('settingsPanel');
    	
	        var hiddenDiv =  $('<div>')
	        .addClass('modalPage')
	        .attr('id','hiddenDiv');
	    
	        $('body').append(hiddenDiv);
	        
	        var settings = $('<div>')
	            .addClass('settings')
	            .addClass('ui-corner-all')
	            .addClass('darkbg');
	        
	        var peak = $('<div>')
	            .addClass('peak');
	        
	        var me = this;

	        settings.append($('<div>').text($.i18n('Settings')).addClass('settingsHeader'));
	        
	        for (var i = 0, l = settingsList.length; i < l; i++) {
	            var setting = settingsList[i];
	            var label = setting.label;
	            var action = setting.action;
	            var value = setting.value;
	            var id = setting.id;
	            var type = setting.type;
	            var visibleOnScreen = setting.visibleOnScreen == false ? setting.visibleOnScreen : true;
	
	            if(visibleOnScreen) {
		            var row = $('<div class = "setting">');
		            var label = $('<div>').addClass('label').text(label);
		            var input = null;
		            if (type.toLowerCase() == "checkbox") {
		            	input = $('<input type = "' + type + '">')
		                .attr('checked', value)
		                .attr('id',id)
		            	.addClass('setting_' + type);
		            
		            	if(!conf.clearApplyButtons) {
			            		input.click((function(input, action) {
				                return function() {
				                    input.is(":checked") ? action(true) : action(false);
				                };
				            }(input, action)));
		            	}
			            
		            } else if (type.toLowerCase() == "textbox"){
		            	input = $('<input type = "' + type + '">')
		                .attr('id',id)
		                .addClass('setting_' + type);
		            }	
		            
		            row.append(label);
		            row.append(input);
		            input.uniform();
		            settings.append(row);
	            }
	        }
	        
	        if(conf.clearApplyButtons) {
	        	var buttonRow = $('<div class = "setting">');
	        	
	        	var buttonCancel = $('<button>').button()
	            .text($.i18n('Clear'))
	            .attr('id','cancelSettingButton');
	            
	            var buttonApply = $('<button>').button()
	            .text($.i18n('Apply'))
	            .attr('id','applySettingButton');
	            
	            buttonRow.append(buttonApply);
	            buttonRow.append(buttonCancel);
	            settings.append(buttonRow);
	        } 
	        
	        this.createSettingsButton();

	        this.__settingsPanel.append(settings);
	        this.__settingsPanel.append(peak);
	        this.__left.append(this.__settingsButton);
	        $('body').append(this.__settingsPanel);
	        
	        this.__settingsPanel.click(function(event) {
	            event.stopPropagation();
	        });
	        
    	} else {
    		this.createSettingsButton();
            this.updateFilters(settingsList);
    		this.__left.append(this.__settingsButton);
    		$('body').append(this.__settingsPanel);
    	}
    };
    
    ctor.prototype.updateFilters = function(settingsList) {
        for (var i in settingsList) {
            var filter = settingsList[i];
            if (filter.type == 'textBox'){
                $('#' + filter.id).val(filter.value ? filter.value : "");
            }else{// checkbox
                $('#' + filter.id).attr("checked", filter.value);
            }
        }
        
        $.uniform.update();
    };

    
    ctor.prototype.createSettingsButton = function() {
        var me = this;
        
        this.__settingsButton = $('<button>')
	        .attr('id', 'settingsButton')
	    	.button({
	        icons : {
	            primary: 'ui-icon-volume-off',
	            text: false
	        }
	    });
        
        this.__left.append(this.__settingsButton);
        
        this.__settingsButton.click(function(event) {
            event.stopPropagation();
            
            if (me.__settingsVisible) {
                me.__hideSettings();
            } else {
                me.__showSettings();
            }
        });
    };
    
    ctor.prototype.addShowEventHistoryButton = function() {
    	var me = this;
    	
    	var eventHistoryButton = $('<button>').button()
        .text($.i18n("Show Event History"))
        .addClass('udInfoButton')
        .attr('id','eventHistoryButton')
        .click(function() {
        	localStorage.setItem("showEventHistoryPressed", true);
            localStorage.setItem("unitId", $('.selectedFormationUnit').data('unitId'));
            localStorage.setItem("unitNumber", $(".selectedFormationUnit").html());
        	window.location.replace(window.location.origin + "/event")
        }); 

        return eventHistoryButton;
    };
    
    ctor.prototype.checkExtraActions = function() {
    	var unitNumber = localStorage.getItem("unitNumber");
    	
    	if($.parseJSON(localStorage.getItem("showEventHistoryPressed"))) {
            var fleetId = localStorage.getItem("fleetId");
    		var unitId = localStorage.getItem("unitId");
    		nx.rest.unit.lastMaintenance(fleetId, unitId, function(response) {
    			var dateFrom;
    			if (!!response) {
    				dateFrom = $.format('{date : YYYY/MM/DD HH:mm:ss}', response);
    			} else {
    				var currentEpoch = new Date().getTime() / 1000 | 0;
    	    		var dateEpoch = currentEpoch - 172800 * 60; //120 days in seconds
    	    		var date = new Date(dateEpoch*1000);
    	    		dateFrom = $.format('{date : YYYY/MM/DD HH:mm:ss}', date);
    			}
                $('#timePickerFrom').timepicker().data('obj').setTime(new Date(dateFrom));
                $('#showAdvanced').click()
                $('#unitNumber').val(unitNumber);
                $('#searchEvents').click();
                localStorage.removeItem("showEventHistoryPressed");
                localStorage.removeItem("unitId");
                localStorage.removeItem("unitNumber");
                localStorage.removeItem("fleetId");
    		});
    	}
    	
    	if($.parseJSON(localStorage.getItem("eventDetailShowEventHistory"))) {
    		var currentEpoch = new Date().getTime() / 1000 | 0;
    		var dateEpoch = currentEpoch - 1209600; //14 days in seconds
    		var date = new Date(dateEpoch*1000);
            var dateFormatted = $.format('{date : YYYY/MM/DD}', date);
    		$('#timePickerFrom').timepicker().data('obj').setTime(new Date(dateFormatted));
    		$('#showAdvanced').click()
    		$('#unitNumber').val(unitNumber);
    		$('#searchEvents').click();
    		localStorage.removeItem("eventDetailShowEventHistory");
    		localStorage.removeItem("unitNumber");
    	}
    };
    
    
    /**
     * Add a div to the footer.
     */
    ctor.prototype.addDivToFooter = function(div) {
        div.css('float', 'left');
        
        this.__left.append(div);
    };
    
    
    ctor.prototype.clearFilters = function() {
        var settings = $('div.filtersPanel');
        settings.hide('slide', {direction: 'down'}, function() {
            $(this).remove();
        });
    };
    
    ctor.prototype.__showFilters = function() {
        var offsetLeft = this.__filtersButton.offset().left;
        
        if (offsetLeft > 20) {
            this.__filtersPanel.css('left', offsetLeft - 20);
        } else {
            var peak = this.__filtersPanel.find('.peak');
            this.__filtersPanel.css('left', offsetLeft);
            peak.css('margin-left', 5);
        }
        
        this.__filtersPanel.show('slide', {direction: 'down'});
        this.__filtersVisible = true;
        this.__filtersPanel.find('input')[0].focus();
        
        // Hide header and content
        $('#hiddenDiv').show();
        
    };

    ctor.prototype.hideFilters = function() {
        this.__hideFilters();
        
    };

    ctor.prototype.__hideFilters = function() {
        if(this.__filtersVisible) {
            this.__filtersPanel.hide('slide', {direction: 'down'});
            this.__filtersVisible = false;
        }
        
        // make header and content visible
        $('#hiddenDiv').hide();

    };
    
    ctor.prototype.removeFiltersButton = function(settingsList) {
        if(!!nx.main.__filtersButton) {
            this.__filtersButton.remove();
            this.__hideFilters();
            this.__filtersButton = null;
        }
    };
    
    
    ctor.prototype.addFilters = function(filtersList, filtersValues) {
        //this.clearSettings();

        this.__filtersPanel = $('<div>')
        .addClass('filtersPanel');
        
        
        var hiddenDiv =  $('<div>')
            .addClass('modalPage')
            .attr('id','hiddenDiv');
        
        $('body').append(hiddenDiv);
        
        var filters = $('<div>')
            .addClass('filters')
            .addClass('ui-corner-all')
            .addClass('darkbg');
        
        var peak = $('<div>')
        .addClass('peak');
        
        var me = this;
        
        filters.append($('<div>').text($.i18n('Filters')).addClass('filtersHeader'));
        
        for (var i = 0, l = filtersList.length; i < l; i++) {
            var filter = filtersList[i];
            var label = $.i18n(filter.label);
            var action = filter.action;
            var value = filter.value;
            var id = filter.id;
            var type = filter.type;
            
            var row = $('<div class = "filter">');
            var label = $('<div>').addClass('label').text(label);
            var input = $('<input type = "' + type + '">')
                .attr('id',id)
                .addClass('filter_' + type);
            
            if (!!filtersValues && !!filtersValues[id]) {
            	var value = filtersValues[id];
            	
            	if (type == "checkBox") {
            		if (value == "checked") {
            			input.attr('checked', 'checked');
            		}
                } else if ( type == "textBox") {
                	input.val(value);
                }
            }
            
            row.append(label);
            row.append(input);
            input.uniform();
            filters.append(row);
        }
        
        var buttonRow = $('<div class = "filter">');
        
        var buttonCancel = $('<button>').button()
            .text($.i18n('Clear'))
            .attr('id','cancelFilterButton');
            
        var buttonApply = $('<button>').button()
            .text($.i18n('Apply'))
            .attr('id','applyFilterButton');
            
        buttonRow.append(buttonApply);
        buttonRow.append(buttonCancel);
        filters.append(buttonRow);
        
        this.__filtersButton = $('<button>')
            .attr('id', 'filtersButton')
            .attr('title','Filters (Ctrl+A)')
            .button({
                icons : {
                    primary: 'ui-icon-volume-off',
                    text: false
                }
            });
        
        this.__filtersPanel.append(filters);
        this.__filtersPanel.append(peak);
        this.__left.prepend(this.__filtersButton);
        $('body').append(this.__filtersPanel);
        
        this.__filtersPanel.click(function(event) {
            event.stopPropagation();
        });
        
        this.__filtersButton.click(function(event) {
            event.stopPropagation();
            
            if (me.__filtersVisible) {
                me.__hideFilters();
            } else {
                me.__showFilters();
            }
        });
        var ctrl = false;
        $(document).keyup(function(e) {
            if (e.which == me.__CTRL)
                ctrl = false;
        }).keydown(function(e) {
            if (e.which == me.__CTRL) {
                ctrl = true;
            } else if (e.which == me.__A_KEY && ctrl == true) {
                e.preventDefault();
                
                if (me.__filtersVisible) {
                    me.__hideFilters();
                } else {
                    me.__showFilters();
                }
            }
        });
    };
    
    return ctor;
}());
