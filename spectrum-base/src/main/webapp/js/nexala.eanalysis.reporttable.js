var nx = nx || {};
nx.ea = nx.ea || {};

nx.ea.ReportTable = (function() {
    function ctor(eventAnalysis, panel) {
        this.__eventAnalysis = eventAnalysis;
        this.__panel = panel;
        this.__table = null;
        this.__grouping = null;
        this.__data = null;
        
        this.__initialize();
    };

    ctor.prototype.__initialize = function() {
    };

    ctor.prototype.update = function(grouping, data) {
        this.__grouping = grouping;
        this.__data = data;

        this.__resetTable();
        this.__createTable();
    };

    ctor.prototype.__resetTable = function() {
    	var me = this;
        this.__table = null;

    	var eaExportButton = $('<button title = "'+$.i18n("Export Events to CSV")+'">'+$.i18n("Export")+'</button>').button({
            icons : {
                secondary: 'ui-icon-arrowthickstop-1-s'
            }
        }).click(function(e) {
            me.__download();
        });
        
        this.__panel
            .empty()
            .append($("<div>")
                    .attr("id", "eaExportButton")
                    .append(eaExportButton)
                    )
            .append(
                $("<div>")
                    .attr("id", "eaReportTable")
                    .append(
                        $("<table>").addClass("ui-corner-all")
                    )
            );
    };

    ctor.prototype.__createTable = function() {
        var me = this;

        this.__table = $('#eaReportTable').tabular({
            'columns': [this.__getTableColumns()],
            'cellClickHandler': (function(me) {
                return function(cell, data, columnDef) {
                    me.__clickHandler(cell, data, columnDef);
                }
            })(me),
            //'cellMouseOverHandler': (function(me) {
            //    return function(cell, data, columnDef) {
            //        me.__mouseoverHandler(cell, data, columnDef);
            //    }
            //})(me),
            'scrollbar': true,
            'fixedHeader': true
        });

        this.__updateTable();
    };

    ctor.prototype.__getTableColumns = function() {
        var columns = [];

        for (var index in this.__grouping.columns) {
            var column = this.__grouping.columns[index];
            var formatter = (column.type == 'timestamp' ? this.__timeFormatter : this.__formatter);
            columns.push({
                'name': column.name,
                'text': column.header,
                'width': column.width + 'px',
                'type': column.type,
                'visible': column.visible,
                'sortable': column.sortable,
                'format': column.format,
                'handler': column.handler,
                'formatter': formatter
            });
        }

        return columns;
    };

    ctor.prototype.__clickHandler = function(cell, data, columnDef) {
        var keyPosition = this.__grouping.getColumnPosition('referenceKey');
        var referencekey = data.data[keyPosition];

        this.__eventAnalysis.openReportDetail(referencekey);
    };

    ctor.prototype.__updateTable = function() {
        if (!this.__data) {
            return;
        }

        var tabularData = [];

        for (var index in this.__data.records) {
            var reportBean = this.__data.records[index];
            
            var dataRow = [];
            for (var columnIndex in this.__grouping.columns) {
                var column = this.__grouping.columns[columnIndex];
                dataRow.push(reportBean[column.name]);
            }

            tabularData.push({'id': index, 'data': dataRow});
        }

        this.__table.setData(tabularData, true);

        var sorter = this.__grouping.defaultSorter;
        this.__table.sortByColumnName(sorter.columnName);
        if (sorter.desc == true) {
            this.__table.sortByColumnName(sorter.columnName);
        }
    };

    ctor.prototype.__formatter = function(column, value) {
        return !!value ? '<div align="center">' + value + '</div>' : "&nbsp;";
    };
    
    ctor.prototype.__timeFormatter = function(column, value) {
        return !!value ? '<div align="center">' + $.format("{date:DD/MM/YYYY}", parseFloat(value)) + '</div>' : "&nbsp;";
    };
    
    ctor.prototype.__download = function() {
        var filters = this.__eventAnalysis.__filterDialog.getFilters();
        filters.grouping = this.__eventAnalysis.__selectedGrouping;    
    	url = nx.rest.eventanalysis.download();
    	nx.util.downloadPostWithJson(url, filters);
    }

    return ctor;
})();
