(function( $ ) {
    var UnitSelector = function(element, configuration, action, hideSearch) {
        var DOWN = 40;
        var ENTER = 13;
        var ESC = 27;
        var SHIFT = 16;
        var TAB = 9;
        var UP = 38;
        
        var me = this;
        
        //configuration for the search
        this.configuration = configuration;
        
        // the action to execute when a row is selected
        this.action = action;
        
        this.hideSearch = hideSearch;

        /** currently animating the shake effect on input text */
        //var shaking = false;
        
        /** current animating the results dropdown */
        var animating = false;
        
        /** results drop down open */
        var open = false;
        
        /** The unit selector element */ 
        var el = $(element);
        
        /** The height of the selector, including borders */
        var elHeight = el.outerHeight();
        
        /** The width of the selector, including borders */
        //var elWidth = el.outerWidth();
        
        /** The input box where the user types */
        this.inputBox = el.find('input.textBox');
        this.inputBox.css({
        	'width' : configuration.panelWidth
        });
        
        /** The results pane */
        var results = $('<div>')
            .addClass('usResults')
            .css({
                'display': 'none'
            });
        
        var resultsList = [];
        
        /**
         * A map of keyCodes which are currently depressed, shift
         * is the only key, which will be added here
         */
        var keysDown = {};
        
        /**
         * The animation queue.
         * @see doAnimation
         * @see scheduleShow
         * @see scheduleHide
         */
        var animQueue = [];
        
        /**
         * The search queue
         */
        var searchQueue = [];
        
        el.parent().append(results);
        
        /** Clears the input box onClick */
        this.inputBox.click(function(evt) {
            me.inputBox.val('');
            evt.stopPropagation();
        });

        /** Capture shift, key-up, key-down and tab */
        this.inputBox.keydown(function(evt) {
            if (evt.which == 16) {
                keysDown[evt.which] = true;
            } else if ((keysDown[SHIFT] && evt.which == TAB) || evt.which == UP) {
                evt.preventDefault();
    
                var focus = $(results).find('table tbody tr.ui-state-active');
    
                if (focus.length > 0) {
                    var prev = focus.prev();
                    focus.removeClass('ui-state-active');
                    
                    if (prev.length > 0) {
                        prev.addClass('ui-state-active');
                    }
                }
            } else if (!keysDown[SHIFT] && evt.which == TAB || evt.which == DOWN) {
                evt.preventDefault();

                var focus = $(results).find('table tbody tr.ui-state-active');

                if (focus.length == 0) {
                    $(results).find('table tbody tr:first')
                        .addClass('ui-state-active');
                } else {
                    var next = focus.next();

                    if (next.length > 0 && next.hasClass('result')) {
                        focus.removeClass('ui-state-active');
                        next.addClass('ui-state-active');
                    }
                }
            } else if (evt.which == ENTER) {
                
                if (searchQueue.length > 0) {
                    $.subscribe('display-results', function() {
                        scheduledDeferred(apply);
                    });
                } else {
                    scheduleDeferred(apply);
                }
            } else if (evt.which == ESC) { // ESC
                // This prevents a keypress from showing the results box again
                evt.preventDefault();
                // hide..
                scheduleHide(me.hideSearch);
            }
        });
    
        /** Capture changes to the input box. */
        this.inputBox.on('cut paste copy keydown', function(evt) {
            var kc = evt.which;
    
            // Ignore arrows + shift & tab
            if (kc != UP && kc != DOWN && kc != SHIFT && kc != ENTER && kc != TAB && kc != ESC) {
                scheduleDeferred(function() {
                    var searchStr = me.inputBox.val();
                    
                    if (!!searchStr) {
                        scheduleSearch(searchStr, me.configuration, me);
                    }
                });
            }
        });
        
        /** Removes a key from the keysDown map */
        this.inputBox.keyup(function(evt) {
            if (evt.which == 16) {
                delete keysDown[evt.which];
            }
        });
        
        /**
         * Shakes the inputBox to indicate that the value entered
         * in the input box does not match any headcodes, sets,
         * units or vehicles
         */
        /*var visualBell = function() {
            if (shaking == false) {
                shaking = true;
                me.inputBox.effect('shake', {'times': 2, 'distance': 3, 'direction': 'up'}, 50, function() {
                    shaking = false;
                });
            }
        };*/
        
        /**
         * Calls a function with setTimeout, i.e. outside the event loop,
         * this allows, for example, the correct value of an input box
         * to be retrieved
         */
        var scheduleDeferred = function(fun) {
            setTimeout(fun, 0);
        };
        
        /**
         * Selects the currently "focused" result, or the item at
         * the top of the list if not result is "focused". The
         * inputBox will also be blurred and the results hidden
         */
        var apply = function(record) {
            
            var item = $(results).find('table tbody tr.ui-state-active');
            
            if (item == null && resultsList.length < 1) {
                return;
            } else {
                if (typeof item.data('obj') === "undefined") {
                    var select = record || resultsList[0].fields;
                } else {
                    var select = record || item.data('obj').fields || resultsList[0].fields;
                }
                me.inputBox.val(select.unitnumber);
                me.action(select);
            }
            
            me.inputBox.blur();
            setTimeout(function(){
            	scheduleHide(me.hideSearch);
            }, 100);
        };
        
        /** case-insensitive indexOf function */
        var indexOf = function(str, search) {
            if (str == null || search == null) {
                return -1;
            } else {
                return str.toLowerCase().indexOf(search.toLowerCase());
            }
        };
        
        /**
         * Wraps a section of the string "str" from the "start" index
         * to the "end" index in a span with a class of "hl". This allows
         * the text to be highlighted in the search results
         */
        var highlight = function(str, start, end) {
            if (start == end) {
                return str;
            } else {
                return [
                    str.slice(0, start),
                    "<span class = 'hl'>",
                    str.slice(start, end),
                    "</span>",
                    str.slice(end, str.length)
                ].join("");
            }
        };

        var scheduleSearch = function(searchStr, searchConfig, me) {
            searchQueue.push(searchStr);
            search(searchConfig, me);
        };
        
        /**
         * Searches for a list of stock matching the specified
         * input value and displays the in the results pane
         */
        var search = function(searchConfig, me) {
            var searchStr;
            while (searchQueue.length > 0) {
                searchStr = searchQueue.shift();
            }
            
            // FIXME - need to add some kind of search queue
            nx.rest.unit.find(searchStr, function(data) {
                var headings = [];
                var names = [];
                
                var maxResults = searchConfig.maximumResults -1; 
                var truncate = data.length > maxResults ? true : false;
                if(truncate) {
                    data = data.slice(0, maxResults);
                } 
                
                $.each(searchConfig.columns, function (index, column) { //note, only visible fields should be here
                    headings.push($.i18n(column.header));
                    names.push(column.name);
                });
                
                var records = [];
                $.each(data, function (index, entry) {
                    var record = [];
                    $.each(names , function (index, name) {
                        var desc = entry.fields[name];
                        if ((idx = indexOf(desc, searchStr)) > -1) {
                            desc = highlight(desc, idx, idx + searchStr.length);
                        }
                        record.push(desc);
                    });
                    records.push(record);
                });
                
                resultsList = data || []; 
                
                var dataNew = {
                        headers: headings,
                        resultCount: data.length,
                        truncated: truncate,
                        unitDescs: data,
                        records: records
                }; 
 
                scheduleShow(dataNew, me);
                
                if (searchQueue.length > 0) {
                    search();
                }
            });
        };
        
        /**
         * Adds an action containing an animation to the animation
         * queue. These animations are processed by the doAnimation
         * method. Queuing the animations like this makes the
         * type-ahead selector much more fluid and prevents issues,
         * such as delayed animations when the user is typing faster
         * than the results pane changes are being displayed.
         *
         * If the animator process is not currently running, it is
         * started (by calling doAnimation)
         *
         * @see scheduleHide
         * @see doAnimation
         */
        var scheduleShow = function(data, me) {
            animQueue.push(function(param, me) {
                return function() {
                    displayResults(param, me.hideSearch);
                };
            }(data, me));
    
            if (!animating) {
                doAnimation();
            }
        };
        
        /**
         * Adds an action containing an animation to the animation
         * queue. These animations are processed by the doAnimation
         * method. Queuing the animations like this makes the
         * type-ahead selector much more fluid and prevents issues,
         * such as delayed animations when the user is typing faster
         * than the results pane changes are being displayed.
         *
         * If the animator process is not currently running, it is
         * started (by calling doAnimation)
         *
         * @see scheduleShow
         * @see doAnimation
         */
        var scheduleHide = function(hideSearch) {
            animQueue.push(function() {
            	hideResults(hideSearch);
            });
            
    
            if (!animating) {
                doAnimation();
            }
        };
        
        /**
         * Gets the last animation from the animation queue and
         * executes it. All other animations in the queue will be
         * discarded. The animation executed must have a onComplete
         * handler, which calls the animationComplete() method.
         * When the animation has completed, if there are
         * animations present in the queue, the doAnimation method
         * will be called again.
         */
        var doAnimation = function() {
            var animation;
    
            while (animQueue.length > 0) {
                animation = animQueue.shift();
            }
    
            animating = true;
            animation();
        };
        
        var addExitClickHandler = function(el, hideSearch) {
        	var me = this;
            $('html').on('click', function(evt) {
                scheduleHide(hideSearch);
            });
            
            $(el).on('click', function(evt) {
                evt.stopPropagation();
            });
        };
        
        var removeExitClickHandler = function(el) {
            $('html').off('click');
            $(el).off('click');
        };
        
        /**
         * Called when an animation has finished, if there are
         * items in the animation queue; calls doAnimation.
         * Otherwise sets "animating" to false and returns.
         * @see doAnimation
         * @see scheduleShow
         * @see scheduleHide
         */
        var animationComplete = function() {
            if (animQueue.length > 0) {
                doAnimation();
            } else {
                animating = false;
            }
        };
        
        /**
         * Hides the results box, with a "slide" up animation
         */
        var hideResults = function(hideSearch) {
            removeExitClickHandler();
            
            hideSearch();
            
            $(results).hide();
            el.removeClass('noBorderBottom');
            open = false;
            animationComplete();
        };
        
        /**
         * Shows the results, with a "slide" animation, if the
         * results pane is visible, it will be resized to fit the
         * new data.
         */
        var displayResults = function(data, hideSearch) {
            var elOffset = el.offset();
            var elWidth = el.outerWidth();
            var parent = el.offsetParent();
            var parentOffset = parent.offset();
            var offset = elOffset.top - parentOffset.top + elHeight;
            var matches = data.unitDescs.length;
            var popupHeight = (data.resultCount) * 29;
    
            if (matches > 0) {
                popupHeight += 28;
            }
            
            if (data.truncated === true) {
                popupHeight += 28;
            }
            
            el.addClass('noBorderBottom');
    
            addExitClickHandler(results, hideSearch);
            
            if (!open) {
                $(results).css({
                    'height': popupHeight,
                    'width': elWidth,
                    'display': 'none',
                    'z-index': 100
                }).show("slide", { direction: "up"}, 300, function() {
                    open = true;
                    animationComplete();
                });
            } else {
                if (results.css('height') != popupHeight) {
                    $(results).animate({
                        //'top': (offset) + 'px',
                        //'left': elOffset.left - parentOffset.left,
                        'height': popupHeight + 'px'}, 100, function() {
                            animationComplete();
                        });
                } else {
                    animationComplete();
                }
            }
    
            var table = $('<table>');
            var thead = $('<thead>');
            var tbody = $('<tbody>');

            if (matches > 0) {
                var tr = $('<tr>').addClass('darkbg');
                
                for (var i = 0, l = data.headers.length; i < l; i++) {
                    tr.append('<th>' + data.headers[i] + '</th>');
                }
                
                thead.append(tr);
            }
            
            for (var i = 0; i < matches; i++) {
                var row = $('<tr class = "result">');
                var record = data.records[i];
                var field = data.unitDescs[i].fields;

                // Add a click handler to each row.
                (function(row, field) {
                    row.click(function() {
                       apply(field); 
                    });
                })(row, field);
                
                for (var j = 0; j < data.headers.length; j++) {
                    var val = record[j] || "";
                    row.append('<td>' + val + '</td>');
                }

                row.data('obj', data.unitDescs[i]);
                tbody.append(row);
            }

            if (data.truncated === true) {
                tbody.append('<tr><td colspan = "' +
                        data.headers.length + 
                            '">...</td></tr>');
            }
            
            table.append(thead);
            table.append(tbody);
            
            $(results).empty().append(table);
            
            $.publish('display-results');
        };
    };

    UnitSelector.prototype.setUnit = function(data) {
        this.inputBox.val(data.displayName);
        nx.hub.unitChanged.publish({
            id: data.id,
            unitNumber: data.displayName,
            fleetId: data.fleetId
        });
    };

    $.fn.unitSelector = function(searchConfig, action, hideSearch) {
        this.setUnit = function(unitDesc) {
            var selector = $(this).data('unitSelector');
            selector.setUnit(unitDesc);
        };

        return this.each(function() {
            var element = $(this);
            
            if (element.data('unitSelector')) {
                return;
            }
            
            var unitSelector = new UnitSelector(this, searchConfig, action, hideSearch);
            element.data('unitSelector', unitSelector);
        });
    };
})(jQuery);