var nx = nx || {};

nx.util = {
        CATEGORIES : {
            '-1' : 'INVALID',
            '0' : 'NODATA',
            '1' : 'NORMAL',
            '2' : 'ON',
            '3' : 'OFF',
            '4' : 'WARNING',
            '5' : 'WARNING1',
            '6' : 'WARNING2',
            '7' : 'FAULT',
            '8' : 'FAULT1',
            '9' : 'FAULT2',
            '10' : 'EVENT',
            '11' : 'EVENT_AP2',
            '12' : 'EVENT_AP1',
            '13' : 'EVENT_P2',
            '14' : 'EVENT_P1'
        },

        directions : {
            'BACKWARD': '-1',
            'TIME': '0',
            'FORWARD': '1'
        },

        timestampToHumanReadable : function(timestamp) {
            var units = [
                 604800000,
                 86400000,
                 3600000,
                 60000,
                 1000
            ];
            
            var unitLabels = {
                604800000: '{{PLURAL:$1|week|weeks}}',
                86400000: '{{PLURAL:$1|day|days}}',
                3600000: '{{PLURAL:$1|hour|hours}}',
                60000: '{{PLURAL:$1|minute|minutes}}',
                1000: '{{PLURAL:$1|second|seconds}}'
            };
            
            var timeStr = [];
            
            for (var i = 0, l = units.length; i < l; i++) {
                var unit = units[i];
                var count = (i < units.length - 1) ? parseInt(timestamp / unit) : timestamp / unit;
                
                timestamp = timestamp % unit;

                if (count > 0 || count < 0) {
                	timeStr.push(count + ' ' + $.i18n(unitLabels[unit], count));
                }

            }
            
            return timeStr.join(', ');
        },
        
        /**
         * Returns a formatted Date string
         * @param date
         *      An ISO formatted date
         */
        dateFormatFromISO: function(isoDate) {
            if (isoDate == null || isoDate == undefined) {
                return "";
            }
            
            var dateFields = isoDate.split("-");
            var formattedDate = [dateFields[2], '/',
                                 dateFields[1], '/', 
                                 dateFields[0]].join("");
            
            return formattedDate;
        },
        
        /**
         * Returns a formatted Date string
         * @param timestamp
         *      A long value that represents a timestamp
         */
        dateFormat: function(timestamp) {
            if (timestamp == null || timestamp == undefined) {
                return "";
            }
            
            var format2Digits = function(val) {
                if (val < 10) {
                    return '0' + val;
                }
                
                return val;
            };
            
            var date = new Date(timestamp);
            
            var formattedDate = [format2Digits(date.getDate()), '/',
                                 format2Digits(date.getMonth() + 1), '/', 
                                 date.getFullYear()].join("");
            
            return formattedDate;
        },
        
        /**
         * Returns a formatted Date string as UTC
         * @param timestamp
         *      A long value that represents a timestamp
         */
        dateFormatUTC: function(timestamp) {
            if (timestamp == null || timestamp == undefined) {
                return "";
            }
            
            var format2Digits = function(val) {
                if (val < 10) {
                    return '0' + val;
                }
                
                return val;
            };
            
            var date = new Date(timestamp);
            
            var formattedDate = [format2Digits(date.getUTCDate()), '/',
                                 format2Digits(date.getUTCMonth() + 1), '/', 
                                 date.getUTCFullYear()].join("");
            
            return formattedDate;
        },
        
        /**
         * Returns a formatted Date string YYYY-MM-DD
         * @param timestamp
         *      A long value that represents a timestamp
         */
        dateFormatPlot: function(timestamp) {
            if (timestamp == null || timestamp == undefined) {
                return "";
            }
            
            var format2Digits = function(val) {
                if (val < 10) {
                    return '0' + val;
                }
                
                return val;
            };
            
            var date = new Date(timestamp);
            
            var formattedDate = [date.getFullYear(), '-',
                                 format2Digits(date.getMonth() + 1), '-', 
                                 format2Digits(date.getDate())].join("");
            
            return formattedDate;
        },

        /**
         * Returns a "time ago" value
         * @param timestamp
         *      A long value that represents a timestamp
         */
        timeAgoFormat: function(timestamp) {
            var currDate = new Date();
            var date = new Date(timestamp);
            
            var val = null;
            var desc= null;
            var suffix = ' ago';

            //86400000 = 1 day (24 * 60 * 60 * 1000)
            var days = Math.floor((currDate.getTime() - date.getTime()) / 86400000);
            var years = Math.floor(days / 365);
            var months = Math.floor(days / 30);
            var weeks = Math.floor(days / 7);
            
            var seconds = Math.floor((currDate.getTime() - date.getTime()) / 1000);
            var hours = Math.floor(seconds / 3600);
            var minutes = Math.floor(seconds / 60);

            if (years >= 1) {
            	return $.i18n("$1 {{PLURAL:$1|year|years}} ago", years);
            } else if (months >= 1) {
            	return $.i18n("$1 {{PLURAL:$1|month|months}} ago", months);
            } else if (weeks >= 1) {
            	return $.i18n("$1 {{PLURAL:$1|week|weeks}} ago", weeks);
            } else if (days >= 1) {
            	return $.i18n("$1 {{PLURAL:$1|day|days}} ago", days);
            } else if (hours >= 1) {
            	return $.i18n("$1 {{PLURAL:$1|hour|hours}} ago", hours);
            } else if (minutes >= 1) {
            	return $.i18n("$1 {{PLURAL:$1|minute|minutes}} ago", minutes);
            } else if (seconds >= 1) {
            	return $.i18n("$1 {{PLURAL:$1|second|seconds}} ago", seconds);
            } else {
                val = $.format("{date}", timestamp);
                desc = '';
                suffix = '';
            }

            return [val, ' ', desc, suffix].join("");
        },

        addMaskDiv: function(divId) {
            var sourceDiv = ['<div id="', divId, '"></div>'].join("");
            $(sourceDiv).css({
                'position': 'absolute',
                'left': '0px',
                'top': '0px',
                'right': '0px',
                'bottom': '0px'
            }).appendTo($("body"));
        },

        removeMaskDiv: function(divId) {
            $('#' + divId).remove();
        },

        /**
         * Returns an empty string when the value is not an Integer
         * @param String
         */
        validateInt: function(value) {
            var result = value;
            var aux = parseInt(result) ;
            if (isNaN(aux)) { 
                result = "";
            }
            return result;
        },
        
        /**
         * Add options elements to an JQuery Select
         * @param Select, List of options
         */
        addSelectOption: function(select, list) {
            for (var i in list) {
                var option = list[i];
                select.append($("<option />").val(option.id).text(option.text));
            }
        },
        
        /**
         * Starts a file download, without requiring the user to leave their
         * current page. In order for this to work, the file must have a
         * Content-disposition attribute such as 'attachment:
         * filename=whatever.extension'. This works by inserting an iframe
         * into the body
         * 
         * @param String the url of the file to download
         */
        download: function(url) {
        	var me = this;
        	me.__timer = null;
            if (!!url) {
                var dlFrame = $('<iframe>')
                    .attr('id', 'dlFrame')
                    .attr('src', url)
                    .attr('onload', function() {
                    	me.__timer = window.setInterval(function() {
                    		if ($.cookie('fileDownloaded') != null) {
                    			window.clearInterval(me.__timer);
                            	if ($.cookie('fileDownloaded') != 'true') {
                            		alert('The download failed.');
                            	}
                    			$.cookie('fileDownloaded', null);
                    		}
                    	}, 1000);
                        $('#dlFrame').remove();
                    })
                    .css('display', 'none')
                    .appendTo('body')
            }
        },
        
        downloadPostWithJson: function(url, json) {
            $('<iframe>')
                .attr('id', 'myFrame')
                .attr('onload', function() { $('#myFrame').remove(); })
                .css('display', 'none')
                .appendTo('body');
        	
        	var form = $('<form>')
                .attr('id', 'myForm')
                .attr('action', url)
                .attr('target', 'myFrame')
                .attr('method', 'post')
                .attr('enctype', 'application/x-www-form-urlencoded')
        		.css('display', 'none')
        		.append(
        				$('<input>')
	        				.attr('type', 'hidden')
	                        .attr('name', 'data')
	                        .attr('id', 'data')
	                        .attr('value', JSON.stringify(json))
        		)
        		.append(
                        $('<input>')
                            .attr('type', 'hidden')
                            .attr('name', 'timezone')
                            .attr('id', 'timezone')
                            .attr('value', jstz.determine().name().replace('/', '-'))
                )
        		.appendTo('body');
        	
        	form.submit().remove();
        },
        
        downloadPostWithImage: function(service, imageData){
            $('body').append(
                    $('<iframe>')
                    .attr('id', 'dlFrame')
                    .attr('onload', function() {
                        $('#dlOpsFrame').remove();
                    })
                    .css('display', 'none')
            );

            $('body').append(
                    $('<form>')
                        .attr('id', 'dlOpsForm')
                        .attr('action', service)
                        .attr('target', 'dlOpsFrame')
                        .attr('method', 'post')
                        .attr('enctype', 'multipart/form-data')
                        .append($('<input>')
                                .attr('type', 'hidden')
                                .attr('name', 'imageData')
                                .attr('id', 'imageData')
                                .attr('value', imageData)
                            ) );
            
            $('#dlOpsForm').submit().remove();
        },
        
        downloadPostImageAndData: function(service, imageData, data){
            $('body').append(
                    $('<iframe>')
                    .attr('id', 'dlFrame')
                    .attr('onload', function() {
                        $('#dlOpsFrame').remove();
                    })
                    .css('display', 'none')
            );

            $('body').append(
                    $('<form>')
                        .attr('id', 'dlOpsForm')
                        .attr('action', service)
                        .attr('target', 'dlOpsFrame')
                        .attr('method', 'post')
                        .attr('enctype', 'multipart/form-data')
                        .append($('<input>')
                                .attr('type', 'hidden')
                                .attr('name', 'imageData')
                                .attr('id', 'imageData')
                                .attr('value', imageData),
                                $('<input>')
                                .attr('type', 'hidden')
                                .attr('name', 'data')
                                .attr('id', 'data')
                                .attr('value', data)
                            ) );
            
            $('#dlOpsForm').submit().remove();
        },
        
        /**
         * Open the unit summary identified by pair [fleetId,unitId] or [fleetId, unitNumber] in the tab {@param tab}.
         * One of unitId or unitNumber should be set along with fleetId.
         * For details why there are multiple way to identify unit @see UnitView.java.
         * @param unitId 
         * @param fleetId
         * @param tab 
         *      The Destination tab to be open. Can be null. 
         * @param unitNumber 
         */
        openUnitSummary : function(unitId, fleetId, tab, unitNumber) {

            nx.util.openHistoricUnitSummary(unitId, fleetId, tab, unitNumber, null);
        },
        
       /**
        * Open the unit summary identified by pair [fleetId,unitId] or [fleetId, unitNumber] 
        * in the tab {@param tab} at the specified time {@param timestamp}.
        * One of unitId or unitNumber should be set along with fleetId.
        * For details why there are multiple way to identify unit @see UnitView.java.
        * @param unitId 
        * @param fleetId
        * @param tab 
        *      The Destination tab to be open. Can be null. 
        * @param unitNumber
        * @param timestamp
        * @param faultId
        *      Used in Recovery Tab only 
        */
        openHistoricUnitSummary : function(unitId, fleetId, tab, unitNumber, timestamp, faultId) {
            
            if ( (!unitId || !fleetId) && (!unitNumber || !fleetId) ) {
                return;
            }
            
            var form = $('<form>')
                .attr('action', 'unit')
                .attr('method', 'post');
            
            if (!!unitId) {
                  $('<input>')
                  .attr('type', 'hidden')
                  .attr('name', 'unitId')
                  .attr('value', unitId)
                  .appendTo(form);
            } else {
                  $('<input>')
                  .attr('type', 'hidden')
                  .attr('name', 'unitNumber')
                  .attr('value', unitNumber)
                  .appendTo(form);
            }
            
            $('<input>')
                .attr('type', 'hidden')
                .attr('name', 'fleetId')
                .attr('value', fleetId)
                .appendTo(form);
            
            if (!!tab) {
                $('<input>')
                    .attr('type', 'hidden')
                    .attr('name', 'tab')
                    .attr('value', tab)
                    .appendTo(form);
            }

            if (!!timestamp) {
                $('<input>')
                    .attr('type', 'hidden')
                    .attr('name', 'timestamp')
                    .attr('value', timestamp)
                    .appendTo(form);
            }
            
            if (!!faultId) {
                $('<input>')
                    .attr('type', 'hidden')
                    .attr('name', 'faultId')
                    .attr('value', faultId)
                    .appendTo(form);
            }
            
            var hiddenDiv = $('<div>')
                .append(form)
                .css({'display': 'none'});
            
            $('body').append(hiddenDiv);
            
            form.submit();
        }
};