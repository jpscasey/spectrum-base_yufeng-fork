var nx = nx || {};
nx.cab = nx.cab || {};

/** A UnitSummary tab */
nx.cab.Widget = (function() {
    function ctor() {
    };
    
    /**
     * Updates the value of a widget. Note that a widget may display
     * multiple values, each associated with a different channel. i.e.
     * two dials..
     * 
     * @param channelName The name of the channel to update
     */
    ctor.prototype._update = function(channelName, channelValue, channelCategory) {};
    
    /**
     * Resets the component to a state with no value
     */
    ctor.prototype._reset = function() {};

    return ctor;
})();