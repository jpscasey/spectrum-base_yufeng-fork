var nx = nx || {};
nx.administration = nx.administration || {};

nx.administration.ChannelDefinition = (function() {
    function ctor () {
    	var me = this;
    	
    	this.__fleetCombo = $('#fleetCombo');
        this.__keyword = $('#keywordSearch');
        	
        this.__channelDefinitionGrid = $('#channelDefinitionGrid');
        this.__grid = null;
        this.__columnsConfig = [];
        this.__columns = [];
        this.__details = [];
        this.__isNew = false;
        this.__fleetChanged = true;
        this.__hasChannelDefinitionEditLicence = false;

        // Set labels on the screen
        $('#channelDefinitionDialog').attr("title", $.i18n("Channel Definition Details"));
        $('#channelDefinitionAlertDialog').attr("title", $.i18n("Error"));
 
        me.__initScreen();
    };

    ctor.prototype.__initScreen = function() {
        var me = this;

        nx.rest.fleet.getFleetList(function(fleets) {
            me.__loadFleetCombo(fleets);
            
            nx.rest.channelDefinition.conf(function(configuration) {
                me.__details = configuration.details;
                me.__columnsConfig = configuration.columns;
                me.__channelDefinitionDialog = me.__getDialog();
                me.__doSearch();
            });            
        });

        //keyword field
        this.__keyword.keydown(
            function(e) {
                if (e.which == 13) {
                    me.__doSearch();
                    return false;
                }
            }
        );

        // Search Button
        var searchButton = $('<button title = "'+$.i18n("Search")+'">'+$.i18n('Search')+'</button>').button({
            icons: {
                secondary: 'ui-icon-search'
            }
        }).click(function(e) {
            me.__doSearch();
        }).prop("tabindex", 3);

        $('#channelDefinitionHeader #searchButtonContainer').append(searchButton);
    };

    ctor.prototype.__initTable = function(fleetId) {
        var me = this;
        this.__columns = [];

        for (var i = 0; i < this.__columnsConfig.length; i++) {
            var column = this.__columnsConfig[i];
            
            if (!!column.fleets && $.inArray(fleetId, column.fleets) < 0) {
                continue;
            }
            
            this.__columns.push({
                'name': column.name,
                'text': column.header,
                'width': column.width + 'px',
                'type': column.type,
                'visible': column.visible,
                'sortable': column.sortable,
                'format': column.format,
                'handler': column.handler,
                'formatter': function(el, value) {
                    return me.__columnFormatter(el, value);
                }

            });
        }

        this.__channelDefinitionGrid.empty();
        this.__channelDefinitionGrid.append($('<table>').addClass('ui-corner-all'));
        this.__grid = this.__channelDefinitionGrid.tabular({
            'columns': [me.__columns],
            'cellClickHandler': (function(me) {
                return function(cell, data, columnDef) {
                    me.__clickHandler(cell, data, columnDef);
                }
            })(me),
            'cellMouseOverHandler': (function(me) {
                return function(cell, data, columnDef) {
                    me.__mouseoverHandler(cell, data, columnDef);
                }
            })(me),
            'scrollbar': true,
            'fixedHeader': true,
            'renderOnlyVisibleRows': true
        });
    };
    

    ctor.prototype.__columnFormatter = function(column, value) {
        return !!value ? value : "&nbsp;";
    };

    ctor.prototype.__clickHandler = function(cell, data, columnDef) {
        if (columnDef.handler == 'openChannelDefinitionDialog') {
            var fleetId = data.data[data.data.length - 1];
            var channelId = data.data[0];
            this.__channelDefinitionDialog._show(fleetId, channelId);
        }
    };

    ctor.prototype.__mouseoverHandler = function(cell, data, columnDef) {
        var text = cell[0].innerHTML;
        if (text != '&nbsp;') {
            cell.attr('title', text);
        }
    };

    ctor.prototype.__loadFleetCombo = function(fleets) {
        var me = this;

        $.each(fleets, function (id, fleet) {
            $('<option />', {
                'val': fleet.code,
                'text': fleet.name
            }).appendTo(me.__fleetCombo);
        });
        
        this.__fleetCombo.change(function() {
            me.__fleetChanged = true;
        });
        
        this.__fleetCombo.prop("selectedIndex", '').change();
    };

    ctor.prototype.__doSearch = function() {
        var me = this;

        var fleetId = this.__fleetCombo.val();
        
        if (!!this.__fleetChanged) {
            this.__initTable(fleetId);
            this.__channelDefinitionDialog.__setDetails(fleetId);
            this.__fleetChanged = false;
        }

        var parameters = { 
            'fleetId': fleetId,
            'keyword': this.__keyword.val()
        };
        $("body").addClass("wait");
        nx.rest.channelDefinition.search(parameters, function(channelDefinitions) {
        	$("body").removeClass("wait");
            me.__refreshTable(fleetId, channelDefinitions);
        });
    };

    ctor.prototype.__refreshTable = function(fleetId, channelDefinitions) {
        var gridData = [];

        if (!!channelDefinitions) {
            for (var index in channelDefinitions) {
                var channelDefinition = channelDefinitions[index];
                
                var dataRow = [];
                for (var columnIndex in this.__columns) {
                    var column = this.__columns[columnIndex];
                    var prefixedColumnName = column.name.split('.');

                    var val = '';
                    if (prefixedColumnName.length > 1) {
                        val = channelDefinition[prefixedColumnName[0]][prefixedColumnName[1]];
                    } else {
                        val = channelDefinition[prefixedColumnName[0]];
                    }
                    if(columnIndex ==2){
                    	val= $.i18n(val);
                    }
                    dataRow.push(val);
                }

                dataRow.push(fleetId);
                gridData.push({'id': index, 'data': dataRow});
            }

            this.__grid.setData(gridData, true);
        }
    };
    
    ctor.prototype.__getDialog = function() {
    	
    	var channelDefinitionPage = this;
        return new (function() {
            function ctor () {
                var me = this;
                this.__gotEditLicenceDfd = $.Deferred();
                this.__gotEditLicence = this.__gotEditLicenceDfd.promise();
                nx.rest.channelDefinition.getEditLicence(function(hasLicence){
                    me.__hasChannelDefinitionEditLicence = hasLicence;

                    me.__gotEditLicenceDfd.resolve();
                });
                
                this.__channelDefinitionDialog = $('#channelDefinitionDialog');
                this.__alertDialog = $("#channelDefinitionAlertDialog");
                this.__alertMessage = $("#channelDefinitionAlertMessage");
                this.__dialogFleetId = $('#dialogFleetId');

                this.__dialogRuleButtons = $('#dialogRuleButtons');
                this.__validationAddButton = $('#validationAddButton');

                this.__dialogRule = $('#dialogRule');
                this.__dialogRuleCombo = $('#dialogRuleCombo');
                this.__dialogRuleId = $('#dialogRuleId');
                this.__dialogRuleName = $('#dialogRuleName');
                this.__dialogRuleStatus = $('#dialogRuleStatus');
                this.__dialogRuleActive = $('#dialogRuleActive');

                this.__dialogRuleValidations = $('#dialogRuleValidations');

                this.__details = [];
                this.__channels = null;
                this.__rules = null;
                this.__selectedRule = null;
                this.__status = null;
                this.__channelId = "";
                this.__relatedChannelId = "";
                this.__deletedRule = false;
                this.__nameAsValidationsSource = null;

                this.__initScreen();
	            
	            }
            
	            ctor.prototype.__setDetails = function(fleetId) {
	                var me = this;
                    $.when(me.__gotEditLicence).done(function() {
                        $('#leftPanel').empty();
                        $.each(channelDefinitionPage.__details, function(index, detail) {
                            if (!!detail.fleets && $.inArray(fleetId, detail.fleets) < 0) {
                                return true;
                            }
                            me.__details.push(detail);
                            $('#leftPanel').append(me.__buildField(index, detail, me.__hasChannelDefinitionEditLicence));
                        });
	                });
	            }
	            
	            ctor.prototype.__buildField = function(i, fieldConf, hasLic) {
	            	var me = this;
                
            		var row = $('<div>').addClass('detailRow');
	                var label = $('<div>').addClass('detailLabel').text($.i18n(fieldConf.label));
	                var field = null;
	                
	                if (fieldConf.type == 'input_text') {
	                    field = $('<input>').attr('type', 'text').addClass('detailInput');
	                } else if (fieldConf.type == 'input_number') {
	                    field = $('<input>').attr('type', 'number').addClass('detailInput');
	                    if (!!fieldConf.min) {
	                        field.attr('min', fieldConf.min);
	                    }
	                    if (!!fieldConf.max) {
	                        field.attr('max', fieldConf.max);
	                    }
	                } else if (fieldConf.type == 'checkbox') {
	                    field = $('<input>').attr('type', 'checkbox').addClass('detailCheckbox');
	                } else if (fieldConf.type == 'select') {
	                    field = $('<select>').addClass('detailSelect');
	                } else if (fieldConf.type == 'text_area') {
	                    field = $('<textarea>').attr('spellcheck', 'false').addClass('detailInput');
	                }
	                
	                if (!!field) {
	                    field.attr('id', fieldConf.name + 'Detail');
	                    field.attr('tabindex', i);
	                    
	                    if (!fieldConf.editable || !hasLic) {
	                        field.attr('readonly', 'readonly');
	                        
	                        if (fieldConf.type == 'input_text' || fieldConf.type == 'input_number'
	                            || fieldConf.type == 'text_area') {
	                            field.addClass('readOnlyField');
	                        } else if (fieldConf.type == 'select') {
	                            field.attr('disabled', 'disabled');
	                            field.addClass('readOnlySelect');
	                        }
	                    }
	                    
	                    if (!!fieldConf.maxLength) {
	                        field.attr('maxLength', fieldConf.maxLength);
	                    }
	                    
	                    row.append(label);
	                    row.append(field);
	                    
	                    if (fieldConf.type == 'checkbox' || fieldConf.type == 'select') {
	                        field.uniform();
	                    }
	                }
	   
	                return row;
	            }
	
	            ctor.prototype.__initScreen = function() {
	            	var me = this;
		                nx.rest.channelDefinition.conf(function(configuration) {
		                    me.__nameAsValidationsSource = configuration.nameAsValidationsSource;
		                });
		    
		                this.__alertDialog.dialog({
		                    'autoOpen': false,
		                    'modal': true,
		                    'resizable': false,
		                    'buttons': {
		                        Ok: function() {
		                            $(this).dialog('close');
		                        }
		                    }
		                });
		    
		                this.__channelDefinitionDialog.dialog({
		                    'autoOpen': false,
		                    'height': 370,
		                    'width': 900,
		                    'modal': true,
		                    'resizable': false,
		                    'close': function (event, ui) { window.onbeforeunload = null; }
		                });
		    
		                this.__dialogRuleCombo.change(function() {
		                    var channelRuleId = $(this).val();
		                    me.__showRuleDefinition(channelRuleId);
		                });
		    
		                // rule add button
		                $('<button>')
		                    .attr('title', $.i18n('Add rule'))
		                    .addClass('dialogIconButton')
		                    .button({
		                        icons: {primary: 'ui-icon-plus'}
		                    })
		                    .click(function(e) {
		                        me.__doAddRule();
		                    })
		                    .appendTo(this.__dialogRuleButtons);
		    
		                // rule remove button
		                $('<button>')
		                    .attr('title', $.i18n('Remove rule'))
		                    .addClass('dialogIconButton')
		                    .button({
		                        icons: {primary: 'ui-icon-minus'}
		                    })
		                    .click(function(e) {
		                        me.__doRemoveRule();
		                    })
		                    .appendTo(this.__dialogRuleButtons);
		    
		                // validation add button
		                $('<button>')
		                    .attr('title', $.i18n('Add validation'))
		                    .addClass('dialogIconButton')
		                    .button({
		                        icons: {primary: 'ui-icon-plus'}
		                    })
		                    .click(function(e) {
		                        me.__ruleValidations();
		                        me.__doAddValidation(null);
		                    })
		                    .appendTo(this.__validationAddButton);
		    
		                // save button
		                $('<button title = "'+$.i18n("Save")+'">'+$.i18n("Save")+'</button>')
		                    .button()
		                    .click(function(e) {
		                        me.__collectRuleDetails(me.__selectedRule);
		                        if (me.__deletedRule) {
		                            $('<div></div>').dialog({
		                                buttons: { 
		                                	'Ok': function () { 
		                                		me.__doSave();
		                                		$(this).dialog('close'); 
		                                	}, 
		                                	'Cancel': function () {
		                                		me.__loadRules(me.__channelDefinition.channelRuleList);
		                                		$(this).dialog('close');                                		
		                                	} 
		                                },
		                                close: function (event, ui) { $(this).remove(); },
		                                resizable: false,
		                                title: $.i18n('Notification'),
		                                modal: true
		                            }).text('You are about to remove rule(s). This is not reversible. Are you sure?');
		                        } else {
		                            me.__doSave();
		                        }
		                    })
		                    .prop("tabindex", 3) //FIXME - correct the tabindex
		                    .addClass('ui-button-primary')
		                    .appendTo($('#channelDefinitionDialog #saveButtonContainer'));
		    
		                // cancel button
		                $('<button title = "'+$.i18n("Cancel")+'">'+$.i18n("Cancel")+'</button>')
		                    .button()
		                    .click(function(e) {
		                        me.__hide();
		                        window.onbeforeunload = null;
		                    })
		                    .prop("tabindex", 3) //FIXME - correct the tabindex
		                    .addClass('ui-button-secondary')
		                    .appendTo($('#channelDefinitionDialog #cancelButtonContainer'));
		                
		                // warning message div
		                $('<div>')
		                	.attr('id', 'invalidRuleMessage')
		                	.addClass('invalidRuleDiv')
		                	.appendTo($('#channelDefinitionDialog #bottomPanel'));
	            };
	
	            
	            ctor.prototype.__loadRestrictions = function() {
		            if (!this.__hasChannelDefinitionEditLicence) {
	                	$('#channelDefinitionDialog button').attr('disabled', 'disabled');
	                	$('#channelDefinitionDialog input').attr('disabled', 'disabled');
	                	$('#channelDefinitionDialog textarea').attr('disabled', 'disabled');
	                	$('#channelDefinitionDialog select').attr('disabled', 'disabled');
	                	$('#dialogRuleCombo').removeAttr('disabled', 'disabled');
	                	$.uniform.update($('#channelDefinitionDialog input'));
	                	$.uniform.update($('#channelDefinitionDialog textarea'));
	                	$.uniform.update($('#channelDefinitionDialog select'));
	                	$('#rightPanel input').addClass('readOnlyField');
	                	$('#channelDefinitionDialog button').addClass('ui-state-disabled');
	                }
	            };
        
	            ctor.prototype.__doAddRule = function() {
	            	
	            	if (this.__hasChannelDefinitionEditLicence) {
		                this.__dialogRuleCombo.val('').change();
		    
		                this.__resetRule();
		    
		                this.__dialogRule
		                    .removeClass('ruleNonVisible')
		                    .addClass('ruleVisible');
	            	}
	            };
	
	            ctor.prototype.__doRemoveRule = function() {

	            	if (this.__hasChannelDefinitionEditLicence) {
		                this.__resetRule();
		    
		                var ruleId = this.__dialogRuleCombo.val();
		                if (!!ruleId) {
		                    this.__deletedRule = true;
		                    delete this.__rules[ruleId];
		                    this.__dialogRuleCombo.find('[value="' + ruleId + '"]').remove();
		                    this.__dialogRuleCombo.val('').change();
		                    //$.uniform.update(this.__dialogRuleCombo);
		                }
	                }
	            };
	
	            ctor.prototype.__ruleValidations = function() {
	                this.__collectRuleDetails(this.__selectedRule);
	                
	                var invalidRule = false;
	                var rules = [];
	                $('.invalidInput').removeClass('invalidInput');
	                $('#invalidRuleMessage').empty();
	                
	                for (var index in this.__rules) {
	                    var rule = this.__rules[index];                    
	    
	                    var ruleParam = {
	                        'name': rule.name,
	                        'statusId':rule.status.id,
	                        'active': rule.active,
	                        'validations': []
	                    };
	    
	                    for (var vIndex in rule.validations) {
	                        var validation = rule.validations[vIndex];
	                        
	                        // Search channels to get index matching validation channel
	                        var chanID;
	                        for(var i = 0; i < this.__channels.length; i++){
	                            if(this.__channels[i].id == validation.channel.id){
	                                chanID = i;
	                                break;
	                            }
	                        }
	
	                        if (rule.validations[vIndex].channel.id == '') {
	                            this.__showErrorRuleDefinition(index);
	                            this.__showWarning('Invalid rule "' + rule.name + ': no channel selected');
	                            
	                            invalidRule = true;
	                            
	                            var ruleEl = $('#ruleId_' + index + '_' + vIndex);
	                            var ruleElCh = ruleEl.find('#validationChannel');
	                            ruleElCh.addClass('invalidInput');
	                        }
	                            
	                        // Validate input for Digital channels, ensure they are 0 or 1 or empty  this.__channelDefinition.channelConfig.type == "DIGITAL"
	                        if(this.__channels[chanID].type == "DIGITAL"){
	    
	                            if($.inArray(parseFloat(validation.minValue), [0,1]) == -1 && !isNaN(parseFloat(validation.minValue))){                     
	                                this.__showErrorRuleDefinition(index);
	                                this.__showWarning('Invalid rule "' + rule.name + '": min value of digital channel "' + this.__channels[chanID].name + '" must equal 0 or 1 or blank');
	                                
	                                invalidRule = true;
	                                
	                                var ruleEl = $('#ruleId_' + index + '_' + vIndex);
	                                var ruleElMin = ruleEl.find('#validationMinValue');
	                                var ruleElMax = ruleEl.find('#validationMaxValue');
	                                ruleElMin.addClass('invalidInput');
	                            }
	    
	                            if($.inArray(parseFloat(validation.maxValue), [0,1]) == -1 && !isNaN(parseFloat(validation.maxValue))){
	                                this.__showErrorRuleDefinition(index);
	                                this.__showWarning('Invalid rule "' + rule.name + '": max value of digital channel "' + this.__channels[chanID].name + '"  must equal 0 or 1 or blank');
	                                
	                                invalidRule = true;
	                                
	                                var ruleEl = $('#ruleId_' + index + '_' + vIndex);
	                                var ruleElMin = ruleEl.find('#validationMinValue');
	                                var ruleElMax = ruleEl.find('#validationMaxValue');
	                                ruleElMax.addClass('invalidInput');
	                            }
	                        }
	                                                
	                        if (isNaN(parseFloat(validation.maxValue)) && isNaN(parseFloat(validation.minValue))) {
	                            this.__showErrorRuleDefinition(index);
	                            this.__showWarning('Invalid rule "' + rule.name + '": max and min cannot both be empty');
	        
	                            invalidRule = true;
	                            
	                            var ruleEl = $('#ruleId_' + index + '_' + vIndex);
	                            var ruleElMin = ruleEl.find('#validationMinValue');
	                            var ruleElMax = ruleEl.find('#validationMaxValue');
	                            ruleElMin.addClass('invalidInput');
	                            ruleElMax.addClass('invalidInput');
	                        }
	
	                        if ((parseFloat(validation.minValue) > parseFloat(validation.maxValue))) {
	                        	this.__showErrorRuleDefinition(index);
	                        	this.__showWarning('Min value "'+validation.minValue+'" should be smaller or equals to max value "' + validation.maxValue + '" on rule "' +
	                                    rule.status.name + ' - ' + rule.name + '"');
	                       
	                            invalidRule = true;
	                            
	                            var ruleEl = $('#ruleId_' + index + '_' + vIndex);
	                            var ruleElMin = ruleEl.find('#validationMinValue');
	                            var ruleElMax = ruleEl.find('#validationMaxValue');
	                            ruleElMin.addClass('invalidInput');
	                            ruleElMax.addClass('invalidInput');
	                        }
	                        
	                        if (validation.minValue == null){
	                        	validation.minValue = "";
	                        }
	                        if (validation.maxValue == null){
	                        	validation.maxValue = "";
	                        }
	
	    
	                        if (!!validation.channel.id) {
	                            ruleParam.validations.push({
	                                'channelId': validation.channel.id,
	                                'minValue': validation.minValue,
	                                'maxValue': validation.maxValue,
	                                'minInclusive': validation.minInclusive,
	                                'maxInclusive': validation.maxInclusive
	                            });
	                        }
	                    }
	    
	                    rules.push(ruleParam);
	                }
	                
	                if(!!invalidRule){
	                    $("body").removeClass("wait");
	                    return;
	                }
	                
	                return rules;
	            }
	            
	            ctor.prototype.__doSave = function() {
            	
	            	if (this.__hasChannelDefinitionEditLicence) {
		                var me = this;
		                var invalid = false;
		                var invalidField = "";
		                
		                var channelDefinitionParam = {
		                    'fleetId': this.__dialogFleetId.val(),
		                    'id': this.__channelId,
		                    'relatedChannelId': this.__relatedChannelId
		                };
		                
		                $("body").addClass("wait");
		                $.each(this.__details, function(index, detail) {
		                    if (!!detail.editable) {
		                        var field = $('#' + detail.name + 'Detail');
		                        if (detail.type == 'checkbox') {
		                            channelDefinitionParam[detail.name] = field.is(':checked');
		                        } else {
		                            var value = $.trim(field.val());
		                            if (detail.type == 'input_number' && !!value) {
		                                var num = Number(value);
		                                if (!!detail.min) {
		                                    if (num < detail.min) {
		                                        invalid = true;
		                                        invalidField = detail.label;
		                                        return false;
		                                    }
		                                }
		                                if (!!detail.max) {
		                                    if (num > detail.max) {
		                                        invalid = true;
		                                        invalidField = detail.label;
		                                        return false;
		                                    }
		                                }
		                            }
		                            channelDefinitionParam[detail.name] = value;
		                        }
		                    }
		                });
		                
		                if (!!invalid) {
		                	$("body").removeClass("wait");
		                    this.__showWarning('Invalid value for field ' + invalidField);
		                    return;
		                }
		
		                var rules = me.__ruleValidations();
		    
		                nx.rest.channelDefinition.save(channelDefinitionParam, rules, function(status) {
		                	$("body").removeClass("wait");
		                    if (!status) {
		                        me.__showWarning('Error Saving Channel Definition');
		                    } else {
		                    	channelDefinitionPage.__doSearch();
		                        me.__hide();
		                        window.onbeforeunload = null;
		                    }
		                });
	            	}
	            };
	
	            ctor.prototype.__showAlert = function(message) {
	                this.__alertMessage.text(message);
	                this.__alertDialog.dialog('open');
	            };
	            
	            ctor.prototype.__showWarning = function(message) {
	            	$('#invalidRuleMessage').text(message).addClass('invalidInput');
	            };
	
	            ctor.prototype.__hide = function() {
	                this.__channelDefinitionDialog.dialog('close');
	            };
	
	            ctor.prototype._show = function(fleetId, channelId) {
	                var me = this;
	    
	                me.__deletedRule = false;
	                this.__reset();
	    
	                var channelsDfd = $.Deferred();
	                var channelsLoaded = channelsDfd.promise();
	                nx.rest.channelDefinition.channels(fleetId, function(channels) {
	                    me.__channels = channels;
	                    channelsDfd.resolve();
	                });
	    
	                var groupsDfd = $.Deferred();
	                var groupsLoaded = groupsDfd.promise();
	                nx.rest.channelDefinition.groups(fleetId, function(groups) {
	                    me.__loadGroups(groups);
	                    groupsDfd.resolve();
	                });
	    
	                var statusDfd = $.Deferred();
	                var statusLoaded = statusDfd.promise();
	                nx.rest.channelDefinition.status(fleetId, function(status) {
	                    me.__loadStatus(status);
	                    statusDfd.resolve();
	                });
	    
	                $.when(channelsLoaded, groupsLoaded, statusLoaded).done(function() {
	                    nx.rest.channelDefinition.channel(fleetId, channelId, function(channelDefinition) {
	                        me.__loadChannelDefinition(fleetId, channelDefinition);
	    	                me.__loadRestrictions();
	                    });
	                });
	                
	                this.__channelDefinitionDialog.dialog('open');
	            };
	
	            ctor.prototype.__reset = function() {
	                this.__dialogFleetId.val('');
	                
	                var textFields = $('.detailInput');
	                var checkboxFields = $('.detailCheckbox');
	                var selectFields = $('.detailSelect');
	                
	                $.each(textFields, function(index, textField) {
	                    $(textField).val('');
	                });
	                
	                $.each(checkboxFields, function(index, checkboxField) {
	                    $(checkboxField).prop('checked', false);
	                    $.uniform.update(checkboxField);
	                });
	                
	                $.each(selectFields, function(index, selectField) {
	                    $(selectField).empty();
	                    $.uniform.update(selectField);
	                });
	    
	                this.__dialogRuleCombo.empty();
	                $.uniform.update(this.__dialogRuleCombo);
	    
	                this.__resetRule();
	                
	                this.__channelId = "";
	                this.__relatedChannelId = "";
	                
	                $('.invalidInput').removeClass('invalidInput');
	                $('#invalidRuleMessage').empty();
	            };
	
	            ctor.prototype.__resetRule = function() {
	                this.__selectedRule = null;
	    
	                this.__dialogRule
	                    .removeClass('ruleVisible')
	                    .addClass('ruleNonVisible');
	    
	                this.__dialogRuleId.val('');
	                this.__dialogRuleName.val('');
	    
	                this.__dialogRuleStatus.val('').change();
	    
	                this.__dialogRuleActive.prop('checked', false);
	                $.uniform.update(this.__dialogRuleActive);
	    
	                this.__dialogRuleValidations.text('');
	            };
	
	            ctor.prototype.__loadRules = function(channelRuleList) {
	                var me = this;
	    
	                this.__rules = {};
	    
	    			$('<option />', {
	                    'val': '',
	                    'text': ''
	                }).appendTo(me.__dialogRuleCombo);
	    
	                $(channelRuleList).each(function () {
	                    me.__rules[this.id] = this;
	    
	                    $('<option />', {
	                        'val': this.id,
	                        'text': this.status.name + ' - ' + this.name
	                    }).appendTo(me.__dialogRuleCombo);
	                });
	                
	                if(!!channelRuleList[0]) {
	                	me.__dialogRuleCombo.val(channelRuleList[0].id);
	                	$.uniform.update(this.__dialogRuleCombo);
	                	this.__dialogRuleCombo.change();
	                }
	                
	            };
	
	            ctor.prototype.__showRuleDefinition = function(channelRuleId) {
	                this.__collectRuleDetails(this.__selectedRule);
	    
	                $.uniform.update(this.__dialogRuleCombo);
	                $('#invalidRuleMessage').empty().removeClass('invalidInput');
	    
	                this.__resetRule();
	    
	                this.__selectedRule = this.__rules[channelRuleId];
	    
	                if (!!this.__selectedRule) {
	                    this.__dialogRule
	                        .removeClass('ruleNonVisible')
	                        .addClass('ruleVisible');
	    
	                    this.__dialogRuleId.val(this.__selectedRule.id);
	                    this.__dialogRuleName.val(this.__selectedRule.name);
	                    this.__dialogRuleStatus.val(this.__selectedRule.status.id).change();
	                    this.__dialogRuleActive.prop('checked', this.__selectedRule.active);
	                    $.uniform.update(this.__dialogRuleActive);
	    
	                    this.__showRuleValidations(this.__selectedRule.validations);
	                }
	            };
	
	            ctor.prototype.__collectRuleDetails = function(rule) {
	                var name = $.trim(this.__dialogRuleName.val());
	                var status = this.__status[this.__dialogRuleStatus.val()];
	                this.__isNew = false;
	    
	                if (name == '' || !status) {
	                    return;
	                }
	    
	                if (!rule) {
	                	
	                	this.__isNew = true;
	                	
	                    rule = {
	                        'id': new Date().getTime() // Date.now() doesn't work in IE8
	                    };
	    
	                    $('<option />', {
	                        'val': rule.id,
	                        'text': status.name + ' - ' + name
	                    }).appendTo(this.__dialogRuleCombo);
	                }
	    
	                rule.name = $.trim(name);
	                rule.status = status;
	                rule.active = this.__dialogRuleActive.is(':checked') ;
	                rule.validations = this.__getRuleValidations();
	    
	                this.__rules[rule.id] = rule;
	            };
	            
	            ctor.prototype.__showErrorRuleDefinition = function(channelRuleId) {
	    
	                $.uniform.update(this.__dialogRuleCombo);
	    
	                this.__resetRule();
	    
	                this.__selectedRule = this.__rules[channelRuleId];
	    
	                if (!!this.__selectedRule) {
	                    this.__dialogRule
	                        .removeClass('ruleNonVisible')
	                        .addClass('ruleVisible');
	    
	                    this.__dialogRuleId.val(this.__selectedRule.id);
	                    this.__dialogRuleName.val(this.__selectedRule.name);
	                    this.__dialogRuleStatus.val(this.__selectedRule.status.id).change();
	                    this.__dialogRuleActive.prop('checked', this.__selectedRule.active);
	                    $.uniform.update(this.__dialogRuleActive);
	    
	                    this.__showRuleValidations(this.__selectedRule.validations);
	                }
	                this.__dialogRuleCombo.val(channelRuleId).change();
	            };
	
	            ctor.prototype.__getRuleValidations = function() {
	                var validations = [];
	    
	                var dialogValidations = $('.dialogRuleValidation');
	    
	                $(dialogValidations).each(function () {
	                    var dialogValidation = $(this);
	    
	                    var minValue = dialogValidation.find('#validationMinValue').val();
	                    var minInclusive = dialogValidation.find('#validationMinInc').val();
	                    var channelId = dialogValidation.find('#validationChannel').val();
	                    var maxInclusive = dialogValidation.find('#validationMaxInc').val();
	                    var maxValue = dialogValidation.find('#validationMaxValue').val();
	    
	                    validations.push({
	                        'minValue': $.trim(minValue),
	                        'minInclusive': minInclusive,
	                        'channel': {
	                            'id': channelId
	                        },
	                        'maxInclusive': maxInclusive,
	                        'maxValue': $.trim(maxValue)
	                    });
	                });
	    
	                return validations;
	            };
	
	            ctor.prototype.__showRuleValidations = function(validations) {
	                var me = this;
	    
	                $(validations).each(function (index, item) {
	                    me.__doAddValidation(this, index);
	                });
	            };
	
	            ctor.prototype.__doAddValidation = function(validation, index) {
	                var me = this;
	    
	                if (!validation) {
	                    outer:
	                        if (!this.__selectedRule) {
	                            for (var index in this.__rules) {
	                                this.__selectedRule = this.__rules[index];
	                            }
	                        }
	                }
	                
	                var validationBox = $('<div>')
	                    .addClass('dialogRuleValidation')
	                    .attr('id', 'ruleId_' + this.__selectedRule.id + '_' + index)
	                    .appendTo(this.__dialogRuleValidations);
	    
	                $('<input>')
	                    .attr('id', 'validationMinValue')
	                    .attr('type', 'text')
	                    .val(!!validation ? validation.minValue : '')
	                    .appendTo(validationBox);
	    
	                $('<select>')
	                    .attr('id', 'validationMinInc')
	                    .append($('<option />', {'val': 'false', 'text': '<'}))
	                    .append($('<option />', {'val': 'true', 'text': '<='}))
	                    .val(!!validation && !!validation.minInclusive && validation.minInclusive != "false" ? 'true' : 'false')
	                    .appendTo(validationBox)
	                    .uniform();
	    
	                // begin - channels combo
	                var channelSelect = $('<select>')
	                    .attr('id', 'validationChannel')
	                    .appendTo(validationBox)
	                    .uniform();
	    
	                $('<option />', {
	                    'val': '',
	                    'text': ''
	                }).appendTo(channelSelect);
	    
	                if (this.__nameAsValidationsSource) {
	                    $(this.__channels).each(function () {
	                        $('<option />', {
	                            'val': this.id,
	                            'text': this.name
	                        }).appendTo(channelSelect);
	                    });
	                } else {
	                    $(this.__channels).each(function () {
	                        $('<option />', {
	                            'val': this.id,
	                            'text': $.i18n(this.description)
	                        }).appendTo(channelSelect);
	                    });
	                }
	    
	                channelSelect.val(!!validation ? validation.channel.id : '').change();
	                // end
	    
	                $('<select>')
	                    .attr('id', 'validationMaxInc')
	                    .append($('<option />', {'val': false, 'text': '<'}))
	                    .append($('<option />', {'val': true, 'text': '<='}))
	                    .val(!!validation && !!validation.maxInclusive && validation.maxInclusive != "false" ? 'true' : 'false')
	                    .appendTo(validationBox)
	                    .uniform();
	    
	                $('<input>')
	                    .attr('id', 'validationMaxValue')
	                    .attr('type', 'text')
	                    .val(!!validation ? validation.maxValue : '')
	                    .appendTo(validationBox);
	    
	                // validation remove button
	                var validationRemoveButton = $('<div>')
	                    .attr('id', 'validationRemoveButton')
	                    .appendTo(validationBox);
	    
	                $('<button>')
	                    .attr('title', $.i18n('Remove validation'))
	                    .addClass('dialogIconButton')
	                    .button({
	                        icons: {primary: 'ui-icon-minus'}
	                    })
	                    .click(function(e) {
	                        me.__doRemoveValidation(validationBox);
	                    })
	                    .appendTo(validationRemoveButton);
	                
	                me.__loadRestrictions();
	            };
	
	            ctor.prototype.__doRemoveValidation = function(validationBox) {
	                validationBox.remove();
	            };
	
	            ctor.prototype.__loadGroups = function(groups) {
	                var me = this;
	                var groupCombo = $('#channelGroupIdDetail');
	                
	                if (groupCombo.length > 0) {
	                    $(groups).each(function () {
	                        $('<option />', {
	                            'val': this.id,
	                            'text': $.i18n(this.description)
	                        }).appendTo(groupCombo);
	                    });
	                }
	            };
	
	            ctor.prototype.__loadStatus = function(status) {
	                var me = this;
	    
	                this.__status = {};
	    
	                this.__dialogRuleStatus.empty();
	                $.uniform.update(this.__dialogRuleStatus);
	    
	                $('<option />', {
	                    'val': '',
	                    'text': ''
	                }).appendTo(this.__dialogRuleStatus);
	    
	                $(status).each(function () {
	                    me.__status[this.id] = this;
	    
	                    $('<option />', {
	                        'val': this.id,
	                        'text': this.name
	                    }).appendTo(me.__dialogRuleStatus);
	                });
	            };
	
	            ctor.prototype.__loadChannelDefinition = function(fleetId, channelDefinition) {
	                window.onbeforeunload = function() {
	                    return 'Are you sure you want to navigate away from this page?';
	                };
	                
	                var me = this;
	                me.__channelDefinition = channelDefinition;
	            	var channelConfig = channelDefinition.channelConfig;
	            	this.__dialogFleetId.val(fleetId);
	            	this.__channelId = channelConfig.id;
	            	if (!!channelConfig.relatedChannelGroup) {
	                    this.__relatedChannelId = channelConfig.relatedChannelGroup.id.toString();
	            	}
	
	                $.each(this.__details, function(index, detail) {
	                    var dialogField = $('#' + detail.name + 'Detail');
	                    var value = channelConfig[detail.name];
	                    if (detail.type == 'input_text' || detail.type == 'input_number' 
	                        || detail.type == 'text_area') {
	                    	if(detail.name == 'description'){
	                    		dialogField.val($.i18n(value));
	                    	}
	                    	else{
	                    		dialogField.val(value);
	                    	}
	                    } else if (detail.type == 'checkbox') {
	                        dialogField.prop('checked', value);
	                        $.uniform.update(dialogField);
	                    } else if (detail.type == 'select') {
	                        dialogField.val(value).change();
	                    }
	                });
	    
	                this.__loadRules(channelDefinition.channelRuleList);
	            };
	
	            return ctor;
	        }());
       
        };

    return ctor;
}());

//channel definition access logging
nx.rest.logging.channelDefinition();

$(document).ready(function() {
    new nx.administration.ChannelDefinition();
});