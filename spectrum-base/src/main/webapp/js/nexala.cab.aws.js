var nx = nx || {};
nx.cab = nx.cab || {};

nx.cab.Aws = (function () {

    var COLOR1 = "#eabe3f";
    var COLOR2 = "#e8cf8a";
    var BORDER = "#666";
    var BLACK = "#000";
    var SECTIONS = 20;

    var ctor = function (el, component, settings) {
        nx.cab.Widget.call(this);
        
        this._component = component; 
        this.settings = $.extend({
            borderColor: '#888888',
            borderWidth: 0.004,
            cx: component.x +component.width / 2,
            cy: component.y +component.height / 2,
            faceFill: '90-#333-#222',
            faceShadowFill: '90-#222-#333-#444',
            faceShadowStroke: '#333',
            fontSize: 12,
            radius: Math.min(component.width, component.height) / 2,
            rimFill: '#555555',
            rimWidth: 0.06,
            rimHlFill: '90-#999-#333-#999-#333-#333-#888'
        }, settings);

        this._yellowBlack = true;

        this._paper = el;
        this._draw();
    };
    
    ctor.prototype = new nx.cab.Widget();
    
    ctor.prototype._sector = function(cx, cy, radius, startAngle, endAngle) {
        var x1 = cx + radius * Math.cos(Raphael.rad(-startAngle));
        var x2 = cx + radius * Math.cos(Raphael.rad(-endAngle));
        var y1 = cy + radius * Math.sin(Raphael.rad(-startAngle));
        var y2 = cy + radius * Math.sin(Raphael.rad(-endAngle));
        var path = this._paper.path(["M", cx, cy, "L", x1, y1, "A", radius, radius,
                0, +(endAngle - startAngle > 180), 0, x2, y2, "z"]);
        path.attr({"stroke-width": 0, "stroke": null});
        return path;
    };

    ctor.prototype._draw = function() {
        var cx = this.settings.cx;
        var cy = this.settings.cy;
        var r = this.settings.radius;
        var rimWidth = r * this.settings.rimWidth;
        var borderColor = this.settings.borderColor;
        var borderWidth = r * this.settings.borderWidth;
        var outerGaugeBorder = this._paper.circle(cx, cy, r);
        var i = 0;
        var l = 0;

        outerGaugeBorder.attr({
            fill: borderColor,
            stroke: null
        });

        var rim = this._paper.circle(cx, cy, r - borderWidth);

        rim.attr({
            fill: this.settings.rimFill
        });

        var rimHl = this._paper.circle(
            cx,
            cy - 3,
            r - borderWidth - rimWidth * 0.8,
            r - rimWidth * 0.4
        );

        rimHl.attr({
            fill: this.settings.rimHlFill,
            stroke: null
        });
    
        var faceShadow = this._paper.circle(
            cx,
            cy,
            r - rimWidth - borderWidth);

        faceShadow.attr({
            fill: this.settings.faceShadowFill,
            stroke: this.settings.faceShadowStroke
        });  
    
        var face = this._paper.circle(
            cx,
            cy,
            r - rimWidth - borderWidth - 5);

        face.attr({
            fill: this.settings.faceFill,
            stroke: null
        });

        this._createAws(cx, cy, r * .95);
    };
    

    ctor.prototype._createAws = function(cx, cy, radius) {
        
        this._outer = this._paper.circle(cx, cy, (radius - 1));
        this._inner = this._paper.circle(cx, cy, (radius - 1)/ 2);

        // Set the background colours
        if (this._yellowBlack == true) {
            this._outer.attr({"fill": COLOR1, "stroke": 'none'});
            this._inner.attr({"fill": COLOR2, "stroke": BORDER});
        } else {
            this._outer.attr({"fill": BLACK, "stroke": 'none'});
            this._inner.attr({"fill": BLACK, "stroke": BLACK});
        }
        
        // Add the yellow/black segments
        for (var i = 0; i < SECTIONS; i++) {
            var startAngle = 360 / SECTIONS * i; 
            var endAngle = 360 / SECTIONS * (i + 1); 
            
            if (i % 2 == 1) {
                var path = this._sector(cx, cy, radius - 2.5, startAngle, endAngle);
                path.attr({fill: BLACK});
            }
        }
        
        
        this._set = this._paper.set();
        // Add the yellow/black segments
        for (var i = 0; i < SECTIONS; i++) {
            var startAngle = 360 / SECTIONS * i; 
            var endAngle = 360 / SECTIONS * (i + 1); 
            
            if (i % 2 == 1) {
                var path = this._sector(cx, cy, radius - 2.5, startAngle, endAngle);
                path.attr({fill: BLACK});
                this._set.push(path);
            }
        }
    };
    
    ctor.prototype._update = function(channelName, channelValue, channelCategory) {
        if (!channelValue) {
            this._set.animate({transform :'R' + (360 / SECTIONS)+ ',' +  this.settings.cx + ',' + this.settings.cy}, 500);
        }
        
    };

    return ctor;
}());
