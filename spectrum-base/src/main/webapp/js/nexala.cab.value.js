var nx = nx || {};
nx.cab = nx.cab || {};

/**
 * Different label positions : 
 * 0 : Top
 * 1 : Bottom
 * 2 : left
 * 3 : right
 * 
 * Different text anchors :
 * 0 : Start
 * 1 : Middle
 * 2 : End
 */
nx.cab.Value = (function () {


    var ctor = function (el, component, settings) {
        nx.cab.Widget.call(this);
        this._component = component;
        this.settings = $.extend({
            label : null,
            suffix : '',
            labelPosition : 2,
            textAnchor : 1,
            labelWidth : null,
            defaultColor:'#ddd',
            labelColor: '#ddd',
            width: component.width,
            height: component.height
        }, settings);
        
        this._paper = el;
        this._draw();
    };
    
    ctor.prototype = new nx.cab.Widget();
    
    ctor.prototype._update = function(channelName, channelValue, channelCategory) {
        
        var color = this.settings.defaultColor;
        var format = this.settings.format;
        
        // Check if we don't associate a color for this category
        for (var cat in this.settings.colorForChanCategory) {
            if(cat == channelCategory) {
                color = this.settings.colorForChanCategory[cat];
            }
       }
        
        var value = '';
        if (channelValue != null) {
        	(format !=null)? value = $.format(format,channelValue) : value = channelValue;
           	value += this.settings.suffix;
        }

        var cmp = $('#'+this._component.id);

        cmp.text(value);
        cmp.css('color', color);
    };
    
    ctor.prototype._reset = function() {
    	var cmp = $('#'+this._component.id);
    	cmp.text('');
    };
    
    ctor.prototype._draw = function() {
        
        var xLbl = 0, yLbl = 0, xValue = 0, yValue = 0;
        
        var lblWidth = 0;
        var valWidth = 0;
        var height = 0;
        
        var anchor =  this.settings.textAnchor;
        var lblPos = this.settings.labelPosition;
        
        // Calculate the text anchor
        var textAnchor = 'center';
        
        if (anchor == 0) {
            textAnchor = 'left';
        } else if (anchor == 2) {
            textAnchor = 'right';
        }
        

        if (this.settings.label != null) {
            //Calculate positions

            // vertical alignment
            if(lblPos == 0 || lblPos == 1) {
                xLbl =  0;
                xValue = xLbl;
                lblWidth = this.settings.width;
                valWidth = this.settings.width;
                height = this.settings.height/2;
                
                // Label on top
                if (lblPos == 0) {
                    yLbl = 0;
                    yValue = this.settings.height/2;
                } else {
                    // Label at bottom
                    yValue = 0;
                    yLbl = this.settings.height/2;
                }
            }
            
            // horizontal alignment
            if (lblPos == 2 || lblPos == 3) {
                yLbl = 0;
                yValue = 0;
                height = this.settings.height;
                
                if (this.settings.labelWidth == null) {
                    lblWidth = this.settings.width/2;
                } else {
                    lblWidth = this.settings.labelWidth;
                }
                
                valWidth = this.settings.width - lblWidth;

                
                if (lblPos == 2) {
                    // Label on left
                    xLbl = 0;  
                    xValue = lblWidth;
                } else if (lblPos == 3) {
                    // Label on right
                    xValue = 0;
                    xLbl = valWidth;
                }
                
            }
            


            // Label
            var lbl = jQuery('<div/>', {
                css: {
                    left: (this._component.x+xLbl)+'px',
                    top: (this._component.y +yLbl)+'px',
                    height: height+'px',
                    width: lblWidth +'px',
                    'text-align': textAnchor,
                    color: this.settings.labelColor
                },

                text: this.settings.label
            }).addClass('valueLabel');

            this._paper.container().append(lbl);
        } else {
            // only the value is displayed, take the whole component
            height = this.settings.height;
            valWidth = this.settings.width;
            xValue = 0;
            yValue = 0;
            
        }

        

        // Value
        var value = jQuery('<div/>', {
            id: this._component.id,
            css: {
                left: (this._component.x+xValue)+'px',
                top: (this._component.y +yValue)+'px',
                height: height+'px',
                width: valWidth +'px',
                'text-align': textAnchor,
                color: this.settings.defaultColor
            },
            text: ''
        }).addClass('valueLabel');
        
        if (!!this.settings.cssClassValue) {
            value.addClass(this.settings.cssClassValue);
        }

        this._paper.container().append(value);

        
    };
     
    return ctor;
}());