var nx = nx || {};
nx.unit = nx.unit || {};

nx.unit.TimePicker = (function() {
    var ctor = function(element, time) {
        this.__el = $(element);
        this.__input = this.__el.find('.inputDiv');
        this.__left = this.__el.find('.leftButton');
        this.__now = this.__el.find('.nowButton');
        this.__right = this.__el.find('.rightButton');
        this.__timeout = null;
        this.__dateInput;
        this.__hourInput;
        this.__minInput;
        this.__secInput;
        this.__isLive = true;
        this.__date = new Date(time);
        this.__init(this.__date);
        this.__customAmount = null;

    };

    ctor.prototype.setTime = function(time) {
        if (typeof time == 'number') {
            time = new Date(time);
        }

        if (!this.isValidDate(time)) {
            time = new Date();
        }

        var dateSelected = this.__dateInput.datepicker( "widget" ).is(":visible");
        var hourSelected = this.__hourInput.is(":focus");
        var minSelected = this.__minInput.is(":focus");
        var secSelected = this.__secInput.is(":focus");

        // Change the date only when no fields are selected
        if (!dateSelected && !hourSelected
                && !minSelected && !secSelected) {
            this.__date = time;
            this.__dateInput.datepicker('setDate', time);
            this.__setHours(time.getHours());
            this.__setMinutes(time.getMinutes());
            this.__setSeconds(time.getSeconds());
        }

    };

    /** Returns the timecontroller as a timestamp (milliseconds since epoch) */
    ctor.prototype.getTime = function() {
        if (!this.isValidDate(this.__date)) {
            this.__date = new Date();
        }
        return this.__date.getTime();
    };

    /**
     * if the amount of time function need to be changed.
     * The function will be called with one argument, the number of time of this function have been called.
     * the function is called each 100 ms when the button is hold.
     * If you don't want to use a custom amount of time pass null as argument.
     * @fn the multiplier method. should return the number of milliseconds to add or remove.
     */
    ctor.prototype.setCustomAmount = function(fn) {
        this.__customAmount = fn;
    };

    /**
     * Calculate the amount of time to add or remove for the button handler.
     * return a number of milliseconds
     */
    ctor.prototype.__calculateAmount = function(increment) {
        var mult = increment/10;

        if(mult < 1) {
            mult = 1;
        }

        return (5000 * mult);
    };

    /**
     * Add a button handler.
     * @el the jquery element for the button.
     * @fun function which increment or decrement the timepicker.
     *
     */
    ctor.prototype.__addButtonHandler = function(el, fun) {
        var me = this;

        el.mouseenter(function() {
            over = true;
        });

        el.mousedown(function() {
            down = true;
            $(this).addClass('ui-state-active');

            me._increment = 0;
            var incrFn = function() {
                me._increment++;

                var ms = 5000;
                if (me.__customAmount != null) {
                    ms = me.__customAmount(me._increment);
                } else {
                    ms = me.__calculateAmount(me._increment);
                }


                fun(me, ms);
            }

            incrFn();

            if (me.__timer != null) {
                window.clearInterval(me.__timer);
                me.__timer = null;
            }

            me.__timer = window.setInterval(incrFn, 100);
        });

        el.mouseleave('mouseleave', function() {
            $(this).removeClass('ui-state-active');

            if (me.__timer != null) {
                window.clearInterval(me.__timer);
                me.__timer = null;
                me.__timeChanged();
            }
        });

        el.mouseup(function() {
            $(this).removeClass('ui-state-active');

            if (me.__timer != null) {
                window.clearInterval(me.__timer);
                me.__timer = null;
                me.__timeChanged();
            }
        });
    };

    /**
     * Fires the time-change event. This event is only fired when the user
     * changes the time. It is not fired when the time is changed
     * programmatically i.e. when in "play" mode.
     */
    ctor.prototype.__timeChanged = function() {
        var me = this;

        if (this.__timeout !== null) {
            window.clearTimeout(this.__timeout);
        }

        nx.hub.timeChanged.publish(me.__date.getTime());
        id = null;

    };

    ctor.prototype.__addTimeHandlers = function(maxValue, fun, el, nextEl) {
        var me = this;

        el.click(function() {
            el.val('');
        });

        el.keypress(function(evt) {
            if (evt.charCode < 48 || evt.charCode > 57) {
                evt.preventDefault();
            }

            setTimeout(function() {
                return function() {
                    var val = el.val();
                    var pad = parseInt(val + "0", 10);

                    var valid = parseInt(val) <= maxValue;

                    if (!valid && val.length > 0) {
                        // the number is too big, remove the last figure
                        el.val(val.substring(0, val.length-1));
                    } else if (pad > maxValue) {
                        // Has input box reached maximum value?
                        pad = val.length < 2 ? "0" + val : val;
                        el.val(pad);

                        if (!!nextEl) {
                            nextEl.focus();
                            nextEl.select();
                        } else {
                            el.blur();
                        }
                    } else if (val.length == 2) {
                        // 2 figures have been entered, go to next element
                        if (!!nextEl) {
                            nextEl.focus();
                            nextEl.select();
                        } else {
                            el.blur();
                        }
                    }
                };
            }(el, evt), 0);
        });

        el.blur(function() {
            el.val(me.__padWithLeading0(el.val()));
            fun.apply(me, [parseInt(el.val(), 10)]);
            me.__timeChanged();
        });
    };

    ctor.prototype.__padWithLeading0 = function(val) {
        var pad = "00" + val;
        pad = pad.slice(pad.length - 2);
        return pad;
    };

    ctor.prototype.__init = function(date) {
        var me = this;

        this.__left
            .addClass('ui-widget-header')
            .addClass('ui-corner-left');

        this.__now
            .addClass('ui-widget-header');

        this.__right
            .addClass('ui-widget-header')
            .addClass('ui-corner-right');

        this.__input
            .addClass('ui-widget-content');

        this.__input.append($('<input>')
                .addClass('ui-widget')
                .attr('type', 'text')
                .attr('class', 'date')
                .attr('readyonly', 'readonly'));

        this.__input.append($('<input>')
                .attr('type', 'text')
                .attr('class', 'hour')
                .attr('maxLength', '2')
                .attr('title', 'Hours'));

        this.__input.append($('<input>')
                .attr('type', 'text')
                .attr('class', 'minute')
                .attr('maxLength', '2')
                .attr('title', 'Minutes'));

        this.__input.append($('<input>')
                .attr('type', 'text')
                .attr('class', 'second')
                .attr('maxLength', '2')
                .attr('title', 'Seconds'));

        this.__dateInput = this.__el.find('.date');
        $.datepicker.setDefaults($.datepicker.regional[$.i18n().locale]);
        this.__dateInput.datepicker({
            'dateFormat': 'dd/mm/yy',
            'onSelect': function(a,b,c) {
                var parts = a.split('/');

                me.__date.setDate(1); // temp date = 1 to avoid conflicts with month length
                me.__date.setMonth(parseInt(parts[1], 10) - 1);
                me.__date.setYear(parseInt(parts[2], 10));
                me.__date.setDate(parseInt(parts[0], 10)); // reset date after month/year

                me.__timeChanged();
            },
            'onClose': function(dateText, obj) {
                var parts = dateText.split('/');

                me.__date.setDate(1); // temp date = 1 to avoid conflicts with month length
                me.__date.setMonth(parseInt(parts[1], 10) - 1);
                me.__date.setYear(parseInt(parts[2], 10));
                me.__date.setDate(parseInt(parts[0], 10)); // reset date after month/year

                me.__timeChanged();
            }
        });
        //this.__dateInput.datepicker('option', 'dateFormat', 'dd/mm/yy');

        this.__hourInput = this.__el.find('.hour');
        this.__minInput = this.__el.find('.minute');
        this.__secInput = this.__el.find('.second');

        this.__addTimeHandlers(23, me.__setHours, me.__hourInput, me.__minInput);
        this.__addTimeHandlers(59, me.__setMinutes, me.__minInput, me.__secInput);
        this.__addTimeHandlers(59, me.__setSeconds, me.__secInput);

        this.__addButtonHandler(this.__left, me.__backward, this._multiplierFn);
        this.__addButtonHandler(this.__right, me.__forward, this._multiplierFn);

        // live by default.
        this.setLive();

        this.__now.click(function() {
            if (me.__isLive) {
                me.setNotLive();
                nx.hub.stopLive.publish();
            } else {
                me.setLive();
                nx.hub.startLive.publish();
            }
        });

        me.setTime(date);
    };

    ctor.prototype.setLive = function() {
        this.__now.addClass('ui-state-active');
        this.__isLive = true;
    };

    ctor.prototype.setNotLive = function() {
        this.__now.removeClass('ui-state-active');
        this.__isLive = false;
    };

    ctor.prototype.__setDate = function(year, month, day) {
        this.__date.setYear(year);
        this.__date.setMonth(month);
        this.__date.setDay(day);
    };

    ctor.prototype.__setHours = function(hours) {
        var value = this.__padWithLeading0(hours);
        this.__hourInput.val(value);
        this.__date.setHours(parseInt(value, 10));
    };

    ctor.prototype.isLive = function() {
        return this.__isLive;
    };

    ctor.prototype.__setMinutes = function(minutes) {
        var value = this.__padWithLeading0(minutes);
        this.__minInput.val(value);
        this.__date.setMinutes(parseInt(value, 10));
    };

    ctor.prototype.__setSeconds = function(seconds) {
        var value = this.__padWithLeading0(seconds);
        this.__secInput.val(value);
        this.__date.setSeconds(parseInt(value, 10));
    };

    ctor.prototype.__forward = function(me, amountMs) {
        var newTime = new Date(me.__date.getTime() + amountMs);
        me.setTime(newTime);
    };

    ctor.prototype.__backward = function(me, amountMs) {
        var newTime = new Date(me.__date.getTime() - amountMs);
        me.setTime(newTime);
    };

    ctor.prototype.isValidDate = function(d) {
        if ( Object.prototype.toString.call(d) !== "[object Date]" )
            return false;
          return !isNaN(d.getTime());
    };

    return ctor;
})();

(function($) {
    $.fn.timepicker = function() {
        return this.each(function() {
            var element = $(this);

            if (element.data('obj')) {
                return;
            }

            var unitSelector = new nx.unit.TimePicker(this, new Date());
            element.data('obj', unitSelector);
        });
    };
})(jQuery);