var nx = nx || {};
nx.map = nx.map || {};
nx.map.marker = nx.map.marker || {};

/**
 * Javascript class for a marker. 
 * Should be override for new marker type.
 */
nx.map.marker.Marker = (function() {
    
    var ctor = function ctor(data, legend) {
        this._data = data;
        this._legend = legend;
    };
    
    /**
     *  Return a Microsoft.Maps.Pushpin for this marker.
     *  Used to add the marker to the map
     */
    ctor.prototype.getPushPin = function() {};
    
    /**
     * return a Microsoft.Maps.Infobox for this marker.
     * Used to create the marker popup.
     */
    ctor.prototype.getPopup = function() {};
    
    /**
     * Initialise the marker behavior.
     * Could be override 
     * The default behavior is to show the popup on pushpin hover.
     */
    ctor.prototype.initMarker = function(pushpin, popup) {
    	if (!!pushpin &&!!popup) {
    		Microsoft.Maps.Events.addThrottledHandler(pushpin, 'mouseover',function (mouseEvent) {
    			popup.setOptions({'visible': true}, 250);
    		});

    		Microsoft.Maps.Events.addThrottledHandler(pushpin, 'mouseout',function (mouseEvent) {
    			popup.setOptions({'visible': false}, 250);
    		});	
    	}
    };
    
    return ctor;
})();

/**
 * Provide a list of markers, also the map legend if this type have one.
 * Should be override for new marker type
 */
nx.map.marker.MarkerProvider = (function() {
    
    var ctor = function ctor(providersDesc) {
    	this._enable = false;
        if(!!providersDesc) {
        	// Copy fields
        	this._enable = providersDesc.enabledByDefault;
        	this._type = providersDesc.markerType;
        	this._settingLabel = providersDesc.settingLabel;
            this._legend = providersDesc.legend;
            this._redrawOnViewChange = providersDesc.redrawOnViewChange;
            this._enabledByDefault = providersDesc.enabledByDefault;
            this._zIndex = providersDesc.zIndex;
            this._config = providersDesc.config;
            this._datasource = providersDesc.datasource;
        }
        
    };
    
    /** 
     * Retrieve markers for this position, zoom and time.
     * Should pass in parameter an array of marker to the callback method.
     * @param targetBounds a Microsoft.Maps.LocationRect object
     * @param zoom level of zoom
     * @param time time when we request these informations
     * @param method to call back with the list of marker in parameter
     */ 
    ctor.prototype.getMarkers = function(targetBounds, zoom, time, callback) {};
    
    /** Return the div for the legend of this kind of marker */
    ctor.prototype.getLegend = function(){};
    
    /** If the legend need some javascript, do it here
     * @param map the 
     */
    ctor.prototype.initLegend = function(map){};
    
    /** Update an already existing legend.
     * Used when two different markers share the same legend */
    ctor.prototype.updateLegend = function(){};
    
    /** Enable or disable this provider */
    ctor.prototype.setEnable = function(enable) {
        this._enable = enable;
    };
    
    return ctor;
})();

/** 
 * Should NOT be override.
 * Register the different marker providers depending on marker type.
 * 
 */
nx.map.marker.MarkerProviderRegister = (function() {
    var ctor = function() {
        this.__providerClass = new Object();
    };
    
    /**
     * Register a new marker provider.
     */
    ctor.prototype.register = function(providersDesc) {
    	
    	var type = providersDesc.markerType;
    	
    	//Check the user configuration 
    	if (!!localStorage.mapFilters && typeof JSON.parse(localStorage.mapFilters)[type] != 'undefined')  {
    		providersDesc.enabledByDefault = JSON.parse(localStorage.mapFilters)[type];
    	}
    	
        markerProvider = eval(providersDesc.jsObject);
        this.__providerClass[type] = new markerProvider(providersDesc);
    };
    
    /**
     * Return all enabled providers.
     */
    ctor.prototype.getEnabledMarkerProvider = function() {
        var result = new Array();
        for(var type in this.__providerClass) {
            var current = this.__providerClass[type];
            if (current._enable) {
                result.push(current);
            }
        }
        return result;
    };
    
    
    /**
     * Return all disabled providers.
     */
    ctor.prototype.getDisabledMarkerProvider = function() {
        var result = new Array();
        for(var type in this.__providerClass) {
            var current = this.__providerClass[type];
            if (!current._enable) {
                result.push(current);
            }
        }
        return result;
    };
    
    /**
     * Return an array of marker provider that should be redraw when the view change.
     * That's all enabled marker providers which have redrawOnViewChange property true
     */
    ctor.prototype.getMarkerProvidersToRedraw = function() {
        var result = new Array();
        for(var type in this.__providerClass) {
            var current = this.__providerClass[type];
            if (current._enable && current._redrawOnViewChange) {
                result.push(current);
            }
        }    
        return result;
    };
    
    
    /**
     * Return all providers.
     */
    ctor.prototype.getAllMarkerProvider = function() {
        var result = new Array();
        for(var type in this.__providerClass) {
            var current = this.__providerClass[type];
            result.push(current);
        }    
        return result;
    };
    
    /**
     * Return the provider with the give type.
     */
    ctor.prototype.getMarkerProvider = function(type) {
        return this.__providerClass[type];
    };
    
    /**
     * Return all providers with the datasource passed in parameter.
     * providers without datasource are not returned
     */
    ctor.prototype.getAllMarkerProviderForDatasource = function(datasource) {
        var result = new Array();
        for(var type in this.__providerClass) {
            var current = this.__providerClass[type];
            if (!!current._datasource && current._datasource == datasource) {
            	result.push(current);
            }
        }    
        return result;
    };
    
    
    /** 
     * Set the enable status for the provider with this type.
     */
    ctor.prototype.enableType = function(type, enable) {
        this.__providerClass[type].setEnable(enable);
    };
    
    
    return ctor;
}());
