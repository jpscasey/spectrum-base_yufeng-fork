var nx = nx || {};
nx.unit = nx.unit || {};

/**
 * A UnitSummary tab, which only displays live data. Events published by the
 * timecontroller are ignored
 */
nx.unit.Live = (function() {
    function ctor(us) {
        nx.unit.Tab.call(this, us);
    };
    
    ctor.prototype = nx.unit.Tab.prototype;
});