var nx = nx || {};
nx.datatables = nx.datatables || {};

nx.datatables.htmlFilterRegExp = new RegExp(/<.*?>/g);
nx.datatables.timestampSplitRegExt = new RegExp(/[\/ :]/);

nx.datatables.rawValue = (function(htmlValue) {
    //return htmlValue.replace( nx.datatables.htmlFilterRegExp, "" ); 
	var value = "";
	
	htmlValue = htmlValue.toLowerCase();
	if (htmlValue.indexOf("icon'>") != -1) {
		value = htmlValue;
	}else{
		value = $(htmlValue).text();
	}

    return value;
});

jQuery.fn.dataTableExt.oSort['text-html-asc']  = function(a,b) {
    var x = nx.datatables.rawValue(a).toUpperCase();
    var y = nx.datatables.rawValue(b).toUpperCase();

    return (x < y ? -1 : (x > y ? 1 : 0));
};
jQuery.fn.dataTableExt.oSort['text-html-desc']  = function(a,b) {
    var x = nx.datatables.rawValue(a).toUpperCase();
    var y = nx.datatables.rawValue(b).toUpperCase();
    
    return (x < y ? 1 : (x > y ? -1 : 0));
};

jQuery.fn.dataTableExt.oSort['timestamp-html-asc']  = function(a,b) {
    var x = nx.datatables.rawValue(a).split(nx.datatables.timestampSplitRegExt);
    var y = nx.datatables.rawValue(b).split(nx.datatables.timestampSplitRegExt);
    
    if (x.length < 6 && y.length < 6) {
        return 0;
    } else if (x.length < 6) {
        return -1
    } else if (y.length < 6) {
        return 1
    } else {
        var xDate = new Date(x[2], x[1], x[0], x[3], x[4], x[5]);
        var yDate = new Date(y[2], y[1], y[0], y[3], y[4], y[5]);
        
        if (xDate > yDate) {
            return 1;
        } else if (xDate < yDate) {
            return -1;
        } else {
            return 0;
        }
    }
};
jQuery.fn.dataTableExt.oSort['timestamp-html-desc']  = function(a,b) {
    var x = nx.datatables.rawValue(a).split(nx.datatables.timestampSplitRegExt);
    var y = nx.datatables.rawValue(b).split(nx.datatables.timestampSplitRegExt);
    
    if (x.length < 6 && y.length < 6) {
        return 0;
    } else if (x.length < 6) {
        return 1
    } else if (y.length < 6) {
        return -1
    } else {
        var xDate = new Date(x[2], x[1], x[0], x[3], x[4], x[5]);
        var yDate = new Date(y[2], y[1], y[0], y[3], y[4], y[5]);
        
        if (xDate > yDate) {
            return -1;
        } else if (xDate < yDate) {
            return 1;
        } else {
            return 0;
        }
    }
};

jQuery.fn.dataTableExt.oSort['analogue-html-asc']  = function(a,b) {
    var x = nx.datatables.rawValue(a);
    var y = nx.datatables.rawValue(b);
    
    x = parseFloat( x );
    y = parseFloat( y );

    if (isNaN(x) && isNaN(y)) {
        return 0;
    } else {
        return ((x < y || isNaN(x)) ? -1 : ((x > y || isNaN(y)) ?  1 : 0));
    }
};
jQuery.fn.dataTableExt.oSort['analogue-html-desc']  = function(a,b) {
    var x = nx.datatables.rawValue(a);
    var y = nx.datatables.rawValue(b);

    x = parseFloat( x );
    y = parseFloat( y );

    if (isNaN(x) && isNaN(y)) {
        return 0;
    } else {
        return ((x < y || isNaN(x)) ?  1 : ((x > y || isNaN(y) ) ? -1 : 0));
    }
};

//TODO: will be an implementation? maybe based in the severity/colour
jQuery.fn.dataTableExt.oSort['digital-html-asc']  = function(a,b) {
    return 0;
};
jQuery.fn.dataTableExt.oSort['digital-html-desc']  = function(a,b) {
    return 0;
};

