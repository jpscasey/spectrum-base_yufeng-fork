var nx = nx || {};
nx.unit = nx.unit || {};

nx.unit.StaticDataplots = (function() {
    function ctor(us) {
    	nx.unit.Tab.call(this, us);
    	nx.unit.TimeAware.call(this, us);
    	var me = this;
    	
    	this.__dv = $("#staticDataplots");
        this.__scrollPosition = 0;
        this.__selectedTab = 1;
        this.__changingUnit = false;

    	this.stockTabs = new nx.unit.StockTabs(
                this.__dv.find('.vehicleTabs'),
                true);
    	
    	this.__loadingCharts = false;
    	
        nx.rest.chart.configuration(function(result){
        	me.__initPeriods(result.defaultPeriod, result.periods, result.refreshPeriod);
        	
        	me.__refreshNotLive = result.refreshNotLive;
        	
            me.__addRangeSelector();
            
            me.__reloadButton = $('<button></button>').button({
                icons : {
                    primary: 'ui-icon-refresh'
                },
                label: 'Refresh'
            }).click(function(e) {
            	me.refreshCharts();
            }).css({
            	'margin-right': '5px',
            	'margin-top': '2.5px'})
            .attr('title','Refresh Charts');
            me.__controllerDiv.append(me.__reloadButton);

            $(me.__reloadButton).hide();
            
            me.__subscribeToEvents();
        });
        
        this.__initTabs = new Object();
    	
    };
    
    ctor.prototype = new nx.unit.Tab();

    
    ctor.prototype._logging = function() {
    };
    
    
    /**
     * Return chart configurations
     */
    ctor.prototype.getConfiguration = function(fleetId, callback) {
    	var url = "rest/staticchart/chartconfigs/?";
    	
        var service = url.replace("?", fleetId);

        var params = {
                cache : false,
                success : callback
        };

        $.ajax(service, params);
    };
    
    ctor.prototype._hasTimeController = function() {
        return true;
    };
    
    ctor.prototype.__initPeriods = function(firstPeriodPosition, periodValues, refreshPeriod) {
        this.__firstPeriodPosition = firstPeriodPosition;
        this.__periodValues = periodValues;
        this.__period = periodValues[firstPeriodPosition];
        this.__refreshPeriod = refreshPeriod;
    };
    
    /**
     * Build the request to get the chart data.
     */
    ctor.prototype.__requestChart = function(stockId, period, pane, chartName, timestamp) {
    	
        var newSrc = this.RESTChart(this.__fleetId,
                stockId,
                period / 1000,
                timestamp,
                jstz.determine().name().replace('/', '-'),
                null,
                parseInt(pane.width()),
                chartName);
        
        return newSrc;
    };
    
    ctor.prototype.__prepareChart = function(stockId, chartName, timestamp, callback) {
    	
    	var url = [
    	           '/rest/staticchart/compute/',
    	           this.__fleetId, '/',
    	           stockId, '/',
    	           this.__period / 1000, '/',
    	           timestamp, '/',
    	           jstz.determine().name().replace('/', '-')+'/',
    	           chartName
    	          ];
    	
	    var service = url.join('');
	    
	    $("body").addClass("wait");
	    
	    var success = function() {
	    	$("body").removeClass("wait");
	    	callback();
	    };
	    
	    var params = {
	        cache: false,
	        success: success
	    };
	    
	    $.ajax(service, params);
    };
    
    ctor.prototype._timePickerAmountOfTime = function() {
    	
        var me = this;
        
        this.__unitsummary.getTimePicker().setCustomAmount(function() {
            return (me.__period/2);
        });
        
    };
    
    ctor.prototype._onShow = function() {
        var me = this;
        
        this._timePickerAmountOfTime();
        
        this.__stockSub = nx.hub.stockChanged.subscribe(function(evt, stockId, objEl) {
            me.__stockChanged(stockId, objEl);
            
        });
        
        this.__unitChangeSub = nx.hub.unitChanged.subscribe(function(evt, unit, objEl) {
            me.__unitChanged(unit.id, unit.fleetId);
        });
        
    };
    
    
    ctor.prototype.__stockChanged = function(stockId, objEl) {
    	var me = this;
    	
    	if(me.__init) {
    		return;
    	}
    	var timestamp = this.getUnitSummary().getTimestamp();
    	if (! me.__initTabs[stockId]) {
    		var tab = me.stockTabs.getSelectedTabEl();
    		
    		
            $(tab).empty();
            
            var chartList = me.__getChartListForTab();
            
            
            if(chartList.length > 0) {
            	
            	
            	me.__loadingCharts = true;
            	if(!me.__changingUnit) {
            	    me.__prepareChart(this.stockTabs.getSelectedStockId(), chartList[0], timestamp, function() {
            		    me.__loadingCharts = false;
            		    for (var i = 0; i < chartList.length; i++) {
                  		    me.createChart(tab, chartList[i], timestamp, false, chartList.length);
                        }
            	    });
            	}    
        	}

            if(!me.__changingUnit) {
                me.__initTabs[stockId] = true;
            }    
    	}
    };
    
    
    ctor.prototype.__unitChanged = function(unitId, fleetId) {
    	var me = this;
    	me.__changingUnit = true;
    	me.__initTabs = new Object();
    	var timestamp = this.getUnitSummary().getTimestamp();
    	me.__scrollPosition = $('#' + me.stockTabs.getSelectedStockId()).scrollTop();
    	if (!!me.stockTabs.getSelectedTabEl()) {
    	    var currentTabId = me.stockTabs.getSelectedTabEl().id;
    	    for (var i = 0; i < me.stockTabs.stockList.length; i++) {
    		    if (me.stockTabs.stockList[i].id == currentTabId) {
    			    me.__selectedTab = i;
    		    }
    	    }
    	}
    	    
    	this.getConfiguration(fleetId, function(result) {
    		me.__chartConfigurations = result.chartsIdByVehicleType;

    		nx.rest.unit.stock(unitId, fleetId, function(stockList) {
    			
    			me.__init = true;
    			me.stockTabs.setStock(stockList);
    			
    			me.__typeById = new Object();

    			// Index vehicle type by vehicle id
    			for(var i = 0; i < stockList.length; i++) {
    				var stock = stockList[i];
    				if (!!stock.type) {
    				    me.__typeById[stock.id] = stock.type
    				} else if (stock.vehicleType) {
    				    me.__typeById[stock.id] = stock.vehicleType;
    				}
    			}
    			
    			me.stockTabs.el.tabs('select', me.__selectedTab);
    			
    			var tab = me.stockTabs.getSelectedTabEl();

    			var stockId = me.stockTabs.getSelectedStockId();

    			$(tab).empty();

    			var chartList = me.__getChartListForTab();
    			
    			if(chartList.length > 0) {
    				me.__loadingCharts = true;
                	me.__prepareChart(stockId, chartList[0], timestamp, function() {
                		me.__loadingCharts = false;
                		for (var i = 0; i < chartList.length; i++) {
            				me.createChart(tab, chartList[i], timestamp, true, chartList.length);
            			}
                	});
    			}
    			
    			me.__initTabs[stockId] = true;
    			
    			me.__init = false;
    			
    			me.__changingUnit = false;
    		});

    	});
    };
    
    /**
     * Method called when the tab need to be update.
     * @param data data received
     */
    ctor.prototype._update = function(data) {
        var unitId = data.unitId;
        var me = this;
        
        if (unitId != null && data.fleetId != null) {

            if (this.__fleetId == null) {
                this.__fleetId = data.fleetId;
            }
            
            this.__fleetId = data.fleetId;
            
            if (data.unitChanged()) {
            	 me.__unitChanged(unitId, me.__fleetId);
            	
            } else {
            	if ((this.__refreshPeriod == null || this.__period < this.__refreshPeriod)
            			&& !$(this.__reloadButton).is(":visible")) {
            		me.refreshCharts();
            	}
            }
        }
    };
    
    /**
     * Return the chart list to use for the current tab.
     */
    ctor.prototype.__getChartListForTab = function() {
    	
    		var me = this;
        	var currentId = me.stockTabs.getSelectedStockId();
        	var type = me.__typeById[currentId];
        	if (!!type) {
        		return me.__chartConfigurations[type];
        		
        	}
    	
    	return [];
    };
    
    /**
     * Create a chart in the tab passed in argument.
     * @param tab the tab where the chart will be added
     * @param chartName the chart name
     */
    ctor.prototype.createChart = function(tab, chartName, timestamp, unitChanged, numberOfCharts) {
    	var me = this;

    	if ($('#' + me.stockTabs.getSelectedStockId() + ' div.staticChart').length >= numberOfCharts) {
    		return;
    	}
     	
    	var chart1 = $("<div id='chart"+chartName+"' class='staticChart' />");
    	
        var src = me.__requestChart(me.stockTabs.getSelectedStockId(), me.__period, chart1, chartName, timestamp);
    	var tab = me.stockTabs.getSelectedTabEl();
        
    	var img = $('<img>')
         .attr('alt', 'dataplot')
         .attr('src', src)
         .addClass('dataplot')
         .one("load", function() {
        	 if(unitChanged) {
     		     $('#' + me.stockTabs.getSelectedStockId()).scrollTop(me.__scrollPosition);
        	 }    
         }).each(function() {
        	 if(this.complete) $(this).load();
         });

    	chart1.append(img);
    	 
    	 $(tab).append(chart1);
    };
    
    /**
     * Refresh all charts of the current tab.
     */
    ctor.prototype.refreshCharts = function() {
    	var me = this;
    	if(me.__loadingCharts == true) {
			return;
		}
    	
    	//update charts
    	var tab = me.stockTabs.getSelectedTabEl();
    	var timestamp = this.getUnitSummary().getTimestamp();
    	if (!tab) {
    		return;
    	}
    	var chartList = me.__getChartListForTab();
    		
    	
    	if(chartList.length > 0) {
    		me.__loadingCharts = true;
    		
    		var stockId = this.stockTabs.getSelectedStockId();
    		var period = this.__period;
    		
    		me.__prepareChart(stockId, chartList[0], timestamp, function(){
    			me.__loadingCharts = false;
    			for (var i = 0; i < chartList.length; i++) {
            		me.updateChart(tab, stockId, period, chartList[i], timestamp);
                }
    			
    		});
    		
    	}
    	
    };
    
    /**
     * Update the chart passed in parameter.
     * @param tab the tab where is the chart
     * @param chartName the name of the chart
     */
    ctor.prototype.updateChart = function(tab, stockId, period, chartName, timestamp) {
    	var chart = $("#chart"+chartName);
    	var img = $("#chart"+chartName+" img :first");
        
        var src = this.__requestChart(stockId, period, chart, chartName, timestamp);

    	 img.attr('src', src);
    };
    
    
    /**
     * Build the rest request for chart data
     */
    ctor.prototype.RESTChart = function(fleetId, vehicle, interval, etime, timezone, height, width, chartName) {
    	var url = [
    	           '/rest/staticchart/',
    	           fleetId, '/',
    	           vehicle, '/',
    	           interval, '/',
    	           etime, '/',
    	           timezone, '/',
    	           chartName
    	           ];

    	if (height != null) {
    		url.push('?');
    		url.push('height=');
    		url.push(height);
    	}

    	if (!!width) {
    		if (!!height) {
    			url.push('&');
    		} else {
    			url.push('?');
    		}
    		url.push('width=');
    		url.push(width);
    	}

    	// force ie to don't use the cached image
    	if(!!height || !!width) {
    		url.push('&');
    	} else {
    		url.push('?');
    	}
    	url.push('requestDate=');
    	url.push(new Date().getTime());

    	return url.join('');
    };
    
    /** Adds a jQueryUI slider to the vehicle tabs header */
    ctor.prototype.__addRangeSelector = function() {
               var tabheader = this.__dv.find('.vehicleTabs').children('ul');
               
               this.__controllerDiv = $('<div>')
                   .css({
                       'float': 'left',
                       'height': '33px',
                       'width': '300px',
                       'margin-top': '-8px'
                   }).addClass('ui-state-default')
                   .addClass('ui-corner-all');
               
               var div = $('<div>').css({
                   'position': 'absolute',
                   'top': '-2px',
                   'left': '6px',
                   'height': '20px',
                   'width': '200px',
                   'box-sizing': 'border-box',
                   'padding-left': '10px',
                   'padding-right': '10px',
                   'padding-top': '4px',
                   'direction': 'ltr'
               });
               
               var slider = $('<div>').css({
                   'width': '100%'
               });
               
               var value = $('<div>').css({
                   'position': 'absolute',
                   'top': '9px',
                   'left': '82px',
                   'height': '20px',
                   'line-height': '16px',
                   'width': '110px',
                   'direction': 'ltr',
                   'text-align': 'left',
                   'padding-top': '2px'
               });
               
               div.append(slider);
               this.__controllerDiv.append(div);
               this.__controllerDiv.append(value);
               tabheader.append(this.__controllerDiv);
               
               var me = this;
               
               value.text(nx.util.timestampToHumanReadable(me.__periodValues[me.__firstPeriodPosition]));
               
               slider.slider({
                   min: 0,
                   max: me.__periodValues.length - 1,
                   value: me.__firstPeriodPosition,
                   slide: function(event, ui) {
                       me.__period = me.__periodValues[ui.value];
                       value.text(nx.util.timestampToHumanReadable(me.__period));
                       if (!!me.__valuesGrid) {
                           me.__valuesGrid.getScroller().scrollTop(0);
                           me.__valuesGrid.getScroller().find(".tableContainer").height((me.__period / 1000) *21);
                       }
                       
                       if (me.__period >= me.__refreshPeriod) {
                    	   $(me.__reloadButton).show();
                    	   me.getUnitSummary().setNotLive();
                    	   me.getUnitSummary().__dm.stop();
                    	   
                       } else {
                    	   if (!me.__refreshNotLive || $('.nowButton').hasClass('ui-state-active')) {
                    	       $(me.__reloadButton).hide();
                    	   }    
                       }
                   },
                   stop: function(event, ui) {
                       me.refreshCharts();
                   }
               });
           };
           
    ctor.prototype.__subscribeToEvents = function() {
    	var me = this;
    	
    	nx.hub.play.subscribe(function(e) {
    		if (me.__refreshNotLive && me.__period < me.__refreshPeriod) {
     	        $(me.__reloadButton).hide();
    		}    
    	});
    	
    	nx.hub.pause.subscribe(function(e) {
    		if (me.__refreshNotLive && me.__period < me.__refreshPeriod) {
      	        $(me.__reloadButton).show();
    		}    
     	});
    	
    	nx.hub.timeChanged.subscribe(function(e) {
    		if (me.__refreshNotLive && me.__period < me.__refreshPeriod) {
      	        $(me.__reloadButton).show();
    		}    
     	});
    	
    	nx.hub.startLive.subscribe(function(e) {
    		if (me.__refreshNotLive && me.__period < me.__refreshPeriod) {
      	        $(me.__reloadButton).hide();
    		}    
     	});
    	
    	nx.hub.stopLive.subscribe(function(e) {
    		if (me.__refreshNotLive && me.__period < me.__refreshPeriod) {
      	        $(me.__reloadButton).show();
    		}    
     	});
    };

    return ctor;
})();
