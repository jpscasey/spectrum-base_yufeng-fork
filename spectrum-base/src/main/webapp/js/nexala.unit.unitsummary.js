var nx = nx || {};
nx.unit = nx.unit || {};

nx.unit.DataManager = (function() {
    function ctor(unitsummary) {
        this.__unitsummary = unitsummary;
        this.__timer = null;
        this.__subscribeToEvents();      
        
        this.__unitId = null;
        this.__fleetId = null;
        this.__timestamp = null;
        this.__isUnitDetail = null;

        this.__refreshRate = null;
    };
    
    ctor.prototype.__subscribeToEvents = function() {
        var me = this;
        
        // Remove all unit change subscriptions, i.e.
        // from nexala.spectrum (main toolbar), unit summary
        // has its own subscription below.
        nx.hub.unitChanged.unsubscribe();
        
        nx.hub.play.subscribe(function(e) {
            me.start();
        });
        
        nx.hub.pause.subscribe(function(e) {
            me.stop();
        });
        
        nx.hub.unitChanged.subscribe(function(e, unit) {
            me.unitChanged(unit);
        });
        
        nx.hub.timeChanged.subscribe(function(e, timestamp) {
            me.timeChanged(timestamp);
        });
        
        nx.hub.startLive.subscribe(function(e) {
            me.startLive();
        });
        
        nx.hub.stopLive.subscribe(function(e) {
            me.stop();
        });
    };
    
    ctor.prototype.setLive = function() {
    	var me = this;
        var us = this.__unitsummary;
        
        if (!!me.__fleetId && !!me.__unitId) {
        	 nx.rest.unit.lastTimestamp(me.__fleetId, me.__unitId, function(response) {
         		us.setTimestamp(response);
         		us.getTimePicker().setLive();
         		me.doUpdate(me.__unitId, me.__fleetId, false, us.getTimestamp());
         	});
        } else {
        	us.setTimestamp(new Date().getTime());
     		us.getTimePicker().setLive();
        }
        
    };
    
    ctor.prototype.setNotLive = function() {
        var me = this;
        var us = this.__unitsummary;

        us.getTimePicker().setNotLive();
    };
    
    ctor.prototype.startLive = function() {
        this.setLive();
        this.start();
    };
    
    ctor.prototype.start = function() {
        if (this.__unitsummary.getTimePicker().isLive()) {
            this.setLive();
        }
        
        var us = this.__unitsummary;
        this.doUpdate(this.__unitId, this.__fleetId, false, us.getTimestamp());
        this.play();
    };
    
    ctor.prototype.play = function() {
        var me = this;
        var us = this.__unitsummary;
        
        this.__unitsummary.getToggleButton().play();

        // Stop previous timer if it exists
        if (this.__timer != null) {
            window.clearInterval(this.__timer);
        }
        
        this.__timer = window.setInterval(function() {
            var timestamp = us.getTimestamp();
            
            if(us.getTimePicker().isLive()) {
            	// Get the last available timestamp
            	var fleetId = us.getFleetId();
            	var unitId = us.getCurrentTab().getUnit();
            	nx.rest.unit.lastTimestamp(fleetId, unitId, function(response) {
            		us.setTimestamp(response);
            	});
            } else {
            	us.setTimestamp(timestamp + me.__refreshRate);
            }
            
            
            me.doUpdate(me.__unitId, me.__fleetId, false, timestamp + me.__refreshRate);
        }, me.__refreshRate);
    };
    
    ctor.prototype.stop = function() {
        this.pause();
        
        var timestamp = this.__unitsummary.getTimestamp();
        this.doUpdate(this.__unitId, this.__fleetId, false, timestamp);
    };
    
    ctor.prototype.pause = function() {
        this.__unitsummary.getToggleButton().pause();
        if (this.__timer != null) {
            window.clearInterval(this.__timer);
            this.__timer = null;
        }
    };
    
    /**
     * When the user changes the time, we flip the toggle button to
     * paused
     */
    ctor.prototype.timeChanged = function(timestamp) {
        this.pause();
        this.__unitsummary.getTimePicker().setNotLive();
        this.__unitsummary.getToggleButton().pause();
        
        this.doUpdate(this.__unitId, this.__fleetId, false, timestamp);
    };
    
    /**
     * Fired when the unit changes, when the unit is changed we keep
     * the current timeController state, i.e. PLAY, REPLAY, PAUSED.
     */
    ctor.prototype.unitChanged = function(unit) {
        var timestamp = this.__unitsummary.getTimestamp();
        var me = this;
        var us = this.__unitsummary;
        
        if(unit.userAction && this.__fleetId != unit.fleetId) {
        	// Fleet changed , rebuild page -> invoke original
            nx.main.openUnitSummary(unit);
        } else {
	        // If it's live data, retrieve the last timestamp for this unit
        	var unitId = unit.id;
        	var fleetId = unit.fleetId;
        	var sameFormation = unit.sameFormation;
        	
	        if(us.getTimePicker().isLive()) {
	        	//get the last available timestamp
	        	nx.rest.unit.lastTimestamp(fleetId, unitId, function(response) {
	        		us.setTimestamp(response);
	        		me.doUpdate(unitId, fleetId, sameFormation, response);
	        	});
	        }
	        
	        this.__unitsummary.updateFormation(unitId, fleetId);
	        if(!us.getTimePicker().isLive()) {
	            this.doUpdate(unitId, fleetId, sameFormation, timestamp);
	        }
        }
    };
    
    /** Updates the current tab */
    ctor.prototype.doUpdate = function(unitId, fleetId, sameFormation, timestamp) {
        var unitChanged = false, timeChanged = false;
        
        if (unitId != null) {
            this.__unitId = unitId;
        }
        
        if (fleetId != null) {
            this.__fleetId = fleetId;
        }
        
        if (timestamp != null) {
            this.__timestamp = timestamp;
        }
        
        var currentPluginsList = this.__unitsummary.getCurrentPlugins();
        if (!!currentPluginsList) {
            var currentPlugin = null;
            for (var i = 0, l = currentPluginsList.length; i < l; i++) {
                currentPlugin = currentPluginsList[i];
                currentPlugin._update({
                    'unitId': unitId,
                    'fleetId': fleetId,
                    'sameFormation' : sameFormation,
                    'timestamp': timestamp
                });
            }
        }

        var currentTab = this.__unitsummary.getCurrentTab();
        if (currentTab != null) {
            var timeChanged = false;
            var unitChanged = false;
            var refreshNeeded = currentTab.needUpdate();

            if (unitId != null && unitId != currentTab.getUnit()) {
                unitChanged = true;
            }
            
            if (timestamp != null && timestamp != currentTab.getTimestamp()) {
                timeChanged = true;
            }
            
            if (timeChanged || unitChanged || refreshNeeded) {
            	
                currentTab.setTimestamp(this.__timestamp);
                currentTab.setUnit(this.__unitId);
                
                var data = {
                    'unitId': unitId,
                    'fleetId': fleetId,
                    'timestamp': timestamp,
                    'unitChanged': function() {
                        return unitChanged;
                    },
                    'timeChanged': function() {
                        return timeChanged;
                    },
                    'sameFormation' : sameFormation,
                    'isLive': this.__unitsummary.getTimePicker().isLive()
                };
                
                currentTab._update(data);
                
                var currentTabPluginsList = this.__unitsummary.getCurrentTabPlugins();
                
                if (!!currentTabPluginsList) {
                    for (var i = 0, l = currentTabPluginsList.length; i < l; i++) {
                        currentPlugin = currentTabPluginsList[i];
                        currentPlugin._update(data);
                    }
                }
            }
        }
    };
    
    ctor.prototype.setRefreshRate = function(refreshRate) {
        this.__refreshRate = refreshRate;
    };
    
    return ctor;
})();

/**
 * DataNavigator allows to change the functionality of the controller,
 * so it navigates through data by time or by record
 */
nx.DataNavigator = (function() {
    function ctor(timeController, unitSummary) {
        this.__timeController = timeController;
        this.__unitSummary = unitSummary;

        this.__playPauseButton = this.__timeController.find("#playPauseButton");
        this.__incrementButton = this.__timeController.find("#timePicker .rightButton");
        this.__decrementButton = this.__timeController.find("#timePicker .leftButton");

        // By default, the navigation is done by time
        this.__defaultTimeIncrementAction = {};
        this.__defaultTimeDecrementAction = {};
        this.__getDefaultTimeIncrementDecrementHandlers();
        
        this.__isTimeSelector = true;
    };
    
    ctor.prototype.__getDefaultTimeIncrementDecrementHandlers = function() {
        this.__defaultTimeIncrementAction.mousedown = this.__getDefaultEvent(this.__incrementButton, "mousedown");
        this.__defaultTimeIncrementAction.mouseup = this.__getDefaultEvent(this.__incrementButton, "mouseup");

        this.__defaultTimeDecrementAction.mousedown = this.__getDefaultEvent(this.__decrementButton, "mousedown");
        this.__defaultTimeDecrementAction.mouseup = this.__getDefaultEvent(this.__decrementButton, "mouseup");
    };
    
    ctor.prototype.__getDefaultEvent = function(element, event) {
        var handler = $._data(element.get(0), "events")[event][0].handler;

        return handler;
    };
    
    ctor.prototype.setAsRecordSelector = function() {
        var me = this;

        this.__isTimeSelector = false;
        
        this.__playPauseButton.hide();
        this.__incrementButton.unbind();
        this.__incrementButton.find('a').text('>');
        this.__incrementButton.on({
            mousedown : function(event) {
                $(this).addClass('ui-state-active');
                me.__unitSummary.pause();
                me.__unitSummary.setNotLive();

                me.__getNextRecord();
            },
            mouseup : function(event) {
                $(this).removeClass('ui-state-active');
            },
            mouseleave : function(event) {
                $(this).removeClass('ui-state-active');
            }
        });

        this.__decrementButton.unbind();
        this.__decrementButton.find('a').text('<');
        this.__decrementButton.on({
            mousedown : function(event) {
                $(this).addClass('ui-state-active');
                me.__unitSummary.pause();
                me.__unitSummary.setNotLive();

                me.__getPreviousRecord();
            },
            mouseup : function(event) {
                $(this).removeClass('ui-state-active');
            },
            mouseleave : function(event) {
                $(this).removeClass('ui-state-active');
            }
        });
    };

    ctor.prototype.setAsTimeSelector = function() {
        
        this.__isTimeSelector = true;

        this.__playPauseButton.show();
        this.__incrementButton.unbind();
        this.__incrementButton.find('a').text('+');
        this.__incrementButton.on({
            mousedown : this.__defaultTimeIncrementAction.mousedown,
            mouseup : this.__defaultTimeIncrementAction.mouseup,
            mouseleave : this.__defaultTimeIncrementAction.mouseup
        });

        this.__decrementButton.unbind();
        this.__decrementButton.find('a').text('-');
        this.__decrementButton.on({
            mousedown : this.__defaultTimeDecrementAction.mousedown,
            mouseup : this.__defaultTimeDecrementAction.mouseup,
            mouseleave : this.__defaultTimeDecrementAction.mouseup
        });
    }

    ctor.prototype.__getNextRecord = function() {
        var data = {
            "fleetId": this.__unitSummary.getFleetId(),
            "unitId": this.__unitSummary.getCurrentTab().getUnit(),
            "timestamp": this.__unitSummary.getCurrentTab().getTimestamp(),
            "unitChanged": function() {
                return false;
            },
            "direction": 1
        };
        this.__updateScreen(data, false);
    };

    ctor.prototype.__getPreviousRecord = function() {
        var data = {
            "fleetId": this.__unitSummary.getFleetId(),
            "unitId": this.__unitSummary.getCurrentTab().getUnit(),
            "timestamp": this.__unitSummary.getCurrentTab().getTimestamp(),
            "unitChanged": function() {
                return false;
            },
            "direction": -1
        };
        this.__updateScreen(data, false);
    };
    
    ctor.prototype.__updateScreen = function(data, resetScroll) {
        var currentTabPluginsList = this.__unitSummary.getCurrentTabPlugins();
        this.__updatePlugins(currentTabPluginsList, data, resetScroll);
        
        var currentTab = this.__unitSummary.getCurrentTab();
        if (currentTab != null) {
            this.__updateTab(currentTab, data, resetScroll);
        }
    };
    
    ctor.prototype.__updateTab = function(tab, data, resetScroll) {
        tab._update(data, resetScroll);
        
        var currentTabPluginsList = this.__unitSummary.getCurrentTabPlugins();
        this.__updatePlugins(currentTabPluginsList, data, resetScroll);
    };
    
    ctor.prototype.__updatePlugins = function(plugins, data, resetScroll) {
        if (!!plugins) {
            var plugin = null;
            for (var i = 0, l = plugins.length; i < l; i++) {
                plugin = plugins[i];
                plugin._update(data, resetScroll);
            }
        }
    };
    
    ctor.prototype.isTimeMode = function() {
        return this.__isTimeSelector;
    };
    
    ctor.prototype.isRecordMode = function() {
        return !this.__isTimeSelector;
    };

    return ctor;
})();

/**
 * UnitSummary - Displays data at a Unit level. The UnitSummary consists of a
 * UnitSelector, a TimePicker and a set of tabs, each containing a different
 * view of the data.
 * 
 * The tabs are initialized lazily. All of the html for the tabs is rendered
 * server side, however the tab will not be initialized until the tabs "show"
 * event is fired. In order for this to work, each tab must be bound to a class
 * before the UnitSummary.initialize method is called.
 */
nx.unit.UnitSummary = (function() {

    function ctor() {
        var me = this;
        this.__fleetId = null;
        this.__bound = {};
        this.__boundPlugins = [];
        this.__boundTabPlugins = {};
        this.__boundDialogs = {};
        this.__currentTab = null;
        this.__currentDialog = null;
        this.__timeController = $('#timeController');
        this.__timePicker = $('#timePicker').timepicker();
        this.__unitSelector = $('#unitSelector').unitSelector();
        this.__toggleButton = $('#playPauseButton').togglebutton(false);
        this.__refreshRate = null;
        
        this.__unitFormationSelector = $('#unitFormationSelector');
        
        this.__dm = new nx.unit.DataManager(this);
        this.__dn = new nx.DataNavigator(this.__timeController, this);
    };

    ctor.prototype.initialize = function(unitDesc, initialTab, timestamp, faultId) {
        var me = this;
        this.timestamp = timestamp;
        this.__faultId = faultId;
        var tabs = $('#unitTabs');
        var numTabs = $('#unitTabs ul li').length;

        this.__initializePlugins();
        
        if (!!unitDesc) {
            this.__fleetId = unitDesc.fleetId;
        }
        
        tabs.tabs({
            'show' : function(event, ui) {
                me.show(ui);
            }
        });
        
        if (initialTab != null) {
            tabs.tabs('select', initialTab);
        }

        var globalRefreshTimeDfd = $.Deferred()
        var globalRefreshTimeLoaded = globalRefreshTimeDfd.promise();
        nx.rest.system.globalRefreshRate(function(refreshRate) {
            me.__refreshRate = refreshRate;
            me.__dm.setRefreshRate(refreshRate)
            globalRefreshTimeDfd.resolve();
        });
        
        $.when(globalRefreshTimeLoaded).done(function() {
            if (!!timestamp) {
                me.setTimestamp(timestamp);
                me.setNotLive();
                me.__toggleButton = $('#playPauseButton').togglebutton(false);
                me.getTimePicker().__timeChanged();
            } else {
                me.__toggleButton = $('#playPauseButton').togglebutton(true);
                me.__dm.startLive();
            }
            
            if (!!unitDesc) {
                nx.main.setUnit(unitDesc);
            }
        });
        
    };
    
    ctor.prototype.__initializePlugins = function() {
        for (var i = 0, l = this.__boundPlugins.length; i < l; i++) {
            var plugin = this.__boundPlugins[i];
            
            if (plugin.object === null) {
                plugin.object = new plugin.clazz(this);
            }
        }
    };
    
    ctor.prototype.show = function(ui) {
        var key = ui.tab.hash;
        var binding = this.__bound[key];
        var boundTabPlugins = this.__boundTabPlugins[key];
        
        if (key == '#us_tab1') {
            this.__isUnitDetail = true;
        } else {
            this.__isUnitDetail = false;
        }

        if (!!binding) {
            if (binding.object === null) {
                binding.object = new binding.clazz(this);
            }
            
            var tabPlugins = [];
            
            if (this.__currentTab != null) {
                this.__currentTab._onHide();
            }
            
            this.__currentTab = binding.object;

            binding.object._logging();
            
            if (!!binding.object._hasTimeController()) {
                this.__timeController.show();
            } else {
                this.__timeController.hide();
            }
            
            if (boundTabPlugins !== undefined) {
                for (var i = 0, l = boundTabPlugins.length; i < l; i++) {
                    var tabPlugin = boundTabPlugins[i];
                    
                    if (tabPlugin.object === null) {
                        tabPlugin.object = new tabPlugin.clazz(this);
                        tabPlugins.push(tabPlugin.object);
                    }
                }
            }
            this.__currentTabPlugins = $.map(boundTabPlugins || [], function(tabPlugin) { return tabPlugin.object; });

            if (binding.dataNavigationType == 'TIME') {
                this.__dn.setAsTimeSelector();
            }
            else if (binding.dataNavigationType == 'RECORD') {
                this.__dn.setAsRecordSelector();
            }
            binding.object._onShow();
            
            if (this.__isUnitDetail) {
            	if (typeof this.__dm.play === "function") {
            		this.__dm.play();                    
            	}
            	this.__dm.doUpdate(
                    this.__dm.__unitId, // FIXME
                    this.__dm.__fleetId,
                    false,
                    new Date().getTime());
            } else {
                this.__dm.doUpdate(
                    this.__dm.__unitId, // FIXME
                    this.__dm.__fleetId,
                    false,
                    this.getTimestamp());
            }
        } else {
            this.__currentTab = null;
        }
    };

    ctor.prototype.showDialog = function(key, parameters) {
        var binding = this.__boundDialogs[key];
        if (!!binding) {
            if (binding.object === null) {
                binding.object = new binding.clazz(this, parameters);
            }
            
            this.__currentDialog = binding.object;

            binding.object._logging();
            $(key).dialog('open');
            binding.object.doUpdate(parameters.fleetId, parameters.id, false, binding.object.getTimestamp());
            $(key).find(":focus").blur();
        }
        
        return this.__currentDialog;
    };
    
    ctor.prototype.updateFormation = function(unitId, fleetId) {
        if (!unitId || !fleetId) {
            return;
        }
        
        var me = this;
        
        this.__unitFormationSelector.html("");
        
        nx.rest.unit.formationUnits(unitId, fleetId, function(response) {
            for (var i = 0, l = response.length; i < l; i++) {
                var unit = response[i];
                
                if (unit != null) {
                    $('<a>')
                        .data('unitId', unit.id)
                        .text(unit.unitNumber)
                        .addClass('formationUnit')
                        .addClass(unit.id == unitId ? 'selectedFormationUnit' : '')
                        .click(
                            function (event) { 
                                event.preventDefault();
                                nx.hub.unitChanged.publish({id: $(this).data('unitId'), fleetId: fleetId});
                            }
                        )
                        .appendTo(me.__unitFormationSelector);
                }
            }
        });
    };
    
    ctor.prototype.getTimestamp = function() {
        return this.__timePicker.data('obj').getTime();
    };
    
    ctor.prototype.setTimestamp = function(timestamp) {
        this.__timePicker.data('obj').setTime(timestamp);
    };
    
    ctor.prototype.getToggleButton = function() {
        return this.__toggleButton.data('obj');
    };
    
    ctor.prototype.getUnitSelector = function() {
        return this.__unitSelector.data('obj');
    };
    
    ctor.prototype.getTimePicker = function() {
        return this.__timePicker.data('obj');
    };
    
    ctor.prototype.getCurrentTab = function() {
        return this.__currentTab;
    };
    
    ctor.prototype.getRefreshRate = function() {
        return this.__refreshRate;
    };
    
    ctor.prototype.getCurrentDialog = function() {
        return this.__currentDialog;
    };
    
    ctor.prototype.setCurrentDialog = function(currentDialog) {
        this.__currentDialog = currentDialog;
    };
    
    ctor.prototype.getCurrentPlugins = function() {
        return $.map(this.__boundPlugins || [], function(plugin) { return plugin.object; });
    };
    
    ctor.prototype.getCurrentTabPlugins = function() {
        return this.__currentTabPlugins;
    };
    
    ctor.prototype.bind = function(key, clazz, dataNavigationType) {
        this.__bound[key] = {'clazz': clazz, 'object': null, 'dataNavigationType': dataNavigationType};
    };
    
    ctor.prototype.bindPlugin = function(clazz) {
        this.__boundPlugins.push({'clazz': clazz, 'object': null});
    }
    
    ctor.prototype.bindTabPlugin = function(key, clazz) {
        if (this.__boundTabPlugins[key] === undefined) {
            this.__boundTabPlugins[key] = [{'clazz': clazz, 'object': null}];
        } else {
            this.__boundTabPlugins[key].push({'clazz': clazz, 'object': null});
        }
    };
    
    ctor.prototype.bindDialog = function(key, clazz) {
        this.__boundDialogs[key] = {'clazz': clazz, 'object': null};
    };
    
    ctor.prototype.getFleetId = function() {
        return this.__fleetId;
    };

    ctor.prototype.setNotLive = function() {
        this.__dm.setNotLive();
    };
    
    ctor.prototype.pause = function() {
        this.__dm.pause();
    };

    return ctor;
})();
