var nx = nx || {};
nx.unit = nx.unit || {};

nx.unit.Cab = (function() {
    function ctor(us) {
        nx.unit.TimeAware.call(this, us);
        this.__cab = $('#cabMain');
        this.__prevTimestamp = 0;
        this.__uiInitialised = false;
        // Is true if the layout is loading (prevent to load it multiple time)
        this.__uiLayoutLoading = false;
        this.__components = {};
        this.__stockSub = null;
        this.__stockTabs = new nx.unit.StockTabs(
                this.__cab.find('.vehicleTabs'),
                true,
                false);
        
        this._isOnlyOneConfig = false;
        this.__fleetId = null;
        this.__unitId = null;
    };
    
    /** Extend nx.unit.TimeAware */
    ctor.prototype = new nx.unit.TimeAware();
    
    /** Extend nx.unit.Tab */
    ctor.prototype = new nx.unit.Tab();

    ctor.prototype._onHide = function() {
        if (this.__stockSub != null) {
            this.__stockSub.unsubscribe();
            this.__stockSub = null;
        }
        
        if (this.__unitChangeSub != null) {
            this.__unitChangeSub.unsubscribe();
            this.__unitChangeSub = null;
        }
    };
    
    ctor.prototype._onShow = function() {
        var me = this;
        
        this.__stockSub = nx.hub.stockChanged.subscribe(function(evt, stockId, objEl) {
            if(me._isOnlyOneConfig == false) {
                me.__clearCab();
                me.__initUiAsync();
            }
            me.__stockChanged(stockId, objEl);
            
        });
        
        this.__unitChangeSub = nx.hub.unitChanged.subscribe(function(evt, unit, objEl) {
            me.__unitChanged(unit.id, unit.fleetId);
        });
        
    };
    

    
    ctor.prototype.__initUiAsync = function() {
        var me = this;
        
        if ((me._isOnlyOneConfig) && (this.__uiInitialised)) {
            return;
        }
        
        if(!this.__uiLayoutLoading && !!this.__fleetId) {

            me.__uiLayoutLoading = true;

            var stockId = this.__stockTabs.getSelectedStockId();
            nx.rest.cab.layout(this.__fleetId, stockId, function(cabConfig) {

                me._isOnlyOneConfig = cabConfig.isOnlyOneConfig;
                me.__clearCab();

                // Resize elements
                var cabMain = $('#cabMain');
                var absoluteCab = $('#absoluteCab');
                var cabBorder = $('#cabBorder');

                cabMain.height(cabConfig.height+60);
                cabBorder.width(cabConfig.width);

                if((cabConfig.width)+20 > $(window).width()) {
                    cabMain.width(cabConfig.width+20);
                    absoluteCab.width(cabConfig.width);
                } else {
                    cabMain.width('100%');
                    absoluteCab.width('auto');
                }


                absoluteCab.height(cabConfig.height+20);


                var cc = $('#cabContainer');

                cc.width(cabConfig.width);
                cc.height(cabConfig.height);

                me._isOnlyOneConfig = cabConfig.onlyOneCabConfig;


                Raphael.fn.container = function () {
                    return cc;
                };

                me._paper = Raphael(cc[0], cabConfig.width, cabConfig.height);

                var components = cabConfig.layout;
                var panels = "";
                var labels = "";
                for (var i = 0, l = components.length; i < l; i++) {
                    var component = components[i];
                    var settings = component.settingsObject;
                    var clazz = component.widgetClassName;
                    var className = component.className;
                    var domId = component.id;

                    if (clazz == "label" ) {

                        labels += "<div class = 'cabLabel "+className+"' style = 'left: "+component.x+"px; top: "+component.y
                        +"px; height: "+component.height+"px; width: "+component.width+"px'>"+component.text+"</div>";

                    } else if(clazz == "panel") {

                        panels += "<div id='"+domId+"' class= 'component "+className+"' style = 'left: "+component.x+"px; top: "+
                        component.y+"px; height: "+component.height+"px; width: "+component.width+"px'></div>";

                    } else {

                        if (clazz != null) {

                            clazz = eval(clazz);
                            var obj = new clazz(me._paper, component, settings);

                            for (var j = 0; j < component.channels.length; j++) {
                                var key = component.channels[j];
                                var array = me.__components[key];
                                if (array == null) {
                                    array = new Array();
                                }
                                array[array.length] = obj; 
                                me.__components[key] = array;
                            }

                        }

                    }

                }

                cc.prepend(panels);
                cc.append(labels);

                // on finish layout... Maybe we've received some data while
                // we were drawing the layout..

                me.__uiInitialised = true;
                me.__uiLayoutLoading = false;
                me.__refreshCab();
            });

        }
    };
    
    ctor.prototype._update = function(data) {
        var unitId = data.unitId;
        this.__fleetId = data.fleetId;
        var timestamp = data.timestamp;
        this.__unitId = unitId;
        
        if (unitId != null) {
            if (data.unitChanged()) {
                this.__unitChanged(data.unitId, data.fleetId);
            } else {
                this.__refreshCab();
            }
        }
    };
    
    ctor.prototype.__unitChanged = function(unitId, fleetId) {
        var me = this;
        if(me._isOnlyOneConfig == false) {
            me.__clearCab();
        }
        nx.rest.cab.stock(fleetId, unitId, function(stockList) {
            me.__stockTabs.setStock(stockList);
            $('#absoluteCab').show();
            me.__initUiAsync();
        });
    };
    
    ctor.prototype.__stockChanged = function(stockId, objEl) {
        this.__prevTimestamp = 0;
        
        this.__resetCab();
        this.__refreshCab();
    };
    
    ctor.prototype.__resetCab = function() {
        for (var key in this.__components) {
            var component = this.__components[key];
            for(var i = 0, l = component.length; i < l; i++) {
                component[i]._reset();
            }
        }
    };
    
    ctor.prototype.__clearCab = function() {
        
        $('#cabContainer').empty();
        
        this.__uiInitialised = false;
        this.__components ={};
    };
    
    
    ctor.prototype.__refreshCab = function() {
        if (!this.__uiInitialised) {
            return;
        }
        
        var me = this;
        var stockId = this.__stockTabs.getSelectedStockId();
        
        var timestamp = this.getUnitSummary().getTimestamp();
        
        if (stockId) {
            nx.rest.cab.data(me.__fleetId, me.__unitId, stockId, me.__prevTimestamp, timestamp, function(data) {
                var channels = data.channels;
                me.__prevTimestamp = data.timestamp;
                
                for (var key in channels) {
                    var channel = channels[key];
                    var widgetArray = me.__components[key];
                    
                    if (widgetArray  != null) {
                        
                        for (var i = 0; i < widgetArray.length; i++) {
                            var widget = widgetArray[i];
                            if (widget != null) {
                                if (!!widget._update) {
                                    widget._update(key, channel[0], channel[1]);
                                }
                            }
                        }   
                    }
                }
            });
        }
    };
    
    ctor.prototype._logging = function() {
        //logging unit summary - data plots access
        nx.rest.logging.unitCabView();
    };
    
    ctor.prototype._hasTimeController = function() {
        return true;
    };

    return ctor;
}());