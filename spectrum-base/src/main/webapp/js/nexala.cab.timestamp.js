var nx = nx || {};
nx.cab = nx.cab || {};

nx.cab.TimeStamp = (function () {

    var __radiusRoundedCorner = 5;
    var __borderSize = 2;
    var __txtDateTime;

    var ctor = function (el, component, settings) {
        nx.cab.Widget.call(this);
        this._component = component;
        this.settings = $.extend({
            width: component.width,
            height: component.height
        }, settings);
        
        this._paper = el;
        this._draw();
    };
    
    ctor.prototype = new nx.cab.Widget();
    
    ctor.prototype._update = function(channelName, channelValue, channelCategory) {
        if (!!channelValue) {
            var timestamp = $.format('{date}', channelValue);
            
            this.__txtDateTime.attr({
                text : timestamp});
        }
    };
    
  
    
    ctor.prototype._reset = function() {
        this.__txtDateTime.attr({
            text : ''});
    };
    
    ctor.prototype._draw = function() {
        var xOffset = this._component.x;
        var yOffset = this._component.y;
        
        // Draw timestamp
        this._paper.rect(xOffset,yOffset,
                this.settings.width,
                this.settings.height,
                __radiusRoundedCorner
            ).attr({
                fill: '90-#222-#333-#444',
                stroke: 'none'});
        
        // Draw border
        this._paper.rect(xOffset+__borderSize, yOffset+__borderSize,
                this.settings.width - (__borderSize*2) ,
                this.settings.height - (__borderSize*2),
                __radiusRoundedCorner).attr({
                    stroke: '#555555'});
        
        this.__txtDateTime = this._paper.text(xOffset+this.settings.width/2, yOffset+this.settings.height/2, '')
        .attr({'fill': '#ddd',
               'font-weight': 'bold'});

    };
     
    return ctor;
}());