var nx = nx || {};
nx.raphael = nx.raphael || {};

nx.raphael.verticalText = (function(jQueryHeaderElement) {
    var element = jQueryHeaderElement;
    var parentElement = element.parent();
    var id = element.attr('id');
    var text = jQueryHeaderElement.text();
    var height = parentElement.height();
    var width = 20;
    var fontFamily = element.css('font-family');
    var fontSize = element.css('font-size');
    
    element.html('');
    
    var paper = Raphael(id, width, height);
    var text = paper.text(5, height/2, text);
    text.attr({ 'font-family': fontFamily, 'font-size': fontSize, 'text-anchor': 'end'});
    text.rotate(270);
});

nx.raphael.eventAlert = (function(jQueryElement) {
    var element = jQueryElement;
    
    var id = element.attr('id');
    var width = element.width();
    var height = element.height();
    
    var paper = Raphael(id, width, height);
    
    var c1 = paper.circle(24, 24, 0).attr({'stroke': '#AA0000', 'stroke-width': '3px'});
    var c2 = paper.circle(24, 24, 0).attr({'stroke': '#AA0000', 'stroke-width': '3px'});
    var c3 = paper.circle(24, 24, 0).attr({'stroke': '#AA0000', 'stroke-width': '3px'});

    c1.animate({r: 24}, 1200);

    setTimeout(function() {
        c1.animate({'opacity': 0}, 1050);
        c2.animate({r: 16}, 1050);
    }, 150);

    setTimeout(function() {
        c2.animate({'opacity': 0}, 950);
        c3.animate({r: 8, opacity: 0}, 950);
    }, 250);
});
