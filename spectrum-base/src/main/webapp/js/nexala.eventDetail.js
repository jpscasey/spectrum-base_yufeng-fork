var nx = nx || {};
nx.event = nx.event || {};

nx.event.EventDetail = (function() {
    function ctor() {
        var me = this;

        this.__configDfd = $.Deferred();
        this.__configLoaded = this.__configDfd.promise();

        this.__rendererDfd = null;
        this.__rendererLoaded = null;

        this.__eventDfd = null;
        this.__eventLoaded = null;
        this.__cssLoaded = false;

        /** A map from the event field to the label which represents it */
        this.__eventDetailLabels = {};

        /** A map from the column name to the column object */
        this.__columnsMap = {};

        /** The selected event object */
        this.__event = null;
        this.__fleetId = null;
        this.__eventIdList = [];
        this.__eventIdListBookmark = null;
        
        this.__configuration = null;

        this.__eventDetailUpdater = null;

        this.__eventDetailRenderer = null;

        this.__eventDetail = $('#eventDetail');
        this.__toolbar;
        
        this.__eventsAreLinkable = false;
        this.__eventLink = '';
        this._removeActionItem = false;
        
        this.__dialog = $('#eventDetailDrillDown').dialog({
            dialogClass: 'eventDetail',
            autoOpen : false,
            height : 580,
            width : 1000,
            modal : true,
            resizable : false,
            title : $.i18n('Event'),
            close : function(event, ui) {
                nx.hub.stockChanged.unsubscribe();
                $('.dropdown-menu').hide();
                me.__eventIdListBookmark = null;
            }
        });
        
        nx.rest.help.config(function(response) {
        	if($.parseJSON(response["eventDetail"])) {
        		var button = $('<button>')
            	.addClass('eventDetailHelpButton')
    	        .button({icons: {primary: 'ui-icon-help eventDetailHelpIcon'}})
    	        .click(function(e) {
    	        	nx.rest.help.config(function(response) {
                    	var helpPath = response["helpDocumentPath"];
                    	window.open(helpPath + '#' + 'eventDetail', '_blank');
    	        	});	
    	        });

            $(".eventDetail").children(".ui-dialog-titlebar").append(button);
        	}
        });
      
        this.__hiddenDiv = $('<div>').attr('id', 'screenMask').addClass('screenMask');
        $('body').append(this.__hiddenDiv);
        
        this.__commentDialogs = {};
        this.__currentDialog = null;
        
        this.__licences = {};
        this.obtainedLicencesDfd = this.__getLicences();
        
        this.__fleetPermissions = {};
        this.obtainedFleetPermissionsDfd = this.__getFleetList();

        nx.rest.event.eventDetailConfiguration(function(configuration) {
            me.__configuration = configuration;
            
            me.obtainedLicencesDfd.then(function() {
                me.__initialise(configuration);
                me.__configDfd.resolve();
            });
        });
    };
    
    ctor.prototype.__getLicences = function() {
        var me = this;
        var getLicenceDfd = $.Deferred();
        
        nx.rest.event.getLicences(function(response) {
            me.__licences = response;
            
            getLicenceDfd.resolve();
        });
        
        return getLicenceDfd.promise();
    }
    
    ctor.prototype.__getFleetList = function() {
    	var me = this;
    	var getFleetListDfd = $.Deferred();
    	
    	nx.rest.fleet.getFleetList(function(response) {
    		me.__fleetPermissions = response;
    		
    		getFleetListDfd.resolve();
    	});
    	
    	return getFleetListDfd.promise();
    }

    ctor.prototype.__resetDeferredActions = function() {
        this.__rendererDfd = $.Deferred();
        this.__rendererLoaded = this.__rendererDfd.promise();

        this.__eventDfd = $.Deferred();
        this.__eventLoaded = this.__eventDfd.promise();
    }

    ctor.prototype.show = function(fleetId, eventId, eventListIds) {
        var me = this;
        
        if (!me.__licences['eventDetail']) {
        	return;
        }
        
        var permission = false;
        for (var i in me.__fleetPermissions) {
    		if (me.__fleetPermissions[i].code == fleetId) {
    			permission = true;
    		}
    	}
        if (!permission) {
        	$('<div></div>').dialog({
                buttons: { 'Ok': function () { $(this).dialog('close'); } },
                close: function (event, ui) { $(this).remove(); },
                resizable: false,
                title: $.i18n('Notification'),
                modal: true
            }).text($.i18n('You do not have permission to access this.'));
        	return;
        }

        this.__resetDeferredActions();

        // Loading the event details --
        // We should only do this once we are sure the configuration has been 
        // loaded to avoid js Uncaught TypeError.
        $.when(this.__configLoaded).done(function() {    
            me.__loadEvent(fleetId, eventId);
            
            if (!!eventListIds) {
                me.__eventIdList = eventListIds;
                if (eventListIds.every(isNaN)) {
                var eventIndex =  me.__eventIdList.indexOf(fleetId + "-" + eventId); 
                } else {
                	var eventIndex =  me.__eventIdList.indexOf(eventId);
                }
                if(!!me.__eventIdListBookmark) {
                    eventIndex = me.__eventIdList.indexOf(me.__eventIdListBookmark);
                }
                if (eventIndex < me.__eventIdList.length - 1) {
                    $('#nextEvent').removeClass("ui-state-disabled").attr("disabled", false);
                } else {
                    $('#nextEvent').addClass("ui-state-disabled").attr("disabled", true);
                }
                if (eventIndex > 0) {
                    $('#previousEvent').removeClass("ui-state-disabled").attr("disabled", false);
                } else {
                    $('#previousEvent').addClass("ui-state-disabled").attr("disabled", true);
                }
            } else {
                $('#nextEvent').addClass("ui-state-disabled").attr("disabled", true);
                $('#previousEvent').addClass("ui-state-disabled").attr("disabled", true);
            }
                
        });
        this.__reset();

        this.__dialog.dialog('open');
        
        $.when(this.__eventLoaded, this.__configLoaded).done(function() {
            me._displayEventDetail(me.__event);
        });

        $.when(this.__rendererLoaded, this.__configLoaded, this.__eventLoaded).done(function() {
            me.__eventDetailRenderer.render($('#eventDetailData'), me.__configuration, me.__event, me);
        });
    };
    
    ctor.prototype.__generateLink = function() {
    	var me = this;
    	
    	var data = {};
    	data.eventId = me.__event.id;
    	data.fleetId = me.__fleetId;
    	data.eventIds = [];
    	
    	var result = '/eventLink?event=' + data.fleetId +'-' +data.eventId;
    	return result;
    };
    
    ctor.prototype.__getTextLinkUrl = function() {
    	var me = this;
    	return window.location.protocol + '//' + window.location.hostname + ':' + window.location.port + me.__eventLink;
    };
    
    ctor.prototype.__updateEventLink = function() {
    	var me = this;
    	me.__eventLink = me.__generateLink();
    	var textURL = me.__getTextLinkUrl();

    	var title = $('#ui-dialog-title-eventDetailDrillDown');
    	title.html("<a href='" + textURL + "' target='_blank' class='eventTitleLink'>" + $.i18n('Event') + ' ' + me.__fleetId + '-' + me.__event.id + "</a>");
    };
        
    ctor.prototype.__prev = function() {
    	if (this.__eventIdList.every(isNaN)) {
            var eventIndex =  this.__eventIdList.indexOf(this.__event.fleetId + "-" + this.__event.id);
            } else {
            	var eventIndex =  this.__eventIdList.indexOf(this.__event.id);
            }
        // bookmark for eventgroups
        if(!!this.__eventIdListBookmark) {
            eventIndex = this.__eventIdList.indexOf(this.__eventIdListBookmark);
            this.__eventIdListBookmark = null;
        }
        if (eventIndex > 0) {
            $('.dropdown-menu').hide();
            var newEvent = this.__eventIdList[eventIndex - 1];
            var fleetId = this.__event.fleetId;
            var eventId = this.__event.id;
            if (!!newEvent && isNaN(newEvent)) {
                var newEventId = newEvent.split("-");
                fleetId = newEventId[0];
                eventId = newEventId[1];
            } else {
            	eventId = newEvent;
            }
            this.show(fleetId, eventId, this.__eventIdList);
        }
        
    };

    ctor.prototype.__next = function() {
    	if (this.__eventIdList.every(isNaN)) {
            var eventIndex =  this.__eventIdList.indexOf(this.__event.fleetId + "-" + this.__event.id);
            } else {
            	var eventIndex =  this.__eventIdList.indexOf(this.__event.id);
            }
        // bookmark for eventgroups
        if(!!this.__eventIdListBookmark) {
            eventIndex = this.__eventIdList.indexOf(this.__eventIdListBookmark);
            this.__eventIdListBookmark = null;
        }
        if (eventIndex < this.__eventIdList.length - 1) {
            $('.dropdown-menu').hide();
            var newEvent = this.__eventIdList[eventIndex + 1];
            var fleetId = this.__event.fleetId;
            var eventId = this.__event.id;
            if (!!newEvent && isNaN(newEvent)) {
                var newEventId = newEvent.split("-");
                fleetId = newEventId[0];
                eventId = newEventId[1];
            } else {
            	eventId = newEvent;
            }
            this.show(fleetId, eventId, this.__eventIdList);
        }
        
    };
    
    ctor.prototype.__loadEvent = function(fleetId, eventId) {
        var me = this;
        
        nx.rest.event.eventDetail(fleetId, eventId, function(event) {
            me.__event = event.fields;
            me.__fleetId = fleetId;
            me.__eventDfd.resolve();
            
            if (!!me.__eventsAreLinkable) {
            	me.__updateEventLink();
            }
            
            var eventRenderer = me.__configuration.eventDetailRenderer;
            
            function instanciateDetailRenderer(rendererObject) {
                me.__eventDetailRenderer = new rendererObject();
                me.__rendererDfd.resolve();
            }
            ;

            // If our renderer object exists, we can instanciate it directly
            if (!!nx.event[eventRenderer.rendererObject]) {
                instanciateDetailRenderer(nx.event[eventRenderer.rendererObject]);
            } else {
                // If our renderer object doesn't exists, we will have to download the script, that contains the object
                $.getScript(eventRenderer.rendererFile, function(data, textStatus, jqxhr) {
                    instanciateDetailRenderer(nx.event[eventRenderer.rendererObject]);
                });
            }
            
            // Loading additional css files
            if (!me.__cssLoaded) {
                $.each(eventRenderer.additionalCssFileList, function(index, cssFile) {
                    $("<link/>", {
                        rel: "stylesheet",
                        type: "text/css",
                        href: cssFile
                    }).appendTo("head");
                });
                me.__cssLoaded = true;
            }
            
            // Loading additional js files
            $.each(eventRenderer.additionalJsFileList, function(index, jsFile) {
                $.getScript(jsFile, function(data, textStatus, jqxhr) {
                });
            });
        });
    }

    ctor.prototype.__initialise = function(configuration) {
        var me = this;
        
        me.__initToolbar();
        
        if(!!configuration.linkableEvents) {
        	me.__eventsAreLinkable = true;
        }
        
        var columns = configuration.infoFields;
        
        $('#eventDescriptionTable').remove();
        var eventDescTable = $('<table>').attr('id', 'eventDescriptionTable').appendTo('#eventDescription');
        
        for ( var i = 0, l = columns.length; i < l; i++) {
            var column = columns[i];
            if (column.visible) {
                var row = $('<tr>');

                var label = $('<label>').addClass('eventLabel').text($.i18n(column.label));
                $('<td>').append(label).appendTo(row);

                var valueLabel = $('<label>');

                var value = $('<td>').append(valueLabel).appendTo(row);

                eventDescTable.append(row);

                if (column.reference) {
                    this.__eventDetailLabels[column.referencedField] = label;
                }

                this.__eventDetailLabels[column.name] = valueLabel;
            }

            this.__columnsMap[column.name] = column;
        }

        // We use "this.__getKeys" because IE 8 doesn't support Object.keys
        if (!!configuration.eventStatusActions && !!this.__licences['updateStatus']
                && this.__getKeys(configuration.eventStatusActions).length > 0) {

            var edUpdaterContainer = $('<div>').attr('id', 'edUpdaterContainer');

            eventDescTable.append($('<tr>').append($('<td>').attr('colspan', '2').append(edUpdaterContainer)));

            this.__eventDetailUpdater = new nx.event.EventDetailUpdater(edUpdaterContainer, this);
        }

        // Fill the remaining space with a map...
        this.__fillSpaceWithMap(configuration);
    };
    
    ctor.prototype.__initToolbar = function() {
        var me = this;
        var toolbar = $('#eventDetailToolbar').addClass('ui-widget-header');
        me.__toolbar = toolbar;
        
        // build nav buttons
        if(!!me.__configuration.navigationButtonsEnabled) {
            me.__initAppNavButtonDropdown(me.__configuration.navigationButtons, toolbar)
        }
        
        if(!!me.__configuration.actionButtonsEnabled) {
            me.__initActionButtonDropdown(me.__configuration.toolbarButtons, toolbar);
        }
        
        me.__createEventNavButtons(toolbar);
    };
    
    ctor.prototype.__createEventNavButtons = function(toolbar) {
        var me = this;
        
        $('<div>').attr('id', 'eventDetailTopButtonContainer').appendTo(toolbar);
        
        var createPreviousButton = $('<button id="previousEvent"><span class="ui-icon ui-icon-triangle-1-w"></button>')
            .addClass('eventNavButton')
            .button({
                //label: $.i18n('&lt; Prev')
            }).click(function(e) {
                nx.hub.stockChanged.unsubscribe();
                me.__prev();
            }).appendTo($('#eventDetailTopButtonContainer'));
        
        var createNextButton = $('<button id="nextEvent"><span class="ui-icon ui-icon-triangle-1-e"></span></button>')
            .addClass('eventNavButton')
            .button({
                //label: $.i18n('Next &gt;')
            }).click(function(e) {
                nx.hub.stockChanged.unsubscribe();
                me.__next();
            }).appendTo($('#eventDetailTopButtonContainer'));
    };
    
    ctor.prototype.__initActionButtonDropdown = function(conf, target) {
        var me = this;
        
        var container = $('<div>')
            .addClass('dropdown')
            .attr('id', 'actionButtonDropdown')
            .appendTo(target);
        
        var button = $('<button>')
                .attr('type', 'button')
                .html($.i18n('Action'))
                .appendTo(container).button();
        var caret = $('<span class="ui-icon ui-icon-triangle-1-s"></span>').addClass('dropdown-caret').appendTo(button);
        
        var list = $('<div>').hide().addClass('dropdown-menu').appendTo(container);
        
        $.each(conf, function(i, actionButton) {
            if (!actionButton.isLicenced || (!!actionButton.isLicenced && !!me.__licences[actionButton.name])) {            
                var item = $('<div>').addClass('dropdownBtnContainer').appendTo(list);
                $('<button>')
                    .attr('id', actionButton.name + 'Btn')
                    .addClass('dropdownBtn')
                    .html($.i18n(actionButton.displayName))
                    .button()
                    .appendTo(item)
                    .click(function() {
                        if (!!actionButton.opensPopup) {
                            window.onbeforeunload = function() {
                                return $.i18n("LeavePage");
                            };
                            me.__currentDialog = me.__commentDialogs[actionButton.name];
                            me.__currentDialog.__open();
                        } else {
                            me.__execButtonAction(actionButton.name, false);
                        }
                    });
                
                if (!!actionButton.opensPopup) {
                    me.__commentDialogs[actionButton.name] = new nx.event.eventDetailCommentDialog(me, actionButton);
                }    
            }
            else {
            	me._removeActionItem=true;
            }
        });
        
        button.click(function() {
            if(list.css('display') == 'none') {
                $('.dropdown-menu').hide();
                caret.removeClass();
                caret.addClass('ui-icon ui-icon-triangle-1-n');
                list.show();
            } else {
                caret.removeClass();
                caret.addClass('ui-icon ui-icon-triangle-1-s');
                list.hide();
            }
        });
        
        if(me._removeActionItem){
        	$('#actionButtonDropdown').remove();
        }
    };
    
    ctor.prototype.__execButtonAction = function(mode, calledFromPopup) {       
        var me = this;
        switch(mode) {
        case 'comment' :
            if (calledFromPopup) {
                comment = this.__checkForComment();
                this.__comment("com", comment);
            }
            break;
        case 'acknowledge' :
            this.__acknowledge(calledFromPopup);
            break;
        case 'deactivate' :
            $('<div></div>').dialog({
                buttons: { 'Ok': function () { me.__deactivate(calledFromPopup);$(this).dialog('close'); }, 'Cancel': function () { $(this).dialog('close');me.__currentDialog.__close(); } },
                close: function (event, ui) { $(this).remove(); },
                resizable: false,
                title: $.i18n('Notification'),
                modal: true
            }).text($.i18n('You are about to deactivate this event. This is not reversible. Are you sure?'));
            break;
        case 'createMaximoServiceRequest' :
            this.__createServiceRequest(calledFromPopup);
            break;
        case 'group' :
            localStorage.setItem("eventDetailShowEventHistory", true);
            localStorage.setItem("unitNumber", this.__event.unitNumber);
            window.location.replace(window.location.origin + "/event")
            break;
        case 'sendFault' :
            this.__sendFault();
            break;
        case 'saveData' :
        	this.__updateSelectedPeriodValues();
            break;
                
            
        }
    };

    ctor.prototype.__initAppNavButtonDropdown = function(conf, target) {
        var me = this;
        
        var container = $('<div>')
            .addClass('dropdown')
            .attr('id', 'appNavButtonDropdown')
            .appendTo(target);
        
        var button = $('<button>')
                .attr('type', 'button')
                .html($.i18n('Navigation'))
                .appendTo(container).button();
        var caret = $('<span class="ui-icon ui-icon-triangle-1-s"></span>').addClass('dropdown-caret').appendTo(button);
        
        var list = $('<div>').hide().addClass('dropdown-menu').appendTo(container);
                
        $.each(conf, function(i, navButton) {
            var item = $('<div>').addClass('dropdownBtnContainer').appendTo(list);
            $('<button>')
                .attr('id', navButton.id)
                .addClass('dropdownBtn')
                .html($.i18n(navButton.displayName))
                .button()
                .appendTo(item)
                .click(function() {
                    if (!!navButton.openUnitSummary) {
                        var event =  me.__event;
                        nx.util.openHistoricUnitSummary(event.sourceId, event.fleetId,
                                navButton.unitSummaryTab, event.sourceNumber, event.createTime, event.id);
                    } else if (!!button.alternativeAction) {
                        eval(navButton.alternativeAction);
                    }
                });
        });
        
        button.click(function() {
            if(list.css('display') == 'none') {
                $('.dropdown-menu').hide();
                caret.removeClass();
                caret.addClass('ui-icon ui-icon-triangle-1-n');
                list.show();
            } else {
                caret.removeClass();
                caret.addClass('ui-icon ui-icon-triangle-1-s');
                list.hide();
            }
        });
    };
    
    ctor.prototype.__comment = function(action, comment, timestamp) {
        var me = this;
        
        if (!comment) {
            return;
        }
        
        var fleetId = this.__event.fleetId;
        var eventId = this.__event.id;
        if (!timestamp) {
            timestamp = null;
        }
        var userName = '';
        
        nx.rest.event.saveComment(fleetId, eventId, timestamp, comment, action, function(response) {
            me.__currentDialog.__clearComment();
            me.__currentDialog.__close();
            me.__eventDetailRenderer.__refreshComments();
        });
    };
    
    ctor.prototype.__checkForComment = function() {
        var comment = $.trim(this.__currentDialog.__getComment());
        if(comment.length > 0) {
            return comment;
        } else {
            return null;
        }
    };
    
    ctor.prototype.__checkForValidInputs = function() {
    	var values = this.__currentDialog.__getInputValues();
    	return values;
    };
    
    ctor.prototype.__acknowledge = function(requiresComment) {
        var me = this;
        if (requiresComment) {
            var comment = this.__checkForComment();
        }
        
        if (!requiresComment || !!comment) {
            nx.rest.event.acknowledge(me.__event.fleetId, me.__event.id, me.__event.rowVersion, me.__event.faultGroupId, function() {
                
                if (requiresComment) {
                    me.__comment("ack", comment);
                }
                
                me.__reload();
            }, function(jqXHR, textStatus, errorThrown) {
                if (jqXHR.status == 409) {
                    if (!!me.__currentDialog) {
                        me.__currentDialog.__close(); 
                    }
                    alert($.i18n("Can't save the change, another user did changes to this event."));
                    me.__reload();
                }
            });
        }
    };
    
    ctor.prototype.__deactivate = function(requiresComment) {
        var me = this;
        
        if (requiresComment) {
            var comment = this.__checkForComment();
        }
        
        if (!requiresComment || !!comment) {
           var userName = '';
               
            nx.rest.system.user(function(user) {
                if(user.name != null && user.name.trim().length > 0) {
                    userName = user.name;
                } else {
                    userName = user.id;
                }
                
                nx.rest.event.deactivate(me.__event.fleetId, me.__event.id, userName, false, me.__event.rowVersion, function(endTime) {
                    
                    nx.rest.event.getEndTime(me.__event.fleetId, me.__event.id, function(getEndTime) {
                        if ((requiresComment) && (getEndTime)) {
                            me.__comment("deac", comment, getEndTime);
                        }
                    });
                    
                    me.__reload();
                }, function(jqXHR, textStatus, errorThrown) {
                    if (jqXHR.status == 409) {
                        if (!!me.__currentDialog) {
                            me.__currentDialog.__close();
                        }
                        alert($.i18n("Can't save the change, another user did changes to this event."));
                        me.__reload();
                    }
                });
            });
        }
    };
    
    ctor.prototype.__updateSelectedPeriodValues = function () {
    	var me = this;
    	var vehicleId = me.__event.sourceId;
    	var periodSelected = localStorage.getItem('selectedPeriod');
    	var fleetId = me.__fleetId;
    	var eventID = me.__event.id;
    	var timestamp = me.__event.createTime;
    	
    	nx.rest.event.saveFaultPeriod(fleetId,periodSelected, eventID, timestamp, vehicleId, function() {

            $('<div></div>').dialog({
                buttons: { 'Ok': function () { $(this).dialog('close'); } },
                close: function (event, ui) { $(this).remove(); },
                resizable: false,
                title: $.i18n('Notification'),
                modal: true
            }).html($.i18n('Data saved successfully.'));
        
    		
          }, function(jqXHR, textStatus, errorThrown) {


              $('<div></div>').dialog({
                  buttons: { 'Ok': function () { $(this).dialog('close'); } },
                  close: function (event, ui) { $(this).remove(); },
                  resizable: false,
                  title: $.i18n('Notification'),
                  modal: true
              }).html($.i18n('Error saving data.'));
          
      		
            
              if (jqXHR.status == 409) {
                  me.__reload();
              }
          });
    	
    	
	};
    

    // TODO all of this shouldn't be in spectrum-base, should be refactored
    ctor.prototype.__createServiceRequest = function(calledFromPopup) {
        var me = this;
        
        var validInputs = this.__currentDialog.__validateInputs();
        var userInputs = this.__checkForValidInputs();

        // get all information needed for building the service request
        me.__event.description =  userInputs.description != null ? userInputs.description : me.__event.description;
        me.__event.externalRecord = userInputs.externalRecord != null ? userInputs.externalRecord : me.__event.externalRecord;
        me.__event.serviceRequestPriority = userInputs.serviceRequestPriority != null ? userInputs.serviceRequestPriority : me.__event.serviceRequestPriority;
        me.__event.systemCode = userInputs.systemCode != null ? userInputs.systemCode : me.__event.systemCode;
        me.__event.controlRoomPriority = userInputs.controlRoomPriority != null ? userInputs.controlRoomPriority : me.__event.controlRoomPriority;
        me.__event.positionCode = userInputs.positionCode != null ? userInputs.positionCode : me.__event.positionCode;
        me.__event.qualityProfile = userInputs.qualityProfile != null ? userInputs.qualityProfile : me.__event.qualityProfile;
        me.__event.headcode = userInputs.headcode != null ? userInputs.headcode : me.__event.headcode;
        me.__event.safetyPriority = userInputs.safetyPriority != null ? userInputs.safetyPriority : me.__event.safetyPriority;
        me.__event.maintenanceFunction = userInputs.maintenanceFunction != null ? userInputs.maintenanceFunction : me.__event.maintenanceFunction;
        me.__event.notificationTimestamp = userInputs.notificationTimestamp != null ? userInputs.notificationTimestamp : me.__event.notificationTimestamp;
        me.__event.materialNumber = userInputs.materialNumber != null ? userInputs.materialNumber : me.__event.materialNumber;
        
        if (!calledFromPopup || (!!validInputs)) {

            // build parameters to send to Maximo as Service Request
            var params = {
	            'fleetId' : me.__event.fleetId,
	            'eventId': me.__event.id,
	            'rowVersion': me.__event.rowVersion,
	            'description': me.__event.description,
	            'longDescription': me.__event.engineerAdvice + "\n" +
                                   me.__event.additionalInfo,
	            'eventCode': me.__event.eventCode,
	            'serviceRequestPriority': me.__event.serviceRequestPriority,
	            'systemCode': me.__event.systemCode,
	            'controlRoomPriority': me.__event.controlRoomPriority,
	            'locationCode': me.__event.positionCode,
	            'qualityProfile': me.__event.qualityProfile,
	            'headcode': me.__event.headcode,
	            'safetyPriority': me.__event.safetyPriority,
	            'workorderLocationType': me.__event.maintenanceFunction,
	            'createTime': me.__event.createTime,
	            'unitNumber': me.__event.unitNumber,
	        };

            // if the request is from popup save the comment and then
            // make a service request, otherwise simply send the service request	
            if (calledFromPopup) {
	        	var fleetId = this.__event.fleetId;
                var eventId = this.__event.id;
                var timestamp = null;
                var action = 'req';
                var comment = this.__checkForComment();
                if(comment == null){
                	comment = '';
                }

                nx.rest.event.saveComment(fleetId, eventId, timestamp, comment, action, function(response) {
                    me.__sendServiceRequest(params);
                });
            } else {
                me.__sendServiceRequest(params);
            }
        } 
    };

    ctor.prototype.__sendServiceRequest = function(parameters) {
        var me = this;

        $("body").addClass("cursorWaiting");
        $('#screenMask').show();

        // TODO implement a generic service for service requests in spectrum base
        nx.ns.rest.maximo.createServiceRequest(parameters, function(response) {
            $('<div></div>').dialog({
                buttons: { 'Ok': function () { $(this).dialog('close'); } },
                close: function (event, ui) { $(this).remove(); },
                resizable: false,
                title: $.i18n('Notification'),
                modal: true
            }).html(nx.event.eventDetailSuccessfulServiceRequestHandler(response));
            
            me.__currentDialog.__clearComment();
            me.__currentDialog.__close();
                        
            sessionStorage.setItem('newEventsFlag', 'true');
            $("body").removeClass("cursorWaiting");
            $('#screenMask').hide();
            me.__reload();
            $('#searchEvents').click();
        }, function(jqXHR, textStatus, errorThrown) {
            if (jqXHR.status == 409) {
                alert($.i18n("Can't send the request, another user did changes to this event."));
                $("body").removeClass("cursorWaiting");
                $('#screenMask').hide();
                me.__reload();
            } else {
                $('<div></div>').dialog({
                    buttons: { 'Ok': function () { $(this).dialog('close'); } },
                    close: function (event, ui) { $(this).remove(); },
                    resizable: false,
                    title: $.i18n('Notification'),
                    modal: true
                }).text(jqXHR.status + ' ' + errorThrown);
                 
                $("body").removeClass("cursorWaiting");
                $('#screenMask').hide();
                me.__reload();
            }                        
        });
    }
    
    ctor.prototype.__sendFault = function () {
        var me = this;
        
        if (!this.__event.e2mFaultCode) {
            $('body').append("<div id='notificationPopup'><p>No E2M fault code found.</p></div>");
            $('#notificationPopup').dialog({
                autoOpen : true,
                height : 100,
                width : 280,
                modal : true,
                resizable: false,
                title : $.i18n('Error - Unable to send fault'),
                close : function() {
                    $('#notificationPopup').remove();
                }
            }); 
        } else {
            nx.rest.event.sendFault(this.__event.fleetId, this.__event.id, function(response) {
                me._displayEventDetail(response);
            });
        }
    };
    
    ctor.prototype.__getKeys = function(obj) {
        var keys = [];
        for(var key in obj) {
            keys.push(key);
        }
        
        return keys;
    };

    ctor.prototype.__fillSpaceWithMap = function(configuration) {
        if (configuration.mapEnabled) {
            $('#eventDescriptionTable').css('max-height', '271px')
            var toolbarHeight = $('#eventDetailToolbar').height();
            var eventDescriptionHeight = $('#eventDescription').height();
            $('#eventLocation').css({
                top : (eventDescriptionHeight + 5) + 'px',
                height : (this.__dialog.height() - (eventDescriptionHeight + 5) - (toolbarHeight + 5))
            }).show();
        }
    };

    ctor.prototype.__reset = function() {
        $('#eventDetailDataSlider').empty();
        localStorage.removeItem("selectedPeriod");
        for ( var key in this.__eventDetailLabels) {
            var label = this.__eventDetailLabels[key];
            label.text('');
        }
        
        $('#ui-dialog-title-eventDetailDrillDown').html("<a>Event</a>");

        $('#edMapBlock').empty();

        if (!!this.__eventDetailUpdater) {
            this.__eventDetailUpdater._reset();
        }

        $('#eventDetailData').empty();
    };

    ctor.prototype.__showMap = function(event) {
        if (!event.latitude || !event.longitude) {
            event.latitude = 0;
            event.longitude = 0;
            // return;
        }

        // Sometimes an Internet connection is not available and maps cannot be loaded. This will allow the rest
        // of the screen to load in this situation.
        if (window.Microsoft === undefined) {
            return;
        }

        var location = new Microsoft.Maps.Location(event.latitude, event.longitude);

        var mapOptions = {
            // Key should come from elsewhere...
            'credentials' : 'AgJGCohfkXMsUBWvLBNVmL4OmIIOQYU-L8xeYNBjTNaMmD8ooayUqKzYwcsS8i9c',
            'disableBirdsEye' : false,
            'enableSearchLogo' : false,
            'enableClickableLogo' : false,
            'backgroundColor' : 'red',
            'showScalebar' : false,
            'showDashboard' : false,
            'showMapTypeSelector' : false,
            'disablePanning' : true,
            'width' : $('#edMapBlock').width(),
            'height' : $('#edMapBlock').height(),
            'mapTypeId' : Microsoft.Maps.MapTypeId.road,
            'center' : location,
            'zoom' : 15
        };

        var pushpinOptions = {
            'icon' : 'img/cross_red.png',
            'width' : '16px',
            'height' : '16px',
            'anchor' : new Microsoft.Maps.Point(8, 8)
        };

        $('#edMapBlock').empty();

        var mapView = new Microsoft.Maps.Map(document.getElementById('edMapBlock'), mapOptions);
        var pushpin = new Microsoft.Maps.Pushpin(location, pushpinOptions);

        mapView.entities.push(pushpin);
    };
    
    ctor.prototype._displayEventDetail = function(event) {
        var me = this;
        
        if ($('#sendFaultButton').length > 0 && event.transmissionStatus 
                && event.transmissionStatus != "Error") {
            $('#sendFaultButton').addClass("ui-state-disabled").attr('disabled', 'disabled');
        } 

        if ($('#deactivateBtn').length > 0) {
            if (!!event.isAcknowledged && !!event.current) {
                $('#deactivateBtn').removeClass("ui-state-disabled").attr("disabled", false);   
            } else if (!event.isAcknowledged && !!event.current || !event.current){
                $('#deactivateBtn').addClass("ui-state-disabled").attr("disabled", true);
            }
        }
        
        if ($('#acknowledgeBtn').length > 0) {
            if (!!event.isAcknowledged) {
                $('#acknowledgeBtn').addClass("ui-state-disabled").attr("disabled", true);
            } else {
                $('#acknowledgeBtn').removeClass("ui-state-disabled").attr("disabled", false);
            }
        }
        
        var hasCreateServiceRequestPermission = false;
        var hasMaximoId = !!event.maximoId;

        if (me.__event.controlRoomPriority && me.__event.controlRoomPriority.trim() != "") {
            var priorityStatus = me.__event.controlRoomPriority;
            var priorityString = 'controlRoomPriority'
            priorityString += priorityStatus;
            outer:
            for (licence in this.__licences) {
                var licenceCheck = 'this.__licences.'
                licenceCheck += licence;
                licenceCheck = eval(licenceCheck);
                if ((licence == priorityString) && (licenceCheck == true)) {
                	hasCreateServiceRequestPermission = true;
                    break outer;
                }
            }
        } else if (me.__event.controlRoomPriority && me.__event.controlRoomPriority.trim() == "") { 
        	//if there is no existing priority, as long as the user has 1 licence
            outer:
            for (licence in this.__licences) {
            	if (licence.substring(0, licence.length-1) == 'controlRoomPriority') {
            		var licenceCheck = 'this.__licences.'
                        licenceCheck += licence;
                        licenceCheck = eval(licenceCheck);
                        if (licenceCheck == true) {
                        	hasCreateServiceRequestPermission = true;
                            break outer;
                        }
            	}

            }
        }
        
        if ($('#createMaximoServiceRequestBtn').length > 0) {
            if (!hasMaximoId && hasCreateServiceRequestPermission) {
                $('#createMaximoServiceRequestBtn').removeClass("ui-state-disabled").attr("disabled", false);
            } else {
                $('#createMaximoServiceRequestBtn').addClass("ui-state-disabled").attr("disabled", true);
            }
        }
        
        if (!!this.__eventDetailUpdater) {
            var eventStatusActions = me.__configuration.eventStatusActions[event.status];

            if (!event.current || !eventStatusActions) {
                this.__eventDetailUpdater._disable();
            } else {
                this.__eventDetailUpdater._configure(event.fleetId, event.id, eventStatusActions);
            }
        }

        for ( var key in me.__eventDetailLabels) {
            var label = me.__eventDetailLabels[key];
            var value = event[key];
            var formatString = me.__columnsMap[key].formatter;
            
            
            if (value == null || $.trim(value) == '') {
                value = '';
            } else if (formatString != null) {
                value = $.format(formatString, value);
            }

            if (me.__columnsMap[key].linkable) {
                var linkableText = document.createElement('a');
                $(linkableText).attr('href', value).attr('target', '_blank').text(value).appendTo(label);
            } else {
                label.append($.i18n(value));
            }
           
            if ((key == 'qualityProfile') && ($.i18n(event.fleetId + '-' + event.qualityProfile) != event.fleetId + '-' + event.qualityProfile)) { 
                label.attr('title', $.i18n(event.fleetId + '-' + event.qualityProfile));
            } else {
                label.attr('title', $.i18n(value));
            }
        }

        // if mapEnabled..
        me.__event = event;

        if (me.__configuration.mapEnabled) {
            me.__fillSpaceWithMap(me.__configuration);
            me.__showMap(event);
        }
    };
    
    ctor.prototype.__reload = function() {
        this.show(this.__fleetId, this.__event.id, this.__eventIdList);
    };
    
    return ctor;
}());

// A general handler for handling the successful response of service request on EventDetail page
nx.event.eventDetailSuccessfulServiceRequestHandler = function (response) {
	return "<p>" + $.i18n('Service Request created.') + "</p>" +
        // if the long description field was longer than the limit
        // warn the user with a message
        (response.maxLimitReached ? "<p>" + $.i18n("LimitReachedLongDescription") + "</p>" : "");
}

nx.event.EventDetailUpdater = (function() {
    function ctor(container, eventDetail) {
        this.__eventDetail = eventDetail;
        
        this.__fleetId = null;
        this.__eventId = null;

        this.__comment = null;
        this.__date = null;
        this.__hour = null;
        this.__minute = null;
        this.__second = null;
        this.__status = null;
        this.__button = null;

        this.__alertMessage = null;
        this.__alertDialog = null;

        this.__initialize(container);

        // XXX - Is it necessary? When and Where to display the comments?
        // Populate the comment field with the first comment
        // nx.rest.event.comments(event.fleetId, event.id, function(response) {
        // display the events
        // });
    }
    ;

    ctor.prototype.__initialize = function(container) {
        var me = this;

        this.__alertMessage = $('<div>').attr('id', 'edUpdaterAlertMessage');

        this.__alertDialog = $('<div>').attr('id', 'edUpdaterAlertDialog')
                .css('display', 'none').append($('<p>').append($('<span>').addClass('ui-icon').css({
                    'float' : 'left',
                    'margin' : '0 7px 20px 0'
                })).append(this.__alertMessage)).appendTo(container);

        this.__alertDialog.dialog({
            'autoOpen' : false,
            'modal' : true,
            'resizable' : false,
            'buttons' : {
                Ok : function() {
                    $(this).dialog('close');
                }
            }
        });

        this.__comment = $('<input>').attr('id', 'edUpdaterComment').attr('type', 'text')
                .attr('placeholder', 'Comment'); // XXX i18n

        $('<div>').append(this.__comment).appendTo(container);

        this.__date = $('<input>').attr('id', 'edUpdaterDate').attr('type', 'text').attr('placeholder', 'Date'); // XXX
                                                                                                                    // i18n

        this.__hour = $('<input>').attr('id', 'edUpdaterHour').attr('type', 'text').attr('placeholder', 'h'); // XXX
                                                                                                                // i18n

        this.__minute = $('<input>').attr('id', 'edUpdaterMinute').attr('type', 'text').attr('placeholder', 'm'); // XXX
                                                                                                                    // i18n

        this.__second = $('<input>').attr('id', 'edUpdaterSecond').attr('type', 'text').attr('placeholder', 's'); // XXX
                                                                                                                    // i18n

        this.__status = $('<select>').attr('id', 'edUpdaterStatus');

        $('<div>').append(this.__date).append(this.__hour).append(this.__minute).append(this.__second).append(
                this.__status).appendTo(container);

        this.__button = $("<button>Update</button>").button({
            text : false
        }).click(function(e) {
            me.__update();
        });

        $('<div>').attr('id', 'edUpdaterDivButton').append(this.__button).appendTo(container);

        this.__date.datepicker({
            'dateFormat' : 'dd/mm/yy',
            'onSelect' : function(a, b, c) {
                me.__hour.select();
            }
        });

        this.__addTimeHandlers(23, this.__hour, this.__minute);
        this.__addTimeHandlers(59, this.__minute, this.__second);
        this.__addTimeHandlers(59, this.__second, this.__status);

        this.__status.uniform();
    };

    ctor.prototype._configure = function(fleetId, eventId, actions) {
        var me = this;

        this.__fleetId = fleetId;
        this.__eventId = eventId;

        this._reset();
        $.each(actions, function(action, description) {
            $('<option />', {
                'val' : action,
                'text' : description
            }).appendTo(me.__status);
        });

        this.__status.prop("selectedIndex", '').change();
    };

    ctor.prototype.__update = function() {
        var me = this;

        var timestamp = this.__date.datepicker('getDate');

        if (!!timestamp) {
            var hour = me.__hour.val();
            timestamp.setHours(hour != '' ? hour : 0);

            var minute = me.__minute.val();
            timestamp.setMinutes(minute != '' ? minute : 0);

            var second = me.__second.val();
            timestamp.setSeconds(second != '' ? second : 0)

            timestamp = timestamp.getTime();
        }

        var parameters = {
            'fleetId' : me.__fleetId,
            'eventId' : me.__eventId,
            'action' : this.__status.val(),
            'comment' : $.trim(this.__comment.val()),
            'method' : 'M',
            'timestamp' : timestamp
        };

        nx.rest.event.update(
            parameters,
            function() {
                me._disable();
                nx.rest.event.eventDetail(me.__fleetId, me.__eventId, function(event) {
                    me.__showAlert($.i18n('Info'), $.i18n('Event status saved'), false);
                    me.__event = event;
                    me.__eventDetail._displayEventDetail(me.__event);
                });
            }, function() {
                me.__showAlert($.i18n('Error'), $.i18n('Error updating event'), true);
            }
        );
    };

    ctor.prototype.__addTimeHandlers = function(maxValue, el, nextEl) {
        var me = this;

        el.focus(function() {
            el.select();
        });

        el.keypress(function(evt) {
            if (evt.charCode < 48 || evt.charCode > 57) {
                evt.preventDefault();
            }

            setTimeout(function() {
                return function() {
                    var val = el.val();
                    var pad = parseInt(val + "0", 10);

                    var valid = parseInt(val) <= maxValue;

                    if (!valid && val.length > 0) {
                        // the number is too big, remove the last figure
                        el.val(val.substring(0, val.length - 1));
                    } else if (pad > maxValue) {
                        // Has input box reached maximum value?
                        pad = val.length < 2 ? "0" + val : val;
                        el.val(pad);

                        nextEl.focus();
                        nextEl.select();
                    } else if (val.length == 2) {
                        nextEl.focus();
                        nextEl.select();
                    }
                };
            }(el, evt), 0);
        });

        el.blur(function() {
            var val = $.trim(el.val());
            if (val != '') {
                var pad = "00" + el.val();
                pad = pad.slice(pad.length - 2);
                el.val(pad);
            }
        });
    };

    ctor.prototype.__clear = function() {
        this.__comment.val('');
        this.__date.val('');
        this.__hour.val('');
        this.__minute.val('');
        this.__second.val('');

        this.__status.empty();
        $.uniform.update(this.__status);
    };

    ctor.prototype._reset = function() {
        this.__clear();

        this.__comment.prop('disabled', false);
        this.__comment.removeClass('ui-state-disabled');

        this.__date.prop('disabled', false);
        this.__date.removeClass('ui-state-disabled');

        this.__hour.prop('disabled', false);
        this.__hour.removeClass('ui-state-disabled');

        this.__minute.prop('disabled', false);
        this.__minute.removeClass('ui-state-disabled');

        this.__second.prop('disabled', false);
        this.__second.removeClass('ui-state-disabled');

        this.__status.prop('disabled', false);
        $.uniform.update(this.__status);

        this.__button.button('enable');
    }

    ctor.prototype._disable = function() {
        this.__clear();

        this.__comment.prop('disabled', true);
        this.__comment.addClass('ui-state-disabled');

        this.__date.prop('disabled', true);
        this.__date.addClass('ui-state-disabled');

        this.__hour.prop('disabled', true);
        this.__hour.addClass('ui-state-disabled');

        this.__minute.prop('disabled', true);
        this.__minute.addClass('ui-state-disabled');

        this.__second.prop('disabled', true);
        this.__second.addClass('ui-state-disabled');

        this.__status.prop('disabled', 'disabled');
        $.uniform.update(this.__status);

        this.__button.button('disable');
    };

    ctor.prototype.__showAlert = function(title, message, isErrorMessage) {
        this.__alertDialog.dialog('option', 'title', title);

        var icon = this.__alertDialog.find('.ui-icon');

        if (!isErrorMessage) {
            icon.removeClass('ui-icon-alert').addClass('ui-icon-check');
        } else {
            icon.removeClass('ui-icon-check').addClass('ui-icon-alert');
        }

        this.__alertMessage.text(message);
        this.__alertDialog.dialog('open');
    };

    return ctor;
}());

nx.event.eventDetailObj = null;

nx.event.open = function(eventId, fleetId, eventListIds) {
	showEventDetails(eventId, fleetId, eventListIds);
};

nx.event.openLink = function(eventId, fleetId, eventListIds) {
    showEventDetails(eventId, fleetId, eventListIds);
};

function showEventDetails(eventId, fleetId, eventListIds){
	if (nx.event.eventDetailObj == null) {
        nx.event.eventDetailObj = new nx.event.EventDetail();
    }
    
    $("body").addClass("cursorWaiting");
    $.when(nx.event.eventDetailObj.obtainedLicencesDfd, nx.event.eventDetailObj.obtainedFleetPermissionsDfd).done(function() {
    	$("body").removeClass("cursorWaiting");
    	if (!nx.event.eventDetailObj.__licences['eventDetail']) {
        	$('<div></div>').dialog({
                buttons: { 'Ok': function () { $(this).dialog('close'); } },
                close: function (event, ui) { $(this).remove(); },
                resizable: false,
                title: $.i18n('Notification'),
                modal: true
            }).text($.i18n('You do not have permission to access this.'));
        	return;
        } else {
        	nx.event.eventDetailObj.show(fleetId, eventId, eventListIds);
        }
    });
};

nx.event.eventDetailCommentDialog = (function() {
    function ctor(eventDetail, config) {
        var me = this;
        this.__eventDetail = eventDetail;
        this.__config = config;
        this.__tempFieldConf = {};
        this.__commentBox = null;
        this.__checkForm = true;
        this.__dialog = $('<div>')
            .attr('id', this.__config.name + 'EventDetailComment')
            .addClass('eventDetailComment')
            .dialog({
                autoOpen : false,
                height : me.__config.popupHeight,
                width : me.__config.popupWidth,
                modal : true,
                zindex: 1001,
                resizable : false,
                title : $.i18n(me.__config.displayName),
                close : function(event, ui) {
                    window.onbeforeunload = null;
                    $('.dropdown-menu').hide();
                    me.__checkForm = true;
                    me.__eventDetail.__currentDialog = null;
                    me.__clearFields();
                },
                beforeClose: function( event, ui ) {
                    // if there is at least one input that the user filled
                    // then ask if he wants to close the window
                    if (me.__isFormFilled() && me.__checkForm) {
                        var yes = $.i18n('Yes');
                        var no = $.i18n('No');

                        var deleteButtons = {};
                        
                        // put no before yes in case user erroneously presses return 
                        deleteButtons[no] = function() {
                            $(this).dialog('close');
                        };
                        deleteButtons[yes] = function() {
                            // set the flag to false so that when closing the main
                            // dialog this warning will not appear again
                            me.__checkForm = false;

                            $(this).dialog('close');
                            $(me.__dialog).dialog('close');
                        };

                        me.__confirmDialog = $('<div>')
                            .attr('id', 'config-delete')
                            .append('<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>'+$.i18n("LeavePage")+'</span></p>')
                            .dialog({
                                resizable: false,
                                modal : true,
                                title : $.i18n('Delete'),
                                buttons: deleteButtons
                            });

                        return false;
                    } else {
                        return true;
                    }
                },
                buttons: [
                    {
                        id: 'submitButton',
                        text: $.i18n(config.popupFields[0].label),
                        click: function() {
                            me.__submit();
                        }
                    }
                ]
            })
            .html('<div id="' + this.__config.name + 'CommentDialogBody" class="commentBodyContainer">');
        
        
        me.__initialise();
    };
    
    ctor.prototype.__initialise = function() {
        var body = $('#' + this.__config.name + 'CommentDialogBody');
        var fieldsConf = this.__config.popupFields;
        var multipleFields = fieldsConf.length > 1 ? true : false;
        
        for (var i in fieldsConf) {
        	var container = $('<div>').addClass('dialogFieldContainer');
        	
        	var conf = fieldsConf[i];
        	if(!!multipleFields) {
        	    if (conf.mandatory) {
        	        var asterisk = $('<div>').html('* ').addClass('dialogFieldAsterisk');
        	        asterisk.appendTo(container);
        	    }
        	    var label = $('<div>').html($.i18n(conf.label)).addClass('dialogFieldLabel');
        		label.appendTo(container);
        	}
        	var field = this.__buildField(conf);
        	field.appendTo(container);
        	
        	container.appendTo(body);
        }
        
        this.__commentBox = $('#' + this.__config.name + '_comment_input');
        $("#comment_comment_input").attr('maxlength','1000');
        $("#acknowledge_comment_input").attr('maxlength','1000');
        $("#deactivate_comment_input").attr('maxlength','1000');
        
        this.__commentBox.html("");
    };
    
    ctor.prototype.__buildField = function(fieldConf) {
    	var me = this;
    	var field = null;
    	var licences = me.__eventDetail.__licences;
    	switch(fieldConf.type) {
	    	case 'input_text' :
	    		field = $('<input>')
	    					.attr('id', me.__config.name + '_' + fieldConf.name+ '_input')
	    					.attr('type', 'text')
	    					.addClass('popupTextField');
	    		break;
	    	case 'text_area' :
	    		field = $('<textarea>')
	    					.attr('id', me.__config.name + '_' + fieldConf.name + '_input')
	    					.addClass('commentTextArea');
	    		break;
	    	case 'select' :
	    		field = $('<select>')
	    					.attr('id', me.__config.name + '_' + fieldConf.name + '_input')
	    					.attr('width', fieldConf.width);
	    		
	    		var values = fieldConf.dropdownOptions;

	    		$.each(values, function(i, value) {
	    			if(value.licence == null || value.licence == "" || licences[value.licence] == true) {
	    				field.append($('<option>', {
		    				value: value.value,
		    				text: $.i18n(value.label)
		    			}));
	    			}
	    		});
	    		field.uniform();
	    		break;
    	} // end switch
    	
    	// populate readonly fields
    	if(fieldConf.editable == false) {
    		if (fieldConf.type == 'select') {
    			field.prop('disabled', 'disabled');
    		} else {
    			field.attr('readonly', 'readonly');
    		}
		}
    	
    	return field;
    	
    };
    
    ctor.prototype.__populateFields = function() {
    	var me = this;
    	me.__tempFieldConf = $.extend(true, {}, me.__config.popupFields);
    	var conf = me.__tempFieldConf;
    	
    	for(var i in conf) {
    		var fieldConf = conf[i];
    		var field = $('#' + me.__config.name + '_' + fieldConf.name + '_input');
    		
    		if ((!me.__eventDetail.__event[fieldConf.name]) || (me.__eventDetail.__event[fieldConf.name] == ' ')) {
                fieldConf.editable = true;
                field.removeAttr('readonly');
                field.prop('disabled', false);
                if (field.parent().attr('class') == 'selector disabled') {
                    field.parent().attr('class', 'selector')
                }
            }
    		
    		if(fieldConf.editable == false && !!me.__eventDetail.__event[fieldConf.name]) {
                var value = me.__eventDetail.__event[fieldConf.name];
    			var formatString = fieldConf.formatter;
    			if (formatString != null) {
                    value = $.format(formatString, value);
                }
    			field.val(value);
    		}
    		
    		if(fieldConf.editable == false) {
    			field.addClass('readOnlyField');
    			if(fieldConf.type == 'select') {
    				$.uniform.update();
    			}
    		}
    		
    		if (fieldConf.editable == true && fieldConf.type == 'select') {
                field.selectedIndex = 0;
				$.uniform.update();
    		}
    	}
    	
    };
    
    ctor.prototype.__clearFields = function() {
    	var me = this;
    	var conf = me.__tempFieldConf;
    	
    	for(var i in conf) {
    		var fieldConf = conf[i];
    		var field = $('#' + me.__config.name + '_' + fieldConf.name + '_input');
    		field.val('').removeClass('readOnlyField');
    		if (fieldConf.editable == true) {
	            fieldConf.editable = false;
	            field.prop('disabled', 'disabled');
	            field.attr('readonly', 'readonly');
    		}
    	}
    	
    	$('.dialogInvalidInput').removeClass('dialogInvalidInput');
    	
    };
    
    ctor.prototype.__open = function() {
    	this.__populateFields();
        this.__dialog.dialog('open');
    };
    
    ctor.prototype.__close = function() {
    	this.__clearFields();
        this.__dialog.dialog('close');
    };
    
    ctor.prototype.__getComment = function() {
    	this.__validateInputs();
        return this.__commentBox.val();
    };
    
    ctor.prototype.__clearComment = function() {
        this.__commentBox.val('');
    };
    
    // gets all *editable* fields from popup into parameters object, ignore readonly fields
    ctor.prototype.__getInputValues = function() {
    	var me = this;
    	var results = {};
    	
    	if(me.__validateInputs() == false) {
    		//return;
    	};
    	
    	for(var i in me.__tempFieldConf) {
    		var fieldConf = me.__tempFieldConf[i];
    		if(fieldConf.editable == true) {
    			var inputName = me.__config.name + '_' + fieldConf.name + '_input';
        		results[fieldConf.name] = $('#' + inputName).val();
    		}	
    	}
    	
    	return results;
    };
    
    
    // This method is used to check if the user has filled at least one field in a form
    ctor.prototype.__isFormFilled = function() {
        var me = this;
        var isFilled = false;

        for (var i in me.__tempFieldConf) {
            var fieldConf = me.__tempFieldConf[i];
            if(fieldConf.editable == true) {
                var inputName = me.__config.name + '_' + fieldConf.name + '_input';
                var inputValue = $('#' + inputName).val();

                if(inputValue != null && inputValue.trim() != '') {
                    isFilled = true;

                    break;
                }
            }
        }

        return isFilled;
    };

    ctor.prototype.__validateInputs = function() {
    	var me = this;
    	var isValid = true;

    	$('.dialogInvalidInput').removeClass('dialogInvalidInput');
    	
    	for (var i in me.__tempFieldConf) {
    		var fieldConf = me.__tempFieldConf[i];
    		if(fieldConf.editable == true) {
    			var inputName = me.__config.name + '_' + fieldConf.name + '_input';
        		var inputValue = $('#' + inputName).val();
        		
        		// invalid if mandatory field with no input
        		if(fieldConf.mandatory == true && (inputValue == null || inputValue.trim() == '')) {
        			isValid = false;
        			if (fieldConf.type == "select") {
        				$('#uniform-' + me.__config.name + '_' + fieldConf.name + '_input').addClass('dialogInvalidInput');
        			} else {
        				$('#' + me.__config.name + '_' + fieldConf.name + '_input').addClass('dialogInvalidInput');
        			}
        		}
    		}
    	}
    	
    	return isValid;
    };
    
    ctor.prototype.__submit = function() {
        this.__eventDetail.__execButtonAction(this.__config.name, true);
    };

    return ctor;
}());