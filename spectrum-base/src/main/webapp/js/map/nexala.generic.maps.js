var nx = nx || {};
nx.maps = nx.maps || {};
nx.maps.Map = (function() {
    /**
     * A generic map that support marker providers.
     * @param conf
     *      The map configuration object {}.
     *      Options:
     *          container: DOM element to hold the map content
     *          defaultView: View to be used as default/overview
     *          mapMarkersProvider: list of marker providers
     *          
     *      View object:
     *      {
     *          desc: 'view description',
     *          type: 'view type',
     *          id: 'view id',
     *          northWest: {
     *              latitude: 'north west latitude coordinate', 
     *              longitude: 'north west longitude coordinate'
     *          },
     *          southEast: {
     *              latitude: 'south east latitude coordinate', 
     *              longitude: 'south east longitude coordinate'
     *          }
     *      }
     *      
     *      mapMarkersProvider object :
     *      
     *      {
     *      	'markerType': id of this marker type,
     *          'settingLabel': the label that will be show in settings for this marker type,
     *          'jsObject': class of the marker provider,
     *          'zIndex': at which z-index should be this marker type,
     *          'enabledByDefault': if marker are shown by default,
     *          'redrawOnViewChange':if marker need to be redraw when the view has changed,
     *          'legendId': id of the legend if any
     *                   
     *		}
     *
     * @param mapOtions bing map options
     *      
     */
    function ctor(conf, mapOptions) {
        this.__config = conf;

        this.__markersLayer = new Object();
        this.__markersPopup = new Array();
        
        var mapType = conf.defaultView.type;
        if (mapType == null || mapType == ""){
            mapType = Microsoft.Maps.MapTypeId.road;
        }
        
        var initialBounds = Microsoft.Maps.LocationRect.fromCorners(
                new Microsoft.Maps.Location(conf.defaultView.northWest.latitude, conf.defaultView.northWest.longitude),
                new Microsoft.Maps.Location(conf.defaultView.southEast.latitude, conf.defaultView.southEast.longitude)
            );
        
        mapOptions.bounds = initialBounds;
        mapOptions.mapTypeId = mapType;
        
        this.__map = new Microsoft.Maps.Map(this.__config.container, mapOptions);
        
        this._markerProviderRegister = new nx.map.marker.MarkerProviderRegister();
        this.registerMarkerProviders(this.__config.mapMarkersProvider);
        
        this._redrawMarkersOnViewChange();
        
        // Create a layer for all providers
        var providers = this._markerProviderRegister.getAllMarkerProvider();
        for (var i in providers) {
        	var type = providers[i]._type;
        	var zIndex = parseInt(providers[i]._zIndex);
        	this.__markersLayer[type] = new Microsoft.Maps.Layer(type);
        	if (zIndex != -1) {
        		this.__markersLayer[type].setZIndex(zIndex);
        	}
        	
        	this.__map.layers.insert(this.__markersLayer[type]);
        	
        }

        this.__createButtons();
    };
    
	ctor.prototype.getOptions = function(){
		return this.__map.getOptions();
	}
    
	ctor.prototype.setOptions = function(options){
		this.__map.setOptions(options);
	}

	ctor.prototype.setView = function(options){
		this.__map.setView(options);
	}
    
    /** return the marker provider with the type passed in parameter */
    ctor.prototype.getMarkerProviderByType = function(type) {
    	return this._markerProviderRegister.getMarkerProvider(type);
    };
    
    
    /**
     * Register the different marker providers.
     */
    ctor.prototype.registerMarkerProviders = function(markerProviders) {
        if (!!markerProviders && markerProviders.length >0) {
            for (var i in markerProviders) {
            	this._markerProviderRegister.register(markerProviders[i]);
            }
            
            // Create legends
            var allProviders = this._markerProviderRegister.getAllMarkerProvider(); 
            var ids = new Array();
            for(var i in allProviders) {
                
                var legendConfig = allProviders[i]._legend;
                if (!!legendConfig) {
                    var legendId = legendConfig.id;
                }
                
                if(!!legendId && jQuery.inArray(legendId, ids) <0) {
                    // No legend with this id, we add it to the map
                	
                	var legend = $(allProviders[i].getLegend());
                	// zIndex 1 to avoid that legend is hidden
                	legend.zIndex(1).hide();
                	
                	$(this.__config.container).append(legend);
                    ids.push(legendId);
                    
                    allProviders[i].initLegend(this.__map);
                
                } else if (!!legendId) {
                    allProviders[i].updateLegend();
                }
            }
        }
        
    };
    
    /**
     * Draw markers for the provider in parameter
     * @param type type of the provider
     */
    ctor.prototype._drawForProvider = function(type) {
    	var provider = this._markerProviderRegister.getMarkerProvider(type);
    	this._redrawProviders([provider], this.__map);
    	
    };
    
    /**
     * Redraw markers for providers in parameter
     * @param providers list of providers to redraw
     * @param map the map
     */
    ctor.prototype._redrawProviders = function(providers, map) {
    	var me = this;
    	var bounds = map.getBounds();
    	var zoom = map.getZoom();
    	var time = new Date();

    	// iterate over providers
    	$.each(providers, function(i, provider) {

    		var type = providers[i]._type;
    		var currentProvider = providers[i];
    		
    		if (providers[i]._enable) {
    			// Get markers for this provider
        		providers[i].getMarkers(bounds, zoom, time, function(markers) {
        			
        			me._clearProviders([currentProvider], map);
        			
        			jQuery.each(markers, function(mk) {
        				var pin = markers[mk].getPushPin();
        				
        				// add the pin to the layer
        				me.__markersLayer[type].add(pin);
        				
        				var popup = markers[mk].getPopup();

        				if (popup != null) {
        					// Add the popup to the layer if exist
        					me.__markersPopup[type].push(popup);
        					popup.setMap(me.__map);
        				}
        				
        				// initialise the marker
        				markers[mk].initMarker(pin, popup);
        				
        			});
        		});
    		} else {
    			me._clearProviders([providers[i]], map);
    		}
    	});


    };
    
    /**
     * Remove all markers created by these providers
     * @param providers an array of provider
     * @param map the map
     */
    ctor.prototype._clearProviders = function(providers, map) {
    	var me = this;
    	// iterate over providers
    	for (var i in providers) {
    		var type = providers[i]._type;

    		// Remove all markers for this provider
    		me.__markersLayer[type].clear();

    		// Remove popups for this provider
    		if (!!me.__markersPopup[type]) {
    			for(var mkPopupRm in me.__markersPopup[type]) {
    				me.__markersPopup[type][mkPopupRm].setMap(null);
    			}
    		}

    		me.__markersPopup[type] = new Array();
    	}
    
    };

    /**
     * Redraw providers that need to when the view change.
     */
    ctor.prototype._redrawMarkersOnViewChange = function() {
        var me = this;
        
        Microsoft.Maps.Events.addThrottledHandler(this.__map, "viewchangeend", function(e) {
            var providers = me._markerProviderRegister.getMarkerProvidersToRedraw();
            var map = me.__map;
            
            me._redrawProviders(providers, map);
            
            var disabledProviders = me._markerProviderRegister.getDisabledMarkerProvider();
            me._clearProviders(disabledProviders, map);
            
        }, 250);
    };
    
    
    /**
     * Return a setting element that can be add to the footer using nx.main.addSettings([setting, setting...])
     * @param markerProvider the marker provider
     */
    ctor.prototype.__getProviderSetting = function(visibility, markerProvider) {
    	var me = this;
    	
    	var markerType = {
    			id: markerProvider._type,
    			label: $.i18n(markerProvider._settingLabel),
    			type: 'checkbox',
    			value: visibility,
    			visibleOnScreen: true,
    			action: function(on, init) {
    					var current = markerProvider;
    					me._markerProviderRegister.enableType(current._type, on);

    					me._drawForProvider(current._type);

    					// Update legend if necessary
    					var legendConfig = current._legend;
    					if (!!legendConfig) {
    					    var legendId = legendConfig.id;
    					}
    					var show = false;
    					var providers = me._markerProviderRegister.getAllMarkerProvider();
    					for(var p in providers) {
    						if(!!legendId && !!providers[p]._legend && legendId == providers[p]._legend.id 
    						        && providers[p]._enable == true) {
    							// A provider using this legend is displayed
    							show = true;
    							$('#'+legendId).show();
    							break;
    						}
    					}

    					if (!show && !!legendId) {
    						// Change visibility of the legend
    						$('#'+legendId).hide();
    					}
    					
    					if(!init) {
    						var userConfJs = JSON.parse(localStorage.mapFilters);
    				    	
    						userConfJs[markerProvider._type] = on;
    				    	localStorage.setItem('mapFilters', JSON.stringify(userConfJs));
    						
        					var userConf = {
        						variable: "mapFilters",
        						data: localStorage.mapFilters
        					};
        					
        					// Save the user config
        					nx.rest.system.setUserConfiguration(userConf, function() {
        		            	
        		            });
    					}
    					
    				}
    	};

    	markerType.action(visibility, true);
    	return markerType;

    };

    
    /**
     * Create zoom buttons
     */
    ctor.prototype.__createButtons = function() {
        var zoomout = $('.zoomer .zoomout');
        var zoomin = $('.zoomer .zoomin');
        
        var me = this;
        
        zoomout.mousedown(function() {
            $(this).addClass('ui-state-active');
            
            var range = me.__map.getZoomRange();
            var zoom = me.__map.getZoom() - 1;
            
            if (zoom > range.min) {
                me.__map.setView({
                    'zoom': zoom
                });
            }
        });
        
        zoomout.mouseup(function() {
            $(this).removeClass('ui-state-active');
        });
        
        zoomin.mousedown(function() {
            $(this).addClass('ui-state-active');
            
            var range = me.__map.getZoomRange();
            var zoom = me.__map.getZoom() + 1;
            
            if (zoom < range.max) {
                me.__map.setView({
                    'zoom': zoom
                });
            }
        });
        
        zoomin.mouseup(function() {
            $(this).removeClass('ui-state-active');
        });
    };
    
    ctor.prototype.road = function() {
        this.__map.setView({mapTypeId: Microsoft.Maps.MapTypeId.road});
    };
    
    ctor.prototype.aerial = function() {
        this.__map.setView({mapTypeId: Microsoft.Maps.MapTypeId.aerial});
    };

    return ctor;
})();