var nx = nx || {};
nx.map = nx.map || {};
nx.map.marker = nx.map.marker || {};

nx.map.marker.RouteMarker = (function() {
    function ctor(data) {
        nx.map.marker.Marker.call(this, data);
        
    };

    /** Extend nx.map.marker.Marker */
    ctor.prototype = new nx.map.marker.Marker();

    /**
     *  Return a Microsoft.Maps.Pushpin for this marker.
     *  Used to add the marker to the map
     */
    ctor.prototype.getPushPin = function() {
    	
    	return new Microsoft.Maps.Polyline(this._data, {
    		'strokeColor' : 'red',
    		'strokeThickness': 3
    	});
    };
    
    /**
     * return a Microsoft.Maps.Infobox for this marker.
     * Used to create the marker popup
     */
    ctor.prototype.getPopup = function() {
    	return null;
    };
    
    /**
     * Initialise the marker behavior.
     * Could be override 
     * Default behavior is show the popup on pushpin hover.
     */
    ctor.prototype.initMarker = function(pushpin, popup) {
    	// DO nothing
    };

    return ctor;
})();


nx.map.marker.RouteMarkerProvider = (function() {
    function ctor(providerDesc) {
        nx.map.marker.MarkerProvider.call(this, providerDesc);
    };

    /** Extend nx.unit.diagram.Provider */
    ctor.prototype = new nx.map.marker.MarkerProvider();
    
    /** Return the div for the legend of this kind of marker */
    ctor.prototype.getLegend = function() {
        return "";
    };
    
    ctor.prototype.setRoute = function(route) {
        this.__route = route;
    };
    
    /** Should return an array of markers*/ 
    ctor.prototype.getMarkers = function(targetBounds, zoom, time, callback) {
    	var markers = []; 
    	
         if(!!this.__route) {
        	 
        	 for(var r in this.__route) {
        		 
        		 var route = this.__route[r];
        		 
        		 var trackCoords = new Array();
        		 var i = 0;
        		 
        		 for(var coordinate in route.coordinate){
                     var coord = route.coordinate[coordinate];
                     trackCoords[i] = new Microsoft.Maps.Location(coord.latitude,coord.longitude);
                     i++;
                 }
        		 markers.push(new nx.map.marker.RouteMarker(trackCoords));
            	 
             }
        	 
         	callback(markers);
         }
         
        
    	
    };

    return ctor;
})();

