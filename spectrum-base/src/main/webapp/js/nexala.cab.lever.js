var nx = nx || {};
nx.cab = nx.cab || {};

nx.cab.Lever = (function () {

    var COLOR1 = "#eabe3f";
    var COLOR2 = "#e8cf8a";
    var BORDER = "#666";
    var BLACK = "#000";
    var SECTIONS = 20;

    var ctor = function (el, component, settings) {
        nx.cab.Widget.call(this);
        this._component = component;
        this.settings = $.extend({
            values: ['3', '2', '1']
        }, settings);

        this._handle = null;
        this._height = component.height;
        this._width = component.width;
        this._paper = el;
        this._draw();
        this._handle.hide();
        this._defaultPosition = 0;
    };
    
    ctor.prototype = new nx.cab.Widget();
    
    ctor.prototype._draw = function() {
        var compXOffset = this._component.x;
        var compYOffset = this._component.y;
        
        var w2 = this._width / 2;
        var w3 = this._width / 3;
        var w4 = this._width / 4;
        var r = w4 - 2;

        var offset = (this._width / 2.5 - r);
        var yoffset = offset + 4;
        var runnerWidth = this._width / 2.5 - offset;
        var labelsWidth = this._width - runnerWidth - 14;
        var height = this._height; 

        var runner = this._paper.rect(compXOffset+offset, compYOffset, runnerWidth, height).attr({
            fill: '90-#333-#222-#333',
            stroke: '#000',
            'stroke-width': 3
        });
            
        var labels = this._paper.rect(
                compXOffset+offset + runnerWidth + 2, 
                compYOffset, 
                labelsWidth, 
                height).attr({
            fill: '90-#444-#777',
            stroke: 'none',
            'stroke-width': 0
        });

        var values = this.settings.values;
        var usableHeight = height - yoffset * 2;
        
        var path = "";

        for (var i = 0, l = values.length; i < l; i++) {
            var value = values[i];
            var x = compXOffset+offset + runnerWidth + 2;
            var y = compYOffset+yoffset + usableHeight - (i * usableHeight) / (l - 1);

            path += ['M', x, y, 'L', x + 12, y].join(',');
            
            var lbl = jQuery('<div/>', {
                css: {
                    left: (x + 15)+'px',
                    top:  (y-7)+'px',
                    color: 'black'
                },

                text: value
            }).addClass('valueLabel');

            this._paper.container().append(lbl);

        }
        
        this._paper.path(path).attr({
            stroke: '#000',
            'stroke-width': 2,
            'text-anchor': 'start'
        });

        this._xoffset = offset + runnerWidth / 2;
        this._yoffset = yoffset;
        this._height = usableHeight;

        this._drawHandle(0, r);
    };

    ctor.prototype._drawHandle = function(v, r) {
        var compXOffset = this._component.x;
        var compYOffset = this._component.y;
        var x = this._xoffset+ compXOffset;
        var y = this._yoffset+compYOffset + this._height -(this._height / (this.settings.values.length - 1)) * v;

        this._handle = this._paper.set(
            this._paper.ellipse(x, y + r/6, r-r/9, r).attr({
                fill: '#000',
                opacity: 0.4
            }),

            this._paper.circle(x, y, r).attr({
                fill: 'r#000-#333',
                stroke: '#000',
                'stroke-width': '1'
            }),
            this._paper.ellipse(x, y, r - r/5, r - r/20).attr({
                stroke: 'none',
                fill: 'r(.5, .1)#999-#999',
                opacity: 0
            }),

            this._paper.circle(x, y, r / 1.8).attr({
                fill: '#222',
                'stroke-width': 0
            }),

            this._paper.circle(x, y, r / 1.9).attr({
                fill: 'black',
                'stroke-width': 0
            }),

            this._text = this._paper.text(x, y, "").attr({
                'text-anchor': 'center',
                'fill': '#ffffff',
                'font-size': 13,
                'font-weight': 'bold'
            })
        );
        // XXX need to add default position?
        this._updateLabel(0);
    };
    
    ctor.prototype._updateLabel = function(position) {
        var textLabel = ('' + this.settings.values[position]).substr(0, 1);
        
        this._handle[5].attr({
            text: textLabel
        });
    };
    
    ctor.prototype._update = function(channelName, channelValue, channelCategory) {
        var cy = this._component.y+this._yoffset + this._height - (this._height / (this.settings.values.length - 1)) * channelValue;
        
        if (channelValue == null) {
            this._handle.hide();
        } else {
            this._handle.show();
            this._handle.attr({
                cy: cy,
                y: cy
            });
            this._updateLabel(channelValue);
        }
    };
    
    ctor.prototype._reset = function() {
        this._handle.hide();
    }

    return ctor;
}());
