/*
 * Copyright (c) Nexala Technologies 2010, All rights reserved.
 */
package com.nexala.spectrum.channel;

/**
 * Categorizable
 * 
 * @author Paul O'Riordan
 * @created 11 Nov 2010
 */
public interface Categorizable {
    public void setCategory(ChannelCategory category);

    public ChannelCategory getCategory();
}
