/*
 * Copyright (c) Nexala Technologies 2010, All rights reserved.
 */


package com.nexala.spectrum.channel;

public interface ChannelRecord {
    public ChannelCollection getChannelCollection();
    public void setChannelCollection(ChannelCollection collection);
    
    public Long getTimestamp();
    public void setTimestamp(Long timestamp);
}
