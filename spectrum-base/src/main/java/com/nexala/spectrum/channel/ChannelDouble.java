/*
 * Copyright (c) Nexala Technologies 2010, All rights reserved.
 */
package com.nexala.spectrum.channel;

import java.text.DecimalFormat;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class ChannelDouble extends Channel<Double> {
    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -7186910637764691627L;

    private Double value;

    public ChannelDouble(String name) {
        super(name);
    }

    public ChannelDouble(String name, Double value) {
        super(name);
        this.value = value;
    }

    public ChannelDouble(String name, Double value, ChannelCategory category) {
        super(name);
        this.value = value;
        this.setCategory(category);
    }

    @Override
    public Double getValue() {
        return value;
    }

    @Override
    public void setValue(Double value) {
        this.value = value;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((this.value == null) ? 0 : this.value.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        ChannelDouble other = (ChannelDouble) obj;
        if (this.value == null) {
            if (other.value != null)
                return false;
        } else if (!this.value.equals(other.value))
            return false;
        return true;
    }

    @Override
    public Double getValueAsDouble() {
        return this.value;
    }

    @Override
    public ChannelDouble copy() {
        ChannelDouble cd = new ChannelDouble(getName());
        cd.setId(getId());
        cd.setValue(getValue());
        cd.setCategory(getCategory());
        return cd;
    }

    @Override
    @JsonIgnore
    public String getStringValue() {
        if (getValue() != null) {
            DecimalFormat df = new DecimalFormat("0");
            df.setMaximumFractionDigits(10);

            return df.format(this.getValue());
        } else {
            return null;
        }
    }

}
