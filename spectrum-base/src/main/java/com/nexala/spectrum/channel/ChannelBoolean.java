/*
 * Copyright (c) Nexala Technologies 2010, All rights reserved.
 */
package com.nexala.spectrum.channel;

/**
 * This class is used to represent a Digital Channel Value.
 * 
 * @author Paul O'Riordan
 * @created 11 Nov 2010
 */
public class ChannelBoolean extends Channel<Boolean> {
    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -7186910637764691627L;

    private Boolean value;

    public ChannelBoolean(String name) {
        super(name);
    }

    public ChannelBoolean(String name, Boolean value) {
        super(name);
        this.value = value;
    }

    public ChannelBoolean(String name, Boolean value, ChannelCategory category) {
        super(name);
        this.value = value;
        this.setCategory(category);
    }

    @Override
    public Boolean getValue() {
        return value;
    }

    @Override
    public void setValue(Boolean value) {
        this.value = value;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result
                + ((this.value == null) ? 0 : this.value.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        ChannelBoolean other = (ChannelBoolean) obj;
        if (this.value == null) {
            if (other.value != null)
                return false;
        } else if (!this.value.equals(other.value))
            return false;
        return true;
    }

    @Override
    public String getStringValue() {
        if (value != null) {
            if (value == true) {
                return "1";
            } else {
                return "0";
            }
        } else {
            return null;
        }
    }

    @Override
    public Double getValueAsDouble() {
        Double doubleValue = null;
        if (this.value != null) {
            doubleValue = this.value ? 1.0 : 0.0;
        }
        return doubleValue;
    }

    public ChannelBoolean copy() {
        ChannelBoolean cb = new ChannelBoolean(getName());
        cb.setId(getId());
        cb.setValue(getValue());
        cb.setCategory(getCategory());
        return cb;
    }
}
