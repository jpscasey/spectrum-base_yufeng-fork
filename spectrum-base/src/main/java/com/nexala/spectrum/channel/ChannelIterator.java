/*
 * Copyright (c) Nexala Technologies 2010, All rights reserved.
 */
package com.nexala.spectrum.channel;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

public final class ChannelIterator implements Iterator<Channel<?>> {

    private Iterator<Entry<String, Channel<?>>> iterator;

    public ChannelIterator(Map<String, Channel<?>> channels) {
        iterator = channels.entrySet().iterator();
    }

    public boolean hasNext() {
        return iterator.hasNext();
    }

    public Channel<?> next() {
        Entry<String, Channel<?>> next = iterator.next();
        return next.getValue();
    }

    public void remove() {
        throw new UnsupportedOperationException();
    }
}
