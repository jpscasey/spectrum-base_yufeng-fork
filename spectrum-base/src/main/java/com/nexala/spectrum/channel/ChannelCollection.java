/*
 * Copyright (c) Nexala Technologies 2010, All rights reserved.
 */
package com.nexala.spectrum.channel;

import java.io.Serializable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.nexala.spectrum.db.beans.ChannelConfig;
import com.nexala.spectrum.rest.serializer.ChannelCollectionSerializer;

@JsonSerialize(using = ChannelCollectionSerializer.class)
public final class ChannelCollection implements Iterable<Channel<?>>,
        Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 120894325999514543L;

    // private static final String NUMBER_CHECK = "^\\d+$";
    // private static final String INVALID_NAME = "Key must not be a number";

    private Map<String, Channel<?>> channels;

    public ChannelCollection() {
        channels = new HashMap<String, Channel<?>>();
    }

    public ChannelCollection(List<Channel<?>> channels) {
        this();
        setChannels(channels);
    }

    public ChannelCollection(Channel<?>... channels) {
        this();
        setChannels(Arrays.asList(channels));
    }

    public void setChannels(List<Channel<?>> channels) {
        this.channels.clear();

        for (Channel<?> c : channels) {
            put(c);
        }
    }

    public Map<String, Channel<?>> getChannels() {
        return channels;
    }

    public ChannelIterator iterator() {
        return new ChannelIterator(this.channels);
    }

    public int size() {
        return this.channels.size();
    }

    public final void put(Channel<?> c) {
        if (c.getId() != -1) {
            this.channels.put(String.valueOf(c.getId()), c);
        } else {
            if (c.getName() != null) {
                // if (c.getName().matches(NUMBER_CHECK)) {
                // throw new IllegalArgumentException(INVALID_NAME);
                // }

                this.channels.put(c.getName(), c);
            }
        }
    }

    public void remove(int id) {
        this.channels.remove(String.valueOf(id));
    }

    /**
     * Removes a channel from the ChannelCollection
     * 
     * @param channel
     *            The name of the Channel, i.e. Channel.getName
     */
    public void remove(String cName) {
        this.channels.remove(cName);
    }

    /**
     * Gets a channel by its ID (Channel.ID) from the ChannelCollection
     * 
     * @param <T>
     * @param id
     *            the channel ID
     * @param clazz
     * @throws RuntimeException
     *             if class is not of the specified type.
     * @return a Channel<?> object. Null if key is not present in map
     */
    public <T extends Channel<?>> T getById(int id, Class<T> clazz) {
        return doGet(String.valueOf(id), clazz);
    }

    public final Channel<?> getById(int id) {
        return channels.get(String.valueOf(id));
    }

    public Channel<?> getByChannel(Channel<?> channel) {
        if (channel.getId() > -1) {
            return getById(channel.getId());
        } else {
            return getByName(channel.getName());
        }
    }

    public Channel<?> getByConfig(ChannelConfig config) {
        if (config.getId() != null) {
            return getById(config.getId());
        } else {
            return getByName(config.getName());
        }
    }

    /**
     * Gets a channel by its name (Channel.Header) from the ChannelCollection
     * 
     * @param <T>
     * @param name
     *            the channel ID
     * @param clazz
     * @throws RuntimeException
     *             if class is not of the specified type.
     * @return a Channel<?> object. Null if key is not present in map
     */
    public <T extends Channel<?>> T getByName(String name, Class<T> clazz) {
        // if (name != null && name.matches(NUMBER_CHECK)) {
        // throw new IllegalArgumentException(INVALID_NAME);
        // }

        return doGet(name, clazz);
    }

    public final Channel<?> getByName(String name) {
        return channels.get(name);
    }

    /**
     * Gets a channel by its short name (ColXXX) from the ChannelCollection
     * 
     * @param <T>
     * @param key
     * @param clazz
     * @throws RuntimeException
     *             if class is not of the specified type.
     * @return a Channel<?> object. Null if key is not present in map
     */
    @SuppressWarnings("unchecked")
    private <T extends Channel<?>> T doGet(String key, Class<T> clazz) {
        Channel<?> channel = channels.get(key);

        if (channel == null) {
            return null;
        }

        if (channel.getClass() == clazz) {
            // Warning is suppressed here, this is safe
            // because of the explicit check above.
            return (T) channel;
        } else {
            throw new RuntimeException("Type mismatch.");
        }
    }
    
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        Map<String, Channel<?>> sorted = new TreeMap<String, Channel<?>>(channels);
        
        for(Entry<String, Channel<?>> kvp : sorted.entrySet()) {
            sb.append(kvp.getKey()).append(" : ").append(kvp.getValue()).append(System.getProperty("line.separator"));
        }
        
        return sb.toString();
    }
}
