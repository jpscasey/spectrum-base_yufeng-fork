
/*
 * Copyright (c) Nexala Technologies 2011, All rights reserved.
 */

package com.nexala.spectrum.channel;

public interface CloneableData extends Cloneable {
    public Object clone() throws CloneNotSupportedException;
}
