/*
 * Copyright (c) Nexala Technologies 2010, All rights reserved.
 */
package com.nexala.spectrum.channel;

public final class ChannelLong extends Channel<Long> {
    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 6884118085859947537L;

    private Long value;

    public ChannelLong(String name) {
        super(name);
    }

    public ChannelLong(String name, Long value) {
        super(name);
        this.value = value;
    }

    public ChannelLong(String name, Long value, ChannelCategory category) {
        super(name);
        this.value = value;
        super.setCategory(category);
    }
    
    @Override
    public Long getValue() {
        return value;
    }

    @Override
    public void setValue(Long value) {
        this.value = value;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result
                + ((this.value == null) ? 0 : this.value.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        ChannelLong other = (ChannelLong) obj;
        if (this.value == null) {
            if (other.value != null)
                return false;
        } else if (!this.value.equals(other.value))
            return false;
        return true;
    }

    public Double getValueAsDouble() {
        Double doubleValue = null;
        if (this.value != null) {
            doubleValue = Double.valueOf(this.value);
        }
        return doubleValue;
    }

    public ChannelLong copy() {
        ChannelLong cl = new ChannelLong(getName());
        cl.setId(getId());
        cl.setValue(getValue());
        cl.setCategory(getCategory());
        return cl;
    }
}
