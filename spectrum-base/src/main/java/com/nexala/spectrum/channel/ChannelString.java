/*
 * Copyright (c) Nexala Technologies 2010, All rights reserved.
 */
package com.nexala.spectrum.channel;

public final class ChannelString extends Channel<String> {
    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 4958715552336858952L;

    private String value;

    public ChannelString(String name) {
        super(name);
    }

    public ChannelString(String name, String value) {
        super(name);
        this.value = value;
    }

    public ChannelString(String name, String value, ChannelCategory category) {
        super(name);
        this.value = value;
        this.setCategory(category);
    }
    
    // @Override
    public String getValue() {
        return value;
    }

    // @Override
    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result
                + ((this.value == null) ? 0 : this.value.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        ChannelString other = (ChannelString) obj;
        if (this.value == null) {
            if (other.value != null)
                return false;
        } else if (!this.value.equals(other.value))
            return false;
        return true;
    }

    public Double getValueAsDouble() {
        Double doubleValue = null;
        if (this.value != null && this.value.trim().length() != 0) {
            doubleValue = Double.parseDouble(this.value);
        }
        return doubleValue;
    }

    public ChannelString copy() {
        ChannelString cs = new ChannelString(getName());
        cs.setId(getId());
        cs.setValue(getValue());
        cs.setCategory(getCategory());
        return cs;
    }
}