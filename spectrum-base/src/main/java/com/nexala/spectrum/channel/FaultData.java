/*
 * Copyright (c) Nexala Technologies 2010, All rights reserved.
 */
package com.nexala.spectrum.channel;

public interface FaultData extends Comparable<FaultData> {
	public String getId();
    public Long getTimestamp();
    public Boolean getClone();
    public void setClone(Boolean clone);
    public void setId(String id);
    public void setTimestamp(Long time);
}