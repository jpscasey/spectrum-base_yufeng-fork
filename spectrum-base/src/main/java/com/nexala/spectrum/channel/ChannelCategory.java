/*
 * Copyright (c) Nexala Technologies 2010, All rights reserved.
 */
package com.nexala.spectrum.channel;

public enum ChannelCategory {
    INVALID(-1),
    NODATA(0),
    NORMAL(1),
    ON(2),
    OFF(3),
    WARNING(4),
    WARNING1(5),
    WARNING2(6),
    FAULT(7),
    FAULT1(8),
    FAULT2(9),
    EVENT(10),
    EVENT_AP2(11),
    EVENT_AP1(12),
    EVENT_P2(13),
    EVENT_P1(14);

    private final int severity;

    ChannelCategory(int severity) {
        this.severity = severity;
    }

    public int getSeverity() {
        return this.severity;
    }

};