package com.nexala.spectrum.validation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import com.google.inject.Inject;
import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.configuration.ConfigurationManager;
import com.nexala.spectrum.db.beans.ChannelConfig;

public class ChannelRuleCache {
    private final Logger LOGGER = Logger.getLogger(ChannelRuleCache.class);

    private final ChannelRuleDao ruleDao;
    private final Long refreshInterval;
    private long lastUpdateCheck = 0;

    /** List of ALL rules */
    private List<ChannelRule> channelRules;

    /** List of rules by channel id (Channel ID -> Rules) */
    private Map<Integer, List<ChannelRule>> channelConfigRules;

    /** Map of channel dependencies (Channel ID -> Channel Config Set) */
    private Map<Integer, Set<ChannelConfig>> channelConfigDependencies;

    @Inject
    public ChannelRuleCache(ChannelRuleDao ruleDao, ConfigurationManager conf) {
        this.ruleDao = ruleDao;

        refreshInterval = conf.getConfAsLong(Spectrum.SPECTRUM_REFRESH_VALIDATIONRULES_INTERVAL);

        loadRules();

        this.lastUpdateCheck = System.currentTimeMillis();
    }

    public Map<Integer, Set<ChannelConfig>> getDependencies() {
        return channelConfigDependencies;
    }

    public synchronized Map<Integer, List<ChannelRule>> getRules() {
        long timeout = System.currentTimeMillis() - lastUpdateCheck;

        if (refreshInterval != null && refreshInterval >= 0 && timeout >= refreshInterval) {

            loadRules();

            this.lastUpdateCheck = System.currentTimeMillis();
        }

        return new HashMap<Integer, List<ChannelRule>>(channelConfigRules);
    }

    private void loadRules() {
        long startTime = System.currentTimeMillis();

        channelRules = ruleDao.findAll();

        channelConfigRules = new HashMap<Integer, List<ChannelRule>>();

        for (ChannelRule rule : channelRules) {
            ChannelConfig channelConfig = rule.getChannel();

            if (channelConfig == null) {
                continue;
            }

            if (channelConfigRules.get(channelConfig.getId()) == null) {
                channelConfigRules.put(channelConfig.getId(), new ArrayList<ChannelRule>());
            }

            channelConfigRules.get(channelConfig.getId()).add(rule);
        }

        loadDependencies();

        LOGGER.info("Loaded " + channelRules.size() + " channel validation rules: "
                + (System.currentTimeMillis() - startTime) + "ms");
    }

    private void loadDependencies() {
        channelConfigDependencies = new HashMap<Integer, Set<ChannelConfig>>();

        for (Integer channelId : channelConfigRules.keySet()) {
            loadChannelDependencies(channelId, channelConfigRules.get(channelId), channelConfigDependencies);
        }
    }

    private void loadChannelDependencies(Integer channelId, List<ChannelRule> rules,
    		Map<Integer, Set<ChannelConfig>> dependencies) {

    	if (dependencies.get(channelId) == null) {
    		dependencies.put(channelId, new HashSet<ChannelConfig>());
    	}

    	for (ChannelRule rule : rules) {
    		if (rule.getChannel().isAlwaysDisplayed()) {
	    		for (ChannelRuleValidation validation : rule.getValidations()) {
	    			dependencies.get(channelId).add(validation.getChannel());
	    		}
    		}
    	}
    }
}
