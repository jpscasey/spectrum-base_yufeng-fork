package com.nexala.spectrum.validation;

import java.util.ArrayList;
import java.util.List;

import com.nexala.spectrum.db.beans.ChannelConfig;

public class ChannelRule {
    private int id;
    private String name;
    private ChannelConfig channel;
    private ChannelStatus status;
    private boolean active;
    private List<ChannelRuleValidation> validations = new ArrayList<ChannelRuleValidation>();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ChannelConfig getChannel() {
        return channel;
    }

    public void setChannel(ChannelConfig channel) {
        this.channel = channel;
    }

    public ChannelStatus getStatus() {
        return status;
    }

    public void setStatus(ChannelStatus status) {
        this.status = status;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public List<ChannelRuleValidation> getValidations() {
        return validations;
    }

    public void setValidations(List<ChannelRuleValidation> validations) {
        this.validations = validations;
    }

    public void addValidation(ChannelRuleValidation validation) {
        validations.add(validation);
    }
}
