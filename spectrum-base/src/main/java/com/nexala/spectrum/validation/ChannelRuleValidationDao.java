/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.validation;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.google.inject.Inject;
import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.configuration.ChannelConfiguration;
import com.nexala.spectrum.db.JdbcRowMapper;
import com.nexala.spectrum.db.QueryManager;
import com.nexala.spectrum.db.dao.BoxedResultSet;
import com.nexala.spectrum.db.dao.BoxedResultSetFactory;
import com.nexala.spectrum.db.dao.DataAccessException;
import com.nexala.spectrum.db.dao.GenericDao;

public class ChannelRuleValidationDao extends GenericDao<ChannelRuleValidation, Long> {

    private static class ChannelRuleValidationMapper extends JdbcRowMapper<ChannelRuleValidation> {

        @Inject
        private ChannelConfiguration channelConfiguration;

        @Inject
        private BoxedResultSetFactory factory;

        @Override
        public final ChannelRuleValidation createRow(ResultSet rs) throws SQLException {

            BoxedResultSet boxedRs = factory.create(rs);

            ChannelRuleValidation record = new ChannelRuleValidation();

            record.setRuleId(boxedRs.getInt("ChannelRuleID"));
            record.setChannel(channelConfiguration.getConfig(boxedRs.getInt("ChannelID")));
            record.setMinValue(boxedRs.getDouble("MinValue"));
            record.setMaxValue(boxedRs.getDouble("MaxValue"));
            record.setMinInclusive(boxedRs.getBoolean("MinInclusive"));
            record.setMaxInclusive(boxedRs.getBoolean("MaxInclusive"));

            return record;
        }
    }

    private final ChannelRuleValidationMapper mapper;

    @Inject
    public ChannelRuleValidationDao(ChannelRuleValidationMapper mapper) {
        this.mapper = mapper;
    }

    public List<ChannelRuleValidation> findAll() throws DataAccessException {
        String query = new QueryManager().getQuery(Spectrum.CHANNEL_RULE_VALIDATION_QUERY);

        return findByQuery(query, mapper);
    }

    public List<ChannelRuleValidation> findByChannelId(int channelId) throws DataAccessException {
        String query = new QueryManager().getQuery(Spectrum.CHANNEL_RULE_VALIDATION_CHANNELID_QUERY);
        return findByQuery(query, mapper, channelId);
    }
}