/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.validation;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.google.inject.Inject;
import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.db.JdbcRowMapper;
import com.nexala.spectrum.db.QueryManager;
import com.nexala.spectrum.db.dao.BoxedResultSet;
import com.nexala.spectrum.db.dao.BoxedResultSetFactory;
import com.nexala.spectrum.db.dao.DataAccessException;
import com.nexala.spectrum.db.dao.GenericDao;

public class ChannelStatusDao extends GenericDao<ChannelStatus, Long> {
    private static class ChannelStatusMapper extends JdbcRowMapper<ChannelStatus> {

        @Inject
        private BoxedResultSetFactory factory;

        @Override
        public final ChannelStatus createRow(ResultSet results) throws SQLException {
            BoxedResultSet rs = factory.create(results);
            return new ChannelStatus(rs.getInt("ID"), rs.getString("Name"), rs.getInt("Priority"));
        }
    }

    @Inject
    private ChannelStatusMapper mapper;

    public List<ChannelStatus> findAll() throws DataAccessException {
        String query = new QueryManager().getQuery(Spectrum.CHANNEL_STATUS_QUERY);
        return this.findByQuery(query, mapper);
    }
}