package com.nexala.spectrum.validation;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.nexala.spectrum.db.beans.ChannelConfig;

public class ChannelRuleValidation {
    private int ruleId;
    private ChannelRule rule;
    private ChannelConfig channel;
    private Double minValue;
    private Double maxValue;
    private boolean minInclusive;
    private boolean maxInclusive;

    @JsonIgnore
    public ChannelRule getRule() {
        return rule;
    }

    public void setRule(ChannelRule rule) {
        this.rule = rule;
    }

    public ChannelConfig getChannel() {
        return channel;
    }

    public void setChannel(ChannelConfig channel) {
        this.channel = channel;
    }

    public Double getMinValue() {
        return minValue;
    }

    public void setMinValue(Double minValue) {
        this.minValue = minValue;
    }

    public Double getMaxValue() {
        return maxValue;
    }

    public void setMaxValue(Double maxValue) {
        this.maxValue = maxValue;
    }

    public boolean isMinInclusive() {
        return minInclusive;
    }

    public void setMinInclusive(boolean minInclusive) {
        this.minInclusive = minInclusive;
    }

    public boolean isMaxInclusive() {
        return maxInclusive;
    }

    public void setMaxInclusive(boolean maxInclusive) {
        this.maxInclusive = maxInclusive;
    }

    public int getRuleId() {
        return ruleId;
    }

    public void setRuleId(int ruleId) {
        this.ruleId = ruleId;
    }
}
