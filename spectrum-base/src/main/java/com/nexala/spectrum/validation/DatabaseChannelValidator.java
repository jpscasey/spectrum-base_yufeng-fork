package com.nexala.spectrum.validation;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.nexala.spectrum.channel.Channel;
import com.nexala.spectrum.channel.ChannelCategory;
import com.nexala.spectrum.channel.ChannelCollection;
import com.nexala.spectrum.configuration.ChannelConfiguration;
import com.nexala.spectrum.db.beans.ChannelConfig;
import com.nexala.spectrum.db.dao.charting.ChannelType;
import com.nexala.spectrum.rest.data.beans.DataSet;

/**
 * 
 * @author poriordan
 */
@Singleton
public class DatabaseChannelValidator implements ChannelValidator {
    private final Logger LOGGER = Logger.getLogger(DatabaseChannelValidator.class);

    private final int STATUS_NO_DATA = 0;
    private final int STATUS_ALL_TRUE = 1;
    private final int STATUS_NOT_ALL_TRUE = 2;

    @Inject
    private ChannelRuleCache channelRuleCache;

    @Inject
    private ChannelStatusUtil channelStatusUtil;

    @Inject
    private ChannelConfiguration channelConfiguration;

    public Set<ChannelConfig> getChannelDependencies(List<ChannelConfig> configs) {
        Set<ChannelConfig> dependencies = new HashSet<ChannelConfig>();

        for (ChannelConfig config : configs) {
            Set<ChannelConfig> channelDependencies = getChannelDependencies(config);
            if (channelDependencies != null) {
                dependencies.addAll(channelDependencies);
            }
        }

        return dependencies;
    }

    public Set<ChannelConfig> getChannelDependencies(ChannelConfig config) {
        return channelRuleCache.getDependencies().get(config.getId());
    }

    public void validate(List<DataSet> data) {
        long startTime = System.currentTimeMillis();

        Map<Integer, List<ChannelRule>> rules = channelRuleCache.getRules();

        if (rules != null && rules.size() > 0) {
            for (DataSet ds : data) {
                validate(rules, ds.getChannels());
            }
        }

        long ms = System.currentTimeMillis() - startTime;
        if (ms > 500) {
            LOGGER.info("Slow channel rules validation: " + ms + "ms");
        }
    }

    private void validate(Map<Integer, List<ChannelRule>> channelConfigRules, ChannelCollection cc) {
        for (Channel<?> channel : cc.getChannels().values()) {
            //only validate channels from channel table
            ChannelConfig channelConfig = channelConfiguration.getConfig(channel);
            if (channelConfig == null) {
                continue;
            }

            if (channel.getValue() == null) {
                channel.setCategory(ChannelCategory.NODATA);
                continue;
            }

            List<ChannelRule> rules = channelConfigRules.get(channel.getId());

            if (rules == null || rules.size() == 0) {
                continue;
            }

            boolean isDefaultCat = true;

            for (ChannelRule rule : rules) {
                List<ChannelRuleValidation> validations = rule.getValidations();

                if (validations.size() > 0) {
                    int status = getValidationStatus(validations, cc);

                    if (status == STATUS_NO_DATA) {
                        channel.setCategory(ChannelCategory.NODATA);
                    } else if (status == STATUS_ALL_TRUE) {
                        ChannelCategory newCat = channelStatusUtil.getChannelCategory(rule.getStatus()); 
                        if (channel.getCategory() != null && newCat != null && !isDefaultCat) {
                            int newPriority = channelStatusUtil.getChannelCategoryPriority(newCat);
                            int currentPriority = channelStatusUtil.getChannelCategoryPriority(channel.getCategory());
                            if (newPriority > currentPriority) {
                                channel.setCategory(newCat);
                            }
                        } else if (newCat != null && isDefaultCat) {
                            channel.setCategory(newCat);
                            isDefaultCat = false;
                        }
                    }
                }
            }
        }
    }

    // FIXME - this should return a ValidationStatus enum
    private int getValidationStatus(List<ChannelRuleValidation> validations, ChannelCollection cc) {
        int trueCount = 0;

        for (ChannelRuleValidation validation : validations) {
            ChannelConfig channel = validation.getChannel();
            Channel<?> data = cc.getById(channel.getId());

            if (data == null || data.getValue() == null) {
                if (data == null) {
                    LOGGER.warn("Channel validation dependency not found for rule '" + validation.getRule().getName()
                            + "', rule channel '" + validation.getRule().getChannel().getName()
                            + "', validation channel '" + validation.getChannel().getName() + "'");
                }

                return STATUS_NO_DATA;
            }

            Double min = validation.getMinValue();
            Double max = validation.getMaxValue();
            Boolean minInclusive = validation.isMinInclusive();
            Boolean maxInclusive = validation.isMaxInclusive();

            ChannelType type = channel.getType();
            
            if (min == null && max == null) {
                LOGGER.warn("Ignoring channel rule '" + validation.getRule().getName() + "', validation channel '"
                        + validation.getChannel().getName()
                        + "'. 'MinValue' AND 'MaxValue' cannot be both equal to NULL");

                continue;
            }

            if (type == ChannelType.DIGITAL) {

                if ((min != null && (min < 0 || min > 1)) || (max != null && (max < 0 || max > 1))) {
                    LOGGER.warn("Ignoring channel rule " + validation.getRule().getName() + "', validation channel '"
                            + validation.getChannel().getName() + "'. DIGITAL channel types must have 'MinValue' "
                            + "and 'MaxValue' between 0 and 1");
                    continue;
                }

                Boolean value;
                Object dbValue = data.getValue();
                if(dbValue instanceof Boolean) {
                	value = (Boolean) data.getValue();
                } else {
                	if(dbValue.equals(1)) { // pivot table for event channels returns int instead of boolean
                    	value = true;
                    } else {
                    	value = false;
                    }
                }
                
                if ((value && (max == null || (max == 1 && maxInclusive)))
                        || (!value && (min == null || (min == 0 && minInclusive)))) {
                    trueCount++;
                }
            } else if (type == ChannelType.ANALOG) {
                
                Double value = data.getValueAsDouble();

                boolean minTrue = false;
                if (min != null
                        && ((minInclusive && value >= min) || (!minInclusive && value > min))) {

                    minTrue = true;
                }

                boolean maxTrue = false;
                if (max != null
                        && ((maxInclusive && value <= max) || (!maxInclusive && value < max))) {

                    maxTrue = true;
                }

                if ((min == null && maxTrue) || (max == null && minTrue) || (minTrue && maxTrue)) {
                    trueCount++;
                }
            }
        }

        if (validations.size() == trueCount) {
            return STATUS_ALL_TRUE;
        }

        return STATUS_NOT_ALL_TRUE;
    }
}
