/*
 * Copyright (c) Nexala Technologies 2013, All rights reserved.
 */
package com.nexala.spectrum.validation;

import java.util.List;
import java.util.Set;

import com.google.inject.ImplementedBy;
import com.nexala.spectrum.db.beans.ChannelConfig;
import com.nexala.spectrum.rest.data.beans.DataSet;

@ImplementedBy(DatabaseChannelValidator.class)
public interface ChannelValidator {
    /**
     * Given a list of ChannelConfig objects - returns a set of ChannelConfigs
     * which are required in order to validate these channels.
     * 
     * @param configs
     * @return
     */
    Set<ChannelConfig> getChannelDependencies(List<ChannelConfig> configs);

    /**
     * Given a Channel config object - returns a set of ChannelConfigs, which
     * are required in order to validate this channel.
     * @param config
     * @return
     */
    Set<ChannelConfig> getChannelDependencies(ChannelConfig config);

    /**
     * Validates a DataSet.
     * @param data
     */
    void validate(List<DataSet> data);
}
