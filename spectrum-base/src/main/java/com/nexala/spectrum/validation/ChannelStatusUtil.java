package com.nexala.spectrum.validation;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.nexala.spectrum.channel.ChannelCategory;

@Singleton
public class ChannelStatusUtil {
    private Map<Integer, ChannelStatus> channelStatusIdMap = Collections
            .synchronizedMap(new HashMap<Integer, ChannelStatus>());

    private Map<ChannelCategory, Integer> prioritisedCategories = Collections
            .synchronizedMap(new HashMap<ChannelCategory, Integer>());

    @Inject
    public ChannelStatusUtil(ChannelStatusDao dao) {
        List<ChannelStatus> list = dao.findAll();

        for (ChannelStatus cs : list) {
            channelStatusIdMap.put(cs.getId(), cs);
            prioritisedCategories.put(getChannelCategory(cs), cs.getPriority());
        }
    }

    public ChannelStatus getChannelStatusById(int id) {
        return channelStatusIdMap.get(id);
    }

    public ChannelCategory getChannelCategory(ChannelStatus channelStatus) {
        ChannelCategory category = null;

        for (ChannelCategory cat : ChannelCategory.values()) {
            if (cat.getSeverity() == channelStatus.getId()) {
                category = cat;
                break;
            }
        }

        return category;
    }

    public int getChannelCategoryPriority(ChannelCategory category) {
        // TODO : add proper code to prevent exception if the ChannelStatus
        // table
        // isn't populated. The ChannelStatus table must be ALWAYS populated,
        // it's used by the channel validation process.
        return prioritisedCategories.get(category);
    }
}
