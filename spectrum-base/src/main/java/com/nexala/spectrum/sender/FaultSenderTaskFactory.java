package com.nexala.spectrum.sender;

import com.google.inject.assistedinject.Assisted;

public interface FaultSenderTaskFactory {
    FaultSenderTask create(@Assisted("name") String name);
}
