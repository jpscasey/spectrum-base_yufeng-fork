package com.nexala.spectrum.sender;

import com.google.inject.Inject;
import com.nexala.spectrum.scheduling.ScheduledTask;
import com.nexala.spectrum.scheduling.TaskFactory;

public class FaultSenderSimpleTaskFactory implements TaskFactory {

    @Inject
    FaultSenderTaskFactory factory;
    
    @Override
    public ScheduledTask getTask(String name) {
        return factory.create(name);
    }

}
