package com.nexala.spectrum.sender;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.configuration.ConfigurationManager;
import com.nexala.spectrum.db.beans.E2mResponse;
import com.nexala.spectrum.db.beans.FaultTransmission;
import com.nexala.spectrum.scheduling.DefaultScheduledTask;

public class FaultSenderTask extends DefaultScheduledTask {

    @Inject
    private FaultSenderProvider faultSenderProvider;

    @Inject
    private ConfigurationManager confManager;

    private final Logger logger = Logger.getLogger(FaultSenderTask.class);
    
    @Override
    public void run(long timestamp) {
        List<FaultTransmission> data = faultSenderProvider.searchData();
        for (FaultTransmission fault : data) {
            E2mResponse response = sendToE2m(fault);
            boolean successful = response.isSuccessful();
            String message = response.getMessage();
            faultSenderProvider.updateFaultTransmission(fault.getTransmissionId(), message, successful);
        }
    }

    public E2mResponse sendToE2m(FaultTransmission fault) {

        E2mResponse result = null;

        URI uri = null;

        try {
            try {
                uri = new URIBuilder()
                .setScheme("http")
                .setHost(confManager.getConfAsString(Spectrum.SPECTRUM_FAULT_SENDER_E2M_IP))
                .setPath(confManager.getConfAsString(Spectrum.SPECTRUM_FAULT_SENDER_E2M_PATH))
                .setParameter("assetSerialNo", fault.getUnitNumber())
                .setParameter("coachSerialNo", fault.getVehicleNumber())
                .setParameter("locationCode", fault.getLocationCode())
                .setParameter("locationName", fault.getLocationName())
                .setParameter("faultCode", fault.getE2mFaultCode())
                .setParameter("priorityCode", fault.getE2mPriorityCode())
                .setParameter("notes", fault.getNotes())
                .build();
            } catch (URISyntaxException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            
            HttpClient httpclient = new DefaultHttpClient();
            HttpGet httpget = new HttpGet(uri);
            HttpResponse response = httpclient.execute(httpget);

            HttpEntity entity = response.getEntity();
            String responseString = EntityUtils.toString(entity);
            
            ObjectMapper mapper = new ObjectMapper();
            result = mapper.readValue(responseString, new TypeReference<E2mResponse>(){});
            
        } catch (Exception e) {
            logger.error("Error sending fault " + fault.getFaultId() + " to E2M", e);
            result.setSuccessful(false);
            result.setMessage(e.toString());
        }
        
        return result;
    }

    @Override
    public void destroy() {
        // TODO Auto-generated method stub
        
    }

    @Override
    public int getThreadPriority() {
        return Thread.NORM_PRIORITY;
    }

}
