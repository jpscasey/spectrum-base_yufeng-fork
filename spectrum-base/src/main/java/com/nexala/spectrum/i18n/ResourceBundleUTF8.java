package com.nexala.spectrum.i18n;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;

public class ResourceBundleUTF8 {

    // feature variables
    private ResourceBundle bundle;
    private ResourceBundle bundleProject;
    private Charset fileEncoding;
    
    private final Logger logger = Logger.getLogger(ResourceBundleUTF8.class);

    public ResourceBundleUTF8 (Locale locale, Charset fileEncoding) {
        this.bundle = ResourceBundle.getBundle("i18n.R2mConstants", locale);
        try {
            this.bundleProject = ResourceBundle.getBundle("i18n.R2mConstants-override", locale);
        } catch (MissingResourceException e) {
            this.bundleProject = null;
            logger.warn("No project specific translation file", e);
        }
        
        this.fileEncoding = fileEncoding;
    }

    public ResourceBundleUTF8 (Locale locale) {
        this(locale, StandardCharsets.UTF_8);
    }

    public String getString (String key) {
        String value = null;
        if (bundle.containsKey(key)) {
            value = bundle.getString(key);
        } else if (bundleProject != null && bundleProject.containsKey(key)) {
            value = bundleProject.getString(key);
        }

        if (value != null) {
            value =  new String(value.getBytes(StandardCharsets.ISO_8859_1), fileEncoding);
        }

        return value;
    }
}