package com.nexala.spectrum.db.beans.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.nexala.spectrum.db.JdbcRowMapper;
import com.nexala.spectrum.db.beans.ChartVehicleType;

public class ChartVehicleTypeMapper extends JdbcRowMapper<ChartVehicleType> {
    
    @Override
    public ChartVehicleType createRow(ResultSet rs) throws SQLException {
        ChartVehicleType cv = new ChartVehicleType();
        
        cv.setChartId(rs.getInt("ChartId"));
        cv.setVehicleType(rs.getString("VehicleType"));
        return cv;
    }
}