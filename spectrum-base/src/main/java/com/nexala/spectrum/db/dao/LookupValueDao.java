/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.db.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.google.inject.Inject;
import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.db.JdbcRowMapper;
import com.nexala.spectrum.db.QueryManager;
import com.nexala.spectrum.db.beans.LookupValue;

public class LookupValueDao extends GenericDao<LookupValue, Long> {
    private static class LookupMapper extends JdbcRowMapper<LookupValue> {
        @Inject
        BoxedResultSetFactory factory;
        
        @Override
        public final LookupValue createRow(ResultSet resultSet)
                throws SQLException {

            BoxedResultSet rs = factory.create(resultSet);

            LookupValue record = new LookupValue();
            record.setChannelId(rs.getInt("ChannelID"));
            record.setValue(rs.getString("Value"));
            record.setDisplayValue(rs.getString("DisplayValue"));

            return record;
        }
    }
    
    @Inject
    private LookupMapper mapper;

    public List<LookupValue> findAll() throws DataAccessException {
        String query = new QueryManager()
                .getQuery(Spectrum.LOOKUP_VALUE_CHANNEL_QUERY);
        return this.findByQuery(query, mapper);
    }
}
