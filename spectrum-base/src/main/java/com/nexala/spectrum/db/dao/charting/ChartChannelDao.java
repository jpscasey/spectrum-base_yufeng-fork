package com.nexala.spectrum.db.dao.charting;

import java.util.List;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.db.QueryManager;
import com.nexala.spectrum.db.dao.GenericDao;

@Singleton
public class ChartChannelDao extends GenericDao<ChartChannel, Long> {
    
    @Inject
    private ChartChannelMapper mapper;

    public List<ChartChannel> findByChartId(Long chartId) {
    	List<ChartChannel> channels = null;
    	String query = new QueryManager().getQuery(Spectrum.CHART_CHANNEL_QUERY);
    	channels = this.findByQuery(query, mapper, chartId);

    	return channels;

    }
}