package com.nexala.spectrum.db.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.google.inject.Inject;
import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.db.DbUtil;
import com.nexala.spectrum.db.QueryManager;
import com.nexala.spectrum.db.RowMapper;
import com.nexala.spectrum.db.mappers.DownloadMapper;
import com.nexala.spectrum.rest.data.beans.DownloadInfo;
import com.nexala.spectrum.rest.data.beans.DownloadSearchParameters;
import com.nexala.spectrum.utils.DateUtils;

public class DownloadDao extends GenericDao<DownloadInfo, Long> {

    @Inject
    private DateUtils dateUtils;
    
    private final DownloadMapper mapper;

    @Inject
    public DownloadDao(DownloadMapper mapper) {
        this.mapper = mapper;
    }
    
    
    public void insertDownload(Long vehicleId, int fileType, Long startDate, Long startTime, Long endTime) {
        Connection c = getConnection();
        PreparedStatement ps = null;
        String query = new QueryManager()
                .getQuery(Spectrum.INSERT_DOWNLOAD_STMT);
        try {
            long timestamp = System.currentTimeMillis();
            Timestamp ts = new Timestamp(timestamp);

            ps = c.prepareStatement(query);
            ps.setLong(1, vehicleId);
            ps.setInt(2, 1);
            ps.setTimestamp(3, ts, dateUtils.getDatabaseCalendar());
            ps.setInt(4, fileType);
            ps.setTimestamp(5, startDate != null ? new Timestamp(startDate) : null);
            ps.setTimestamp(6, startTime != null ? new Timestamp(startTime) : null, dateUtils.getDatabaseCalendar());
            ps.setTimestamp(7, endTime 	 != null ? new Timestamp(endTime) : null, dateUtils.getDatabaseCalendar());
            ps.setBoolean(8, true);

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DataAccessException(e);
        } finally {
            DbUtil.closeStatement(ps);
            DbUtil.closeConnection(c);
        }
    }
    
    // TODO make insert generic - requestTypeId and devices are railhead specific
    public void insertDownload(Long vehicleId, int fileType, Long startDate, Long startTime, Long endTime, int requestTypeId, String devices) {
        Connection c = getConnection();
        PreparedStatement ps = null;
        String query = new QueryManager()
                .getQuery(Spectrum.INSERT_DOWNLOAD_STMT);
        try {
            long timestamp = System.currentTimeMillis();
            Timestamp ts = new Timestamp(timestamp);

            ps = c.prepareStatement(query);
            ps.setLong(1, vehicleId);
            ps.setInt(2, 1);
            ps.setTimestamp(3, ts, dateUtils.getDatabaseCalendar());
            ps.setInt(4, fileType);
            ps.setTimestamp(5, startDate != null ? new Timestamp(startDate) : null);
            ps.setTimestamp(6, startTime != null ? new Timestamp(startTime) : null, dateUtils.getDatabaseCalendar());
            ps.setTimestamp(7, endTime 	 != null ? new Timestamp(endTime) : null, dateUtils.getDatabaseCalendar());
            ps.setBoolean(8, true);
            ps.setInt(9, requestTypeId);
            ps.setString(10, devices);

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DataAccessException(e);
        } finally {
            DbUtil.closeStatement(ps);
            DbUtil.closeConnection(c);
        }
    }

    public List<DownloadInfo> searchForDownloads(DownloadSearchParameters param) {
        return searchForDownloads(param, mapper);
    }

    public List<DownloadInfo> searchForDownloads(DownloadSearchParameters param, RowMapper<DownloadInfo> downloadMapper) {

        String query = new QueryManager()
                .getQuery(Spectrum.DOWNLOAD_SEARCH_QUERY);
        StringBuilder sb = new StringBuilder(query);

        List<Object> parameterList = new ArrayList<Object>();

        String unitNumber = param.getUnitNumber();

        if (unitNumber != null && !unitNumber.trim().equals("")) {
            sb.append(" AND UnitNumber = ?");
            parameterList.add(unitNumber);
        }

        String fileType = param.getFileType();

        if (fileType != null && !fileType.trim().equals("")) {
            sb.append(" AND FileType = ?");
            parameterList.add(fileType);
        }

        String vehicle = param.getVehicle();

        if (vehicle != null && !vehicle.trim().equals("")
                && !vehicle.trim().equals("- Select -")) {
            sb.append(" AND VehicleID = ?");
            parameterList.add(vehicle);
        }

        Date dateFrom = param.getDateFrom();
        if (dateFrom != null) {
            parameterList.add(dateFrom);
            sb.append(" AND DateTimeRequested >= ?");
        }

        Date dateTo = param.getDateTo();
        if (dateTo != null) {
            parameterList.add(dateTo);
            sb.append(" AND DateTimeRequested <= ?");
        }

        String fleet = param.getFleet();

        if (fleet != null && !fleet.trim().equals("")) {
            sb.append(" AND Code = ?");
            parameterList.add(fleet);
        }

        sb.append(" ORDER BY DateTimeRequested DESC");

        query = sb.toString();

        List<DownloadInfo> info = findByQuery(query, downloadMapper, parameterList.toArray());



        // Code commented, not sure if we need it for a customer or it is old code
        
        /*
        Iterator<DownloadInfo> it = info.iterator();
        while (it.hasNext()) {
            DownloadInfo record = it.next();
            int vehicleStatus = -1;

            int status = record.getStatus();
            Long requestTime = record.getRequestTimeI();
            Long compTime = record.getCompTimeI();
            Calendar cal = Calendar.getInstance();
            Long currentTime = cal.getTimeInMillis();

            if (compTime == 0L) {

                if (currentTime - requestTime > 172800000) {
                    // red
                    vehicleStatus = 2;
                } else if (currentTime - requestTime > 86400000) {
                    // grey
                    vehicleStatus = 1;
                } else {
                    // none
                    vehicleStatus = 0;
                }

            } else if (status == 0) {
                // green
                vehicleStatus = 3;
            }

            record.setVehicleStatus(vehicleStatus);

        }*/

        return info;
    }

    public boolean currDownloadCheck(String vehicleId, String fileType) {
        String query = null;

        query = new QueryManager()
                .getQuery(Spectrum.DOWNLOAD_CURR_DOWNLOAD_CHECK);
        List<Object> parameterList = new ArrayList<Object>();
        parameterList.add(vehicleId);
        parameterList.add(fileType);
        List<DownloadInfo> results = this.findByQuery(query, mapper,
                parameterList.toArray());
        boolean hasRow = results.size() > 0;
        return hasRow;
    }
}
