package com.nexala.spectrum.db.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.db.JdbcRowMapper;
import com.nexala.spectrum.db.QueryManager;
import com.nexala.spectrum.db.beans.Vehicle;

public class VehicleDao extends GenericDao<Vehicle, Long> {

    private static final JdbcRowMapper<Vehicle> MAPPER = new VehicleMapper();

    private static class VehicleMapper extends JdbcRowMapper<Vehicle> {
        @Override
        public Vehicle createRow(ResultSet rs) throws SQLException {
            Vehicle vehicle = new Vehicle(rs.getInt("ID"),
                    rs.getString("VehicleNumber"));

            try {
                vehicle.setVehicleType(rs.getString("Type"));
            } catch (Exception ex) {
            }

            try {
                vehicle.setPosition(rs.getInt("VehicleOrder"));
            } catch (Exception ex) {
            }

            return vehicle;
        }
    };

    public List<Vehicle> getVehicles() {
        String query = new QueryManager().getQuery(Spectrum.ALL_VEHICLES);
        return findByQuery(query);
    }

    public List<Vehicle> getVehicles(Integer unitId) {
        String query = new QueryManager().getQuery(Spectrum.VEHICLES_FOR_UNIT_ID);
        return findByQuery(query, unitId);
    }
    
    public String getVehicleNumberById(String vehicleId) {
        String query = new QueryManager().getQuery(Spectrum.VEHICLE_NUMBER_BY_ID);
        List<Vehicle> vehicles = findByQuery(query, new VehicleMapper(),
                vehicleId);

        if (vehicles.size() == 1) {
            return vehicles.get(0).getVehicleNumber();
        } else {
            return null;
        }
    }
    
    public Integer getIdByVehicleNumber(String vehicleNumber) {
        String query = new QueryManager().getQuery(Spectrum.VEHICLE_ID_BY_NUMBER);
        List<Vehicle> vehicles = findByQuery(query, new VehicleMapper(), vehicleNumber);
        return vehicles.size() == 0 ? null : vehicles.get(0).getId();
    }
    
    public String getTableName() {
        return "Vehicle";
    }

    public String getIdColumnName() {
        return "ID";
    }

    public JdbcRowMapper<Vehicle> getRowMapper() {
        return MAPPER;
    }
}
