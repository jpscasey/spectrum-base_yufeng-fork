package com.nexala.spectrum.db.mappers;

import com.google.inject.assistedinject.Assisted;
import com.nexala.spectrum.configuration.CalculatedChannelConfiguration;
import com.nexala.spectrum.configuration.ChannelConfiguration;

/**
 * Factory to instantiate datasetMappers.
 */
public interface AnalysisDataSetMapperFactory {
    AnalysisDataSetMapper create(@Assisted ChannelConfiguration channelConf,
            @Assisted CalculatedChannelConfiguration ccConf,
            @Assisted String channelId);
}
