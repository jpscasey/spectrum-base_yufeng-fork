package com.nexala.spectrum.db.dao.charting;

import java.math.BigDecimal;

import com.nexala.spectrum.db.beans.ChannelConfig;
import com.nexala.spectrum.db.dao.Record;

/**
 * An object of this class loosely represents an entry in the ChartChannel
 * table. It holds a reference to the chart it belongs to and to the channel
 * record that defines which channel it represents.
 * 
 * @author Severinas Monkevicius
 */
public class ChartChannel implements Record {
    /**
     * The chart this custom channel belongs to
     */
    private Long chartId;

    /**
     * The base channel this custom channel entry represents
     */
    private ChannelConfig channel;

    /**
     * The scale of this channel. Ignored for digital channels
     */
    private BigDecimal scale;

    /**
     * The order in which this channel appears on the chart. The channel with
     * the lowest value appears first.
     */
    private Integer sequence;

    private BigDecimal minValue;
    private BigDecimal maxValue;

    private Boolean displayedAsDifference;

    public Long getChartId() {
        return this.chartId;
    }
    
    public void setChartId(Long chartId) {
        this.chartId = chartId;
    }

    public ChannelConfig getChannel() {
        return channel;
    }

    public void setChannel(ChannelConfig channel) {
        this.channel = channel;
    }

    public BigDecimal getScale() {
        return scale;
    }

    public void setScale(BigDecimal scale) {
        this.scale = scale;
    }

    public Integer getSequence() {
        return sequence;
    }

    public void setSequence(Integer sequence) {
        this.sequence = sequence;
    }
    
    public BigDecimal getMinValue() {
        return this.minValue;
    }

    public void setMinValue(BigDecimal minValue) {
        this.minValue = minValue;
    }

    public BigDecimal getMaxValue() {
        return this.maxValue;
    }

    public void setMaxValue(BigDecimal maxValue) {
        this.maxValue = maxValue;
    }

	public Boolean isDisplayedAsDifference() {
		return displayedAsDifference;
	}

	public void setDisplayedAsDifference(Boolean displayedAsDifference) {
		this.displayedAsDifference = displayedAsDifference;
	}
    
}
