/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.db.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.analysis.data.AnalysisDataSet;
import com.nexala.spectrum.channel.Channel;
import com.nexala.spectrum.channel.ChannelCollection;
import com.nexala.spectrum.configuration.CalculatedChannel;
import com.nexala.spectrum.configuration.CalculatedChannelConfiguration;
import com.nexala.spectrum.configuration.ChannelCalculationStrategy;
import com.nexala.spectrum.configuration.ChannelConfiguration;
import com.nexala.spectrum.db.ChannelCollectionMapper;
import com.nexala.spectrum.db.ChannelCollectionMapperFactory;
import com.nexala.spectrum.db.JdbcRowMapper;
import com.nexala.spectrum.db.beans.ChannelConfig;
import com.nexala.spectrum.db.dao.BoxedResultSet;
import com.nexala.spectrum.db.dao.BoxedResultSetFactory;

public final class AnalysisDataSetMapper extends JdbcRowMapper<AnalysisDataSet> {

    private final BoxedResultSetFactory factory;
    
    private final ChannelCollectionMapper ccMapper;
    
    private final String channelId;

    private final CalculatedChannelConfiguration ccc;
    
    @Inject
    public AnalysisDataSetMapper(@Assisted ChannelConfiguration channelConf,
            @Assisted CalculatedChannelConfiguration ccc,
            @Assisted String channelId,
            ChannelCollectionMapperFactory ccMapperFactory,
            BoxedResultSetFactory factory) {
        this.channelId = channelId;
        this.ccc = ccc;
        this.ccMapper = ccMapperFactory.create(channelConf);
        this.factory = factory;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public AnalysisDataSet createRow(ResultSet resultSet) throws SQLException {
        
        BoxedResultSet rs = factory.create(resultSet);
        Long rowId = null;
        Integer id = -1;
        Long timestamp = null;
        
        try {
            rowId = rs.getLong("ID");
        } catch (Exception ex) {}
        
        try {
            id = rs.getInt(channelId);
        } catch (Exception ex) {}

        try {
            timestamp = rs.getTimestamp(Spectrum.TIMESTAMP);
        } catch (Exception ex) {}

        ChannelCollection channels = ccMapper.createRow(resultSet);

        for (CalculatedChannel c : ccc.getCalculatedChannels()) {
            ChannelConfig channel = c.getChannel();
            ChannelCalculationStrategy ccs = c.getStrategy();
            Channel<?> calculatedValue = ccs.calculate(channel, channels);

            if (calculatedValue != null) {
                channels.put(calculatedValue);
            }
        }

        return new AnalysisDataSet(rowId, id, timestamp, channels);
    }
}
