package com.nexala.spectrum.db.beans;

/**
 * Bean representing a vehicleType
 * @author brice
 *
 */
public class ChartVehicleType {
	
	private Integer chartId;
	
	private String VehicleType;

	/**
	 * Getter for the chartId.
	 * @return the chartId
	 */
	public Integer getChartId() {
		return chartId;
	}

	/**
	 * Setter for the chartId.
	 * @param chartId the chartId to set
	 */
	public void setChartId(Integer chartId) {
		this.chartId = chartId;
	}

	/**
	 * Getter for the vehicleType.
	 * @return the vehicleType
	 */
	public String getVehicleType() {
		return VehicleType;
	}

	/**
	 * Setter for the vehicleType.
	 * @param vehicleType the vehicleType to set
	 */
	public void setVehicleType(String vehicleType) {
		VehicleType = vehicleType;
	}
	
	

}
