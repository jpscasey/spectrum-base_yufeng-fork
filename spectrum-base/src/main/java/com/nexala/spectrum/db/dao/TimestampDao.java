package com.nexala.spectrum.db.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import com.google.inject.Inject;
import com.nexala.spectrum.db.JdbcRowMapper;
import com.nexala.spectrum.utils.DateUtils;

public class TimestampDao extends GenericDao<Long, Long> {
    
    @Inject
    private DateUtils dateUtils;
    
    class TimestampMapper extends JdbcRowMapper<Long> {
        @Override
        public Long createRow(ResultSet resultSet) throws SQLException {
            Long result = null;
            Timestamp timestamp =  resultSet.getTimestamp(1, dateUtils.getDatabaseCalendar());
            if (timestamp != null) {
                result = timestamp.getTime();
            }
            return result;
        }
    }

    public List<Long> getTimestampByStoredProcedure(String query, Object... parameters) {
        return findByStoredProcedure(query, new TimestampMapper(), parameters);
    }

    public List<Long> getTimestampByQuery(String query, Object... parameters) {
        return findByQuery(query, new TimestampMapper(), parameters);
    }

}
