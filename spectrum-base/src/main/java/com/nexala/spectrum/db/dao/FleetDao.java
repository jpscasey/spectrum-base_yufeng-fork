package com.nexala.spectrum.db.dao;

import java.util.List;

import com.google.inject.Inject;
import com.nexala.spectrum.db.JdbcRowMapper;
import com.nexala.spectrum.db.beans.Fleet;
import com.nexala.spectrum.db.mappers.FleetMapper;

public class FleetDao extends GenericDao<Fleet, String> {
    private FleetMapper mapper = new FleetMapper();
    
    @Inject
    private FleetDao() {

    }
    
    public FleetDao(JndiConnectionProvider con) {
		super.setConnectionProvider(con);
	}

	public List<Fleet> getFleetList() {
        String query = "SELECT ID, Name, Code FROM Fleet";
        List<Fleet> fleetList = findByQuery(query, mapper);
        return fleetList;
    }
	
	public Fleet getFleetByFleetCode(String fleetCode) {
		String query = "SELECT ID, Name, Code FROM Fleet WHERE Code = ?";
		List<Fleet> fleets = findByQuery(query, mapper, fleetCode);
		
		if (fleets.size() > 0) {
            return fleets.get(0);
        } else {
            return null;
        }
	}

    @Override
    public JdbcRowMapper<Fleet> getRowMapper() {
        return mapper;
    }
    
    public String getTableName() {
        return "Fleet";
    }

    public String getIdColumnName() {
        return "ID";
    }
}
