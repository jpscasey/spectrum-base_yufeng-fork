package com.nexala.spectrum.db.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.nexala.spectrum.db.JdbcRowMapper;
import com.nexala.spectrum.db.beans.FaultSource;

public class FaultSourceDao extends GenericDao<FaultSource, String> {
    private static final JdbcRowMapper<FaultSource> MAPPER = new JdbcRowMapper<FaultSource>() {
        @Override
        public FaultSource createRow(ResultSet resultSet) throws SQLException {
            FaultSource fs = new FaultSource(resultSet.getInt("ID"),
                    resultSet.getString("Description"));
            return fs;
        }
    };

    public List<FaultSource> getFaultSourceList() {
        String query = "SELECT ID, Description FROM FaultSource";
        List<FaultSource> faultSourceList = findByQuery(query, MAPPER);
        return faultSourceList;
    }

    @Override
    public JdbcRowMapper<FaultSource> getRowMapper() {
        return MAPPER;
    }

    public String getTableName() {
        return "FaultSource";
    }

    public String getIdColumnName() {
        return "ID";
    }
}
