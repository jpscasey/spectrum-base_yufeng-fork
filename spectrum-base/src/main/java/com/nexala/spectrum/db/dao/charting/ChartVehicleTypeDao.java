package com.nexala.spectrum.db.dao.charting;

import com.google.inject.Inject;
import com.nexala.spectrum.db.RowMapper;
import com.nexala.spectrum.db.beans.ChartVehicleType;
import com.nexala.spectrum.db.beans.mappers.ChartVehicleTypeMapper;
import com.nexala.spectrum.db.dao.GenericDao;

/**
 * Data access object for the chartvehicletype table
 * @author brice
 *
 */
public class ChartVehicleTypeDao extends GenericDao<ChartVehicleType, Integer> {
    
    @Inject 
    ChartVehicleTypeMapper mapper;
    
    @Override
    public String getTableName() {
        return "ChartVehicleType";
    }
    
    @Override
    public RowMapper<ChartVehicleType> getRowMapper() {
        return mapper;
    }
}