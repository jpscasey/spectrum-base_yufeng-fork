/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.db.dao;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.analysis.bean.AnalysisDataMap;
import com.nexala.spectrum.analysis.bean.GenericEvent;
import com.nexala.spectrum.analysis.data.AnalysisDataSet;
import com.nexala.spectrum.analysis.data.GenericEventChannelLoader;
import com.nexala.spectrum.configuration.ApplicationConfiguration;
import com.nexala.spectrum.configuration.CalculatedChannelConfiguration;
import com.nexala.spectrum.configuration.ChannelConfiguration;
import com.nexala.spectrum.db.mappers.AnalysisDataSetMapper;
import com.nexala.spectrum.db.mappers.AnalysisDataSetMapperFactory;

public class LiveFaultChannelDataDao extends GenericDao<AnalysisDataSet, Long> 
        implements AnalysisDataDao<GenericEvent> {
    
    private AnalysisDataSetMapper aRowMapper = null;
    private final ApplicationConfiguration appConf;
    private final GenericEventChannelLoader loader;
    private final String fleetId;
    private final String keyChannel;
    
    @Inject
    public LiveFaultChannelDataDao(
            @Named(Spectrum.FLEET_ID) String fleetId,
            ApplicationConfiguration appConf,
            ChannelConfiguration conf,
            CalculatedChannelConfiguration cconf,
            AnalysisDataSetMapperFactory factory,
            GenericEventChannelLoader loader) {
        this.appConf = appConf;
        this.loader = loader;
        this.fleetId = fleetId;
        this.keyChannel = appConf.get().getString("r2m."+fleetId+".stream-analysis.keyChannel");
        this.aRowMapper = factory.create(conf, cconf, keyChannel);
    }
    
    public AnalysisDataMap<GenericEvent> getLiveData(Long lastRecordId, Long... moreRecordIds) {
        String liveQuery = appConf.get().getString("r2m."+fleetId+".stream-analysis.dataSp");
        
        List<AnalysisDataSet> liveData = findByStoredProcedureWithTimeout(liveQuery, aRowMapper, 60, lastRecordId);

        Map<Integer, List<GenericEvent>> map = loader.loadDataSets(liveData);
        
        //Get the last id
        for (AnalysisDataSet ds : liveData) {
            Long rowId = ds.getRowId();
            
            if (lastRecordId == null || rowId > lastRecordId) {
                lastRecordId = rowId;
            }
        }
        
        return new AnalysisDataMap<GenericEvent>(map, lastRecordId);
    }
    
    public Map<Integer, List<GenericEvent>> getData(Integer stockId, long startTime, long endTime) {
        String ruleTesterQuery = appConf.get().getString("r2m."+fleetId+".stream-analysis.dataTesterSp");
        
        List<AnalysisDataSet> testData = findByStoredProcedureWithTimeout(ruleTesterQuery, aRowMapper, 60,  new Date(startTime), new Date(endTime), stockId);
        
        return loader.loadDataSets(testData);
    }

}
