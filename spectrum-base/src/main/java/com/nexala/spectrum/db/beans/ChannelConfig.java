package com.nexala.spectrum.db.beans;

import java.util.List;

import com.nexala.spectrum.db.dao.charting.ChannelType;

public final class ChannelConfig {
    public static final String SHORT_PREFIX = "Col";
    private final boolean alwaysDisplayed;
    private final Integer channelGroupId;
    private final Integer channelGroupOrder;
    private final Integer columnId;
    private final String comment;
    private final Boolean defaultValue;
    private final String description;
    private final String formatPattern;
    private final String hardwareType;
    private final Integer id;
    private final boolean lookup;
    private final Double maxValue;
    private final Double minValue;
    private final String name;
    private final String ref;
    private final String storageMethod;
    private final ChannelType type;
    private final String dataType;
    private final String unitOfMeasure;
    private final Integer vehicleId;
    private final boolean visibleOnFaultOnly;
    private final boolean displayedAsDifference;
    private final RelatedChannelGroup relatedChannelGroup;
    private final String vehicleType;
    private final List<String> linkedChannels;
    private final String shortName;

    public ChannelConfig(ChannelConfigBuilder builder) {
        this.alwaysDisplayed = builder.isAlwaysDisplayed();
        this.channelGroupId = builder.getChannelGroupId();
        this.channelGroupOrder = builder.getChannelGroupOrder();
        this.columnId = builder.getColumnId();
        this.comment = builder.getComment();
        this.defaultValue = builder.getDefaultValue();
        this.description = builder.getDescription();
        this.formatPattern = builder.getFormatPattern();
        this.hardwareType = builder.getHardwareType();
        this.id = builder.getId();
        this.lookup = builder.isLookup();
        this.maxValue = builder.getMaxValue();
        this.minValue = builder.getMinValue();
        this.name = builder.getName();
        this.ref = builder.getRef();
        this.storageMethod = builder.getStorageMethod();
        this.type = builder.getType();
        this.unitOfMeasure = builder.getUnitOfMeasure();
        this.vehicleId = builder.getVehicleId();
        this.visibleOnFaultOnly = builder.isVisibleOnFaultOnly();
        this.displayedAsDifference = builder.isDisplayedAsDifference();
        this.relatedChannelGroup = builder.getRelatedChannelGroup();
        this.vehicleType = builder.getVehicleType();
        this.linkedChannels = builder.getLinkedChannels();
        this.dataType = builder.getDataType();
        
        if (columnId != null) {
            this.shortName = SHORT_PREFIX + columnId;
        } else {
            this.shortName = null;
        }
        
    }

    public Integer getChannelGroupId() {
        return channelGroupId;
    }

    public Integer getChannelGroupOrder() {
        return channelGroupOrder;
    }

    public Integer getColumnId() {
        return columnId;
    }

    public String getComment() {
        return comment;
    }

    public Boolean getDefaultValue() {
        return defaultValue;
    }

    public String getDescription() {
        return description;
    }

    public String getFormatPattern() {
        return formatPattern;
    }

    public String getHardwareType() {
        return hardwareType;
    }

    public Integer getId() {
        return id;
    }

    public Double getMaxValue() {
        return maxValue;
    }

    public Double getMinValue() {
        return minValue;
    }

    public String getName() {
        return name;
    }

    public String getRef() {
        return ref;
    }

    public String getShortName() {
        return shortName;
    }

    public String getStorageMethod() {
        return storageMethod;
    }

    public ChannelType getType() {
        return type;
    }

    public String getUnitOfMeasure() {
        return unitOfMeasure;
    }

    public Integer getVehicleId() {
        return vehicleId;
    }

    public boolean isAlwaysDisplayed() {
        return alwaysDisplayed;
    }

    public boolean isLookup() {
        return lookup;
    }

    public boolean isVisibleOnFaultOnly() {
        return visibleOnFaultOnly;
    }
    
    public boolean isDisplayedAsDifference() {
    	return displayedAsDifference;
    }
    
    public RelatedChannelGroup getRelatedChannelGroup() {
        return relatedChannelGroup;
    }

	public String getVehicleType() {
        return vehicleType;
    }
	
	public String getDataType() {
        return dataType;
    }

    public List<String> getLinkedChannels() {
        return linkedChannels;
    }

    @Override
	public String toString() {
		return "ChannelConfig "
		        + "[alwaysDisplayed=" + alwaysDisplayed
				+ ", channelGroupId=" + channelGroupId 
				+ ", channelGroupOrder=" + channelGroupOrder 
				+ ", columnId=" + columnId 
				+ ", comment=" + comment 
				+ ", defaultValue=" + defaultValue 
				+ ", description=" + description 
				+ ", formatPattern=" + formatPattern 
				+ ", hardwareType=" + hardwareType 
				+ ", id=" + id 
				+ ", lookup=" + lookup 
				+ ", maxValue=" + maxValue 
				+ ", minValue=" + minValue
				+ ", name=" + name 
				+ ", ref=" + ref 
				+ ", storageMethod=" + storageMethod 
				+ ", type=" + type 
				+ ", unitOfMeasure=" + unitOfMeasure 
				+ ", vehicleId=" + vehicleId 
				+ ", visibleOnFaultOnly=" + visibleOnFaultOnly 
				+ ", displayedAsDifference=" + displayedAsDifference 
                + ", relatedChannelGroup=" + relatedChannelGroup 
				+ "]";
	}
}
