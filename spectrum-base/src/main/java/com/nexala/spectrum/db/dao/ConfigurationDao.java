/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.db.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.db.DbUtil;
import com.nexala.spectrum.db.JdbcRowMapper;
import com.nexala.spectrum.db.QueryManager;
import com.nexala.spectrum.db.RowMapper;
import com.nexala.spectrum.db.beans.Configuration;

@Singleton
public class ConfigurationDao extends GenericDao<Configuration, Long> {
    private static class ConfigurationMapper extends JdbcRowMapper<Configuration> {
        @Override
        public final Configuration createRow(ResultSet rs) throws SQLException {
            return new Configuration(rs.getString(Spectrum.PROPERTY_NAME), rs.getString(Spectrum.PROPERTY_VALUE));
        }
    }

    @Inject
    private ConfigurationMapper mapper;

    /**
     * Return a single configuration querying the database.
     * Deprecated, {@link ConfigurationMapper} should be used instead.  
     * @param propertyName the property name
     * @return the configuration
     * @throws DataAccessException in case of error
     */
    @Deprecated
    public Configuration findByName(String propertyName) throws DataAccessException {
        String query = new QueryManager().getQuery(Spectrum.CONFIGURATION_SINGLE_QUERY);
        List<Configuration> confs = findByQuery(query, mapper, propertyName);
        if (confs != null && confs.size() > 0) {
            return confs.get(0);
        }
        return null;
    }

    public void changeValue(String propertyName, String propertyValue) throws DataAccessException {
        String query = new QueryManager().getQuery(Spectrum.CONFIGURATION_CHANGE_QUERY);

        Connection c = getConnection();
        PreparedStatement ps = null;

        try {
            ps = c.prepareStatement(query);
            ps.setString(1, propertyValue);
            ps.setString(2, propertyName);

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DataAccessException(e);
        } finally {
            DbUtil.closeStatement(ps);
            DbUtil.closeConnection(c);
        }
    }
    
    public void changeValue(String propertyName, long propertyValue) throws DataAccessException {
        changeValue(propertyName, String.valueOf(propertyValue));
    }
    
    /**
     * @see com.nexala.spectrum.db.dao.GenericDao#getTableName()
     */
    @Override
    public String getTableName() {
        return "Configuration";
    }
    
    /**
     * @see com.nexala.spectrum.db.dao.GenericDao#getRowMapper()
     */
    @Override
    public RowMapper<Configuration> getRowMapper() {
        return mapper;
    }
}
