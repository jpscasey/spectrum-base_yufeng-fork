package com.nexala.spectrum.db.dao.charting;

import java.util.ArrayList;
import java.util.List;

import com.nexala.spectrum.db.dao.Record;

public class ChartConfiguration implements Record {
    private Long id;
    private Long configurationId;
    private String name;
    private List<ChartChannel> channels;
    private ChannelType type;
    private Integer sequence;

    public ChartConfiguration() {
        this.channels = new ArrayList<ChartChannel>();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getConfigurationId() {
        return configurationId;
    }

    public void setConfigurationId(Long configurationId) {
        this.configurationId = configurationId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ChartChannel> getChannels() {
        return channels;
    }

    public void setChannels(List<ChartChannel> channels) {
        this.channels = channels;
        
        for (ChartChannel channel: channels) {
            if (channel != null) {
                channel.setChartId(id);
            }
        }
    }

    public void addChannel(ChartChannel channel) {
        this.channels.add(channel);
        
        if (channel != null) {
            channel.setChartId(id);
        }
    }

    public ChannelType getType() {
        return type;
    }

    public void setType(ChannelType type) {
        this.type = type;
    }

    public Integer getSequence() {
        return sequence;
    }

    public void setSequence(Integer sequence) {
        this.sequence = sequence;
    }

    /**
     * Compare a custom chart to another object. They will be considered equal
     * if they are at least both instances of a custom chart and have the same
     * ID.
     * @param o the object to compare with
     * @return true if the IDs of the two custom charts are the same; false
     *         otherwise
     */
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ChartConfiguration)) {
            return false;
        }
        ChartConfiguration c = (ChartConfiguration) o;
        if (this.id != c.id && (this.id == null || c.id == null)) {
            return false;
        }
        return this.id.equals(c.id);
    }
}
