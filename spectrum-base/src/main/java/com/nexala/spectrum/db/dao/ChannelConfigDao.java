/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.db.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.google.inject.Inject;
import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.configuration.ApplicationConfiguration;
import com.nexala.spectrum.db.JdbcRowMapper;
import com.nexala.spectrum.db.QueryManager;
import com.nexala.spectrum.db.beans.ChannelConfig;
import com.nexala.spectrum.db.beans.ChannelConfigBuilder;
import com.nexala.spectrum.db.beans.ChannelDefinitionParam;
import com.nexala.spectrum.db.beans.RelatedChannelGroup;
import com.nexala.spectrum.db.dao.charting.ChannelType;
import com.typesafe.config.Config;

public class ChannelConfigDao extends GenericDao<ChannelConfig, Long> {
    private final Logger LOGGER = Logger.getLogger(ChannelConfigDao.class);
    
    private final Config config;

    @Inject
    private BoxedResultSetFactory factory;

    
    private class ChannelConfigMapper extends JdbcRowMapper<ChannelConfig> {

        @Override
        public ChannelConfig createRow(ResultSet resultSet) throws SQLException {
            BoxedResultSet rs = factory.create(resultSet);

            ChannelConfigBuilder builder = new ChannelConfigBuilder();
            
            setFields(builder, rs);
            
            Integer relatedChannelGroupId = rs.getInt("RelatedChannelID");
            if (relatedChannelGroupId != null && relatedChannelGroupId >= 0) {
                RelatedChannelGroup relatedChannelGroup = new RelatedChannelGroup();
                relatedChannelGroup.setId(relatedChannelGroupId);
                relatedChannelGroup.setDisplayName(rs.getString("RelatedChannelName"));
                builder.setRelatedChannelGroup(relatedChannelGroup);
            }

            return new ChannelConfig(builder);
        }
    }
    
    private class ChannelConfigExtraFieldsMapper extends JdbcRowMapper<ChannelConfig> {
        
        @Override
        public ChannelConfig createRow(ResultSet resultSet) throws SQLException {
            BoxedResultSet rs = factory.create(resultSet);

            ChannelConfigBuilder builder = new ChannelConfigBuilder();

            setFields(builder, rs);
            
            Integer relatedChannelGroupId = rs.getInt("RelatedChannelID");
            if (relatedChannelGroupId != null && relatedChannelGroupId >= 0) {
                RelatedChannelGroup relatedChannelGroup = new RelatedChannelGroup();
                relatedChannelGroup.setId(relatedChannelGroupId);
                relatedChannelGroup.setDisplayName(rs.getString("RelatedChannelName"));
                builder.setRelatedChannelGroup(relatedChannelGroup);
                builder.setVehicleType(rs.getString("VehicleName"));
                List<String> linkedChannels = new ArrayList<String>();
                if (config.hasPath("r2m.channelDefinition.linkedChannels")) {
                    List<String> channels = config.getStringList("r2m.channelDefinition.linkedChannels");
                    for (String channel : channels) {
                        linkedChannels.add(rs.getString(channel));
                    }
                }
                builder.setLinkedChannels(linkedChannels);
            }

            return new ChannelConfig(builder);
        }
    }

    private static void setFields(ChannelConfigBuilder builder, BoxedResultSet rs) throws SQLException {
        builder.setId(rs.getInt("ID"));
        builder.setVehicleId(rs.getInt("VehicleID"));
        builder.setColumnId(rs.getInt("ColumnID"));
        builder.setChannelGroupId(rs.getInt("ChannelGroupID"));
        builder.setName(rs.getString("Name"));
        builder.setDescription(rs.getString("Description"));
        builder.setChannelGroupOrder(rs.getInt("ChannelGroupOrder"));
        builder.setType(ChannelType.fromString(rs.getString("Type")));
        builder.setUnitOfMeasure(rs.getString("UOM"));
        builder.setHardwareType(rs.getString("HardwareType"));
        builder.setStorageMethod(rs.getString("StorageMethod"));
        builder.setRef(rs.getString("Ref"));
        builder.setMinValue(rs.getDouble("MinValue"));
        builder.setMaxValue(rs.getDouble("MaxValue"));
        builder.setComment(rs.getString("Comment"));
        builder.setDefaultValue(rs.getBoolean("DefaultValue"));
        builder.setDataType(rs.getString("DataType"));

        Boolean vofo = rs.getBoolean("VisibleOnFaultOnly");
        builder.setVisibleOnFaultOnly(vofo == null ? false : vofo);

        Boolean ad = rs.getBoolean("IsAlwaysDisplayed");
        builder.setAlwaysDisplayed(ad == null ? false : ad);
        
        Boolean dad = rs.getBoolean("IsDisplayedAsDifference");
        builder.setDisplayedAsDifference(dad == null ? false : dad);

        Boolean lookup = rs.getBoolean("IsLookup");
        builder.setLookup(lookup == null ? false : lookup);
    }
    
    private ChannelConfigMapper mapper = new ChannelConfigMapper();

    private ChannelConfigExtraFieldsMapper extraFieldsMapper = new ChannelConfigExtraFieldsMapper();

    @Inject
    public ChannelConfigDao(ApplicationConfiguration applicationConfiguration) {
        this.config = applicationConfiguration.get();
    }

    @Override
    public List<ChannelConfig> findAll() throws DataAccessException {
        String query = new QueryManager().getQuery(Spectrum.CHANNEL_CONFIG_QUERY);
        return this.findByQuery(query, mapper);
    }

    public List<ChannelConfig> findByKeyword(String keyword) {
        String query = new QueryManager().getQuery(Spectrum.CHANNEL_CONFIG_KEYWORD_QUERY);
        return findByStoredProcedure(query, extraFieldsMapper, keyword);
    }

    public ChannelConfig findById(int channelId) {
        String query = new QueryManager().getQuery(Spectrum.CHANNEL_CONFIG_ID_QUERY);
        List<ChannelConfig> list = findByStoredProcedure(query, extraFieldsMapper, channelId);

        return list != null && list.size() == 1 ? list.get(0) : null;
    }

    public boolean save(String params) {
        String query = new QueryManager().getQuery(Spectrum.CHANNEL_CONFIG_SAVE_QUERY);

        try {
            executeStoredProcedure(query, params);
            LOGGER.info(String.format("Channel definition saved successfully\n%s", params));
            return true;
        } catch (DataAccessException ex) {
            LOGGER.error(String.format("Error saving channel definition: '%s'", params), ex);
        }

        return false;
    }
}