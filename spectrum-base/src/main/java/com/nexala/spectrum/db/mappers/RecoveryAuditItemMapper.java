package com.nexala.spectrum.db.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.nexala.spectrum.db.JdbcRowMapper;
import com.nexala.spectrum.db.beans.RecoveryAuditItem;

public class RecoveryAuditItemMapper extends JdbcRowMapper<RecoveryAuditItem> {
    public RecoveryAuditItem createRow(ResultSet resultSet) throws SQLException {
        RecoveryAuditItem rai = new RecoveryAuditItem();
        rai.setId(resultSet.getLong("id"));
        rai.setTimestamp(resultSet.getDate("Timestamp"));
        rai.setUserName(resultSet.getString("UserName"));
        rai.setContextId(resultSet.getString("Context_id"));
        rai.setPrecedingId(resultSet.getLong("Preceding_id"));
        rai.setAuditItemTypeId(resultSet.getLong("AuditItemType_id"));
        rai.setFaultInstanceId(resultSet.getLong("Fault_id"));
        rai.setNotes(resultSet.getString("Notes"));
        
        return rai;
    }
}
