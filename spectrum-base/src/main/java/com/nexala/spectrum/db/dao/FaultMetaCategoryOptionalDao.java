/*
 * Copyright (c) Nexala Technologies 2015, All rights reserved.
 *
 * $Author$
 * $Date$
 * $Revision$
 * $Source$
 */
package com.nexala.spectrum.db.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.google.inject.Inject;
import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.configuration.ConfigurationManager;
import com.nexala.spectrum.db.JdbcRowMapper;

public class FaultMetaCategoryOptionalDao extends GenericDao<Integer, Integer> {
    private final boolean optionalCategoriesEnabled;

    @Inject
    public FaultMetaCategoryOptionalDao(ConfigurationManager confManager) {
        Boolean enabled = confManager
                .getConfAsBoolean(Spectrum.SPECTRUM_RULE_EDITOR_ENABLE_OPTIONAL_CATEGORIES);
        this.optionalCategoriesEnabled = enabled != null ? enabled : false;
    }

    private class OptionalCategoriesMapper extends JdbcRowMapper<Integer> {
        @Override
        public Integer createRow(ResultSet resultSet) throws SQLException {
            return resultSet.getInt("FaultCategoryID");
        }
    }

    public List<Integer> getOptionalCategoryList(Integer faultMetaId)
            throws DataAccessException {
        if (optionalCategoriesEnabled) {
            return findByQuery(
                    "SELECT * FROM FaultMetaCategoryOptional WHERE FaultMetaID = ?",
                    new OptionalCategoriesMapper(), faultMetaId);
        } else {
            return new ArrayList<Integer>();
        }

    }
}
