/*
 * Copyright (c) Nexala Technologies 2010, All rights reserved.
 */
package com.nexala.spectrum.db.beans;

import java.sql.Timestamp;
import java.util.List;

import com.nexala.spectrum.db.dao.Record;

public class FaultMeta implements Record, Comparable<FaultMeta> {
	private Integer id;
	private String faultCode;
	private String description;
	private String summary;
	private String additionalInfo;
	private String url;
	private Integer categoryId;
	private Integer groupId;
	private Integer sourceId;
	private Integer typeId;
	private Integer typeIdOverride;
	private Boolean hasRecovery;
	private Integer recoveryTypeId;
	private String recoveryProcessPath;
	private List<Integer> optionalCategoryIdList;
    private String e2mFaultCode;
    private String e2mFaultDescription;
    private String e2mPriorityCode;
    private String e2mPriority;
    private Timestamp fromDate;
    private Timestamp toDate;
    private Integer parentId;
    private Integer fleetId;
	
	public FaultMeta() {}
	
	public FaultMeta(Integer id, String faultCode, String description,
            String summary, String additionalInfo, String url, Integer category, Integer typeId) {
        this.id = id;
        this.faultCode = faultCode;
        this.description = description;
        this.summary = summary;
        this.additionalInfo = additionalInfo;
        this.url = url;
        this.categoryId = category;
        this.typeId = typeId;
    }
	
    public FaultMeta(Integer id, String faultCode, String description,
            String summary, String additionalInfo, String url, Integer category, 
            Integer typeId, Integer groupId, Integer sourceId, 
            Integer recoveryTypeId, String recoveryProcessPath, Timestamp fromDate, Timestamp toDate, Integer parentId) {
        this.id = id;
        this.faultCode = faultCode;
        this.description = description;
        this.summary = summary;
        this.additionalInfo = additionalInfo;
        this.url = url;
        this.categoryId = category;
        this.typeId = typeId;
        this.groupId = groupId;
        this.sourceId = sourceId;
        this.recoveryTypeId = recoveryTypeId;
        this.recoveryProcessPath = recoveryProcessPath;
        this.fromDate = fromDate;
        this.toDate = toDate;
        this.parentId = parentId;
    }
    
    public FaultMeta(Integer id, String faultCode, String description,
            String summary, String additionalInfo, String url, Integer category, 
            Integer typeId, Integer groupId, Integer sourceId, 
            Integer recoveryTypeId, String recoveryProcessPath, Timestamp fromDate, 
            Timestamp toDate, Integer parentId, Integer fleetId) {
        this(id, faultCode, description,
                summary, additionalInfo, url, category, 
                typeId, groupId, sourceId, 
                recoveryTypeId, recoveryProcessPath, fromDate, 
                toDate, parentId);
        this.fleetId = fleetId;
    }
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getFaultCode() {
		return faultCode;
	}
	
	public void setFaultCode(String faultCode) {
		this.faultCode = faultCode;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getSummary() {
		return summary;
	}
	
	public void setSummary(String summary) {
		this.summary = summary;
	}
	
	public Integer getCategoryId() {
		return this.categoryId;
	}
	
	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}
	
	public Integer getTypeId() {
		return typeId;
	}
	
	public void setTypeId(Integer typeId) {
		this.typeId = typeId;
	}
	
	public Integer getTypeIdOverride() {
        return typeIdOverride;
    }
    
    public void setTypeIdOverride(Integer typeIdOverride) {
        this.typeIdOverride = typeIdOverride;
    }
	
	public Boolean getHasRecovery() {
		return hasRecovery;
	}

	public void setHasRecovery(Boolean hasRecovery) {
		this.hasRecovery = hasRecovery;
	}

	public Integer getRecoveryTypeId() {
		return this.recoveryTypeId;
	}
	
	public void setRecoveryTypeId(Integer recoveryTypeId) {
		this.recoveryTypeId = recoveryTypeId;
	}
	
	public String getRecoveryProcessPath() {
	    return this.recoveryProcessPath;
	}
	
	public void setRecoveryProcessPath(String recoveryProcessPath) {
	    this.recoveryProcessPath = recoveryProcessPath;
	}
	
	public List<Integer> getOptionalCategoryIdList() {
        return this.optionalCategoryIdList;
    }
    
    public void setOptionalCategoryIdList(List<Integer> optionalCategoryIdList) {
        this.optionalCategoryIdList = optionalCategoryIdList;
    }

    /**
     * Get the url.
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * Set the url.
     * @param url the url to set
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * Get the groupId.
     * @return the groupId
     */
    public Integer getGroupId() {
        return groupId;
    }

    /**
     * Set the groupId.
     * @param groupId the groupId to set
     */
    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    public Integer getSourceId() {
        return sourceId;
    }

    public void setSourceId(Integer sourceId) {
        this.sourceId = sourceId;
    }

    public String getE2mFaultCode() {
        return e2mFaultCode;
    }

    public void setE2mFaultCode(String e2mFaultCode) {
        this.e2mFaultCode = e2mFaultCode;
    }

    public String getE2mFaultDescription() {
        return e2mFaultDescription;
    }

    public void setE2mFaultDescription(String e2mFaultDescription) {
        this.e2mFaultDescription = e2mFaultDescription;
    }

    public String getE2mPriorityCode() {
        return e2mPriorityCode;
    }

    public void setE2mPriorityCode(String e2mPriorityCode) {
        this.e2mPriorityCode = e2mPriorityCode;
    }

    public String getE2mPriority() {
        return e2mPriority;
    }

    public void setE2mPriority(String e2mPriority) {
        this.e2mPriority = e2mPriority;
    }

    @Override
    public int compareTo(FaultMeta o) {
        try {
            Integer myFaultCodeInt = Integer.parseInt(this.getFaultCode());
            Integer oFaultCodeInt = Integer.parseInt(o.getFaultCode());

            return myFaultCodeInt.compareTo(oFaultCodeInt);
        } catch (NumberFormatException e) {
            return this.faultCode.compareTo(o.getFaultCode());
        }
    }

    /**
     * @return the additionalInfo
     */
    public String getAdditionalInfo() {
        return additionalInfo;
    }

    /**
     * @param additionalInfo the additionalInfo to set
     */
    public void setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

    public Timestamp getFromDate() {
        return fromDate;
    }

    public void setFromDate(Timestamp timestamp) {
        this.fromDate = timestamp;
    }

    public Timestamp getToDate() {
        return toDate;
    }

    public void setToDate(Timestamp timestamp) {
        this.toDate = timestamp;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

	public Integer getFleetId() {
		return fleetId;
	}

	public void setFleetId(Integer fleetId) {
		this.fleetId = fleetId;
	}
}