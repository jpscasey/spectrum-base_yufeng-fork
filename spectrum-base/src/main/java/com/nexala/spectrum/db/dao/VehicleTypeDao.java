package com.nexala.spectrum.db.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.google.inject.Inject;
import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.db.JdbcRowMapper;
import com.nexala.spectrum.db.QueryManager;
import com.nexala.spectrum.db.beans.VehicleType;

public class VehicleTypeDao extends GenericDao<VehicleType, Integer> {

    private static class VehicleTypeMapper extends JdbcRowMapper<VehicleType> {

        @Inject
        private BoxedResultSetFactory factory;
        
        @Override
        public VehicleType createRow(ResultSet resultSet)
                throws SQLException {
            BoxedResultSet rs = factory.create(resultSet);

            VehicleType vehicleType = new VehicleType();
            
            vehicleType.setId(rs.getInt("ID"));
            vehicleType.setName(rs.getString("Name"));
            
            return vehicleType;
        }
        
    }
    
    private final VehicleTypeMapper mapper;

    @Inject
    public VehicleTypeDao(VehicleTypeMapper mapper) {
        this.mapper = mapper;
    }
    
    @Override
    public List<VehicleType> findAll() {
        String query = new QueryManager().getQuery(Spectrum.VEHICLE_TYPE_QUERY);
        return this.findByQuery(query, mapper);
    }
    
}
