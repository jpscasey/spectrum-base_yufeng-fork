package com.nexala.spectrum.db.beans;

import java.util.Date;

import com.nexala.spectrum.db.dao.Record;

public class RecoveryAuditItem implements Record {
    private long id;
    private Date timestamp;
    private String userName; // capital N to match the database column (UserName)
    private String contextId;
    private long precedingId;
    private long auditItemTypeId;
    private long faultInstanceId;
    private String notes;
    
    public RecoveryAuditItem() {
        
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getContextId() {
        return contextId;
    }

    public void setContextId(String contextId) {
        this.contextId = contextId;
    }

    public long getPrecedingId() {
        return precedingId;
    }

    public void setPrecedingId(long precedingId) {
        this.precedingId = precedingId;
    }

    public long getAuditItemTypeId() {
        return auditItemTypeId;
    }

    public void setAuditItemTypeId(long auditItemTypeId) {
        this.auditItemTypeId = auditItemTypeId;
    }

    public long getFaultInstanceId() {
        return this.faultInstanceId;
    }
    
    public void setFaultInstanceId(long faultInstanceId) {
        this.faultInstanceId = faultInstanceId;
    }
    
    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }
}
