/**
 * Copyright (c) Nexala Technologies 2015, All rights reserved.
 */
package com.nexala.spectrum.db.dao;

import java.util.ArrayList;
import java.util.List;

import com.google.inject.Inject;
import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.db.QueryManager;
import com.nexala.spectrum.db.RowMapper;
import com.nexala.spectrum.db.mappers.LongMapper;

/**
 * Data access object to get the last ids the rule engine processed
 * @author bbaudry
 *
 */
public class RuleEngineCurrentIdsDao extends GenericDao<Long, Long> {
    
    @Inject
    private LongMapper mapper;
    
    @Inject
    private QueryManager queryManager;
    
    /**
     * Update the list of the last ids the rule engine processed.
     * @param configurationName the configuration name
     * @param ids list of last ids the rule engine processed
     */
    public void updateCurrentIds(String configurationName, List<Long> ids) {
        StringBuilder query = new StringBuilder(getUpdateQuery());
        List<Object> params = new ArrayList<Object>();
        params.add(configurationName);
        params.addAll(ids);
        
        // Build the query to add the parameters
        for (int i = 0; i < ids.size(); i++) {
            query.append(",?");
        }
        
        query.append(")}");
        
        executeStoredProcedure(query.toString(), params.toArray());
    }
    
    /**
     * Get the last ids the rule engine processed.
     * @param configurationName the configuration name
     * @return the last ids the rule engine processed
     */
    public List<Long> getCurrentIds(String configurationName) {
        String query = queryManager.getQuery(Spectrum.RULE_ENGINE_GET_CURRENT_IDS);
        
        return findByStoredProcedure(query, configurationName);
    }
    
    /**
     * Return the update query. 
     * @return the update query
     */
    protected String getUpdateQuery() {
        return queryManager.getQuery(Spectrum.RULE_ENGINE_UPDATE_CURRENT_IDS);
    }
    
    /**
     * @see com.nexala.spectrum.db.dao.GenericDao#getRowMapper()
     */
    @Override
    public RowMapper<Long> getRowMapper() {
        return mapper;
    }
    

}
