/*
 * Copyright (c) Nexala Technologies 2010, All rights reserved.
 */
package com.nexala.spectrum.db.dao;

import java.nio.ByteBuffer;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;

import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import com.nexala.spectrum.utils.DateUtils;

public class BoxedResultSet {

    @Inject
    private DateUtils dateUtils;

    private ResultSet rs;

    @Inject
    BoxedResultSet(@Assisted ResultSet rs) {
        this.rs = rs;
    }

    /**
     * Gets the value for the specified column. May return null
     * 
     * @param columnLabel
     * @return a Boolean value or NULL. Do not attempt to store result in a
     *         "boolean" type. As this will result in a NPE
     */
    public Boolean getBoolean(int columnIndex) throws SQLException {
        Boolean b = rs.getBoolean(columnIndex);

        if (rs.wasNull()) {
            return null;
        }

        return b;
    }

    /**
     * Gets the value for the specified column. May return null
     * 
     * @param columnLabel
     * @return a Boolean value or NULL. Do not attempt to store result in a
     *         "boolean" type. As this will result in a NPE
     */
    public Boolean getBoolean(String columnLabel) throws SQLException {
        Boolean b = rs.getBoolean(columnLabel);

        if (rs.wasNull()) {
            return null;
        }

        return b;
    }

    public byte[] getBytes(int columnIndex) throws SQLException {
        byte[] b = rs.getBytes(columnIndex);

        if (rs.wasNull()) {
            return null;
        }

        return b;
    }
    
    public byte[] getBytes(String columnLabel) throws SQLException {
        byte[] b = rs.getBytes(columnLabel);

        if (rs.wasNull()) {
            return null;
        }

        return b;
    }
    
    public Double getDouble(int columnIndex) throws SQLException {
        Double d = rs.getDouble(columnIndex);

        if (rs.wasNull()) {
            return null;
        }

        return d;
    }

    public Double getDouble(String columnLabel) throws SQLException {
        Double d = rs.getDouble(columnLabel);

        if (rs.wasNull()) {
            return null;
        }

        return d;
    }

    public final Integer getInt(int columnIndex) throws SQLException {
        Integer i = rs.getInt(columnIndex);

        if (rs.wasNull()) {
            return null;
        }

        return i;
    }

    public final Integer getInt(String columnLabel) throws SQLException {
        Integer i = rs.getInt(columnLabel);

        if (rs.wasNull()) {
            return null;
        }

        return i;
    }

    @Deprecated
    public Integer getInteger(int columnIndex) throws SQLException {
        return getInt(columnIndex);
    }

    @Deprecated
    public Integer getInteger(String columnLabel) throws SQLException {
        return getInt(columnLabel);
    }

    public Long getLong(int columnIndex) throws SQLException {
        Long i = rs.getLong(columnIndex);

        if (rs.wasNull()) {
            return null;
        }

        return i;
    }

    public Long getLong(String columnLabel) throws SQLException {
        Long i = rs.getLong(columnLabel);

        if (rs.wasNull()) {
            return null;
        }

        return i;
    }
    
    public final Short getShort(int columnIndex) throws SQLException {
        Short i = rs.getShort(columnIndex);

        if (rs.wasNull()) {
            return null;
        }

        return i;
    }
    
    public final Short getShort(String columnLabel) throws SQLException {
        Short i = rs.getShort(columnLabel);

        if (rs.wasNull()) {
            return null;
        }

        return i;
    }

    public String getString(int columnIndex) throws SQLException {
        return rs.getString(columnIndex);
    }

    public String getString(String columnLabel) throws SQLException {
        return rs.getString(columnLabel);
    }

    public Long getTimestamp(int columnIndex) throws SQLException {
        Timestamp t = rs.getTimestamp(columnIndex, dateUtils.getDatabaseCalendar());

        if (rs.wasNull()) {
            return null;
        }

        return t.getTime();
    }

    public Long getTimestamp(String columnLabel) throws SQLException {
        Timestamp t = rs.getTimestamp(columnLabel, dateUtils.getDatabaseCalendar());

        if (rs.wasNull()) {
            return null;
        }

        return t.getTime();
    }
    
    public Date getDate(int columnIndex) throws SQLException {
        Timestamp t = rs.getTimestamp(columnIndex, dateUtils.getDatabaseCalendar());

        if (rs.wasNull()) {
            return null;
        }

        return t;
    }
    
    public Date getDate(String columnLabel) throws SQLException {
        Timestamp t = rs.getTimestamp(columnLabel, dateUtils.getDatabaseCalendar());

        if (rs.wasNull()) {
            return null;
        }

        return t;
    }
    
    public Long getVersion(String columnLabel) throws SQLException {
        Long result = null;
        byte[] bytes = rs.getBytes(columnLabel);
        result = bytesToLong(bytes);

        if (rs.wasNull()) {
            return null;
        }

        return result;
    }
    
    public Long getVersion(int columnIndex) throws SQLException {
        Long result = null;
        byte[] bytes = rs.getBytes(columnIndex);
        result = bytesToLong(bytes);

        if (rs.wasNull()) {
            return null;
        }

        return result;
    }
    
    private Long bytesToLong(byte[] bytes) {
        
        if (bytes != null && bytes.length == Long.BYTES) {
            ByteBuffer buffer = ByteBuffer.allocate(Long.BYTES);
            buffer.put(bytes);
            buffer.flip();//need flip 
            return buffer.getLong();
        }
        return null;
    }
    
    
}