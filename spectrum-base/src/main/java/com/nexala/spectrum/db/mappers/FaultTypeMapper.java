package com.nexala.spectrum.db.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.nexala.spectrum.db.JdbcRowMapper;
import com.nexala.spectrum.db.beans.FaultType;

public class FaultTypeMapper extends JdbcRowMapper<FaultType> {

    /**
     * {@inheritDoc}
     */
    @Override
    public FaultType createRow(ResultSet rs) throws SQLException {
        return new FaultType(
                rs.getInt("ID"),
                rs.getInt("Priority"),
                rs.getString("Name"),
                rs.getString("DisplayColor"));
    }
}
