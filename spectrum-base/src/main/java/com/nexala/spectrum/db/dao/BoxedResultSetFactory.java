/*
 * Copyright (c) Nexala Technologies 2013, All rights reserved.
 */
package com.nexala.spectrum.db.dao;

import java.sql.ResultSet;

import com.google.inject.assistedinject.Assisted;

public interface BoxedResultSetFactory {
    BoxedResultSet create(@Assisted ResultSet rs);
}
