package com.nexala.spectrum.db.beans;

public class FaultTransmission {

    private Long transmissionId;

    private Long faultId;

    private String transmissionStatus;

    private Integer retries;

    private String locationCode;

    private String locationName;

    private String unitNumber;

    private String vehicleNumber;

    private String notes;

    private String e2mFaultCode;

    private String e2mFaultDescription;

    private String e2mPriorityCode;

    private String e2mPriority;

    public Long getTransmissionId() {
        return transmissionId;
    }

    public void setTransmissionId(Long transmissionId) {
        this.transmissionId = transmissionId;
    }

    public Long getFaultId() {
        return faultId;
    }

    public void setFaultId(Long faultId) {
        this.faultId = faultId;
    }

    public String getTransmissionStatus() {
        return transmissionStatus;
    }

    public void setTransmissionStatus(String transmissionStatus) {
        this.transmissionStatus = transmissionStatus;
    }

    public Integer getRetries() {
        return retries;
    }

    public void setRetries(Integer retries) {
        this.retries = retries;
    }

    public String getLocationCode() {
        return locationCode;
    }

    public void setLocationCode(String locationCode) {
        this.locationCode = locationCode;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public String getUnitNumber() {
        return unitNumber;
    }

    public void setUnitNumber(String unitNumber) {
        this.unitNumber = unitNumber;
    }

    public String getVehicleNumber() {
        return vehicleNumber;
    }

    public void setVehicleNumber(String vehicleNumber) {
        this.vehicleNumber = vehicleNumber;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getE2mFaultCode() {
        return e2mFaultCode;
    }

    public void setE2mFaultCode(String e2mFaultCode) {
        this.e2mFaultCode = e2mFaultCode;
    }

    public String getE2mFaultDescription() {
        return e2mFaultDescription;
    }

    public void setE2mFaultDescription(String e2mFaultDescription) {
        this.e2mFaultDescription = e2mFaultDescription;
    }

    public String getE2mPriorityCode() {
        return e2mPriorityCode;
    }

    public void setE2mPriorityCode(String e2mPriorityCode) {
        this.e2mPriorityCode = e2mPriorityCode;
    }

    public String getE2mPriority() {
        return e2mPriority;
    }

    public void setE2mPriority(String e2mPriority) {
        this.e2mPriority = e2mPriority;
    }
}
