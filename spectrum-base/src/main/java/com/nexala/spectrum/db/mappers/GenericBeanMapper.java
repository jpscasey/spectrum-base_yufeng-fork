package com.nexala.spectrum.db.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.nexala.spectrum.db.JdbcRowMapper;
import com.nexala.spectrum.db.dao.BoxedResultSet;
import com.nexala.spectrum.db.dao.BoxedResultSetFactory;
import com.nexala.spectrum.rest.data.beans.BeanField;
import com.nexala.spectrum.rest.data.beans.GenericBean;

/**
 * Mapper for a generic bean
 * @author Fulvio
 */
public class GenericBeanMapper extends JdbcRowMapper<GenericBean> {

    protected final List<BeanField> fields;

    protected final BoxedResultSetFactory factory;
    
    public GenericBeanMapper(List<BeanField> fields, BoxedResultSetFactory factory) {
        this.fields = fields;
        this.factory = factory;
    }
    
    @Override
    public GenericBean createRow(ResultSet r) throws SQLException {

        GenericBean record = new GenericBean();
        BoxedResultSet rs = factory.create(r);
        
        for (BeanField field : fields) {
            String fieldId = field.getId();
            String dbName = field.getDbName();
            String type = field.getType();
            record.setField(fieldId, extractField(rs, dbName, type));
        }
        
        return record;
    }
}
