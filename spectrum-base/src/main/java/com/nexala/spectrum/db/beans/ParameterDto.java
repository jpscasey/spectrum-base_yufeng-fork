package com.nexala.spectrum.db.beans;

import java.io.Serializable;

public class ParameterDto implements Serializable {
	private static final long serialVersionUID = -5841519135805467612L;

	private String name;
	
	private String value;
	
	private String type;
	
	public ParameterDto() {
		
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getType() {
		return type;
	}
}
