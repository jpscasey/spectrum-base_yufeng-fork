/*
 * Copyright (c) Nexala Technologies 2010, All rights reserved.
 */
package com.nexala.spectrum.db.dao;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.google.inject.Inject;
import com.nexala.spectrum.db.DbUtil;
import com.nexala.spectrum.db.RowMapper;
import com.nexala.spectrum.utils.DateUtils;

/**
 * @author poriordan
 * @author sev
 * @param <T>
 * @param <ID>
 */
public abstract class GenericDao<T, ID extends Serializable> implements HasCrudOperations<T, ID> {
    protected final Logger logger = Logger.getLogger(this.getClass());

    @Inject
    private DateUtils dateUtils;

    @Inject
    private ConnectionProvider connectionProvider;
    
    public void setConnectionProvider(ConnectionProvider connectionProvider) {
    	this.connectionProvider = connectionProvider;
    }

    @Override
    public Connection getConnection() throws DataAccessException {
        return connectionProvider.getConnection();
    }

    @Override
    public String getTableName() {
        String className = this.getClass().getName();
        throw new UnsupportedOperationException("No table name defined for " + className
                + ", please override getTableName()");
    }

    /**
     * Required by findById
     * 
     * @return
     */
    public RowMapper<T> getRowMapper() {
        String className = this.getClass().getName();
        throw new UnsupportedOperationException("No row mapper defined for " + className
                + ", please override getRowMapper()");
    }

    @Override
    public String getIdColumnName() {
        String className = this.getClass().getName();
        throw new UnsupportedOperationException("No identity column name defined for " + className
                + ", please override getIdColumnName()");
    }

    @Override
    public T findById(ID id, RowMapper<T> rowMapper) throws DataAccessException {
        String query = String.format("SELECT * FROM %s WHERE %s = ?", this.getTableName(), this.getIdColumnName());
        List<T> list = this.findByQuery(query, rowMapper, id);
        if (list.size() > 0) {
            return list.get(0);
        } else {
            return null;
        }
    }

    public T findById(ID id) throws DataAccessException {
        return findById(id, getRowMapper());
    }

    /**
     * Execute a SQL SELECT query without a WHERE clause. The table used in the query is determined by calling
     * getTableName.
     * 
     * @param rowMapper the row mapper that should be used to map the rows in the table to objects.
     * @return returns a list with all of the records in a table.
     */
    @Override
    public List<T> findAll(RowMapper<T> rowMapper) throws DataAccessException {
        String query = String.format("SELECT * FROM %s", this.getTableName());
        return this.findByQuery(query, rowMapper);
    }

    public List<T> findAll() throws DataAccessException {
        return findAll(getRowMapper());
    }

    /**
     * Executes a select query against the database.
     * 
     * @param rowMapper the RowMapper to use for result mapping.
     * @param query the SQL query to run.
     * @param parameters any parameters that should be added to the PreparedStatement before executing the query.
     * @return a list of objects mapped to the ResultSet returned by the query.
     */
    @Override
    public List<T> findByQuery(String query, RowMapper<T> rowMapper, Object... parameters) throws DataAccessException {
        List<T> results = rowMapper.newList();
        Connection c = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            c = this.getConnection();
            logger.debug(String.format("Executing query: %s", query.replaceAll("[\\n\\s]+", " ")));
            ps = c.prepareStatement(query);
            int i = 1;
            for (Object parameter : parameters) {
                this.addParameterToPreparedStatement(ps, i++, parameter);
            }
            rs = ps.executeQuery();
            results = rowMapper.createRows(rs);
        } catch (SQLException e) {
            throw new DataAccessException(e);
        } finally {
            DbUtil.closeResultSet(rs);
            DbUtil.closeStatement(ps);
            DbUtil.closeConnection(c);
        }
        logger.debug(String.format("%d rows returned", results.size()));
        return results;
    }

    public List<T> findByQuery(String query, Object... parameters) throws DataAccessException {
        return findByQuery(query, getRowMapper(), parameters);
    }

    /**
     * Execute an arbitrary SQL query that is contained in a PreparedStatement.
     * 
     * @param ps the prepared statement that contains the SQL query.
     * @return normally returns a List of type T. If an exception occurs, null is returned.
     */
    @Override
    public List<T> findByQuery(PreparedStatement ps, RowMapper<T> rowMapper) throws DataAccessException {
        List<T> results = null;

        Connection c = null;
        ResultSet rs = null;

        try {
            c = ps.getConnection();
            rs = ps.executeQuery();
            results = rowMapper.createRows(rs);
        } catch (SQLException e) {
            throw new DataAccessException(e);
        } finally {
            DbUtil.closeResultSet(rs);
            DbUtil.closeStatement(ps);
            DbUtil.closeConnection(c);
        }

        return results;
    }

    public List<T> findByQuery(PreparedStatement ps) throws DataAccessException {
        return findByQuery(ps, getRowMapper());
    }
    
    /**
     * Executes a data manipulation query against the database (INSERT, UPDATE or DELETE).
     * 
     * @param rowMapper the RowMapper to use for result mapping.
     * @param query the SQL query to run.
     * @param parameters any parameters that should be added to the PreparedStatement before executing the query.
     * @return either (1) the row count for SQL Data Manipulation Language (DML) statements or (2) 0 for SQL statements that return nothing.
     */
    @Override
    public Integer executeUpdateByQuery(String query, Object... parameters) throws DataAccessException {
        Connection c = null;
        PreparedStatement ps = null;
        Integer result = null;
        try {
            c = this.getConnection();
            logger.debug(String.format("Executing query: %s", query.replaceAll("[\\n\\s]+", " ")));
            ps = c.prepareStatement(query);
            int i = 1;
            for (Object parameter : parameters) {
                this.addParameterToPreparedStatement(ps, i++, parameter);
            }
            result = ps.executeUpdate();
        } catch (SQLException e) {
            throw new DataAccessException(e);
        } finally {
            DbUtil.closeStatement(ps);
            DbUtil.closeConnection(c);
        }
        
        return result;}

    /**
     * Executes the specified stored procedure. This method should only be used if the stored procedure does not return
     * a result set.
     * 
     * @param query
     * @param parameters
     * @throws DataAccessException
     */
    public void executeStoredProcedure(String query, Object... parameters) throws DataAccessException {
        Connection c = null;
        CallableStatement cs = null;

        try {
            logger.debug(String.format("Executing stored procedure: %s", query.replaceAll("[\\n\\s]+", " ")));
            c = getConnection();
            cs = c.prepareCall(query);
            addParametersToPreparedStatement(cs, parameters);
            cs.execute();
        } catch (SQLException e) {
            throw new DataAccessException(e);
        } finally {
            DbUtil.closeStatement(cs);
            DbUtil.closeConnection(c);
        }
    }

    /**
     * Executes the specified stored procedure and creates a List<T> using the specified RowMapper<T> from the first
     * result set of the stored procedure. For example, the first result might be an update count and the second result
     * might be a result set.
     * 
     * @param query
     * @param rowMapper
     * @param parameters
     * @return
     */
    @Override
    public List<T> findByStoredProcedure(String query, RowMapper<T> rowMapper, Object... parameters)
            throws DataAccessException {
        return findByStoredProcedureWithTimeout(query, rowMapper, null, parameters);
    }
    
    /**
     * Executes the specified stored procedure and creates a List<T> using the specified RowMapper<T> from the first
     * result set of the stored procedure. For example, the first result might be an update count and the second result
     * might be a result set.
     * 
     * @param query
     * @param rowMapper
     * @param timeout timeout in seconds
     * @param parameters
     * @return
     */
    public List<T> findByStoredProcedureWithTimeout(String query, RowMapper<T> rowMapper, Integer timeout, Object... parameters)
            throws DataAccessException {
        List<T> results = new ArrayList<T>();
        Connection c = null;
        CallableStatement cs = null;
        ResultSet resultSet = null;

        try {
            c = getConnection();

            logger.debug(String.format("Executing stored procedure: %s", query.replaceAll("[\\n\\s]+", " ")));
            cs = c.prepareCall(query);
            if (timeout != null) {
                cs.setQueryTimeout(timeout);
            }
            
            addParametersToPreparedStatement(cs, parameters);

            resultSet = cs.executeQuery();
            results = rowMapper.createRows(resultSet);
        } catch (SQLException e) {
            throw new DataAccessException(e);
        } finally {
            DbUtil.closeResultSet(resultSet);
            DbUtil.closeStatement(cs);
            DbUtil.closeConnection(c);
        }

        return results;
    }
    
    public Integer getIntByStoredProcedure(String query, Object... parameters)
            throws DataAccessException {
        Connection c = null;
        CallableStatement cs = null;
        ResultSet resultSet = null;
        Integer result = 0;

        try {
            c = getConnection();

            logger.debug(String.format("Executing stored procedure: %s", query.replaceAll("[\\n\\s]+", " ")));
            cs = c.prepareCall(query);

            addParametersToPreparedStatement(cs, parameters);

            resultSet = cs.executeQuery();
            if (resultSet.next()) {
                result = resultSet.getInt(1);
            }
            
        } catch (SQLException e) {
            throw new DataAccessException(e);
        } finally {
            DbUtil.closeResultSet(resultSet);
            DbUtil.closeStatement(cs);
            DbUtil.closeConnection(c);
        }

        return result;
    }
    
    public List<String> getStringListByQuery(String query, Object... parameters)
            throws DataAccessException {
        Connection c = null;
        PreparedStatement ps = null;
        ResultSet resultSet = null;
        List<String> result = new ArrayList<>();

        try {
            c = this.getConnection();
            logger.debug(String.format("Executing query: %s", query.replaceAll("[\\n\\s]+", " ")));
            ps = c.prepareStatement(query);
            int i = 1;
            for (Object parameter : parameters) {
                this.addParameterToPreparedStatement(ps, i++, parameter);
            }

            resultSet = ps.executeQuery();
            while (resultSet.next()) {
                result.add(resultSet.getString(1));
            }
            
        } catch (SQLException e) {
            throw new DataAccessException(e);
        } finally {
            DbUtil.closeResultSet(resultSet);
            DbUtil.closeStatement(ps);
            DbUtil.closeConnection(c);
        }

        return result;
    }
    
    public Date getDateByQuery(String query, Object... parameters)
            throws DataAccessException {
        Connection c = null;
        PreparedStatement ps = null;
        ResultSet resultSet = null;
        Date result = null;

        try {
            c = this.getConnection();
            logger.debug(String.format("Executing query: %s", query.replaceAll("[\\n\\s]+", " ")));
            ps = c.prepareStatement(query);
            int i = 1;
            for (Object parameter : parameters) {
                this.addParameterToPreparedStatement(ps, i++, parameter);
            }

            resultSet = ps.executeQuery();
            if (resultSet.next()) {
                result = resultSet.getTimestamp(1, dateUtils.getDatabaseCalendar());
            }
            
        } catch (SQLException e) {
            throw new DataAccessException(e);
        } finally {
            DbUtil.closeResultSet(resultSet);
            DbUtil.closeStatement(ps);
            DbUtil.closeConnection(c);
        }
        return result;
    }

    public T findByStoredProcedure(String query, CallableStatementCallback<T> callback, Object... parameters)
            throws DataAccessException {
        T result = null;

        Connection c = null;
        CallableStatement cs = null;
        ResultSet rs = null;

        try {
            c = this.getConnection();
            logger.debug(String.format("Executing stored procedure: %s", query.replaceAll("[\\n\\s]+", " ")));
            cs = c.prepareCall(query);
            int i = 1;
            for (Object parameter : parameters) {
                this.addParameterToPreparedStatement(cs, i++, parameter);
            }

            result = callback.execute(cs);
        } catch (SQLException e) {
            throw new DataAccessException(e);
        } finally {
            DbUtil.closeResultSet(rs);
            DbUtil.closeStatement(cs);
            DbUtil.closeConnection(c);
        }

        return result;
    }

    public List<T> findByStoredProcedure(String query, Object... parameters) throws DataAccessException {
        return findByStoredProcedure(query, getRowMapper(), parameters);
    }
    
    public Date getDateByStoredProcedure(String query, Object... parameters)
            throws DataAccessException {
        Connection c = null;
        CallableStatement cs = null;
        ResultSet resultSet = null;
        Date result = null;

        try {
            c = getConnection();

            logger.debug(String.format("Executing stored procedure: %s", query.replaceAll("[\\n\\s]+", " ")));
            cs = c.prepareCall(query);

            addParametersToPreparedStatement(cs, parameters);

            resultSet = cs.executeQuery();
            if (resultSet.next()) {
                result = resultSet.getTimestamp(1, dateUtils.getDatabaseCalendar());
            }
            
        } catch (SQLException e) {
            throw new DataAccessException(e);
        } finally {
            DbUtil.closeResultSet(resultSet);
            DbUtil.closeStatement(cs);
            DbUtil.closeConnection(c);
        }

        return result;
    }
    
    
    @Override
    public ID create(T b) throws DataAccessException {
        String className = this.getClass().getName();
        throw new UnsupportedOperationException("The create (T b) method is not implemented for " + className);
    }

    @Override
    public ID create(Map<String, Object> columnValues) throws DataAccessException {
        ID id = null;

        Connection c = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            c = this.getConnection();

            String[] columnArray = new String[columnValues.keySet().size()];
            columnArray = columnValues.keySet().toArray(columnArray);
            StringBuilder columnBuilder = new StringBuilder();
            StringBuilder valueBuilder = new StringBuilder();

            for (String column : columnArray) {
                columnBuilder.append(String.format(", [%s]", column));
                valueBuilder.append(", ?");
            }

            String columns = columnBuilder.toString();
            String values = valueBuilder.toString();

            if (!columns.equals("")) {
                columns = columns.substring(2, columns.length());
            }

            if (!values.equals("")) {
                values = values.substring(2, values.length());
            }

            String query = String.format("INSERT INTO %s (%s) VALUES (%s)", this.getTableName(), columns, values);

            ps = c.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);

            int i = 1;

            for (String column : columnArray) {
                this.addParameterToPreparedStatement(ps, i++, columnValues.get(column));
            }

            ps.executeUpdate();

            rs = ps.getGeneratedKeys();
            while (rs.next()) {
                // FIXME This seems to return a BigDecimal for int fields. Need
                // to do something to solve this.
                id = (ID) rs.getObject(1);
            }
        } catch (SQLException e) {
            throw new DataAccessException(e);
        } finally {
            DbUtil.closeResultSet(rs);
            DbUtil.closeStatement(ps);
            DbUtil.closeConnection(c);
        }

        return id;
    }

    @Override
    public void update(ID id, T b) throws DataAccessException {
        String className = this.getClass().getName();
        throw new UnsupportedOperationException(String.format(
                "The update(ID id, T b) method is not implemented for %s", className));
    }

    /**
     * Executes an update query.
     * 
     * @param id the ID of the record that will be updated.
     * @param columnValues a map that contains column names which will be included in the update statement and the
     *        values that those columns should be set to.
     */
    @Override
    public void update(ID id, Map<String, Object> columnValues) throws DataAccessException {
        Connection c = null;
        PreparedStatement ps = null;
        try {
            c = this.getConnection();

            String query = String.format("UPDATE %s SET ", this.getTableName());
            StringBuilder queryBuilder = new StringBuilder(query);

            String[] columnArray = new String[columnValues.keySet().size()];
            columnArray = columnValues.keySet().toArray(columnArray);
            StringBuilder updateClauseBuilder = new StringBuilder();

            for (String column : columnArray) {
                updateClauseBuilder.append(String.format(", %s = ?", column));
            }

            String updateClause = updateClauseBuilder.toString();

            if (!updateClause.equals("")) {
                updateClause = updateClause.substring(2, updateClause.length());
            }

            String whereClause = String.format(" WHERE %s = ? ", this.getIdColumnName());

            queryBuilder.append(updateClause).append(whereClause);
            query = queryBuilder.toString();

            ps = c.prepareStatement(query);

            int i = 1;

            for (String column : columnArray) {
                this.addParameterToPreparedStatement(ps, i++, columnValues.get(column));
            }

            this.addParameterToPreparedStatement(ps, i++, id);

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DataAccessException(e);
        } finally {
            DbUtil.closeStatement(ps);
            DbUtil.closeConnection(c);
        }
    }

    /**
     * Executes a delete query.
     * 
     * @param id the ID of the record that will be deleted.
     */
    @Override
    public void delete(ID id) throws DataAccessException {
        Connection c = null;
        PreparedStatement ps = null;
        try {
            c = this.getConnection();

            String query = "DELETE FROM %s WHERE %s = ?";
            query = String.format(query, this.getTableName(), this.getIdColumnName());

            ps = c.prepareStatement(query);
            this.addParameterToPreparedStatement(ps, 1, id);

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DataAccessException(e);
        } finally {
            DbUtil.closeStatement(ps);
            DbUtil.closeConnection(c);
        }
    }
    
    /**
     * Execute queries in a transaction
     * @param queries
     * @param parameters
     * @return result int The returned value indicate the result of the transaction: 1 - succeed | 0 - no change is made | -1 - failed
     * @throws DataAccessException
     */
    public int executeQueriesInTransaction(List<String> queries, List<Object> parameters) throws DataAccessException {
    	int result = 0;
    	
    	Connection c = null;
    	PreparedStatement ps = null;
    	ResultSet rs = null;
    	
    	if (queries.size() == 0) {
    		return result;
    	}
    	
    	try {
    		c = this.getConnection();
    		
    		StringBuilder queryBuilder = new StringBuilder();
    		
    		// Begin of the transaction
    		queryBuilder.append("DECLARE @Result INT = 0");
    		queryBuilder.append(System.getProperty("line.separator"));
    		queryBuilder.append("BEGIN TRY");
    		queryBuilder.append(System.getProperty("line.separator"));
    		queryBuilder.append("BEGIN TRANSACTION");
    		queryBuilder.append(System.getProperty("line.separator"));
    		
    		for (String query : queries) {
    			queryBuilder.append(query);
    			queryBuilder.append(System.getProperty("line.separator"));
    		}
    		
    		queryBuilder.append("IF @@TRANCOUNT > 0");
    		queryBuilder.append(System.getProperty("line.separator"));
    		
    		queryBuilder.append("SET @Result = 1");
    		queryBuilder.append(System.getProperty("line.separator"));
    		
    		// End of the transaction
    		queryBuilder.append("COMMIT TRANSACTION");
    		queryBuilder.append(System.getProperty("line.separator"));
    		
    		queryBuilder.append("END TRY");
    		queryBuilder.append(System.getProperty("line.separator"));
    		
    		queryBuilder.append("BEGIN CATCH");
    		queryBuilder.append(System.getProperty("line.separator"));
    		
    		queryBuilder.append("IF @@TRANCOUNT > 0");
    		queryBuilder.append(System.getProperty("line.separator"));
    		
    		queryBuilder.append("BEGIN");
    		queryBuilder.append(System.getProperty("line.separator"));
    		
    		queryBuilder.append("ROLLBACK TRANSACTION");
    		queryBuilder.append(System.getProperty("line.separator"));
    		
    		queryBuilder.append("SET @Result = -1");
    		queryBuilder.append(System.getProperty("line.separator"));
    		
    		queryBuilder.append("END");
    		queryBuilder.append(System.getProperty("line.separator"));
    		
    		queryBuilder.append("END CATCH");
    		queryBuilder.append(System.getProperty("line.separator"));
    		
    		queryBuilder.append("SELECT @Result AS result");
    		queryBuilder.append(System.getProperty("line.separator"));
    		
    		ps = c.prepareStatement(queryBuilder.toString());

    		// bind parameters
    		for (int i = 0;i < parameters.size();i++) {
    			this.addParameterToPreparedStatement(ps, i + 1, parameters.get(i));
    		}
    		
    		rs = ps.executeQuery();
    		if (rs.next()) {
    			result = rs.getInt(1);
    		}
    		
    	} catch (SQLException e) {
    		throw new DataAccessException(e);
    	} finally {
    		DbUtil.closeResultSet(rs);
    		DbUtil.closeStatement(ps);
    		DbUtil.closeConnection(c);
    	}
    	
    	return result;
    }

    private final void addParametersToPreparedStatement(PreparedStatement ps, Object... parameters) throws SQLException {
        int i = 1;
        for (Object obj : parameters) {
            addParameterToPreparedStatement(ps, i++, obj);
        }
    }

    // This method should either be private, or should not propagate
    // SQLException

    /**
     * Adds a parameter to a prepared statement.
     * 
     * @param ps the prepared statement.
     * @param index the index of the parameter.
     * @param parameter the value of the parameter. Accepts java.lang.Boolean, java.lang.Integer, java.lang.Long,
     *        java.lang.String, java.util.Date and java.math.BigDecimal values.
     * @throws SQLException if a database access error occurs.
     */
    protected final void addParameterToPreparedStatement(PreparedStatement ps, int index, Object parameter)
            throws SQLException {
        if (parameter != null) {
            this.logger.debug(String.format("Parameter %d: %s (%s)", index, parameter, parameter.getClass().getName()));
            if (parameter instanceof Boolean) {
                ps.setBoolean(index, (Boolean) parameter);
            } else if (parameter instanceof Integer) {
                ps.setInt(index, (Integer) parameter);
            } else if (parameter instanceof Long) {
                ps.setLong(index, (Long) parameter);
            } else if (parameter instanceof String) {
                ps.setString(index, (String) parameter);
            } else if (parameter instanceof Date) {
                Timestamp t = new Timestamp(((Date) parameter).getTime());
                ps.setTimestamp(index, t, dateUtils.getDatabaseCalendar());
            } else if (parameter instanceof Double) {
                ps.setDouble(index, (Double) parameter);
            } else if (parameter instanceof BigDecimal) {
                ps.setBigDecimal(index, (BigDecimal) parameter);
            } else {
                String className = parameter.getClass().getName();
                throw new IllegalArgumentException(String.format("Cannot add "
                        + "an instance of %s to the prepared statement. "
                        + "Only java.lang.Boolean, java.lang.Integer, " + "java.lang.Long, java.lang.String, "
                        + "java.lang.Double, java.util.Date and " + "java.math.BigDecimal values are allowed",
                        className));
            }
        } else {
            this.logger.debug(String.format("Parameter %d: null", index));
            ps.setString(index, null);
        }
    }
}