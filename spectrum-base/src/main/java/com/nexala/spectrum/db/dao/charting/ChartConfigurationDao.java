package com.nexala.spectrum.db.dao.charting;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.google.inject.Inject;
import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.db.DbUtil;
import com.nexala.spectrum.db.QueryManager;
import com.nexala.spectrum.db.RowMapper;
import com.nexala.spectrum.db.dao.DataAccessException;
import com.nexala.spectrum.db.dao.GenericDao;
import com.nexala.spectrum.licensing.Licence;

public class ChartConfigurationDao extends
        GenericDao<DataViewConfiguration, Long> {

    @Inject
    private ChartConfigurationMapper mapper;

    @Inject
    private QueryManager qm;

    public List<DataViewConfiguration> findUserConfigurations(Licence licence) {
        String query = qm.getQuery(Spectrum.USER_CHART_CONFIGURATIONS);

        if (licence.getLoginUser() != null) {
            return findByQuery(query, mapper, licence.getLoginUser().getLogin());
        } else {
            return new ArrayList<DataViewConfiguration>();
        }
    }

    public List<DataViewConfiguration> findSystemConfigurations() {
        String query = qm.getQuery(Spectrum.SYSTEM_CHART_CONFIGURATIONS);
        return findByQuery(query, mapper);
    }

    public DataViewConfiguration findChart(Long chartId, String chartName,
            String username) {
        String query = qm.getQuery(Spectrum.CHART_BY_ID_AND_NAME);

        List<DataViewConfiguration> configs = findByQuery(query, mapper,
                chartId, chartName, username);

        if (configs.size() > 0) {
            return configs.get(0);
        } else {
            return null;
        }
    }

    public DataViewConfiguration createChart(String name,
            List<Integer> channelIds, String userName)
            throws DataAccessException {

        Integer chartId = null;

        String createChart = qm.getQuery(Spectrum.CREATE_CHART);

        String insertChannel = qm.getQuery(Spectrum.INSERT_CHANNEL);

        Connection c = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            c = this.getConnection();
            c.setAutoCommit(false);

            ps = c.prepareStatement(createChart,
                    Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, userName);
            ps.setString(2, name);
            ps.executeUpdate();

            // Get the ID of the newly inserted chart.
            rs = ps.getGeneratedKeys();

            while (rs.next()) {
                chartId = rs.getInt(1);
            }

            ps.close();

            ps = c.prepareStatement(insertChannel);

            for (int i = 0, l = channelIds.size(); i < l; i++) {
                Integer channelId = channelIds.get(i);
                ps.setLong(1, chartId);
                ps.setInt(2, channelId);
                ps.setDouble(3, 1D);
                ps.setInt(4, i + 1);
                ps.executeUpdate();
            }

            c.commit();
        } catch (SQLException e) {
            throw new DataAccessException(e);
        } finally {
            DbUtil.closeResultSet(rs);
            DbUtil.closeStatement(ps);
            DbUtil.closeConnection(c);
        }

        // private Long id;
        // private String name;
        // private boolean system;
        // private Long userId;

        DataViewConfiguration config = new DataViewConfiguration();
        config.setId((long) chartId);
        config.setName(name);
        config.setUserName(userName);

        return config;
    }

    public void saveChart(Long chartId, List<Integer> channelIds, String userId)
            throws DataAccessException {
        String deleteChannels = qm.getQuery(Spectrum.DELETE_CHART_CHANNELS);

        String insertChannel = qm.getQuery(Spectrum.INSERT_CHANNEL);

        Connection c = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            c = this.getConnection();

            // All of the queries are wrapped in a transaction
            c.setAutoCommit(false);

            // delete the chart channels.
            ps = c.prepareStatement(deleteChannels);
            ps.setLong(1, chartId);
            ps.setString(2, userId);
            ps.executeUpdate();
            ps.close();

            // Insert the channels..
            ps = c.prepareStatement(insertChannel);

            for (int i = 0, l = channelIds.size(); i < l; i++) {
                Integer channelId = channelIds.get(i);
                ps.setLong(1, chartId);
                ps.setInt(2, channelId);
                ps.setDouble(3, 1D);
                ps.setInt(4, i + 1);
                ps.executeUpdate();
            }

            c.commit();
        } catch (SQLException e) {
            throw new DataAccessException(e);
        } finally {
            DbUtil.closeResultSet(rs);
            DbUtil.closeStatement(ps);
            DbUtil.closeConnection(c);
        }
    }

    public void deleteChart(Long configurationId, String userId)
            throws DataAccessException {
        String deleteChannels = qm.getQuery(Spectrum.DELETE_CHART_CHANNELS);

        String deleteChart = qm.getQuery(Spectrum.DELETE_CHART);

        Connection c = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            c = this.getConnection();

            // All of the queries are wrapped in a transaction
            c.setAutoCommit(false);

            ps = c.prepareStatement(deleteChannels);
            ps.setLong(1, configurationId);
            ps.setString(2, userId);
            ps.executeUpdate();
            ps.close();

            ps = c.prepareStatement(deleteChart);
            ps.setLong(1, configurationId);
            ps.setString(2, userId);
            ps.executeUpdate();
            ps.close();

            c.commit();
        } catch (SQLException e) {
            throw new DataAccessException(e);
        } finally {
            DbUtil.closeResultSet(rs);
            DbUtil.closeStatement(ps);
            DbUtil.closeConnection(c);
        }
    }
    
    @Override
    public RowMapper<DataViewConfiguration> getRowMapper() {
    	return mapper;
    }
    
    @Override
    public String getIdColumnName() {
    	return "id";
    }
    
    @Override
    public String getTableName() {
    	return "Chart";
    }

    public void renameChart(Long chartId, String newName, String userId)
            throws DataAccessException {
        String renameChart = qm.getQuery(Spectrum.RENAME_CHART);

        Connection c = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            c = this.getConnection();
            ps = c.prepareStatement(renameChart);
            ps.setString(1, newName);
            ps.setLong(2, chartId);
            ps.setString(3, userId);
            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            throw new DataAccessException(e);
        } finally {
            DbUtil.closeResultSet(rs);
            DbUtil.closeStatement(ps);
            DbUtil.closeConnection(c);
        }
    }
}
