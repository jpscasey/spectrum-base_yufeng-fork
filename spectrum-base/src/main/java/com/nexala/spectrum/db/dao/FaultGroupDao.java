package com.nexala.spectrum.db.dao;

import java.util.List;

import com.nexala.spectrum.db.JdbcRowMapper;
import com.nexala.spectrum.db.beans.FaultGroup;
import com.nexala.spectrum.db.mappers.FaultGroupMapper;

public class FaultGroupDao extends GenericDao<FaultGroup, String> {
    private FaultGroupMapper mapper = new FaultGroupMapper();
    
    public List<FaultGroup> getFaultGroupList() {
        String query = "SELECT ID, Name FROM FaultGroup";
        List<FaultGroup> faultTypeList = findByQuery(query, mapper);
        return faultTypeList;
    }

    @Override
    public JdbcRowMapper<FaultGroup> getRowMapper() {
        return mapper;
    }
    
    public String getTableName() {
        return "FaultGroup";
    }

    public String getIdColumnName() {
        return "ID";
    }
}
