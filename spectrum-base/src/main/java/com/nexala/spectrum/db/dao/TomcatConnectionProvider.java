/*
 * Copyright (c) Nexala Technologies 2013, All rights reserved.
 *
 * @author dclerkin
 * @date 15/11/2013
 */
package com.nexala.spectrum.db.dao;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.tomcat.jdbc.pool.DataSource;
import org.apache.tomcat.jdbc.pool.PoolProperties;

import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
public class TomcatConnectionProvider implements ConnectionProvider {
    private static DataSource datasource;
    private static final String DRIVER = "telemetryservice.ncu.update.db.driver";
    private static final String URL = "telemetryservice.ncu.update.db.url";
    private static final String USERNAME = "telemetryservice.ncu.update.db.username";
    private static final String PASSWORD = "telemetryservice.ncu.update.db.password";

    @Inject
    public TomcatConnectionProvider(TomcatConnectionProperties p) {
        PoolProperties props = configurePoolProperties(p.get(DRIVER), p.get(URL), p.get(USERNAME), p.get(PASSWORD));
        datasource = new DataSource(props);
    }

    private final PoolProperties configurePoolProperties(String driver, String url, String username, String password) {
        PoolProperties properties = new PoolProperties();
        properties.setDriverClassName(driver);
        properties.setUrl(url);
        properties.setUsername(username);
        properties.setPassword(password);
        return properties;
    }

    public final synchronized Connection getConnection() throws DataAccessException {
        try {
            return datasource.getConnection();
        } catch (SQLException ex) {
            throw new DataAccessException(ex);
        }
    }
}
