package com.nexala.spectrum.db.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.nexala.spectrum.db.JdbcRowMapper;
import com.nexala.spectrum.db.beans.FaultGroup;

public class FaultGroupMapper extends JdbcRowMapper<FaultGroup> {

    /**
     * {@inheritDoc}
     */
    @Override
    public FaultGroup createRow(ResultSet rs) throws SQLException {
        return new FaultGroup(
                rs.getInt("ID"),
                rs.getString("Name"));
    }
}
