/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.db.beans;

public final class ChannelGroup {
    private int id;
    private String description;
    private boolean visible;
    
    public ChannelGroup(int id, boolean visible, String description) {
        this.id = id;
        this.visible = visible;
        this.description = description;
    }
    
    public int getId() {
        return id;
    }
    
    public boolean isVisible() {
        return visible;
    }
    
    public String getDescription() {
        return description;
    }
}