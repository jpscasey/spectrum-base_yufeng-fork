/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.db.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.google.inject.Inject;
import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.db.JdbcRowMapper;
import com.nexala.spectrum.db.QueryManager;

public class FleetChannelDao extends GenericDao<Integer, Long> {

    @Inject
    private QueryManager queries;

    @Inject
    private BoxedResultSetFactory factory;

    private final JdbcRowMapper<Integer> mapper = new JdbcRowMapper<Integer>() {
        @Override
        public Integer createRow(ResultSet resultSet) throws SQLException {
            BoxedResultSet rs = factory.create(resultSet);
            return rs.getInt("ID");
        }
    };

    public List<Integer> getInvalidChannels(String fleet) {
        return this.findByQuery(queries.getQuery(Spectrum.INVALID_FLEET_CHANNEL_QUERY), mapper, fleet);
    }

}