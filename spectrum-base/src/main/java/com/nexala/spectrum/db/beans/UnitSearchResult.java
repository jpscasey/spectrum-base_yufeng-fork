/*
 * Copyright (c) Nexala Technologies 2013, All rights reserved.
 */
package com.nexala.spectrum.db.beans;

public final class UnitSearchResult extends Unit {
    private final String headcode;
    
    public UnitSearchResult(Integer id, String unitNumber, String unitType, String headcode) {
        if (id == null) {
            throw new NullPointerException("id may not be null");
        }

        super.setId(id);
        super.setUnitNumber(unitNumber);
        super.setUnitType(unitType);
        this.headcode = headcode;
    }

    public String getHeadcode() {
        return headcode;
    }
}
