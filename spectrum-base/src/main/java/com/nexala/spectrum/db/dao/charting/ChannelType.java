package com.nexala.spectrum.db.dao.charting;

public enum ChannelType {
    DIGITAL("Digital"),
    ANALOG("Analog"),
    BINARY("Binary"),
    HEXADECIMAL("Hexadecimal"),
    CALCULATED("Calculated"),
    TIMESTAMP("Timestamp"),
    TEXT("Text");

    private ChannelType(String name) {
        this.name = name;
    }

    private String name;

    public String toString() {
        return this.name;
    }

    /**
     * This method takes a type name and returns the enum associated with it
     * @param typeName the name of the channel type
     * @return the channel type associated with a given type name. If no such
     *         type is found, null is returned
     */
    public static ChannelType fromString(String typeName) {
        ChannelType type = null;
        for (ChannelType ct: ChannelType.values()) {
            if (ct.toString().equals(typeName)) {
                type = ct;
                break;
            }
        }
        return type;
    }
}
