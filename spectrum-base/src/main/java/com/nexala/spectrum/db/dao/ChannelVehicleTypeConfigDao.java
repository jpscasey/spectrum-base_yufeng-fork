package com.nexala.spectrum.db.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.google.inject.Inject;
import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.db.JdbcRowMapper;
import com.nexala.spectrum.db.QueryManager;
import com.nexala.spectrum.db.beans.ChannelVehicleTypeConfig;

public class ChannelVehicleTypeConfigDao extends GenericDao<ChannelVehicleTypeConfig, Integer> {

    private static class ChannelVehicleTypeConfigMapper extends JdbcRowMapper<ChannelVehicleTypeConfig> {

        @Inject
        private BoxedResultSetFactory factory;
        
        @Override
        public ChannelVehicleTypeConfig createRow(ResultSet resultSet)
                throws SQLException {
            BoxedResultSet rs = factory.create(resultSet);

            ChannelVehicleTypeConfig channelVehicleTypeConfig = new ChannelVehicleTypeConfig();
            
            channelVehicleTypeConfig.setChannelId(rs.getInt("ChannelID"));
            channelVehicleTypeConfig.setVehicleTypeId(rs.getInt("VehicleTypeID"));
            
            return channelVehicleTypeConfig;
        }
        
    }
    
    private final ChannelVehicleTypeConfigMapper mapper;

    @Inject
    public ChannelVehicleTypeConfigDao(ChannelVehicleTypeConfigMapper mapper) {
        this.mapper = mapper;
    }
    
    @Override
    public List<ChannelVehicleTypeConfig> findAll() {
        String query = new QueryManager().getQuery(Spectrum.CHANNEL_VEHICLE_TYPE_CONFIG_QUERY);
        return this.findByQuery(query, mapper);
    }
    
}
