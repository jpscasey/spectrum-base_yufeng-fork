/*
 * Copyright (c) Nexala Technologies 2010, All rights reserved.
 */
package com.nexala.spectrum.db;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Source;
import javax.xml.transform.sax.SAXSource;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public abstract class BaseQueryManager {

    private static final Map<String, Map<String, String>> map =
            new ConcurrentHashMap<String, Map<String, String>>();
    
    public final String getQuery(String queryName) {
        
        String key = getKey();
        
        if (map.get(key) == null) {
            Map<String, String> queries = new ConcurrentHashMap<String, String>();
            parseQueryXmlFile(queries);
            map.put(key, queries);
        }

        return map.get(key).get(queryName);
    }

    private final InputStream getXsdResource() {
        // TODO add to Constants?
        return BaseQueryManager.class.getResourceAsStream("spectrum-sql.xsd");
    }

    /** Returns a unique name for storing the storing the queries in the cache */
    public abstract String getKey();
    
    public abstract InputStream getSqlResource();

    public final void parseQueryXmlFile(Map<String, String> queries) {
        Document doc = null;
        InputStream inputStream = getSqlResource();

        if (validateSqlXml(inputStream)) {

            // validateSqlXml closes the inputStream,
            // so need to reopen.
            inputStream = getSqlResource();

            DocumentBuilderFactory factory = DocumentBuilderFactory
                    .newInstance();
            factory.setNamespaceAware(true);

            try {
                DocumentBuilder builder = factory.newDocumentBuilder();
                doc = builder.parse(inputStream);
            } catch (ParserConfigurationException e) {
                e.printStackTrace();
            } catch (SAXException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (doc == null) {
                return;
            }

            try {
                XPath xpath = XPathFactory.newInstance().newXPath();
                NodeList nodes = (NodeList) xpath.evaluate("/queries/query",
                        doc, XPathConstants.NODESET);

                for (int i = 0; i < nodes.getLength(); i++) {
                    Node node = nodes.item(i);
                    String id = node.getAttributes().getNamedItem("id")
                            .getNodeValue();
                    String query = node.getTextContent();

                    if (id != null && query != null) {
                        queries.put(id, query.trim());
                    }
                }

            } catch (XPathExpressionException e) {
                e.printStackTrace();
            }
        } else {
            throw new RuntimeException("Invalid query XML file.");
        }
    }

    private final Schema getSchema() {

        InputStream input;
        StreamSource source;
        SchemaFactory schemaFactory = SchemaFactory
                .newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        Schema schema = null;

        input = getXsdResource();

        if (input != null) {
            source = new StreamSource(input);

            try {
                schema = schemaFactory.newSchema(source);
            } catch (SAXException e) {
                e.printStackTrace();
            }
        }

        return schema;
    }

    /**
     * Validates the supplied InputStream against the spectrum-sql.xsd schema.
     * 
     * Note that Validator.validate usage in this method closes the
     * InputStream, so it will need to be reopened after validating.
     * 
     * @param is
     * @return
     */
    public final boolean validateSqlXml(InputStream is) {

        boolean valid = false;
        Schema schema = getSchema();

        Validator validator = schema.newValidator();

        Source saxSource = new SAXSource(new InputSource(is));

        try {
            validator.validate(saxSource);
            valid = true;
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    throw new RuntimeException("Unable to close InputStream");
                }
            }
        }

        return valid;
    }
}
