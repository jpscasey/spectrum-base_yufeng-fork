package com.nexala.spectrum.db.beans;

import java.util.Date;

/**
 * Bean to hold a variable in the user configuration.
 * @author BBaudry
 *
 */
public class UserConfiguration {
    
    private String username;
    
    private Date timestamp;
    
    private String variable;
    
    private String data;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public String getVariable() {
        return variable;
    }

    public void setVariable(String variable) {
        this.variable = variable;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

}
