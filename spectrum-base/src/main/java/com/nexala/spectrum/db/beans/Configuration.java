package com.nexala.spectrum.db.beans;

import org.apache.log4j.Logger;

/**
 * Represent a configuration.
 * Associate a value to a key
 * @author bbaudry
 *
 */
public class Configuration {
    private final Logger logger = Logger.getLogger(Configuration.class);

    private final String name;
    private final String value;

    public Configuration(String name, String value) {
        this.name = name;
        this.value = value;
    }

    /**
     * Get the value as Boolean.
     * @return the value as Boolean.
     */
    public Boolean getBooleanValue() {
        if ("1".equals(value) || "0".equals(value)) {
            return new Boolean("1".equals(value));
        } else if (value != null && ("true".equals(value.toLowerCase()) 
                || "false".equals(value.toLowerCase()))) {
            return new Boolean("true".equals(value.toLowerCase()));
        } else {
            logger.warn(String.format("Invalid boolean value '%s' for configuration name '%s'", value, name));
            return null;
        }
    }
    
    /**
     * Get the value as Long.
     * @return the value as Long.
     */
    public Long getLongValue() {
        try {
            return Long.parseLong(value);
        } catch (NumberFormatException ex) {
            logger.warn(String.format("Invalid long value '%s' for configuration name '%s'", value, name));
        }
        return null;
    }    

    /**
     * Get the value as integer.
     * @return the value as integer
     */
    public Integer getIntegerValue() {
        try {
            return new Integer(value);
        } catch (NumberFormatException ex) {
            logger.warn(String.format("Invalid integer value '%s' for configuration name '%s'", value, name));
        }
        return null;
    }

    /**
     * Get the name.
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Get the value.
     * @return the value
     */
    public String getValue() {
        return value;
    }

}
