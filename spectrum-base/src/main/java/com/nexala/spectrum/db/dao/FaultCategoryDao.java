/*
 * Copyright (c) Nexala Technologies 2010, All rights reserved.
 */
package com.nexala.spectrum.db.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.List;

import com.nexala.spectrum.db.JdbcRowMapper;
import com.nexala.spectrum.db.beans.FaultCategory;

public class FaultCategoryDao extends GenericDao<FaultCategory, Integer> {
    private static final JdbcRowMapper<FaultCategory> MAPPER = new JdbcRowMapper<FaultCategory>() {
        @Override
        public FaultCategory createRow(ResultSet resultSet) throws SQLException {
            FaultCategory cat = new FaultCategory();
            cat.setId(resultSet.getInt("ID"));
            cat.setCategory(resultSet.getString("Category"));
            cat.setReportingOnly(resultSet.getBoolean("ReportingOnly"));
            return cat;
        }
    };

    private static final JdbcRowMapper<FaultCategory> MAPPER_LITE = new JdbcRowMapper<FaultCategory>() {
        @Override
        public FaultCategory createRow(ResultSet resultSet) throws SQLException {
            FaultCategory cat = new FaultCategory();
            cat.setId(resultSet.getInt("InsertedID"));
            return cat;
        }
    };
    
    /**
     * Gets a faultMeta record by its faultCode, returns null if no faultMeta record with the specified faultCode exists
     * 
     * @param faultCode
     * @return
     */
    public FaultCategory findByName(String name) throws DataAccessException {
        List<FaultCategory> faults = findByQuery("SELECT * FROM FaultCategory WHERE Category = ?", MAPPER,
                name);

        if (faults.size() > 0) {
            return faults.get(0);
        } else {
            return null;
        }
    }

    public Integer insert(FaultCategory t) throws DataAccessException {
        String query = "{ call NSP_InsertFaultCategory(?, ?) }";

        List<FaultCategory> liteFaultCategory = this.findByStoredProcedure(query, MAPPER_LITE, t.getCategory(),
                t.getReportingOnly());

        return liteFaultCategory.get(0).getId();
    }
    
    /**
     * @see com.nexala.spectrum.db.dao.GenericDao#create(java.lang.Object)
     */
    @Override
    public Integer create(FaultCategory b) throws DataAccessException {
        return insert(b);
    }

    @Override
    public void update(Integer id, FaultCategory t) throws DataAccessException {
        String query = "{ call NSP_UpdateFaultCategory(?, ?, ?) }";

        this.executeStoredProcedure(query, id, t.getCategory(), t.getReportingOnly());
    }

    @Override
    public void delete(Integer id) throws DataAccessException {
        String query = "{ call NSP_DeleteFaultCategory(?) }";

        this.executeStoredProcedure(query, id);
    }

    @Override
    public JdbcRowMapper<FaultCategory> getRowMapper() {
        return MAPPER;
    }

    @Override
    public String getTableName() {
        return "FaultCategory";
    }

    @Override
    public String getIdColumnName() {
        return "ID";
    }
    
    @Override
    public List<FaultCategory> findAll() throws DataAccessException {
        List<FaultCategory> result = findByQuery("SELECT * FROM FaultCategory ORDER BY Category", getRowMapper());
        Collections.sort(result);
        
        return result;
    }
}
