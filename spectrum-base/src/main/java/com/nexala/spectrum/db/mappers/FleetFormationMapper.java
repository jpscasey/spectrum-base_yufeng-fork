package com.nexala.spectrum.db.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.apache.log4j.Logger;

import com.nexala.spectrum.configuration.ApplicationConfiguration;
import com.nexala.spectrum.db.JdbcRowMapper;
import com.nexala.spectrum.db.beans.FleetFormation;

public class FleetFormationMapper extends JdbcRowMapper<FleetFormation> {
    
    private String separator;
    
    private static final Logger LOGGER = Logger.getLogger(FleetFormationMapper.class);
    
    @Inject
    public FleetFormationMapper(ApplicationConfiguration appConf) {
        separator = appConf.get().getString("r2m.fleetFormationSeparator");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public FleetFormation createRow(ResultSet rs) throws SQLException {
        FleetFormation result = new FleetFormation();
        result.setId(rs.getInt("ID"));
        String unitIdList = rs.getString("UnitIDList");
        
        if (unitIdList != null && !unitIdList.isEmpty()) {
            
                String[] unitIds = unitIdList.split(separator);
                List<Integer> unitIdArrayList = new ArrayList<>();
                
                for (String unit : unitIds) {
                    try {
                        if (!unit.isEmpty()) {
                            unitIdArrayList.add(Integer.parseInt(unit));
                        }
                    } catch (NumberFormatException e) {
                        LOGGER.error("invalid formation :"+unit+ "");
                    }
                }
                
                result.setUnitId(unitIdArrayList);
        }
        
        String unitNumberList = rs.getString("UnitNumberList");
        
        if (unitNumberList != null) {
            List<String> unitNumbersResultList = new ArrayList<>();
            String[] unitNumbers = unitNumberList.split(separator);
            
            for (String unitNumber : unitNumbers) {
                unitNumbersResultList.add(unitNumber.trim());
            }
            result.setUnitNumber(unitNumbersResultList);
        }
        
        result.setValidFrom(rs.getTimestamp("ValidFrom"));
        result.setValidTo(rs.getTimestamp("ValidTo"));
        
        return result;
    }
}
