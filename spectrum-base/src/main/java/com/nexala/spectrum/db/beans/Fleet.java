package com.nexala.spectrum.db.beans;

public class Fleet {
	private Integer id;
	private final String name;
    private final String code;

    public Fleet(Integer id, String name, String code) {
        this.id = id;
        this.name = name;
    	this.code = code;
    }
    
    public Integer getId() {
        return id;
    }
    
    public String getName() {
        return name;
    }

    public String getCode() {
        return code;
    }
}
