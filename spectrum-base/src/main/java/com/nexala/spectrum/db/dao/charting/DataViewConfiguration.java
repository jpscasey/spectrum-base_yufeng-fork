package com.nexala.spectrum.db.dao.charting;

import java.util.ArrayList;
import java.util.List;

import com.nexala.spectrum.db.dao.Record;

/**
 * Represents a record in the Chart table.
 * 
 * XXX This should probably be renamed to "Chart"
 */
public class DataViewConfiguration implements Record {
    private List<ChartChannel> channels;
    private Long id;
    private String name;
    private boolean system;
    private Integer ticks;
    private String userName;

    public DataViewConfiguration() {
        this.channels = new ArrayList<ChartChannel>();
    }

    public void addChannel(ChartChannel channel) {
        this.channels.add(channel);

        if (channel != null) {
            channel.setChartId(id);
        }
    }

    public List<ChartChannel> getChannels() {
        return channels;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }


    public boolean isSystem() {
        return system;
    }

    public void setChannels(List<ChartChannel> channels) {
        this.channels = channels;

        for (ChartChannel channel : channels) {
            if (channel != null) {
                channel.setChartId(id);
            }
        }
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSystem(boolean system) {
        this.system = system;
    }

	public Integer getTicks() {
		return ticks;
	}

	public void setTicks(Integer ticks) {
		this.ticks = ticks;
	}

    /**
     * Return the userName.
     * @return the userName
     */
    public String getUserName() {
        return userName;
    }

    /**
     * Setter for the userName.
     * @param userName the userName to set
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

}
