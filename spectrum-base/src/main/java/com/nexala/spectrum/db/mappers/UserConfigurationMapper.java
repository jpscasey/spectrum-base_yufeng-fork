package com.nexala.spectrum.db.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.google.inject.Inject;
import com.nexala.spectrum.db.JdbcRowMapper;
import com.nexala.spectrum.db.beans.UserConfiguration;
import com.nexala.spectrum.utils.DateUtils;

/**
 * Mapper for user configuration.
 * @author BBaudry
 *
 */
public class UserConfigurationMapper extends JdbcRowMapper<UserConfiguration> {
    
    @Inject
    private DateUtils dateUtils;

    /**
     * {@inheritDoc}
     */
    @Override
    public UserConfiguration createRow(ResultSet rs) throws SQLException {
        UserConfiguration result = new UserConfiguration();
        
        result.setUsername(rs.getString("Username"));
        result.setVariable(rs.getString("Variable"));
        result.setTimestamp(rs.getTimestamp("UpdateTimestamp", dateUtils.getDatabaseCalendar()));
        result.setData(rs.getString("Data"));
        
        return result;
    }
}
