package com.nexala.spectrum.db.beans;

import java.util.Collection;

import com.nexala.spectrum.db.dao.Record;

public class Unit implements Record {
    private Integer id;
    private String unitNumber;
    private String unitType;
    private int unitPosition;
    private int unitOrientation;
    private Collection<Vehicle> vehicles;

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUnitNumber() {
        return this.unitNumber;
    }

    public void setUnitNumber(String unitNumber) {
        this.unitNumber = unitNumber;
    }

    public String getUnitType() {
        return this.unitType;
    }

    public void setUnitType(String unitType) {
        this.unitType = unitType;
    }

    public int getUnitPosition() {
        return unitPosition;
    }

    public void setUnitPosition(int unitPosition) {
        this.unitPosition = unitPosition;
    }

    public Collection<Vehicle> getVehicles() {
        return vehicles;
    }

    public void setVehicles(Collection<Vehicle> vehicles) {
        this.vehicles = vehicles;
        for (Vehicle vehicle : vehicles) {
            if (vehicle != null) {
                vehicle.setUnitId(this.id);
            }
        }
    }

    public void addVehicle(Vehicle vehicle) {
        this.vehicles.add(vehicle);
        if (vehicle != null) {
            vehicle.setUnitId(this.id);
        }
    }

    public int getUnitOrientation() {
        return unitOrientation;
    }

    public void setUnitOrientation(int unitOrientation) {
        this.unitOrientation = unitOrientation;
    }
}
