/*
 * Copyright (c) Nexala Technologies 2017, All rights reserved.
 */

package com.nexala.spectrum.db.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.google.inject.Inject;
import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.configuration.ApplicationConfiguration;
import com.nexala.spectrum.db.JdbcRowMapper;
import com.nexala.spectrum.db.RowMapper;
import com.nexala.spectrum.db.beans.FaultMetaExtraField;

/**
 * Dao for the faultMetaExtraField table.
 * @author BBaudry
 *
 */
public class FaultMetaExtraFieldDao extends GenericDao<FaultMetaExtraField, Integer> {
    
    private FaultMetaExtraFieldMapper mapper;
    private ApplicationConfiguration appConf;

    @Inject
    public FaultMetaExtraFieldDao(FaultMetaExtraFieldMapper mapper, ApplicationConfiguration appConf) {
        this.mapper = mapper;
        this.appConf = appConf;
    }
    
    private static class FaultMetaExtraFieldMapper extends JdbcRowMapper<FaultMetaExtraField> {
        @Override
        public FaultMetaExtraField createRow(ResultSet resultSet) throws SQLException {
            FaultMetaExtraField fm = new FaultMetaExtraField();
            
            fm.setFaultMetatId(resultSet.getInt("FaultMetaId"));
            fm.setField(resultSet.getString("Field"));
            fm.setValue(resultSet.getString("Value"));
            
            return fm;
        }
    }
    
    
    /**
     * Insert a value for the extra field passed in parameter.
     * @param faultMetaId the fault meta Id
     * @param field the field name
     * @param value the field value
     */
    public void create(Integer faultMetaId, String field, String value) {
        String query = appConf.getQuery(Spectrum.FAULT_META_EXTRA_FIELD_INSERT_QUERY);
        executeUpdateByQuery(query, faultMetaId, field, value);
        
    }
    
    /**
     * Update a value for the extra field passed in parameter.
     * @param faultMetaId the fault meta id
     * @param field the field name
     * @param value the value
     */
    public void update(Integer faultMetaId, String field, String value) {
        String query = appConf.getQuery(Spectrum.FAULT_META_EXTRA_FIELD_UPDATE_QUERY);
        executeUpdateByQuery(query, value, faultMetaId, field);
    }
    
    
    
    /**
     * Return the extra fields for the fault meta passed in parameter.
     * @param faultMetaId the fault meta id
     * @return the list of extra fields for this fault meta
     */
    public List<FaultMetaExtraField> get(Integer faultMetaId) {
        String query = appConf.getQuery(Spectrum.FAULT_META_EXTRA_FIELD_GET_QUERY);
        return findByQuery(query, mapper, faultMetaId);
    }
    
    /**
     * Delete the value passed in parameter
     * @param faultMetaId the fault meta id
     * @param field the field to delete
     */
    public void delete(Integer faultMetaId, String field) {
        String query = appConf.getQuery(Spectrum.FAULT_META_EXTRA_FIELD_DELETE_QUERY);
        executeUpdateByQuery(query, faultMetaId, field);
        
    }
    
    public List<FaultMetaExtraField> findPrimaryExtraFields() throws DataAccessException {
    	return findPrimaryExtraFields("");
    }
    
    public List<FaultMetaExtraField> findPrimaryExtraFields(String fleetCode) throws DataAccessException {
        String query;
        List<FaultMetaExtraField> result ;
        
        if (fleetCode != null &&
            !fleetCode.isEmpty()) {
        	query = appConf.getQuery(Spectrum.FAULT_META_EXTRA_FIELD_PARENT_QUERY_WITH_FLEET_CODE);
        	result = findByQuery(query, mapper, fleetCode);
        } else {
        	query = appConf.getQuery(Spectrum.FAULT_META_EXTRA_FIELD_PARENT_QUERY);
        	result = findByQuery(query, mapper);
        }
        return result;
    }
    
    public List<FaultMetaExtraField> findOverrideExtraFields() throws DataAccessException {
    	return findOverrideExtraFields("");
    }
    
    public List<FaultMetaExtraField> findOverrideExtraFields(String fleetCode) throws DataAccessException {
        String query;
        List<FaultMetaExtraField> result;
        
        if (fleetCode != null &&
        	!fleetCode.isEmpty()) {
        	query = appConf.getQuery(Spectrum.FAULT_META_EXTRA_FIELD_CHILD_QUERY_WITH_FLEET_CODE);
        	result = findByQuery(query, mapper, fleetCode);
        } else {
        	query = appConf.getQuery(Spectrum.FAULT_META_EXTRA_FIELD_CHILD_QUERY);
        	result = findByQuery(query, mapper);
        }
        
        return result;
    }
    
    @Override
    public RowMapper<FaultMetaExtraField> getRowMapper() {
        return mapper;
    }
    
    @Override
    public String getTableName() {
        return "FaultMetaExtraField";
    }

}
