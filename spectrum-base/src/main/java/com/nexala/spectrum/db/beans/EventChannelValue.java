package com.nexala.spectrum.db.beans;

import com.nexala.spectrum.channel.ChannelCollection;
import com.nexala.spectrum.rest.data.beans.DataSet;

public class EventChannelValue implements DataSet {

    private Long id;
    
    private Long timestamp;
    
    private Integer sourceId;
    
    private Integer channelId;
    
    private Boolean value;

    public EventChannelValue(Long id, Long timestamp, Integer sourceId, Integer channelId, Boolean value) {
        super();
        this.id = id;
        this.timestamp = timestamp;
        this.sourceId = sourceId;
        this.channelId = channelId;
        this.value = value;
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public Integer getSourceId() {
        return sourceId;
    }

    public void setSourceId(Integer sourceId) {
        this.sourceId = sourceId;
    }

    public Integer getChannelId() {
        return channelId;
    }

    public void setChannelId(Integer channelId) {
        this.channelId = channelId;
    }

    public Boolean getValue() {
        return value;
    }

    public void setValue(Boolean value) {
        this.value = value;
    }
    
    
    @Override
    public ChannelCollection getChannels() {
        throw new UnsupportedOperationException();
    }
    
    @Override
    public void setChannels(ChannelCollection cc) {
        throw new UnsupportedOperationException();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((channelId == null) ? 0 : channelId.hashCode());
        result = prime * result + ((sourceId == null) ? 0 : sourceId.hashCode());
        result = prime * result + ((timestamp == null) ? 0 : timestamp.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        EventChannelValue other = (EventChannelValue) obj;
        if (channelId == null) {
            if (other.channelId != null)
                return false;
        } else if (!channelId.equals(other.channelId))
            return false;
        if (sourceId == null) {
            if (other.sourceId != null)
                return false;
        } else if (!sourceId.equals(other.sourceId))
            return false;
        if (timestamp == null) {
            if (other.timestamp != null)
                return false;
        } else if (!timestamp.equals(other.timestamp))
            return false;
        return true;
    }
    
    
}
