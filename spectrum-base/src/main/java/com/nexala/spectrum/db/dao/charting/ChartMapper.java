package com.nexala.spectrum.db.dao.charting;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.nexala.spectrum.db.JdbcRowMapper;

public class ChartMapper extends JdbcRowMapper<ChartConfiguration> {
    @Override
    public final ChartConfiguration createRow(ResultSet resultSet) throws SQLException {
        ChartConfiguration chart = new ChartConfiguration();
        chart.setId(resultSet.getLong("ID"));
        chart.setName(resultSet.getString("Name"));
        chart.setType(ChannelType.fromString(resultSet.getString("Type")));
        chart.setSequence(resultSet.getInt("Sequence"));
        return chart;
    }
}