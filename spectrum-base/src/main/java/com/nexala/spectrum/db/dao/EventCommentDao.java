/*
 * Copyright (c) Nexala Technologies 2013, All rights reserved.
 */
package com.nexala.spectrum.db.dao;

import java.sql.Date;
import java.util.List;

import com.google.inject.Inject;
import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.db.QueryManager;
import com.nexala.spectrum.db.beans.EventComment;
import com.nexala.spectrum.db.mappers.EventCommentMapper;

public class EventCommentDao extends GenericDao<EventComment, Integer> {
    @Inject
    private EventCommentMapper rowMapper;

    public List<EventComment> getComments(Integer eventId) {
        String query = new QueryManager().getQuery(Spectrum.EVENT_COMMENTS);
        return findByQuery(query, rowMapper, eventId);
    }
    
    public List<EventComment> saveComment(Integer eventId, Long timestamp, String userName, String comment, String action) {
    	String query = new QueryManager().getQuery(Spectrum.EVENT_SAVE_COMMENT);
    	return findByQuery(query, rowMapper, eventId, new Date(timestamp), userName, comment, action, eventId);
    }
}
