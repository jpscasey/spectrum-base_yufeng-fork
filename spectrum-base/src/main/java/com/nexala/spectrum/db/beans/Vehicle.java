package com.nexala.spectrum.db.beans;

import com.nexala.spectrum.db.dao.Record;

/* FIXME - this class should have a generic name, i.e. StockExtended, as Stock already exists.
    unitId should be parentId, vehicleNumber -> description, vehicleType -> type */
public class Vehicle implements Record {
    private int id;
    
    /** XXX change to Integer */
    private int unitId = -1;
    private String vehicleNumber;
    private String vehicleType;
    private Integer vehicleTypeId;
    private int position;

    public Vehicle(int id, String vehicleNumber) {
        this.id = id;
        this.vehicleNumber = vehicleNumber;
    }

    public Vehicle(int id, String vehicleNumber, int position) {
        this.id = id;
        this.vehicleNumber = vehicleNumber;
        this.position = position;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public int getParentId() {
        return unitId;
    }
    
    @Deprecated 
    public int getUnitId() {
        return unitId;
    }

    public void setParentId(int id) {
        this.unitId = id;
    }
    
    @Deprecated
    public void setUnitId(int id) {
        this.unitId = id;
    }

    public void setVehicleNumber(String unitNumber) {
        this.vehicleNumber = unitNumber;
    }

    public String getVehicleNumber() {
        return vehicleNumber;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public Integer getVehicleTypeId() {
        return vehicleTypeId;
    }

    public void setVehicleTypeId(Integer vehicleTypeId) {
        this.vehicleTypeId = vehicleTypeId;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }
}
