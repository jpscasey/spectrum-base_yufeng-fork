package com.nexala.spectrum.db;

import java.io.InputStream;

import com.nexala.spectrum.Spectrum;

public class QueryManager extends BaseQueryManager {
    @Override
    public String getKey() {
        return "SPECTRUM";
    }
    
    @Override
    public InputStream getSqlResource() {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        return classLoader.getResourceAsStream(Spectrum.SPECTRUM_BASE_SQL_XML);
    }
}
