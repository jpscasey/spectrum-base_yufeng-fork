package com.nexala.spectrum.db.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import com.google.inject.Inject;
import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.db.JdbcRowMapper;
import com.nexala.spectrum.db.QueryManager;
import com.nexala.spectrum.db.beans.Unit;

public class UnitDao extends GenericDao<Unit, Long> {
	
    @Inject
    private VehicleDao vehicleDao;

	private static final JdbcRowMapper<Unit> shallowUnitMapper = new ShallowUnitMapper();
	
    /**
     * A mapper that retrieves and maps the standard fields of a unit.
     */
	private static class ShallowUnitMapper extends JdbcRowMapper<Unit> {
        public Unit createRow(ResultSet rs) throws SQLException {
            Unit unit = new Unit();
            unit.setId(rs.getInt("ID"));
            unit.setUnitNumber(rs.getString("UnitNumber"));
            unit.setUnitType(rs.getString("UnitType"));

            try {
                unit.setUnitPosition(rs.getInt("UnitPosition"));
                unit.setUnitOrientation(rs.getInt("UnitOrientation"));
            } catch (Exception ex) {
            }

            return unit;
        }
    };
    
    /**
     * A mapper that retrieves vehicles that belong to the unit being mapped as
     * well as the standard fields.
     */
    private class DeepUnitMapper extends ShallowUnitMapper {
        @Override
        public Unit createRow(ResultSet rs) throws SQLException {
            Unit unit = super.createRow(rs);
            unit.setVehicles(vehicleDao.getVehicles(unit.getId()));
            return unit;
        }
    };
    
    public List<Unit> findUnitsByUnitIds(String unitIds) {
        if (unitIds == null) {
            throw new IllegalArgumentException("At least one unit id "
                    + "needs to be specified");
        }

        String query = new QueryManager().getQuery(Spectrum.UNITS_BY_UNIT_IDS)
                .replace("#UNIT_IDS#", unitIds);

        return findByQuery(query, new DeepUnitMapper());
    }
    
    public List<Unit> findUnitFormationByUnitId(long unitId) {
        String query = new QueryManager()
                .getQuery(Spectrum.UNIT_FORMATION_BY_UNIT_ID);
        return findByQuery(query, shallowUnitMapper, unitId, unitId);
    }
    
    public Unit findByUnitNumber(String unitNumber) throws DataAccessException {
        String query = new QueryManager().getQuery(Spectrum.UNIT_BY_UNIT_NUMBER);
        List<Unit> units = findByQuery(query, shallowUnitMapper, unitNumber);

        if (units.size() > 0) {
            return units.get(0);
        }
        else {
            return null;
        }
    }
    
    public List<Unit> getAllUnits(String fleetCode) throws DataAccessException {
        String query = new QueryManager().getQuery(Spectrum.ALL_UNITS_QUERY);
        return findByQuery(query, new DeepUnitMapper(), fleetCode);
    }
    
	public List<String> getUnitTypes() {
		String query = new QueryManager().getQuery(Spectrum.UNIT_TYPES);
  		return getStringListByQuery(query);
	}
	
    public Unit findById(long unitId) throws DataAccessException {
        return findById(unitId, new DeepUnitMapper());
    }
    
	public Date getLastMaintTimestamp(long unitId) {
        String query = new QueryManager()
                .getQuery(Spectrum.UNIT_LAST_MAINTENANCE_TIMESTAMP);
        return getDateByQuery(query, unitId);
    }
	
	public List<String> getUnitStatus(long unitId) {
        String query = new QueryManager()
                .getQuery(Spectrum.UNIT_STATUS);
        return getStringListByQuery(query, unitId);
    }
    
    public String getTableName() {
        return "Unit";
    }

    public String getIdColumnName() {
        return "ID";
    }
    
    public JdbcRowMapper<Unit> getRowMapper() {
        return shallowUnitMapper;
    }

}
