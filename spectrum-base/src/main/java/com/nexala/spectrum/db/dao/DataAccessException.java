/*
 * Copyright (c) Nexala Technologies 2010, All rights reserved.
 */
package com.nexala.spectrum.db.dao;

public class DataAccessException extends RuntimeException {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 3866249459167025556L;
    
    public DataAccessException(String s) {
        super(s);
    }
    
    public DataAccessException(Throwable t) {
        super(t);
    }
    
    public DataAccessException(String msg, Throwable t) {
        super(msg, t);
    }
}