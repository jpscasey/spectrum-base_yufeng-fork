/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.db.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.google.inject.Singleton;
import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.db.JdbcRowMapper;
import com.nexala.spectrum.db.QueryManager;
import com.nexala.spectrum.rest.data.beans.EventFormation;

@Singleton
public class EventFormationDao extends GenericDao<EventFormation, Long> {
    
    private class EventMapper extends JdbcRowMapper<EventFormation> {
        @Override
        public EventFormation createRow(ResultSet rs) throws SQLException {
            EventFormation record = new EventFormation();
            record.setVehicleId(rs.getLong("VehicleID"));
            record.setVehicleNumber(rs.getString("VehicleNumber"));
            record.setVehicleOrder(rs.getInt("VehicleOrder"));
            record.setIsFaultVehicle(rs.getBoolean("IsFaultVehicle"));

            return record;
        }
    }
    /**
     * Returns an event from an event Id. An event can be a fault, warning or
     * event.
     * 
     * @param Long eventId
     * @return EventFormation
     */
    public List<EventFormation> getEventFormationByEventId(Long eventId) {
        String query = new QueryManager()
                .getQuery(Spectrum.EVENT_FORMATION);
        List<EventFormation> events = findByStoredProcedure(query, new EventMapper(),
                eventId);
        return events;

    }
}