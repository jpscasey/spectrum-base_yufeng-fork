package com.nexala.spectrum.db.dao;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;

import com.nexala.spectrum.db.JdbcRowMapper;
import com.nexala.spectrum.db.beans.RecoveryAuditItemType;
import com.nexala.spectrum.db.mappers.RecoveryAuditItemTypeMapper;

public class RecoveryAuditItemTypeDao extends GenericDao<RecoveryAuditItemType, Long> {
    private final JdbcRowMapper<RecoveryAuditItemType> rowMapper = new RecoveryAuditItemTypeMapper();
    
    /**
     * Find an item type by its signature (question flag, text, response, user
     * action flag).
     * 
     * @param isQuestion        Question flag
     * @param itemText          Item text contents
     * @param itemResponse      Response to item by user/system.
     * @param isUserAction      User action flag.
     * @param isReversion       Reversion flag
     * @param isExecution       Execution flag
     * @param isOutcome         Outcome flag
     * @param isNextStep        Next step flag
     * @return Null if none found, otherwise return resulting object.
     */
    public RecoveryAuditItemType findBySignature(boolean isQuestion, 
            String itemText, String itemResponse, boolean isUserAction,
            boolean isReversion, boolean isExecution, boolean isOutcome,
            boolean isNextStep) throws DataAccessException {
        
        String itemTextCond;
        String itemResponseCond;
        
        if(itemText == null) {
            itemTextCond = "OR [ItemText] IS NULL";
        }
        else {
            itemTextCond = "";
        }
        
        if(itemResponse == null) {
            itemResponseCond = "OR [Answer] IS NULL";
        }
        else {
            itemResponseCond = "";
        }
        
        String sql = "SELECT * FROM [" + this.getTableName() + "] WHERE "
            + " [Question] = ?"
            + " AND ([ItemText] = ? " + itemTextCond + ")"
            + " AND ([Answer] = ? " + itemResponseCond + ")"
            + " AND [UserAction] = ?"
            + " AND [Reversion] = ?"
            + " AND [Execution] = ?"
            + " AND [Outcome] = ?"
            + " AND [NextStep] = ?";
        
        Long isQuestionParam = isQuestion ? new Long(1L) : new Long(0L);
        Long isUserActionParam = isUserAction ? new Long(1L) : new Long(0L);
        
        List<RecoveryAuditItemType> results = findByQuery(sql,
                isQuestionParam, itemText, itemResponse, isUserActionParam,
                isReversion, isExecution, isOutcome, isNextStep);
        
        if(results.size() > 0) {
            return results.get(0);
        }
        else {
            return null;
        }
    }
    
    public Long create(RecoveryAuditItemType object) throws DataAccessException {
        HashMap<String, Object> columnValues = new HashMap<String, Object>();
        
        columnValues.put("Question", object.isQuestion() ? new Long(1) : new Long(0));
        columnValues.put("ItemText", object.getItemText());
        columnValues.put("Answer", object.getAnswer());
        columnValues.put("UserAction", object.isUserAction() ? new Long(1) : new Long(0));
        columnValues.put("Reversion", object.isReversion());
        columnValues.put("Execution", object.isExecution());
        columnValues.put("Outcome", object.isOutcome());
        columnValues.put("NextStep", object.isNextStep());
        
        Object result = create(columnValues);
        return new Long(((BigDecimal)result).longValue());
    }
    
    public JdbcRowMapper<RecoveryAuditItemType> getRowMapper() {
        return this.rowMapper;
    }
    
    public String getTableName() {
        return "RecoveryAuditItemType";
    }
    
    public String getIdColumnName() {
        return "id";
    }
}
