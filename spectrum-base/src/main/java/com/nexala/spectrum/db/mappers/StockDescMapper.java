/*
 * Copyright (c) Nexala Technologies 2013, All rights reserved.
 */
package com.nexala.spectrum.db.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.google.inject.Inject;
import com.nexala.spectrum.db.JdbcRowMapper;
import com.nexala.spectrum.db.dao.BoxedResultSet;
import com.nexala.spectrum.db.dao.BoxedResultSetFactory;
import com.nexala.spectrum.rest.data.beans.StockDesc;

public class StockDescMapper extends JdbcRowMapper<StockDesc> {
    
    @Inject
    private BoxedResultSetFactory factory;
    
    @Override
    public StockDesc createRow(ResultSet resultSet) throws SQLException {
        BoxedResultSet brs = factory.create(resultSet);
        
        return new StockDesc(
            brs.getInt("ID"),
            brs.getString("StockNumber"),
            brs.getInt("StockOrder"),
            null,
            true,
            "Unit"
        );	
    }
}
