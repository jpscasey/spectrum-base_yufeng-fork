package com.nexala.spectrum.db.dao;

import java.util.List;

import com.nexala.spectrum.db.JdbcRowMapper;
import com.nexala.spectrum.db.beans.FaultType;
import com.nexala.spectrum.db.mappers.FaultTypeMapper;

public class FaultTypeDao extends GenericDao<FaultType, String> {
    private FaultTypeMapper mapper = new FaultTypeMapper();
    
    public List<FaultType> getFaultTypeList() {
        String query = "SELECT ID, Name, DisplayColor, Priority FROM FaultType";
        List<FaultType> faultTypeList = findByQuery(query, mapper);
        return faultTypeList;
    }

    @Override
    public JdbcRowMapper<FaultType> getRowMapper() {
        return mapper;
    }
    
    public String getTableName() {
        return "FaultType";
    }

    public String getIdColumnName() {
        return "ID";
    }
}
