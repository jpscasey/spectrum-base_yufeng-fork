package com.nexala.spectrum.db.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.google.inject.Inject;
import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.configuration.ConfigurationManager;
import com.nexala.spectrum.db.JdbcRowMapper;
import com.nexala.spectrum.db.beans.E2mFaultCode;

public class E2mFaultCodeDao extends GenericDao<E2mFaultCode, Integer> {
    private final boolean optionalE2mFaultCodesEnabled;

    @Inject
    public E2mFaultCodeDao(ConfigurationManager confManager) {
        Boolean enabled = confManager
                .getConfAsBoolean(Spectrum.SPECTRUM_FAULT_SENDER_ENABLE);
        this.optionalE2mFaultCodesEnabled = enabled != null ? enabled : false;
    }

    private class E2mFaultCodeMapper extends JdbcRowMapper<E2mFaultCode> {
        @Override
        public E2mFaultCode createRow(ResultSet resultSet) throws SQLException {

            E2mFaultCode result = new E2mFaultCode();

            result.setCode(resultSet.getString("E2MFaultCode"));
            result.setDescription(resultSet.getString("E2MDescription"));
            result.setPriorityCode(resultSet.getString("E2MPriorityCode"));
            result.setPriority(resultSet.getString("E2MPriority"));

            return result;
        }
    }

    public E2mFaultCode getE2mFaultCode(Integer faultMetaId)
            throws DataAccessException {
        if (optionalE2mFaultCodesEnabled) {
            List<E2mFaultCode> e2mFaultCode = findByQuery(
                    "SELECT * FROM FaultMetaE2M WHERE FaultMetaID = ?",
                    new E2mFaultCodeMapper(), faultMetaId);
            return e2mFaultCode.size() > 0 ? e2mFaultCode.get(0) : new E2mFaultCode();
        } else {
            return new E2mFaultCode();
        }

    }

}
