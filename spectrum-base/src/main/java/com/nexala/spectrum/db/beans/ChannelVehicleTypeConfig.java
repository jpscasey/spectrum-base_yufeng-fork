package com.nexala.spectrum.db.beans;

public class ChannelVehicleTypeConfig {

    private Integer channelId;
    
    private Integer vehicleTypeId;

    public Integer getChannelId() {
        return channelId;
    }

    public void setChannelId(Integer channelId) {
        this.channelId = channelId;
    }

    public Integer getVehicleTypeId() {
        return vehicleTypeId;
    }

    public void setVehicleTypeId(Integer vehicleTypeId) {
        this.vehicleTypeId = vehicleTypeId;
    }
    
}
