/*
 * Copyright (c) Nexala Technologies 2010, All rights reserved.
 */
package com.nexala.spectrum.db.beans;

import com.nexala.spectrum.db.dao.Record;

public class FaultCategory implements Record, Comparable<FaultCategory> {
	private String category;
	private Integer id;
	private Boolean reportingOnly;
	
	public FaultCategory() {}
	
	public FaultCategory(Integer id, String category) {
		this.id = id;
		this.category = category;
	}
	
	public FaultCategory(String category) {
		this.category = category;
	}
	
	public String getCategory() {
		return category;
	}
	
	public Integer getId() {
		return id;
	}
	
	public Boolean getReportingOnly() {
        return reportingOnly;
    }
	
	public void setCategory(String category) {
		this.category = category;
	}

    public void setId(Integer id) {
		this.id = id;
	}

    public void setReportingOnly(Boolean reportingOnly) {
        this.reportingOnly = reportingOnly;
    }

    @Override
    public int compareTo(FaultCategory o) {
        try {
            Integer myCategoryInt = Integer.parseInt(this.category);
            Integer oCategoryInt = Integer.parseInt(o.getCategory());

            return myCategoryInt.compareTo(oCategoryInt);
        } catch (NumberFormatException e) {
            return this.category.compareTo(o.getCategory());
        }
    }
}
