/*
 * Copyright (c) Nexala Technologies 2013, All rights reserved.
 *
 * $Author$
 * $Date$
 * $Revision$
 * $Source$
 */
package com.nexala.spectrum.db.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.google.inject.Inject;
import com.nexala.spectrum.db.JdbcRowMapper;
import com.nexala.spectrum.db.dao.BoxedResultSet;
import com.nexala.spectrum.db.dao.BoxedResultSetFactory;
import com.nexala.spectrum.rest.data.beans.BeanField;
import com.nexala.spectrum.rest.data.beans.DownloadInfo;
import com.nexala.spectrum.view.conf.DownloadConfiguration;

public class DownloadMapper extends JdbcRowMapper<DownloadInfo>{
    
	@Inject
    private BoxedResultSetFactory factory;
	
    @Inject
    DownloadConfiguration config;
    
    @Override
    public DownloadInfo createRow(ResultSet resultSet) throws SQLException {
        
    	BoxedResultSet rs = factory.create(resultSet);
        DownloadInfo d = new DownloadInfo();

        List<BeanField> downloadFields = config.getFields();
        
        for (BeanField downloadField : downloadFields) {
            String fieldId = downloadField.getId();
            String dbName = downloadField.getDbName();
            String type = downloadField.getType();
            d.setField(fieldId, extractField(rs, dbName, type));
        }
		return d;
    }
}
