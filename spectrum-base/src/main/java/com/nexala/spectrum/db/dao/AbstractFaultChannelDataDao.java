package com.nexala.spectrum.db.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.nexala.spectrum.analysis.AnalysisData;
import com.nexala.spectrum.rest.data.beans.DataSet;

public abstract class AbstractFaultChannelDataDao<T, ID extends Serializable, V extends AnalysisData> extends GenericDao<T, ID> {

    protected abstract Map<Integer, List<V>> getVehicleMap(List<? extends DataSet> dataSetList);

    protected abstract List<V> getAsVehicle(List<? extends DataSet> dataSetList);

    protected final Map<Integer, List<DataSet>> getDataSetMap(List<? extends DataSet> dataSetList) {

        Map<Integer, List<DataSet>> dataSetMap = new HashMap<Integer, List<DataSet>>();

        if (dataSetList != null) {
            for (DataSet ds : dataSetList) {
                Integer id = ds.getSourceId();

                if (dataSetMap.get(id) == null) {
                    dataSetMap.put(id, new ArrayList<DataSet>());
                }

                dataSetMap.get(id).add(ds);
            }
        }

        return dataSetMap;
    }

}
