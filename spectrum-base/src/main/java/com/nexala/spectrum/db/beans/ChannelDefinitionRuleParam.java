package com.nexala.spectrum.db.beans;

import java.util.List;

import org.apache.commons.lang.StringEscapeUtils;

public class ChannelDefinitionRuleParam {
    private String name;
    private String statusId;
    private String active;
    private List<ChannelDefinitionRuleValidationParam> validations;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatusId() {
        return statusId;
    }

    public void setStatusId(String statusId) {
        this.statusId = statusId;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public List<ChannelDefinitionRuleValidationParam> getValidations() {
        return validations;
    }

    public void setValidations(List<ChannelDefinitionRuleValidationParam> validations) {
        this.validations = validations;
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("<rule ");
        sb.append("name=\"" + StringEscapeUtils.escapeXml(name) + "\" ");
        sb.append("statusid=\"" + StringEscapeUtils.escapeXml(statusId) + "\" ");
        sb.append("active=\"" + StringEscapeUtils.escapeXml(active) + "\" ");
        sb.append(">");

        for (ChannelDefinitionRuleValidationParam validation : validations) {
            sb.append(validation.toString());
        }

        sb.append("</rule>");

        return sb.toString();
    }
}
