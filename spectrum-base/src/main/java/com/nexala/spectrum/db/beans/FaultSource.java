package com.nexala.spectrum.db.beans;

import java.io.Serializable;

public final class FaultSource implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -1668663991299971366L;
    private Integer id;
    private String description;

    /**
     * Non-argument default constructor required for GWT RPC serialization. <em>Do not use.</em>
     */
    public FaultSource() {}

    public FaultSource(Integer id, String description) {
        this.id = id;
        this.description = description;
    }

    public Integer getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

}
