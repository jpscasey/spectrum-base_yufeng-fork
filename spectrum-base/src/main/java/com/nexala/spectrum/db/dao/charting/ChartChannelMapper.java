package com.nexala.spectrum.db.dao.charting;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.google.inject.Inject;
import com.nexala.spectrum.configuration.ChannelConfiguration;
import com.nexala.spectrum.db.JdbcRowMapper;
import com.nexala.spectrum.db.beans.ChannelConfig;

public class ChartChannelMapper extends JdbcRowMapper<ChartChannel> {

    @Inject
    ChannelConfiguration channelConfig;

    
    @Inject
    public ChartChannelMapper() {
    }

    @Override
    public final ChartChannel createRow(ResultSet resultSet) throws SQLException {
        // Get details about the channel.
        ChannelConfig config = channelConfig.getConfig(resultSet
                .getInt("ChannelID"));
        
        String scale = resultSet.getString("Scale");

        ChartChannel channel = new ChartChannel();
        channel.setChartId(resultSet.getLong("ChartID"));
        channel.setChannel(config);
        channel.setScale(scale != null ? new BigDecimal(scale) : null);
        channel.setSequence(resultSet.getInt("Sequence"));
        channel.setMinValue(config.getMinValue() != null ? 
        		new BigDecimal(config.getMinValue()) : null);
        channel.setMaxValue(config.getMaxValue() != null ? 
        		new BigDecimal(config.getMaxValue()) : null);
        channel.setDisplayedAsDifference(Boolean.valueOf(config.isDisplayedAsDifference()));

        return channel;
    }
}
