/*
 * Copyright (c) Nexala Technologies 2010, All rights reserved.
 */
package com.nexala.spectrum.db;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

import com.nexala.spectrum.db.dao.DataAccessException;

/**
 * A set of static helper methods for closing database connections, etc. This
 * makes helps to keep the DAO layer very clean.
 * 
 * <pre>
 * <code>
 * try {
 *     rs = stmt.executeQuery();
 * } catch (SQLException e) {
 *     throw new RuntimeException(e);
 * } finally {
 *     DbUtil.closeResultSet(rs);
 *     DbUtil.closeStatement(stmt);
 *     DbUtil.closeConnection(connection);
 * }
 * </code>
 * </pre>
 * 
 * @author Paul O'Riordan
 * @created 28 Oct 2010
 */
public final class DbUtil {
    private DbUtil() {}

    public static void closeConnection(Connection c) throws DataAccessException {
        try {
            if (c != null) {
                c.close();
            }
        } catch (SQLException e) {
            throw new DataAccessException(e);
        }
    }

    public static void closeStatement(Statement s) throws DataAccessException {
        try {
            if (s != null) {
                s.close();
            }
        } catch (SQLException e) {
            throw new DataAccessException(e);
        }
    }

    public static void closeResultSet(ResultSet rs) throws DataAccessException {
        try {
            if (rs != null) {
                rs.close();
            }
        } catch (SQLException e) {
            throw new DataAccessException(e);
        }
    }

    public static boolean hasColumn(ResultSet rs, String columnName)
            throws SQLException {
        ResultSetMetaData meta = rs.getMetaData();
        
        for (int i = 1; i <= meta.getColumnCount(); i++) {
            if (meta.getColumnName(i).equalsIgnoreCase(columnName)) {
                return true;
            }
        }
        
        return false;
    }
}
