/*
 * Copyright (c) Nexala Technologies 2013, All rights reserved.
 *
 * @author dclerkin
 * @date 15/11/2013
 */
package com.nexala.spectrum.db.dao;

import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import com.google.inject.Inject;
import com.google.inject.name.Named;


public class JndiConnectionProvider implements ConnectionProvider {

    private String dataSourceName = null;

    private DataSource dataSource = null;
    
    @Inject
    public JndiConnectionProvider(@Named("dataSource") String datasourceName) {
    	this.dataSourceName = datasourceName;
	}

    @Override
    public Connection getConnection() throws DataAccessException {
        Connection connection = null;

        if (dataSource == null) {
            try {
                Context ctx = new InitialContext();
                dataSource = (DataSource) ctx.lookup(getJndiDataSourceName());
                if (dataSource != null) {
                    connection = dataSource.getConnection();
                }
            } catch (NamingException e) {
                throw new RuntimeException(e);
            } catch (SQLException e) {
                throw new DataAccessException(e);
            }
        } else {
            try {
                connection = dataSource.getConnection();
            } catch (SQLException e) {
                throw new DataAccessException(e);
            }
        }

        if (connection == null) {
            throw new RuntimeException("Unable to create Initial Context");
        }

        return connection;
    }
    


    public String getJndiDataSourceName() {
        return dataSourceName;
    }

}
