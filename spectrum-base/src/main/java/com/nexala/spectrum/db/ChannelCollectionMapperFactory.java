package com.nexala.spectrum.db;

import com.google.inject.assistedinject.Assisted;
import com.nexala.spectrum.configuration.ChannelConfiguration;

/**
 * Factory to instanciate a {@link ChannelCollectionMapper}
 * @author brice
 *
 */
public interface ChannelCollectionMapperFactory {
    ChannelCollectionMapper create(@Assisted ChannelConfiguration channelConf);
}
