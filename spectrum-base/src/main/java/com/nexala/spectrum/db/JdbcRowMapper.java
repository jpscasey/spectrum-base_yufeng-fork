/*
 * Copyright (c) Nexala Technologies 2010, All rights reserved.
 */
package com.nexala.spectrum.db;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.nexala.spectrum.db.dao.BoxedResultSet;
import com.nexala.spectrum.db.dao.DataAccessException;

/**
 * This is an abstract class which provides an interface for converting result
 * sets into bean arrays. Sample implementation of createRow(ResultSet
 * resultSet):
 * 
 * <pre>
 * <code>
 * public MyBean createRow(ResultSet resultSet) throws SQLException {
 * 	return new MyBean(
 * 		resultSet.getLong(1),
 * 		resultSet.getString(2),
 * 		resultSet.getString(3)
 * );
 * }
 * </pre>
 * 
 * </code>
 * 
 * @author Paul O'Riordan
 * @created 9 Nov 2010
 * 
 * @param <T>
 */
public abstract class JdbcRowMapper<T> implements RowMapper<T> {

    /**
     * {@inheritDoc}
     */
    public abstract T createRow(ResultSet resultSet) throws SQLException;

    /**
     * {@inheritDoc}
     */
    public List<T> createRows(ResultSet resultSet) throws SQLException {

        List<T> beans = newList();

        try {
            while (resultSet.next()) {
                beans.add(createRow(resultSet));
            }
        } catch (SQLException e) {
            throw new DataAccessException(e);
        }

        return beans;
    }
    
    /**
     * {@inheritDoc}
     */
    public List<T> newList() {
        return new ArrayList<T>();
    }
    
    public Object extractField(BoxedResultSet rs, String dbName, String type) throws SQLException {
        switch (type.toUpperCase()) {
        case "STRING":
            return rs.getString(dbName);
        case "INT":
            return rs.getInt(dbName);
        case "DOUBLE":
            return rs.getDouble(dbName);
        case "BOOLEAN":
            return rs.getBoolean(dbName);
        case "TIMESTAMP":
            return rs.getTimestamp(dbName);
        case "VERSION":
            return rs.getVersion(dbName);
        }

        return null;
    }
}
