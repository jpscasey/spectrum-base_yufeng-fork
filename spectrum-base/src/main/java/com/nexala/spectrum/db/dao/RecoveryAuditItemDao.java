package com.nexala.spectrum.db.dao;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;

import com.nexala.spectrum.db.JdbcRowMapper;
import com.nexala.spectrum.db.beans.RecoveryAuditItem;
import com.nexala.spectrum.db.mappers.RecoveryAuditItemMapper;

/**
 * DAO for recovery audit log entries.
 * 
 * @author amartin
 *
 */
public class RecoveryAuditItemDao extends GenericDao<RecoveryAuditItem, Long> {
    private final JdbcRowMapper<RecoveryAuditItem> rowMapper = new RecoveryAuditItemMapper();
    
    public Long create(RecoveryAuditItem item) throws DataAccessException {
        HashMap<String, Object> columns = new HashMap<String, Object>();
        
        columns.put("Timestamp", item.getTimestamp());
        columns.put("UserName", item.getUserName());
        columns.put("Fault_id", item.getFaultInstanceId());
        columns.put("Context_id", item.getContextId());
        columns.put("Fault_id", item.getFaultInstanceId());
        columns.put("Preceding_id", item.getPrecedingId());
        columns.put("AuditItemType_id", item.getAuditItemTypeId());
        columns.put("Notes", item.getNotes());
        
        Object result = this.create(columns);
        return new Long(((BigDecimal)result).longValue());
    }
    
    public List<RecoveryAuditItem> findByFaultId(Long faultId, boolean orderByTimestamp) throws DataAccessException {
        String sql = "SELECT * FROM [" + this.getTableName() + "] WHERE "
            + " [Fault_id] = ?";
        
        if(orderByTimestamp) {
            sql = sql + " ORDER BY [Timestamp]";
        }
        
        return this.findByQuery(sql, faultId);
    }
    
    public JdbcRowMapper<RecoveryAuditItem> getRowMapper() {
        return this.rowMapper;
    }
    
    public String getTableName() {
        return "RecoveryAuditItem";
    }
}
