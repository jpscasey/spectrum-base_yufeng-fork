package com.nexala.spectrum.db.beans;

import java.util.List;

import com.nexala.spectrum.validation.ChannelRule;

public final class ChannelDefinition {
    private final ChannelConfig channelConfig;
    private final ChannelGroupConfig channelGroupConfig;
    private final List<ChannelRule> channelRuleList;

    public ChannelDefinition(ChannelConfig channelConfig, ChannelGroupConfig channelGroupConfig,
            List<ChannelRule> channelRuleList) {

        this.channelConfig = channelConfig;
        this.channelGroupConfig = channelGroupConfig;
        this.channelRuleList = channelRuleList;
    }

    public ChannelConfig getChannelConfig() {
        return channelConfig;
    }

    public ChannelGroupConfig getChannelGroupConfig() {
        return channelGroupConfig;
    }

    public List<ChannelRule> getChannelRuleList() {
        return channelRuleList;
    }

    public String getRulesStatus() {
        StringBuffer sb = new StringBuffer();

        if (channelRuleList != null) {
            String sep = "";
            for (ChannelRule rule : channelRuleList) {
                if (sb.indexOf(rule.getStatus().getName()) < 0) {
                    sb.append(sep + rule.getStatus().getName());
                    sep = ", ";
                }
            }
        }

        return sb.toString();
    }
}
