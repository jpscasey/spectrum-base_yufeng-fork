/*
 * Copyright (c) Nexala Technologies 2013, All rights reserved.
 */
package com.nexala.spectrum.db.beans;

public final class EventComment {
    private final int eventId;
    private final String text;
    private final long timestamp;
    private final String userName;
    private final String action;

    public EventComment(int faultId, long timestamp, String userName, String text, String action) {
        this.eventId = faultId;
        this.timestamp = timestamp;
        this.userName = userName;
        this.text = text;
        this.action = action;
    }

    public int getFaultId() {
        return eventId;
    }

    public String getText() {
        return text;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public String getUserName() {
        return userName;
    }

    public String getAction() {
        return action;
    }
}
