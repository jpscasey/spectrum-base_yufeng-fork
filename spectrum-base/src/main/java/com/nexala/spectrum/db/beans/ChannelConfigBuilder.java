/*
 * Copyright (c) Nexala Technologies 2013, All rights reserved.
 */
package com.nexala.spectrum.db.beans;

import java.util.List;

import com.nexala.spectrum.db.dao.charting.ChannelType;

public class ChannelConfigBuilder {
    private boolean alwaysDisplayed;
    private Integer channelGroupId;
    private Integer channelGroupOrder;
    private Integer columnId;
    private String comment;
    private Boolean defaultValue;
    private String description;
    private String formatPattern;
    private String hardwareType;
    private Integer id;
    private boolean lookup;
    private Double maxValue;
    private Double minValue;
    private String name;
    private String ref;
    private String storageMethod;
    private ChannelType type;
    private String dataType;
    private String unitOfMeasure;
    private Integer vehicleId;
    private boolean visibleOnFaultOnly;
    private boolean displayedAsDifference;
    private RelatedChannelGroup relatedChannelGroup;
    private String vehicleType;
    private List<String> linkedChannels;

    public Integer getChannelGroupId() {
        return channelGroupId;
    }

    public Integer getChannelGroupOrder() {
        return channelGroupOrder;
    }

    public Integer getColumnId() {
        return columnId;
    }

    public String getComment() {
        return comment;
    }

    public Boolean getDefaultValue() {
        return defaultValue;
    }

    public String getDescription() {
        return description;
    }

    public String getFormatPattern() {
        return formatPattern;
    }

    public String getHardwareType() {
        return hardwareType;
    }

    public Integer getId() {
        return id;
    }

    public Double getMaxValue() {
        return maxValue;
    }

    public Double getMinValue() {
        return minValue;
    }

    public String getName() {
        return name;
    }

    public String getRef() {
        return ref;
    }

    public String getStorageMethod() {
        return storageMethod;
    }

    public ChannelType getType() {
        return type;
    }

    public String getUnitOfMeasure() {
        return unitOfMeasure;
    }

    public Integer getVehicleId() {
        return vehicleId;
    }

    public boolean isAlwaysDisplayed() {
        return alwaysDisplayed;
    }

    public boolean isLookup() {
        return lookup;
    }

    public boolean isVisibleOnFaultOnly() {
        return visibleOnFaultOnly;
    }
    
    public boolean isDisplayedAsDifference() {
    	return this.displayedAsDifference;
    }
    
    public RelatedChannelGroup getRelatedChannelGroup() {
        return relatedChannelGroup;
    }
    
    public String getDataType() {
        return dataType;
    }
    
    public ChannelConfigBuilder setAlwaysDisplayed(boolean alwaysDisplayed) {
        this.alwaysDisplayed = alwaysDisplayed;
        return this;
    }

    public ChannelConfigBuilder setChannelGroupId(Integer channelGroupId) {
        this.channelGroupId = channelGroupId;
        return this;
    }

    public ChannelConfigBuilder setChannelGroupOrder(Integer channelGroupOrder) {
        this.channelGroupOrder = channelGroupOrder;
        return this;
    }

    public ChannelConfigBuilder setColumnId(Integer columnId) {
        this.columnId = columnId;
        return this;
    }

    public ChannelConfigBuilder setComment(String comment) {
        this.comment = comment;
        return this;
    }

    public ChannelConfigBuilder setDefaultValue(Boolean defaultValue) {
        this.defaultValue = defaultValue;
        return this;
    }

    public ChannelConfigBuilder setDescription(String description) {
        this.description = description;
        return this;
    }

    public ChannelConfigBuilder setFormatPattern(String formatPattern) {
        this.formatPattern = formatPattern;
        return this;
    }

    public ChannelConfigBuilder setHardwareType(String hardwareType) {
        this.hardwareType = hardwareType;
        return this;
    }

    public ChannelConfigBuilder setId(Integer id) {
        this.id = id;
        return this;
    }

    public ChannelConfigBuilder setLookup(boolean lookup) {
        this.lookup = lookup;
        return this;
    }

    public ChannelConfigBuilder setMaxValue(Double maxValue) {
        this.maxValue = maxValue;
        return this;
    }

    public ChannelConfigBuilder setMinValue(Double minValue) {
        this.minValue = minValue;
        return this;
    }

    public ChannelConfigBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public ChannelConfigBuilder setRef(String ref) {
        this.ref = ref;
        return this;
    }

    public ChannelConfigBuilder setStorageMethod(String storageMethod) {
        this.storageMethod = storageMethod;
        return this;
    }

    public ChannelConfigBuilder setType(ChannelType type) {
        this.type = type;
        return this;
    }

    public ChannelConfigBuilder setUnitOfMeasure(String unitOfMeasure) {
        this.unitOfMeasure = unitOfMeasure;
        return this;
    }

    public ChannelConfigBuilder setVehicleId(Integer vehicleId) {
        this.vehicleId = vehicleId;
        return this;
    }

    public ChannelConfigBuilder setVisibleOnFaultOnly(boolean visibleOnFaultOnly) {
        this.visibleOnFaultOnly = visibleOnFaultOnly;
        return this;
    }
    
    public ChannelConfigBuilder setDisplayedAsDifference(boolean displayedAsDifference){
    	this.displayedAsDifference = displayedAsDifference;
    	return this;
    }
    
    public ChannelConfigBuilder setRelatedChannelGroup(RelatedChannelGroup relatedChannelGroup) {
        this.relatedChannelGroup = relatedChannelGroup;
        return this;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public List<String> getLinkedChannels() {
        return linkedChannels;
    }

    public void setLinkedChannels(List<String> linkedChannels) {
        this.linkedChannels = linkedChannels;
    }

    public ChannelConfigBuilder setDataType(String dataType) {
        this.dataType = dataType;
        return this;
    }
    
}
