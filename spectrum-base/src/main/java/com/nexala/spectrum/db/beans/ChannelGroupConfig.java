/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.db.beans;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

// XXX See ChannelGroup and remove it :)
public final class ChannelGroupConfig {
    private final Integer id;
    private final String name;
    private final String description;
    private final boolean visible;
    private final List<ChannelConfig> channelConfigList = new ArrayList<ChannelConfig>();

    public ChannelGroupConfig(Integer id, String name, String description,
            Boolean visible) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.visible = visible;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public synchronized List<ChannelConfig> getChannelConfigList() {
        return channelConfigList;
    }

    public synchronized void addChannelConfig(ChannelConfig channelConfig) {
        this.channelConfigList.add(channelConfig);
        Collections.sort(channelConfigList, new ChannelComparator());
    }

    public synchronized List<String> getChannelDescriptionList() {
        List<String> result = new ArrayList<String>();

        for (ChannelConfig conf : channelConfigList) {
            result.add(conf.getDescription());
        }

        return result;
    }

    public synchronized List<String> getChannelColumnNameList() {
        List<String> result = new ArrayList<String>();

        for (ChannelConfig conf : channelConfigList) {
            result.add(conf.getShortName());
        }

        return result;
    }

    public synchronized List<Integer> getChannelIdList() {
        List<Integer> result = new ArrayList<Integer>();

        for (ChannelConfig conf : channelConfigList) {
            result.add(conf.getId());
        }

        return result;
    }

    public synchronized List<String> getChannelNameList() {
        List<String> result = new ArrayList<String>();

        for (ChannelConfig conf : channelConfigList) {
            result.add(conf.getName());
        }

        return result;
    }

    public boolean isVisible() {
        return visible;
    }

    /**
     * Compare channel by the channel order, if the order is the same or there is no order sort by name.
     * Channels with no order are at the end.
     * @author brice
     *
     */
    private class ChannelComparator implements Comparator<ChannelConfig> {

        /**
         * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
         */
        @Override
        public int compare(ChannelConfig conf1, ChannelConfig conf2) {
            int result;
            if (conf1.getChannelGroupOrder() == null && conf2.getChannelGroupOrder() == null) {
                //Both not sorted
                result = conf1.getName().compareTo(conf2.getName());
            } else if (conf1.getChannelGroupOrder() == null) {
                //Only conf1 not sorted
                result = 1;
            } else if (conf2.getChannelGroupOrder() == null) {
                // Only conf2 not sorted
                result = -1;
            } else {
                // Both conf1 and conf2 sorted compare them
                result = conf1.getChannelGroupOrder().compareTo(conf2.getChannelGroupOrder());

                if (result == 0) {
                    // if the number is the same, sort by the name alphabeticaly
                    result = conf1.getName().compareTo(conf2.getName());
                }
            }

            return result;
        }
    }
}
