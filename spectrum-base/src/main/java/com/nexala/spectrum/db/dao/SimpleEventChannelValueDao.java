package com.nexala.spectrum.db.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.configuration.ApplicationConfiguration;
import com.nexala.spectrum.db.JdbcRowMapper;
import com.nexala.spectrum.db.QueryManager;
import com.nexala.spectrum.db.beans.EventChannelValue;
import com.nexala.spectrum.db.mappers.SimpleEventChannelMapper;

public class SimpleEventChannelValueDao extends GenericDao<EventChannelValue, Long> {

    private static class LightEventChannelMapper extends JdbcRowMapper<EventChannelValue> {

        @Inject
        private BoxedResultSetFactory factory;
        
        @Override
        public EventChannelValue createRow(ResultSet resultSet)
                throws SQLException {
            BoxedResultSet rs = factory.create(resultSet);
            
            Integer unitId;
            try {
                unitId = rs.getInt("UnitID");
            } catch (java.sql.SQLException e) {
                unitId = null;
            }
            
            Integer channelId;
            try {
                channelId = rs.getInt("ChannelID");
            } catch (java.sql.SQLException e) {
                channelId = null;
            }
            
            Boolean value;
            try {
                value = rs.getBoolean("Value");
            } catch (java.sql.SQLException e) {
                value = null;
            }
            
            return new EventChannelValue(null, null, unitId, channelId, value);
        }
        
    }
    
    @Inject
    private SimpleEventChannelMapper rowMapper;
    
    private final LightEventChannelMapper lightMapper;
    
    @Inject
    private ApplicationConfiguration appConf;
    
    private final String fleetId;
    
    @Inject
    public SimpleEventChannelValueDao(@Named(Spectrum.FLEET_ID) String fleetId, LightEventChannelMapper lightMapper) {
        this.fleetId = fleetId;
        this.lightMapper = lightMapper;
    }
    
    public List<EventChannelValue> getLiveEventChannelData(Long recordId, Long lastTimestamp) {
        String liveEventChannelDataQuery = appConf.get().getString("r2m."+fleetId+".stream-analysis.eventDataSp");
        return findByStoredProcedureWithTimeout(liveEventChannelDataQuery, rowMapper, 60, recordId, new Date(lastTimestamp));
    }
    
    
    public List<EventChannelValue> getEventChannelData(Integer stockId, long startTime, long endTime) {
        String liveEventChannelDataQuery = appConf.get().getString("r2m."+fleetId+".stream-analysis.eventDataTesterSp");
        return findByStoredProcedureWithTimeout(liveEventChannelDataQuery, rowMapper, 60,  new Date(startTime), new Date(endTime), stockId);
    }
    
    public List<EventChannelValue> getEventChannelDataEnrichmentData(Long recordId) {
        String latestEventDataSinceQuery = appConf.get().getString("r2m."+fleetId+".stream-analysis.eventDataLatestSinceSp");
        Integer maxTimestampDifferenceMinutes = appConf.get().getInt("r2m."+fleetId+".stream-analysis.maxEventChannelTimeDifferenceMinutes");
        
        // This data will be all the newest unique unit/channel pairings that are still valid
        // ie records that are not older than the configuration allows
        return findByStoredProcedureWithTimeout(latestEventDataSinceQuery, rowMapper, 60, recordId, maxTimestampDifferenceMinutes);
    }
    
    public List<EventChannelValue> getEventChannelDataEnrichmentData(Integer stockId, long startTime) {
        String latestEventDataSinceQuery = appConf.get().getString("r2m."+fleetId+".stream-analysis.eventDataTesterLatestSinceSp");
        Integer maxTimestampDifferenceMinutes = appConf.get().getInt("r2m."+fleetId+".stream-analysis.maxEventChannelTimeDifferenceMinutes");
        
        // This data will be all the newest unique unit/channel pairings that are still valid
        // ie records that are not older than the configuration allows
        return findByStoredProcedureWithTimeout(latestEventDataSinceQuery, rowMapper, 60,  new Date(startTime), stockId, maxTimestampDifferenceMinutes);
    }
    
    public List<EventChannelValue> getAllEventChannelValueLatest() {
        String allEventChannelValueLatestQuery = new QueryManager().getQuery(Spectrum.ALL_EVENT_CHANNEL_VALUE_LATEST_QUERY);
        return findByQuery(allEventChannelValueLatestQuery, lightMapper);
    }
    
}
