package com.nexala.spectrum.db.beans;


import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * Dto for fault meta extra fields.
 * @author jcasey
 *
 */
public class FaultExtraFieldDto implements Serializable {

    private static final long serialVersionUID = 1L;
    
    private String name;
    
    private String label;
    
    private String type;
    
    private Map<String, String> values;
    
    private Integer maxLength;
    
    private Integer min;
    
    private Integer max;
    
    private boolean sameRow;
    
    private List<Map<String, String>> disabledConditions;
    
    private String style;
    
    private boolean mandatory;
    
    private String unit;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Map<String, String> getValues() {
        return values;
    }

    public void setValues(Map<String, String> values) {
        this.values = values;
    }

    public boolean isSameRow() {
        return sameRow;
    }

    public void setSameRow(boolean sameRow) {
        this.sameRow = sameRow;
    }

    public Integer getMaxLength() {
        return maxLength;
    }

    public void setMaxLength(Integer maxLength) {
        this.maxLength = maxLength;
    }

    public Integer getMin() {
        return min;
    }

    public void setMin(Integer min) {
        this.min = min;
    }

    public Integer getMax() {
        return max;
    }

    public void setMax(Integer max) {
        this.max = max;
    }
    
    public List<Map<String, String>> getDisabledConditions() {
        return disabledConditions;
    }

    public void setDisabledConditions(List<Map<String, String>> disabledConditions) {
        this.disabledConditions = disabledConditions;
    }  

	public String getStyle() {
        return style;
    }

	public void setStyle(String style) {
        this.style = style;
    }

    public boolean isMandatory() {
        return mandatory;
    }

    public void setMandatory(boolean mandatory) {
        this.mandatory = mandatory;
    }
    
    public String getUnit() {
        return unit;
        
    }
    
    public void setUnit(String unit) {
        this.unit = unit;
    }
    
}
