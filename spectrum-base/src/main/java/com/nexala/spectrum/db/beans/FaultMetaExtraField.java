/*
 * Copyright (c) Nexala Technologies 2010, All rights reserved.
 */
package com.nexala.spectrum.db.beans;

import com.nexala.spectrum.db.dao.Record;

/**
 * Bean representing a record in FaultMetaExtraField table
 * @author BBaudry
 *
 */
public class FaultMetaExtraField implements Record {
	
    private Integer faultMetatId;
	
    private String field;
    
    private String value;

    public Integer getFaultMetatId() {
        return faultMetatId;
    }

    public void setFaultMetatId(Integer faultMetatId) {
        this.faultMetatId = faultMetatId;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
	
}
