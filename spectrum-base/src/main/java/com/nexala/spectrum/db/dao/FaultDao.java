package com.nexala.spectrum.db.dao;

import java.util.HashMap;
import java.util.Map;

import com.nexala.spectrum.db.dao.GenericDao;

public class FaultDao extends GenericDao<Object, Long> {

    public void updateRecoveryStatus(Long faultId, String recoveryStatus) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("RecoveryStatus", recoveryStatus);
        update(faultId, params);
    }
    
    @Override
    public String getTableName() {
        return "Fault";
    }

    @Override
    public String getIdColumnName() {
        return "ID";
    }
}
