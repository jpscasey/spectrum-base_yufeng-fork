package com.nexala.spectrum.db.beans;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;
import java.util.Map;


public class FaultMetaDto implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = -5120043889051796595L;
    private Integer id;
    private String faultCode;
    private String description;
    private String summary;
    private String additionalInfo;
    private String url;
    private Integer categoryId;
    private Integer type;
    private Integer fleetId; 
    private Boolean groupByFleet;
        
    private Integer groupId;
    private Integer sourceId;
    private Integer recoveryTypeId;
    private Boolean hasRecovery;
    private String recoveryProcessPath;
    private List<Integer> optionalCategoryIdList;
    private String e2mFaultCode;
    private String e2mFaultDescription;
    private String e2mPriorityCode;
    private String e2mPriority;
    private Map<String, String> extraFieldValue;
    private Map<String, String> overrideFieldValue;
    private Map<String, FaultExtraFieldDto> extraFieldConfigs;
    private String username;
    private Timestamp fromDate;    //Could also be Date
    private Timestamp toDate;      //Could also be Date
    private Integer parentId;
    private long overrideStartTimestamp;
    private long overrideEndTimestamp;
    
    public Integer getId() {
        return id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }
    
    public String getFaultCode() {
        return faultCode;
    }
    
    public void setFaultCode(String faultCode) {
        this.faultCode = faultCode;
    }
    
    public String getDescription() {
        return description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }
    
    public String getSummary() {
        return summary;
    }
    
    public void setSummary(String summary) {
        this.summary = summary;
    }
    
    public Integer getCategoryId() {
        return categoryId;
    }
    
    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }
    
    public Integer getType() {
        return type;
    }
    
    public void setType(Integer type) {
        this.type = type;
    }
    
    public Boolean getHasRecovery() {
        return this.hasRecovery;
    }
    
    public void setHasRecovery(Boolean hasRecovery) {
        this.hasRecovery = hasRecovery;
    }
    
    public Integer getRecoveryTypeId() {
        return recoveryTypeId;
    }
    
    public void setRecoveryTypeId(Integer recoveryTypeId) {
        this.recoveryTypeId = recoveryTypeId;
    }
    
    public String getRecoveryProcessPath() {
        return recoveryProcessPath;
    }
    
    public void setRecoveryProcessPath(String recoveryProcessPath) {
        this.recoveryProcessPath = recoveryProcessPath;
    }
    
    public List<Integer> getOptionalCategoryIdList() {
        return this.optionalCategoryIdList;
    }
    
    public void setOptionalCategoryIdList(List<Integer> optionalCategoryIdList) {
        this.optionalCategoryIdList = optionalCategoryIdList;
    }
    
    /**
     * Check for equality based on ID field.
     * 
     */
    public boolean equals(Object o) {
        if(!(o instanceof FaultMetaDto)) {
            return false;
        }
        
        FaultMetaDto castedO = (FaultMetaDto)o;
        return castedO.getFaultCode().equals(this.faultCode);
    }

    /**
     * Get the url.
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * Set the url.
     * @param url the url to set
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * Get the groupId.
     * @return the groupId
     */
    public Integer getGroupId() {
        return groupId;
    }

    /**
     * Set the groupId.
     * @param groupId the groupId to set
     */
    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    public Integer getSourceId() {
        return sourceId;
    }

    public void setSourceId(Integer sourceId) {
        this.sourceId = sourceId;
    }

    public String getE2mFaultCode() {
        return e2mFaultCode;
    }

    public void setE2mFaultCode(String e2mFaultCode) {
        this.e2mFaultCode = e2mFaultCode;
    }

    public String getE2mFaultDescription() {
        return e2mFaultDescription;
    }

    public void setE2mFaultDescription(String e2mFaultDescription) {
        this.e2mFaultDescription = e2mFaultDescription;
    }

    public String getE2mPriorityCode() {
        return e2mPriorityCode;
    }

    public void setE2mPriorityCode(String e2mPriorityCode) {
        this.e2mPriorityCode = e2mPriorityCode;
    }

    public String getE2mPriority() {
        return e2mPriority;
    }

    public void setE2mPriority(String e2mPriority) {
        this.e2mPriority = e2mPriority;
    }

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public void setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

    public Map<String, String> getExtraFieldValue() {
        return extraFieldValue;
    }

    public void setExtraFieldValue(Map<String, String> extraFieldValue) {
        this.extraFieldValue = extraFieldValue;
    }
    
    public Map<String, String> getOverrideFieldValue() {
        return overrideFieldValue;
    }

    public void setOverrideFieldValue(Map<String, String> overrideFieldValue) {
        this.overrideFieldValue = overrideFieldValue;
    }
    
    public long getOverrideStartTimestamp() {
        return overrideStartTimestamp;
    }
    
    public void setOverrideStartTimestamp(long overrideStartTimestamp) {
        this.overrideStartTimestamp = overrideStartTimestamp;
    }

    public long getOverrideEndTimestamp() {
        return overrideEndTimestamp;
    }
    
    public void setOverrideEndTimestamp(long overrideEndTimestamp) {
        this.overrideEndTimestamp = overrideEndTimestamp;
    }
    
    public Integer getOverrideType() {
        return type;
    }
    
    public void setOverrideType(Integer type) {
        this.type = type;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Timestamp getFromDate() {
        return fromDate;
    }

    public void setFromDate(Timestamp time) {
        this.fromDate = time;
    }

    public Timestamp getToDate() {
        return toDate;
    }

    public void setToDate(Timestamp timestamp) {
        this.toDate = timestamp;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

	public Integer getFleetId() {
		return fleetId;
	}

	public void setFleetId(Integer fleetId) {
		this.fleetId = fleetId;
	}

	public Boolean getGroupByFleet() {
		return groupByFleet;
	}

	public void setGroupByFleet(Boolean groupByFleet) {
		this.groupByFleet = groupByFleet;
	}

	public Map<String, FaultExtraFieldDto> getExtraFieldConfigs() {
		return extraFieldConfigs;
	}

	public void setExtraFieldConfigs(Map<String, FaultExtraFieldDto> extraFieldConfig) {
		this.extraFieldConfigs = extraFieldConfig;
	}
}