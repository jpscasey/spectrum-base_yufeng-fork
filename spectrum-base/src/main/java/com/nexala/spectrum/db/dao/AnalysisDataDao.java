package com.nexala.spectrum.db.dao;

import com.nexala.spectrum.analysis.AnalysisData;
import com.nexala.spectrum.analysis.bean.AnalysisDataMap;

public interface AnalysisDataDao<T extends AnalysisData> {
	public AnalysisDataMap<T> getLiveData(Long lastRecordId, Long... moreRecordIds);
}
