package com.nexala.spectrum.db.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.db.JdbcRowMapper;
import com.nexala.spectrum.db.QueryManager;
import com.nexala.spectrum.db.beans.FaultTransmission;

public class FaultTransmissionDao extends GenericDao<FaultTransmission, Integer> {
    
    private class FaultTransmissionMapper extends
            JdbcRowMapper<FaultTransmission> {
        @Override
        public FaultTransmission createRow(ResultSet resultSet) throws SQLException {
            
            FaultTransmission ft = new FaultTransmission();
            
            ft.setTransmissionId(resultSet.getLong("ID"));
            ft.setFaultId(resultSet.getLong("FaultID"));
            ft.setLocationName(resultSet.getString("LocationName"));
            ft.setLocationCode(resultSet.getString("LocationCode"));
            ft.setNotes(resultSet.getString("Notes"));
            ft.setRetries(resultSet.getInt("Retries"));
            ft.setTransmissionStatus(resultSet.getString("TransmissionStatus"));
            ft.setUnitNumber(resultSet.getString("UnitNumber"));
            ft.setVehicleNumber(resultSet.getString("FaultVehicleNumber"));
            ft.setE2mFaultCode(resultSet.getString("E2MFaultCode"));
            ft.setE2mFaultDescription(resultSet.getString("E2MDescription"));
            ft.setE2mPriorityCode(resultSet.getString("E2MPriorityCode"));
            ft.setE2mPriority(resultSet.getString("E2MPriority"));
            
            return ft;
        }
    }

    public List<FaultTransmission> searchData() {
        String query = new QueryManager().getQuery(Spectrum.FAULT_SENDER_GET_DATA);
        List<FaultTransmission> result = findByQuery(query, new FaultTransmissionMapper());
        return result;
    }

    public void insertFaultTransmission(Long eventId) {
        String query = new QueryManager().getQuery(Spectrum.FAULT_SENDER_INSERT);
        executeStoredProcedure(query, eventId);
    }
    
    public void updateFaultTransmission(Long id, String response, boolean successful, Integer maxRetries) {
        String query = new QueryManager().getQuery(Spectrum.FAULT_SENDER_UPDATE);
        executeStoredProcedure(query, id, response, successful, maxRetries);
    }
}
