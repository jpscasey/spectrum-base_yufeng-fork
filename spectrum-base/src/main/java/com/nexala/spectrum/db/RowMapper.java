/*
 * Copyright (c) Nexala Technologies 2013, All rights reserved.
 */
package com.nexala.spectrum.db;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public interface RowMapper<T> {

    /**
     * This method should create a bean object from the specified resultSet. This specified bean should be a subclass of
     * record. This method is invoked through createRows(ResultSet resultSet) and should not be invoked directly.
     * 
     * @param resultSet
     * @return
     * @throws SQLException
     */
    T createRow(ResultSet resultSet) throws SQLException;

    /**
     * Returns an ArrayList of beans of type T. This method is dependant on the createRow(ResultSet resultSet) method,
     * which <i>must</i> be overridden in a child class.
     * 
     * @param resultSet
     * @return The List of beans of type T.
     * @throws SQLException
     */
    List<T> createRows(ResultSet resultSet) throws SQLException;

    /**
     * This method returns an instance of an object, which implements the List interface. This method returns an
     * instance of an ArrayList by default, but may be overridden to return another implementation of List to prevent
     * expensive conversions, in higher level code.
     * 
     * <pre>
     * {@code
     * &#064;Override public List&lt;Foo&gt; newList() {
     *  return new LinkedList&lt;Foo&gt;();
     * }
     * </pre>
     * 
     * @return an instance of an object implementing the List interface.
     */
    List<T> newList();
}
