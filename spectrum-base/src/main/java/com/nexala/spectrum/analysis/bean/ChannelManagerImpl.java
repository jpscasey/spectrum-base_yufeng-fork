package com.nexala.spectrum.analysis.bean;

import java.util.List;

import javax.inject.Singleton;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.analysis.manager.AbstractChannelManager;
import com.nexala.spectrum.configuration.ApplicationConfiguration;
import com.nexala.spectrum.configuration.ChannelConfiguration;
import com.nexala.spectrum.db.beans.ChannelConfig;
import com.typesafe.config.Config;

/**
 * Implementation of channel manager of r2m.
 * Initialize the channel manager with all the channels in database + the one describe in the application conf of any 
 * @author BBaudry
 *
 */
@Singleton
public class ChannelManagerImpl extends AbstractChannelManager {

    private ChannelConfiguration channelConf;

    private ApplicationConfiguration appConf;

    private String fleetId;

    @Inject
    public ChannelManagerImpl(ChannelConfiguration channelConf, ApplicationConfiguration appConf,
            @Named(Spectrum.FLEET_ID) String fleetId) {
        this.channelConf = channelConf;
        this.appConf = appConf;
        this.fleetId = fleetId;
        initialize();
    }

    protected void initialize() {
        List<ChannelConfig> configList = channelConf.getConfigList();

        for (ChannelConfig c : configList) {
            Class<?> returnType = null;
            String type = c.getDataType();
            if (type != null) {
                if (type.toLowerCase().startsWith("decimal")) {
                    returnType = Double.TYPE;
                } else if (type.equalsIgnoreCase("bit")) {
                    returnType = Boolean.TYPE;
                } else if (type.toLowerCase().startsWith("varchar")) {
                    returnType = String.class;
                } else if (type.equalsIgnoreCase("int") || type.equalsIgnoreCase("tinyint") 
                        || type.equalsIgnoreCase("smallint")) {
                    returnType = Integer.TYPE;
                } else if (type.equalsIgnoreCase("bigint")) {
                    returnType = Long.TYPE;
                }
            }
            addChannel("ch" + c.getId(), returnType); // FIXME remove this stupid ch from everywhere
        }

        addExtraChannels();

        // Add the extra channels from the project configuration if any.
        String path = "r2m." + fleetId + ".stream-analysis.extra-channels";
        if (appConf.get().hasPath(path)) {
            List<? extends Config> config = appConf.get().getConfigList(path);
            for (Config c : config) {
                Class<?> returnType = null;
                String name = c.getString("name");
                String type = c.getString("type");

                if (type != null) {
                    if (type.equalsIgnoreCase("Double")) {
                        returnType = Double.TYPE;
                    } else if (type.equalsIgnoreCase("Boolean")) {
                        returnType = Boolean.TYPE;
                    } else if (type.equalsIgnoreCase("String")) {
                        returnType = String.class;
                    } else if (type.equalsIgnoreCase("Integer")) {
                        returnType = Integer.TYPE;
                    } else if (type.equalsIgnoreCase("Long")) {
                        returnType = Long.TYPE;
                    }
                }

                addChannel(name, returnType);
            }
        }
        
        // Add the sources from the project configuration if any
        String sourcesPath = "r2m." + fleetId + ".stream-analysis.sources";
        if (appConf.get().hasPath(sourcesPath)) {
            List<String> sources = appConf.get().getStringList(sourcesPath);
            for (String source : sources) {
                addSource(source);
            }
        }
        
    }
}
