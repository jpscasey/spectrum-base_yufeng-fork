package com.nexala.spectrum.analysis.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.inject.Inject;
import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.analysis.bean.AnalysisDataMap;
import com.nexala.spectrum.analysis.bean.EventReference;
import com.nexala.spectrum.analysis.bean.GenericEvent;
import com.nexala.spectrum.analysis.handlers.EmailHandler;
import com.nexala.spectrum.db.dao.EventDao;
import com.nexala.spectrum.db.dao.LiveFaultChannelDataDao;
import com.nexala.spectrum.rest.data.beans.Event;

/**
 * Analysis provider for the generic event.
 * 
 * @author BBaudry
 *
 */
public class GenericAnalysisProvider extends AbstractAnalysisProvider<GenericEvent> {

    @Inject
    private LiveFaultChannelDataDao channelDataDao;
    
    @Inject
    private EventDao eventDao;
    
    @Inject
    private EmailHandler eh;

    @Override
    public AnalysisDataMap<GenericEvent> getData(List<Long> lastRecordIds) {
        return channelDataDao.getLiveData(lastRecordIds.size() == 0 ? null : lastRecordIds.get(0));
    }
    
    @Override
    public Map<Integer, List<GenericEvent>> getData(Integer stockId, long startTime, long endTime) {
        return channelDataDao.getData(stockId, startTime, endTime);
    }

    /**
     * @see com.nexala.spectrum.analysis.data.AnalysisDataProvider#createFault(java.lang.String, java.lang.Long, java.lang.Integer)
     */
    @Override
    public long createFault(String faultCode, Long timestamp, Integer sourceId, String ruleName, String contextId, String fleetId, Map<String, String> extraFields) {
        return eventDao.createFault(faultCode, timestamp, sourceId, ruleName, contextId, fleetId);
    }

    /**
     * @see com.nexala.spectrum.analysis.data.AnalysisDataProvider#retractFault(long, long)
     */
    @Override
    public void retractFault(long faultId, long timestamp) {
        eventDao.retractFault(faultId, timestamp);
    }

    /**
     * @see com.nexala.spectrum.analysis.data.AnalysisDataProvider#sendFaultEmail(long, java.lang.String)
     */
    @Override
    public void sendFaultEmail(long faultId, String emailPath) {
        Event event = eventDao.getEventById(faultId);
        eh.sendEmail(emailPath, event);
    }

    @Override
    public void retractFaults() {
        eventDao.retractFaults();
        
    }

    @Override
    public Map<Integer, List<EventReference>> getActivatedEvents() {
        
        Map<Integer, List<EventReference>> result = new HashMap<>();
        List<Event> allLive = eventDao.getAllLive();
        for (Event current : allLive) {
            Integer sourceId = (Integer) current.getField(Spectrum.EVENT_SOURCE_ID);
            
            List<EventReference> lst = result.get(sourceId);
            
            if (lst == null) {
                lst = new ArrayList<>();
                result.put(sourceId, lst);
            }
            
            EventReference event = new EventReference();
            event.setId(((Integer) current.getField(Spectrum.EVENT_ID)).longValue());
            event.setFaultCode((String) current.getField(Spectrum.EVENT_CODE));
            lst.add(event);
        }
        
        return result;
    }
}