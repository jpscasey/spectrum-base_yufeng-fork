package com.nexala.spectrum.analysis.handlers;

import java.text.DateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.inject.Inject;
import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.configuration.ApplicationConfiguration;
import com.nexala.spectrum.rest.data.beans.Event;
import com.typesafe.config.Config;


public class DefaultEmailConfiguration implements EmailConfiguration<Event> {

    @Inject
    private ApplicationConfiguration appConfig;
    
    @Override
    public Map<String, String> getVariableMap(Event event, DateFormat dateFormat) {
        Map<String, String> variableMap = new HashMap<String, String>();

        if (event != null) {
            String formattedCreateTime = null;

            if (event.getField(Spectrum.EVENT_CREATE_TIME) != null) {
                synchronized (dateFormat) {
                    formattedCreateTime = dateFormat.format(
                            new Date((Long) event.getField(Spectrum.EVENT_CREATE_TIME)));
                }
            }
            
            List<? extends Config> variables = appConfig.get().getConfigList("r2m.editor.variables");
            for (Config current : variables) {
            	String variableName = current.getString("name");
            	String fieldName = current.getString("db");
            	if(fieldName.equals(Spectrum.EVENT_CREATE_TIME)) {
            		variableMap.put(variableName, formattedCreateTime);
            	} else {
            		variableMap.put(variableName, validateField(event.getField(fieldName)));
            	}
            }
        }
        return variableMap;
    }

    private String validateField(Object value) {
        return value == null ? "" : value.toString();
    }
}
