/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.analysis;

public final class EventSource {

    /** The ID of the vehicle / unit, etc., which is being analysed */
    private final Integer sourceId;

    /**
     * The descriptive name of the vehicle / unit, etc., which is being analysed
     */
    private final String sourceDesc;

    public EventSource(Integer sourceId, String sourceDesc) {
        if (sourceId == null) {
            throw new NullPointerException("sourceId may not be null");
        }
        
        if (sourceDesc == null) {
            throw new NullPointerException("sourceDesc may not be null");
        }
        
        this.sourceId = sourceId;
        this.sourceDesc = sourceDesc;
    }

    public Integer getSourceId() {
        return sourceId;
    }

    public String getSourceDesc() {
        return sourceDesc;
    }
}
