/**
 * Copyright (c) Nexala Technologies 2015, All rights reserved.
 */
package com.nexala.spectrum.analysis.data;

import java.util.List;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.analysis.AnalysisData;
import com.nexala.spectrum.analysis.bean.ChannelManagerConfiguration;
import com.nexala.spectrum.analysis.manager.ChannelManager;
import com.nexala.spectrum.configuration.ApplicationConfiguration;
import com.nexala.spectrum.db.dao.RuleEngineCurrentIdsDao;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigRenderOptions;

/**
 * Implements some default behavior for analysis provider
 * 
 * @author bbaudry
 *
 */
public abstract class AbstractAnalysisProvider<T extends AnalysisData> implements AnalysisDataProvider<T> {

    @Inject
    protected RuleEngineCurrentIdsDao currentIdsDao;
    
    @Inject
    protected ApplicationConfiguration appConf;
    
    @Inject
    protected ChannelManager channelManager;
    
    @Inject
    @Named(Spectrum.FLEET_ID)
    protected String fleetId;
    
    /**
     * @see com.nexala.spectrum.analysis.data.AnalysisDataProvider#getCurrentIds()
     */
    @Override
    public List<Long> getCurrentIds(String configurationName) {
        return currentIdsDao.getCurrentIds(configurationName);
    }

    /**
     * @see com.nexala.spectrum.analysis.data.AnalysisDataProvider#updateCurrentIds(java.util.List)
     */
    @Override
    public void updateCurrentIds(String configurationName, List<Long> ids) {
        currentIdsDao.updateCurrentIds(configurationName, ids);
    }

    /**
     * @see com.nexala.spectrum.analysis.data.AnalysisDataProvider#getStreamAnalysisConfiguration(java.lang.String)
     */
    @Override
    public String getStreamAnalysisConfiguration() {
        Config defaultConf = appConf.get().getConfig("r2m.stream-analysis.config");

        Config config = defaultConf;
        String path = "r2m." + fleetId + ".stream-analysis.config";
        if (appConf.get().hasPath(path)) {
            // There is a fleet specific configuration.
            config = appConf.get().getConfig(path).withFallback(defaultConf);
        }

        return config.root().render(ConfigRenderOptions.concise().setJson(true));
    }

    /**
     * @see com.nexala.spectrum.analysis.data.AnalysisDataProvider#getChannelManagerConfiguration()
     */
    @Override
    public ChannelManagerConfiguration getChannelManagerConfiguration() {
        return channelManager.getAsChannelManagerConfiguration();
    }

}
