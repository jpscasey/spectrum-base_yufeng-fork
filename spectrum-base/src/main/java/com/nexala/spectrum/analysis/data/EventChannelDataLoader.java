package com.nexala.spectrum.analysis.data;

import com.nexala.spectrum.analysis.AnalysisData;
import com.nexala.spectrum.rest.data.beans.DataSet;

public interface EventChannelDataLoader<T extends AnalysisData> {
    T loadDataSet(T stock, DataSet data); 
}
