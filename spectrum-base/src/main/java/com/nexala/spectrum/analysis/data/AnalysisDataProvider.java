package com.nexala.spectrum.analysis.data;

import java.util.List;
import java.util.Map;

import com.nexala.spectrum.analysis.AnalysisData;
import com.nexala.spectrum.analysis.bean.AnalysisDataMap;
import com.nexala.spectrum.analysis.bean.ChannelManagerConfiguration;
import com.nexala.spectrum.analysis.bean.EventReference;

/**
 * Provider for a stream analysis engine.
 * @author BBaudry
 *
 * @param <T> The bean used by the rule engine
 */
public interface AnalysisDataProvider<T extends AnalysisData> {
    /**
     * Returns all channels with IDs greater than the lastRecordId
     * 
     * @param lastRecordIds A list of the last record Ids inserted into the analysis engine. Generally, this list
     *  will only contain a single record (may be null). However, if there are multiple data sources, e.g. TMDS data,
     *  Teloc Data. It will be necessary to transmit multiple record Ids.
     *  
     *  It is the responsibility of the implementing project to ensure that the AnalysisDataMap, record IDs are always
     *  saved in the same order. If one or more records are not received / 
     *  
     * @return
     */
    public AnalysisDataMap<T> getData(List<Long> lastRecordIds);
    
    
    /**
     * Return a map of activated event codes by sourceId.
     * Used by the rule engine when faults can be deactivated manually (outside of rule engine),
     * or when the faults shouldn't be retracted at startup.
     * 
     * @return the map of activated events by sourceId
     */
    public Map<Integer, List<EventReference>> getActivatedEvents();
    
    /**
     * This method returns channel data (of type T), between the specified start
     * and end times for the specified stock. Whether this method returns
     * vehicle / unit data is up to the API user to decide.
     * 
     * @param stockId
     *            The stockId (vehicleId to test)
     * @param startTime
     * @param endTime
     * 
     * @return a List<? extends AnalysisData> of channel data which can be inserted
     *         into the fault engine
     */
    public Map<Integer, List<T>> getData(Integer stockId,
            long startTime, long endTime);
    
    /**
     * Update the list of the last ids the rule engine processed.
     * @param configurationName the configuration name
     * @param ids list of last ids the rule engine processe
     */
    public void updateCurrentIds(String configurationName, List<Long> ids);
    
    /**
     * Get the last ids the rule engine processed.
     * @param configurationName the configuration name
     * @return the last ids the rule engine processed
     */
    public List<Long> getCurrentIds(String configurationName);
    
    /**
     * Return the configuration of the rule engine in JSON format.
     * @return the configuration of the rule engine as a string
     */
    public String getStreamAnalysisConfiguration();
    
    /**
     * Return the channel manager configuration.
     * @param fleetId the fleet id
     * @return the list of channel
     */
    public ChannelManagerConfiguration getChannelManagerConfiguration();
    
    
    /**
     * Create the fault with the details in parameter.
     * @param faultCode the fault code
     * @param timestamp the timestamp
     * @param sourceId the source id
     * @param ruleName the name of the rule
     * @param contextId the context id
     * @param extraFields any extra fields to be processed as part of creating a fault
     * @throws SqlRuntimeException
     *             if execution fails for any reason. This happen if the fault
     *             is a duplicate, the vehicle info is invalid or if ChannelData
     *             doesn't exist for the vehicle...
     * @return id of the fault created
     */
    public long createFault(String faultCode, Long timestamp, Integer sourceId, String ruleName, String contextId, String fleetId, Map<String, String> extraFields);
    
    
    /**
     * Retract a fault with the details in parameter
     * @param faultId the fault id
     * @param timestamp when the fault was retracted
     */
    public void retractFault(long faultId, long timestamp);
    
    
    /**
     * Retract all the faults.
     */
    public void retractFaults();
    
    /**
     * Send the fault email 
     * @param faultId the fault id
     * @param emailPath path of the email template
     */
    public void sendFaultEmail(long faultId, String emailPath);
}