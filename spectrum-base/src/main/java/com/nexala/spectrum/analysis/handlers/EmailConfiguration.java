package com.nexala.spectrum.analysis.handlers;

import java.text.DateFormat;
import java.util.Map;

import com.google.inject.ImplementedBy;

@ImplementedBy(DefaultEmailConfiguration.class)
public interface EmailConfiguration<T> {
    Map<String, String> getVariableMap(T event, DateFormat dateFormat);
}
