package com.nexala.spectrum.analysis.data;

import com.nexala.spectrum.channel.ChannelCollection;
import com.nexala.spectrum.rest.data.beans.DataSetImpl;

public class AnalysisDataSet extends DataSetImpl {
    private final Long rowId;
    
    public AnalysisDataSet(Long rowId, Integer id, Long timestamp, ChannelCollection cc) {
        super(id, timestamp, cc);
        this.rowId = rowId;
    }

    public Long getRowId() {
        return rowId;
    }
}
