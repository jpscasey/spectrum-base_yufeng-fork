package com.nexala.spectrum.providers.chart.impl;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import org.apache.log4j.Logger;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.nexala.spectrum.caching.CacheWrapper;
import com.nexala.spectrum.caching.EhCacheWrapper;
import com.nexala.spectrum.charting.StaticChart;
import com.nexala.spectrum.charting.StaticChart.ChartType;
import com.nexala.spectrum.charting.StaticChartConfig;
import com.nexala.spectrum.charting.StaticChartFactory;
import com.nexala.spectrum.db.beans.ChannelConfig;
import com.nexala.spectrum.db.beans.ChartVehicleType;
import com.nexala.spectrum.db.dao.charting.ChannelType;
import com.nexala.spectrum.db.dao.charting.ChartChannel;
import com.nexala.spectrum.db.dao.charting.DataViewConfiguration;
import com.nexala.spectrum.rest.data.ChannelDataProvider;
import com.nexala.spectrum.rest.data.ChartDataProvider;
import com.nexala.spectrum.rest.data.ChartVehicleTypeProvider;
import com.nexala.spectrum.rest.data.StaticChartProvider;
import com.nexala.spectrum.rest.data.beans.DataSet;
import com.nexala.spectrum.service.bean.StaticChartBean;
import com.nexala.spectrum.service.bean.StaticChartKey;
import com.nexala.spectrum.service.bean.StaticCharts;
import com.nexala.spectrum.utils.DateUtils;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.config.CacheConfiguration;
import net.sf.ehcache.config.MemoryUnit;
import net.sf.ehcache.config.PersistenceConfiguration;
import net.sf.ehcache.config.PersistenceConfiguration.Strategy;

/**
 * A static chart provider.
 * 
 * @author brice
 *
 */
@Singleton
public class StaticChartProviderImpl implements StaticChartProvider {

    private static final Logger logger = Logger
            .getLogger(StaticChartProviderImpl.class);

    // Minimum height for the charts
    private static final int MIN_HEIGHT = 50;

    private CacheWrapper<StaticChartKey, StaticCharts> cache;

    @Inject
    private Map<String, ChartDataProvider> chartProviders;

    @Inject
    private StaticChartFactory chartFactory;

    @Inject
    private Map<String, ChannelDataProvider> cdProviders;

    @Inject
    private Map<String, ChartVehicleTypeProvider> vehicleTypeChartProviders;

    public static final String CACHE_NAME = "staticsCharts";

    @Inject
    public StaticChartProviderImpl(CacheManager cacheManager) {

        CacheConfiguration cacheConfig = new CacheConfiguration()
                .timeToLiveSeconds(60).maxBytesLocalHeap(5,
                        MemoryUnit.MEGABYTES);
        cacheConfig.setName(CACHE_NAME);

        PersistenceConfiguration persistanceConfig = new PersistenceConfiguration();
        persistanceConfig.strategy(Strategy.NONE);
        cacheConfig.addPersistence(persistanceConfig);

        Cache cacheObj = new Cache(cacheConfig);
        cacheManager.addCache(cacheObj);
        // Ehcache cacheObj = cacheManager.addCacheIfAbsent(CACHE_NAME);
        cache = new EhCacheWrapper<StaticChartKey, StaticCharts>(cacheObj);
    }

    /**
     * 
     * Return the static chart.
     * 
     * @param fleetId
     * @param vehicleId
     * @param interval
     * @param endTimeParam
     * @param timezone
     * @param chartId
     * @param height
     * @param width
     * @return
     * @throws IOException
     */
    public byte[] getChart(String fleetId, int vehicleId, int interval,
            Long endTime, String timezone, Integer chartId, Integer height,
            Integer width) throws IOException {

        byte[] result = null;

        StaticCharts charts = null;
        StaticChartKey key = new StaticChartKey(fleetId, vehicleId, interval,
                endTime, timezone);

        try {
            cache.acquireReadLockOnKey(key);
            charts = cache.get(key);
        } finally {
            cache.releaseReadLockOnKey(key);
        }

        if (charts != null && charts.getChart(chartId) != null) {
            result = charts.getChart(chartId).getChartImage();
        }

        return result;
    }

    /**
     * @see com.nexala.spectrum.greateranglia.providers.chart.StaticChartProvider#computeChart(java.lang.String,
     *      int, int, java.lang.Long, int, java.lang.Integer, java.lang.Integer)
     */
    @Override
    public void computeChart(String fleetId, int vehicleId, int interval,
            Long endTime, String timezone, Integer chartId, Integer width,
            Integer height) throws IOException {

        StaticChartKey key = new StaticChartKey(fleetId, vehicleId, interval,
                endTime, timezone);

        try {
            cache.acquireWriteLockOnKey(key);

            String vehicleType = getVehicleType(fleetId, chartId);

            List<StaticChartConfig> chartConfs = getAllChannelsConfig(fleetId,
                    vehicleType);

            StaticCharts charts = new StaticCharts();

            List<StaticChart> chartList = new ArrayList<>();
            
            // Generate diagrams
            for (StaticChartConfig config : chartConfs) {

                logger.debug("STATICCHARTCONFIG");
                logger.debug(config);
                
                List<Integer> channelIds = config.getChannelsId();
                TimeZone tz = DateUtils.getClientTimeZone(timezone);

                List<ChannelConfig> channels = configurations(channelIds,
                        fleetId);
                Integer computedHeight = height;
                if (config.getType().equals(ChartType.DIGITAL)) {
                    if (channels.size() == 1)
                        computedHeight = MIN_HEIGHT;
                    else
                        computedHeight = channels.size() * 25 + 15;

                    // ON jfree chart the last element added is displayed first,
                    // reverse the list order
                    Collections.reverse(channels);
                }

                StaticChart chart = chartFactory.create(width, computedHeight,
                        channels, tz, config);
                
                chartList.add(chart);
                
            }
            
            // Plot the data
            plotData(fleetId, vehicleId, interval, endTime, chartConfs, chartList);
            
            // Build the charts
            for (StaticChart chart : chartList) {
                StaticChartBean current = new StaticChartBean();
                current.setChartId(chart.getConfig().getId());
                
                byte[] image = chart.render(interval * 1000, endTime);

                current.setChartImage(image);
                charts.addChart(current);
                
            }

            cache.put(key, charts);

        } finally {
            cache.releaseWriteLockOnKey(key);
        }

    }

    /**
     * Plot the data for the charts passed in parameter.
     * @param fleetId the fleetId
     * @param vehicleId the vehicleId
     * @param interval how much data should be displayed in seconds
     * @param endTime end time
     * @param chartConfs the chart configuration
     * @param chartList list of charts
     */
    private void plotData(String fleetId, int vehicleId, int interval, Long endTime,
            List<StaticChartConfig> chartConfs, List<StaticChart> chartList) {
        // Get list of all channels used on static charts for this vehicle
        // type
        List<Integer> allChannelIds = new ArrayList<Integer>();
        for (StaticChartConfig config : chartConfs) {
            allChannelIds.addAll(config.getChannelsId());
        }
        
        // Use list of channel IDs to get all channel configs
        List<ChannelConfig> allChannels = chartProviders.get(fleetId)
                .getConfigurations(allChannelIds);
        
        Double samplingRate = interval * 1000.0 / 1200;
        
        long dataStartTime = endTime - interval*1000;
        long currentStartTime = dataStartTime;
        
        int dataChunck = 600; // 10 minutes of data
        
        // Iterate to get all the data by chuncks of 10 minutes
        while (currentStartTime < (endTime+1)) {
            long currentEndTime = currentStartTime + (dataChunck*1000); 
            if (currentEndTime > endTime) {
                currentEndTime = endTime;
            }
            
            // Get datas
            List<DataSet> data = cdProviders.get(fleetId).getSampledVehicleData(
                        vehicleId, currentStartTime, currentEndTime, allChannels, false,
                        samplingRate.longValue());

            for (StaticChart sc : chartList) {
                sc.plot(data);
            }
            
            currentStartTime = currentEndTime + 1; // position start to the next chunck of data
        }
        
    }

    /**
     * Return all the chart configurations for the fleet.
     * 
     * @param fleetId
     *            the fleet id
     * @return all the channels configurations
     */
    public List<StaticChartConfig> getAllChannelsConfig(String fleetId) {
        List<StaticChartConfig> result = new ArrayList<StaticChartConfig>();

        List<ChartVehicleType> chartVehicleTypes = vehicleTypeChartProviders
                .get(fleetId).getAllChartVehicleTypes();
        Map<Integer, String> vehicleTypebyChartId = new HashMap<Integer, String>();
        for (ChartVehicleType current : chartVehicleTypes) {
            vehicleTypebyChartId.put(current.getChartId(),
                    current.getVehicleType());
        }

        List<DataViewConfiguration> configs = chartProviders.get(fleetId)
                .findSystemConfigurations();
        for (DataViewConfiguration config : configs) {

            StaticChartConfig chart = getChartConfig(fleetId, config);

            String vehicleType = vehicleTypebyChartId.get(chart.getId());
            // set the vehicle type to the configuration
            chart.setVehicleType(vehicleType);

            result.add(chart);
        }

        return result;
    }

    /**
     * Return all the chart configurations for the fleet.
     * 
     * @param fleetId
     *            the fleet id
     * @return all the channels configurations
     */
    public List<StaticChartConfig> getAllChannelsConfig(String fleetId,
            String vehicleType) {
        List<StaticChartConfig> result = new ArrayList<StaticChartConfig>();

        for (StaticChartConfig current : getAllChannelsConfig(fleetId)) {
            if (current.getVehicleType() != null 
                    && current.getVehicleType().equals(vehicleType)) {
                result.add(current);
            }
        }

        return result;
    }

    /**
     * Return the static chart configuration for the chart config in parameter
     * 
     * @param fleetId
     *            the fleet id
     * @param config
     *            the configuration
     * @return the static chart configuration
     */
    private StaticChartConfig getChartConfig(String fleetId,
            DataViewConfiguration config) {
        List<ChartChannel> chartChannels = chartProviders.get(fleetId)
                .findChannels(config.getId());
        config.setChannels(chartChannels);

        StaticChartConfig chart = new StaticChartConfig();
        chart.setChartName(config.getName());
        chart.setId(config.getId().intValue());
        chart.setType(ChartType.DIGITAL);
        chart.setTicks(config.getTicks());

        Map<Integer, BigDecimal> scaleByChannelsId = new HashMap<Integer, BigDecimal>();
        List<Integer> channels = new ArrayList<Integer>();
        BigDecimal lowerBound = null;
        BigDecimal upperBound = null;

        for (ChartChannel chan : config.getChannels()) {
            channels.add(chan.getChannel().getId());
            // TODO it's not efficient to get the chart type
            if (ChannelType.ANALOG.equals(chan.getChannel().getType())) {
                chart.setType(ChartType.ANALOGUE);
            }

            BigDecimal minValue = chan.getMinValue();
            BigDecimal maxValue = chan.getMaxValue();

            if (minValue != null) {
                if (lowerBound == null || minValue.compareTo(lowerBound) < 0) {
                    lowerBound = minValue;
                }
            }
            if (maxValue != null) {
                if (upperBound == null || maxValue.compareTo(upperBound) > 0) {
                    upperBound = maxValue;
                }
            }

            scaleByChannelsId.put(chan.getChannel().getId(), chan.getScale());
        }
        chart.setChannelsId(channels);
        chart.setLowerBound(lowerBound);
        chart.setUpperBound(upperBound);
        chart.setScaleByChannelsId(scaleByChannelsId);

        // XXX do that in another way
        //applySpecificChartType(chart, fleetId);

        return chart;
    }

    /**
     * FIXME should not do such thing, the chart type should probably be in the
     * chart table Set the chart type in specific cases
     * 
     * @param chart
     *            the static chart
     * @param fleetId
     *            the fleet id
     */
//    private void applySpecificChartType(StaticChartConfig chart, String fleetId) {
//        if (GreaterAnglia.CLASS90.equals(fleetId) && chart.getId().equals(2)) {
//            chart.setType(ChartType.DIGITAL);
//        }
//    }

    /**
     * Return the list of channel configuration for the given ids.
     * 
     * @param ids
     *            list of channels ids
     * @param fleetId
     *            the fleet id
     * @return the list of channel configuration for the given ids.
     */
    private List<ChannelConfig> configurations(List<Integer> ids, String fleetId) {
        List<ChannelConfig> configurations = new ArrayList<ChannelConfig>(
                ids.size());

        for (Integer id : ids) {
            ChannelConfig config = chartProviders.get(fleetId)
                    .getChannelConfiguration().getConfig(id);
            if (config != null) {
                configurations.add(config);
            }
        }

        return configurations;
    }

    /**
     * Get the vehicle type of the chart passed in parameter
     * 
     * @param fleetId
     *            the fleet id
     * @param chartId
     *            the chart id
     * @return the vehicle type
     */
    private String getVehicleType(String fleetId, Integer chartId) {
        // FIXME should be a better way to do that
        ChartVehicleTypeProvider provider = vehicleTypeChartProviders
                .get(fleetId);
        for (ChartVehicleType current : provider.getAllChartVehicleTypes()) {
            if (current.getChartId().equals(chartId)) {
                return current.getVehicleType();
            }
        }

        return null;
    }

}
