package com.nexala.spectrum.eventanalysis.beans;

public class EAnalysisVehicle {
    private final String fleetCode;
    private final int id;
    private final String number;
    private final String type;

    public EAnalysisVehicle(String fleetCode, int id, String number, String type) {
        this.fleetCode = fleetCode;
        this.id = id;
        this.number = number;
        this.type = type;
    }

    public String getFleetCode() {
        return fleetCode;
    }

    public int getId() {
        return id;
    }

    public String getNumber() {
        return number;
    }

    public String getType() {
        return type;
    }
}
