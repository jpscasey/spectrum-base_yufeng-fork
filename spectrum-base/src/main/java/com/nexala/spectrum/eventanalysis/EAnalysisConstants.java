/*
 * Copyright (c) Nexala Technologies 2013, All rights reserved.
 */
package com.nexala.spectrum.eventanalysis;


public class EAnalysisConstants {
    public static final String EVENT_ANALYSIS_DESC = "Event Analysis";
    public static final String EVENT_ANALYSIS_VIEW = "eventAnalysis";
    public static final String EVENT_ANALYSIS_LICENCE_KEY = "spectrum.eventanalysis";

    public static final String LABEL_COLUMN = "labelColumn";
    public static final String SORTER = "sorter";
    public static final String EMPTY_KEY = "emptyKey";
    public static final String EMPTY_KEY_VALUE = "-1";

    // Queries
    public static final String EVENT_ANALYSIS_BEANS = "eventAnalysisBeans";
    public static final String EVENT_ANALYSIS_VEHICLE_TYPES = "eventAnalysisVehicleTypes";
    public static final String EVENT_ANALYSIS_FAULT_TYPES = "eventAnalysisFaultTypes";
    public static final String EVENT_ANALYSIS_FAULT_CATEGORIES = "eventAnalysisFaultCategories";
    public static final String EVENT_ANALYSIS_SEARCH_FAULT_META = "eventAnalysisSearchFaultMeta";
    public static final String EVENT_ANALYSIS_SEARCH_UNIT = "eventAnalysisSearchUnit";
    public static final String EVENT_ANALYSIS_SEARCH_VEHICLE = "eventAnalysisSearchVehicle";
    public static final String EVENT_ANALYSIS_SEARCH_LOCATION = "eventAnalysisSearchLocation";
    public static final String EVENT_ANALYSIS_ALL_UNITS = "eventAnalysisAllUnits";
    public static final String EVENT_ANALYSIS_ALL_VEHICLES = "eventAnalysisAllVehicles";
    public static final String EVENT_ANALYSIS_ALL_FAULT_META = "eventAnalysisAllFaultMeta";
    public static final String EVENT_ANALYSIS_ALL_LOCATIONS = "eventAnalysisAllLocations";

    // Properties
    public static final String PROPERTY_EVENT_ANALYSIS_GROUPINGS = "spectrum.eventanalysis.groupings";
    public static final String PROPERTY_EVENT_ANALYSIS_GROUPINGS_CODE = "code";
    public static final String PROPERTY_EVENT_ANALYSIS_GROUPINGS_LABEL = "label";
    public static final String PROPERTY_EVENT_ANALYSIS_GROUPINGS_PARAMS = "params";
    public static final String PROPERTY_EVENT_ANALYSIS_GROUPINGS_CONTENT_SIZE = "contentsize";

    // Grouping references
    public static final String EVENT_ANALYSIS_GROUPING = "grouping";
    public static final String EVENT_ANALYSIS_GROUPING_VEHICLE = "VEHICLE";
    public static final String EVENT_ANALYSIS_GROUPING_UNIT = "UNIT";
    public static final String EVENT_ANALYSIS_GROUPING_FAULTCODE = "FAULTCODE";
    public static final String EVENT_ANALYSIS_GROUPING_LOCATION = "LOCATION";
    public static final String EVENT_ANALYSIS_GROUPING_DATE = "DATE";
    public static final String EVENT_ANALYSIS_GROUPING_HEADCODE = "HEADCODE";
    public static final String EVENT_ANALYSIS_GROUPING_CATEGORY = "CATEGORY";
    public static final String EVENT_ANALYSIS_GROUPING_SET = "SET";

    // Labels
    public static final String LABEL_UNIT = "Unit";
    public static final String LABEL_COUNT = "Count";
    public static final String LABEL_TOTAL_COUNT = "Total Count";
    public static final String LABEL_TOTAL_DURATION = "Total Duration";
    public static final String LABEL_AVERAGE_DURATION = "Average Duration";
    public static final String LABEL_VEHICLE = "Vehicle";
    public static final String LABEL_CODE = "Code";
    public static final String LABEL_DESCRIPTION = "Description";
    public static final String LABEL_CATEGORY = "Category";
    public static final String LABEL_TYPE = "Type";
    public static final String LABEL_LOCATION = "Location";
    public static final String LABEL_CREATE_TIME = "Create Time";
    public static final String LABEL_HEADCODE = "Headcode";
    public static final String LABEL_CURRENT = "Current";
    public static final String LABEL_SET = "Set";

    // Column references
    public static final String COLUMN_UNIT_NUMBER = "unitNumber";
    public static final String COLUMN_COUNT = "Count";
    public static final String COLUMN_TOTAL_COUNT = "totalCount";
    public static final String COLUMN_TOTAL_DURATION = "totalDuration";
    public static final String COLUMN_AVERAGE_DURATION = "averageDuration";
    public static final String COLUMN_VEHICLE_NUMBER = "vehicleNumber";
    public static final String COLUMN_FAULT_CODE = "faultCode";
    public static final String COLUMN_DESCRIPTION = "description";
    public static final String COLUMN_CATEGORY = "category";
    public static final String COLUMN_FAULT_TYPE = "faultType";
    public static final String COLUMN_LOCATION_CODE = "locationCode";
    public static final String COLUMN_CREATE_TIME = "createTime";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_FLEET_ID = "fleetId";
    public static final String COLUMN_FAULT_TYPE_COLOR = "faultTypeColor";
    public static final String COLUMN_FAULT_TYPE_PRIORITY = "faultTypePriority";
    public static final String COLUMN_HEADCODE = "headcode";
    public static final String COLUMN_CURRENT = "current";
    public static final String COLUMN_REFERENCE_KEY = "referenceKey";
    public static final String COLUMN_REFERENCE_VALUE = "referenceValue";
    public static final String COLUMN_FAULT_TYPE_ID = "faultTypeId";
    public static final String COLUMN_SET = "setCode";

    // Filter Ids
    public static final String FILTER_FLEET = "fleetId";
    public static final String FILTER_UNIT = "unitId";
    public static final String FILTER_VEHICLE_TYPE = "vehicleType";
    public static final String FILTER_VEHICLE = "vehicleId";
    public static final String FILTER_FAULT_TYPE = "faultTypeId";
    public static final String FILTER_FAULT_CATEGORY = "faultCategoryId";
    public static final String FILTER_FAULT_META = "faultMetaId";
    public static final String FILTER_FAULT_CODE = "faultCode";
    public static final String FILTER_LOCATION = "locationId";
    public static final String FILTER_HEADCODE = "headcode";
    public static final String FILTER_UNIT_TYPE = "unitType";
    public static final String FILTER_SERVICE_REQUEST = "createdByRule";
    public static final String FILTER_ACKNOWLEDGED = "isAcknowledged";
    public static final String FILTER_HAS_RECOVERY = "hasRecovery";
}