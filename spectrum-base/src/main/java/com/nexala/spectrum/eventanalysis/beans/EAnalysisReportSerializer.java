package com.nexala.spectrum.eventanalysis.beans;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.nexala.spectrum.eventanalysis.EAnalysisConstants;

public class EAnalysisReportSerializer extends JsonSerializer<EAnalysisReport> {

    @Override
    public void serialize(EAnalysisReport report, JsonGenerator jgen, SerializerProvider provider) throws IOException,
            JsonGenerationException {

        jgen.writeStartObject();

        jgen.writeFieldName(EAnalysisConstants.COLUMN_FLEET_ID);
        jgen.writeString(report.getFleetId());

        for (Map.Entry<String, String> descriptor : report.getDescriptors().entrySet()) {
            jgen.writeFieldName(descriptor.getKey());
            jgen.writeString(descriptor.getValue());
        }

        jgen.writeFieldName(EAnalysisConstants.COLUMN_TOTAL_COUNT);
        jgen.writeNumber(report.getTotalCount());

        jgen.writeFieldName(EAnalysisConstants.COLUMN_TOTAL_DURATION);
        jgen.writeString(formatTime(report.getTotalDuration().longValue()));

        jgen.writeFieldName(EAnalysisConstants.COLUMN_AVERAGE_DURATION);
        jgen.writeString(formatTime(report.getAverageDuration().longValue()));

        Map<Integer, Integer> counters = report.getFaultTypeCountMap();

        for (EAnalysisFaultType faultType : report.getFaultTypes()) {
            jgen.writeFieldName(faultType.getName() + EAnalysisConstants.COLUMN_COUNT);
            jgen.writeNumber(counters.get(faultType.getId()));
        }

        jgen.writeEndObject();
    }

    private String formatTime(long timeInMilliseconds) {
        long milliseconds = timeInMilliseconds;

        long hours = TimeUnit.MILLISECONDS.toHours(milliseconds);
        milliseconds = milliseconds - TimeUnit.HOURS.toMillis(hours);
        long minutes = TimeUnit.MILLISECONDS.toMinutes(milliseconds);
        milliseconds = milliseconds - TimeUnit.MINUTES.toMillis(minutes);
        long seconds = TimeUnit.MILLISECONDS.toSeconds(milliseconds);
        milliseconds = milliseconds - TimeUnit.SECONDS.toMillis(seconds);

        return String.format("%02d", hours) + ":" + String.format("%02d", minutes) + ":"
                + String.format("%02d", seconds) + "." + String.format("%03d", milliseconds);
    }
}