package com.nexala.spectrum.eventanalysis.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.google.inject.Inject;
import com.nexala.spectrum.db.JdbcRowMapper;
import com.nexala.spectrum.db.QueryManager;
import com.nexala.spectrum.db.dao.BoxedResultSet;
import com.nexala.spectrum.db.dao.BoxedResultSetFactory;
import com.nexala.spectrum.db.dao.GenericDao;
import com.nexala.spectrum.eventanalysis.EAnalysisConstants;
import com.nexala.spectrum.eventanalysis.beans.EAnalysisLocation;

public class EAnalysisLocationDao extends GenericDao<EAnalysisLocation, Integer> {
    @Inject
    private QueryManager queryManager;

    private static class EAnalysisLocationMapper extends JdbcRowMapper<EAnalysisLocation> {
        @Inject
        private BoxedResultSetFactory factory;

        @Override
        public EAnalysisLocation createRow(ResultSet resultSet) throws SQLException {
            BoxedResultSet rs = factory.create(resultSet);

            return new EAnalysisLocation(
                        rs.getInt("LocationID"),
                        rs.getString("Tiploc"),
                        rs.getString("LocationName"),
                        rs.getString("LocationCode"));
        }
    }

    @Inject
    private EAnalysisLocationMapper mapper;

    public List<EAnalysisLocation> search(String name) {
        String query = queryManager.getQuery(EAnalysisConstants.EVENT_ANALYSIS_SEARCH_LOCATION);
        return findByQuery(query, mapper, name);
    }
    
    public List<EAnalysisLocation> allLocations() {
        String query = queryManager.getQuery(EAnalysisConstants.EVENT_ANALYSIS_ALL_LOCATIONS);
        return findByQuery(query, mapper);
    }
}
