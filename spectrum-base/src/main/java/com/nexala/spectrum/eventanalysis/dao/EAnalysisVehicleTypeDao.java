package com.nexala.spectrum.eventanalysis.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.google.inject.Inject;
import com.nexala.spectrum.db.JdbcRowMapper;
import com.nexala.spectrum.db.QueryManager;
import com.nexala.spectrum.db.dao.GenericDao;
import com.nexala.spectrum.eventanalysis.EAnalysisConstants;
import com.nexala.spectrum.eventanalysis.beans.EAnalysisVehicleType;

public class EAnalysisVehicleTypeDao extends
        GenericDao<EAnalysisVehicleType, String> {
    @Inject
    private QueryManager queryManager;

    private static class EAnalysisVehicleTypeMapper extends
            JdbcRowMapper<EAnalysisVehicleType> {
        @Override
        public final EAnalysisVehicleType createRow(ResultSet resultSet)
                throws SQLException {
            return new EAnalysisVehicleType(resultSet.getString("VehicleType"),
                    resultSet.getString("VehicleType"));
        }
    }

    @Inject
    private EAnalysisVehicleTypeMapper mapper;

    public List<EAnalysisVehicleType> getVehicleTypeList() {
        String query = queryManager
                .getQuery(EAnalysisConstants.EVENT_ANALYSIS_VEHICLE_TYPES);
        return findByStoredProcedure(query, mapper);
    }
}
