package com.nexala.spectrum.eventanalysis.rest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import com.google.inject.Singleton;
import com.nexala.spectrum.eventanalysis.EAnalysisConstants;
import com.nexala.spectrum.eventanalysis.beans.EAnalysisBean;
import com.nexala.spectrum.eventanalysis.beans.EAnalysisFaultType;
import com.nexala.spectrum.eventanalysis.beans.EAnalysisGrouping;
import com.nexala.spectrum.eventanalysis.beans.EAnalysisReport;

@Singleton
public class EAnalysisBeanReporter {
    public Map<String, List<EAnalysisReport>> getReport(List<EAnalysisGrouping> groupings, List<EAnalysisBean> beans,
            List<EAnalysisFaultType> faultTypes, TimeZone tz) {

        Map<String, List<EAnalysisReport>> data = new HashMap<String, List<EAnalysisReport>>();

        for (EAnalysisGrouping grouping : groupings) {
            if (grouping.getCode().equals(EAnalysisConstants.EVENT_ANALYSIS_GROUPING_VEHICLE)) {
                data.put(grouping.getCode(), getReport(EAnalysisReporterProvider.VEHICLE, beans, faultTypes, tz));
            } else if (grouping.getCode().equals(EAnalysisConstants.EVENT_ANALYSIS_GROUPING_UNIT)) {
                data.put(grouping.getCode(), getReport(EAnalysisReporterProvider.UNIT, beans, faultTypes, tz));
            } else if (grouping.getCode().equals(EAnalysisConstants.EVENT_ANALYSIS_GROUPING_FAULTCODE)) {
                data.put(grouping.getCode(), getReport(EAnalysisReporterProvider.FAULT_META, beans, faultTypes, tz));
            } else if (grouping.getCode().equals(EAnalysisConstants.EVENT_ANALYSIS_GROUPING_LOCATION)) {
                data.put(grouping.getCode(), getReport(EAnalysisReporterProvider.LOCATION, beans, faultTypes, tz));
            } else if (grouping.getCode().equals(EAnalysisConstants.EVENT_ANALYSIS_GROUPING_DATE)) {
                data.put(grouping.getCode(), getReport(EAnalysisReporterProvider.DATE, beans, faultTypes, tz));
            } else if (grouping.getCode().equals(EAnalysisConstants.EVENT_ANALYSIS_GROUPING_HEADCODE)) {
                data.put(grouping.getCode(), getReport(EAnalysisReporterProvider.HEADCODE, beans, faultTypes, tz));
            } else if (grouping.getCode().equals(EAnalysisConstants.EVENT_ANALYSIS_GROUPING_CATEGORY)) {
                data.put(grouping.getCode(), getReport(EAnalysisReporterProvider.CATEGORY, beans, faultTypes, tz));
            } else if (grouping.getCode().equals(EAnalysisConstants.EVENT_ANALYSIS_GROUPING_SET)) {
                data.put(grouping.getCode(), getReport(EAnalysisReporterProvider.SET, beans, faultTypes, tz));
            }
        }

        return data;
    }

    private List<EAnalysisReport> getReport(EAnalysisReporter reporter, List<EAnalysisBean> beans,
            List<EAnalysisFaultType> faultTypes, TimeZone tz) {

        Map<String, EAnalysisReport> reports = new HashMap<String, EAnalysisReport>();

        for (EAnalysisBean bean : beans) {
            Map<String, String> descriptors = reporter.getDescriptors(bean, tz);

            String key = descriptors.get(EAnalysisConstants.COLUMN_REFERENCE_KEY);

            if (key == null) { 
                continue;
            }

            if (reports.get(key) == null) {
                reports.put(key, new EAnalysisReport(bean.getFleetId(), descriptors, faultTypes));
            }

            reports.get(key).addBean(bean);
        }

        List<EAnalysisReport> report = new ArrayList<EAnalysisReport>(reports.values());
        Collections.sort(report, reporter.getReportSortComparator()); 

        return report;
    }
}
