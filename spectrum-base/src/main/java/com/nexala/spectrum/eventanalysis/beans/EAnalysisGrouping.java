package com.nexala.spectrum.eventanalysis.beans;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.nexala.spectrum.eventanalysis.EAnalysisConstants;
import com.nexala.spectrum.view.conf.Column;
import com.nexala.spectrum.view.conf.ColumnSorterBean;

public class EAnalysisGrouping {
    private final String code;
    private final String label;
    private final Map<String, String> params;
    private final String contentSize;
    private String rendererObject;
    private boolean countersColumns;
    private ColumnSorterBean defaultSorter;
    private List<Column> columns;
    private Object[] extraFields;

    public EAnalysisGrouping(String code, String label, String params, String contentSize, Object... extraFields) {
        this.code = code;
        this.label = label;
        this.params = getMapParams(params);
        this.contentSize = contentSize;

        this.rendererObject = "nx.ea.ChartRenderer";
        this.countersColumns = true;
        this.defaultSorter = new ColumnSorterBean(EAnalysisConstants.COLUMN_TOTAL_COUNT, true);
        this.extraFields = extraFields;
    }

    public String getCode() {
        return code;
    }

    public String getLabel() {
        return label;
    }

    public Map<String, String> getParams() {
        return params;
    }

    public String getContentSize() {
        return contentSize;
    }

    public void setRendererObject(String rendererObject) {
        this.rendererObject = rendererObject;
    }

    public String getRendererObject() {
        return rendererObject;
    }

    public void setCountersColumns(boolean countersColumns) {
        this.countersColumns = countersColumns;
    }

    public boolean hasCountersColumns() {
        return countersColumns;
    }

    public void setDefaultSorter(ColumnSorterBean defaultSorter) {
        this.defaultSorter = defaultSorter;
    }

    public ColumnSorterBean getDefaultSorter() {
        return defaultSorter;
    }

    public void setColumns(List<Column> columns) {
        this.columns = columns;
    }

    public List<Column> getColumns() {
        return columns;
    }

    public void setExtraFields(Object... extraFields) {
        this.extraFields = extraFields;
    }

    public Object[] getExtraFields() {
        return extraFields;
    }

    private Map<String, String> getMapParams(String params) {
        Map<String, String> mapParams = new HashMap<String, String>();

        if (params != null) {
            String[] paramsArray = params.split(",");
            for (String param : paramsArray) {
                String[] keyValue = param.split("=");
                if (keyValue.length == 2) {
                    mapParams.put(keyValue[0].trim(), keyValue[1].trim());
                }
            }
        }

        return mapParams;
    }
}
