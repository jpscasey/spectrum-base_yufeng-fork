package com.nexala.spectrum.eventanalysis.rest;

import java.util.Date;
import java.util.List;
import java.util.TreeMap;

import com.google.inject.ImplementedBy;
import com.nexala.spectrum.eventanalysis.beans.EAnalysisBean;
import com.nexala.spectrum.eventanalysis.beans.EAnalysisFaultCategory;
import com.nexala.spectrum.eventanalysis.beans.EAnalysisFaultMeta;
import com.nexala.spectrum.eventanalysis.beans.EAnalysisFaultType;
import com.nexala.spectrum.eventanalysis.beans.EAnalysisFilterOption;
import com.nexala.spectrum.eventanalysis.beans.EAnalysisLocation;
import com.nexala.spectrum.eventanalysis.beans.EAnalysisUnit;
import com.nexala.spectrum.eventanalysis.beans.EAnalysisVehicle;
import com.nexala.spectrum.eventanalysis.beans.EAnalysisVehicleType;

@ImplementedBy(DefaultEAnalysisProvider.class)
public interface EAnalysisProvider {
    List<EAnalysisFilterOption> getFilterOptions(String filterName);

    List<EAnalysisVehicleType> getVehicleTypeList();

    List<EAnalysisFaultType> getFaultTypeList();

    List<EAnalysisFaultCategory> getFaultCategoryList();

    List<EAnalysisFaultMeta> searchFaultMeta(String keyword,
            TreeMap<String, String> searchParameters);

    List<EAnalysisUnit> searchUnit(String unitNumber);

    List<EAnalysisVehicle> searchVehicle(String vehicleType,
            String vehicleNumber, String unitIds);

    List<EAnalysisLocation> searchLocation(String locationName);

    List<EAnalysisBean> searchData(String fleetId, Date startDate,
            Date endDate, TreeMap<String, String> searchParameters);

    List<EAnalysisBean> searchDetailData(String fleetId, Date startDate,
            Date endDate, String grouping, String referenceKey,
            TreeMap<String, String> searchParameters);
}
