package com.nexala.spectrum.eventanalysis.beans;

public class EAnalysisUnit {
    private final String fleetCode;
    private final int id;
    private final String number;

    public EAnalysisUnit(String fleetCode, int id, String number) {
        this.fleetCode = fleetCode;
        this.id = id;
        this.number = number;
    }

    public String getFleetCode() {
        return fleetCode;
    }

    public int getId() {
        return id;
    }

    public String getNumber() {
        return number;
    }
}
