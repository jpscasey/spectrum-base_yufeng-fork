package com.nexala.spectrum.eventanalysis.rest;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;
import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.eventanalysis.EAnalysisConstants;
import com.nexala.spectrum.eventanalysis.beans.EAnalysisBean;
import com.nexala.spectrum.eventanalysis.beans.EAnalysisFaultCategory;
import com.nexala.spectrum.eventanalysis.beans.EAnalysisFaultMeta;
import com.nexala.spectrum.eventanalysis.beans.EAnalysisFaultType;
import com.nexala.spectrum.eventanalysis.beans.EAnalysisFilterOption;
import com.nexala.spectrum.eventanalysis.beans.EAnalysisGrouping;
import com.nexala.spectrum.eventanalysis.beans.EAnalysisLocation;
import com.nexala.spectrum.eventanalysis.beans.EAnalysisReport;
import com.nexala.spectrum.eventanalysis.beans.EAnalysisUnit;
import com.nexala.spectrum.eventanalysis.beans.EAnalysisVehicle;
import com.nexala.spectrum.eventanalysis.beans.EAnalysisVehicleType;
import com.nexala.spectrum.eventanalysis.conf.EAnalysisConfiguration;
import com.nexala.spectrum.i18n.ResourceBundleUTF8;
import com.nexala.spectrum.licensing.Licence;
import com.nexala.spectrum.licensing.LoggingData;
import com.nexala.spectrum.rest.cache.Cache;
import com.nexala.spectrum.rest.data.FleetProvider;
import com.nexala.spectrum.utils.DateUtils;
import com.nexala.spectrum.view.conf.AppConfiguration;
import com.nexala.spectrum.view.conf.Column;
import com.nexala.spectrum.view.conf.ColumnSorterBean;
import com.nexala.spectrum.view.conf.ExportConfiguration;
import com.nexala.spectrum.view.conf.Field;
import com.nexala.spectrum.view.conf.FieldType;

import au.com.bytecode.opencsv.CSVWriter;

@Path("/eventanalysis")
@Singleton
public class EAnalysisService {
	@Inject
	private AppConfiguration appConf;
	
    @Inject
    private Map<String, EAnalysisProvider> eaProviders;

    @Inject
    private EAnalysisConfiguration eaConfiguration;

    @Inject
    private EAnalysisBeanReporter eaReporter;

    @Inject
    private Map<String, FleetProvider> fleetProviders;

    @Inject
    private Provider<HttpServletRequest> requestProvider;
    
    @Inject
    private Provider<HttpServletResponse> responseProvider;


    @GET
    @Path("/groupings")
    @Produces(MediaType.APPLICATION_JSON)
    public List<EAnalysisGrouping> getGroupings() {
        TreeMap<String, EAnalysisFaultType> faultTypes = new TreeMap<String, EAnalysisFaultType>();
        Licence licence = Licence.get(requestProvider.get(), responseProvider.get(), true);

        for (Map.Entry<String, EAnalysisProvider> entry : eaProviders
                .entrySet()) {
            for (EAnalysisFaultType faultType : eaProviders.get(entry.getKey())
                    .getFaultTypeList()) {
                faultTypes.put(faultType.getName(), faultType);
            }
        }

        return eaConfiguration.getGroupings(new ArrayList<EAnalysisFaultType>(
                faultTypes.values()), licence);
    }

    @GET
    @Cache
    @LoggingData
    @Path("/searchFields")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Field> searchFields() {
        return eaConfiguration.getSearchFields();
    }

    @GET
    @Path("/detailColumns")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Column> getDetailColumns() {
        return eaConfiguration.getDetailColumns();
    }

    @GET
    @Path("/detailDefaultSorter")
    @Produces(MediaType.APPLICATION_JSON)
    public ColumnSorterBean getDetailDefaultSorter() {
        return eaConfiguration.getDetailDefaultSorter();
    }
    
    @GET
    @Path("/filtersPerRow")
    @Produces(MediaType.APPLICATION_JSON)
    public Integer getFiltersPerRow() {
        return eaConfiguration.getFiltersPerRow();
    }

    @GET
    @Path("/filterOptions")
    @Produces(MediaType.APPLICATION_JSON)
    public Map<String, Map<String, List<EAnalysisFilterOption>>> getFilterOptions() {
        Map<String, Map<String, List<EAnalysisFilterOption>>> filterOptions = new HashMap<String, Map<String, List<EAnalysisFilterOption>>>();

        List<Field> filters = searchFields();
        Licence licence = Licence.get(requestProvider.get(), responseProvider.get(), true);

        for (Field f : filters) {
            if (FieldType.SELECT.toString().toLowerCase().equals(f.getType())
                    || FieldType.MULTIPLE_SELECT.toString().toLowerCase().equals(f.getType())) {
                Map<String, List<EAnalysisFilterOption>> mappedByFilter = new HashMap<String, List<EAnalysisFilterOption>>();

                for (String fleetId : eaProviders.keySet()) {
                    String fleetLicence = fleetProviders.get(fleetId).getLicence(fleetId);
                    if (fleetLicence == null || licence.hasResourceAccess(fleetLicence)) {
                        mappedByFilter.put(fleetId, eaProviders.get(fleetId).getFilterOptions(f.getName()));
                    }
                }

                filterOptions.put(f.getName(), mappedByFilter);
            }
        }

        return filterOptions;
    }

    // FIXME Is this used?
    @GET
    @Path("/vehicleTypes")
    @Produces(MediaType.APPLICATION_JSON)
    public Map<String, List<EAnalysisVehicleType>> getVehicleTypes() {
        Map<String, List<EAnalysisVehicleType>> vehicleTypes = new HashMap<String, List<EAnalysisVehicleType>>();

        for (Map.Entry<String, EAnalysisProvider> entry : eaProviders
                .entrySet()) {
            String key = entry.getKey();
            vehicleTypes.put(key, eaProviders.get(key).getVehicleTypeList());
        }

        return vehicleTypes;
    }

    // FIXME Is this used?
    @GET
    @Path("/faultTypes")
    @Produces(MediaType.APPLICATION_JSON)
    public List<EAnalysisFaultType> getFaultTypes() {
        List<EAnalysisFaultType> faultTypes = new ArrayList<EAnalysisFaultType>();

        // Get the first provider as the fault type are not fleet dependent
        Iterator<EAnalysisProvider> it = eaProviders.values().iterator();

        if (it.hasNext()) {
            faultTypes.addAll(it.next().getFaultTypeList());
        }

        return faultTypes;
    }

    // FIXME Is this used?
    @GET
    @Path("/faultCategories")
    @Produces(MediaType.APPLICATION_JSON)
    public List<EAnalysisFaultCategory> getFaultCategories() {
        List<EAnalysisFaultCategory> faultCategories = new ArrayList<EAnalysisFaultCategory>();

        // Get the first provider as the fault type are not fleet dependent
        Iterator<EAnalysisProvider> it = eaProviders.values().iterator();

        if (it.hasNext()) {
            faultCategories.addAll(it.next().getFaultCategoryList());
        }

        return faultCategories;
    }

    @GET
    @Path("/searchUnit/{fleetId}/{unitNumber}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<EAnalysisUnit> searchUnit(@PathParam("fleetId") String fleetId,
            @PathParam("unitNumber") String unitNumber) {
        List<EAnalysisUnit> result = new ArrayList<EAnalysisUnit>();
        Licence licence = Licence.get(requestProvider.get(), responseProvider.get(), true);

        // Get the providers
        Map<String, EAnalysisProvider> providers = getProviders(fleetId, licence);

        // Iterate over the relevant providers
        for (EAnalysisProvider current : providers.values()) {
            result.addAll(current.searchUnit(unitNumber));
        }

        return result;
    }

    @POST
    @Path("/searchVehicle")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public List<EAnalysisVehicle> searchVehicle(Map<String, String> params) {
        List<EAnalysisVehicle> result = new ArrayList<EAnalysisVehicle>();
        Licence licence = Licence.get(requestProvider.get(), responseProvider.get(), true);

        String unitId = params.get("unitId");
        String vehicleType = params.get("vehicleType");
        String vehicleNumber = params.get("vehicleNumber");
        String fleetId = params.get("fleetId");
        String unitList[] = {};
        if (unitId != null) {
            unitList = unitId.split(",");
        }

        // Get the providers
        Map<String, EAnalysisProvider> providers = getProviders(fleetId, licence);

        // Iterate over the relevant providers
        for (String fleet : providers.keySet()) {
            String units = "";
            boolean firstUnit = true;
            for (String unit : unitList) {
                if (unit.split("-")[0].equals(fleet)) {
                    if (!firstUnit) {
                        units += ",";
                    }
                    units += unit.split("-")[1];
                    firstUnit = false;
                }
            }
            result.addAll(providers.get(fleet).searchVehicle(vehicleType, vehicleNumber,
                    units.isEmpty() ? null : units));
        }

        return result;
    }

    @POST
    @Path("/searchFaultMeta")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public List<EAnalysisFaultMeta> searchFaultMeta(
            TreeMap<String, String> params) {
        List<EAnalysisFaultMeta> result = new ArrayList<EAnalysisFaultMeta>();
        Licence licence = Licence.get(requestProvider.get(), responseProvider.get(), true);
        String fleetId = params.remove("fleetId");
        String keyword = params.remove("faultMetaKeyword");

        // Get the providers
        Map<String, EAnalysisProvider> providers = getProviders(fleetId, licence);

        // Iterate over the relevant providers
        for (EAnalysisProvider current : providers.values()) {
            result.addAll(current.searchFaultMeta(keyword, params));
        }

        return result;
    }

    @GET
    @Path("/searchLocation/{fleetId}/{location}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<EAnalysisLocation> searchLocation(
            @PathParam("fleetId") String fleetId,
            @PathParam("location") String location) {
        List<EAnalysisLocation> result = new ArrayList<EAnalysisLocation>();
        Licence licence = Licence.get(requestProvider.get(), responseProvider.get(), true);

        // Get the providers
        Map<String, EAnalysisProvider> providers = getProviders(fleetId, licence);

        // Iterate over the relevant providers
        List<String> locationCodes = new ArrayList<String>();
        for (EAnalysisProvider current : providers.values()) {
            List<EAnalysisLocation> locations = current.searchLocation(location);
            for (EAnalysisLocation currentLoc : locations) {
                if (!locationCodes.contains(currentLoc.getCode())) {
                    locationCodes.add(currentLoc.getCode());
                    result.add(currentLoc);
                }
            }
        }

        return result;
    }

    @POST
    @LoggingData
    @Path("/searchData")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Map<String, List<EAnalysisReport>> searchData(TreeMap<String, String> searchParameters) {
        String fleetId = searchParameters.remove("fleetId");
        Date startDate = new Date(Long.parseLong(searchParameters.remove("dateFrom")));
        Date endDate = new Date(Long.parseLong(searchParameters.remove("dateTo")));
        
        TimeZone timezone = DateUtils.getClientTimeZone(searchParameters.remove("timezone"));
        
        List<EAnalysisBean> beans = new ArrayList<EAnalysisBean>();
        List<EAnalysisFaultType> faultTypeList = new ArrayList<EAnalysisFaultType>();
        Licence licence = Licence.get(requestProvider.get(), responseProvider.get(), true);

        String units = searchParameters.remove(EAnalysisConstants.FILTER_UNIT);
        List<String> unitList = new ArrayList<String>();
        if (units != null && !units.isEmpty()) {
            unitList = Arrays.asList(units.split(","));
        }
        String vehicles = searchParameters.remove(EAnalysisConstants.FILTER_VEHICLE);
        List<String> vehicleList = new ArrayList<String>();
        if (vehicles != null && !vehicles.isEmpty()) {
            vehicleList = Arrays.asList(vehicles.split(","));
        }

        // Get the providers
        Map<String, EAnalysisProvider> providers = getProviders(fleetId, licence);

        // Iterate over it
        for (String fleet : providers.keySet()) {
            TreeMap<String, String> fleetParameters = searchParameters;
            
            beans.addAll(providers.get(fleet).searchData(fleet, startDate, endDate,
                    filterStocks(fleetParameters, unitList, vehicleList, fleet)));
            faultTypeList.addAll(providers.get(fleet).getFaultTypeList());
        }

        return eaReporter.getReport(eaConfiguration.getGroupings(), beans,
                faultTypeList, timezone);
    }

    @POST
    @LoggingData
    @Path("/searchDetailData")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public List<EAnalysisBean> searchDetailData(TreeMap<String, String> searchParameters)
            throws JsonParseException, JsonMappingException, IOException {
        String fleetId = searchParameters.remove("fleetId");
        Date startDate = new Date(Long.parseLong(searchParameters.remove("dateFrom")));
        Date endDate = new Date(Long.parseLong(searchParameters.remove("dateTo")));
        String grouping = searchParameters.remove(EAnalysisConstants.EVENT_ANALYSIS_GROUPING);
        String referenceKey = searchParameters.remove(EAnalysisConstants.COLUMN_REFERENCE_KEY);
        searchParameters.remove("timezone");
        
        List<EAnalysisBean> beans = new ArrayList<EAnalysisBean>();
        Licence licence = Licence.get(requestProvider.get(), responseProvider.get(), true);

        String units = searchParameters.remove(EAnalysisConstants.FILTER_UNIT);
        List<String> unitList = new ArrayList<String>();
        if (units != null && !units.isEmpty()) {
            unitList = Arrays.asList(units.split(","));
        }
        String vehicles = searchParameters.remove(EAnalysisConstants.FILTER_VEHICLE);
        List<String> vehicleList = new ArrayList<String>();
        if (vehicles != null && !vehicles.isEmpty()) {
            vehicleList = Arrays.asList(vehicles.split(","));
        }
        
        // Get the providers
        Map<String, EAnalysisProvider> providers = getProviders(fleetId, licence);

        // Iterate over the relevant providers
        for (String fleet : providers.keySet()) {
            TreeMap<String, String> fleetParameters = searchParameters;

            beans.addAll(providers.get(fleet).searchDetailData(fleet,
                    startDate, endDate, grouping, referenceKey,
                    filterStocks(fleetParameters, unitList, vehicleList, fleet)));
        }

        return beans;
    }

    @POST
    @LoggingData
    @Path("/download")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces("text/csv")
    public Response download(@Context HttpServletResponse response, @FormParam("data") String data,
            @FormParam("timezone") String timezone) throws IOException {

        TreeMap<String, String> searchParameters = new ObjectMapper().readValue(data,
                TypeFactory.defaultInstance().constructMapType(TreeMap.class, String.class, String.class));

        // FIXME doesn't handle summerTime use the jstz.js instead
        TimeZone tz = DateUtils.getClientTimeZone(timezone);

        List<EAnalysisBean> beans = new ArrayList<EAnalysisBean>();
        List<EAnalysisFaultType> faultTypeList = new ArrayList<EAnalysisFaultType>();
        Licence licence = Licence.get(requestProvider.get(), responseProvider.get(), true);
        
        String fleetId = searchParameters.remove("fleetId");
        Date startDate = new Date(Long.parseLong(searchParameters.remove("dateFrom")));
        Date endDate = new Date(Long.parseLong(searchParameters.remove("dateTo")));        
        String groupingCode = searchParameters.remove(EAnalysisConstants.EVENT_ANALYSIS_GROUPING);
        
        List<EAnalysisGrouping> groupings = new ArrayList<EAnalysisGrouping>();
        for (EAnalysisGrouping grouping : getGroupings()) {
            if (grouping.getCode().equals(groupingCode)) {
                groupings.add(grouping);
                break;
            }
        }
        
        String units = searchParameters.remove(EAnalysisConstants.FILTER_UNIT);
        List<String> unitList = new ArrayList<String>();
        if (units != null && !units.isEmpty()) {
            unitList = Arrays.asList(units.split(","));
        }
        String vehicles = searchParameters.remove(EAnalysisConstants.FILTER_VEHICLE);
        List<String> vehicleList = new ArrayList<String>();
        if (vehicles != null && !vehicles.isEmpty()) {
            vehicleList = Arrays.asList(vehicles.split(","));
        }
        
        // Get the providers
        Map<String, EAnalysisProvider> providers = getProviders(fleetId, licence);

        // Iterate over it
        for (String fleet : providers.keySet()) {
            TreeMap<String, String> fleetParameters = searchParameters;
            
            beans.addAll(providers.get(fleet).searchData(fleet, startDate, endDate,
                    filterStocks(fleetParameters, unitList, vehicleList, fleet)));
            faultTypeList.addAll(providers.get(fleet).getFaultTypeList());
        }
        
        createCsv(groupingCode, eaReporter.getReport(groupings, beans, faultTypeList, tz).get(groupingCode),
                groupings.get(0), tz, response, licence);
        return Response.ok().build();
    }
    
    private TreeMap<String, String> filterStocks (TreeMap<String, String> parameters, List<String> units,
            List<String> vehicles, String fleetId) {
        
        String unitIds = "";
        if (units.size() > 0) {
            boolean firstUnit = true;
            for (String unit : units) {
                String unitFleet = unit.split("-")[0];
                if (!firstUnit) {
                    unitIds += ",";
                }
                if (unitFleet.equals(fleetId) || unitFleet.equals(Spectrum.ALL_CODE)) {
                    unitIds += unit.split("-")[1];
                } else {
                    unitIds += "0";
                }
                firstUnit = false;
            }
        }
        parameters.put(EAnalysisConstants.FILTER_UNIT, unitIds);
        
        String vehicleIds = "";
        if (vehicles.size() > 0) {
            boolean firstVehicle = true;
            for (String vehicle : vehicles) {
                String vehicleFleet = vehicle.split("-")[0];
                if (!firstVehicle) {
                    vehicleIds += ",";
                }
                if (vehicleFleet.equals(fleetId) || vehicleFleet.equals(Spectrum.ALL_CODE)) {
                    vehicleIds += vehicle.split("-")[1];
                } else {
                    vehicleIds += "0";
                }
                firstVehicle = false;
            }
        }
        parameters.put(EAnalysisConstants.FILTER_VEHICLE, vehicleIds);
        
        return parameters;
    }

    private void createCsv(String groupingCode, List<EAnalysisReport> data, EAnalysisGrouping grouping,
            TimeZone timezone, HttpServletResponse response, Licence licence)
            throws IOException {
    	ExportConfiguration exportConf = appConf.getExportConfig();
        OutputStream outputStream = response.getOutputStream();
        ResourceBundleUTF8 bundle = new ResourceBundleUTF8(licence.getUserLocale());
        String fileName = bundle.getString(Spectrum.EVENT_ANALYSIS_FILE_NAME);
        String groupingCodeI18n = bundle.getString(groupingCode.toLowerCase());
        response.setContentType("text/" + exportConf.getFileExtension());
        response.addHeader("Content-disposition", "attachment; filename=" + fileName + "-" 
                + groupingCodeI18n + "." + exportConf.getFileExtension());

        PrintWriter writer = new PrintWriter(outputStream);
        CSVWriter csvWriter = new CSVWriter(writer, exportConf.getSeparator());
        
        List<String> headers = new ArrayList<String>();
        for (Column col : grouping.getColumns()) {
            if (col.isVisible()) {
                headers.add(col.getHeader());
            }
        }

        csvWriter.writeNext(headers.toArray(new String[headers.size()]));

        for (Iterator<EAnalysisReport> it = data.iterator(); it.hasNext();) {
            EAnalysisReport row = it.next();
            csvWriter.writeNext(eaConfiguration.getExportRow(groupingCode, row, grouping.hasCountersColumns(),
                    timezone));
        }

        csvWriter.close();
        writer.close();
        outputStream.close();
    }

    /**
     * Return the or the multiples providers for the fault code in parameter. If
     * the fault code is {@link Spectrum#ALL_CODE} then return all providers
     * 
     * @param fleetCode
     *            the fleet code or {@link Spectrum#ALL_CODE}
     * @return the list of providers
     */
    private Map<String, EAnalysisProvider> getProviders(String fleetCode, Licence licence) {
        Map<String, EAnalysisProvider> result = new HashMap<String, EAnalysisProvider>();
        if (Spectrum.ALL_CODE.equals(fleetCode)) {          
            for (String fleetId : eaProviders.keySet()) {
                String fleetLicence = fleetProviders.get(fleetId).getLicence(fleetId);
                if (fleetLicence == null || licence.hasResourceAccess(fleetLicence)) {
                    result.put(fleetId, eaProviders.get(fleetId));
                }
            }                
        } else {
            result.put(fleetCode, eaProviders.get(fleetCode));
        }

        return result;
    }
}
