package com.nexala.spectrum.eventanalysis.beans;

public class EAnalysisBean {
    private final String fleetId;
    private final int id;
    private final long createTime;
    private final boolean current;
    private final Long endTime;
    private final String headcode;
    private final String setCode;
    private final Integer unitId;
    private final String unitNumber;
    private final Integer vehicleId;
    private final String vehicleNumber;
    private final int faultMetaId;
    private final String faultCode;
    private final String description;
    private final int faultTypeId;
    private final String faultType;
    private final String faultTypeColor;
    private final Integer faultTypePriority;
    private final int categoryId;
    private final String category;
    private final Double latitude;
    private final Double longitude;
    private final Integer locationId;
    private final String locationCode;

    public EAnalysisBean(EAnalysisBeanBuilder builder) {
        this.fleetId = builder.getFleetId();
        this.id = builder.getId(); 
        this.createTime = builder.getCreateTime();
        this.current = builder.isCurrent();
        this.endTime = builder.getEndTime();
        this.headcode = builder.getHeadcode();
        this.setCode = builder.getSetCode();
        this.unitId = builder.getUnitId();
        this.unitNumber = builder.getUnitNumber();
        this.vehicleId = builder.getVehicleId();
        this.vehicleNumber = builder.getVehicleNumber();
        this.faultMetaId = builder.getFaultMetaId();
        this.faultCode = builder.getFaultCode();
        this.description = builder.getDescription();
        this.faultTypeId = builder.getFaultTypeId();
        this.faultType = builder.getFaultType();
        this.faultTypeColor = builder.getFaultTypeColor();
        this.faultTypePriority = builder.getFaultTypePriority();
        this.categoryId = builder.getCategoryId();
        this.category = builder.getCategory();
        this.latitude = builder.getLatitude();
        this.longitude = builder.getLongitude();
        this.locationId = builder.getLocationId();
        this.locationCode = builder.getLocationCode();
    }

    public String getFleetId() {
        return fleetId;
    }

    public int getId() {
        return id;
    }

    public long getCreateTime() {
        return createTime;
    }

    public boolean isCurrent() {
        return current;
    }

    public Long getEndTime() {
        return endTime;
    }

    public String getHeadcode() {
        return headcode;
    }

    public String getSetCode() {
        return setCode;
    }

    public Integer getUnitId() {
        return unitId;
    }

    public String getUnitNumber() {
        return unitNumber;
    }

    public Integer getVehicleId() {
        return vehicleId;
    }

    public String getVehicleNumber() {
        return vehicleNumber;
    }

    public int getFaultMetaId() {
        return faultMetaId;
    }

    public String getFaultCode() {
        return faultCode;
    }

    public String getDescription() {
        return description;
    }

    public int getFaultTypeId() {
        return faultTypeId;
    }

    public String getFaultType() {
        return faultType;
    }

    public String getFaultTypeColor() {
        return faultTypeColor;
    }

    public Integer getFaultTypePriority() {
        return faultTypePriority;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public String getCategory() {
        return category;
    }

    public Double getLatitude() {
        return latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public Integer getLocationId() {
        return locationId;
    }

    public String getLocationCode() {
        return locationCode;
    }
}
