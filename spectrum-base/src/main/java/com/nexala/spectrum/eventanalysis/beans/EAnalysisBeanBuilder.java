package com.nexala.spectrum.eventanalysis.beans;

public class EAnalysisBeanBuilder {
    private String fleetid;
    private int id;
    private long createTime;
    private boolean current;
    private Long endTime = null;
    private String headcode = null;
    private String setCode = null;
    private Integer unitId = null;
    private String unitNumber = null;
    private Integer vehicleId = null;
    private String vehicleNumber = null;
    private int faultMetaId;
    private String faultCode = null;
    private String description = null;
    private int faultTypeId;
    private String faultType = null;
    private String faultTypeColor = null;
    private Integer faultTypePriority;
    private int categoryId;
    private String category = null;
    private Double latitude = null;
    private Double longitude = null;
    private Integer locationId = null;
    private String locationCode = null;

    public EAnalysisBean build() {
        return new EAnalysisBean(this);
    }

    public String getFleetId() {
        return fleetid;
    }

    public EAnalysisBeanBuilder setFleetId(String fleetId) {
        this.fleetid = fleetId;
        return this;
    }

    public int getId() {
        return id;
    }

    public EAnalysisBeanBuilder setId(int id) {
        this.id = id;
        return this;
    }

    public long getCreateTime() {
        return createTime;
    }

    public EAnalysisBeanBuilder setCreateTime(long createTime) {
        this.createTime = createTime;
        return this;
    }

    public boolean isCurrent() {
        return current;
    }

    public EAnalysisBeanBuilder setCurrent(boolean current) {
        this.current = current;
        return this;
    }

    public Long getEndTime() {
        return endTime;
    }

    public EAnalysisBeanBuilder setEndTime(Long endTime) {
        this.endTime = endTime;
        return this;
    }

    public String getHeadcode() {
        return headcode;
    }

    public EAnalysisBeanBuilder setHeadcode(String headcode) {
        this.headcode = headcode;
        return this;
    }

    public String getSetCode() {
        return setCode;
    }

    public EAnalysisBeanBuilder setSetCode(String setCode) {
        this.setCode = setCode;
        return this;
    }

    public Integer getUnitId() {
        return unitId;
    }

    public EAnalysisBeanBuilder setUnitId(Integer unitId) {
        this.unitId = unitId;
        return this;
    }

    public String getUnitNumber() {
        return unitNumber;
    }

    public EAnalysisBeanBuilder setUnitNumber(String unitNumber) {
        this.unitNumber = unitNumber;
        return this;
    }

    public Integer getVehicleId() {
        return vehicleId;
    }

    public EAnalysisBeanBuilder setVehicleId(Integer vehicleId) {
        this.vehicleId = vehicleId;
        return this;
    }

    public String getVehicleNumber() {
        return vehicleNumber;
    }

    public EAnalysisBeanBuilder setVehicleNumber(String vehicleNumber) {
        this.vehicleNumber = vehicleNumber;
        return this;
    }

    public int getFaultMetaId() {
        return faultMetaId;
    }

    public EAnalysisBeanBuilder setFaultMetaId(int faultMetaId) {
        this.faultMetaId = faultMetaId;
        return this;
    }

    public String getFaultCode() {
        return faultCode;
    }

    public EAnalysisBeanBuilder setFaultCode(String faultCode) {
        this.faultCode = faultCode;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public EAnalysisBeanBuilder setDescription(String description) {
        this.description = description;
        return this;
    }

    public int getFaultTypeId() {
        return faultTypeId;
    }

    public EAnalysisBeanBuilder setFaultTypeId(int faultTypeId) {
        this.faultTypeId = faultTypeId;
        return this;
    }

    public String getFaultType() {
        return faultType;
    }

    public EAnalysisBeanBuilder setFaultType(String faultType) {
        this.faultType = faultType;
        return this;
    }

    public String getFaultTypeColor() {
        return faultTypeColor;
    }

    public EAnalysisBeanBuilder setFaultTypeColor(String faultTypeColor) {
        this.faultTypeColor = faultTypeColor;
        return this;
    }

    public Integer getFaultTypePriority() {
        return faultTypePriority;
    }

    public EAnalysisBeanBuilder setFaultTypePriority(Integer faultTypePriority) {
        this.faultTypePriority = faultTypePriority;
        return this;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public EAnalysisBeanBuilder setCategoryId(int categoryId) {
        this.categoryId = categoryId;
        return this;
    }

    public String getCategory() {
        return category;
    }

    public EAnalysisBeanBuilder setCategory(String category) {
        this.category = category;
        return this;
    }

    public Double getLatitude() {
        return latitude;
    }

    public EAnalysisBeanBuilder setLatitude(Double latitude) {
        this.latitude = latitude;
        return this;
    }

    public Double getLongitude() {
        return longitude;
    }

    public EAnalysisBeanBuilder setLongitude(Double longitude) {
        this.longitude = longitude;
        return this;
    }

    public Integer getLocationId() {
        return locationId;
    }

    public EAnalysisBeanBuilder setLocationId(Integer locationId) {
        this.locationId = locationId;
        return this;
    }

    public String getLocationCode() {
        return locationCode;
    }

    public EAnalysisBeanBuilder setLocationCode(String locationCode) {
        this.locationCode = locationCode;
        return this;
    }
}
