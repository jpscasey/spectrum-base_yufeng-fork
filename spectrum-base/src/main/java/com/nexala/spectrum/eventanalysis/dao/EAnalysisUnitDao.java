package com.nexala.spectrum.eventanalysis.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.google.inject.Inject;
import com.nexala.spectrum.db.JdbcRowMapper;
import com.nexala.spectrum.db.QueryManager;
import com.nexala.spectrum.db.dao.BoxedResultSet;
import com.nexala.spectrum.db.dao.BoxedResultSetFactory;
import com.nexala.spectrum.db.dao.GenericDao;
import com.nexala.spectrum.eventanalysis.EAnalysisConstants;
import com.nexala.spectrum.eventanalysis.beans.EAnalysisUnit;

public class EAnalysisUnitDao extends GenericDao<EAnalysisUnit, Integer> {
    @Inject
    private QueryManager queryManager;

    private static class EAnalysisUnitMapper extends JdbcRowMapper<EAnalysisUnit> {
        @Inject
        private BoxedResultSetFactory factory;

        @Override
        public EAnalysisUnit createRow(ResultSet resultSet) throws SQLException {
            BoxedResultSet rs = factory.create(resultSet);
            return new EAnalysisUnit(rs.getString("FleetCode"),
                    rs.getInt("UnitID"), rs.getString("UnitNumber"));
        }
    }

    @Inject
    private EAnalysisUnitMapper mapper;

    public List<EAnalysisUnit> search(String number) {
        number = number.replaceAll("\\.\\*", "%");
        number = number.replaceAll("\\.", "_");
        String query = queryManager.getQuery(EAnalysisConstants.EVENT_ANALYSIS_SEARCH_UNIT);
        return findByStoredProcedure(query, mapper, number);
    }

    public List<EAnalysisUnit> allUnits() {
        String query = queryManager.getQuery(EAnalysisConstants.EVENT_ANALYSIS_ALL_UNITS);
        return findByQuery(query, mapper);
    }
}
