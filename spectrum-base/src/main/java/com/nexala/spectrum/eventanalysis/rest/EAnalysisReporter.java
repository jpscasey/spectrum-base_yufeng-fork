package com.nexala.spectrum.eventanalysis.rest;

import java.util.Comparator;
import java.util.Map;
import java.util.TimeZone;

import com.nexala.spectrum.eventanalysis.beans.EAnalysisBean;
import com.nexala.spectrum.eventanalysis.beans.EAnalysisReport;

public interface EAnalysisReporter {
    Map<String, String> getDescriptors(EAnalysisBean bean, TimeZone tz);

    Comparator<EAnalysisReport> getReportSortComparator();
}
