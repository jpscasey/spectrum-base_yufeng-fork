package com.nexala.spectrum.eventanalysis.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.google.inject.Inject;
import com.nexala.spectrum.db.JdbcRowMapper;
import com.nexala.spectrum.db.QueryManager;
import com.nexala.spectrum.db.dao.BoxedResultSet;
import com.nexala.spectrum.db.dao.BoxedResultSetFactory;
import com.nexala.spectrum.db.dao.GenericDao;
import com.nexala.spectrum.eventanalysis.EAnalysisConstants;
import com.nexala.spectrum.eventanalysis.beans.EAnalysisVehicle;

public class EAnalysisVehicleDao extends GenericDao<EAnalysisVehicle, Integer> {
    @Inject
    private QueryManager queryManager;

    private static class EAnalysisVehicleMapper extends JdbcRowMapper<EAnalysisVehicle> {
        @Inject
        private BoxedResultSetFactory factory;

        @Override
        public final EAnalysisVehicle createRow(ResultSet resultSet)
                throws SQLException {
            BoxedResultSet rs = factory.create(resultSet);
            return new EAnalysisVehicle(rs.getString("FleetCode"),
                    rs.getInt("VehicleID"), rs.getString("VehicleNumber"),
                    rs.getString("VehicleType"));
        }
    }

    @Inject
    private EAnalysisVehicleMapper mapper;

    public List<EAnalysisVehicle> search(String vehicleType, String vehicleNumber, String unitIds) {
        String query = queryManager.getQuery(EAnalysisConstants.EVENT_ANALYSIS_SEARCH_VEHICLE);
        return findByStoredProcedure(query, mapper, vehicleType, vehicleNumber, unitIds);
    }

    public List<EAnalysisVehicle> allVehicles() {
        String query = queryManager.getQuery(EAnalysisConstants.EVENT_ANALYSIS_ALL_VEHICLES);
        return findByQuery(query, mapper);
    }
}
