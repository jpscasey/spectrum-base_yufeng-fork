package com.nexala.spectrum.eventanalysis.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.google.inject.Inject;
import com.nexala.spectrum.db.JdbcRowMapper;
import com.nexala.spectrum.db.QueryManager;
import com.nexala.spectrum.db.dao.BoxedResultSet;
import com.nexala.spectrum.db.dao.BoxedResultSetFactory;
import com.nexala.spectrum.db.dao.GenericDao;
import com.nexala.spectrum.eventanalysis.EAnalysisConstants;
import com.nexala.spectrum.eventanalysis.beans.EAnalysisFaultCategory;

public class EAnalysisFaultCategoryDao extends GenericDao<EAnalysisFaultCategory, String> {
    @Inject
    private QueryManager queryManager;

    private static class EAnalysisFaultCategoryMapper extends JdbcRowMapper<EAnalysisFaultCategory> {
        @Inject
        private BoxedResultSetFactory factory;

        @Override
        public EAnalysisFaultCategory createRow(ResultSet resultSet) throws SQLException {
            BoxedResultSet rs = factory.create(resultSet);
            return new EAnalysisFaultCategory(rs.getInt("FaultCategory"), rs.getString("FaultCategoryName"));
        }
    }

    @Inject
    private EAnalysisFaultCategoryMapper mapper;

    public List<EAnalysisFaultCategory> getFaultCategoryList() {
        String query = queryManager.getQuery(EAnalysisConstants.EVENT_ANALYSIS_FAULT_CATEGORIES);
        return findByStoredProcedure(query, mapper);
    }
}
