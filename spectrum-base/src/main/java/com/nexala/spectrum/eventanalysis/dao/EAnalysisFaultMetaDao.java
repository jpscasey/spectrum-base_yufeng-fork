package com.nexala.spectrum.eventanalysis.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.google.inject.Inject;
import com.nexala.spectrum.db.JdbcRowMapper;
import com.nexala.spectrum.db.QueryManager;
import com.nexala.spectrum.db.dao.BoxedResultSet;
import com.nexala.spectrum.db.dao.BoxedResultSetFactory;
import com.nexala.spectrum.db.dao.GenericDao;
import com.nexala.spectrum.eventanalysis.EAnalysisConstants;
import com.nexala.spectrum.eventanalysis.beans.EAnalysisFaultMeta;

public class EAnalysisFaultMetaDao extends GenericDao<EAnalysisFaultMeta, String> {
    @Inject
    private QueryManager queryManager;

    private static class EAnalysisFaultMetaMapper extends JdbcRowMapper<EAnalysisFaultMeta> {
        @Inject
        private BoxedResultSetFactory factory;

        @Override
        public EAnalysisFaultMeta createRow(ResultSet resultSet) throws SQLException {
            BoxedResultSet rs = factory.create(resultSet);
            return new EAnalysisFaultMeta(rs.getInt("FaultID"), rs.getString("FaultDesc"), rs.getString("FaultCode"));
        }
    }

    @Inject
    private EAnalysisFaultMetaMapper mapper;

    public List<EAnalysisFaultMeta> search(String keyword,
            TreeMap<String, String> searchParameters) {
        String query = queryManager
                .getQuery(EAnalysisConstants.EVENT_ANALYSIS_SEARCH_FAULT_META);
        query = query
                .replace("__ADDITIONAL_FIELDS__", params(searchParameters));

        ArrayList<Object> qparams = new ArrayList<Object>();
        qparams.add(keyword);

        // parameters are sorted by parameter key from tree map
        for (String key : searchParameters.keySet()) {
            qparams.add(searchParameters.get(key));
        }

        return findByStoredProcedure(query, mapper, qparams.toArray());
    }

    private String params(Map<String, String> parameters) {
        StringBuilder sb = new StringBuilder();

        for (String key : parameters.keySet()) {
            sb.append(", ?");
        }

        return sb.toString();
    }

    public List<EAnalysisFaultMeta> allFaultMeta() {
        String query = queryManager.getQuery(EAnalysisConstants.EVENT_ANALYSIS_ALL_FAULT_META);
        return findByQuery(query, mapper);
    }
}
