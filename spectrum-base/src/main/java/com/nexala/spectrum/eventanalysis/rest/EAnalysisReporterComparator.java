package com.nexala.spectrum.eventanalysis.rest;

import java.util.Comparator;

import com.nexala.spectrum.eventanalysis.EAnalysisConstants;
import com.nexala.spectrum.eventanalysis.beans.EAnalysisReport;

public class EAnalysisReporterComparator {
    public static final Comparator<EAnalysisReport> TOTAL_COUNT_DESC = new Comparator<EAnalysisReport>() {
        public int compare(EAnalysisReport o1, EAnalysisReport o2) {
            if (o1.getTotalCount() > o2.getTotalCount()) {
                return -1;
            } else if (o1.getTotalCount() < o2.getTotalCount()) {
                return 1;
            } else {
                return 0;
            }
        }
    };

    public static final Comparator<EAnalysisReport> CREATE_TIME_ASC = new Comparator<EAnalysisReport>() {
        public int compare(EAnalysisReport o1, EAnalysisReport o2) {
            long t1 = Long.valueOf(o1.getDescriptors().get(EAnalysisConstants.COLUMN_REFERENCE_KEY));
            long t2 = Long.valueOf(o2.getDescriptors().get(EAnalysisConstants.COLUMN_REFERENCE_KEY));

            if (t1 < t2) {
                return -1;
            } else if (t1 > t2) {
                return 1;
            } else {
                return 0;
            }
        }
    };
}
