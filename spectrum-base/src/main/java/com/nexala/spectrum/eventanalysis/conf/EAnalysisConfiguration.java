package com.nexala.spectrum.eventanalysis.conf;

import java.util.List;
import java.util.TimeZone;

import com.google.inject.ImplementedBy;
import com.nexala.spectrum.eventanalysis.beans.EAnalysisFaultType;
import com.nexala.spectrum.eventanalysis.beans.EAnalysisGrouping;
import com.nexala.spectrum.eventanalysis.beans.EAnalysisReport;
import com.nexala.spectrum.licensing.Licence;
import com.nexala.spectrum.view.conf.Column;
import com.nexala.spectrum.view.conf.ColumnSorterBean;
import com.nexala.spectrum.view.conf.Field;
import com.nexala.spectrum.view.conf.ScreenConfiguration;

@ImplementedBy(EAnalysisConfigurationImpl.class)
public interface EAnalysisConfiguration extends ScreenConfiguration {
    List<EAnalysisGrouping> getGroupings();

    List<EAnalysisGrouping> getGroupings(List<EAnalysisFaultType> eaFaultTypes, Licence licence);

    List<Column> getDetailColumns();

    ColumnSorterBean getDetailDefaultSorter();

    List<Field> getSearchFields();

    String[] getExportRow(String groupingCode, EAnalysisReport row, boolean hasCountersColumn, TimeZone tz);
    
    Integer getFiltersPerRow();
}
