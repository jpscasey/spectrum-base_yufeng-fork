package com.nexala.spectrum.eventanalysis.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.google.inject.Inject;
import com.nexala.spectrum.db.JdbcRowMapper;
import com.nexala.spectrum.db.QueryManager;
import com.nexala.spectrum.db.dao.BoxedResultSet;
import com.nexala.spectrum.db.dao.BoxedResultSetFactory;
import com.nexala.spectrum.db.dao.GenericDao;
import com.nexala.spectrum.eventanalysis.EAnalysisConstants;
import com.nexala.spectrum.eventanalysis.beans.EAnalysisFaultType;

public class EAnalysisFaultTypeDao extends GenericDao<EAnalysisFaultType, String> {
    @Inject
    private QueryManager queryManager;

    private static class EAnalysisFaultTypeMapper extends JdbcRowMapper<EAnalysisFaultType> {
        @Inject
        private BoxedResultSetFactory factory;

        @Override
        public EAnalysisFaultType createRow(ResultSet resultSet) throws SQLException {
            BoxedResultSet rs = factory.create(resultSet);

            return new EAnalysisFaultType(rs.getInt("FaultType"), rs.getString("FaultTypeName"),
                    rs.getString("FaultColor"));
        }
    }

    @Inject
    private EAnalysisFaultTypeMapper mapper;

    public List<EAnalysisFaultType> getFaultTypeList() {
        String query = queryManager.getQuery(EAnalysisConstants.EVENT_ANALYSIS_FAULT_TYPES);
        return findByStoredProcedure(query, mapper);
    }
}
