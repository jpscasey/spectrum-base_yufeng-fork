/*
 * Copyright (c) Nexala Technologies 2013, All rights reserved.
 */
package com.nexala.spectrum.eventanalysis.view;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.common.collect.Lists;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.nexala.spectrum.eventanalysis.EAnalysisConstants;
import com.nexala.spectrum.eventanalysis.conf.EAnalysisConfiguration;
import com.nexala.spectrum.licensing.Licence;
import com.nexala.spectrum.licensing.RestrictedResource;
import com.nexala.spectrum.view.Plugin;
import com.nexala.spectrum.view.TemplateServlet;
import com.nexala.spectrum.view.conf.App;
import com.nexala.spectrum.view.conf.AppConfiguration;

@Singleton
public class EAnalysisView extends TemplateServlet {

    private static final long serialVersionUID = 7743892486241418025L;

    @Inject
    private EAnalysisConfiguration eAnalysisConfiguration;

    @Inject
    private AppConfiguration appConf;

    @RestrictedResource(EAnalysisConstants.EVENT_ANALYSIS_LICENCE_KEY)
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException {
        Licence licence = Licence.get(request, response, true);

        List<String> includes = new ArrayList<String>();

        Map<String, Object> rootMap = new HashMap<String, Object>();
        rootMap.put("head", "eanalysis_head.html.ftl");
        rootMap.put("includes", includes);
        rootMap.put("cssIncludesList", Lists.partition(getCssIncludes(), 10));
        rootMap.put("jsIncludesList", Lists.partition(getJsIncludes(), 10));
        rootMap.put("conf", eAnalysisConfiguration);
        
        rootMap.put("groupings", eAnalysisConfiguration.getGroupings());
        rootMap.put("screenName", eAnalysisConfiguration.getScreenName());

        doRender(request, response, "eanalysis.html.ftl", rootMap, licence);
    }
 
    private List<String> getCssIncludes() {
        List<Plugin> plugins = eAnalysisConfiguration.getPlugins();
        
        List<String> cssIncludes = new ArrayList<String>();
        
        for (Plugin p : plugins) {
            cssIncludes.addAll(p.getStylesheets());
        }
        
        return new ArrayList<String>(cssIncludes);
    }
    
    private List<String> getJsIncludes() {
        List<Plugin> plugins = eAnalysisConfiguration.getPlugins();
        
        List<String> jsIncludes = new ArrayList<String>();
        
        for (Plugin p : plugins) {
            jsIncludes.addAll(p.getSources());
        }
        
        return new ArrayList<String>(jsIncludes);
    }
    
    
    public App getView() {
        return appConf.getApp(EAnalysisConstants.EVENT_ANALYSIS_DESC);
    }
}
