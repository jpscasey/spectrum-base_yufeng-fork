package com.nexala.spectrum.eventanalysis.beans;

public class EAnalysisFaultMeta {
    private final int id;
    private final String description;
    private final String code;

    public EAnalysisFaultMeta(int id, String description, String code) {
        this.id = id;
        this.description = description;
        this.code = code;
    }

    public int getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public String getCode() {
        return code;
    }
}
