package com.nexala.spectrum.eventanalysis.beans;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(using = EAnalysisReportSerializer.class)
public class EAnalysisReport {
    private final String fleetId;
    private final Map<String, String> descriptors;
    private final List<EAnalysisFaultType> faultTypes;

    private List<EAnalysisBean> beans;
    private Map<Integer, Integer> faultTypeCountMap;
    private Double totalDuration;
    private Double averageDuration;

    public EAnalysisReport(String fleetId, Map<String, String> descriptors, List<EAnalysisFaultType> faultTypes) {
        this.fleetId = fleetId;
        this.descriptors = descriptors;
        this.faultTypes = faultTypes;

        beans = new ArrayList<EAnalysisBean>();

        faultTypeCountMap = new HashMap<Integer, Integer>();
        for (EAnalysisFaultType faultType : faultTypes) {
            faultTypeCountMap.put(faultType.getId(), 0);
        }

        totalDuration = 0d;
        averageDuration = 0d;
    }

    public String getFleetId() {
        return fleetId;
    }

    public Map<String, String> getDescriptors() {
        return descriptors;
    }

    public List<EAnalysisFaultType> getFaultTypes() {
        return faultTypes;
    }

    public List<EAnalysisBean> getBeans() {
        return beans;
    }

    public Map<Integer, Integer> getFaultTypeCountMap() {
        return faultTypeCountMap;
    }

    public Integer getTotalCount() {
        return beans.size();
    }

    public Double getTotalDuration() {
        return totalDuration;
    }

    public Double getAverageDuration() {
        return averageDuration;
    }

    public void addBean(EAnalysisBean bean) {
        beans.add(bean);

        faultTypeCountMap.put(bean.getFaultTypeId(), faultTypeCountMap.get(bean.getFaultTypeId()) + 1);

        totalDuration += Math.abs(bean.getCreateTime()
                - (bean.getEndTime() != null ? bean.getEndTime() : bean.getCreateTime()));

        averageDuration = totalDuration / beans.size();
    }
}
