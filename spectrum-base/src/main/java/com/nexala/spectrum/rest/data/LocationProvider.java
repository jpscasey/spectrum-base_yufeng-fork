package com.nexala.spectrum.rest.data;

import com.nexala.spectrum.db.beans.Location;


/**
 * Provide methods for location
 * @author brice
 *
 */
public interface LocationProvider {
    
    /**
     * Return the location for this code
     * @param code the location code
     * @return the location
     */
    public Location getLocationByCode(String code);
    
    /**
     * Return the current location of this vehicle.
     * @param vehicleId the vehicleId
     * @return the location
     */
    public Location getVehicleCurrentLocation(Integer vehicleId);

}
