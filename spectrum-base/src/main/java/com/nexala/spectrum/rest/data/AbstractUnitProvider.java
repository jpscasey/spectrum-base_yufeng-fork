package com.nexala.spectrum.rest.data;

import java.util.ArrayList;
import java.util.List;

import com.google.inject.Inject;
import com.nexala.spectrum.db.beans.FleetFormation;
import com.nexala.spectrum.db.beans.Unit;
import com.nexala.spectrum.db.beans.Vehicle;
import com.nexala.spectrum.db.dao.DataAccessException;
import com.nexala.spectrum.db.dao.FleetFormationDao;
import com.nexala.spectrum.db.dao.UnitDao;
import com.nexala.spectrum.db.dao.UnitSearchDao;
import com.nexala.spectrum.rest.data.beans.Formation;
import com.nexala.spectrum.rest.data.beans.GenericBean;
import com.nexala.spectrum.rest.data.beans.StockDesc;

public abstract class AbstractUnitProvider implements UnitProvider {
	
	@Inject
    private UnitDao unitDao;
	
	@Inject
	private FleetFormationDao fleetFormationDao;
	
	@Inject
    private UnitSearchDao searchDao;
    
    public abstract String getFleetCode();

    @Override
    public List<GenericBean> searchUnitsByString(String substr) throws DataAccessException {
        return searchDao.getFleetUnitsByString(getFleetCode(), substr); 
    }
    
	@Override
	public Unit searchUnit(int unitId) throws DataAccessException {
		return unitDao.findById(unitId);
	}

	@Override
	public Unit searchUnit(String unitNumber) throws DataAccessException {
		return unitDao.findByUnitNumber(unitNumber);
	}

	@Override
	public List<StockDesc> getStockForUnit(int unitId) throws DataAccessException {
        Unit unit = unitDao.findById(unitId);
        List<StockDesc> stock = new ArrayList<StockDesc>();

        stock.add(new StockDesc(unit.getId(), unit.getUnitNumber(), 0, null, true, unit.getUnitType()));

        return stock;
	}

	@Override
	public List<Formation> formation(String unitIds) throws DataAccessException {
        List<Formation> formationList = new ArrayList<Formation>();

        if (unitIds != null) {
            for (Unit unit : unitDao.findUnitsByUnitIds(unitIds)) {
                Formation formation = new Formation(unit);
                for (Vehicle vehicle : unit.getVehicles()) {
                    formation.addVehicle(vehicle);
                }

                formationList.add(formation);
            }
        }

        return formationList;
	}

	@Override
	public List<Unit> getFormationUnits(int unitId) throws DataAccessException {
		return unitDao.findUnitFormationByUnitId(unitId);
	}
	
	@Override
	public List<FleetFormation> getValidFleetFormations() throws DataAccessException {
	    return fleetFormationDao.getValidFormations();
	}

    @Override
    public List<Unit> getAllUnits() throws DataAccessException {
        return unitDao.getAllUnits(getFleetCode());
    }

	@Override
	public List<String> getUnitTypes() throws DataAccessException {
		return unitDao.getUnitTypes();
	}
}
