/*
 * Copyright (c) Nexala Technologies 2013, All rights reserved.
 */
package com.nexala.spectrum.rest.service;

import java.util.List;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.nexala.spectrum.licensing.LoggingData;
import com.nexala.spectrum.rest.data.StockProvider;
import com.nexala.spectrum.rest.data.beans.StockDesc;

@Path("/stock")
@Singleton
public class StockService {

    @Inject
    private Map<String, StockProvider> providers;

    @GET
    @LoggingData
    @Path("/{stockId: [0-9]+}")
    @Produces(MediaType.APPLICATION_JSON)
    public StockDesc stock(@PathParam("stockId") int stockId) {
        return null;
    }

    @GET
    @LoggingData
    @Path("/children/{stockCollectionId: [0-9]+}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<StockDesc> children(@PathParam("stockCollectionId") int stockCollectionId) {
        return null;
    }

    @GET
    @LoggingData
    @Path("/siblings/{fleetId}/{stockId: [0-9]+}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<StockDesc> siblings(@PathParam("fleetId") String fleetId, @PathParam("stockId") int stockId) {
        return providers.get(fleetId).getSiblings(stockId);
    }
    
    @GET
    @LoggingData
    @Path("/stocksByEvent/{fleetId}/{eventId: [0-9]+}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<StockDesc> stocksByEvent(
            @PathParam("fleetId") String fleetId,
            @PathParam("eventId") int eventId) {
        return providers.get(fleetId).getStocksByEvent(eventId);
    }
}
