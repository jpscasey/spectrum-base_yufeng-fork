package com.nexala.spectrum.rest.service.bean;

import java.util.List;

public class UnitDetailRow {
	private int position;
	
	private List<UnitDetailCell> cells;

	public int getPosition() {
		return position;
	}

	public void setPosition(int position) {
		this.position = position;
	}

	public List<UnitDetailCell> getCells() {
		return cells;
	}

	public void setCells(List<UnitDetailCell> cells) {
		this.cells = cells;
	}
}
