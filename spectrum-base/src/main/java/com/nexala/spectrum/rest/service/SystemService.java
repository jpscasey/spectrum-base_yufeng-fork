package com.nexala.spectrum.rest.service;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.nexala.spectrum.configuration.ApplicationConfiguration;
import com.nexala.spectrum.db.beans.UserConfiguration;
import com.nexala.spectrum.licensing.Licence;
import com.nexala.spectrum.licensing.LoginUser;
import com.nexala.spectrum.rest.data.UserConfigurationProvider;
import com.nexala.spectrum.rest.service.bean.BrandingBean;
import com.nexala.spectrum.rest.service.bean.SpectrumConfiguration;

@Path("/system/")
@Singleton
public class SystemService {

    @Inject
    private ApplicationConfiguration appConf;

    @Inject
    private UserConfigurationProvider userConfProvider;

    @Inject
    private ObjectMapper objectMapper;
    
    @Inject
    private HelpService helpService;
    
    @Inject
    private EventService eventService;
    
    @Inject
    private UnitService unitService;

    private final Logger LOGGER = Logger.getLogger(SystemService.class);

    @GET
    @Path("/user")
    @Produces(MediaType.APPLICATION_JSON)
    public LoginUser user(@Context HttpServletRequest request, @Context HttpServletResponse response) {
        return Licence.get(request, response, true).getLoginUser();
    }

    @GET
    @Path("/globalRefreshRate")
    @Produces(MediaType.APPLICATION_JSON)
    public Integer getGlobalRefreshRate() {
        return appConf.get().getInt("r2m.globalScreenRefreshRateInMilliseconds");
    }

    @POST
    @Path("/setUserConfiguration/")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response setUserConfiguration(@Context HttpServletRequest request, @Context HttpServletResponse response,
            Map<String, Object> data) {
        Licence licence = Licence.get(request, response, true);

        String dataStr;
        try {
            dataStr = objectMapper.writeValueAsString(data.get("data"));
        } catch (JsonProcessingException e) {
            LOGGER.error("Can't parse the user configuration", e);
            dataStr = null;
        }

        userConfProvider.updateUserConfiguration(licence.getLoginUser().getLogin(), 
                data.get("variable").toString(), dataStr);

        return Response.ok().build();
    }
    
    @GET
    @Path("/getUserConfiguration/{variable}")
    @Produces(MediaType.APPLICATION_JSON)
    public String getUserConfiguration(@Context HttpServletRequest request, 
            @Context HttpServletResponse response, @PathParam("variable")String variable) {
        String result = null;
        Licence licence = Licence.get(request, response, true);
        UserConfiguration configuration = userConfProvider.getUserConfiguration(licence.getLoginUser().getLogin(), variable);
        if (configuration != null) {
            result = configuration.getData();
        }
        return result;
    }
    
    @GET
    @Path("/brandingConfiguration")
    @Produces(MediaType.APPLICATION_JSON)
    public BrandingBean getBrandingConfiguration() {
        BrandingBean b = new BrandingBean();
        
        b.setLogo(appConf.get().getString("r2m.branding"));
        b.setLogoStyle(appConf.get().getString("r2m.brandingStyle"));
        
        return b;
    }
    
    @GET
    @Path("/spectrumConfiguration")
    @Produces(MediaType.APPLICATION_JSON)
    public SpectrumConfiguration spectrumConfiguration(@Context HttpServletRequest request, @Context HttpServletResponse response) {
        SpectrumConfiguration result = new SpectrumConfiguration();
        
        result.setGlobalRefreshRate(getGlobalRefreshRate());
        result.setUser(user(request,response));
        result.setEventsPanelLicence(eventService.hasEventsPanelLicence(request));
        result.setHelpConfig(helpService.helpConfig(request));
        result.setUnitSearchConfiguration(unitService.unitSearchConfiguration());
       
       
       return result;
    }
    
    
    
    
}
