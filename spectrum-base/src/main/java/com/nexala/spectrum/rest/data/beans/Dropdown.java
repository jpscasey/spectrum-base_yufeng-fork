package com.nexala.spectrum.rest.data.beans;

public class Dropdown {

    // set code as -1 for the label to be used as value as well
    private Integer code;
    private String label;
    
    public Dropdown() {}

    public Dropdown(Integer code, String label) {
        this.code = code;
        this.label = label;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer integer) {
        this.code = integer;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}
