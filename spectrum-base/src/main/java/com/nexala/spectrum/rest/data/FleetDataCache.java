/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.rest.data;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.channel.ChannelCollection;
import com.nexala.spectrum.channel.ChannelString;
import com.nexala.spectrum.configuration.ConfigurationManager;
import com.nexala.spectrum.licensing.DataLicensor;
import com.nexala.spectrum.rest.data.beans.DataSet;
import com.nexala.spectrum.rest.data.beans.DataSetImpl;
import com.nexala.spectrum.view.conf.CommonFleetConfiguration;


/**
 * Utils to cache fleet data every {@link FleetDataCache#cacheInterval}.
 * 
 * @author poriordan
 */
@Singleton
public class FleetDataCache {

    @Inject
    private DataLicensor dataLicensor;

    @Inject
    private Map<String, FleetProvider> fleetProviders;
    
    public static int cacheInterval;
    
    
    private Map<String, List<DataSet>> fleetCache = new ConcurrentHashMap<String, List<DataSet>>();
    private ListMultimap<String, String> fleetsPerDatabase = ArrayListMultimap.create();
    
    private Long timestamp = null;

    @Inject
    public FleetDataCache(ConfigurationManager conf, CommonFleetConfiguration fleetConf) {
        cacheInterval = conf.getConfAsInteger(Spectrum.SPECTRUM_REFRESH_FLEET_CACHE_INTERVAL, 5000);

        fleetsPerDatabase = fleetConf.getFleetsPerDatabase();

    }


    /**
     * Return data for this time and fleet.
     * @param timestamp timestamp provided by {@link FleetDataCache#roundedTimestamp()}
     * @param fleetId the fleet id
     * @return list of datas
     */
    public List<DataSet> get(long timestamp, String fleetId) {
        List<DataSet> cachedData = new ArrayList<DataSet>();
        
        synchronized (this) {
            if (this.timestamp == null || this.timestamp < timestamp) {

                add(timestamp);
                this.timestamp = timestamp;
            }   
        }
        
        if (fleetCache.containsKey(fleetId)) {
            cachedData = fleetCache.get(fleetId);
        }

        return dataLicensor.getLicenced(cachedData);
    }

    /**
     * Add data in the cache for this timestamp and fleet.
     * @param timestamp timestamp provided by {@link FleetDataCache#roundedTimestamp()}
     * @param fleetId the fleet id
     * @param vehicles the datas
     */
    public void add(long timestamp) {

        ListMultimap<String, DataSet> temp = ArrayListMultimap.create();

        for (String database : fleetsPerDatabase.keySet()) {
            String fleet = fleetsPerDatabase.get(database).get(0);
            List<DataSet> data = fleetProviders.get(fleet).getLiveData();
            if (data != null) {
                for (DataSet d : data){
                    ChannelCollection channels = d.getChannels();
                    String vehicleId = channels.getByName(Spectrum.VEHICLE_ID).getValue().toString();
                    for (int i = 0; i < fleetsPerDatabase.get(database).size(); i++) {
                        String fleetCode = fleetsPerDatabase.get(database).get(i);
                        if (channels.getByName(Spectrum.FLEET_CODE) != null && channels
                                .getByName(Spectrum.FLEET_CODE, ChannelString.class).getValue().equals(fleetCode)) {
                            temp.put(fleetCode, new DataSetImpl(Integer.parseInt(vehicleId), channels));
                        } 
                    }
                }
            }
        }

        ListMultimap<String, DataSet> result = ArrayListMultimap.create(temp);
        for (String key : result.keySet()) {
            fleetCache.put(key, result.get(key));
        }
        
    }
    
    /**
     * Round down the timestamp using {@link FleetDataCache#cacheInterval}.
     * @return timestamp round down
     */
    public static long roundedTimestamp() {
        long timestamp = System.currentTimeMillis();
        return timestamp - (timestamp % cacheInterval);
    }

}
