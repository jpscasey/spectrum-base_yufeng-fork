/**
 * 
 */
package com.nexala.spectrum.rest.data;

import java.io.IOException;
import java.util.List;

import com.nexala.spectrum.db.beans.ChannelConfig;
import com.nexala.spectrum.licensing.Licence;
import com.nexala.spectrum.rest.data.beans.DataSet;

/**
 * Chart provider common for all fleets.
 * 
 * @author bbaudry
 *
 */
public interface CommonChartProvider {
    /**
     * Create the chart image used on the tab.
     * @param fleetId the fleet id
     * @param vehicleId the vehicle id
     * @param interval interval to display
     * @param endTimeParam the end time
     * @param timezone the user timezone
     * @param channelList list of channels to include in the dataplot
     * @param height height of the chart
     * @param width width of the chart
     * @return Image data (png)
     * @throws IOException in case of error
     */
	public byte[] createImage(String fleetId, int vehicleId, int interval, String endTimeParam, String timezone,
			List<ChannelConfig> channels, Integer height, Integer width, Licence licence) throws IOException;

    /**
     * Get the data to export by chunks
     * @param fleetId the fleet id
     * @param vehicleId the vehicle id
     * @param endTimeParam the end time
     * @param interval interval to display
     * @param channels list of channels to include in the exported file
     * @return List of Datasets
     */
    public List<DataSet> getVehicleDataExport(String fleetId, int vehicleId, Long endTime, int interval, List<ChannelConfig> channels,
            boolean loadLookupValues);

	
}
