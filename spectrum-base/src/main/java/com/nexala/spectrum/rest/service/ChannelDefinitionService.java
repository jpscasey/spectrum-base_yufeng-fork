/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.rest.service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.apache.commons.lang.StringEscapeUtils;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;
import com.nexala.spectrum.db.beans.ChannelConfig;
import com.nexala.spectrum.db.beans.ChannelDefinition;
import com.nexala.spectrum.db.beans.ChannelDefinitionParam;
import com.nexala.spectrum.db.beans.ChannelDefinitionRuleParam;
import com.nexala.spectrum.db.beans.ChannelGroupConfig;
import com.nexala.spectrum.licensing.Licence;
import com.nexala.spectrum.licensing.LoggingData;
import com.nexala.spectrum.rest.data.ChannelDefinitionProvider;
import com.nexala.spectrum.validation.ChannelStatus;
import com.nexala.spectrum.view.Constants;
import com.nexala.spectrum.view.conf.ChannelDefinitionConfiguration;

@Path("/channeldefinition")
@Singleton
public class ChannelDefinitionService {

    @Inject
    private ChannelDefinitionConfiguration commonDefinitionConf;

    @Inject
    private Map<String, ChannelDefinitionProvider> channelDefinitionProviders;

    @Inject
    private Provider<HttpServletResponse> responseProvider;

    @GET
    @Path("/conf")
    @Produces(MediaType.APPLICATION_JSON)
    public ChannelDefinitionConfiguration getConf() {
        return commonDefinitionConf;
    }

    @POST
    @LoggingData
    @Path("/search")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public List<ChannelDefinition> search(Map<String, String> parameters) {
        String fleetId = parameters.get("fleetId");
        String keyword = parameters.get("keyword");

        return channelDefinitionProviders.get(fleetId).search(keyword != null ? keyword : "");
    }

    @GET
    @Path("/groups/{fleetId}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<ChannelGroupConfig> groups(@PathParam("fleetId") String fleetId) {
        return channelDefinitionProviders.get(fleetId).getGroups();
    }

    @GET
    @Path("/status/{fleetId}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<ChannelStatus> status(@PathParam("fleetId") String fleetId) {
        return channelDefinitionProviders.get(fleetId).getStatus();
    }

    @GET
    @LoggingData
    @Path("/channel/{fleetId}/{channelId}")
    @Produces(MediaType.APPLICATION_JSON)
    public ChannelDefinition channel(@PathParam("fleetId") String fleetId, @PathParam("channelId") int channelId) {
        return channelDefinitionProviders.get(fleetId).getChannelDefinition(channelId);
    }

    @GET
    @LoggingData
    @Path("/channels/{fleetId}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<ChannelConfig> channels(@PathParam("fleetId") String fleetId) {
        return channelDefinitionProviders.get(fleetId).getChannelConfigs();
    }

    @POST
    @LoggingData
    @Path("/save")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public boolean save(ChannelDefinitionParam data) {
        String fleetId = null;
        Map<String, Object> params = data.getParameters();

        StringBuffer sb = new StringBuffer();
        sb.append("<channel ");
        
        for (String key : params.keySet()) {
            String par = params.get(key).toString();
            if (key.equals("fleetId")) {
                fleetId = par;
            } else {
                sb.append(key.toLowerCase() + "=\"" + StringEscapeUtils.escapeXml(par) + "\" ");
            }
        }
        sb.append(">");
        
        for (ChannelDefinitionRuleParam rule : data.getRules()) {
            sb.append(rule.toString());
        }

        sb.append("</channel>");
        
        return channelDefinitionProviders.get(fleetId).save(sb.toString());
    }
    
    @GET
    @Path("/getEditLicence")
    @Produces(MediaType.APPLICATION_JSON)
    public Boolean hasChannelDefinitionEditLicence(@Context HttpServletRequest request) {
        boolean hasChannelDefinitionEditLicence = false;
        Licence licence = Licence.get(request, responseProvider.get(), true);

        if (licence.hasResourceAccess(Constants.CHANNEL_DEFINITION_LIC_KEY)) {
        	hasChannelDefinitionEditLicence = true;
        }

        return hasChannelDefinitionEditLicence;
    }
}
