/**
 * 
 */
package com.nexala.spectrum.rest.data;

import com.nexala.spectrum.db.beans.UserConfiguration;

/**
 * Provide the user configuration.
 * @author bbaudry
 *
 */
public interface UserConfigurationProvider {
    
    /**
     * Return the user configuration for the given user and variable. 
     * If the variable don't exist it's created and the data is initialized to null.
     * @param username the username
     * @param variable the variable we want to retrieve
     * @return the user configuratio
     */
    public UserConfiguration getUserConfiguration(String username, String variable);
    
    /**
     * Update the user configuration.
     * @param username the username
     * @param variable the variable we want to update
     * @param data the new value
     */
    public void updateUserConfiguration(String username, String variable, String data);
    

}
