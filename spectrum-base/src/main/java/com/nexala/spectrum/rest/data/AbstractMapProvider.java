/*
 * Copyright (c) Nexala Technologies 2015, All rights reserved.
 *
 * $Author$
 * $Date$
 * $Revision$
 * $Source$
 */
package com.nexala.spectrum.rest.data;

import java.util.List;

import com.google.inject.Inject;
import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.channel.ChannelCollection;
import com.nexala.spectrum.channel.ChannelLong;
import com.nexala.spectrum.rest.data.beans.DataSet;
import com.nexala.spectrum.view.conf.MapConfiguration;


public abstract class AbstractMapProvider implements MapProvider {

    @Inject
    private MapConfiguration configuration;
    
    protected boolean isActive(List<DataSet> data, Long endTime) {
        Long maxInactiveTime = 0L;
        Long timestamp = null;
        
        if (configuration.getMaxInactiveTime() != null) {
            maxInactiveTime = configuration.getMaxInactiveTime();

            for (DataSet cv : data) {
                ChannelCollection channels = cv.getChannels();

                if (timestamp == null) {
                    Long t = channels.getByName(Spectrum.TIMESTAMP, ChannelLong.class).getValue();
                    if (t != null) {
                        timestamp = t;
                        break;
                    }
                }
            }
            if (endTime == null) {
                endTime = System.currentTimeMillis();
            }
            return timestamp != null && timestamp > endTime - maxInactiveTime;
        } else {
            return true;
        }
    }

}
