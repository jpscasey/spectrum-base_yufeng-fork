package com.nexala.spectrum.rest.service.bean.editor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "variable")
@XmlAccessorType(XmlAccessType.FIELD)
public class Variable {
    
    @XmlElement(name="name", required=true)
    private String name;
    
    @XmlElement(name="description", required=true)
    private String description;
    
    @XmlElement(name="type", required=true)
    private String type;

    /**
     * Return the name.
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Setter for the name.
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Return the description.
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Setter for the description.
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Return the type.
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * Setter for the type.
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }
    

}
