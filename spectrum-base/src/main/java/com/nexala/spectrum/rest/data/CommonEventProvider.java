/*
 * Copyright (c) Nexala Technologies 2013, All rights reserved.
 *
 * $Author$
 * $Date$
 * $Revision$
 * $Source$
 */
package com.nexala.spectrum.rest.data;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.google.inject.ImplementedBy;
import com.nexala.spectrum.db.beans.FaultCategory;
import com.nexala.spectrum.db.beans.FaultMeta;
import com.nexala.spectrum.db.beans.FaultType;
import com.nexala.spectrum.db.beans.Fleet;
import com.nexala.spectrum.db.beans.UserConfiguration;
import com.nexala.spectrum.licensing.Licence;
import com.nexala.spectrum.rest.data.beans.Dropdown;
import com.nexala.spectrum.rest.data.beans.Event;
import com.nexala.spectrum.rest.data.beans.EventSearchParameter;

@ImplementedBy(DefaultCommonEventProvider.class)
public interface CommonEventProvider {

    public List<FaultType> getAllFleetsFaultTypes();
    
    public List<FaultMeta> getAllFleetsFaultCodes();

    public List<Event> getAllFleetsEventsSince(long since);
    
    public List<Event> searchAllFleetsEvents(Date dateFrom, Date dateTo, String live, String keyword, Integer limit, Licence licence);

    public List<Event> searchAllFleetsEvents(Date dateFrom, Date dateTo, String code, String type, String description, String category,
            String live, List<EventSearchParameter> parameters, Integer limit, Licence licence);
    
    public boolean areFaultsRetracted(Long timestamp);
    
    public Integer countAllFleetsLiveEvents(List<Object> criteria, Licence licence);

    public Map<String, List<Dropdown>> getAdditionnalDropDownValues(List<Fleet> fleets);

    public Long getLastEventTimestamp(boolean live);

    public List<FaultCategory> getLiveAndRecentCategories();

    public List<FaultType> getLiveAndRecentSeverities();

    public List<Fleet> getLiveAndRecentFleets();

	public Map<String, List<Dropdown>> getNewFaultDropDownValues();
	
	public List<Event> getCurrentEvents(Integer limit, List<Object> searchCriteria, Licence licence);
	
	public UserConfiguration getEventPanelUserConfiguration(Licence licence);
}
