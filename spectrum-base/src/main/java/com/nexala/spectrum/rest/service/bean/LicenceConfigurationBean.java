package com.nexala.spectrum.rest.service.bean;

public class LicenceConfigurationBean {
	private String permission;
	private String licenceKey;
	
	public String getPermission() {
		return permission;
	}

	public void setPermission(String permission) {
		this.permission = permission;
	}

	public String getLicenceKey() {
		return licenceKey;
	}

	public void setLicenceKey(String licenceKey) {
		this.licenceKey = licenceKey;
	}

}
