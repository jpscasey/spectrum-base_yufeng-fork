package com.nexala.spectrum.rest.data.beans.route;

/**
 * 
 * Bean representing a route.
 * @author brice
 *
 */
public class Route {
    
    String id = null;
    
    String label = null;
    
    /**
     * Default constructor
     */
    public Route() {
        
    }
    
    /**
     * Constructor with id and label in parameter
     * @param id the route id
     * @param label the route label
     */
    public Route(String id, String label) {
        this.id = id;
        this.label = label;
    }

    /**
     * Getter for id.
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * Setter for id.
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Getter for label.
     * @return the label
     */
    public String getLabel() {
        return label;
    }

    /**
     * Setter for label.
     * @param label the label to set
     */
    public void setLabel(String label) {
        this.label = label;
    }

}
