package com.nexala.spectrum.rest.data.beans;


/**
 * EventFormation 
 * 
 * @author Javier Munoz
 * 
 */
public class EventFormation{
    private Long vehicleId;
    private String vehicleNumber;
    private Integer vehicleOrder;
    private Boolean isFaultVehicle;
    public Long getVehicleId() {
        return vehicleId;
    }
    public void setVehicleId(Long vehicleId) {
        this.vehicleId = vehicleId;
    }
    public String getVehicleNumber() {
        return vehicleNumber;
    }
    public void setVehicleNumber(String vehicleNumber) {
        this.vehicleNumber = vehicleNumber;
    }
    public Integer getVehicleOrder() {
        return vehicleOrder;
    }
    public void setVehicleOrder(Integer vehicleOrder) {
        this.vehicleOrder = vehicleOrder;
    }
    public Boolean getIsFaultVehicle() {
        return isFaultVehicle;
    }
    public void setIsFaultVehicle(Boolean isFaultVehicle) {
        this.isFaultVehicle = isFaultVehicle;
    }
    
    
  
}