package com.nexala.spectrum.rest.service.bean;

import java.util.Map;

import com.nexala.spectrum.licensing.LoginUser;
import com.nexala.spectrum.view.conf.unit.UnitSearchConfiguration;

/**
 * A bean containing the configuration needed on all the r2m screens.
 * See nexala.spectrum.js file.
 * @author BBaudry
 *
 */
public class SpectrumConfiguration {
    
    private Map<String, String> helpConfig;
    
    private Integer globalRefreshRate;
    
    private UnitSearchConfiguration unitSearchConfiguration;
    
    private Boolean eventsPanelLicence;
    
    private LoginUser user;

    public Map<String, String> getHelpConfig() {
        return helpConfig;
    }

    public void setHelpConfig(Map<String, String> helpConfig) {
        this.helpConfig = helpConfig;
    }

    public Integer getGlobalRefreshRate() {
        return globalRefreshRate;
    }

    public void setGlobalRefreshRate(Integer globalRefreshRate) {
        this.globalRefreshRate = globalRefreshRate;
    }

    public UnitSearchConfiguration getUnitSearchConfiguration() {
        return unitSearchConfiguration;
    }

    public void setUnitSearchConfiguration(UnitSearchConfiguration unitSearchConfiguration) {
        this.unitSearchConfiguration = unitSearchConfiguration;
    }

    public Boolean getEventsPanelLicence() {
        return eventsPanelLicence;
    }

    public void setEventsPanelLicence(Boolean eventsPanelLicence) {
        this.eventsPanelLicence = eventsPanelLicence;
    }

    public LoginUser getUser() {
        return user;
    }

    public void setUser(LoginUser user) {
        this.user = user;
    }

}
