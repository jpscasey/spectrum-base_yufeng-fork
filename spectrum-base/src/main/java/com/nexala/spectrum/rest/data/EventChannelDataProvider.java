/*
 * Copyright (c) Nexala Technologies 2013, All rights reserved.
 */
package com.nexala.spectrum.rest.data;

import java.util.List;

import com.google.inject.ImplementedBy;
import com.nexala.spectrum.rest.data.beans.DataSet;
import com.nexala.spectrum.rest.data.beans.SourceEventChannelData;

@ImplementedBy(DefaultEventChannelDataProvider.class)
public interface EventChannelDataProvider {
    public List<DataSet> getEventChannelDataByVehicle(
            Integer eventSource,
            Integer vehicleId);
    
    /**
     * Return the eventChannelData for all the sources.
     * @return the eventChannelData for all the sources.
     */
    public List<SourceEventChannelData> getEventChannelData();
    
}
