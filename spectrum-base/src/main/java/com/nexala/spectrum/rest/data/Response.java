/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.rest.data;

import com.fasterxml.jackson.annotation.JsonUnwrapped;

public class Response<T> {
    private final long timestamp;
    
    @JsonUnwrapped
    private T data;
    
    public Response(long timestamp, T data) {
        this.timestamp = timestamp;
        this.data = data;
    }

    public long getTimestamp() {
        return timestamp;
    }
    
    public T getData() {
        return data;
    }
    
    public static<T> Response<T> of(long timestamp, T data) {
        return new Response<T>(timestamp, data);
    }
    
    @Deprecated
    public static<T> Response<T> of(T data) {
        return new Response<T>(System.currentTimeMillis(), data);
    }
}
