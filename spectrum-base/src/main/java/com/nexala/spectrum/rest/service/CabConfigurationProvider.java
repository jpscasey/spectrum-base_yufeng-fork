package com.nexala.spectrum.rest.service;

import com.nexala.spectrum.licensing.ScreenRequiresFeature;
import com.nexala.spectrum.view.conf.DetailScreen;
import com.nexala.spectrum.view.conf.cab.CabConfiguration;

/**
 * Provider for the cab configuration.
 * @author brice
 *
 */
public interface CabConfigurationProvider extends DetailScreen, ScreenRequiresFeature {
    
    /**
     * Return the cab configuration to use.
     * @param vehicleId the vehicle id, could be null
     * @return the cab configuration to use
     */
    public CabConfiguration getCabConfiguration(Integer vehicleId);
    
    
    /**
     * Return the javascript class name of the plugin used for configuration.
     * Null if no plugins.
     * @return Return the javascript class name of the plugin used, false otherwise
     */
    public String getPlugin();
    

}
