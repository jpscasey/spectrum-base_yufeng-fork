package com.nexala.spectrum.rest.service;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.statistics.StatisticsGateway;

import com.google.inject.Inject;
import com.google.inject.Singleton;

/**
 * Rest services for the cache.
 * @author brice
 *
 */
@Path("/cache/")
@Singleton
public class CacheService {
	
	@Inject
	private CacheManager cacheManager;
	
	@GET
	@Path("/statistics")
	public String statistics() {
	    StringBuilder sb = new StringBuilder();

	    /* get stats for all known caches */
	    for (String name : cacheManager.getCacheNames()) {
	      Cache cache = cacheManager.getCache(name);
	      StatisticsGateway stats = cache.getStatistics();
	      

	      sb.append(String.format("%s: %s objects, %s hits, %s expired %s memory size\n",
	        name,
	        stats.getLocalHeapSize(),
	        stats.cacheHitCount(),
	        stats.cacheExpiredCount(),
	        cache.calculateInMemorySize()
	      ));
	    }
	    return sb.toString();
	  }

}
