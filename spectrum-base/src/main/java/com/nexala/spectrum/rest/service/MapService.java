package com.nexala.spectrum.rest.service;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;
import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.channel.Channel;
import com.nexala.spectrum.channel.ChannelLong;
import com.nexala.spectrum.licensing.DataLicensor;
import com.nexala.spectrum.licensing.Licence;
import com.nexala.spectrum.licensing.LoggingData;
import com.nexala.spectrum.mapping.Coordinate;
import com.nexala.spectrum.rest.cache.Cache;
import com.nexala.spectrum.rest.data.DataUtils;
import com.nexala.spectrum.rest.data.FleetProvider;
import com.nexala.spectrum.rest.data.MapDataCache;
import com.nexala.spectrum.rest.data.MapProvider;
import com.nexala.spectrum.rest.data.Response;
import com.nexala.spectrum.rest.data.UserMapConfiguration;
import com.nexala.spectrum.rest.data.beans.DataSet;
import com.nexala.spectrum.rest.security.TimeoutFree;
import com.nexala.spectrum.view.conf.MapConfiguration;
import com.nexala.spectrum.view.conf.maps.MapLicencedConfiguration;
import com.nexala.spectrum.view.conf.maps.MapMarkerProvider;

import au.com.bytecode.opencsv.CSVWriter;

@Path("/map/")
@Singleton
public class MapService {
    @Inject
    private MapDataCache cache;

    @Inject
    private Map<String, MapProvider> mapProviders;

    @Inject
    private MapConfiguration configuration;
    
    @Inject
    private DataLicensor dataLicensor;

    @Inject
    private Map<String, FleetProvider> fleetProviders;

    @Inject
    private MapMarkerProvider mapMarkerProvider;

    @Inject
    private Provider<HttpServletRequest> requestProvider;
    
    @Inject
    private Provider<HttpServletResponse> responseProvider;

    @GET
    @Cache
    @LoggingData
    @Path("/conf")
    @Produces(MediaType.APPLICATION_JSON)
    public Response<UserMapConfiguration> conf() {
        Licence licence = Licence.get(requestProvider.get(), responseProvider.get(), true);
        return Response.of(configuration.getUserMapConfiguration(licence));
    }
    
    @GET
    @Cache
    @LoggingData
    @Path("/licencedConf")
    @Produces(MediaType.APPLICATION_JSON)
    public MapLicencedConfiguration licencedConf() {
        Licence licence = Licence.get(requestProvider.get(), responseProvider.get(), true);
        return configuration.getMapLicencedObjects(licence);
    }

    @GET
    @TimeoutFree
    @LoggingData
    @Path("/data")
    @Produces(MediaType.APPLICATION_JSON)
    public Response<List<DataSet>> all() {
        List<DataSet> result = new ArrayList<DataSet>();

        long roundedTime = MapDataCache.roundedTimestamp();
        Licence licence = Licence.get(requestProvider.get(), responseProvider.get(), true);

        synchronized (cache) {
            if (!cache.exists(roundedTime)) {
                cache.add(roundedTime);
            }
        }

        for (String fleetId : mapProviders.keySet()) {
            String fleetLicence = fleetProviders.get(fleetId).getLicence(fleetId);
            if (fleetLicence == null || licence.hasResourceAccess(fleetLicence)) {
                result.addAll(cache.get(roundedTime, fleetId));
            }
        }

        return Response.of(dataLicensor.getLicenced(result));
    }

    @GET
    @LoggingData
    @Path("/data/{timestamp: [0-9]+}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response<List<DataSet>> all(@PathParam("timestamp") long timestamp) {
        List<DataSet> timedResult = new ArrayList<DataSet>();
        List<DataSet> liveResult = this.all().getData();
        Licence licence = Licence.get(requestProvider.get(), responseProvider.get(), true);

        for (String fleetId : mapProviders.keySet()) {

            String fleetLicence = fleetProviders.get(fleetId).getLicence(fleetId);

            if (fleetLicence == null || licence.hasResourceAccess(fleetLicence)) {
                timedResult.addAll(mapProviders.get(fleetId).getHistoricFleetData(timestamp));
            }
        }

        timedResult = joinData(filterDataByTime(liveResult, timestamp), timedResult);

        return Response.of(dataLicensor.getLicenced(timedResult));
    }

    @GET
    @LoggingData
    @Path("/markers/{providerId}/{latNW}/{longNW}/{latSE}/{longSE}/{zoom}/{data}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<?> getMarkers(@PathParam("providerId") String providerId, @PathParam("latNW") Double latNW,
            @PathParam("longNW") Double longNW, @PathParam("latSE") Double latSE, @PathParam("longSE") Double longSE,
            @PathParam("zoom") Integer zoom, @PathParam("data") String data) {
        List<Object> result = new ArrayList<Object>();
        Licence licence = Licence.get(requestProvider.get(), responseProvider.get(), true);
        Coordinate nw = new Coordinate(latNW, longNW);
        Coordinate se = new Coordinate(latSE, longSE);

        result.addAll(mapMarkerProvider.getMarkerData(nw, se, zoom, data, providerId, licence));

        return result;
    }
    
    @GET
    @LoggingData
    @Path("/exportMarkers")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public javax.ws.rs.core.Response downloadWspCsvReport(
            @Context HttpServletResponse response,
            @QueryParam("markerType") String markerType,
            @QueryParam("leftTopLatitude") Double leftTopLatitude,
            @QueryParam("leftTopLongitude") Double leftTopLongitude,
            @QueryParam("rightBottomLatitude") Double rightBottomLatitude,
            @QueryParam("rightBottomLongitude") Double rightBottomLongitude,
            @QueryParam("category") List<String> categories
            ) throws IOException {

        Licence licence = Licence.get(requestProvider.get(), responseProvider.get(),  true);
        
        // GENERATING REPORT
        List<Integer> categoriesList = new ArrayList<Integer>();
        for(String cat : categories) {
            categoriesList.add(new Integer(cat));
        }
        
        List<String[]> tableReport = mapMarkerProvider.generateExportReport(
                new Coordinate(leftTopLatitude, leftTopLongitude), 
                new Coordinate(rightBottomLatitude, rightBottomLongitude), 
                categoriesList, markerType, licence);
        
        // SENDING THE REPORT BACK AS CSV
        response.setContentType("text/csv");
        response.addHeader("Content-disposition",
                String.format("attachment; filename=wsp-export-%1$tY%1$tm%1$td_%1$tH%1$tM%1$tS.csv", new Date()));
        
        OutputStream outputStream = response.getOutputStream();
        printAsCsv(tableReport, outputStream);
        outputStream.close();

        return javax.ws.rs.core.Response.ok().build();
    }

    @GET
    @TimeoutFree
    @LoggingData
    @Path("/data/{fleetId}/{unitId: [0-9]+}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response<DataSet> unit(@PathParam("fleetId") String fleetId, @PathParam("unitId") int unitId) {
        return Response.of(mapProviders.get(fleetId).unit(unitId, null));
    }

    @GET
    @LoggingData
    @Path("/data/{fleetId}/{unitId: [0-9]+}/{timestamp: [0-9]+}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response<DataSet> unit(@PathParam("unitId") int unitId, @PathParam("fleetId") String fleetId,
            @PathParam("timestamp") long timestamp) {
        return Response.of(mapProviders.get(fleetId).unit(unitId, timestamp));
    }

    private List<DataSet> filterDataByTime(List<DataSet> data, long timestamp) {
        List<DataSet> filteredData = new ArrayList<DataSet>();

        for (DataSet ds : data) {
            Channel<?> timestampChannel = ds.getChannels().getByName(Spectrum.TIMESTAMP);
            if ((timestampChannel != null) && (timestampChannel.getValue() != null)) {
                if (((ChannelLong) timestampChannel).getValue() < timestamp) {
                    filteredData.add(ds);
                }
            }
        }

        return filteredData;
    }

    private List<DataSet> joinData(List<DataSet> liveData, List<DataSet> timedData) {
        Set<DataSet> joinedData = new HashSet<DataSet>(timedData); // To avoid duplicates

        Map<String, DataSet> timedUnitFormationMap = DataUtils.getFormationByStockMap(timedData);
        Map<String, DataSet> liveUnitFormationMap = DataUtils.getFormationByStockMap(liveData);

        Set<String> liveUnits = liveUnitFormationMap.keySet();
        liveUnits.removeAll(timedUnitFormationMap.keySet());
        for (String unit : liveUnits) {
            DataSet formation = liveUnitFormationMap.get(unit);

            if (formation != null) {
                joinedData.add(formation);
            }
        }

        return new ArrayList<DataSet>(joinedData);
    }

    private void printAsCsv(List<String[]> csvRows, OutputStream stream) {
        PrintWriter writer = new PrintWriter(stream);
        CSVWriter csvWriter = new CSVWriter(writer);

        csvWriter.writeAll(csvRows);

        try {
            csvWriter.close();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}