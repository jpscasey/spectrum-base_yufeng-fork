package com.nexala.spectrum.rest.data;

import java.util.List;

import com.nexala.spectrum.db.beans.ChartVehicleType;

public interface ChartVehicleTypeProvider {
    
    /**
     * Return all {@link ChartVehicleType} .
     * @return all {@link ChartVehicleType} .
     */
    List<ChartVehicleType> getAllChartVehicleTypes();

}