package com.nexala.spectrum.rest.data.beans;

import java.util.List;
import java.util.Map;

import com.nexala.spectrum.db.beans.Unit;

public class UnitDetail {
    private Unit unit;
    private DataSet infoData;
    private Map<Integer, Diagram> diagramData;
    private List<StockDesc> tabs;
    private List<Event> live;
    private List<Event> recent;

    public static UnitDetail get() {
        return new UnitDetail();
    }

    public void setUnit(Unit unit) {
        this.unit = unit;
    }

    public Unit getUnit() {
        return unit;
    }

    public DataSet getInfoData() {
        return infoData;
    }

    public void setInfoData(DataSet infoData) {
        this.infoData = infoData;
    }

    public Map<Integer, Diagram> getDiagramData() {
        return diagramData;
    }

    public void setDiagramData(Map<Integer, Diagram> diagramData) {
        this.diagramData = diagramData;
    }

    public List<StockDesc> getTabs() {
        return tabs;
    }

    public void setTabs(List<StockDesc> tabs) {
        this.tabs = tabs;
    }

    /**
     * Getter for live.
     * @return the live
     */
    public List<Event> getLive() {
        return live;
    }

    /**
     * Setter for live.
     * @param live the live to set
     */
    public void setLive(List<Event> live) {
        this.live = live;
    }

    /**
     * Getter for recent.
     * @return the recent
     */
    public List<Event> getRecent() {
        return recent;
    }

    /**
     * Setter for recent.
     * @param recent the recent to set
     */
    public void setRecent(List<Event> recent) {
        this.recent = recent;
    }
}
