package com.nexala.spectrum.rest.data.beans;

/**
 * Bean for event severity.
 * @author brice
 *
 */
public class EventSeverity {
    
    private String label = null;
    
    private String code = null;

    /**
     * Getter for label.
     * @return the label
     */
    public String getLabel() {
        return label;
    }

    /**
     * Setter for label.
     * @param label the label to set
     */
    public void setLabel(String label) {
        this.label = label;
    }

    /**
     * Getter for code.
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * Setter for code.
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }
    
}
