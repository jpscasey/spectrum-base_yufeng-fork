package com.nexala.spectrum.rest.data.beans;

import java.util.HashMap;
import java.util.Map;

/**
 * Bean for a generic object
 * @author Fulvio
 */
public class GenericBean {

    protected Map<String, Object> fields = new HashMap<String, Object>();

    public Map<String, Object> getFields() {
        return fields;
    }

    public Object getField(String key) {
        return fields.get(key);
    }

    public void setField(String key, Object value) {
        this.fields.put(key, value);
    }
}
