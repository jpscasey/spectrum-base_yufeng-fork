package com.nexala.spectrum.rest.service;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.nexala.spectrum.analysis.bean.AnalysisDataMap;
import com.nexala.spectrum.analysis.bean.ChannelManagerConfiguration;
import com.nexala.spectrum.analysis.bean.EventReference;
import com.nexala.spectrum.analysis.bean.GenericEvent;
import com.nexala.spectrum.analysis.data.AnalysisDataProvider;
import com.nexala.spectrum.analysis.manager.ChannelManager;
import com.nexala.spectrum.configuration.SystemConfiguration;
import com.nexala.spectrum.db.beans.FaultMetaDto;
import com.nexala.spectrum.rest.data.FaultMetaProvider;
import com.nexala.spectrum.view.conf.CommonFleetConfiguration;

/**
 * Services for the stream analysis engine.
 * 
 * @author BBaudry
 *
 */
@Path("/railstreamdata")
@Singleton
public class RailStreamDataService {
    
    private final static Logger LOGGER = Logger.getLogger(RailStreamDataService.class);

    @Inject
    private Map<String, AnalysisDataProvider<GenericEvent>> providers;
    
    @Inject
    private Map<String, FaultMetaProvider> faultMetaProviderMap;

    @Inject
    CommonFleetConfiguration fleetConf;
    
    @Inject
    SystemConfiguration systemConfig;
        
    /**
     * Return the stream analysis configuration as JSON.
     * 
     * @param fleetId the fleet id
     * @return the configuration as JSON
     */
    @GET
    @Path("/getConfig/{fleetId}")
    @Produces(MediaType.APPLICATION_JSON)
    public String getConfiguration(@PathParam("fleetId") String fleetId) {
        return providers.get(fleetId).getStreamAnalysisConfiguration();
    }
    
    /**
     * Return a map of activated event codes by sourceId.
     * Used by the rule engine when faults can be deactivated manually (outside of rule engine),
     * or when the faults shouldn't be retracted at startup.
     * 
     * @param fleetId the fleet id
     * @return the map of activated events by sourceId 
     */
    @GET
    @Path("/getActivatedEvents/{fleetId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Map<Integer, List<EventReference>> getActivatedEvents(@PathParam("fleetId") String fleetId) {
        return providers.get(fleetId).getActivatedEvents();
    }

    /**
     * Return the data received after the id passed in parameter. Data is returned in chunks i.e if too much data is
     * available only a part of it will be returned
     * 
     * @param fleetId the fleet id
     * @param recordIds id of the last data processed. If the data come from multiple sources multiple id can be passed
     *            separated by comma.
     * @return the data
     * @throws IOException in case of exception
     */
    @GET
    @Path("/getData/{fleetId}/{ids}")
    @Produces(MediaType.APPLICATION_JSON)
    public AnalysisDataMap<?> getData(@PathParam("fleetId") String fleetId, @PathParam("ids") String recordIds) {
        List<Long> lastRecordsId = convertStringToLongs(recordIds);

        return providers.get(fleetId).getData(lastRecordsId);
    }
    
    /**
     * Return test data for the rule engine. Test data is the data for one stock on a limited amount of time
     * 
     * @param fleetId the fleet id
     * @param stockId the stock id
     * @param startDate smaller timestamp we should retriever
     * @param endDate older timestamp we should retrieve
     * @return the test data
     * @throws IOException in case of exception
     */
    @GET
    @Path("/getTestData/{fleetId}/{stockId: [0-9]+}/{startDate: [0-9]+}/{endDate: [0-9]+}")
    @Produces(MediaType.APPLICATION_JSON)
    public Map<Integer, List<GenericEvent>> getTestData(@PathParam("fleetId") String fleetId,
            @PathParam("stockId") Integer stockId, @PathParam("startDate") long startDate,
            @PathParam("endDate") long endDate) {

        return providers.get(fleetId).getData(stockId, startDate, endDate);
    }

    /**
     * Return the last available data for the stream analysis engine. Need to be called only when the last processed id
     * from the stream analysis engine is unknow
     * 
     * @param fleetId the fleet id.
     * @return the data for the stream analysis engine
     */
    @GET
    @Path("/getData/{fleetId}")
    @Produces(MediaType.APPLICATION_JSON)
    public AnalysisDataMap<?> getData(@PathParam("fleetId") String fleetId) {
        AnalysisDataMap<?> data = providers.get(fleetId).getData(Collections.<Long> emptyList());

        return data;
    }

    /**
     * Return the channel configuration for a fleet. THis should be used to set up a {@link ChannelManager}
     * 
     * @param fleetId the fleet id.
     * @return the list for channels that the rule engine is supporting
     * @throws IOException
     */
    @GET
    @Path("/getChannelsConfiguration/{fleetId}")
    @Produces(MediaType.APPLICATION_JSON)
    public ChannelManagerConfiguration getChannelConfiguration(@PathParam("fleetId") String fleetId) {
        ChannelManagerConfiguration result = providers.get(fleetId).getChannelManagerConfiguration();

        return result;
    }

    /**
     * Save the FaultMeta including FaultMetaExtraFields for each fleet.
     * 
     * @param faultFields, Fields to be saved to FaultMeta and FaultMetaExtraFields
     * @return faultMetaId, the ID for the current FaultMeta being saved
     */
    @POST
    @Path("/saveFaultConfiguration")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Integer saveFaultConfiguration(Map<String, Object> faultMetaJson) {
        Integer faultMetaId = null;
        
        // Convert JSON body to a FaultMeta object
        ObjectMapper oMapper = new ObjectMapper();
        FaultMetaDto faultMetaDto = oMapper.convertValue(faultMetaJson.get("fleets"), FaultMetaDto.class);
        
        boolean newFault = faultMetaDto.getId() == null ? true : false;
        if (newFault) {
            // If the Fault is new, we want to use the same ID accross all the providers
            Integer newFaultId = null;
            for (FaultMetaProvider faultMetaProvider : faultMetaProviderMap.values()) {
                faultMetaId = faultMetaProvider.createFaultMeta(faultMetaDto, newFaultId);
                newFaultId = faultMetaId;
            }
        } else {
            // Update the fault if it has an ID already
            for (FaultMetaProvider faultMetaProvider : faultMetaProviderMap.values()) {
                faultMetaId = faultMetaProvider.updateFaultMeta(faultMetaDto);
            }
        }
        
        return faultMetaId;
    }
    
    /**
     * Delete the FaultMeta including FaultMetaExtraFields for each fleet.
     * 
     * @param faultFields, Fields to be saved to FaultMeta and FaultMetaExtraFields
     * @return faultMetaId, the ID for the current FaultMeta being deleted
     */
    @POST
    @Path("/deleteFaultConfiguration")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Integer deleteFaultConfiguration(Map<String, Object> faultMetaJson) {
        Integer faultMetaId = null;
        
        // Convert JSON body to a FaultMeta object
        ObjectMapper oMapper = new ObjectMapper();
        FaultMetaDto faultMetaDto = oMapper.convertValue(faultMetaJson.get("fleets"), FaultMetaDto.class);
        
        List<String> databaseNames = systemConfig.getDatabaseNames();
        List<String> queries = new ArrayList<>();
        List<Object> parameters = new ArrayList<>();
        
        if (databaseNames.size() == 0) {
        	//TODO: Log this error
        	return faultMetaId;
        }
        
        FaultMetaProvider provider = (FaultMetaProvider)faultMetaProviderMap.values().toArray()[0];
        for (String databaseName : databaseNames) {
        	provider.fillDeleteFaultMetaQueryAndParam(faultMetaDto, queries, parameters);
        	
        	for (int i = 0;i < queries.size();i++) {
        		String query = queries.get(i);
        		queries.set(i, query.replaceAll("DB_NAME", databaseName));
        	}
        }
        
        int result = provider.deleteFaultMetaWithTransaction(queries, parameters);
        
        return result;
    }
    
    /**
     * Create a fault that have been fired by the stream analysis engine.
     * 
     * @param fleetId the fleet id
     * @param eventCode the event code
     * @param timestamp the timestamp
     * @param sourceId the source id
     * @return id of the fault created
     * @throws IOException
     */
    @POST
    @Path("/createFault/{fleetId}/{eventCode}/{timestamp: [0-9]+}/{sourceId: [0-9]+}/{ruleName}/{contextId}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Long createFault(@PathParam("fleetId") String fleetId, @PathParam("eventCode") String eventCode,
            @PathParam("timestamp") Long timestamp, @PathParam("sourceId") Integer sourceId,
            @PathParam("ruleName") String ruleName, @PathParam("contextId") String contextId,
            Map<String, String> extraFields) {
    	
    	// Decode the ruleName parameter as it contains escaped backslashes
    	String decodedName = null;
    	String decodedEventCode = null;
    	try {
			 decodedName = URLDecoder.decode(ruleName, "UTF-8");
			 decodedEventCode = URLDecoder.decode(eventCode, "UTF-8");
		} catch (UnsupportedEncodingException e) {
		    LOGGER.error("Unable to decode rule name in createFault", e);
		}

        return providers.get(fleetId).createFault(decodedEventCode, timestamp, sourceId, decodedName, contextId, fleetId, extraFields);

    }

    /**
     * Retract a fault that have been fired by the stream analysis engine
     * @param fleetId the fleet id
     * @param faultId the fault id
     * @return true if the fault was retracted correctly, false otherwise
     */
    @GET
    @Path("/retractFault/{fleetId}/{faultId}/{timestamp: [0-9]+}")
    @Produces(MediaType.APPLICATION_JSON)
    public Boolean retractFault(@PathParam("fleetId") String fleetId, @PathParam("faultId") Long faultId,
            @PathParam("timestamp") Long timestamp) {
        // Call the provider, in case of exception the return code will be 500
        providers.get(fleetId).retractFault(faultId, timestamp);

        return true;
    }
    
    /**
     * Retract a fault that have been fired by the strean analysis engine
     * @param fleetId the fleet id
     * @param faultId the fault id
     * @return true if the fault was retracted correctly, false otherwise
     */
    @GET
    @Path("/retractAllFaults/{fleetId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Boolean retractAllFaults(@PathParam("fleetId") String fleetId) {
        // Call the provider, in case of exception the return code will be 500
        providers.get(fleetId).retractFaults();

        return true;
    }

    /**
     * Send an email after a fault fired.
     * @param fleetId the fleet id
     * @param faultId the fault id
     * @param emailPath the email path
     * @return true if the email was sent, false otherwise
     */
    @GET
    @Path("/faultEmail/{fleetId}/{faultId: [0-9]+}/{emailPath}")
    @Produces(MediaType.APPLICATION_JSON)
    public Boolean sendFaultEmail(@PathParam("fleetId") String fleetId, @PathParam("faultId") Long faultId,
            @PathParam("emailPath") String emailPath) {
        // Call the provider, in case of exception the return code will be 500
        providers.get(fleetId).sendFaultEmail(faultId, emailPath);

        return true;
    }

    /**
     * Update the ids of the last item processed by the stream analysis engine
     * @param fleetId the fleet id
     * @param configurationName name of the configuration
     * @param recordIds the list of ids
     */
    @GET
    @Path("/updateCurrentIds/{fleetId}/{configurationName}/{ids}")
    @Produces(MediaType.APPLICATION_JSON)
    public Boolean updateCurrentIds(@PathParam("fleetId") String fleetId,
            @PathParam("configurationName") String configurationName, @PathParam("ids") String recordIds) {
        
        List<Long> list = convertStringToLongs(recordIds);
        providers.get(fleetId).updateCurrentIds(configurationName, list);
        
        return true;
    }
    
    /**
     * Return the last items processed by the stream analysis engine
     * @param fleetId the fleet id
     * @param configurationName the configuration name
     * @return id of the last item processed (multiple ids if from multiple sources)
     */
    @GET
    @Path("/getCurrentIds/{fleetId}/{configurationName}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Long> getCurrentIds(@PathParam("fleetId") String fleetId,
            @PathParam("configurationName") String configurationName) {
        
        return providers.get(fleetId).getCurrentIds(configurationName);
    }
    
    /**
     * Convert the string in parameter in a list of longs
     * @param str the string to convert
     * @return the list of longs
     */
    private List<Long> convertStringToLongs(String str) {
        String[] split = str.split(",");
        List<Long> lastRecordsId = new ArrayList<Long>();
        for (String current : split) {
            if ("null".equals(current)) {
                lastRecordsId.add(null);
            } else {
                lastRecordsId.add(Long.valueOf(current));
            }
        }
        return lastRecordsId;
    }

}
