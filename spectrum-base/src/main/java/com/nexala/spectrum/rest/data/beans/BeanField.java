package com.nexala.spectrum.rest.data.beans;

/**
 * Bean for a generic bean field
 * @author Fulvio
 */
public class BeanField {
    
    private String id;

    private String dbName;

    private String type;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDbName() {
        return dbName;
    }

    public void setDbName(String dbName) {
        this.dbName = dbName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
