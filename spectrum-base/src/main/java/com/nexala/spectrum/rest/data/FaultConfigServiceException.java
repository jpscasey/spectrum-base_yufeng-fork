package com.nexala.spectrum.rest.data;

public class FaultConfigServiceException extends RuntimeException {

    /**
     * 
     */
    private static final long serialVersionUID = -7362170098412813901L;

    public FaultConfigServiceException() {}
    
    public FaultConfigServiceException(String message) {
        super(message);
    }
    
    public FaultConfigServiceException(Exception e) {
        super(e);
    }
}