/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.rest.data;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.SpectrumProperties;
import com.nexala.spectrum.channel.ChannelCollection;
import com.nexala.spectrum.channel.ChannelString;
import com.nexala.spectrum.licensing.DataLicensor;
import com.nexala.spectrum.rest.data.beans.DataSet;
import com.nexala.spectrum.rest.data.beans.DataSetImpl;
import com.nexala.spectrum.view.conf.CommonFleetConfiguration;


/**
 * Utils to cache map data every {@link MapDataCache#cacheInterval}.
 * 
 * @author poriordan
 */
@Singleton
public class MapDataCache {
    @Inject
    private DataLicensor dataLicensor;

    @Inject
    private Map<String, MapProvider> mapProvider;

    public static int cacheInterval;
    public static long expirationTime;
    
    private Timer timer = null;

    private Map<Long,Map<String, List<DataSet>>> mapCache = new ConcurrentHashMap<Long, Map<String, List<DataSet>>>();
    private ListMultimap<String, String> fleetsPerDatabase = ArrayListMultimap.create();

    @Inject
    public MapDataCache(SpectrumProperties properties, CommonFleetConfiguration fleetConf) {
        cacheInterval = Integer.parseInt(properties.get("spectrum.refresh.mapcache.interval", "2000"));
        expirationTime = Long.parseLong(properties.get("spectrum.refresh.mapcacheexpiration.interval", "15000"));

        timer = new Timer();
        timer.scheduleAtFixedRate(new ExpireCacheTask(), 0, expirationTime);

        fleetsPerDatabase = fleetConf.getFleetsPerDatabase();
    }

    /**
     * Return true if values for this fleet and this time are cached.
     * @param timestamp timestamp provided by {@link MapDataCache#roundedTimestamp()}
     * @param fleetId the fleet
     * @return true if fleet data are cached for this time
     */
    public boolean exists(long timestamp) {
        return mapCache.get(timestamp) != null;
    }

    /**
     * Return data for this time and fleet.
     * @param timestamp timestamp provided by {@link MapDataCache#roundedTimestamp()}
     * @param fleetId the fleet id
     * @return list of datas
     */
    public List<DataSet> get(long timestamp, String fleetId) {
        List<DataSet> cachedData = new ArrayList<DataSet>();

        Map<String, List<DataSet>> cacheForTimestamp = mapCache.get(timestamp);
        if (cacheForTimestamp != null) {
            List<DataSet> cacheForFleet = cacheForTimestamp.get(fleetId);
            if (cacheForFleet != null) {
                cachedData = cacheForFleet;
            } 
        } 

        return dataLicensor.getLicenced(cachedData);
    }

    /**
     * Add data in the cache for this timestamp and fleet.
     * @param timestamp timestamp provided by {@link MapDataCache#roundedTimestamp()}
     * @param fleetId the fleet id
     * @param vehicles the datas
     */
    public void add(long timestamp) {
        
        ListMultimap<String, DataSet> temp = ArrayListMultimap.create();
        Map<String, List<DataSet>> map = mapCache.get(timestamp);
        
        if (map == null) {
            map = new ConcurrentHashMap<String, List<DataSet>>();
            mapCache.put(timestamp, map);
        }
        
        for (String database : fleetsPerDatabase.keySet()) {
            String fleet = fleetsPerDatabase.get(database).get(0);
            List<DataSet> data = mapProvider.get(fleet).getLiveData();

            if (data != null) {
                for (DataSet d : data){
                    ChannelCollection channels = d.getChannels();
                    String vehicleId = channels.getByName(Spectrum.VEHICLE_ID).getValue().toString();
                    for (int i = 0; i < fleetsPerDatabase.get(database).size(); i++) {
                        String fleetCode = fleetsPerDatabase.get(database).get(i);
                        if (channels.getByName(Spectrum.FLEET_ID) != null && channels
                                .getByName(Spectrum.FLEET_ID, ChannelString.class).getValue().equals(fleetCode)) {
                            temp.put(fleetCode, new DataSetImpl(Integer.parseInt(vehicleId), channels));
                        }  
                    }
                }
            }

            ListMultimap<String, DataSet> result = ArrayListMultimap.create(temp);
            for (String key : result.keySet()) {
                map.put(key, result.get(key));
            }
        }
    }

    
    /**
     * Round down the timestamp using {@link MapDataCache#cacheInterval}.
     * @return timestamp round down
     */
    public static long roundedTimestamp() {
        long timestamp = System.currentTimeMillis();
        return timestamp - (timestamp % cacheInterval);
    }
    
    
    /**
     * Delete cached values before {@link MapDataCache#cacheInterval}
     * @param timestamp timestamp
     */
    private class ExpireCacheTask extends TimerTask {

        @Override
        public void run() {
            long timestamp = MapDataCache.roundedTimestamp();
            
            timestamp = MapDataCache.roundedTimestamp();
            // Don't allow other thread to access the map during deletion
            synchronized (mapCache) {
                Iterator<Long> iter = mapCache.keySet().iterator();

                while (iter.hasNext()) {
                    long cacheTimestamp = iter.next();

                    if (timestamp - expirationTime > cacheTimestamp) {
                        iter.remove();
                    }
                }
            }
        }

    }
}
