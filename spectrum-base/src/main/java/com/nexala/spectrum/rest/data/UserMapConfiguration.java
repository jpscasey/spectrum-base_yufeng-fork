package com.nexala.spectrum.rest.data;

import com.fasterxml.jackson.annotation.JsonUnwrapped;
import com.nexala.spectrum.view.conf.MapConfiguration;

/**
 * Bean holding the Map Configuration with the user configuration.
 * @author BBaudry
 *
 */
public class UserMapConfiguration {
    
    @JsonUnwrapped
    private final String userConfiguration;
    
    @JsonUnwrapped
    private final MapConfiguration mapConfiguration;
    
    
    /**
     * @param userConfiguration
     * @param mapConfiguration
     */
    public UserMapConfiguration(String userConfiguration, MapConfiguration mapConfiguration) {
        this.userConfiguration = userConfiguration;
        this.mapConfiguration = mapConfiguration;
    }
    
}
