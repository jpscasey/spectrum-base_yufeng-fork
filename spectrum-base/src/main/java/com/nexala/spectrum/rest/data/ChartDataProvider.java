package com.nexala.spectrum.rest.data;

import java.util.List;

import com.nexala.spectrum.configuration.ChannelConfiguration;
import com.nexala.spectrum.db.beans.ChannelConfig;
import com.nexala.spectrum.db.dao.DataAccessException;
import com.nexala.spectrum.db.dao.charting.ChartChannel;
import com.nexala.spectrum.db.dao.charting.DataViewConfiguration;
import com.nexala.spectrum.licensing.Licence;

public interface ChartDataProvider {
    
    List<ChartChannel> findChannels(long chartId);
    
    ChannelConfiguration getChannelConfiguration();
    
    List<DataViewConfiguration> findUserConfigurations(Licence licence);
    
    List<DataViewConfiguration> findSystemConfigurations();
    
    DataViewConfiguration findChart(Long chartId, String chartName, String userId);
    
    DataViewConfiguration createChart(String name, List<Integer> channelIds, String userId) throws DataAccessException;
    
    void saveChart(Long chartId, List<Integer> channelIds, String userId) throws DataAccessException;
    
    void deleteChart(Long configurationId, String userId) throws DataAccessException;
    
    void renameChart(Long chartId, String newName, String userId) throws DataAccessException;
    
    List<ChannelConfig> getConfigurations(List<Integer> channelIds);
    
    List<ChannelConfig> getConfigurations(List<Integer> channelIds, Integer vehicleId);
}
