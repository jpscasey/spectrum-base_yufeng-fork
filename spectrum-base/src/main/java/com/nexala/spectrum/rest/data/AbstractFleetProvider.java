package com.nexala.spectrum.rest.data;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.google.inject.Inject;
import com.nexala.spectrum.configuration.ApplicationConfiguration;
import com.nexala.spectrum.configuration.ChannelConfiguration;
import com.nexala.spectrum.db.beans.ChannelConfig;
import com.nexala.spectrum.db.beans.ChannelGroupConfig;
import com.nexala.spectrum.db.beans.UserConfiguration;
import com.nexala.spectrum.db.dao.DataAccessException;
import com.nexala.spectrum.db.dao.UnitSearchDao;
import com.nexala.spectrum.licensing.Licence;
import com.nexala.spectrum.rest.data.beans.DataSet;
import com.nexala.spectrum.rest.data.beans.DrillDown;
import com.nexala.spectrum.rest.data.beans.Formation;
import com.nexala.spectrum.rest.data.beans.GenericBean;
import com.nexala.spectrum.validation.ChannelValidator;
import com.nexala.spectrum.view.conf.DrillDownConfiguration;
import com.typesafe.config.Config;

public abstract class AbstractFleetProvider implements FleetProvider {

    private ChannelConfiguration channelConfiguration;

    private ChannelDataProvider channelDataProvider;

    protected ChannelValidator channelValidator;
    
    private UnitProvider unitProvider;
    
    private DrillDownConfiguration configuration;
    
    private ApplicationConfiguration applicationConfiguration;
    
    private UserConfigurationProvider userConfigProvider;
    
	@Inject
    private UnitSearchDao searchDao;

    public AbstractFleetProvider(ChannelConfiguration channelConfiguration, ChannelDataProvider channelDataProvider,
            ChannelValidator channelValidator, UnitProvider unitProvider, DrillDownConfiguration configuration,
            ApplicationConfiguration applicationConfiguration, UserConfigurationProvider userConfigProvider) {
        this.channelConfiguration = channelConfiguration;
        this.channelDataProvider = channelDataProvider;
        this.channelValidator = channelValidator;
        this.unitProvider = unitProvider;
        this.configuration = configuration;
        this.applicationConfiguration = applicationConfiguration;
        this.userConfigProvider = userConfigProvider;
    }

    /**
     * @param config
     * @param fleetId
     * @param unitIds FIXME
     * @param group
     * @return
     */
    public DrillDown getGroupData(boolean config, String fleetId, String unitIds, String group) {
        DrillDown drillDown = DrillDown.get();

        ChannelGroupConfig groupConfig = channelConfiguration.getGroup(group);
        if (groupConfig != null) {
            if (config) {
                drillDown.setChannelNameList(getChannelDescriptions(groupConfig));
                List<Formation> formationList = unitProvider.formation(unitIds);
                drillDown.setColumns(configuration.getColumns(fleetId, formationList));
                drillDown.setActiveVehicle(channelDataProvider.getActiveVehicle(unitIds));
            }
            
            List<Integer> channelIds = getChannelIds(groupConfig);

            /**
             * As the drilldown only queries specific channels, the dependency channels necessary for channel validation
             * must be included in the query
             */
            Set<ChannelConfig> dependencies = channelValidator.getChannelDependencies(
                    getChannelConfigs(groupConfig));

            for (ChannelConfig dependencyConfig : dependencies) {
                Integer channelId = dependencyConfig.getId();
                if (channelId != null && !channelIds.contains(channelId)) {
                    channelIds.add(channelId);
                }
            }

            if (drillDown.getChannelNameList().size() > 0) {

                List<DataSet> data = channelDataProvider.getLiveChannelData(channelIds, unitIds);

                drillDown.setData(data);
            }
        }
        return drillDown;
    }
    
    
    /**
     * Return the list of channel descriptions in this group.
     * Can be filtered to return fleet dependant channel list
     * @param groupConfig the group config
     * @return list of channel description
     */
    protected List<String> getChannelDescriptions(ChannelGroupConfig groupConfig) {
    	
    	List<ChannelConfig> channelList = groupConfig.getChannelConfigList();
    	List<String> result = new ArrayList<String>();

        for (ChannelConfig channel : channelList) {
            if (channel.isAlwaysDisplayed()) {
                result.add(channel.getDescription());
            }
        }
        return result;
    }
    
    /**
     * Return the list of channel ids.
     * Can be filtered to return fleet dependant channel list
     * @param groupConfig the group config
     * @return list of channel id
     */
    protected List<Integer> getChannelIds(ChannelGroupConfig groupConfig) {

        List<ChannelConfig> channelList = groupConfig.getChannelConfigList();
        List<Integer> result = new ArrayList<Integer>();

        for (ChannelConfig channel : channelList) {
            if (channel.isAlwaysDisplayed()) {
                result.add(channel.getId());
            }
        }
        return result;
    }
    
    /**
     * Return the list of channel configurations.
     * Can be filtered to return fleet dependant channel configurations list
     * @param groupConfig the group config
     * @return list of channel configurations
     */
    protected List<ChannelConfig> getChannelConfigs(ChannelGroupConfig groupConfig) {
        return groupConfig.getChannelConfigList();
    }
    
    public String getLicence(String fleetId) {
        Config fleetConfig = applicationConfiguration.get().getConfig("r2m.fleets." + fleetId);
        String licence = null;

        if (fleetConfig.hasPath("licence")) {
            licence = fleetConfig.getString("licence");
        }

        return licence;
    }
    
    @Override
    public UserConfiguration getUserConfiguration(Licence licence, String fleetId) {
        return userConfigProvider.getUserConfiguration(licence.getLoginUser().getLogin(), "fleetConf"+fleetId);
    }
    
    @Override
    public List<GenericBean> searchUnitsByString(String fleetId, String substr) throws DataAccessException {
        return searchDao.getCreateFaultFleetUnitsByString(fleetId, substr); 
    }
}
