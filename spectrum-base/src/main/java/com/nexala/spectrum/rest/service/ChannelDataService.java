/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.rest.service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.nexala.spectrum.licensing.Licence;
import com.nexala.spectrum.licensing.LoggingData;
import com.nexala.spectrum.rest.data.ChannelDataProvider;
import com.nexala.spectrum.rest.data.GroupProvider;
import com.nexala.spectrum.rest.data.UnitProvider;
import com.nexala.spectrum.rest.data.beans.ChannelData;
import com.nexala.spectrum.rest.data.beans.DataSet;
import com.nexala.spectrum.rest.data.beans.StockDesc;
import com.nexala.spectrum.rest.data.beans.channeldata.ChannelsGroup;
import com.nexala.spectrum.rest.security.TimeoutFree;

@Path("/channeldata")
@Singleton
public class ChannelDataService {
    @Inject
    private Map<String, GroupProvider> groupProviders;

    @Inject
    private Map<String, UnitProvider> unitProviders;

    @Inject
    private Map<String, ChannelDataProvider> channelDataProviders;

    @GET
    @LoggingData
    @TimeoutFree
    @Path("/unit/{fleetId}/{stockId}/{timestamp}/{direction}/{groupId}/{allVisible}")
    @Produces(MediaType.APPLICATION_JSON)
    public ChannelData unit(@PathParam("fleetId") String fleetId, @PathParam("stockId") int stockId,
            @PathParam("timestamp") long timestamp, @PathParam("direction") int direction,
            @PathParam("groupId") int groupId, @PathParam("allVisible") boolean allVisible,
            @Context HttpServletRequest request, @Context HttpServletResponse response) {

        List<DataSet> data = channelDataProviders.get(fleetId).getUnitData(stockId, timestamp, direction);

        Licence licence = Licence.get(request, response, true);

        return groupProviders.get(fleetId).getChannelData(data, groupId, allVisible, licence);
    }

    @GET
    @LoggingData
    @Path("/groupdata/{fleetId}/{groupId}/{stockId}")
    @Produces(MediaType.APPLICATION_JSON)
    public ChannelsGroup getGroupData(@PathParam("fleetId") String fleetId, @PathParam("groupId") int groupId,
            @PathParam("stockId") int stockId, @Context HttpServletRequest request, @Context HttpServletResponse response) {

        List<StockDesc> stock = unitProviders.get(fleetId).getStockForUnit(stockId);

        Licence licence = Licence.get(request, response, true);

        return groupProviders.get(fleetId).getGroupData(groupId, stock, licence);
    }

}
