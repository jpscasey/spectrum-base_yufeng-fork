package com.nexala.spectrum.rest.data;

import java.util.ArrayList;
import java.util.List;

import com.nexala.spectrum.rest.data.beans.route.Route;
import com.nexala.spectrum.rest.data.beans.route.RouteView;

public class DefaultRouteMapProvider implements RouteMapProvider {
    @Override
    public RouteView getRouteView(String id) {
        return new RouteView();
    }

    @Override
    public List<Route> getAllRoutes() {
        return new ArrayList<Route>();
    }
}
