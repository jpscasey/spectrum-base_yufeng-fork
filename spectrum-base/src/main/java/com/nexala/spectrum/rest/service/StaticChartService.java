package com.nexala.spectrum.rest.service;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.nexala.spectrum.charting.StaticChartConfig;
import com.nexala.spectrum.db.beans.ChartVehicleType;
import com.nexala.spectrum.licensing.LoggingData;
import com.nexala.spectrum.rest.data.ChartVehicleTypeProvider;
import com.nexala.spectrum.rest.data.StaticChartProvider;
import com.nexala.spectrum.rest.security.TimeoutFree;
import com.nexala.spectrum.service.bean.StaticChartsConfigBean;

@Path("/staticchart/")
@Singleton
public class StaticChartService {

    public static final int HEIGHT = 240;
    public static final int WIDTH = 1250;

    
    
    @Inject
    private Map<String, ChartVehicleTypeProvider> chartVhclTypeProvider;
    
    @Inject
    private Map<String, StaticChartProvider> staticChartProviderMap;
    
    @GET
    @TimeoutFree
    @Path("/compute/{fleetId}/{vehicleId: [0-9]+}/{interval: [0-9]+}/{endtime: [0-9]*}/{timezone}/{chartId}")
    @Produces("image/png")
    @LoggingData
    public Response computeDiagrams(
            @PathParam("fleetId") String fleetId,
            @PathParam("vehicleId") int vehicleId,
            @PathParam("interval") int interval,
            @PathParam("endtime") String endTimeParam,
            @PathParam("timezone") String timezone,
            @PathParam("chartId") Integer chartId,
            @QueryParam("height") Integer heightP) {
        
        Long endTime = null;
        Integer height = heightP;
        
        if (endTimeParam != null && endTimeParam.matches("[0-9]+")) {
            endTime = Long.parseLong(endTimeParam);
        } else {
            endTime = new Date().getTime();
        }
        
        if (heightP == null) {
            height = HEIGHT;
        }
        
        try {
            staticChartProviderMap.get(fleetId).computeChart(
                    fleetId, vehicleId, interval, endTime, timezone, chartId, WIDTH, height);
        } catch (IOException e) {
            return Response.serverError().build();
        }
        
        return Response.ok().build();
    }

    @GET
    @TimeoutFree
    @Path("/{fleetId}/{vehicleId: [0-9]+}/{interval: [0-9]+}/{endtime: [0-9]*}/{timezone}/{chartId}")
    @Produces("image/png")
    @LoggingData
    public Response getDiagrams(
            @PathParam("fleetId") String fleetId,
            @PathParam("vehicleId") int vehicleId,
            @PathParam("interval") int interval,
            @PathParam("endtime") String endTimeParam,
            @PathParam("timezone") String timezone,
            @PathParam("chartId") Integer chartId,
            @QueryParam("height") Integer height,
            @QueryParam("width") Integer width) {

        Long endTime = null;

        if (endTimeParam != null && endTimeParam.matches("[0-9]+")) {
            endTime = Long.parseLong(endTimeParam);
        } else {
            endTime = new Date().getTime();
        }

        try {
            byte[] imageData = staticChartProviderMap.get(fleetId).getChart(fleetId, vehicleId, interval, endTime, timezone, chartId, height, WIDTH);
            if(imageData != null) {
                ResponseBuilder response = Response.ok(new ByteArrayInputStream(
                        imageData), new MediaType("image", "png"));
                response.header("Cache-Control", "max-age=86400");
                
                return response.build();
            }

        } catch (IOException e) {
            
        }

        ResponseBuilder response = Response.ok(null, new MediaType("image", "png"));
        response.header("Cache-Control", "max-age=86400");
        
        return response.build();
        
    }

    @GET
    @TimeoutFree
    @Path("/chartconfigs/{fleetId}")
    @Produces(MediaType.APPLICATION_JSON)
    @LoggingData
    public StaticChartsConfigBean getChartsConfigs(@PathParam("fleetId") String fleetId) {
        StaticChartsConfigBean result = new StaticChartsConfigBean();
        
        Map<String, List<Integer>> chartsIdByVehicleType = new HashMap<String, List<Integer>>();
        
        // Create a map of vehicle type indexed by charts id
        List<ChartVehicleType> chartVehicleTypes = chartVhclTypeProvider.get(fleetId).getAllChartVehicleTypes();
        Map<Integer, String> vehicleTypebyChartId = new HashMap<Integer, String>();
        for (ChartVehicleType current : chartVehicleTypes) {
            vehicleTypebyChartId.put(current.getChartId(), current.getVehicleType());
        }
        
        // Create the map of charts ids by vehicle type
        List<StaticChartConfig> channelsConfig = staticChartProviderMap.get(fleetId).getAllChannelsConfig(fleetId);
        for (StaticChartConfig conf : channelsConfig) {
            Integer id = conf.getId();
            
            String vehicleType = vehicleTypebyChartId.get(id);
            if (vehicleType != null) {
                // Set the vehicle type to the configuration
                conf.setVehicleType(vehicleType);

                List<Integer> chartIds = chartsIdByVehicleType.get(vehicleType);
                if(chartIds == null) {
                    chartIds = new ArrayList<Integer>();
                    chartsIdByVehicleType.put(vehicleType, chartIds);
                }

                chartIds.add(id);
            }
        }
        
        result.setChartsIdByVehicleType(chartsIdByVehicleType);
        
        return result;
    }
}