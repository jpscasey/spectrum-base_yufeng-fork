/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.rest.data;

import java.util.List;

import com.nexala.spectrum.rest.data.beans.DataSet;

public interface MapProvider {

    DataSet unit(int unitId, Long timestamp);

    List<DataSet> getLiveData();

    List<DataSet> getHistoricFleetData(long timestamp);
}
