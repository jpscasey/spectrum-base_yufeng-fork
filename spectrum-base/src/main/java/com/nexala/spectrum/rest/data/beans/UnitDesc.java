/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.rest.data.beans;

import java.util.ArrayList;
import java.util.List;

public final class UnitDesc {
    /**
     * The db record id of the unit, this will be used for retrieving unit
     * channel data
     */
    private final long id;

    /**
     * This is the display name for the UnitDesc, i.e. this will be the value
     * that appears in the UnitSelector after a value has been selected
     */
    private final String displayName;

    /**
     * This is the fleetId to identify to which fleet this unit belong to
     */
    private final String fleetId;

    /**
     * The list of values which will be displayed in the search results, e.g.
     * Headcode, UnitNumber, etc.
     */
    private final List<String> descriptors;

    public UnitDesc(final long id, final String displayName,
            final String fleetId) {
        this.id = id;
        this.displayName = displayName;
        this.fleetId = fleetId;
        this.descriptors = new ArrayList<String>();
    }

    /**
     * @param descriptor
     *            A string used to identify a unit.
     */
    public void addDescriptor(String descriptor) {
        descriptors.add(descriptor);
    }

    public long getId() {
        return id;
    }

    public List<String> getDescriptors() {
        return descriptors;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getFleetId() {
        return fleetId;
    }
}