/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.rest.serializer;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.nexala.spectrum.channel.Channel;

public class ChannelSerializer extends JsonSerializer<Channel<?>> {


    @Override
    public void serialize(Channel<?> channel, JsonGenerator jgen,
            SerializerProvider provider) throws IOException,
            JsonGenerationException {
        jgen.writeStartArray();
        jgen.writeObject(channel.getValue());
        jgen.writeNumber(channel.getCategory().getSeverity());
        jgen.writeEndArray();
    }
}