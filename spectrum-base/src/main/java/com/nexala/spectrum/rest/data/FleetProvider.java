/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.rest.data;

import java.util.List;

import com.nexala.spectrum.db.beans.UserConfiguration;
import com.nexala.spectrum.licensing.Licence;
import com.nexala.spectrum.rest.data.beans.DataSet;
import com.nexala.spectrum.rest.data.beans.DrillDown;
import com.nexala.spectrum.rest.data.beans.GenericBean;

public interface FleetProvider {
    /** Returns the formations (vehicles grouped by headcode / unit number */
    List<DataSet> getFleet(List<DataSet> vehicles);

    DrillDown getGroupData(boolean config, String fleetId, String unitIds, String group);

    String getLicence(String fleetId);

    List<DataSet> getLiveData();
    
    UserConfiguration getUserConfiguration(Licence licence, String fleetId);

	List<GenericBean> searchUnitsByString(String fleetId, String substr);
}
