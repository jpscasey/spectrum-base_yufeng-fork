package com.nexala.spectrum.rest.service.bean.editor;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementRefs;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "packages")
@XmlAccessorType(XmlAccessType.FIELD)
public class Packages {
    
    @XmlAnyElement
    @XmlElementRefs({
        @XmlElementRef(type = Package.class)
    })
    private List<Package> packages;

    /**
     * Return the packages.
     * @return the packages
     */
    public List<Package> getPackages() {
        return packages;
    }

    /**
     * Setter for the packages.
     * @param packages the packages to set
     */
    public void setPackages(List<Package> packages) {
        this.packages = packages;
    }

}
