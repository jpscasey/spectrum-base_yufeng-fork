package com.nexala.spectrum.rest.data.beans;

public final class EventSearchParameter {
    private String field;
    private String searchString;

    /**
     * @field The db name of the field to search. This value may not be null & must match the T-SQL identifier name
     *        format. See regex below.
     *        
     * @field searchString the value to search for. This value may not be null.
     */
    public EventSearchParameter(String field, String searchString) {
        if (field == null) {
            throw new NullPointerException("Field may not be null");
        }

        if (searchString == null) {
            throw new NullPointerException("SearchString may not be null");
        }

        if (!field.matches("[a-zA-Z0-9_@#][a-zA-Z0-9_@#$]*")) {
            throw new SecurityException("Invalid field name");
        }

        this.field = field;
        this.searchString = searchString;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getSearchString() {
        return searchString;
    }

    public void setSearchString(String searchString) {
        this.searchString = searchString;
    }
}
