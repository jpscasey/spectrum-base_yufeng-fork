package com.nexala.spectrum.rest.cache;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotate with this to enable client side cache.<br>
 * Default value is 1 day cache (sMaxAge = 86400) <br>
 * Basically arguments come from cache-control http header 
 * <a href="http://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html#sec14.9"> http://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html#sec14.9 </a>
 * 
 * @see RestCacheInterceptor
 * @author brice
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Cache
{
   int maxAge() default -1;
   int sMaxAge() default 86400;
   boolean noStore() default false;
   boolean noTransform() default true;
   boolean mustRevalidate() default false;
   boolean proxyRevalidate() default false;
   boolean isPrivate() default true;
}