package com.nexala.spectrum.rest.data;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.google.inject.Inject;
import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.configuration.ConfigurationManager;
import com.nexala.spectrum.db.beans.LookupValue;
import com.nexala.spectrum.db.dao.LookupValueDao;

public class ChannelLookupTableCache {
    private final Logger LOGGER = Logger.getLogger(ChannelLookupTableCache.class);

    private final LookupValueDao dao;
    private final Long refreshInterval;
    private long lastUpdateCheck = 0;

    private Map<Integer, Map<String, LookupValue>> lookupValuesMap;

    @Inject
    public ChannelLookupTableCache(LookupValueDao dao, ConfigurationManager conf) {
        this.dao = dao;

        refreshInterval = conf.getConfAsLong(Spectrum.SPECTRUM_REFRESH_LOOKUPVALUES_INTERVAL);

        loadLookupValues();

        this.lastUpdateCheck = System.currentTimeMillis();
    }

    public synchronized Map<Integer, Map<String, LookupValue>> getLookupValues() {
        long timeout = System.currentTimeMillis() - lastUpdateCheck;

        if (refreshInterval != null && refreshInterval >= 0 && timeout >= refreshInterval) {

            loadLookupValues();

            this.lastUpdateCheck = System.currentTimeMillis();
        }

        return new HashMap<Integer, Map<String, LookupValue>>(lookupValuesMap);
    }

    private void loadLookupValues() {
        long startTime = System.currentTimeMillis();

        List<LookupValue> lookupValues = dao.findAll();

        lookupValuesMap = new HashMap<Integer, Map<String, LookupValue>>();

        for (LookupValue value : lookupValues) {
            if (lookupValuesMap.get(value.getChannelId()) == null) {
                lookupValuesMap.put(value.getChannelId(), new HashMap<String, LookupValue>());
            }

            lookupValuesMap.get(value.getChannelId()).put(value.getValue(), value);
        }

        LOGGER.info("Loaded " + lookupValues.size() + " lookup values: " + (System.currentTimeMillis() - startTime)
                + "ms");
    }
}
