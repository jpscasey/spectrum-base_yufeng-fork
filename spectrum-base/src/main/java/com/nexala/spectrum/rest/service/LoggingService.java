package com.nexala.spectrum.rest.service;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import com.google.inject.Singleton;
import com.nexala.spectrum.eventanalysis.EAnalysisConstants;
import com.nexala.spectrum.licensing.LoggingResource;
import com.nexala.spectrum.view.Constants;

/**
 * The calls are logged in the ServletLogResourceInterceptor
 */

@Path("/logging/")
@Singleton
public class LoggingService {
    @GET
    @LoggingResource(Constants.FLEET_LIC_KEY)
    @Path("/fleet")
    public Response fleet() {
        return Response.ok().build();
    }

    @GET
    @LoggingResource(Constants.UNIT_LIC_KEY)
    @Path("/unit")
    public Response unit() {
        return Response.ok().build();
    }

    @GET
    @LoggingResource(Constants.UNIT_DETAIL_LIC_KEY)
    @Path("/unit/detail")
    public Response unitSummaryDetail() {
        return Response.ok().build();
    }

    @GET
    @LoggingResource(Constants.UNIT_CHANNELDATA_LIC_KEY)
    @Path("/unit/channeldata")
    public Response unitSummaryChannelData() {
        return Response.ok().build();
    }

    @GET
    @LoggingResource(Constants.UNIT_DATAPLOTS_LIC_KEY)
    @Path("/unit/dataplots")
    public Response unitSummaryDataPlots() {
        return Response.ok().build();
    }

    @GET
    @LoggingResource(Constants.UNIT_CABVIEW_LIC_KEY)
    @Path("/unit/cabview")
    public Response unitSummaryCabView() {
        return Response.ok().build();
    }

    @GET
    @LoggingResource(Constants.UNIT_EVENT_LIC_KEY)
    @Path("/unit/event")
    public Response unitSummaryEvent() {
        return Response.ok().build();
    }

    @GET
    @LoggingResource(Constants.UNIT_MAP_LIC_KEY)
    @Path("/unit/map")
    public Response unitSummaryMap() {
        return Response.ok().build();
    }

    @GET
    @LoggingResource(Constants.MAP_LIC_KEY)
    @Path("/map")
    public Response map() {
        return Response.ok().build();
    }

    @GET
    @LoggingResource(Constants.EVENT_LIC_KEY)
    @Path("/event")
    public Response event() {
        return Response.ok().build();
    }

    @GET
    @LoggingResource(Constants.DOWNLOAD_LIC_KEY)
    @Path("/download")
    public Response download() {
        return Response.ok().build();
    }

    @GET
    @LoggingResource(Constants.ADMINISTRATION_CHANNELDEFINITION_LIC_KEY)
    @Path("/channeldefinition")
    public Response channelDefinition() {
        return Response.ok().build();
    }

    @GET
    @LoggingResource(EAnalysisConstants.EVENT_ANALYSIS_LICENCE_KEY)
    @Path("/eventanalysis")
    public Response eventAnalysis() {
        return Response.ok().build();
    }
}