package com.nexala.spectrum.rest.data;

import java.util.List;

import com.nexala.spectrum.db.beans.FleetFormation;

/**
 * Cache for fleetformation.
 * @author BBaudry
 *
 */
public interface FleetFormationCache {
    
    /**
     * In charge of getting the fleet formation.
     * If the cache is too old will reload it.
     * @param fleet the fleet id
     */
    public List<FleetFormation> getValidFleetFormations(String fleet);


}
