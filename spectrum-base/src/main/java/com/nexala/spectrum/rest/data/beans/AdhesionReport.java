package com.nexala.spectrum.rest.data.beans;

import java.sql.Timestamp;

import com.nexala.spectrum.mapping.Coordinate;

public class AdhesionReport extends Coordinate {

    protected Long id;
    protected String vehicleNumber;
    protected Timestamp timestamp;
    protected Double gCourse;
    protected Integer category;
    protected String locationCode;
    protected Integer value;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getVehicleNumber() {
        return vehicleNumber;
    }

    public void setVehicleNumber(String vehicleNumber) {
        this.vehicleNumber = vehicleNumber;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public Double getgCourse() {
        return gCourse;
    }

    public void setgCourse(Double gCourse) {
        this.gCourse = gCourse;
    }

    public Integer getCategory() {
        return category;
    }

    public void setCategory(Integer category) {
        this.category = category;
    }

    public String getLocationCode() {
        return locationCode;
    }

    public void setLocationCode(String locationCode) {
        this.locationCode = locationCode;
    }
    
    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }
}
