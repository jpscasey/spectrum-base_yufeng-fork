/*
 * Copyright (c) Nexala Technologies 2013, All rights reserved.
 */
package com.nexala.spectrum.rest.data;

import java.util.ArrayList;
import java.util.List;

import com.google.inject.Inject;
import com.nexala.spectrum.rest.data.beans.DataSet;
import com.nexala.spectrum.rest.data.beans.EventChannelDataCache;
import com.nexala.spectrum.rest.data.beans.SourceEventChannelData;

/**
 * This class provides a default implementation of
 * {@link EventChannelDataProvider}, which returns no channel data.
 * 
 * @author poriordan
 */
public class DefaultEventChannelDataProvider implements EventChannelDataProvider {
    
    private final EventChannelDataCache eventchannelCache;
    
    @Inject
    public DefaultEventChannelDataProvider(EventChannelDataCache eventchannelCache) {
        this.eventchannelCache = eventchannelCache;
    }
    
    @Override
    public List<DataSet> getEventChannelDataByVehicle(Integer eventSource,
            Integer vehicleId) {
        return new ArrayList<DataSet>();
    }
    
    /**
     * @see com.nexala.spectrum.rest.data.EventChannelDataProvider#getEventChannelData()
     */
    @Override
    public List<SourceEventChannelData> getEventChannelData() {
        return eventchannelCache.getEventChannelData();
    }
}

