package com.nexala.spectrum.rest.data;

import java.util.List;

import com.nexala.spectrum.db.beans.ChannelConfig;

public class SortableChannelConfigList implements Comparable<SortableChannelConfigList> {

    private Integer id;

    private Integer order;

    private List<ChannelConfig> channelConfigList;

    public SortableChannelConfigList (Integer id, Integer order, List<ChannelConfig> channelConfigList) {
        this.id = id;
        this.order = order;
        this.channelConfigList = channelConfigList;
    }
    
    public Integer getId() {
        return id;
    }

    public Integer getOrder() {
        return order;
    }

    public List<ChannelConfig> getChannelConfigList() {
        return channelConfigList;
    }

    public void addChannelConfigList(ChannelConfig channelConfig) {
        this.channelConfigList.add(channelConfig);
    }

    @Override
    public int compareTo(SortableChannelConfigList list) {

        if (order != null && order != 0 && list.order != null && list.order != 0) {
            return order.compareTo(list.order);
        } else if ((order == null || order == 0) && (list.order == null || list.order == 0)) {
            return id.compareTo(list.id);
        } else if (order == null || order == 0) {
            return 1;
        } else {
            return -1;
        }
    }
}
