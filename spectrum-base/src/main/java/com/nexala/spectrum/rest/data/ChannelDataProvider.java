/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.rest.data;

import java.util.List;

import com.nexala.spectrum.db.beans.ChannelConfig;
import com.nexala.spectrum.rest.data.beans.DataSet;

public interface ChannelDataProvider {

       
    /**
	 * Return the vehicles data applying some sampling
     * @param samplingRate the sampling rate. ie 1000 will return only one records per seconds
     * @return the sampled data
     */
    List<DataSet> getSampledVehicleData(int vehicleId, Long startTime, Long endTime, List<ChannelConfig> channels,
            boolean loadLookupValues, Long samplingRate);
    
    List<DataSet> getUnitData(int stockId, Long timestamp, int direction);

    List<DataSet> getLiveChannelData();

    List<DataSet> getHistoricFleetData(long timestamp);

    List<DataSet> getLiveChannelData(List<Integer> columnList, String unitIds);

    String getActiveVehicle(String unitIds);

    String getVehicleNumberById(String vehicleId);

    List<DataSet> getVehicleData(int vehicleId, Long endTime, int interval, Integer endRow, List<ChannelConfig> channels,
            boolean loadLookupValues);

    /**
     * Return the data for the vehicle for the period of time passed in parameter
     * @param vehicleId the vehicle id
     * @param startTime the start time
     * @param endTime the end time
     * @param channels the list of channel configuration
     * @param loadLookupValues if the lookup values need to be loaded
     * @return the data for that period of time
     */
    List<DataSet> getVehicleDataChart(int vehicleId, long startTime, long endTime, List<ChannelConfig> channels,
            boolean loadLookupValues);
    
    List<DataSet> getVehicleDataExport(int vehicleId, long startTime, long endTime, List<ChannelConfig> channels,
            boolean loadLookupValues);
    
    /**
     * Return the more recent timestamp received before the time passed in parameter.
     * Timestamp could be null, in that case the last received data timestamp should be return. 
     * @param unitId the unitId.
     * @param timestamp the timestamp, if null return the live timestamp
     * @return the last received data from the time passed in parameter
     */
    Long getUnitTimestamp(int unitId, Long timestamp);
    
    /**
     * Return the last available maintenance timestamp of this unit
     * @param unitId the unitId.
     * @return the last available maintenance timestamp of the unit
     */
    Long getUnitLastMaintTimestamp(int unitId);

}
