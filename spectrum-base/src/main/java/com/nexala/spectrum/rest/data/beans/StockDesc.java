/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.rest.data.beans;

public class StockDesc {
    private final String description;
    private final long id;
    private final int position;
    private final String type;
    private final boolean leading;
    private final String vehicleType;

    public StockDesc(long id, String description, int position, String type, boolean leading, String vehicleType) {
        this.id = id;
        this.description = description;
        this.position = position;
        this.type = type;
        this.leading = leading;
        this.vehicleType = vehicleType;
    }

    public String getDescription() {
        return description;
    }

    public long getId() {
        return id;
    }

    public int getPosition() {
        return position;
    }

    public String getType() {
        return type;
    }
    
    public boolean getLeading() {
        return leading;
    }

    public String getVehicleType() {
        return vehicleType;
    }
}
