/**
 * 
 */
package com.nexala.spectrum.rest.data;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import com.google.inject.Inject;
import com.nexala.spectrum.charting.DVChart;
import com.nexala.spectrum.charting.DVChartFactory;
import com.nexala.spectrum.configuration.ConfigurationManager;
import com.nexala.spectrum.db.beans.ChannelConfig;
import com.nexala.spectrum.rest.data.beans.DataSet;
import com.nexala.spectrum.utils.DateUtils;

import com.nexala.spectrum.licensing.Licence;
/**
 * Default implementation of {@link CommonChartProvider}.
 * @author bbaudry
 *
 */
public class DefaultCommonChartProvider implements CommonChartProvider {
    
    private static final int DEFAULT_CHART_WIDTH = 800;
    
    @Inject
    private DVChartFactory chartFactory;
    
    @Inject
    private Map<String, ChannelDataProvider> cdProviders;
    
    @Inject
    private ConfigurationManager confManager;

    /**
     * @see com.nexala.spectrum.rest.data.CommonChartProvider#createImage(java.lang.String,
     *      int, int, java.lang.String, java.lang.String, java.lang.String,
     *      java.lang.Integer, java.lang.Integer)
     */
    @Override
    public byte[] createImage(String fleetId, int vehicleId, int interval, String endTimeParam, 
            String timezone, List<ChannelConfig> channels, Integer height, Integer width, Licence license) throws IOException {
        
        final List<ChannelConfig> channelsDataPlot = new ArrayList<ChannelConfig>(channels);
        Long endTime = Long.parseLong(endTimeParam);

        /*
         * We are getting more data to be able to plot until the edge of the
         * graph based on the previous values and the next ones. Add 60 seconds
         * to the interval (get 30 seconds of data before and 30 seconds of data
         * after)
         */
        int dataInterval = interval + 60;
        long dataEndTime = endTime + 30000; // add 30 seconds to end time

        

        TimeZone tz = DateUtils.getClientTimeZone(timezone);

        DVChart chart = chartFactory.create(
                width == null || width < 0 ? DEFAULT_CHART_WIDTH : width, channelsDataPlot, tz);
        
        // Initialize the chart
        chart.init(interval * 1000, endTime,license);
        
        // Plot the date
        plotData(fleetId, vehicleId, channels, dataInterval, dataEndTime, chart);

        byte[] imageData = chart.render();

        return imageData;
    }

    /**
     * Get the data by chunks of 5 minutes and plot it on the chart
     * @param fleetId the fleetId
     * @param vehicleId the vehicle id
     * @param channels channel config
     * @param dataInterval how much data is retrieved in seconds
     * @param dataEndTime the end time
     * @param chart the chart where we are ploting the data
     */
    private void plotData(String fleetId, int vehicleId, List<ChannelConfig> channels, int dataInterval,
            long dataEndTime, DVChart chart) {
        long dataStartTime = dataEndTime - dataInterval*1000;
        long currentStartTime = dataStartTime;
        
        int dataChunck = 600; // 10 minutes of data
        
        while (currentStartTime < (dataEndTime+1)) {
            long currentEndTime = currentStartTime + (dataChunck*1000); 
            if (currentEndTime > dataEndTime) {
                currentEndTime = dataEndTime;
            }
            
            List<DataSet> data = cdProviders.get(fleetId).getVehicleDataChart(vehicleId, currentStartTime,
                    currentEndTime,  channels, false); 
            chart.plot(data);
            
            currentStartTime = currentEndTime + 1; // position start to the next chunck of data
        }
        
    }
    
    public List<DataSet> getVehicleDataExport(String fleetId, int vehicleId, Long dataEndTime, int dataInterval, List<ChannelConfig> channels,
            boolean loadLookupValues) {
    	long dataStartTime = dataEndTime - dataInterval*1000;
        long currentStartTime = dataStartTime;
        
        int dataChunck = 600; // 10 minutes of data
        
        List<DataSet> data = new ArrayList<DataSet>();
        
        while (currentStartTime < (dataEndTime+1)) {
            long currentEndTime = currentStartTime + (dataChunck*1000); 
            if (currentEndTime > dataEndTime) {
                currentEndTime = dataEndTime;
            }
            
            data.addAll(cdProviders.get(fleetId).getVehicleDataExport(vehicleId, currentStartTime,
                    currentEndTime,  channels, false)); 
            
            currentStartTime = currentEndTime + 1; // position start to the next chunck of data
        }
		
        return data;   	
    }    
}
