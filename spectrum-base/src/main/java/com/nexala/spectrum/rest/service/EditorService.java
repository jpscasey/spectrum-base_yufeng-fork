package com.nexala.spectrum.rest.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.nexala.spectrum.configuration.ApplicationConfiguration;
import com.nexala.spectrum.i18n.ResourceBundleUTF8;
import com.nexala.spectrum.rest.service.bean.DownloadConfigurationUIBean;
import com.nexala.spectrum.rest.service.bean.editor.FaultConfigurationMeta;
import com.nexala.spectrum.rest.service.bean.editor.Fleet;
import com.nexala.spectrum.rest.service.bean.editor.Package;
import com.nexala.spectrum.rest.service.bean.editor.Packages;
import com.nexala.spectrum.rest.service.bean.editor.Variable;
import com.nexala.spectrum.rest.service.bean.editor.Variables;
import com.nexala.spectrum.view.conf.DownloadConfiguration;
import com.nexala.spectrum.view.conf.FieldBuilder;
import com.typesafe.config.Config;

@Path("/editor")
@Singleton
public class EditorService {
    
    private static final Logger LOGGER = Logger.getLogger(EditorService.class);
    
    @Inject
    private ApplicationConfiguration appConfig;
    
    @Inject
    private DownloadConfiguration downloadConfiguration;
    
    @GET
    @Path("/packages")
    @Produces(MediaType.APPLICATION_XML)
    public Packages getPackages() {
        Packages result = new Packages();
        
        List<Package> pkgList = new ArrayList<>();
        List<? extends Config> packages = appConfig.get().getConfigList("r2m.editor.packages");
        for (Config current : packages) {
            Package p = new Package();
            p.setName(current.getString("name"));
            p.setParentPath(current.getString("parent-path"));
            p.setDisplayName(current.getString("display-name"));
            if (current.hasPath("licence")) {
                p.setLicence(current.getString("licence"));
            }
            p.setType(current.getString("type"));
            if (current.hasPath("fleet-code")) {
            	p.setFleetCode(current.getString("fleet-code"));
            }
            
            pkgList.add(p);
        }
        result.setPackages(pkgList);
        
        return result;
    }

    @GET
    @Path("/variables")
    @Produces(MediaType.APPLICATION_XML)
    public Variables getVariables() {
        Variables result = new Variables();

        List<Variable> varList = new ArrayList<>();
        List<? extends Config> variables = appConfig.get().getConfigList("r2m.editor.variables");
        for (Config current : variables) {
            Variable v = new Variable();
            v.setName(current.getString("name"));
            v.setDescription(current.getString("description"));
            v.setType(current.getString("type"));

            varList.add(v);
        }
        result.setVariables(varList);

        return result;
    }
    
    @GET
    @Path("/faultConfigurationMeta")
    @Produces(MediaType.APPLICATION_XML)
    public FaultConfigurationMeta getFaultConfigurationMeta() {
    	FaultConfigurationMeta result = new FaultConfigurationMeta();
    	
    	Config conf = appConfig.get();
    	String faultConfigMeta = "r2m.editor.faultConfigurationMeta";
    	
    	if (conf.hasPath(faultConfigMeta)) {
    		
    		String groupByFleetPath = faultConfigMeta + ".group-by-fleet";
    		if (conf.hasPath(groupByFleetPath)) {
    			Boolean groupByFleet = conf.getBoolean(groupByFleetPath);
    			result.setGroupByFleet(groupByFleet);
    		}
    		
    		String fleetPath = faultConfigMeta + ".fleets";
    		if (conf.hasPath(fleetPath)) {
    			List<Fleet> fleetList = new ArrayList<>();
    			List<? extends Config> fleets = conf.getConfigList(fleetPath);
    			for (Config current : fleets) {
    				Fleet fleet = new Fleet();
    				fleet.setFleetCode(current.getString("fleet-code"));
    				
    				fleetList.add(fleet);
    			}
    			result.setFleets(fleetList);
    		}
    	} 
    	
    	return result;
    }
    
    
    @GET
    @Path("/faultMetaExtraFields/{locale}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<FieldBuilder> getFaultMetaExtraFields(@PathParam("locale") String locale) {
        List<FieldBuilder> result = new ArrayList<>();
        
        ResourceBundleUTF8 bundle = new ResourceBundleUTF8(new Locale(locale));
        
        Config config = appConfig.get();
        if (config.hasPath("r2m.editor.faultMetaExtraFields")) {
            List<? extends Config> configList = config.getConfigList("r2m.editor.faultMetaExtraFields");
            
            for (Config current : configList) {
                result.add(appConfig.getFieldBuilder(current, null, bundle));
            }
        }
        
        return result;
    }
    
    @GET
    @Path("/overrideFields/{locale}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<FieldBuilder> getOverrideFields(@PathParam("locale") String locale) {
        List<FieldBuilder> result = new ArrayList<>();
        
        ResourceBundleUTF8 bundle = new ResourceBundleUTF8(new Locale(locale));
        
        Config config = appConfig.get();
        if (config.hasPath("r2m.editor.overrideFields")) {
            List<? extends Config> configList = config.getConfigList("r2m.editor.overrideFields");
            
            for (Config current : configList) {
                result.add(appConfig.getFieldBuilder(current, null, bundle));
            }
        }
        
        return result;
    }
    
	@GET
	@Path("/consequenceExtraFields/{locale}")
	@Produces(MediaType.APPLICATION_JSON)
	public Map<String, List<FieldBuilder>> getConsequenceExtraFields(@PathParam("locale") String locale) {
		Map<String, List<FieldBuilder>> result = new HashMap<String, List<FieldBuilder>>();
		String extraFieldsPath = "r2m.editor.consequenceExtraFields";

		// check if application configuration has consequenceExtraFields path
		// if true store the content and return it as a result

		if (appConfig.get().hasPath(extraFieldsPath)) {
			ResourceBundleUTF8 bundle = new ResourceBundleUTF8(new Locale(locale));

			Config consequences = appConfig.get().getConfig(extraFieldsPath);
			List<String> consequenceNames = new ArrayList<String>(consequences.root().unwrapped().keySet());

			for (String consequenceName : consequenceNames) {
				List<FieldBuilder> consequenceExtraFields = new ArrayList<>();

				for (Config current : consequences.getConfigList(consequenceName)) {
					consequenceExtraFields.add(appConfig.getFieldBuilder(current, null, bundle));
				}

				result.put(consequenceName, consequenceExtraFields);
			}
		}

		return result;
	}
	
	@GET
	@Path("/downloadRequestFields/{locale}")
	@Produces(MediaType.APPLICATION_JSON)
	public DownloadConfigurationUIBean getDownloadRequestFields(@PathParam("locale") String locale) {
		DownloadConfigurationUIBean result = new DownloadConfigurationUIBean();
        result.setFileTypes(downloadConfiguration.getFileTypes());
        result.setRuleEditorDRFilters(downloadConfiguration.getRuleEditorDRFilters());
        result.setThresholdTime(downloadConfiguration.getThresholdTime());
        result.setGapTime(downloadConfiguration.getGapTime());
        
		return result;
	}

}
