package com.nexala.spectrum.rest.data.beans;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.inject.Inject;
import com.nexala.spectrum.db.dao.SourceEventChannelDataDao;

/**
 * Cache for {@link SourceEventChannelData}. Should be binded as singleton.
 * @author BBaudry
 *
 */
public class EventChannelDataCache {
    
    @Inject
    private SourceEventChannelDataDao dao;
    
    private Date latestTimestamp = null;
    
    private Map<Integer, SourceEventChannelData> eventChannelDataBySource = new HashMap<>();
    
    /**
     * Retrieve the last updated records and return the event channel data.
     * @return the event channel data
     */
    public synchronized List<SourceEventChannelData> getEventChannelData() {
        
        List<SourceEventChannelData> allEventChannelValues = dao.getAllEventChannelValues();
        doInsert(allEventChannelValues);
        
        return Collections.unmodifiableList(new ArrayList<>(eventChannelDataBySource.values()));
    }
    
    
    /**
     * Insert the event channel data in parameter into the cache. 
     * @param newValues the values to add to the cache
     */
    private void doInsert(List<SourceEventChannelData> newValues) {
        
        for (SourceEventChannelData current : newValues) {
            Integer sourceId = current.getSourceId();
            SourceEventChannelData cached = eventChannelDataBySource.get(sourceId);
            if (cached == null) {
                eventChannelDataBySource.put(sourceId, current);
                updateLatestTimestamp(current);
            } else {
                cached.merge(current);
                updateLatestTimestamp(cached);
            }
        }
        
    }
    
    /**
     * Update the cache latest timestamp.
     * @param ecd the SourceEventChannelData 
     */
    private void updateLatestTimestamp(SourceEventChannelData ecd) {
        if (latestTimestamp == null || ecd.getLastInsertTime().after(latestTimestamp)) {
            latestTimestamp = ecd.getLatestTimestamp();
        }
    }
    

}
