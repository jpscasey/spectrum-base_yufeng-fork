/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.rest.service;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.log4j.Logger;
import org.jboss.resteasy.plugins.guice.ModuleProcessor;
import org.jboss.resteasy.plugins.server.servlet.ResteasyBootstrap;
import org.jboss.resteasy.spi.Registry;
import org.jboss.resteasy.spi.ResteasyProviderFactory;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Module;
import com.google.inject.assistedinject.FactoryModuleBuilder;
import com.google.inject.name.Names;
import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.caching.CacheModule;
import com.nexala.spectrum.configuration.ChannelConfigurationBeanFactory;
import com.nexala.spectrum.configuration.ModuleCollection;
import com.nexala.spectrum.db.dao.BoxedResultSetFactory;
import com.nexala.spectrum.view.PluginFactory;
import com.nexala.spectrum.view.SpectrumViewModule;
import com.nexala.spectrum.view.conf.AppConfiguration;

/**
 * <p>
 * This listener is used to
 * </p>
 * 
 * <p>
 * This listener requires an init-param [rest.guice.module] to be defined, in
 * the web.xml. This parameter should have the full class name of a guice module
 * which contains bindings for all of spectrum data interfaces.
 * </p>
 * 
 * @author poriordan
 */
public class SpectrumContextListener extends ResteasyBootstrap implements ServletContextListener {

    private final static Logger logger = Logger.getLogger(SpectrumContextListener.class);

    private Module[] implModules;

    private Module[] serviceModules;


    @Override
    public void contextInitialized(final ServletContextEvent event) {
        super.contextInitialized(event);

        ServletContext context = event.getServletContext();
        ServletContextHolder.getInstance().setServletContext(context);

        implModules = resolveModuleCollection(context, "spectrum.modules.web");
        serviceModules = resolveModuleCollection(context,
                "spectrum.modules.services");

        final Registry registry = (Registry) context
                .getAttribute(Registry.class.getName());
        final ResteasyProviderFactory providerFactory = (ResteasyProviderFactory) context
                .getAttribute(ResteasyProviderFactory.class.getName());
        final ModuleProcessor processor = new ModuleProcessor(registry,
                providerFactory);
        //final Stage stage = getStage(context);
        
        Injector injector = getInjector();
        
        processor.processInjector(injector);

        // load parent injectors
        while (injector.getParent() != null) {
            injector = injector.getParent();
            processor.processInjector(injector);
        }
        
        triggerAnnotatedMethods(PostConstruct.class);
    }

    private Module[] resolveModuleCollection(ServletContext ctx,
            String initParam) {
        String clsName = ctx.getInitParameter(initParam);

        ModuleCollection module = null;

        if (clsName != null) {
            try {
                Class<?> clazz = Class.forName(clsName);
                Object object = clazz.newInstance();

                if (object instanceof ModuleCollection) {
                    module = (ModuleCollection) object;
                } else {
                    throw new IllegalArgumentException(initParam + " is "
                            + "not an instance of "
                            + ModuleCollection.class.getName());
                }
            } catch (ClassNotFoundException e) {
                logger.fatal(initParam + " class " + clsName + " not found");
            } catch (InstantiationException e) {
                logger.fatal(initParam + " is not default instantiable");
            } catch (IllegalAccessException e) {
                logger.fatal(initParam + " is not default instantiable");
            }
        } else {
            logger.warn(initParam + " module class name not specified");
        }

        if (module != null) {
            return module.getModules();
        } else {
            return new Module[0];
        }
    }

    protected Injector getInjector() {
        Injector mainInjector = Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {
                bind(String.class).annotatedWith(
                        Names.named(Spectrum.SPECTRUM_PROPERTIES_PATH_PROP))
                        .toInstance(Spectrum.SPECTRUM_PROPERTIES_PATH);
                install(new FactoryModuleBuilder()
                        .build(BoxedResultSetFactory.class));
                install(new FactoryModuleBuilder().build(PluginFactory.class));
                install(new FactoryModuleBuilder()
                        .build(ChannelConfigurationBeanFactory.class));
            }
        });

        List<Module> implModulesLst = new ArrayList<Module>(
                Arrays.asList(implModules));
        implModulesLst.add(new CacheModule());
        Injector implInjector = mainInjector.createChildInjector(implModulesLst
                .toArray(new Module[implModulesLst.size()]));

        List<Module> servlets = new ArrayList<Module>(
                Arrays.asList(serviceModules));
        servlets.add(new SpectrumViewModule(implInjector
                .getInstance(AppConfiguration.class)));
        servlets.add(new RestModule());
        return implInjector.createChildInjector(servlets);
    }
    
    
    @Override
    public void contextDestroyed(final ServletContextEvent event)
    {
       triggerAnnotatedMethods(PreDestroy.class);
    }

    private void triggerAnnotatedMethods(
            final Class<? extends Annotation> annotationClass) {
        
        for (Module module : this.implModules) {
            triggerAnnotationMethod(annotationClass, module);
        }
        
        for (Module module : this.serviceModules) {
            triggerAnnotationMethod(annotationClass, module);
        }
    }

    /**
     * @param annotationClass
     * @param module
     */
    private void triggerAnnotationMethod(
            final Class<? extends Annotation> annotationClass, Module module) {
        final Method[] methods = module.getClass().getMethods();
          for (Method method : methods)
          {
             if (method.isAnnotationPresent(annotationClass))
             {
                if(method.getParameterTypes().length > 0)
                {
                   logger.warn("Cannot execute expected module "+module.getClass().getSimpleName()+"'s @"+annotationClass.getSimpleName()+" method "+method.getName()+" because it has unexpected parameters: skipping.");
                   continue;
                }
                try
                {
                   method.invoke(module);
                } catch (InvocationTargetException ex) {
                   logger.warn("Problem running annotation method @" + annotationClass.getSimpleName(), ex);
                } catch (IllegalAccessException ex) {
                   logger.warn("Problem running annotation method @" + annotationClass.getSimpleName(), ex);
                }
             }
          }
    }
}