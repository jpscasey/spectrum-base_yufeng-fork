package com.nexala.spectrum.rest.data;

public class FeatureAccessDeniedException extends Exception {
    private static final long serialVersionUID = 4007116719784815962L;

    public FeatureAccessDeniedException() {
        super();
    }
    
    public FeatureAccessDeniedException(String message) {
        super(message);
    }
    
    public FeatureAccessDeniedException(Throwable t) {
        super(t);
    }
}