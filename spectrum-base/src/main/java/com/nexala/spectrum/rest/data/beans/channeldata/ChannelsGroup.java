package com.nexala.spectrum.rest.data.beans.channeldata;

import java.util.List;

import com.nexala.spectrum.view.conf.Column;

/**
 * Bean representing a group in the "Channel Data" tab from the Unit Summary. 
 * It contain the list of channels, and the table columns.
 * @author brice
 *
 */
public class ChannelsGroup {
    
    
    List<ChannelInfo> channels = null;
    
    
    List<Column> columns = null;


    /**
     * Getter for channels.
     * @return the channels
     */
    public List<ChannelInfo> getChannels() {
        return channels;
    }


    /**
     * Setter for channels.
     * @param channels the channels to set
     */
    public void setChannels(List<ChannelInfo> channels) {
        this.channels = channels;
    }


    /**
     * Getter for columns.
     * @return the columns
     */
    public List<Column> getColumns() {
        return columns;
    }


    /**
     * Setter for columns.
     * @param columns the columns to set
     */
    public void setColumns(List<Column> columns) {
        this.columns = columns;
    }

}
