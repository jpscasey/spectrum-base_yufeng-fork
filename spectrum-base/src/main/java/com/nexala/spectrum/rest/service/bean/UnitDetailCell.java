package com.nexala.spectrum.rest.service.bean;

import java.util.ArrayList;
import java.util.List;

import com.nexala.spectrum.view.conf.Column;
import com.nexala.spectrum.view.conf.MouseOverText;

public class UnitDetailCell {
	private String name;
	private String title;
	private String mode;
	
	private List<Column> columns;
	
	private List<EventFilterBean> filters = new ArrayList<EventFilterBean>();
	
	private List<MouseOverText> rowMouseOver = new ArrayList<MouseOverText>();

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public List<Column> getColumns() {
		return columns;
	}

	public void setColumns(List<Column> columns) {
		this.columns = columns;
	}

    public List<EventFilterBean> getFilters() {
        return filters;
    }

    public void setFilters(List<EventFilterBean> filters) {
        this.filters = filters;
    }

    public List<MouseOverText> getRowMouseOver() {
        return rowMouseOver;
    }

    public void setRowMouseOver(List<MouseOverText> rowMouseOver) {
        this.rowMouseOver = rowMouseOver;
    }
}
