package com.nexala.spectrum.rest.data.beans;

import com.nexala.spectrum.channel.ChannelCollection;

public class DataSetImpl implements DataSet {
    
    private final Integer id;
    private final Long timestamp;
    private ChannelCollection cc;

    public DataSetImpl(Integer id, ChannelCollection cc) {
        this.id = id;
        this.cc = cc;
        this.timestamp = null;
    }
    
    public DataSetImpl(Integer id, Long timestamp, ChannelCollection cc) {
        this.id = id;
        this.timestamp = timestamp;
        this.cc = cc;
    }

    @Override
    public Integer getSourceId() {
        return id;
    }

    @Override
    public Long getTimestamp() {
        return timestamp;
    }

    @Override
    public ChannelCollection getChannels() {
        return cc;
    }
    
    @Override
    public void setChannels(ChannelCollection cc) {
        this.cc = cc;
    }
}
