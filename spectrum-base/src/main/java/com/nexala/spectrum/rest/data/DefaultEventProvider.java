/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.rest.data;

import static com.nexala.spectrum.view.conf.ChannelDataGridStyle.CHANNELDATA_COMMON;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import com.google.inject.Inject;
import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.configuration.ApplicationConfiguration;
import com.nexala.spectrum.db.beans.ChannelConfig;
import com.nexala.spectrum.db.beans.ChannelGroupConfig;
import com.nexala.spectrum.db.beans.EventComment;
import com.nexala.spectrum.db.beans.FaultCategory;
import com.nexala.spectrum.db.beans.FaultGroup;
import com.nexala.spectrum.db.beans.FaultGroupInputParam;
import com.nexala.spectrum.db.beans.FaultMeta;
import com.nexala.spectrum.db.beans.FaultMetaExtraField;
import com.nexala.spectrum.db.beans.FaultType;
import com.nexala.spectrum.db.dao.EventCommentDao;
import com.nexala.spectrum.db.dao.EventDao;
import com.nexala.spectrum.db.dao.EventFormationDao;
import com.nexala.spectrum.db.dao.FaultCategoryDao;
import com.nexala.spectrum.db.dao.FaultGroupDao;
import com.nexala.spectrum.db.dao.FaultMetaDao;
import com.nexala.spectrum.db.dao.FaultMetaExtraFieldDao;
import com.nexala.spectrum.db.dao.FaultTransmissionDao;
import com.nexala.spectrum.db.dao.FaultTypeDao;
import com.nexala.spectrum.db.dao.charting.ChannelType;
import com.nexala.spectrum.rest.data.beans.ChannelData;
import com.nexala.spectrum.rest.data.beans.Event;
import com.nexala.spectrum.rest.data.beans.EventFormation;
import com.nexala.spectrum.rest.data.beans.EventSearchParameter;
import com.nexala.spectrum.rest.service.bean.EventTableBean;
import com.nexala.spectrum.view.conf.Column;
import com.nexala.spectrum.view.conf.TextColumnBuilder;

/**
 * Provides a default implementation of the {@link EventProvider} interface, which should be used by almost all Spectrum
 * projects, unless they are not using the standard event table.
 * @author poriordan
 */
public class DefaultEventProvider implements EventProvider {

    private static int NUMBER_OF_UNIT_NOT_LIVE_EVENTS = 15;

    
    protected EventDao eventDao;

    @Inject
    private EventCommentDao eventCommentDao;

    @Inject
    private EventFormationDao eventFormationDao;

    @Inject
    private FaultCategoryDao faultCategoryDao;

    @Inject
    protected FaultMetaDao faultMetaDao;

    @Inject
    private FaultTypeDao faultTypeDao;
    
    @Inject
    private FaultGroupDao faultGroupDao;
    
    @Inject
    private FaultTransmissionDao faultTransmissionDao;
    
    @Inject
    private FaultMetaExtraFieldDao fmemDao;
    
    @Inject
    protected ApplicationConfiguration appConf;
    
    @Inject
    public DefaultEventProvider(EventDao eventDao) {
        this.eventDao = eventDao;
    }

    private static final Comparator<Event> SORT_BY_CREATE_TIME_DESC = new Comparator<Event>() {
        @Override
        public int compare(Event e1, Event e2) {
            return (int) ((Long) e2.getField(Spectrum.EVENT_CREATE_TIME)).compareTo(((Long) e1.getField(Spectrum.EVENT_CREATE_TIME)));
        }
    };

    @Override
    public void update(Integer eventId, String action, String comment, String userId, Long timestamp, String method) {
        eventDao.update(eventId, action, comment, userId, timestamp, method);
    }

    @Override
    public List<EventComment> getComments(Integer eventId) {
        return eventCommentDao.getComments(eventId);
    }
    
    @Override
    public List<EventComment> saveComment(Integer eventId, Long timestamp, String userId, String comment, String action) {
    	return eventCommentDao.saveComment(eventId, timestamp, userId, comment, action);
    }

    @Override
    public Event getEventById(Long eventId, boolean displayDropdownLabels) {
        Event event = eventDao.getEventById(eventId);
        
        setFaultMetaExtraFields(event, displayDropdownLabels);
        
        return event;
    }
    
    protected void setFaultMetaExtraFields(Event event, boolean displayDropdownLabels) {
        Integer faultMetaId = (Integer) event.getField(Spectrum.EVENT_FAULT_META);
        if (faultMetaId != null) {
            List<FaultMetaExtraField> fmExtraFields = fmemDao.get(faultMetaId);
            
            for (FaultMetaExtraField field : fmExtraFields) {
                String label = field.getValue();
                String code = field.getField();
                event.setField(code, label);
            }
        }
    }

    @Override
    public List<EventFormation> getEventFormationByEventId(Long eventId) {
        List<EventFormation> eventFormation = eventFormationDao.getEventFormationByEventId(eventId);
        return eventFormation;
    }

    @Override
    public List<Event> getAllByUnit(Integer unitId) {
        return eventDao.allByUnit(unitId);
    }

    @Override
    public List<Event> getLiveByUnit(Integer unitId) {
        return eventDao.liveByUnit(unitId);
    }

    @Override
    public List<Event> getRecentByUnit(Integer unitId, int numberOf) {
        return eventDao.notLiveByUnit(unitId, numberOf);
    }
    
    @Override
    public List<Event> getNotAcknowledgedByUnit(Integer unitId) {
    	return eventDao.notAcknowledgedByUnit(unitId);
    }
    
    @Override
    public List<Event> getAcknowledgedByUnit(Integer unitId) {
    	return eventDao.acknowledgedByUnit(unitId);
    }

    @Override
    public List<Event> getEventsSince(long since) {
        return eventDao.getEventsSince(since);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Event> getEventsForFormation(List<Integer> unitIds) {
        List<Event> events = new ArrayList<Event>();

        for (Integer unitId : unitIds) {
            events.addAll(eventDao.liveByUnit(unitId));
            events.addAll(eventDao.notLiveByUnit(unitId, NUMBER_OF_UNIT_NOT_LIVE_EVENTS));
        }

        Collections.sort(events, SORT_BY_CREATE_TIME_DESC);

        return events;
    }

    @Override
    public List<Event> searchEvents(Date dateFrom, Date dateTo, String live, String keyword, Integer limit) {
        return eventDao.searchEvents(limit, dateFrom, dateTo, live, keyword);
    }

    @Override
    public List<Event> searchEvents(Date dateFrom, Date dateTo, String code, String type, String description, String category,
            String live, List<EventSearchParameter> parameters, Integer limit) {
        return eventDao.searchEvents(limit, dateFrom, dateTo, live, code, type, description, category, parameters);
    }

    @Override
    public ChannelData getEventChannelDataForStock(Integer eventId, Integer vehicleId, Integer groupId) {
        return new ChannelData();
    }

    @Override
    public Integer getEventCountLive(List<Object> criteria) {
        return eventDao.getEventCountLive(criteria);
    }

    @Override
    public Integer getEventCountSince(List<Object> criteria, long timestamp) {
        return eventDao.getEventCountLive(criteria);
    }
    
    @Override
    public List<Event> getCurrentPanel(Integer limit, List<Object> searchCriteria) {
    	List<Event> result = eventDao.getCurrent(limit, searchCriteria);
    	
    	return result;
    }
    
    @Override
    public Long createNewFault(Integer unitId, String faultCode, String fleetId, Long timestamp) {
    	
    	String contextId = "";
    	
    	// create a fault and immediately retract it, the rule engine will not retract a manual fault by itself
    	Long result = eventDao.createNewFault(faultCode, fleetId, timestamp, unitId, contextId);
    	eventDao.retractFault(result, timestamp);
    	
    	return result;
    }
    
    @Override
    public List<FaultCategory> getFaultCategories() {
        return faultCategoryDao.findAll();
    }

    @Override
    public List<FaultMeta> getFaultCodes() {
        return faultMetaDao.findAllPrimaryFields();
    }
    
    /**
     * @see com.nexala.spectrum.rest.data.EventProvider#getFaultMetaByCode(java.lang.String)
     */
    @Override
    public FaultMeta getFaultMetaByCode(String faultCode) {
        return faultMetaDao.findByFaultCode(faultCode);
    }

    @Override
    public List<FaultType> getFaultTypes() {
        return faultTypeDao.findAll();
    }
    
    @Override
    public List<FaultGroup> getFaultGroupNames() {
        return faultGroupDao.findAll();
    }

    @Override
    public List<EventTableBean> getExtendedEventTableData(Long eventId) {
        return Collections.<EventTableBean>emptyList();
    }

	@Override
	public boolean areFaultsRetracted(Long timestamp) {
		return this.eventDao.areFaultsRetracted(timestamp);
	}

    @Override
    public Long getLastLiveEventTimestamp() {
        return eventDao.getLastLiveEventTimestamp();
    }

    @Override
    public Long getLastRecentEventTimestamp() {
        return eventDao.getLastRecentEventTimestamp();
    }

    @Override
    public void sendFault(long eventId) {
        faultTransmissionDao.insertFaultTransmission(eventId);
    }

    // This is quite nasty and should be client-side code, in my opinion.
    // Used for Event Detail.
    protected List<Column> getColumns(ChannelGroupConfig groupConfig) {
        List<Column> columns = new ArrayList<Column>();

        columns.add(
                new TextColumnBuilder()
                    .name(Spectrum.TIMESTAMP)
                    .displayName(Spectrum.TIMESTAMP)
                    .width(140)
                    .styles(CHANNELDATA_COMMON)
                    .sortable(false)
                    .build()
        );

        for (ChannelConfig channel : groupConfig.getChannelConfigList()) {
            if (channel != null && channel.getShortName() != null) {
                int width = 20;

                if (channel.isLookup()) {
                    // FIXME - I think we should use auto widths for these columns
                    width = 100;
                } else if (channel.getType() != null && channel.getType() == ChannelType.ANALOG) {
                    width = 55;
                }
                
                // The following fiddle was used to calculated the magic numbers below: http://jsfiddle.net/Xj4qb/
                /*if (channel.getUnitOfMeasure() != null) {
                    int uomLength = channel.getUnitOfMeasure().length();
                    
                    if (uomLength > 0) {
                        width += (uomLength + 2) * 6;
                    }
                }*/

                columns.add(new TextColumnBuilder().name(channel.getName()).displayName(channel.getDescription())
                		.mouseOverHeader(channel.getComment()).width(width).styles(CHANNELDATA_COMMON).sortable(false).build());
            }
        }

        return columns;
    }

	@Override
	public void acknowledgeEvent(int eventId, Long rowVersion, Integer eventGroupId) {
	    if(eventGroupId != null){
	        eventDao.acknowledgeGroup(eventGroupId);
	    } else {
	        eventDao.acknowledge(eventId, rowVersion);	        
	    }
	}
	
	@Override
	public void deactivateEvent(int eventId, String userName, Boolean isCurrent, Long rowVersion) {
	    eventDao.deactivate(eventId, userName, isCurrent, rowVersion);
	}

    @Override
    public void createFaultGroup(String username, List<FaultGroupInputParam> params, Long leadEventId) {
        eventDao.groupFaults(username, params, leadEventId);
    }
    
    @Override
    public void updateFaultGroup(String username, List<FaultGroupInputParam> params, Long leadEventId) {
    	eventDao.updateGroup(username, params, leadEventId);
    	
    }

    @Override
    public List<Event> getEventsByGroup(Integer groupId) {
        return eventDao.getEventsByGroup(groupId);
    }
    
    @Override
    public Date getEndTime(Integer eventId) {
        return eventDao.getEndTime(eventId);
    }
    
    @Override
    public List<Event> getLiveFormationEventsByUnit(Integer unitId) {
        return eventDao.liveEventsFormationByUnit(unitId);
    }

    @Override
    public List<Event> getRecentFormationEventsByUnit(Integer unitId, int numberOf) {
        return eventDao.notLiveEventsFormationByUnit(unitId, numberOf);
    }
    @Override
  	public void saveFaultPeriod(Integer eventId, Integer vehicleId, Integer period, Long timestamp) {
      	
    	eventDao.updateSelectedPeriodValue(eventId,vehicleId,period,timestamp);
      	
  	}

	
}
