package com.nexala.spectrum.rest.data.beans;

import java.util.List;

import com.nexala.spectrum.view.conf.Field;

public class EventDetailToolbarButton {
	private final String name;
	private final String displayName;
	private final Boolean opensPopup;
	private final Integer popupHeight;
	private final Integer popupWidth;
	private final Boolean isLicenced;
	private final List<Field> popupFields;
	
	public EventDetailToolbarButton(String name, String displayName, Boolean opensPopup, Integer popupHeight, Integer popupWidth, Boolean isLicenced, List<Field> popupFields) {
		this.name = name;
		this.displayName = displayName;
		this.opensPopup = opensPopup;
		this.popupHeight = popupHeight;
		this.popupWidth = popupWidth;
		this.isLicenced = isLicenced;
		this.popupFields = popupFields;
	}

	public String getName() {
		return name;
	}

	public String getDisplayName() {
		return displayName;
	}

	public Boolean getOpensPopup() {
		return opensPopup;
	}
	
	public Integer getPopupHeight() {
		return popupHeight;
	}
	
	public Integer getPopupWidth() {
		return popupWidth;
	}

	public Boolean getIsLicenced() {
		return isLicenced;
	}
	
	public List<Field> getPopupFields() {
		return popupFields;
	}
	
}
