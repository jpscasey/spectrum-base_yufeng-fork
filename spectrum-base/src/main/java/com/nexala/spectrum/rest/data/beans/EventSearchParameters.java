package com.nexala.spectrum.rest.data.beans;

import java.util.Date;

/**
 * Defines search parameters available for the event history.
 * @author Javier Munoz
 */
public class EventSearchParameters {
    private Date dateFrom;
    private Date dateTo;
    private String headCode;
    private String unitCode;
    private String faultVehicle;
    private String faultCode;
    private String description;
    private String location;
    private String type;
    private String category;
    private String keyword;
    private String live;

    private String normaliseString(String str) {
        if(str.equals("")){
            str = null;
        }
        return str;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public String getHeadCode() {
        return headCode;
    }

    public void setHeadCode(String headCode) {
        this.headCode = normaliseString(headCode);
    }

    public String getUnitCode() {
        return unitCode;
    }

    public void setUnitCode(String unitCode) {
        this.unitCode = normaliseString(unitCode);
    }

    public String getFaultVehicle() {
        return faultVehicle;
    }

    public void setFaultVehicle(String faultVehicle) {
        this.faultVehicle = normaliseString(faultVehicle);
    }
    
    public String getFaultMetaId() {
        return faultCode;
    }

    public void setMetaId(String faultCode) {
        this.faultCode = normaliseString(faultCode);
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = normaliseString(description);
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = normaliseString(location);
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = normaliseString(type);
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = normaliseString(category);
    }

    public String getLive() {
        return live;
    }

    public void setLive(String live) {
        this.live = normaliseString(live);
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = normaliseString(keyword);
    }
}
