package com.nexala.spectrum.rest.data;

import java.util.Date;
import java.util.List;

import com.google.inject.ImplementedBy;
import com.nexala.spectrum.licensing.Licence;
import com.nexala.spectrum.rest.data.beans.Event;
import com.nexala.spectrum.rest.data.beans.EventSearchParameter;

/**
 * Provides search results for the event history screen. The implementation should return events for <em>all</em>
 * fleets.
 * @author poriordan
 */
@ImplementedBy(DefaultEventSearchProvider.class)
public interface EventSearchProvider {

    List<Event> getEventsSince(CommonEventProvider commonEventProvider, long timestamp);

    List<Event> searchEvents(CommonEventProvider commonEventProvider, Date fromDate, Date toDate, String live,
            String keyword, Integer limit, Licence licence);
    
    List<Event> searchFleetEvents(EventProvider eventProvider, Date fromDate, Date toDate, String live,
            String keyword, Integer limit);

    List<Event> searchEvents(CommonEventProvider commonEventProvider, Date dateFrom, Date dateTo, String live,
            String code, String type, String description, String category, List<EventSearchParameter> parameters,
            Integer limit, Licence licence);
    
    List<Event> searchFleetEvents(EventProvider eventProvider, Date dateFrom, Date dateTo, String live,
            String code, String type, String description, String category, List<EventSearchParameter> parameters, Integer limit);
    
}
