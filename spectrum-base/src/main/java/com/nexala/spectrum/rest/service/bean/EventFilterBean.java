package com.nexala.spectrum.rest.service.bean;

import java.util.Collection;
import java.util.List;

import com.nexala.spectrum.view.conf.FieldType;

/**
 * Bean containing filters for event. Basically severities and categories
 * @author brice
 *
 */
public class EventFilterBean {
    private String name;
    private String label;
    private Integer width;
    private FieldType fieldType;
    private String idType;
    private Collection<FilterOptionBean> options = null;
    private Collection<FilterOptionBean> defaultOptions = null;
    private Collection<String> defaultValues = null;
    private Boolean checkedAsDefault = null;
    private Boolean includeValues = null; // true to include (all ==), false to exclude (all !=)
    private List<String> fleets;
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public FieldType getFieldType() {
        return fieldType;
    }

    public void setFieldType(FieldType fieldType) {
        this.fieldType = fieldType;
    }

    public Collection<FilterOptionBean> getOptions() {
        return options;
    }
    
    public void setOptions(Collection<FilterOptionBean> options) {
        this.options = options;
    }

    public Collection<String> getDefaultValues() {
        return defaultValues;
    }

    public void setDefaultValues(Collection<String> defaultValues) {
        this.defaultValues = defaultValues;
    }

    public Collection<FilterOptionBean> getDefaultOptions() {
        return defaultOptions;
    }

    public void setDefaultOptions(Collection<FilterOptionBean> defaultOptions) {
        this.defaultOptions = defaultOptions;
    }

	public Boolean getCheckedAsDefault() {
		return checkedAsDefault;
	}

	public void setCheckedAsDefault(Boolean checkedAsDefault) {
		this.checkedAsDefault = checkedAsDefault;
	}

	public Boolean getIncludeValues() {
		return includeValues;
	}

	public void setIncludeValues(Boolean includeValues) {
		this.includeValues = includeValues;
	}

	public String getIdType() {
		return idType;
	}

	public void setIdType(String idType) {
		this.idType = idType;
	}

    public List<String> getFleets() {
        return fleets;
    }

    public void setFleets(List<String> fleets) {
        this.fleets = fleets;
    }
}
