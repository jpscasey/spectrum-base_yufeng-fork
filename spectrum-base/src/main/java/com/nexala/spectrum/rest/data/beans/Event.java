package com.nexala.spectrum.rest.data.beans;

import java.util.Map;

/**
 * Event (fault / warning / event)
 * 
 * @author antonio
 */
public class Event extends GenericBean {

    public void setFields(Map<String, Object> fields) {
        this.fields = fields;
    }
}