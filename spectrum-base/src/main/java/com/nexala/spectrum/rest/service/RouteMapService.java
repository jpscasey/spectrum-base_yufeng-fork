package com.nexala.spectrum.rest.service;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.nexala.spectrum.rest.cache.Cache;
import com.nexala.spectrum.rest.data.RouteMapProvider;
import com.nexala.spectrum.rest.data.beans.route.Route;
import com.nexala.spectrum.rest.data.beans.route.RouteView;

/**
 * Provide services for the route map view.
 * @author brice
 *
 */
@Path("/routemap/")
@Singleton
public class RouteMapService {
    
    
    @Inject(optional=true)
    private RouteMapProvider routeMapProvider;

    @GET
    @Cache
    @Path("/routeview/{routeId}/")
    @Produces(MediaType.APPLICATION_JSON)
    public RouteView getRouteViewComponents(@PathParam("routeId") String routeId) {
        
        if (routeMapProvider == null) {
            return null;
        }
        
        RouteView result = routeMapProvider.getRouteView(routeId);

        return result;
    }
    
    @GET
    @Cache
    @Path("/allroutes/")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Route> getAllRoutes() {
        
        if (routeMapProvider == null) {
            return null;
        }

        return routeMapProvider.getAllRoutes();
    }
    
    
    
}
