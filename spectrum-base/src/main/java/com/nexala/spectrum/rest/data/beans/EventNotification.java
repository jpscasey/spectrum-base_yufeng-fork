/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.rest.data.beans;

public final class EventNotification {
    /**
     * The number of currently live events
     */
    private final int eventCount;

    /**
     * Whether or not new events have been created since the last user request.
     */
    private final boolean newEvents;
    
    /**
     * Contain timestamp of the last event
     */
    private final Long lastEvent;

    public EventNotification(int eventCount, boolean newEvents, Long lastEvent) {
        this.eventCount = eventCount;
        this.newEvents = newEvents;
        this.lastEvent = lastEvent;
    }

    public int getEventCount() {
        return eventCount;
    }

    public boolean getNewEvents() {
        return newEvents;
    }

    /**
     * Getter for lastEvent.
     * @return the lastEvent
     */
    public Long getLastEvent() {
        return lastEvent;
    }
}