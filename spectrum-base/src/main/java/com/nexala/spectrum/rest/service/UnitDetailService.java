package com.nexala.spectrum.rest.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.google.common.collect.Lists;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.db.beans.FaultCategory;
import com.nexala.spectrum.db.beans.FaultType;
import com.nexala.spectrum.licensing.DataLicensor;
import com.nexala.spectrum.licensing.Licence;
import com.nexala.spectrum.licensing.LoggingData;
import com.nexala.spectrum.rest.cache.Cache;
import com.nexala.spectrum.rest.data.ChannelDataProvider;
import com.nexala.spectrum.rest.data.CommonEventProvider;
import com.nexala.spectrum.rest.data.EventProvider;
import com.nexala.spectrum.rest.data.UnitDetailProvider;
import com.nexala.spectrum.rest.data.beans.DataSet;
import com.nexala.spectrum.rest.data.beans.Event;
import com.nexala.spectrum.rest.data.beans.GenericBean;
import com.nexala.spectrum.rest.data.beans.Restriction;
import com.nexala.spectrum.rest.data.beans.UnitDetail;
import com.nexala.spectrum.rest.security.TimeoutFree;
import com.nexala.spectrum.rest.service.bean.EventFilterBean;
import com.nexala.spectrum.rest.service.bean.FilterOptionBean;
import com.nexala.spectrum.rest.service.bean.UnitDetailCell;
import com.nexala.spectrum.rest.service.bean.UnitDetailRow;
import com.nexala.spectrum.view.conf.unit.UnitDetailConfiguration;

@Path("/unitdetail/")
@Singleton
public class UnitDetailService {
    @Inject
    private Map<String, UnitDetailConfiguration> unitConfs;

    @Inject
    private Map<String, ChannelDataProvider> channelDataProviders;

    @Inject
    private Map<String, UnitDetailProvider> unitDetailProvider;

    @Inject
    private Map<String, EventProvider> eventProviders;
    
    @Inject
    private CommonEventProvider commonEventProvider;

    @Inject
    private DataLicensor dataLicensor;

    @GET
    @Cache
    @Path("/conf/{fleetId}")
    @Produces(MediaType.APPLICATION_JSON)
    public UnitDetailConfiguration conf(@PathParam("fleetId") String fleetId, @Context HttpServletRequest request) throws IOException {
        UnitDetailConfiguration configuration = unitConfs.get(fleetId);
        
        for (UnitDetailRow row : configuration.getUdPanelRows()) {
            for (UnitDetailCell cell : row.getCells()) {
                List<EventFilterBean> filters = new ArrayList<EventFilterBean>();
                for (EventFilterBean filter : cell.getFilters()) { 
                    if(filter.getFleets() == null || filter.getFleets().contains(fleetId)){
                        filters.add(filter);
                        
                        Collection<FilterOptionBean> options = null;
                        if (filter.getName().equals(Spectrum.UNIT_DETAIL_CATEGORY_FILTER)) {
                            options = getCategoriesAsFilters(getEventCategories());
                        } else if (filter.getName().equals(Spectrum.UNIT_DETAIL_FAULT_TYPE_FILTER)) {
                            options = getFaultTypesAsFilters(getFaultTypes());
                        }
                        filter.setOptions(options); 
                    } 
                }
                cell.setFilters(filters);
            }
        }
        
        return configuration;
    }

    @GET
    @LoggingData
    @TimeoutFree
    @Path("/data/{fleetId}/{unitId}/{timestamp}")
    @Produces(MediaType.APPLICATION_JSON)
    public UnitDetail data(@PathParam("fleetId") String fleetId, @PathParam("unitId") int stockId,
            @PathParam("timestamp") long timestamp) {
        UnitDetail result = unitDetailProvider.get(fleetId).getUnitDetail(stockId, timestamp);
                
        // Apply data licensor to events if not empty
        List<Event> live = result.getLive();
        List<Event> recent = result.getRecent();
        
        if (live != null) {
            result.setLive(dataLicensor.getLicenced(live));
        }
        
        if (recent != null) {
            result.setRecent(dataLicensor.getLicenced(recent));
        }
        
        return result;
    }

    @GET
    @LoggingData
    @Path("/events/all/{fleetId}/{unitId}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Event> allEvents(@PathParam("fleetId") String fleetId, @PathParam("unitId") int unitId) {
        return dataLicensor.getLicenced(eventProviders.get(fleetId).getAllByUnit(unitId));
    }

    @GET
    @LoggingData
    @Path("/events/live/{fleetId}/{unitId}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Event> liveEvents(@PathParam("fleetId") String fleetId, @PathParam("unitId") int unitId) {
        return dataLicensor.getLicenced(eventProviders.get(fleetId).getLiveByUnit(unitId));
    }

    @GET
    @LoggingData
    @Path("/events/recent/{fleetId}/{unitId}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Event> recentEvents(@PathParam("fleetId") String fleetId, @PathParam("unitId") int unitId) {
        return dataLicensor.getLicenced(eventProviders.get(fleetId).getRecentByUnit(unitId,
                Spectrum.UNIT_DETAIL_NUMBER_OF_RECENT_EVENTS));
    }
    
    @GET
    @LoggingData
    @Path("/events/liveFormation/{fleetId}/{unitId}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Event> liveFormationEvents(@PathParam("fleetId") String fleetId, @PathParam("unitId") int unitId) {
        return dataLicensor.getLicenced(eventProviders.get(fleetId).getLiveFormationEventsByUnit(unitId));
    }
    
    @GET
    @LoggingData
    @Path("/events/recentFormation/{fleetId}/{unitId}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Event> recentFormationEvents(@PathParam("fleetId") String fleetId, @PathParam("unitId") int unitId) {
        return dataLicensor.getLicenced(eventProviders.get(fleetId).getRecentFormationEventsByUnit(unitId,
                Spectrum.UNIT_DETAIL_NUMBER_OF_RECENT_EVENTS));
    }
    
    @GET
    @LoggingData
    @Path("/events/notacknowledged/{fleetId}/{unitId}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Event> notAcknowledgedEvents(@PathParam("fleetId") String fleetId, @PathParam("unitId") int unitId) {
    	return dataLicensor.getLicenced(eventProviders.get(fleetId).getNotAcknowledgedByUnit(unitId));
    }
    
    @GET
    @LoggingData
    @Path("/events/acknowledged/{fleetId}/{unitId}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Event> acknowledgedEvents(@PathParam("fleetId") String fleetId, @PathParam("unitId") int unitId) {
    	return dataLicensor.getLicenced(eventProviders.get(fleetId).getAcknowledgedByUnit(unitId));
    }

    @GET
    @LoggingData
    @Path("/restrictions/all/{fleetId}/{unitId}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<GenericBean> allRestrictions(@PathParam("fleetId") String fleetId, @PathParam("unitId") int unitId) {
        return unitDetailProvider.get(fleetId).getRestrictions(unitId);
    }

    @GET
    @LoggingData
    @Path("/restrictions/live/{fleetId}/{unitId}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Restriction> liveRestrictions(@PathParam("fleetId") String fleetId, @PathParam("unitId") int unitId) {
        // TODO implement restrictions...
        // XXX It's questionable whether restrictions should be part of spectrum base
        return Lists.newArrayList();
    }

    @GET
    @LoggingData
    @Path("/restrictions/recent/{fleetId}/{unitId}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Restriction> recentRestrictions(@PathParam("fleetId") String fleetId, @PathParam("unitId") int unitId) {
        // TODO implement restrictions...
        // XXX It's questionable whether restrictions should be part of spectrum base
        return Lists.newArrayList();
    }
    
    @GET
    @LoggingData
    @Path("/workOrders/{fleetId}/{unitId}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<GenericBean> workOrders(@PathParam("fleetId") String fleetId, @PathParam("unitId") int unitId) {
        return unitDetailProvider.get(fleetId).getWorkOrders(unitId);
    }
    
    @GET
    @LoggingData
    @Path("/workOrderComment/{fleetId}/{woNumber}")
    @Produces(MediaType.APPLICATION_JSON)
    public String workOrderComment(@PathParam("fleetId") String fleetId, @PathParam("woNumber") int woNumber) {
        return unitDetailProvider.get(fleetId).getWorkOrderComment(woNumber);
    }

    @GET
    @LoggingData
    @TimeoutFree
    @Path("/vehiclesTimestamp/{fleetId}/{unitId}/{timestamp}")
    @Produces(MediaType.APPLICATION_JSON)
    public DataSet getVehiclesTimestamp(@PathParam("fleetId") String fleetId, @PathParam("unitId") int unitId,
            @PathParam("timestamp") long timestamp) {

        List<DataSet> data = channelDataProviders.get(fleetId).getUnitData(unitId, timestamp, Spectrum.RECORD_BY_TIME);

        return unitDetailProvider.get(fleetId).getVehiclesTimestamp(data);
    }

    /**
     * Return the unit label for an unit id.
     * @param unitId the unit id
     * @param fleetId the fleet id
     * @param timestamp
     * @return the unit label for an unit
     */
    @GET
    @LoggingData
    @Path("/data/info/{fleetId}/{unitId}/{timestamp}")
    @Produces(MediaType.TEXT_HTML)
    public String getUnitInfoLabel(@PathParam("fleetId") String fleetId, @PathParam("unitId") int unitId,
            @PathParam("timestamp") long timestamp) {

        List<DataSet> data = channelDataProviders.get(fleetId).getUnitData(unitId, timestamp, Spectrum.RECORD_BY_TIME);

        return unitDetailProvider.get(fleetId).getUnitInfoLabel(data);
    }
    
    @GET
    @Path("/haslicence/{licenceKey}")
    @Produces(MediaType.APPLICATION_JSON)
    public Boolean hasLicence(@PathParam("licenceKey") String licenceKey, @Context HttpServletRequest request
            , @Context HttpServletResponse response) {
        boolean hasLicence = false;
        Licence licence = Licence.get(request, response, true);
        
        if (licence.hasResourceAccess(licenceKey)) {
            hasLicence = true;
        }
        
        return hasLicence;
    }
    
    private Collection<FilterOptionBean> getCategoriesAsFilters(Collection<FaultCategory> categories) throws IOException {
        List<FilterOptionBean> result = new ArrayList<FilterOptionBean>();
        for (FaultCategory category : categories) {
            FilterOptionBean fob = new FilterOptionBean();
            fob.setDisplayName(category.getCategory());
            fob.setId(category.getId().toString());
            result.add(fob);                
        }
        
        return result;
    }
    
    private Collection<FaultCategory> getEventCategories() {
        Map<String, FaultCategory> categories = new LinkedHashMap<String, FaultCategory>();

        for (FaultCategory s : commonEventProvider.getLiveAndRecentCategories()) {
            if (!categories.containsKey(s)) {
                categories.put(s.getCategory(), s);
            }
        }

        return categories.values();
    }
    
    private Collection<FilterOptionBean> getFaultTypesAsFilters(Collection<FaultType> faultTypes) throws IOException {
        List<FilterOptionBean> result = new ArrayList<FilterOptionBean>();
        for (FaultType faultType : faultTypes) {
            FilterOptionBean fob = new FilterOptionBean();
            fob.setDisplayName(faultType.getName());
            fob.setId(faultType.getId().toString());
            result.add(fob);                
        }
        
        return result;
    }
    
    private Collection<FaultType> getFaultTypes() {
        Map<String, FaultType> categories = new LinkedHashMap<String, FaultType>();

        for (FaultType s : commonEventProvider.getAllFleetsFaultTypes()) {
            if (!categories.containsKey(s)) {
                categories.put(s.getName(), s);
            }
        }

        return categories.values();
    }
    
    
}
