/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.rest.data.beans;

import java.util.ArrayList;
import java.util.List;

import com.nexala.spectrum.db.beans.Unit;
import com.nexala.spectrum.db.beans.Vehicle;

public class Formation {
    private final Unit unit;
    private final List<Vehicle> vehicles = new ArrayList<Vehicle>();

    public Formation(Unit unit) {
        this.unit = unit;
    }

    public Unit getUnit() {
        return unit;
    }

    public List<Vehicle> getVehicles() {
        return vehicles;
    }

    public void addVehicle(Vehicle vehicle) {
        vehicles.add(vehicle);
    }
}