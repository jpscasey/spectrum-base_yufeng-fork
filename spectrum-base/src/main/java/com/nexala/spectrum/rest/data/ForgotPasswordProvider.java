/**
 * Copyright (c) Nexala Technologies 2015, All rights reserved.
 */
package com.nexala.spectrum.rest.data;


/**
 * Provider for the forgot password feature.
 * @author bbaudry
 *
 */
public interface ForgotPasswordProvider {
    
    /**
     * Send an email to the email provided if it match one user
     * @param email the email adress we need to send to
     * @param host the host name of the app for which the query was done
     * @return true if the email was sent, false otherwise.
     */
    public boolean sendForgotPasswordEmail(String email, String host) throws Exception;
    
    
    /**
     * Change the user password
     * @param username the username
     * @param token the token
     * @param newPassword the new password
     * @param newPassword the new password confirmed
     * @throws Exception in case of exception
     */
    public void changePassword(String username, String token, String newPassword, String confirmPassword) throws Exception;
    
}
