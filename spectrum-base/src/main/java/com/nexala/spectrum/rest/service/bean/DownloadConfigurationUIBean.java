package com.nexala.spectrum.rest.service.bean;

import java.util.List;

import com.nexala.spectrum.rest.data.beans.BeanField;
import com.nexala.spectrum.view.conf.Column;
import com.nexala.spectrum.view.conf.Field;
import com.nexala.spectrum.view.conf.FieldBuilder;
import com.nexala.spectrum.view.conf.FileType;


public class DownloadConfigurationUIBean {
    
    private List<Column> columns = null;
    
    private List<FileType> fileTypes = null;
    
    private List<BeanField> downloadFields = null;
    
    private List<Field> popupFilters = null;
    
    private List<FieldBuilder> ruleEditorDRFilters = null;
    
    private int thresholdTime;
    
    private int gapTime;
    
    private Boolean vehicleSelector = false;
    
    private Boolean retrievable = false;
    
    public DownloadConfigurationUIBean() {
    	
    }

	public List<Column> getColumns() {
		return columns;
	}

	public void setColumns(List<Column> columns) {
		this.columns = columns;
	}

	public List<FileType> getFileTypes() {
		return fileTypes;
	}

	public void setFileTypes(List<FileType> fileTypes) {
		this.fileTypes = fileTypes;
	}

	public List<BeanField> getDownloadFields() {
		return downloadFields;
	}

	public void setDownloadFields(List<BeanField> downloadFields) {
		this.downloadFields = downloadFields;
	}

	public List<Field> getPopupFilters() {
		return popupFilters;
	}

	public void setPopupFilters(List<Field> popupFilters) {
		this.popupFilters = popupFilters;
	}	

	public List<FieldBuilder> getRuleEditorDRFilters() {
		return ruleEditorDRFilters;
	}

	public void setRuleEditorDRFilters(List<FieldBuilder> ruleEditorDRFilters) {
		this.ruleEditorDRFilters = ruleEditorDRFilters;
	}

	public int getThresholdTime() {
		return thresholdTime;
	}

	public void setThresholdTime(int thresholdTime) {
		this.thresholdTime = thresholdTime;
	}

	public int getGapTime() {
		return gapTime;
	}

	public void setGapTime(int gapTime) {
		this.gapTime = gapTime;
	}

	public Boolean getVehicleSelector() {
		return vehicleSelector;
	}

	public void setVehicleSelector(Boolean vehicleSelector) {
		this.vehicleSelector = vehicleSelector;
	}

	public Boolean getRetrievable() {
		return retrievable;
	}

	public void setRetrievable(Boolean retrievable) {
		this.retrievable = retrievable;
	}
}
