/**
 * 
 */
package com.nexala.spectrum.rest.service.bean;

import com.nexala.spectrum.view.conf.ColumnType;


/**
 * A bean holding the data for a row in the custom event detail 
 * @author bbaudry
 *
 */
public class EventTableBean {
    
    private String label;
    
    private Object value;
    
    private ColumnType columnType;

    public EventTableBean(String label, String value) {
        setLabel(label);
        setValue(value);
        setColumnType(ColumnType.TEXT);
    }
    
    
    public EventTableBean(String label, Object value, ColumnType type) {
        setLabel(label);
        setValue(value);
        setColumnType(type);
    }

    /**
     * Get the label.
     * @return the label
     */
    public String getLabel() {
        return label;
    }

    /**
     * Set the label.
     * @param label the label to set
     */
    public void setLabel(String label) {
        this.label = label;
    }

    /**
     * Get the value.
     * @return the value
     */
    public Object getValue() {
        return value;
    }

    /**
     * Set the value.
     * @param value the value to set
     */
    public void setValue(Object value) {
        this.value = value;
    }

    /**
     * Get the columnType.
     * @return the columnType
     */
    public ColumnType getColumnType() {
        return columnType;
    }

    /**
     * Set the columnType.
     * @param columnType the columnType to set
     */
    public void setColumnType(ColumnType columnType) {
        if (columnType == ColumnType.GROUP || columnType == ColumnType.STOCK) {
            throw new UnsupportedOperationException("This column type is not supported");
        }
        
        this.columnType = columnType;
    }
    
    
    

}
