/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.rest.data;

import java.util.List;

import com.nexala.spectrum.rest.data.beans.DataSet;
import com.nexala.spectrum.rest.data.beans.StockDesc;

public interface CabDataProvider {
    /**
     * Returns the data for the specified vehicle and timestamp; if no data
     * exists for the specified time, it will return the nearest record from the
     * 5 seconds preceding the timestamp.
     * 
     * @param vehicleId
     * @param timestamp
     *            If null specified, gets the latest data for the vehicle.
     * @return The record for the specified parameters, or null if no matching
     *         record can be found.
     */
    DataSet getVehicleData(int vehicleId, int fleetFormationId, Long prevTimestamp, Long timestamp);

    List<StockDesc> getStockForUnit(int fleetFormationId);
}
