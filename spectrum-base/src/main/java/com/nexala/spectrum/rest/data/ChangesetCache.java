/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.rest.data;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.google.inject.Singleton;
import com.nexala.spectrum.rest.data.beans.DataSet;

@Singleton
public class ChangesetCache {
    
    // timestamp1 -> (timestamp2 -> list<vehicle>).
    private Map<Long, Map<Long, List<DataSet>>> cache = new LinkedHashMap<Long, Map<Long, List<DataSet>>>();
    
    public boolean exists(long timestamp1, long timestamp2) {
        Map<Long, List<DataSet>> map = cache.get(timestamp1);
        
        if (map == null || !map.containsKey(timestamp2)) {
            return false;
        }

        return true;
    }
    
    public void add(long timestamp1, long timestamp2, List<DataSet> changesets) {
        throw new UnsupportedOperationException();
        /*
        if (!cache.containsKey(timestamp1)) {
            cache.put(timestamp1, new LinkedHashMap<Long, List<Vehicle>>());
        }*/
    }
}
