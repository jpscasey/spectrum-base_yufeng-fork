package com.nexala.spectrum.rest.service;

import java.io.IOException;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.nexala.spectrum.rest.cache.Cache;
import com.nexala.spectrum.view.conf.UnitConfiguration;

@Path("/unitsummary/")
@Singleton
public class UnitSummaryService {
    @Inject
    private Map<String, UnitConfiguration> unitConfs;

    @GET
    @Cache
    @Path("/conf/{fleetId}")
    @Produces(MediaType.APPLICATION_JSON)
    public UnitConfiguration conf(@PathParam("fleetId") String fleetId, @Context HttpServletRequest request) throws IOException {
        UnitConfiguration configuration = unitConfs.get(fleetId);
        
        return configuration;
    }
}
