package com.nexala.spectrum.rest.data;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.nexala.spectrum.db.beans.ChannelConfig;
import com.nexala.spectrum.db.beans.LookupValue;

@Singleton
public class ChannelLookupProvider {
    @Inject
    private ChannelLookupTableCache cache;

    public List<Integer> getLookupChannelIds() {
        return new ArrayList<Integer>(cache.getLookupValues().keySet());
    }

    /**
     * Retrieves a LookupValue by the channel config and lookup value
     * 
     * @param ChannelConfig
     *            channelConfig
     * @param lookupValue
     * @return LookupValue
     */
    public LookupValue get(ChannelConfig channelConfig, String value) {
        if (channelConfig.isLookup())
        	return get(channelConfig.getId(), value);
        else
        	return null;
    }

    /**
     * Retrieves a LookupValue by the channel id and lookup value
     * 
     * @param channelId
     * @param lookupValue
     * @return LookupValue
     */
    public LookupValue get(int channelId, String value) {
        Map<String, LookupValue> map = cache.getLookupValues().get(channelId);

        if (map != null) {
            return map.get(value);
        }

        return null;
    }

    public List<LookupValue> get(int channelId) {
        Map<String, LookupValue> map = cache.getLookupValues().get(channelId);

        if (map != null) {
            return new ArrayList<LookupValue>(map.values());
        }

        return null;
    }
}
