/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.rest.data;

import java.util.ArrayList;
import java.util.List;

import com.google.inject.Inject;
import com.nexala.spectrum.db.beans.ChannelConfig;
import com.nexala.spectrum.db.beans.ChannelDefinition;
import com.nexala.spectrum.db.beans.ChannelDefinitionParam;
import com.nexala.spectrum.db.beans.ChannelGroupConfig;
import com.nexala.spectrum.db.dao.ChannelConfigDao;
import com.nexala.spectrum.db.dao.ChannelGroupConfigDao;
import com.nexala.spectrum.validation.ChannelRule;
import com.nexala.spectrum.validation.ChannelRuleDao;
import com.nexala.spectrum.validation.ChannelStatus;
import com.nexala.spectrum.validation.ChannelStatusDao;

public class DefaultChannelDefinitionProvider implements ChannelDefinitionProvider {
    @Inject
    private ChannelConfigDao configDao;

    @Inject
    private ChannelGroupConfigDao groupDao;

    @Inject
    private ChannelStatusDao statusDao;

    @Inject
    private ChannelRuleDao ruleDao;

    @Override
    public List<ChannelDefinition> search(String keyword) {
        List<ChannelConfig> configList = configDao.findByKeyword(keyword);
        List<ChannelGroupConfig> groupList = getGroups();
        List<ChannelRule> ruleList = ruleDao.findAll();

        List<ChannelDefinition> definitionList = new ArrayList<ChannelDefinition>();

        for (ChannelConfig config : configList) {
            definitionList.add(new ChannelDefinition(config, getGroup(config.getChannelGroupId(), groupList), getRules(
                    config.getId(), ruleList)));
        }

        return definitionList;
    }

    @Override
    public ChannelDefinition getChannelDefinition(int channelId) {
        ChannelConfig config = configDao.findById(channelId);
        ChannelGroupConfig groupConfig = groupDao.findById(config.getChannelGroupId());
        List<ChannelRule> ruleList = ruleDao.findByChannelId(config.getId());

        return new ChannelDefinition(config, groupConfig, ruleList);
    }

    @Override
    public List<ChannelGroupConfig> getGroups() {
        return groupDao.findAll();
    }

    @Override
    public List<ChannelStatus> getStatus() {
        return statusDao.findAll();
    }

    @Override
    public boolean save(String params) {
        return configDao.save(params);
    }

    private ChannelGroupConfig getGroup(int groupId, List<ChannelGroupConfig> groupList) {
        if (groupList != null) {
            for (ChannelGroupConfig conf : groupList) {
                if (conf.getId().equals(groupId)) {
                    return conf;
                }
            }
        }

        return null;
    }

    private List<ChannelRule> getRules(int channelId, List<ChannelRule> allRules) {
        List<ChannelRule> rules = new ArrayList<ChannelRule>();

        if (allRules == null) {
            return rules;
        }

        for (ChannelRule rule : allRules) {
            if (rule.getChannel().getId().equals(channelId)) {
                rules.add(rule);
            }
        }

        return rules;
    }

    @Override
    public List<ChannelConfig> getChannelConfigs() {
        return configDao.findAll();
    }
}
