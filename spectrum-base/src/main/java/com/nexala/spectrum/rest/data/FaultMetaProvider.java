package com.nexala.spectrum.rest.data;

import java.util.List;

import com.nexala.spectrum.db.beans.FaultMetaDto;

public interface FaultMetaProvider {
    /**
     * Update FaultMeta, FaultMetaExtraFields for each fleet in potentially multiple databases
     * @param faultFields the fields to be saved for FaultMeta and FaultMetaExtraFields
     * @return the id of the FaultMeta
     */
    public Integer updateFaultMeta(FaultMetaDto faultMetaDto);
    
    public Integer createFaultMeta(FaultMetaDto faultMetaDto, Integer newFaultId);

    public void deleteFaultMeta(FaultMetaDto faultMetaDto);
    
    public void fillDeleteFaultMetaQueryAndParam(FaultMetaDto faultMetaDto, List<String> queries, List<Object> parameters);

	public int deleteFaultMetaWithTransaction(List<String> queries, List<Object> parameters);
}