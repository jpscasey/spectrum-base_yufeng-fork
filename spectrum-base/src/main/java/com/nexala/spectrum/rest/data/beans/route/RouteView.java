package com.nexala.spectrum.rest.data.beans.route;

import java.util.List;

/**
 * Represent a route view.
 * Contain the list of depots and list of stations. 
 * @author brice
 *
 */
// FIXME rename this class
public class RouteView {

    private Route route = null;
    
    /**
     * Length of the route.
     * For example for a route length of 100, 
     * a station with the offset 100 will be a the end of the route.
     */
    private Double routeLength = null;
    
    private List<Station> stations = null;
    
    private List<Depot> depots = null;

    /**
     * Getter for stations.
     * @return the stations
     */
    public List<Station> getStations() {
        return stations;
    }

    /**
     * Setter for stations.
     * @param stations the stations to set
     */
    public void setStations(List<Station> stations) {
        this.stations = stations;
    }

    /**
     * Getter for depots.
     * @return the depots
     */
    public List<Depot> getDepots() {
        return depots;
    }

    /**
     * Setter for depots.
     * @param depots the depots to set
     */
    public void setDepots(List<Depot> depots) {
        this.depots = depots;
    }

    /**
     * Getter for route.
     * @return the route
     */
    public Route getRoute() {
        return route;
    }

    /**
     * Setter for route.
     * @param route the route to set
     */
    public void setRoute(Route route) {
        this.route = route;
    }

    /**
     * Getter for routeLength.
     * @return the routeLength
     */
    public Double getRouteLength() {
        return routeLength;
    }

    /**
     * Setter for routeLength.
     * @param routeLength the routeLength to set
     */
    public void setRouteLength(Double routeLength) {
        this.routeLength = routeLength;
    }


}
