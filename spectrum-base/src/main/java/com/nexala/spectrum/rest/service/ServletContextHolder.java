package com.nexala.spectrum.rest.service;

import javax.servlet.ServletContext;

/**
 * Hold the reference to the servletContext.
 * @author brice
 *
 */
public class ServletContextHolder {
    
    private static final ServletContextHolder instance = new ServletContextHolder();
    
    private ServletContext servletContext;

    /**
     * Get the servletContext.
     * @return the servletContext
     */
    public ServletContext getServletContext() {
        return servletContext;
    }

    /**
     * Set the servlet context.
     * @param servletContext the servletContext to set
     */
    public void setServletContext(ServletContext servletContext) {
        this.servletContext = servletContext;
    }

    /**
     * Get the instance
     * @return the instance
     */
    public static ServletContextHolder getInstance() {
        return instance;
    }

    

}
