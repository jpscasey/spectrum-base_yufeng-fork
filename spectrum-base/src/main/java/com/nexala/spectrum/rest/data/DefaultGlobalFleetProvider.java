package com.nexala.spectrum.rest.data;

import java.util.List;

import com.google.inject.Inject;
import com.nexala.spectrum.db.beans.Fleet;
import com.nexala.spectrum.db.dao.FleetDao;

public class DefaultGlobalFleetProvider implements GlobalFleetProvider {
	
	@Inject
	FleetDao fDao;

	@Override
	public List<Fleet> getFleets() {
		return fDao.findAll();
	}

}
