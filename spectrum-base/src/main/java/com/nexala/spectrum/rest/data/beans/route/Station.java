package com.nexala.spectrum.rest.data.beans.route;

/**
 * A station on the route map.
 * @author brice
 *
 */
public class Station {
    
    private String description = null;
    
    private String code = null;
    
    private Double latitude = null;
    
    private Double longitude = null;
    
    /**
     * offset from the beginning of the route.
     */
    private Double offset = null;

    /**
     * Getter for description.
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Setter for description.
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Getter for code.
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * Setter for code.
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * Getter for latitude.
     * @return the latitude
     */
    public Double getLatitude() {
        return latitude;
    }

    /**
     * Setter for latitude.
     * @param latitude the latitude to set
     */
    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    /**
     * Getter for longitude.
     * @return the longitude
     */
    public Double getLongitude() {
        return longitude;
    }

    /**
     * Setter for longitude.
     * @param longitude the longitude to set
     */
    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    /**
     * Getter for offset.
     * @return the offset
     */
    public Double getOffset() {
        return offset;
    }

    /**
     * Setter for offset.
     * @param offset the offset to set
     */
    public void setOffset(Double offset) {
        this.offset = offset;
    }
    


}
