/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.rest.service;

import static com.nexala.spectrum.view.conf.ChannelDataGridStyle.CHANNELDATA_COMMON;
import static com.nexala.spectrum.view.conf.ChannelDataGridStyle.CHANNELDATA_TIMESTAMP;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.apache.log4j.Logger;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.lowagie.text.DocumentException;
import com.nexala.spectrum.db.beans.ChannelConfig;
import com.nexala.spectrum.db.dao.charting.ChannelType;
import com.nexala.spectrum.db.dao.charting.ChartChannel;
import com.nexala.spectrum.db.dao.charting.DataViewConfiguration;
import com.nexala.spectrum.exportpdf.AnalysisCreatePdf;
import com.nexala.spectrum.i18n.ResourceBundleUTF8;
import com.nexala.spectrum.licensing.Licence;
import com.nexala.spectrum.licensing.LoggingData;
import com.nexala.spectrum.rest.cache.Cache;
import com.nexala.spectrum.rest.data.ChannelDataProvider;
import com.nexala.spectrum.rest.data.ChartDataProvider;
import com.nexala.spectrum.rest.data.CommonChartProvider;
import com.nexala.spectrum.rest.data.beans.ChannelData;
import com.nexala.spectrum.rest.data.beans.DataSet;
import com.nexala.spectrum.rest.security.TimeoutFree;
import com.nexala.spectrum.utils.DateUtils;
import com.nexala.spectrum.view.conf.AppConfiguration;
import com.nexala.spectrum.view.conf.ChartConfiguration;
import com.nexala.spectrum.view.conf.Column;
import com.nexala.spectrum.view.conf.ExportConfiguration;
import com.nexala.spectrum.view.conf.TextColumnBuilder;
import com.nexala.spectrum.view.conf.unit.DataExportConfiguration;
import com.nexala.spectrum.view.conf.unit.DataPlotConfiguration;

import au.com.bytecode.opencsv.CSVWriter;

@Path("/chart/")
@Singleton
public class ChartService {
	
	@Inject
	private AppConfiguration appConf;
	
	@Inject
	private DataPlotConfiguration dataPlotConfiguration;

    @Inject
    private Map<String, ChannelDataProvider> cdProviders;

    @Inject
    private Map<String, ChartDataProvider> chartProviders;

    @Inject
    private DataExportConfiguration dataExportConfiguration;
    
    @Inject
    private CommonChartProvider commonChartProvider;

    private static final Logger LOGGER = Logger.getLogger(ChartService.class);
    
    @GET
    @Cache
    @Path("/system/{fleetId}")
    @Produces(MediaType.APPLICATION_JSON)
    @LoggingData
    public List<DataViewConfiguration> getSystemCharts(
            @PathParam("fleetId") String fleetId) {

        return chartProviders.get(fleetId).findSystemConfigurations();
    }

    @GET
    @Path("/user/{fleetId}")
    @Produces(MediaType.APPLICATION_JSON)
    @LoggingData
    public List<DataViewConfiguration> getUserCharts(
            @PathParam("fleetId") String fleetId,
            @Context HttpServletRequest request,
            @Context HttpServletResponse response) {
        Licence licence = Licence.get(request, response, true);

        List<DataViewConfiguration> configurations = chartProviders.get(fleetId).findUserConfigurations(licence);

        return configurations;
    }

    @GET
    @Path("/channels/{fleetId}/{chartId: [0-9]+}")
    @Produces(MediaType.APPLICATION_JSON)
    @LoggingData
    public List<Integer> getChannels(@PathParam("fleetId") String fleetId,
            @PathParam("chartId") long chartId) {
        
        List<ChartChannel> channels = chartProviders.get(fleetId).findChannels(chartId);

        List<Integer> channelIds = new ArrayList<Integer>();

        if (channels != null) {
            for (ChartChannel channel : channels) {
                channelIds.add(channel.getChannel().getId());
            }
        }
        
        return channelIds;
    }

    @GET
    @Path("/save/{fleetId}/{chartId: [0-9]+}/{chartName: .+}/{channels: [a-zA-Z0-9]+(,[a-zA-Z0-9]+)*}")
    @Produces(MediaType.APPLICATION_JSON)
    @LoggingData
    public DataViewConfiguration saveExisting(
            @PathParam("fleetId") String fleetId,
            @PathParam("chartId") Long chartId,
            @PathParam("chartName") String chartName,
            @PathParam("channels") String channelList,
            @Context HttpServletRequest request,
            @Context HttpServletResponse response) {

        String username = Licence.get(request, response, true).getLoginUser().getLogin();

        // Parse the channel ids..
        List<Integer> channelIds = new ArrayList<Integer>();

        for (String channel : channelList.split(",")) {
            channelIds.add(Integer.parseInt(channel));
        }

        // If the user has specified a chart ID, then they are updating
        // an existing chart AND the chartName matches the specified chartName.
        if (chartId != null) {
            if (chartProviders.get(fleetId).findChart(chartId, chartName, username) != null) {
                chartProviders.get(fleetId).saveChart(chartId, channelIds, username);
                return null;
            }
        }

        // Otherwise - creating a new configuration.
        return chartProviders.get(fleetId).createChart(chartName, channelIds, username);
    }

    @GET
    @Path("/save/{fleetId}/{chartName: .+}/{channels: [a-zA-Z0-9]+(,[a-zA-Z0-9]+)*}")
    @Produces(MediaType.APPLICATION_JSON)
    @LoggingData
    public DataViewConfiguration saveNew(@PathParam("fleetId") String fleetId,
            @PathParam("chartName") String chartName,
            @PathParam("channels") String channelList,
            @Context HttpServletRequest request,
            @Context HttpServletResponse response) {
        String userId = Licence.get(request, response, true).getLoginUser().getLogin();

        // Parse the channel ids..
        List<Integer> channelIds = new ArrayList<Integer>();

        for (String channel : channelList.split(",")) {
            channelIds.add(Integer.parseInt(channel));
        }

        // Otherwise - creating a new configuration..
        return chartProviders.get(fleetId).createChart(chartName, channelIds, userId);
    }

    @PUT
    @Path("/rename/{fleetId}/{chartId: [0-9]+}/{chartName: .+}/")
    @LoggingData
    public void rename(@PathParam("fleetId") String fleetId,
            @PathParam("chartId") long chartId,
            @PathParam("chartName") String chartName,
            @Context HttpServletRequest request,
            @Context HttpServletResponse response) {
        Licence licence = Licence.get(request, response, true);

        chartProviders.get(fleetId).renameChart(chartId, chartName, licence.getLoginUser().getLogin());
    }

    @PUT
    @Path("/delete/{fleetId}/{chartId: [0-9]+}/")
    @LoggingData
    public void delete(@PathParam("fleetId") String fleetId,
            @PathParam("chartId") long chartId,
            @Context HttpServletRequest request,
            @Context HttpServletResponse response) {
        Licence licence = Licence.get(request, response, true);

        chartProviders.get(fleetId).deleteChart(chartId, licence.getLoginUser().getLogin());
    }
    
    
    //This is used for chart view
    @GET
    @TimeoutFree
    @Path("/{fleetId}/{vehicleId: [0-9]+}/{interval: [0-9]+}/{endtime: [0-9]+}/{timezone}/{channels: [a-zA-Z0-9]+(,[a-zA-Z0-9]+)*}")
    @Produces("image/png")
    @LoggingData
    public Response getData(
            @PathParam("fleetId") String fleetId,
            @PathParam("vehicleId") int vehicleId,
            @PathParam("interval") int interval,
            @PathParam("endtime") String endTimeParam,
            @PathParam("timezone") String timezone,
            @PathParam("channels") String channelList,
            @QueryParam("height") Integer height,
            @QueryParam("width") Integer width,
            @Context HttpServletRequest httpRequest,
            @Context HttpServletResponse httpResponse) {
   
        try {
            List<Integer> channelIds = getIdList(channelList);
            List<ChannelConfig> channels = chartProviders.get(fleetId).getConfigurations(channelIds, vehicleId);
            
            Licence licence = Licence.get(httpRequest, httpResponse, true);

            byte[] imageData = commonChartProvider.createImage(fleetId, vehicleId, 
                    interval, endTimeParam, timezone, channels, height, width,licence);
            ResponseBuilder response = Response.ok(new ByteArrayInputStream(
                    imageData), new MediaType("image", "png"));
            response.header("Cache-Control", "max-age=86400");
            return response.build();
        } catch (IOException e) {
            LOGGER.error(e);
        }

        return Response.serverError().build();
    }
    
    
    //this is used for table view
    @GET
    @Path("values/{fleetId}/{vehicleId: [0-9]+}/{interval: [0-9]+}/{endtime: [0-9]*}/{endrow: [0-9]+}/{channels: [a-zA-Z0-9]+(,[a-zA-Z0-9]+)*}")
    @Produces(MediaType.APPLICATION_JSON)
    @LoggingData
    public ChannelData getDataValue(@PathParam("fleetId") String fleetId,
            @PathParam("vehicleId") int vehicleId,
            @PathParam("interval") int interval,
            @PathParam("endtime") String endTimeParam,
            @PathParam("endrow") int endRow,
            @PathParam("channels") String channelList) throws IOException {

        Long endTime = null;
        
        if (endTimeParam != null && endTimeParam.matches("[0-9]+")) {
            endTime = Long.parseLong(endTimeParam);
        }
        
        ChannelData channelData = new ChannelData();

        List<Integer> channelIds = getIdList(channelList);
        List<ChannelConfig> channels = chartProviders.get(fleetId).getConfigurations(channelIds, vehicleId);

        channelData.setColumns(getColumns(channels));

        List<DataSet> data = cdProviders.get(fleetId).getVehicleData(vehicleId,
                endTime, interval, endRow, channels, true);

        channelData.setData(data);
        channelData.setTotalRows(data.size());

        return channelData;
    }
    
    //This is used for export chart data to csv
    @GET
    @Path("export/{fleetId}/{vehicleId: [0-9]+}/{interval: [0-9]+}/{endtime: [0-9]*}/{channels: [a-zA-Z0-9]+(,[a-zA-Z0-9]+)*}/{timezone}")
    @Produces("text/csv")
    @LoggingData
    public Response getDataExport(
            @PathParam("fleetId") String fleetId,
            @PathParam("vehicleId") int vehicleId,
            @PathParam("interval") int interval,
            @PathParam("endtime") String endTimeParam,
            @PathParam("channels") String channelList,
            @PathParam("timezone") String timezone,
            @Context HttpServletRequest request,
            @Context HttpServletResponse response) throws IOException {
        
        Licence licence = Licence.get(request, response, true);

        Long endTime = null;
        
        if (endTimeParam != null && endTimeParam.matches("[0-9]+")) {
            endTime = Long.parseLong(endTimeParam);
        } else {
            endTime = System.currentTimeMillis();
        }
        

        TimeZone tz = DateUtils.getClientTimeZone(timezone);
        
        List<Integer> channelIds = getIdList(channelList);
        List<ChannelConfig> channels = chartProviders.get(fleetId).getConfigurations(channelIds, vehicleId);

        List<DataSet> data = commonChartProvider.getVehicleDataExport(fleetId, vehicleId,
                endTime, interval, channels, true);

        String vehicleNumber = dataExportConfiguration.getSourceNumber(
                String.valueOf(vehicleId), fleetId);
        
        String[] header = dataExportConfiguration.getHeader(vehicleNumber,
                endTime - (interval * 1000), endTime, tz, fleetId, licence);

        String filename = dataExportConfiguration.getFileName(vehicleNumber,
                endTime - interval, tz);

        return createFile(filename, header, data, channels, tz, licence);

    }



    private Response createFile(String filename, String[] header,
            List<DataSet> dataList, List<ChannelConfig> channels, TimeZone tz, Licence licence)
            throws IOException {
        
    	ExportConfiguration exportConf = appConf.getExportConfig();
        ResourceBundleUTF8 bundle = new ResourceBundleUTF8(licence.getUserLocale());

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        PrintWriter writer = new PrintWriter(outputStream);
        CSVWriter csvWriter = new CSVWriter(writer, exportConf.getSeparator());

        csvWriter.writeNext(header);

        String[] titleRow = new String[channels.size() + 1];
        titleRow[0] = bundle.getString("timestamp");
        String readFromConfig;
        int i = 1;
        for (ChannelConfig channel : channels) {
        	readFromConfig =bundle.getString(channel.getName());
        	if(null!= readFromConfig  && !"".equals(readFromConfig))
        	{
        		 titleRow[i] = readFromConfig;
        	}
        	else {
            titleRow[i] = channel.getDescription();
        	}
            i++;
        }
        csvWriter.writeNext(titleRow);

        List<String[]> rows = new ArrayList<String[]>();
        for (DataSet data : dataList) {
            String[] row = dataExportConfiguration.getRow(data, channels, tz);
            
            if (row != null) {
                rows.add(row);
            }
        }

        Collections.sort(rows, new SortValues());

        for (String[] row : rows) {
            csvWriter.writeNext(row);
        }

        csvWriter.close();
        writer.close();
        outputStream.close();
        ResponseBuilder response = Response.ok(outputStream.toByteArray());
        response.header("Content-disposition", "attachment; filename="
                + filename + "." + exportConf.getFileExtension());

        return response.build();
    }


    class SortValues implements Comparator<String[]> {

        @Override
        public int compare(String[] strDate1, String[] strDate2) {
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss.SSS");
            
            Date date1 = null;
            Date date2 = null;
            
            try {
                date1 = formatter.parse(strDate1[0]);
                date2 = formatter.parse(strDate1[0]);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            
            return date1.compareTo(date2);
        }
    }

    public static List<Column> getColumns(List<ChannelConfig> channelList) {
        List<Column> columns = new ArrayList<Column>();
                
        columns.add(new TextColumnBuilder()
            .displayName("TimeStamp")
            .name("TimeStamp")
            .width(160)
            .styles(CHANNELDATA_TIMESTAMP)
            .sortable(false)
            .mouseOverHeader("TimeStamp")
            .build());

        for (ChannelConfig channel : channelList) {
            if (channel != null && channel.getShortName() != null) {
                int width = 20;
                if (channel.isLookup()) {
                    width = 65;
                } else if (channel.getType() != null
                        && channel.getType() == ChannelType.ANALOG) {
                    width = 55;
                }
                
                columns.add(new TextColumnBuilder()
                    .displayName(channel.getName())
                    .name(channel.getShortName())
                    .width(width)
                    .styles(CHANNELDATA_COMMON)
                    .sortable(false)
                    .build());
            }
        }

        return columns;
    }
     
    @POST
    @TimeoutFree
    @Path("exportaspdf/{fleetId}/{unitNumber}/{vehicleId: [0-9]+}/{interval: [0-9]+}/{endtime: [0-9]*}/{timezone}/{channels: [a-zA-Z0-9]+(,[a-zA-Z0-9]+)*}/{type}")
    @Produces("image/png")
    @LoggingData
    public Response downloadPdf(
            @PathParam("fleetId") String fleetId,
            @PathParam("unitNumber") String unitNumber,
            @PathParam("vehicleId") int vehicleId,
            @PathParam("interval") int interval,
            @PathParam("endtime") String endTimeParam,
            @PathParam("timezone") String timezone,
            @PathParam("channels") String channelList,
            @QueryParam("height") Integer height,
            @QueryParam("width") Integer width,
            String data,
            @PathParam("type") String type,
            @Context HttpServletResponse response,
            @Context HttpServletRequest request) {
         
    		Licence licence = Licence.get(request, response, true);
        byte[] imageData;
        try {
            List<Integer> channelIds = getIdList(channelList);
            List<ChannelConfig> channels = chartProviders.get(fleetId).getConfigurations(channelIds, vehicleId);
            
            imageData = commonChartProvider.createImage(fleetId, vehicleId, interval, 
                    endTimeParam, timezone, channels, height, width,licence);
            String img = javax.xml.bind.DatatypeConverter.printBase64Binary(imageData);
            
            String dataTable = extractData(data);
            
            String vehicleNumber = cdProviders.get(fleetId).getVehicleNumberById(
                    String.valueOf(vehicleId));        
                    
            AnalysisCreatePdf.createFile(img, dataTable, type, response, unitNumber, vehicleNumber);
        } catch (IOException e) {
            LOGGER.error(e);
        } catch (DocumentException e) {
            LOGGER.error(e);
        }
        
        return javax.ws.rs.core.Response.ok().build();
    }
    
    @GET
    @Path("/configuration")
    @Produces(MediaType.APPLICATION_JSON)
    @LoggingData
    public ChartConfiguration getPeriodValues() {
    	ChartConfiguration conf = dataPlotConfiguration.getChartConfiguration();
    	conf.setVirtualScrollBar(dataPlotConfiguration.getVirtualScrollBar());
    	conf.setIsSystemChartsIncluded(dataPlotConfiguration.isSystemChartsIncluded());
    	conf.setChartEnabledByFleet(dataPlotConfiguration.getChartEnabledByFleet());
    	conf.setChannelGroupLabelConfig(dataPlotConfiguration.getChannelLabelConfig());
    	conf.setTableChannelLabelConfig(dataPlotConfiguration.getTableChannelLabelConfig());
        return conf;
    }
      
    /**
     * Parse the String in parameter and return the list of ids
     * @param channelList the list of channels as string
     * @return the list of channel as integer
     */
    private List<Integer> getIdList(String channelList) {
        List<Integer> channelIds = new ArrayList<Integer>();

        for (String channel : channelList.split(",")) {
            channelIds.add(Integer.parseInt(channel));
        }
        return channelIds;
    }
    
    private String extractData(String data) {
        int posFin = data.indexOf("name=\"data\"");
        String dataTable = data.substring(posFin + 11).trim();
        int pos2 = dataTable.indexOf("------");
        dataTable = dataTable.substring(0, pos2).trim();
        
        return dataTable;
    }
        
}
