package com.nexala.spectrum.rest.data;

import javax.inject.Inject;

import com.nexala.spectrum.db.beans.UserConfiguration;
import com.nexala.spectrum.db.dao.UserConfigurationDao;

public class DefaultUserConfigurationProvider implements UserConfigurationProvider {
    
    @Inject
    private UserConfigurationDao dao;

    @Override
    public UserConfiguration getUserConfiguration(String username, String variable) {
        return dao.getUserConfiguration(username, variable);
    }

    @Override
    public void updateUserConfiguration(String username, String variable, String data) {
        dao.updateUserConfiguration(username, variable, data);

    }

}
