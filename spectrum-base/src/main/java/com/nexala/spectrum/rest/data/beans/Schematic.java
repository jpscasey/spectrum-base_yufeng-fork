package com.nexala.spectrum.rest.data.beans;

import java.util.HashMap;
import java.util.List;

public class Schematic {
	private String name;
	private String displayName;
	private String jsClassPath;
	private String jsConstructor;
	private List<SchematicChannel> channelList;
	
	private HashMap<String, Integer> canvasSize;
	private HashMap<String, Integer> raphaelSize;
	private HashMap<String, Double> schematicParams;
	
	public Schematic() {}
	
	public String getJSConstructor() {
		return jsConstructor;
	}
	
	public void setJSConstructor(String jsConstructor) {
		this.jsConstructor = jsConstructor;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getJSClassPath() {
		return jsClassPath;
	}

	public void setJSClassPath(String jsClassPath) {
		this.jsClassPath = jsClassPath;
	}

	public HashMap<String, Integer> getCanvasSize() {
		return canvasSize;
	}

	public void setCanvasSize(HashMap<String, Integer> canvasSize) {
		this.canvasSize = canvasSize;
	}

	public HashMap<String, Integer> getRaphaelSize() {
		return raphaelSize;
	}

	public void setRaphaelSize(HashMap<String, Integer> raphaelSize) {
		this.raphaelSize = raphaelSize;
	}

	public HashMap<String, Double> getSchematicParams() {
		return schematicParams;
	}

	public void setSchematicParams(HashMap<String, Double> schematicParams) {
		this.schematicParams = schematicParams;
	}

	public List<SchematicChannel> getChannelList() {
		return channelList;
	}

	public void setChannelList(List<SchematicChannel> channelList) {
		this.channelList = channelList;
	}
}
