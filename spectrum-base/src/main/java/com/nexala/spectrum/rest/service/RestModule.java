/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.rest.service;

import javax.inject.Singleton;

import org.aopalliance.intercept.MethodInterceptor;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.matcher.Matchers;
import com.google.inject.servlet.ServletModule;
import com.nexala.spectrum.eventanalysis.rest.EAnalysisService;
import com.nexala.spectrum.rest.cache.Cache;
import com.nexala.spectrum.rest.cache.RestCacheInterceptor;
import com.nexala.spectrum.rest.data.DataUtils;
import com.nexala.spectrum.view.MergeServlet;
import com.nexala.spectrum.web.exception.OptimisticLockExceptionMapper;
import com.nexala.spectrum.web.service.RuleTestService;

public class RestModule extends ServletModule {
    @Override
    protected void configureServlets() {
        // FIXME is this necessary?
        requestStaticInjection(DataUtils.class);

        // Cache interceptor
        MethodInterceptor cacheInterceptor = new RestCacheInterceptor();
        requestInjection(cacheInterceptor);
        bindInterceptor(Matchers.any(),
                Matchers.annotatedWith(Cache.class),
                cacheInterceptor);

        bind(CabService.class);
        bind(ChannelDataService.class);
        bind(ChannelDefinitionService.class);
        bind(StaticChartService.class);
        bind(ChartService.class);
        bind(DownloadService.class);
        bind(EventService.class);
        bind(EAnalysisService.class);
        bind(FleetService.class);
        bind(GroupService.class);
        bind(HelpService.class);
        bind(LocationService.class);
        bind(LoggingService.class);
        bind(MapService.class);
        bind(RouteMapService.class);
        bind(StockService.class);
        bind(UnitDetailService.class);
        bind(UnitSummaryService.class);
        bind(RecoveryService.class);
        bind(UnitService.class);
        bind(SystemService.class);
        bind(RailStreamDataService.class);
        bind(EditorService.class);
        bind(SchematicService.class);
        
        bind(OptimisticLockExceptionMapper.class).in(Singleton.class);
        bind(ObjectMapper.class).in(Singleton.class);
        
        serve("/css/merge").with(MergeServlet.class);
        serve("/js/merge").with(MergeServlet.class);

        serve("/test").with(RuleTestService.class);
    }
}