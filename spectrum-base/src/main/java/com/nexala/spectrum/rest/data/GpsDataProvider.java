package com.nexala.spectrum.rest.data;

import java.util.List;

import com.nexala.spectrum.rest.data.beans.DataSet;
import com.nexala.spectrum.rest.data.beans.Event;

public interface GpsDataProvider {

    List<DataSet> getLiveGpsData();
    
    List<DataSet> getHistoricGpsData(long timestamp);

    List<Event> getLiveByUnit(Integer unitId);

}