package com.nexala.spectrum.rest.data.beans;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.nexala.spectrum.channel.Channel;
import com.nexala.spectrum.view.conf.Column;

public class ChannelData {
    private List<Column> columns = new ArrayList<Column>();
    private List<DataSet> data = new ArrayList<DataSet>();

    // TODO: as it doesn't represent groups, but the group categories, change
    // the name to groupCategories and the map to <Integer, ChannelCategory>
    private Map<Integer, Channel<?>> groups = new HashMap<Integer, Channel<?>>();

    private Integer totalRows;

    public List<Column> getColumns() {
        return columns;
    }

    public void setColumns(List<Column> columns) {
        this.columns = columns;
    }

    public void setData(List<DataSet> data) {
        this.data = data;
    }

    public List<DataSet> getData() {
        return data;
    }

    public void setGroups(Map<Integer, Channel<?>> groups) {
        this.groups = groups;
    }

    public Map<Integer, Channel<?>> getGroups() {
        return groups;
    }

    public Integer getTotalRows() {
        return totalRows;
    }

    public void setTotalRows(Integer totalRows) {
        this.totalRows = totalRows;
    }
}
