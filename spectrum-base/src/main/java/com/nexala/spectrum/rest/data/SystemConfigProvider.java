package com.nexala.spectrum.rest.data;

public interface SystemConfigProvider {

	public String getDatabaseName();
}
