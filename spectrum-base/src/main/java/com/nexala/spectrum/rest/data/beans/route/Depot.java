package com.nexala.spectrum.rest.data.beans.route;

import java.util.List;

/**
 * Bean representing a depot for the route map view.
 * @author brice
 *
 */
public class Depot {
    
    
    private int x = 0;
    
    private int y = 0;
    
    private int width = 0;
    
    private int height = 0;
    
    private String code = null;
    
    private String name = null;
    
    private Double latitude = null;
    
    private Double longitude = null;
    
    private List<DepotVehicleGroup> depotGroups = null;

    /**
     * Getter for x.
     * @return the x
     */
    public int getX() {
        return x;
    }

    /**
     * Setter for x.
     * @param x the x to set
     */
    public void setX(int x) {
        this.x = x;
    }

    /**
     * Getter for y.
     * @return the y
     */
    public int getY() {
        return y;
    }

    /**
     * Setter for y.
     * @param y the y to set
     */
    public void setY(int y) {
        this.y = y;
    }

    /**
     * Getter for width.
     * @return the width
     */
    public int getWidth() {
        return width;
    }

    /**
     * Setter for width.
     * @param width the width to set
     */
    public void setWidth(int width) {
        this.width = width;
    }

    /**
     * Getter for height.
     * @return the height
     */
    public int getHeight() {
        return height;
    }

    /**
     * Setter for height.
     * @param height the height to set
     */
    public void setHeight(int height) {
        this.height = height;
    }

    /**
     * Getter for code.
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * Setter for code.
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * Getter for name.
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Setter for name.
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Getter for latitude.
     * @return the latitude
     */
    public Double getLatitude() {
        return latitude;
    }

    /**
     * Setter for latitude.
     * @param latitude the latitude to set
     */
    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    /**
     * Getter for longitude.
     * @return the longitude
     */
    public Double getLongitude() {
        return longitude;
    }

    /**
     * Setter for longitude.
     * @param longitude the longitude to set
     */
    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    /**
     * Getter for depotSet.
     * @return the depotSet
     */
    public List<DepotVehicleGroup> getDepotGroups() {
        return depotGroups;
    }

    /**
     * Setter for depotSet.
     * @param depotSet the depotSet to set
     */
    public void setDepotGroups(List<DepotVehicleGroup> depotSet) {
        this.depotGroups = depotSet;
    }


     
    

}
