package com.nexala.spectrum.rest.data.beans;

/**
 * A key for the channel data cache.
 * the key is the combination of the fleet id, source id, and source type (vehicle, unit or formation)
 * @author BBaudry
 *
 */
public class ChannelDataKey {

    public static final String VEHICLE_TYPE = "v";

    public static final String UNIT_TYPE = "u";

    public static final String FORMATION_TYPE = "f";

    private final String fleet;

    private final Integer source;

    private final String type;

    public ChannelDataKey(String fleet, Integer source, String type) {
        this.fleet = fleet;
        this.source = source;
        this.type = type;
    }

    public String getFleet() {
        return fleet;
    }

    public Integer getSource() {
        return source;
    }

    public String getType() {
        return type;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((fleet == null) ? 0 : fleet.hashCode());
        result = prime * result + ((source == null) ? 0 : source.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ChannelDataKey other = (ChannelDataKey) obj;
        if (fleet == null) {
            if (other.fleet != null)
                return false;
        } else if (!fleet.equals(other.fleet))
            return false;
        if (source == null) {
            if (other.source != null)
                return false;
        } else if (!source.equals(other.source))
            return false;
        if (type == null) {
            if (other.type != null)
                return false;
        } else if (!type.equals(other.type))
            return false;
        return true;
    }
    
}
