/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.rest.data;

import java.util.List;

import com.nexala.spectrum.db.beans.FleetFormation;
import com.nexala.spectrum.db.beans.Unit;
import com.nexala.spectrum.db.dao.DataAccessException;
import com.nexala.spectrum.rest.data.beans.Formation;
import com.nexala.spectrum.rest.data.beans.GenericBean;
import com.nexala.spectrum.rest.data.beans.StockDesc;
import com.nexala.spectrum.rest.data.beans.UnitDescList;

public interface UnitProvider {
    /**
     * Returns a UnitDescList, containing the headers required by the
     * UnitSelector and the Units with descriptors (e.g. headcode, unit code)
     * matching the substr, i.e. ^.*<substr>.*$.
     * 
     * @param substr
     * @return
     */
    UnitDescList searchUnits(String substr) throws DataAccessException;
 
    /**
     * Returns an Unit
     * 
     * @param unitId
     */
    Unit searchUnit(int unitId) throws DataAccessException;

    /**
     * Finds a unit by its displayName, rather than its database ID.
     * 
     * @param unitNumber
     * @return
     * @throws DataAccessException
     */
    Unit searchUnit(String unitNumber) throws DataAccessException;
    
    /**
     * Returns a list of all of the Stock (read Vehicles) which are part of
     * the specified Unit.
     * 
     * @param unitId
     * @return a list of Stock.
     * @throws DataAccessException
     */
    List<StockDesc> getStockForUnit(int unitId) throws DataAccessException;
    
    /**
     * TODO DOC
     * @param unitIds
     * @return
     * @throws DataAccessException
     */
    List<Formation> formation(String unitIds) throws DataAccessException;

    List<Unit> getFormationUnits(int unitId) throws DataAccessException;
    
    /**
     * Return the valid fleet formations
     * @return the valid fleet formations
     * @throws DataAccessException In case of exception
     */
    List<FleetFormation> getValidFleetFormations() throws DataAccessException;

    /**
     * Returns a list of all units with vehicles
     * 
     * @return a list of Unit.
     * @throws DataAccessException
     */
    List<Unit> getAllUnits() throws DataAccessException;
    
    /**
     * Returns a list of unit types
     * 
     * @return a list of Unit Types.
     * @throws DataAccessException
     */
    List<String> getUnitTypes() throws DataAccessException;
    
    /**
     * Returns a List<GenericBean>, containing the headers required by the
     * UnitSelector and the Units with descriptors (e.g. headcode, unit code)
     * matching the substr, i.e. ^.*<substr>.*$.
     * 
     * @param substr
     * @return
     */
    List<GenericBean> searchUnitsByString(String substr) throws DataAccessException;
}