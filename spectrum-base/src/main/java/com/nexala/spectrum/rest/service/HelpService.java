package com.nexala.spectrum.rest.service;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.nexala.spectrum.view.conf.AppConfiguration;

@Path("/help/")
@Singleton
public class HelpService {

    @Inject
    private AppConfiguration appConf;

    @GET
    @Path("/config")
    @Produces(MediaType.APPLICATION_JSON)
    public Map<String, String> helpConfig(@Context HttpServletRequest request) {
        return appConf.getHelpConfig();
    }
}
