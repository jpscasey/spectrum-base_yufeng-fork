/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.rest.data;

import java.io.IOException;
import java.net.URL;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.HttpMethod;
import com.amazonaws.SdkClientException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import com.amazonaws.services.s3.model.ResponseHeaderOverrides;
import com.google.inject.Inject;
import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.db.beans.ParameterDto;
import com.nexala.spectrum.db.dao.DownloadDao;
import com.nexala.spectrum.db.mappers.DownloadMapper;
import com.nexala.spectrum.rest.data.beans.DownloadInfo;
import com.nexala.spectrum.rest.data.beans.DownloadSearchParameters;

public class DefaultDownloadProvider implements DownloadProvider{
	
    public final Logger logger = Logger.getLogger(DefaultDownloadProvider.class);

    @Inject
    private DownloadDao downloadDao;
    
    @Inject
    private DownloadMapper customMapper;
    
    public List<DownloadInfo> searchDownload(Date dateFrom, Date dateTo,
            String unit, String vehicle, String fileType) {

        DownloadSearchParameters parameters = new DownloadSearchParameters();

        parameters.setDateFrom(dateFrom);
        parameters.setDateTo(dateTo);
        parameters.setUnitNumber(unit);
        parameters.setVehicle(vehicle);
        parameters.setFileType(fileType);

        List<DownloadInfo> downloadList = downloadDao
                .searchForDownloads(parameters, customMapper);
        
        return downloadList;
    }

    public void insertDownload(String vehicleId, String fileType, String startDate, String startTime,
    		String endTime) {
        downloadDao.insertDownload(Long.parseLong(vehicleId),
                Integer.parseInt(fileType), !startDate.equals("") ? Long.parseLong(startDate) : null,
                		!startTime.equals("") ? Long.parseLong(startTime) : null, !endTime.equals("") ? Long.parseLong(endTime) : null);
    }

    // TODO make insert generic - requestTypeId and devices are railhead specific
    public void insertDownload(String vehicleId, String fileType, String startDate, String startTime, String endTime
    		, String requestTypeId, String devices) {
        downloadDao.insertDownload(Long.parseLong(vehicleId),
                Integer.parseInt(fileType), !startDate.equals("") ? Long.parseLong(startDate) : null,
                		!startTime.equals("") ? Long.parseLong(startTime) : null, !endTime.equals("") ? Long.parseLong(endTime) : null
                				, Integer.parseInt(requestTypeId), !devices.equals("") ? devices : null);
    
    }

    public Boolean checkCurrentDownload(String vehicleId, String fileType) {
        return downloadDao.currDownloadCheck(vehicleId, fileType);
    }

    public String downloadFile(String path)
            throws IOException {
    		
    	URL url = null;
    	
		String[] pathSplit = path.split("/", -1); 
		String bucketName = pathSplit[3]; 
		StringBuilder objectKey = new StringBuilder("");
		for(int i = 4; i < pathSplit.length; i++) {
			objectKey = (i == 4) ? objectKey.append(pathSplit[i]) : objectKey.append("/" + pathSplit[i]);
		}
		
		String fileName = pathSplit[pathSplit.length - 1];
    	
    	try {
        	
    		AmazonS3 s3Client;
    		String awsRegion = System.getenv(Spectrum.AWS_REGION);
    		
    		if(awsRegion != null) {
        		s3Client = AmazonS3ClientBuilder.standard()
						.withRegion(awsRegion)
				    	.build();
    		} else {
    			s3Client = AmazonS3ClientBuilder.standard()
				    	.build();
    		}
        	
            // Set the presigned URL to expire after one minute.
            Date expiration = new Date();
            long expTimeMillis = expiration.getTime();
            expTimeMillis += 1000 * 60 * 60;
            expiration.setTime(expTimeMillis);
        	
            // Generate the presigned URL.
            logger.info("Generating pre-signed URL.");
            
            ResponseHeaderOverrides responseHeaders = new ResponseHeaderOverrides()
            	    .withContentDisposition("attachment; filename=\"" + fileName + "\"");
    		
            GeneratePresignedUrlRequest generatePresignedUrlRequest = 
                    new GeneratePresignedUrlRequest(bucketName, objectKey.toString())
                    .withMethod(HttpMethod.GET)
                    .withResponseHeaders(responseHeaders)
                    .withExpiration(expiration);
            
            url = s3Client.generatePresignedUrl(generatePresignedUrlRequest);
  
    	}
        catch(AmazonServiceException e) {
            // The call was transmitted successfully, but Amazon S3 couldn't process 
            // it, so it returned an error response.
        	logger.error(e);
        }
        catch(SdkClientException e) {
            // Amazon S3 couldn't be contacted for a response, or the client
            // couldn't parse the response from Amazon S3.
        	logger.error(e);
        }
    	
    	return url.toString();   
    }

	@Override
	public void insertDownload(List<ParameterDto> parameters) {
		// TODO: Add handler for execution failure
		
		// The default value of requestType is 'Video Clip' => requestTypeId = 1;
		// This is the default behaviour, create different provider for the project
		Integer requestTypeId = 1;
    	Long vehicleId = null;
    	Integer fileType = null; 
    	Long startDate = null; 
    	Long startTime = null; 
    	Long endTime = null; 
    	Long duration = null;
    	String devices = null;
    	
    	for (ParameterDto param : parameters) {
    		String name = param.getName().toLowerCase();
    		String value = param.getValue();
    		String type = param.getType();
    		
    		if (name == null ||
    			value == null) {
    			continue;
    		}
    		
    		switch(name) {
    			case "sourceid" :
    				vehicleId = Long.parseLong(value);
    				break;
    			case "filetypename":
    				fileType = Integer.parseInt(value);
    				break;
    			case "device":
    				devices = value;
    				break;
    			case "duration":
    				duration = Long.parseLong(value);
    				break;
    			case "timestamp":
    				startDate = Long.parseLong(value);
    				startTime = Long.parseLong(value);
    				break;
    		}
    		
    	}
    	
    	if (startTime != null &&
    		duration != null) {
    		endTime = startTime + duration * 60 * 1000;
    	}
    	
    	if (vehicleId != null && 
	    	startDate != null &&  
	    	startTime != null &&  
	    	endTime != null &&  
	    	duration != null && 
	    	requestTypeId != null &&  
	    	devices != null &&
	    	fileType != null) {
    		downloadDao.insertDownload(vehicleId, fileType, startDate, startTime, endTime, requestTypeId, devices);
    	}
    }    
}