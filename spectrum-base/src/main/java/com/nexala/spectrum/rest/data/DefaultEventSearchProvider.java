package com.nexala.spectrum.rest.data;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.licensing.Licence;
import com.nexala.spectrum.rest.data.beans.Event;
import com.nexala.spectrum.rest.data.beans.EventSearchParameter;

public class DefaultEventSearchProvider implements EventSearchProvider {

    private static final Comparator<Event> eventCompareDesc = new Comparator<Event>() {
        @Override
        public int compare(Event o1, Event o2) {
            return ((Long) o2.getField(Spectrum.EVENT_CREATE_TIME)).compareTo(((Long) o1.getField(Spectrum.EVENT_CREATE_TIME)));
        }
    };

    @Override
    public List<Event> getEventsSince(CommonEventProvider commonEventProvider, long timestamp) {
        List<Event> events = commonEventProvider.getAllFleetsEventsSince(timestamp);
        Collections.sort(events, eventCompareDesc);
        return events;
    }

    @Override
    public List<Event> searchEvents(CommonEventProvider commonEventProvider, Date fromDate, Date toDate, String live,
            String keyword, Integer limit, Licence licence) {
        List<Event> events = commonEventProvider.searchAllFleetsEvents(fromDate, toDate, live, keyword, limit, licence);
        Collections.sort(events, eventCompareDesc);
        return events;
    }
    
    @Override
    public List<Event> searchFleetEvents(EventProvider eventProvider, Date fromDate, Date toDate, String live,
            String keyword, Integer limit) {
        List<Event> events = eventProvider.searchEvents(fromDate, toDate, live, keyword, limit);
        Collections.sort(events, eventCompareDesc);
        return events;
    }

    @Override
    public List<Event> searchEvents(CommonEventProvider commonEventProvider, Date dateFrom, Date dateTo, String live,
            String code, String type, String description, String category, List<EventSearchParameter> parameters,
            Integer limit, Licence licence) {
        List<Event> events = commonEventProvider.searchAllFleetsEvents(dateFrom, dateTo, code, type, description,
                category, live, parameters, limit, licence);
        Collections.sort(events, eventCompareDesc);
        return events;
    }

    @Override
    public List<Event> searchFleetEvents(EventProvider eventProvider, Date dateFrom, Date dateTo, String live, String code,
            String type, String description, String category, List<EventSearchParameter> parameters, Integer limit) {
        List<Event> events = eventProvider.searchEvents(dateFrom, dateTo, 
                code, type, description, category, live, parameters, limit);
        Collections.sort(events, eventCompareDesc);
        return events;
    }
}
