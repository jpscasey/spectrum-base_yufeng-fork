package com.nexala.spectrum.rest.service.bean.editor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "package")
@XmlAccessorType(XmlAccessType.FIELD)
public class Package {
    
    @XmlElement(name="name", required=true)
    private String name;
    
    @XmlElement(name="parent-path", required=true)
    private String parentPath;
    
    @XmlElement(name="display-name", required=true)
    private String displayName;
    
    @XmlElement(name="licence", required=false)
    private String licence;
    
    @XmlElement(name="type", required=true)
    private String type;
    
    @XmlElement(name="fleet-code", required=false)
    private String fleetCode;

    /**
     * Return the name.
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Setter for the name.
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Return the parentPath.
     * @return the parentPath
     */
    public String getParentPath() {
        return parentPath;
    }

    /**
     * Setter for the parentPath.
     * @param parentPath the parentPath to set
     */
    public void setParentPath(String parentPath) {
        this.parentPath = parentPath;
    }

    /**
     * Return the displayName.
     * @return the displayName
     */
    public String getDisplayName() {
        return displayName;
    }

    /**
     * Setter for the displayName.
     * @param displayName the displayName to set
     */
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }
    
    /**
     * Return the licence.
     * @return the licence
     */
    public String getLicence() {
        return licence;
    }

    /**
     * Setter for the licence.
     * @param licence the licence to set
     */
    public void setLicence(String licence) {
        this.licence = licence;
    }

    /**
     * Return the type.
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * Setter for the type.
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * Return the fleetCode.
     * @return the fleetCode
     */
	public String getFleetCode() {
		return fleetCode;
	}

	/**
     * Setter for the fleetCode.
     * @param fleetCode the fleetCode to set
     */
	public void setFleetCode(String fleetCode) {
		this.fleetCode = fleetCode;
	}

}
