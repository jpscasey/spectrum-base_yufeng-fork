package com.nexala.spectrum.rest.service;

import java.util.List;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.nexala.spectrum.licensing.LoggingData;
import com.nexala.spectrum.rest.data.CabDataProvider;
import com.nexala.spectrum.rest.data.beans.DataSet;
import com.nexala.spectrum.rest.data.beans.StockDesc;
import com.nexala.spectrum.rest.security.TimeoutFree;
import com.nexala.spectrum.view.conf.cab.CabConfiguration;

@Path("/cab")
@Singleton
public class CabService {
    @Inject
    private Map<String, CabConfigurationProvider> cabConfigService;

    @Inject
    private Map<String, CabDataProvider> providers;

    /**
     * Return the layout for the vehicle passed in parameter.
     * @param vehicleId the vehicle id or null if not provided
     * @return  the configuration
     */
    @GET
    @Path("/layout/{fleetId}/{vehicleId}")
    @Produces(MediaType.APPLICATION_JSON)
    @LoggingData
    public CabConfiguration layout(@PathParam("fleetId") String fleetId,
            @PathParam("vehicleId") String vehicleId) {
        Integer id = null;
        try {
            id = Integer.parseInt(vehicleId);
        } catch (NumberFormatException e) {
            
        }
        
        return cabConfigService.get(fleetId).getCabConfiguration(id);
    }

    @GET
    @Path("/data/{fleetId}/{fleetFormationId: [0-9]+}/{vehicleId: [0-9]+}/{prevTimestamp: [0-9]+}/{timestamp: [0-9]+}")
    @Produces(MediaType.APPLICATION_JSON)
    @LoggingData
    public DataSet cabData(@PathParam("fleetId") String fleetId,
            @PathParam("fleetFormationId") int fleetFormationId,
            @PathParam("vehicleId") int vehicleId,
            @PathParam("prevTimestamp") Long prevTimestamp,
            @PathParam("timestamp") Long timestamp) {

        return providers.get(fleetId).getVehicleData(vehicleId, fleetFormationId, prevTimestamp, timestamp);
    }

    @GET
    @TimeoutFree
    @Path("/data/{fleetId}/{fleetFormationId: [0-9]+}/{vehicleId: [0-9]+}/{prevTimestamp: [0-9]+}")
    @Produces(MediaType.APPLICATION_JSON)
    @LoggingData
    public DataSet liveCabData(@PathParam("fleetId") String fleetId,
            @PathParam("fleetFormationId") int fleetFormationId,
            @PathParam("vehicleId") int vehicleId,
            @PathParam("prevTimestamp") Long prevTimestamp) {
        return providers.get(fleetId).getVehicleData(vehicleId, fleetFormationId, prevTimestamp, null);
    }

    @GET
    @LoggingData
    @Path("/stock/{fleetId}/{unitId: [0-9]+}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<StockDesc> stock(@PathParam("fleetId") String fleetId, @PathParam("unitId") int unitId) {
        return providers.get(fleetId).getStockForUnit(unitId);
    }
}
