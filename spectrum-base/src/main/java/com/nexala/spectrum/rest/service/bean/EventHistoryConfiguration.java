package com.nexala.spectrum.rest.service.bean;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.nexala.spectrum.db.beans.FaultCategory;
import com.nexala.spectrum.db.beans.FaultMeta;
import com.nexala.spectrum.db.beans.FaultType;
import com.nexala.spectrum.rest.data.beans.Dropdown;
import com.nexala.spectrum.rest.data.beans.EventDefinedFilterField;
import com.nexala.spectrum.view.conf.Column;
import com.nexala.spectrum.view.conf.Field;

/**
 * Bean containing the configuration for the Event history screen.
 * @author brice
 *
 */
public class EventHistoryConfiguration {
    
    private Collection<Field> additionnalFields = null;
    
    private Collection<FaultCategory> eventCategories = null;
    
    private Collection<FaultMeta> eventCodes = null;
    
    private Collection<FaultType> eventTypes = null;
    
    private List<Column> eventColumns = null;
        
    private Integer searchLimit = null;
    
    private Integer exportLimit = null;
    
    private Map<String,List<Dropdown>> additionalDropDownValues = null;
    
    private boolean isDefinedFiltersVisible;
    
    private Map<String, List<EventDefinedFilterField>> definedFilters = null;
    
    private boolean isGroupButtonVisible;
    
    private boolean displayGroupsInBlock;
    
    private int defaultSortColumn;
    /**
     * Getter for additionnalFields.
     * @return the additionnalFields
     */
    public Collection<Field> getAdditionnalFields() {
        return additionnalFields;
    }


    /**
     * Setter for additionnalFields.
     * @param additionnalFields the additionnalFields to set
     */
    public void setAdditionnalFields(Collection<Field> additionnalFields) {
        this.additionnalFields = additionnalFields;
    }


    /**
     * Getter for eventCategories.
     * @return the eventCategories
     */
    public Collection<FaultCategory> getEventCategories() {
        return eventCategories;
    }


    /**
     * Setter for eventCategories.
     * @param eventCategories the eventCategories to set
     */
    public void setEventCategories(Collection<FaultCategory> eventCategories) {
        this.eventCategories = eventCategories;
    }


    /**
     * Getter for eventCodes.
     * @return the eventCodes
     */
    public Collection<FaultMeta> getEventCodes() {
        return eventCodes;
    }


    /**
     * Setter for eventCodes.
     * @param eventCodes the eventCodes to set
     */
    public void setEventCodes(Collection<FaultMeta> eventCodes) {
        this.eventCodes = eventCodes;
    }


    /**
     * Getter for eventTypes.
     * @return the eventTypes
     */
    public Collection<FaultType> getEventTypes() {
        return eventTypes;
    }


    /**
     * Setter for eventTypes.
     * @param eventTypes the eventTypes to set
     */
    public void setEventTypes(Collection<FaultType> eventTypes) {
        this.eventTypes = eventTypes;
    }


    /**
     * Getter for eventColumns.
     * @return the eventColumns
     */
    public List<Column> getEventColumns() {
        return eventColumns;
    }


    /**
     * Setter for eventColumns.
     * @param eventColumns the eventColumns to set
     */
    public void setEventColumns(List<Column> eventColumns) {
        this.eventColumns = eventColumns;
    }

    public Integer getSearchLimit() {
        return searchLimit;
    }

    public void setSearchLimit(Integer searchLimit) {
        this.searchLimit = searchLimit;
    }
    
    public Integer getExportLimit() {
        return exportLimit;
    }

    public void setExportLimit(Integer exportLimit) {
        this.exportLimit = exportLimit;
    }


    public Map<String,List<Dropdown>> getAdditionalDropDownValues() {
        return additionalDropDownValues;
    }


    public void setAdditionalDropDownValues(Map<String,List<Dropdown>> additionalDropDownValues) {
        this.additionalDropDownValues = additionalDropDownValues;
    }


    public boolean isDefinedFiltersVisible() {
        return isDefinedFiltersVisible;
    }


    public void setDefinedFiltersVisible(boolean isDefinedFiltersVisible) {
        this.isDefinedFiltersVisible = isDefinedFiltersVisible;
    }
    
    public Map<String, List<EventDefinedFilterField>> getDefinedFilters() {
        return definedFilters;
    }
    
    public void setDefinedFilters(Map<String, List<EventDefinedFilterField>> definedFilters) {
        this.definedFilters = definedFilters;
    }
    
    public boolean isGroupButtonVisible() {
        return isGroupButtonVisible;
    }

    public void setGroupButtonVisible(boolean isGroupButtonVisible) {
        this.isGroupButtonVisible = isGroupButtonVisible;
    }


	public boolean isDisplayGroupsInBlock() {
		return displayGroupsInBlock;
	}


	public void setDisplayGroupsInBlock(boolean displayGroupsInBlock) {
		this.displayGroupsInBlock = displayGroupsInBlock;
	}


	public int getDefaultSortColumn() {
		return defaultSortColumn;
	}


	public void setDefaultSortColumn(int defaultSortColumn) {
		this.defaultSortColumn = defaultSortColumn;
	}
	
	
}
