/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.rest.serializer;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.nexala.spectrum.channel.Channel;
import com.nexala.spectrum.channel.ChannelCollection;

public class ChannelCollectionSerializer extends JsonSerializer<ChannelCollection> {

    @Override
    public void serialize(ChannelCollection cc, JsonGenerator jgen,
            SerializerProvider provider) throws IOException,
            JsonGenerationException {

        jgen.writeStartObject();
        
        for (Channel<?> channel : cc) {
            jgen.writeFieldName(channel.getColumnName());
            jgen.writeStartArray();
            jgen.writeObject(channel.getValue());
            jgen.writeNumber(channel.getCategory().getSeverity());
            jgen.writeEndArray();
        }

        jgen.writeEndObject();
    }
}