package com.nexala.spectrum.configuration;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import org.apache.log4j.Logger;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.db.beans.Configuration;
import com.nexala.spectrum.rest.data.ConfigurationProvider;
import com.nexala.spectrum.rest.data.FleetDataCache;

/**
 * Manager for the system configurations.
 * Cache the configurations and reload this cache every {@link Spectrum#SPECTRUM_REFRESH_CONFIGURATION_INTERVAL} (default is 1 minute).
 * @author bbaudry
 *
 */
@Singleton
public class ConfigurationManager {
    
    protected final Logger LOGGER = Logger.getLogger(this.getClass());
    
    @Inject
    private ConfigurationProvider configurationProvider;
    
    private final Map<String, Configuration> configurations = new HashMap<String, Configuration>();
    
    private final ReentrantReadWriteLock rwl = new ReentrantReadWriteLock();
    private final Lock readLock = rwl.readLock();
    private final Lock writeLock = rwl.writeLock();
    
    public static long updateInterval;
    
    private Timer timer = null;
    
    @Inject
    public ConfigurationManager(ConfigurationProvider configurationProvider) {
        
        this.configurationProvider = configurationProvider;
        
        try {
            updateConfigurations(configurationProvider.getAll());
        } catch (Exception e) {
            LOGGER.error("Can't load the configurations", e);
        }
        
        updateInterval = getConfAsLong(Spectrum.SPECTRUM_REFRESH_CONFIGURATION_INTERVAL, 60000);
        timer = new Timer();
        timer.scheduleAtFixedRate(new UpdateConfigurations(), updateInterval, updateInterval);
    }
    
    
    /**
     * Update the map of configurations
     * @param configs
     */
    private void updateConfigurations(Collection<Configuration> configs) {

        writeLock.lock();
        
        try {
            // Don't allow other thread to access the map during update
            configurations.clear();
            for (Configuration conf : configs) {
                configurations.put(conf.getName(), conf);
            }
        } finally {
            writeLock.unlock();
        }

    }
    
    /**
     * Return a read only map of all the configurations.
     * @return a read only map of all the configurations.
     */
    public Map<String, Configuration> getAllConfigurations() {
        Map<String, Configuration> result = null;
        
        readLock.lock();
        try {
            result = Collections.unmodifiableMap(configurations);
        } finally {
            readLock.unlock();
        }
        
        return result;   
    }
    
    /**
     * Return the configuration with the key in parameter.
     * @param key the key
     * @return the configuration
     */
    public Configuration getConfiguration(String key) {
        Configuration result = null;
        
        readLock.lock();
        try {
            result = configurations.get(key);
        } finally {
            readLock.unlock();
        }
        
        return result;
    }
    
    /**
     * Return the configuration value as boolean.
     * @param key the configuration key
     * @return the configuration value as boolean, null if don't exist
     */
    public Boolean getConfAsBoolean(String key) {
        Boolean result = null;
        
        Configuration conf = getConfiguration(key);
        if (conf != null) {
            result = conf.getBooleanValue();
        }
        
        return result;
    }
    
    /**
     * Return the configuration value as boolean or the default value if it don't exist
     * @param key the configuration key
     * @param defaultValue the default value
     * @return the configuration value as boolean
     */
    public Boolean getConfAsBoolean(String key, boolean defaultValue) {
        Boolean result = getConfAsBoolean(key);
        
        if (result == null) {
            result = defaultValue;
        }
        
        return result;
    }
    
    /**
     * Return the configuration value as Integer.
     * @param key the configuration key
     * @return the configuration value as integer, null if don't exist
     */
    public Integer getConfAsInteger(String key) {
        Integer result = null;
        
        Configuration conf = getConfiguration(key);
        if (conf != null) {
            result = conf.getIntegerValue();
        }
        
        return result;
    }
    
    /**
     * Return the configuration value as Integer or the default value if it don't exist
     * @param key the configuration key
     * @param defaultValue the default value
     * @return the configuration value as Integer
     */
    public Integer getConfAsInteger(String key, int defaultValue) {
        Integer result = getConfAsInteger(key);
        
        if (result == null) {
            result = defaultValue;
        }
        
        return result;
    }
    
    /**
     * Return the configuration value as Long.
     * @param key the configuration key
     * @return the configuration value as Long, null if don't exist
     */
    public Long getConfAsLong(String key) {
        Long result = null;
        
        Configuration conf = getConfiguration(key);
        if (conf != null) {
            result = conf.getLongValue();
        }
        
        return result;
    }
    
    /**
     * Return the configuration value as Long or the default value if it don't exist
     * @param key the configuration key
     * @param defaultValue the default value
     * @return the configuration value as Long
     */
    public Long getConfAsLong(String key, long defaultValue) {
        Long result = getConfAsLong(key);
        
        if (result == null) {
            result = defaultValue;
        }
        
        return result;
    }
    
    /**
     * Return the configuration value as String.
     * @param key the configuration key
     * @return the configuration value as String, null if don't exist
     */
    public String getConfAsString(String key) {
        String result = null;
        
        Configuration conf = getConfiguration(key);
        if (conf != null) {
            result = conf.getValue();
        }
        
        return result;
    }
    
    /**
     * Return the configuration value as String or the default value if it don't exist
     * @param key the configuration key
     * @param defaultValue the default value
     * @return the configuration value as String
     */
    public String getConfAsString(String key, String defaultValue) {
        String result = getConfAsString(key);
        
        if (result == null) {
            result = defaultValue;
        }
        
        return result;
    }
    
    /**
     * Delete cached values before {@link FleetDataCache#cacheInterval}
     * @param timestamp timestamp
     */
    private class UpdateConfigurations extends TimerTask {

        @Override
        public void run() {
            
            try {
                updateConfigurations(configurationProvider.getAll());
            } catch (Exception e) {
                LOGGER.error("Can't update the configurations", e);
            }
            
        }

    }

}
