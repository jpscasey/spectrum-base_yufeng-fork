package com.nexala.spectrum.configuration;

import java.util.List;

import org.apache.log4j.Logger;

import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.google.common.collect.Lists;
import com.nexala.spectrum.db.beans.ChannelConfig;
import com.nexala.spectrum.db.dao.FleetChannelDao;

public abstract class FleetChannelConfig {
    private final Logger LOGGER = Logger.getLogger(FleetChannelConfig.class);

    private FleetChannelDao fleetChannelDao;

    private String fleet;

    private List<Integer> fleetChannelCache;
    
    protected FleetChannelConfig(String fleet, FleetChannelDao fleetChannelDao) {
        this.fleet = fleet;
        this.fleetChannelDao = fleetChannelDao;
        loadValues();
    }

    private void loadValues() {
        long startTime = System.currentTimeMillis();
        fleetChannelCache = fleetChannelDao.getInvalidChannels(fleet);
        LOGGER.info(String.format("Loaded %d invalid channel settings for Fleet %s in %dms", fleetChannelCache.size(),
                fleet, System.currentTimeMillis() - startTime));
    }

    public List<ChannelConfig> filterChannelConfig(List<ChannelConfig> channels) {
        return Lists.newArrayList(Collections2.filter(channels, new Predicate<ChannelConfig>() {
            public boolean apply(ChannelConfig channel) {
                return !fleetChannelCache.contains(channel.getId());
            }
        }));
    }

    public List<Integer> filter(List<Integer> channels) {
        return Lists.newArrayList(Collections2.filter(channels, new Predicate<Integer>() {
            public boolean apply(Integer channel) {
                return !fleetChannelCache.contains(channel);
            }
        }));
    }

}
