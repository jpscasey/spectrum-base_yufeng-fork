/*
 * Copyright (c) Nexala Technologies 2013, All rights reserved.
 */
package com.nexala.spectrum.configuration;

import com.nexala.spectrum.channel.Channel;
import com.nexala.spectrum.channel.ChannelCollection;
import com.nexala.spectrum.db.beans.ChannelConfig;

public interface ChannelCalculationStrategy {
    /**
     * Calculates a value for the specified calculatedChannel
     * 
     * @param calculatedChannel The channel to calculate the value for
     * @param channels The set of actual channel values.
     * @return
     */
    public Channel<?> calculate(ChannelConfig calculatedChannel,
            ChannelCollection channels);
}