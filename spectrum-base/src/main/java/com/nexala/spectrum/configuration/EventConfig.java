/*
 * Copyright (c) Nexala Technologies 2016, All rights reserved.
 */
package com.nexala.spectrum.configuration;

import java.util.ArrayList;
import java.util.List;

import com.google.inject.Inject;
import com.nexala.spectrum.rest.data.beans.EventField;
import com.typesafe.config.Config;

/**
 * Get the configuration for an Event from config file
 * @author Fulvio
 */
public class EventConfig {

    private List<EventField> eventFields;

    @Inject
    public EventConfig(ApplicationConfiguration applicationConfiguration) {

        this.eventFields = new ArrayList<EventField>();
        List<? extends Config> eventConfig = applicationConfiguration.get().getConfigList("r2m.event");
        
        for (Config conf : eventConfig) {
            
            EventField field = new EventField();
            
            field.setId(conf.getString("id"));
            field.setDbName(conf.getString("db"));
            field.setType(conf.getString("type"));

            List<String> fleets = new ArrayList<String>();
            if (conf.hasPath("fleets")) {
                for (String fleetId : conf.getStringList("fleets")) {
                    fleets.add(fleetId);
                }
            }
            field.setFleets(fleets);

            List<String> screens = new ArrayList<String>();
            if (conf.hasPath("screens")) {
                for (String screen : conf.getStringList("screens")) {
                    screens.add(screen);
                }
            }
            field.setScreens(screens);

            this.eventFields.add(field);
        }
    }

    public List<EventField> getFields() {
        return eventFields;
    }
}
