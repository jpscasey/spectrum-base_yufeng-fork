/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.configuration;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.db.beans.ChannelConfig;
import com.nexala.spectrum.db.beans.ChannelGroupConfig;
import com.nexala.spectrum.db.beans.ChannelVehicleTypeConfig;
import com.nexala.spectrum.db.dao.ChannelConfigDao;
import com.nexala.spectrum.db.dao.ChannelGroupConfigDao;
import com.nexala.spectrum.db.dao.ChannelVehicleTypeConfigDao;
import com.nexala.spectrum.db.dao.DataAccessException;

@Singleton
public class ChannelConfigurationBeanCache {
    private final Logger LOGGER = Logger.getLogger(ChannelConfigurationBeanCache.class);

    private final ChannelConfigDao confDao;
    private final ChannelGroupConfigDao groupConfDao;
    private final ChannelVehicleTypeConfigDao channelVehicleTypeDao;
    protected final ChannelConfigurationBeanFactory factory;
    private final Long refreshInterval;

    protected ChannelConfigurationBean configurationBean;
    private long lastUpdateCheck = 0;

    @Inject
    public ChannelConfigurationBeanCache(ChannelConfigDao confDao, ChannelGroupConfigDao groupConfDao,
            ConfigurationManager conf, ChannelConfigurationBeanFactory factory, ChannelVehicleTypeConfigDao channelVehicleTypeConfigDao)
            throws DataAccessException {

        this.confDao = confDao;
        this.groupConfDao = groupConfDao;
        this.factory = factory;
        this.channelVehicleTypeDao = channelVehicleTypeConfigDao;

        this.refreshInterval = conf.getConfAsLong(Spectrum.SPECTRUM_REFRESH_CHANNELCONFIGURATIONS_INTERVAL);

        loadChannelDefinitions();

        this.lastUpdateCheck = System.currentTimeMillis();
    }

    public synchronized ChannelConfigurationBean getChannelConfigurationBean() {
        long timeout = System.currentTimeMillis() - lastUpdateCheck;

        if (refreshInterval != null && refreshInterval >= 0 && timeout >= refreshInterval) {

            loadChannelDefinitions();

            this.lastUpdateCheck = System.currentTimeMillis();
        }

        return configurationBean;
    }

    private void loadChannelDefinitions() {
        long startTime = System.currentTimeMillis();

        List<ChannelConfig> channelConfigList = confDao.findAll();
        List<ChannelGroupConfig> channelGroupConfigList = groupConfDao.findAll();
        List<ChannelVehicleTypeConfig> channelVehicleTypeConfigList = channelVehicleTypeDao.findAll();
        Map<Integer, Integer> idToChannel = new HashMap<Integer, Integer>();
        Map<String, Integer> nameToChannel = new HashMap<String, Integer>();
        Map<String, Integer> shortNameToChannel = new HashMap<String, Integer>();
        Map<Integer, List<Integer>> channelIdToVehicleTypeList = new HashMap<Integer, List<Integer>>();

        for (int i = 0, l = channelConfigList.size(); i < l; i++) {
            ChannelConfig channel = channelConfigList.get(i);

            ChannelGroupConfig group = getGroup(channel.getChannelGroupId(), channelGroupConfigList);

            // For each channel, get it's ChannelGroupConfig object
            // and add it to that group.
            group.addChannelConfig(channel);

            // Populate the nameToChannel index.
            nameToChannel.put(channel.getName().toUpperCase(), i);

            // Populate the idToChannel index.
            idToChannel.put(channel.getId(), i);

            // Populate the shortNameToChannel index.
            shortNameToChannel.put(channel.getShortName(), i);
        }
        
        for (ChannelVehicleTypeConfig channelVehicleTypeConfig : channelVehicleTypeConfigList) {
            if (!channelIdToVehicleTypeList.containsKey(channelVehicleTypeConfig.getChannelId())) {
                channelIdToVehicleTypeList.put(channelVehicleTypeConfig.getChannelId(), new ArrayList<Integer>());
            }
            channelIdToVehicleTypeList.get(channelVehicleTypeConfig.getChannelId()).add(channelVehicleTypeConfig.getVehicleTypeId());
        }

        configurationBean = factory.create(
                Collections.unmodifiableList(channelConfigList),
                Collections.unmodifiableList(channelGroupConfigList),
                Collections.unmodifiableList(channelVehicleTypeConfigList),
                Collections.unmodifiableMap(idToChannel),
                Collections.unmodifiableMap(nameToChannel),
                Collections.unmodifiableMap(shortNameToChannel),
                Collections.unmodifiableMap(channelIdToVehicleTypeList)
            );

        LOGGER.info("Loaded " + channelConfigList.size() + " channel definitions: "
                + (System.currentTimeMillis() - startTime) + "ms");
    }

    private ChannelGroupConfig getGroup(Integer id, List<ChannelGroupConfig> channelGroupConfigList) {
        if (channelGroupConfigList != null) {
            for (ChannelGroupConfig conf : channelGroupConfigList) {
                if (conf.getId().equals(id)) {
                    return conf;
                }
            }
        }

        return null;
    }
    
}