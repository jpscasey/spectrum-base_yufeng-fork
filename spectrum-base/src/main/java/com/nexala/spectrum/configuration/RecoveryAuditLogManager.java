package com.nexala.spectrum.configuration;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.nexala.spectrum.db.beans.RecoveryAuditItem;
import com.nexala.spectrum.db.beans.RecoveryAuditItemType;
import com.nexala.spectrum.db.dao.RecoveryAuditItemDao;
import com.nexala.spectrum.db.dao.RecoveryAuditItemTypeDao;

@Singleton
public class RecoveryAuditLogManager {
    
    private Map<Object, Long> faultIdentifiers = new ConcurrentHashMap<Object, Long>();
    private HashMap<String, Long> precedingSteps;

    @Inject
    private RecoveryAuditItemDao itemDao;

    @Inject
    private RecoveryAuditItemTypeDao itemTypeDao;
    
    /**
     * Acquire a reference to the current instance of the manager.
     * @return
     */
    
    /**
     * Main constructor.
     */
    @Inject
    public RecoveryAuditLogManager(RecoveryAuditItemDao itemDao,
            RecoveryAuditItemTypeDao itemTypeDao) {
        
        this.itemDao = itemDao;
        this.itemTypeDao = itemTypeDao;
        this.precedingSteps = new HashMap<String, Long>();
    }
    
    /**
     * Logs a process execution event.
     * 
     * @param recoveryId
     * @param contextId
     * @param username
     */
    public void logExecuteProcess(Long faultInstanceId, String contextId, 
            String username) {
        
        faultIdentifiers.put(contextId, faultInstanceId);
        this.logItem(faultInstanceId, contextId, false, null, null,
                null, true, false, true, false, false, username);
    }
    
    /**
     * Logs a get next step event
     * 
     */
    public void logNextStep(String contextId, String itemText,
            String username) {
        Long faultInstanceId = faultIdentifiers.get(contextId);
        
        if(faultInstanceId == null) {
            faultInstanceId = 0L;
        }
        
        this.logItem(faultInstanceId, contextId, false, itemText, null, null,
                false, false, false, false, true, username);
    }
    
    /**
     * Logs a channel data result on a question.
     */
    public void logChannelDataResult(String contextId,
            String itemText, String itemResponse, String username) {
        Long faultInstanceId = faultIdentifiers.get(contextId);
        
        if(faultInstanceId == null) {
            faultInstanceId = 0L;
        }
        
        this.logItem(faultInstanceId, contextId, true, itemText, itemResponse,
                null, false, false, false, false, false, username);
    }
    
    /**
     * Logs a user answering a question.
     */
    public void logQuestionAnswer(String contextId, String itemText,
            String itemResponse, String notes, String username) {
        Long faultInstanceId = faultIdentifiers.get(contextId);
        
        if(faultInstanceId == null) {
            faultInstanceId = 0L;
        }
        
        this.logItem(faultInstanceId, contextId, true, itemText, itemResponse, 
                notes, true, false, false, false, false, username);
    }
    
    /**
     * Logs a user action result (worked or didn't work).
     */
    public void logActionResult(String contextId, String itemText,
            String itemResponse, String notes, String username) {
        Long faultInstanceId = faultIdentifiers.get(contextId);
        
        if(faultInstanceId == null) {
            faultInstanceId = 0L;
        }
        
        this.logItem(faultInstanceId, contextId, false, itemText, itemResponse,
                notes, true, false, false, false, false, username);
    }
    
    /**
     * Logs a process outcome.
     */
    public void logOutcome(String contextId,
            String itemText, String username) {
        Long faultInstanceId = faultIdentifiers.get(contextId);
        
        if(faultInstanceId == null) {
            faultInstanceId = 0L;
        }
        
        this.logItem(faultInstanceId, contextId, false, itemText, null, null, false, 
                false, false, true, false, username);
    }
    
    /**
     * Logs a reversion event
     */
    public void logReversion(String contextId,
            String itemText, String username) {
        Long faultInstanceId = faultIdentifiers.get(contextId);
        
        if(faultInstanceId == null) {
            faultInstanceId = 0L;
        }
        
        this.logItem(faultInstanceId, contextId, false, itemText, null, null, true,
                true, false, false, false, username);
    }
    
    /**
     * Logs a recovery completion event.
     * 
     * @param contextId         Recovery context ID
     * @param username          Username to be logged
     */
    public void logCompletion(String contextId, String username) {
        Long faultInstanceId = faultIdentifiers.get(contextId);
        
        if(faultInstanceId == null) {
            faultInstanceId = 0L;
        }
        
        this.logItem(faultInstanceId, contextId, false, null, null, null, true,
                false, false, true, false, username);
        
        faultIdentifiers.remove(contextId);
    }
    
    /**
     * Log an item.
     * 
     * @param faultInstanceId   The ID of the fault instance in the RTM database.
     * @param contextId         The ID of the current recovery context (from
     *                          the recovery manager).
     * @param isQuestion        Whether this item is a question (true) or an 
     *                          action (false).
     * @param itemText          The text of the question or the action.
     * @param itemResponse      The text of the response selected by the user.
     * @param isUserAction      Whether this is an action performed by the user
     *                          (e.g. a click on a button) or not (e.g. a
     *                          channel data check result).
     * @param username          The user carrying out the recovery.
     */
    private synchronized void logItem(Long faultInstanceId, String contextId, 
            boolean isQuestion, String itemText, String itemResponse, String notes, 
            boolean isUserAction, boolean isReversion, boolean isExecution,
            boolean isOutcome, boolean isNextStep, String username) {
        
        RecoveryAuditItemType itemType = itemTypeDao.findBySignature(isQuestion, 
                itemText, itemResponse, isUserAction, isReversion, 
                isExecution, isOutcome, isNextStep);
        
        if(itemType == null) {
            itemType = new RecoveryAuditItemType();
            itemType.setItemText(itemText);
            itemType.setAnswer(itemResponse);
            itemType.setQuestion(isQuestion);
            itemType.setUserAction(isUserAction);
            itemType.setReversion(isReversion);
            itemType.setOutcome(isOutcome);
            itemType.setExecution(isExecution);
            itemType.setNextStep(isNextStep);
            
            Long id = itemTypeDao.create(itemType);
            itemType.setId(id);
        }
        
        RecoveryAuditItem item = new RecoveryAuditItem();
        item.setTimestamp(new Date());
        item.setContextId(contextId);
        item.setUserName(username);
        item.setFaultInstanceId(faultInstanceId);
        item.setNotes(notes);
        
        if(this.precedingSteps.containsKey(contextId)) {
            item.setPrecedingId(this.precedingSteps.get(contextId));
        }
        
        item.setAuditItemTypeId(itemType.getId());
        
        Long itemId = itemDao.create(item);
        
        this.precedingSteps.put(contextId, itemId);
    }

}
