/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.configuration;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.nexala.spectrum.db.dao.ChannelConfigDao;
import com.nexala.spectrum.db.dao.ChannelGroupConfigDao;
import com.nexala.spectrum.db.dao.ChannelVehicleTypeConfigDao;
import com.nexala.spectrum.db.dao.DataAccessException;

/**
 * A channel configuration cache which don't reload the channels.
 * (the reload works with a boolean in database, only one can reload(nasty)....) 
 * @author brice
 *
 */
@Singleton
public class StaticChannelConfigurationBeanCache  extends ChannelConfigurationBeanCache {
    
    @Inject
    public StaticChannelConfigurationBeanCache(ChannelConfigDao confDao, ChannelGroupConfigDao groupConfDao,
            ConfigurationManager confManager, ChannelConfigurationBeanFactory factory, ChannelVehicleTypeConfigDao channelVehicleTypeConfigDao)
            throws DataAccessException {
        super(confDao, groupConfDao, confManager, factory, channelVehicleTypeConfigDao);
    }

    /**
     * Don't check for reload channel configurations.
     * @see com.nexala.spectrum.configuration.ChannelConfigurationBeanCache#getChannelConfigurationBean()
     */
    @Override
    public synchronized ChannelConfigurationBean getChannelConfigurationBean() {
        return configurationBean;
    }

}