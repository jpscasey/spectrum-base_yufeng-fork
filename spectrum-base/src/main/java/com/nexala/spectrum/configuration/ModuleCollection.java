/*
 * Copyright (c) Nexala Technologies 2013, All rights reserved.
 */
package com.nexala.spectrum.configuration;

import com.google.inject.Module;

/**
 * An interface which represents a collection of modules. ModuleCollections may
 * be instantiated using reflection [via the default constructor]. So,
 * implementing classes must not have a private/protected constructor or
 * anything else which will prevent default instantiation, e.g. new
 * MyModuleCollection()
 */
public interface ModuleCollection {
    /**
     * Returns an array of Guice modules. 
     * @return
     */
    Module[] getModules();
}
