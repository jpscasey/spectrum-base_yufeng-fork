package com.nexala.spectrum.configuration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.google.inject.Singleton;
import com.nexala.spectrum.channel.ChannelCategory;
import com.nexala.spectrum.i18n.ResourceBundleUTF8;
import com.nexala.spectrum.rest.data.beans.BeanField;
import com.nexala.spectrum.rest.service.bean.EventFilterBean;
import com.nexala.spectrum.rest.service.bean.FilterOptionBean;
import com.nexala.spectrum.view.Plugin;
import com.nexala.spectrum.view.conf.Column;
import com.nexala.spectrum.view.conf.ColumnBuilder;
import com.nexala.spectrum.view.conf.ColumnType;
import com.nexala.spectrum.view.conf.DisabledCondition;
import com.nexala.spectrum.view.conf.DropdownOption;
import com.nexala.spectrum.view.conf.Field;
import com.nexala.spectrum.view.conf.FieldBuilder;
import com.nexala.spectrum.view.conf.FieldType;
import com.nexala.spectrum.view.conf.LabelValue;
import com.nexala.spectrum.view.conf.MouseOverText;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

@Singleton
public class ApplicationConfiguration {
    private final Config conf;
    
    public ApplicationConfiguration() {
        conf = 
            // customer specific files
            ConfigFactory.load("config/event/r2m-event-override")
                .withFallback(ConfigFactory.load("config/event/r2m-eventAnalysis-override"))
                .withFallback(ConfigFactory.load("config/event/r2m-eventDetail-override"))
                .withFallback(ConfigFactory.load("config/event/r2m-eventHistory-override"))
                .withFallback(ConfigFactory.load("config/event/r2m-liveAndRecentPanel-override"))
                .withFallback(ConfigFactory.load("config/fleet/r2m-fleetLocation-override"))
                .withFallback(ConfigFactory.load("config/fleet/r2m-fleetSummary-override"))
                .withFallback(ConfigFactory.load("config/unit/r2m-dataView-override"))
                .withFallback(ConfigFactory.load("config/unit/r2m-unitDetail-override"))
                .withFallback(ConfigFactory.load("config/unit/r2m-unitSummary-override"))
                .withFallback(ConfigFactory.load("config/unit/r2m-recovery-override"))
                .withFallback(ConfigFactory.load("config/unit/r2m-channelData-override.conf"))
                .withFallback(ConfigFactory.load("config/unit/r2m-schematic-override"))
                .withFallback(ConfigFactory.load("config/unit/r2m-unitSearch-override.conf"))
                .withFallback(ConfigFactory.load("config/r2m-channelDefinition-override"))
                .withFallback(ConfigFactory.load("config/r2m-download-override"))
                .withFallback(ConfigFactory.load("config/r2m-editor-override"))
                .withFallback(ConfigFactory.load("config/r2m-screens-override"))
                .withFallback(ConfigFactory.load("config/r2m-stream-analysis-override"))
                .withFallback(ConfigFactory.load("config/r2m-override"))
                .withFallback(ConfigFactory.load("config/r2m-queries-override.conf"))
                .withFallback(ConfigFactory.load("config/r2m-dropdowns-override.conf"))
                .withFallback(ConfigFactory.load("config/r2m-tabular-styles-override.conf"))
            // base config files
                .withFallback(ConfigFactory.load("config/event/r2m-event"))
                .withFallback(ConfigFactory.load("config/event/r2m-eventAnalysis"))
                .withFallback(ConfigFactory.load("config/event/r2m-eventDetail"))
                .withFallback(ConfigFactory.load("config/event/r2m-eventHistory"))
                .withFallback(ConfigFactory.load("config/event/r2m-liveAndRecentPanel"))
                .withFallback(ConfigFactory.load("config/fleet/r2m-fleetLocation"))
                .withFallback(ConfigFactory.load("config/fleet/r2m-fleetSummary"))
                .withFallback(ConfigFactory.load("config/unit/r2m-dataView"))
                .withFallback(ConfigFactory.load("config/unit/r2m-unitDetail"))
                .withFallback(ConfigFactory.load("config/unit/r2m-unitSummary"))
                .withFallback(ConfigFactory.load("config/unit/r2m-recovery"))
                .withFallback(ConfigFactory.load("config/unit/r2m-channelData"))
        		.withFallback(ConfigFactory.load("config/unit/r2m-schematic"))
                .withFallback(ConfigFactory.load("config/unit/r2m-unitSearch.conf"))
                .withFallback(ConfigFactory.load("config/r2m-channelDefinition"))
                .withFallback(ConfigFactory.load("config/r2m-download"))
                .withFallback(ConfigFactory.load("config/r2m-editor"))
                .withFallback(ConfigFactory.load("config/r2m-screens"))
                .withFallback(ConfigFactory.load("config/r2m-stream-analysis"))
                .withFallback(ConfigFactory.load("config/r2m"))
                .withFallback(ConfigFactory.load("config/r2m-queries.conf"))
                .withFallback(ConfigFactory.load("config/r2m-dropdowns.conf"))
                .withFallback(ConfigFactory.load("config/r2m-tabular-styles.conf"));
    }

    public Config get() {
        return conf;
    }

    public List<Plugin> getPlugins(String screen) {
        List<Plugin> result = new ArrayList<Plugin>();
        
        if (conf.hasPath("r2m." + screen + ".plugins")) {
            List<? extends Config> plugins = conf.getConfigList("r2m." + screen + ".plugins");
            for (Config plugin : plugins) {
                result.add(extractPlugin(plugin));
            }
        }
        return result;
    }

    public Plugin getParentPlugin(String screen) {
        
        if (conf.hasPath("r2m." + screen + ".parentPlugin")) {
            return extractPlugin(conf.getConfig("r2m." + screen + ".parentPlugin"));
        }

        return new Plugin("", Collections.<String> emptyList(),
                Collections.<String> emptyList());
    }

    private Plugin extractPlugin(Config pluginConfig) {
        String name = pluginConfig.getString("name");
        List<String> jsList = new ArrayList<String>();
        List<String> cssList = new ArrayList<String>();

        if (pluginConfig.hasPath("jsList")) {
            jsList = pluginConfig.getStringList("jsList");
        }

        if (pluginConfig.hasPath("cssList")) {
            cssList = pluginConfig.getStringList("cssList");
        }

        return new Plugin(name, jsList, cssList);
    }

    public Map<String, Config> getFleetConfigList() {
        Map<String, Config> result = new HashMap<String, Config>();

        Config fleets = conf.getConfig("r2m.fleets");
        List<String> fleetIds = new ArrayList<String>(fleets.root().unwrapped().keySet());
        
        for (String fleetId : fleetIds) {
            result.put(fleetId, fleets.getConfig(fleetId));
        }

        return result;
    }

    public Field buildField(Config config, Integer defaultWidth) {
        FieldBuilder fieldBuilder = getFieldBuilder(config, defaultWidth, null);
        
        return fieldBuilder.build();
    }
    
    public FieldBuilder getFieldBuilder(Config config, Integer defaultWidth, ResourceBundleUTF8 bundle) {
        
        FieldBuilder fieldBuilder = new FieldBuilder();

        if (config.hasPath("displayName")) {
            fieldBuilder.displayName(config.getString("displayName"));
        }
        if (config.hasPath("name")) {
            fieldBuilder.name(config.getString("name"));
        }
        if (config.hasPath("format")) {
            fieldBuilder.formatter(config.getString("format"));
        }
        if (config.hasPath("visible")) {
            fieldBuilder.visible(config.getBoolean("visible"));
        }
        if (config.hasPath("linkable")) {
            fieldBuilder.linkable(config.getBoolean("linkable"));
        }
        if (config.hasPath("referencedField")) {
            fieldBuilder.referencedField(config.getString("referencedField"));
        }
        if (config.hasPath("width")) {
            fieldBuilder.width(config.getInt("width"));
        } else if (defaultWidth != null) {
            fieldBuilder.width(defaultWidth);
        }
        if (config.hasPath("fleets")) {
            fieldBuilder.fleets(config.getStringList("fleets"));
        }
        if (config.hasPath("editable")) {
            fieldBuilder.editable(config.getBoolean("editable"));
        }
        if (config.hasPath("mandatory")) {
            fieldBuilder.mandatory(config.getBoolean("mandatory"));
        }
        if (config.hasPath("maxLength")) {
            fieldBuilder.maxLength(config.getInt("maxLength"));
        }
        if (config.hasPath("min")) {
            fieldBuilder.min(config.getInt("min"));
        }
        if (config.hasPath("max")) {
            fieldBuilder.max(config.getInt("max"));
        }
        if (config.hasPath("style")) {
            fieldBuilder.style(config.getString("style"));
        }

        if (config.hasPath("type")) {
            String type = config.getString("type");
            switch (type.toUpperCase()) {
            case "SELECT":
                fieldBuilder.type(FieldType.SELECT);
                break;
            case "INPUT_TEXT":
                fieldBuilder.type(FieldType.INPUT_TEXT);
                break;
            case "SIMPLE_TEXT":
                fieldBuilder.type(FieldType.SIMPLE_TEXT);
                break;
            case "MULTIPLE_SELECT":
                fieldBuilder.type(FieldType.MULTIPLE_SELECT);
                break;
            case "TEXT_AREA":
                fieldBuilder.type(FieldType.TEXT_AREA);
                break;
            case "CHECKBOX":
                fieldBuilder.type(FieldType.CHECKBOX);
                break;
            case "INPUT_NUMBER":
                fieldBuilder.type(FieldType.INPUT_NUMBER);
                break;
            case "TABLE":
                fieldBuilder.type(FieldType.TABLE);
                break;
            case "DATE":
            	fieldBuilder.type(FieldType.DATE);
            	break;
            case "TIME":
            	fieldBuilder.type(FieldType.TIME);
                break;
            case "AUTOCOMPLETE":
                fieldBuilder.type(FieldType.AUTOCOMPLETE);
                break;
            default:
                fieldBuilder.type(FieldType.LABEL);
            }
        }

        if (config.hasPath("columns")) {
            List<? extends Config> columnFields = config.getConfigList("columns");
            List<Column> columns = new ArrayList<>();
            for (Config columnConfig : columnFields) {
                columns.add(this.columnBuilder(columnConfig).build());
            }
            fieldBuilder.columns(columns);
        }
        
        if (config.hasPath("i18nDisplayName")) {
            String label = config.getString("i18nDisplayName");
            if (bundle != null) {
                label = bundle.getString(label);
            }
            fieldBuilder.displayName(label);
        }
        
        if (config.hasPath("values")) {
            List<LabelValue> labelValues = new ArrayList<>();
            List<? extends Config> values = config.getConfigList("values");
            for (Config currentVal : values) {
                labelValues.add(buildLabelValue(currentVal, bundle));
            }
            
            fieldBuilder.setValues(labelValues);
        }
        
        if (config.hasPath("dropdownOptions")) {
            List<DropdownOption> dropdownOptions = new ArrayList<>();
            List<? extends Config> values = config.getConfigList("dropdownOptions");
            for (Config currentVal : values) {
                dropdownOptions.add(buildDropdownOptions(currentVal, bundle));
            }
            
            fieldBuilder.setDropdownOptions(dropdownOptions);
        }
        
        if (config.hasPath("disabledCondition")) {
            List<DisabledCondition> disabledConditions = new ArrayList<>();
            List<? extends Config> conditions = config.getConfigList("disabledCondition");
            for (Config currentVal : conditions) {
                disabledConditions.add(buildDisabledConditions(currentVal, bundle));
            }
            
            fieldBuilder.setDisabledConditions(disabledConditions);
        }
        
        if (config.hasPath("usages")) {
        	fieldBuilder.setUsages(config.getStringList("usages"));
        }
        
        if (config.hasPath("unit")) {
        	fieldBuilder.setUnit(config.getString("unit"));
        }
        
        return fieldBuilder;
    }
    
    public LabelValue buildLabelValue(Config labelValueConf, ResourceBundleUTF8 bundle) {
        LabelValue lblValue = new LabelValue();
        lblValue.setValue(labelValueConf.getString("value"));

        String label = labelValueConf.getString("label");
        if (bundle != null) {
            label = bundle.getString(label);
        }
        lblValue.setLabel(label);
        
        return lblValue;
    }
    
    public DropdownOption buildDropdownOptions(Config dropdownOptionConf, ResourceBundleUTF8 bundle) {
        DropdownOption dropdown = new DropdownOption();
        dropdown.setValue(dropdownOptionConf.getString("value"));

        String label = dropdownOptionConf.getString("label");
        if (bundle != null) {
            label = bundle.getString(label);
        }
        dropdown.setLabel(label);
        
        if (dropdownOptionConf.hasPath("licence")) {
            dropdown.setLicence(dropdownOptionConf.getString("licence"));
        }
        
        return dropdown;
    }
    
    public DisabledCondition buildDisabledConditions(Config dropdownOptionConf, ResourceBundleUTF8 bundle) {
        DisabledCondition disabledCondition = new DisabledCondition();
        
        if (dropdownOptionConf.hasPath("filterName")) {
        	disabledCondition.setFilterName(dropdownOptionConf.getString("filterName"));
        }
        
        if (dropdownOptionConf.hasPath("operator")) {
        	disabledCondition.setOperator(dropdownOptionConf.getString("operator"));
        }
        
        if (dropdownOptionConf.hasPath("value")) {
        	disabledCondition.setValue(dropdownOptionConf.getString("value"));
        } 
        
        return disabledCondition;
    }

    public ColumnBuilder columnBuilder(Config columnConfig) {
        ColumnBuilder columnBuilder = new ColumnBuilder();

        if (columnConfig.hasPath("displayName")) {
            columnBuilder.displayName(columnConfig.getString("displayName"));
        }
        if (columnConfig.hasPath("name")) {
            columnBuilder.name(columnConfig.getString("name"));
        }
        if (columnConfig.hasPath("visible")) {
            columnBuilder.visible(columnConfig.getBoolean("visible"));
        }
        if (columnConfig.hasPath("sortable")) {
            columnBuilder.sortable(columnConfig.getBoolean("sortable"));
        }
        if (columnConfig.hasPath("mouseOverHeader")) {
            columnBuilder.mouseOverHeader(columnConfig.getString("mouseOverHeader"));
        }
        if (columnConfig.hasPath("width")) {
            columnBuilder.width(columnConfig.getInt("width"));
        }
        if (columnConfig.hasPath("handler")) {
            columnBuilder.handler(columnConfig.getString("handler"));
        }
        if (columnConfig.hasPath("format")) {
            columnBuilder.format(columnConfig.getString("format"));
        }
        if (columnConfig.hasPath("fleets")) {
            columnBuilder.fleets(columnConfig.getStringList("fleets"));
        }
        if (columnConfig.hasPath("constant")) {
            columnBuilder.constant(columnConfig.getString("constant"));
        }
        if (columnConfig.hasPath("licence")) {
            columnBuilder.licence(columnConfig.getString("licence"));
        }
        if (columnConfig.hasPath("cssClass")) {
            columnBuilder.cssClass(columnConfig.getString("cssClass"));
        }
        if (columnConfig.hasPath("resizable")) {
            columnBuilder.resizable(columnConfig.getBoolean("resizable"));
        }
        if (columnConfig.hasPath("sortByColumn")) {
            columnBuilder.sortByColumn(columnConfig.getString("sortByColumn"));
        }
        if (columnConfig.hasPath("autosize")) {
            columnBuilder.autosize(columnConfig.getBoolean("autosize"));
        }
        if (columnConfig.hasPath("exportable")) {
            columnBuilder.exportable(columnConfig.getBoolean("exportable"));
        }
        if (columnConfig.hasPath("type")) {
            String type = columnConfig.getString("type");
            switch (type.toUpperCase()) {
                case "ANALOGUE":
                    columnBuilder.type(ColumnType.ANALOGUE);
                    break;
                case "DIGITAL":
                    columnBuilder.type(ColumnType.DIGITAL);
                    break;
                case "TEXT":
                    columnBuilder.type(ColumnType.TEXT);
                    break;
                case "STOCK":
                    columnBuilder.type(ColumnType.STOCK);
                    break;
                case "DURATION":
                    columnBuilder.type(ColumnType.DURATION);
                    break;
                case "GROUP":
                    columnBuilder.type(ColumnType.GROUP);
                    break;
                case "TIMESTAMP":
                    columnBuilder.type(ColumnType.TIMESTAMP);
                    break;
                case "CHECKBOX":
                    columnBuilder.type(ColumnType.CHECKBOX);
                    break;
                default:
                    columnBuilder.type(ColumnType.DIGITAL);
            }
        }
        if (columnConfig.hasPath("rowMouseOver")) {
            List<MouseOverText> mouseOverContent = new ArrayList<MouseOverText>();
            List<? extends Config> mouseOverLines = columnConfig.getConfigList("rowMouseOver");

            for (Config line : mouseOverLines) {
                mouseOverContent.add(buildMouseOver(line));
            }

            columnBuilder.mouseOverContent(mouseOverContent);
        }

        if (columnConfig.hasPath("columnMouseOver")) {
            List<MouseOverText> mouseOverContent = new ArrayList<MouseOverText>();
            List<? extends Config> mouseOverLines = columnConfig.getConfigList("columnMouseOver");

            for (Config line : mouseOverLines) {
                mouseOverContent.add(buildMouseOver(line));
            }

            columnBuilder.mouseOverContent(mouseOverContent);
        }
        
        if (columnConfig.hasPath("displayField")) {
            String displayField = columnConfig.getString("displayField");
            columnBuilder.setDisplayField(displayField);
        }
        if (columnConfig.hasPath("style") && this.conf.hasPath("r2m.tabularStyles." + columnConfig.getString("style"))) {
            columnBuilder.styles(buildStyles(this.conf.getConfigList("r2m.tabularStyles." + columnConfig.getString("style"))));
        }

        return columnBuilder;
    }
    
    public MouseOverText buildMouseOver(Config mouseOverConf) {
        MouseOverText result = new MouseOverText();

        if (mouseOverConf.hasPath("label")) {
            result.setLabel(mouseOverConf.getString("label"));
        }
        if (mouseOverConf.hasPath("value")) {
            result.setValue(mouseOverConf.getString("value"));
        }
        if (mouseOverConf.hasPath("format")) {
            result.setFormat(mouseOverConf.getString("format"));
        }
        if (mouseOverConf.hasPath("constant")) {
            result.setConstant(mouseOverConf.getString("constant"));
        }

        return result;
    }
    
    public Map<ChannelCategory, String> buildStyles(List<? extends Config> styleConfigs) {
        Map<ChannelCategory, String> styles = new HashMap<ChannelCategory, String>();
        
        for (Config styleConfig : styleConfigs) {
            String style = styleConfig.getString("style");
            switch (styleConfig.getString("channelCategory").toUpperCase()) {
                case "INVALID":
                    styles.put(ChannelCategory.INVALID, style);
                    break;
                case "NODATA":
                    styles.put(ChannelCategory.NODATA, style);
                    break;
                case "NORMAL":
                    styles.put(ChannelCategory.NORMAL, style);
                    break;
                case "ON":
                    styles.put(ChannelCategory.ON, style);
                    break;
                case "OFF":
                    styles.put(ChannelCategory.OFF, style);
                    break;
                case "WARNING":
                    styles.put(ChannelCategory.WARNING, style);
                    break;
                case "WARNING1":
                    styles.put(ChannelCategory.WARNING1, style);
                    break;
                case "WARNING2":
                    styles.put(ChannelCategory.WARNING2, style);
                    break;
                case "FAULT":
                    styles.put(ChannelCategory.FAULT, style);
                    break;
                case "FAULT1":
                    styles.put(ChannelCategory.FAULT1, style);
                    break;
                case "FAULT2":
                    styles.put(ChannelCategory.FAULT2, style);
                    break;
                case "EVENT":
                    styles.put(ChannelCategory.EVENT, style);
                    break;
                case "EVENT_AP2":
                    styles.put(ChannelCategory.EVENT_AP2, style);
                    break;
                case "EVENT_AP1":
                    styles.put(ChannelCategory.EVENT_AP1, style);
                    break;
                case "EVENT_P2":
                    styles.put(ChannelCategory.EVENT_P2, style);
                    break;
                case "EVENT_P1":
                    styles.put(ChannelCategory.EVENT_P1, style);
                    break;
            }
        }
        
        return styles;
    }
    
    public EventFilterBean buildEventFilter(Config filterConfig) {
        EventFilterBean result = new EventFilterBean();
        
        if (filterConfig.hasPath("name")) {
            result.setName(filterConfig.getString("name"));
        }
        
        if (filterConfig.hasPath("displayName")) {
            result.setLabel(filterConfig.getString("displayName"));
        }
        
        if (filterConfig.hasPath("label")) {
            result.setLabel(filterConfig.getString("label"));
        }  
        
        if (filterConfig.hasPath("fleets")) {
            List<String> fleets = filterConfig.getStringList("fleets");            
            result.setFleets(fleets);
        }        
        
        if (filterConfig.hasPath("width")) {
        	String widthConfig = filterConfig.getString("width");
        	if (StringUtils.isNumeric(widthConfig)) {
        		int width = Integer.parseInt(widthConfig);
        		result.setWidth(width);
        	}
        }        
        
        if (filterConfig.hasPath("defaultValue")) {
            result.setDefaultValues(Arrays.asList(filterConfig.getString("defaultValue")));
        }
        
        if (filterConfig.hasPath("type")) {
            String type = filterConfig.getString("type");
            switch (type.toUpperCase()) {
            case "SELECT":
                result.setFieldType(FieldType.SELECT);
                break;
            case "INPUT_TEXT":
                result.setFieldType(FieldType.INPUT_TEXT);
                break;
            case "MULTIPLE_SELECT":
                result.setFieldType(FieldType.MULTIPLE_SELECT);
                break;
            case "CHECKBOX":
                result.setFieldType(FieldType.CHECKBOX);
                break;
            case "INVISIBLE":
                result.setFieldType(FieldType.INVISIBLE);
                break;
            default:
                result.setFieldType(FieldType.LABEL);
            }
        }
        
        if (filterConfig.hasPath("idType")) {
            String idType = filterConfig.getString("idType");
            result.setIdType(idType);
        }
        
        if (filterConfig.hasPath("defaultOptions")) {
            List<? extends Config> defaultOptionsConf = filterConfig.getConfigList("defaultOptions");
            List<FilterOptionBean> defaultOptions = new ArrayList<FilterOptionBean>();

            for (Config option : defaultOptionsConf) {
                defaultOptions.add(buildFilterOption(option));
            }

            result.setDefaultOptions(defaultOptions);
        }        
        
        if (filterConfig.hasPath("checkedAsDefault")) {
        	result.setCheckedAsDefault(filterConfig.getBoolean("checkedAsDefault"));
        }
        
        if (filterConfig.hasPath("includeValues")) {
        	result.setIncludeValues(filterConfig.getBoolean("includeValues"));
        }
        
        return result;
    }
    
    public FilterOptionBean buildFilterOption(Config optionConf) {
        FilterOptionBean result = new FilterOptionBean();

        result.setId(optionConf.getString("id"));
        result.setDisplayName(optionConf.getString("displayName"));
        
        return result;
    }
    
    public BeanField buildBeanField(Config beanFieldConf) {
        BeanField result = new BeanField();
        
        result.setId(beanFieldConf.getString("id"));
        result.setDbName(beanFieldConf.getString("db"));
        result.setType(beanFieldConf.getString("type"));
        
        return result;
    }
    
    public String getQuery(String queryId) {
        return conf.getString("r2m.queries." + queryId);
    }
}
