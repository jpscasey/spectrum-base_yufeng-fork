
/*
 * Copyright (c) Nexala Technologies 2011, All rights reserved.
 */

package com.nexala.spectrum.configuration;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This is a collection of static methods for handling file paths
 * 
 * @author poriordan
 */
public final class PathUtils {

    // Prevent instantiation.
    private PathUtils() {
        throw new AssertionError("Instantiation prohibited");
    }

    /**
     * Resolves environment variables in the specified String, which must be in
     * the format \$\{\S+?}. For example, given the input:
     * 
     * <p><code>${JBOSS_HOME}\server\default\deploy</code></p>
     * 
     * will return something like:
     * 
     * <p><code>C:\dev\jboss-4.0.5\server\default\deploy</code></p>  
     * 
     * @param path
     * @return A new String with the environment variables resolved.    
     */
    public static final String resolvePath(final String path) {
        final StringBuffer buffer = new StringBuffer();
        final Pattern pattern = Pattern.compile("[$]\\{(\\S+?)\\}");
        final Matcher matcher = pattern.matcher(path);
        int lastPos = 0;
        
        while (matcher.find()) {
            String varName = matcher.group(1);
            String varValue = System.getenv(varName);
            buffer.append(path.substring(lastPos, matcher.start()));
            buffer.append(varValue);
            lastPos = matcher.end();
        }
        
        buffer.append(path.substring(lastPos, path.length()));
        return buffer.toString();
    }
}
