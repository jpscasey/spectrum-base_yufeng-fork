
/*
 * Copyright (c) Nexala Technologies 2011, All rights reserved.
 */

package com.nexala.spectrum.configuration;

public class ConfigurationException extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = -6971982764815442331L;

    public ConfigurationException(String msg) {
        super(msg);
    }
    
    public ConfigurationException(Exception e) {
        super(e);
    }
    
    public ConfigurationException(String msg, Exception e) {
        super(msg, e);
    }
}
