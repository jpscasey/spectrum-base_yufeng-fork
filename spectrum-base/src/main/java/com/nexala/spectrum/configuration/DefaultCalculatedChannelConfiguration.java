/*
 * Copyright (c) Nexala Technologies 2013, All rights reserved.
 */
package com.nexala.spectrum.configuration;

import java.util.ArrayList;
import java.util.List;

public class DefaultCalculatedChannelConfiguration implements
        CalculatedChannelConfiguration {
    private final List<CalculatedChannel> empty = new ArrayList<CalculatedChannel>();

    @Override
    public List<CalculatedChannel> getCalculatedChannels() {
        return empty;
    }
}
