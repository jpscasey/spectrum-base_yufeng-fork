/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.configuration;

import java.util.ArrayList;
import java.util.List;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.nexala.spectrum.channel.Channel;
import com.nexala.spectrum.db.beans.ChannelConfig;
import com.nexala.spectrum.db.beans.ChannelGroup;
import com.nexala.spectrum.db.beans.ChannelGroupConfig;
import com.nexala.spectrum.db.beans.ChannelVehicleTypeConfig;

@Singleton
public class ChannelConfiguration {
    private final ChannelConfigurationBeanCache cache;

    @Inject
    public ChannelConfiguration(ChannelConfigurationBeanCache cache) {
        this.cache = cache;
    }

    /**
     * Retrieves a ChannelConfig by the channel name (case insensitive)
     * 
     * @param name
     * @return
     */
    public ChannelConfig getConfig(String name) {
        ChannelConfigurationBean bean = cache.getChannelConfigurationBean();

        Integer index = bean.getNameToChannelMap().get(name.toUpperCase());
        if (index == null || index >= bean.getChannelConfigList().size()) {
            return null;
        } else {
            return bean.getChannelConfigList().get(index);
        }
    }

    /**
     * Retrieves a ChannelConfig by the channel id
     * 
     * @param id
     * @return
     */
    public ChannelConfig getConfig(int id) {
        ChannelConfigurationBean bean = cache.getChannelConfigurationBean();

        Integer index = bean.getIdToChannelMap().get(id);
        if (index == null || index >= bean.getChannelConfigList().size()) {
            return null;
        } else {
            return bean.getChannelConfigList().get(index);
        }
    }

    public ChannelConfig getConfig(Channel<?> channel) {
        ChannelConfigurationBean bean = cache.getChannelConfigurationBean();

        final Integer index;
        if (channel.getId() > -1) {
            index = bean.getIdToChannelMap().get(channel.getId());
        } else {
            index = bean.getNameToChannelMap().get(channel.getName());
        }

        if (index == null || index >= bean.getChannelConfigList().size()) {
            return null;
        } else {
            return bean.getChannelConfigList().get(index);
        }
    }

    /**
     * Retrieves a ChannelConfig by it's shortName, e.g. COL1 or its even
     * shorter name C1
     */
    public ChannelConfig getConfigByShortName(String name) {
        ChannelConfigurationBean bean = cache.getChannelConfigurationBean();

        Integer index = bean.getShortNameToChannelMap().get(name);
        if (index == null || index >= bean.getChannelConfigList().size()) {
            return null;
        } else {
            return bean.getChannelConfigList().get(index);
        }
    }

    public ChannelGroupConfig getGroup(Integer id) {
        List<ChannelGroupConfig> channelGroupConfigList = cache.getChannelConfigurationBean()
                .getChannelGroupConfigList();

        if (channelGroupConfigList != null) {
            for (ChannelGroupConfig conf : channelGroupConfigList) {
                if (conf.getId().equals(id)) {
                    return conf;
                }
            }
        }

        return null;
    }

    public ChannelGroupConfig getGroup(String name) {
        List<ChannelGroupConfig> channelGroupConfigList = cache.getChannelConfigurationBean()
                .getChannelGroupConfigList();

        if (channelGroupConfigList != null) {
            for (ChannelGroupConfig conf : channelGroupConfigList) {
                if (conf.getName().equalsIgnoreCase(name)) {
                    return conf;
                }
            }
        }

        return null;
    }

    public List<ChannelGroupConfig> getGroupList() {
        return cache.getChannelConfigurationBean().getChannelGroupConfigList();
    }

    public List<ChannelGroup> getVisibleGroupList() {
        List<ChannelGroupConfig> channelGroupConfigList = cache.getChannelConfigurationBean()
                .getChannelGroupConfigList();

        List<ChannelGroup> groups = new ArrayList<ChannelGroup>();
        for (ChannelGroupConfig group : channelGroupConfigList) {
            if (group.isVisible()) {
                groups.add(new ChannelGroup(group.getId(), group.isVisible(), group.getDescription()));
            }
        }

        return groups;
    }

    public List<ChannelConfig> getConfigList() {
        return cache.getChannelConfigurationBean().getChannelConfigList();
    }
    
    public List<ChannelVehicleTypeConfig> getChannelVehicleTypeConfigList() {
        return cache.getChannelConfigurationBean().getChannelVehicleTypeConfigList();
    }
    
    public List<Integer> getVehicleTypeListByChannelId(Integer channelId) {
        return cache.getChannelConfigurationBean().getChannelIdToVehicleTypeMap().get(channelId);
    }

}