
/*
 * Copyright (c) Nexala Technologies 2011, All rights reserved.
 */

package com.nexala.spectrum.configuration;

import java.io.CharArrayReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

import javax.xml.XMLConstants;
import javax.xml.transform.Source;
import javax.xml.transform.sax.SAXSource;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.apache.log4j.Logger;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public abstract class XmlConfigurator<T> {

    private char [] xmlDoc;
    private final Logger logger = Logger.getLogger(this.getClass());
    
    public XmlConfigurator(char [] xmlDoc) {
        if (xmlDoc == null) {
            throw new NullPointerException("xmlDoc may not be null");
        }
        
        this.xmlDoc = xmlDoc;
    }
    
    public XmlConfigurator(InputStream xmlDoc) {
        if (xmlDoc == null) {
            throw new NullPointerException("xmlDoc may not be null");
        }
        
        setXmlDoc(xmlDoc);
    }
    
    public XmlConfigurator(String xmlDoc) {
        if (xmlDoc == null) {
            throw new NullPointerException("xmlDoc may not be null");
        }
        
        this.xmlDoc = xmlDoc.toCharArray();
    }
    
    private final Schema getSchema() throws SAXException {

        InputStream input;
        StreamSource source;
        SchemaFactory schemaFactory = SchemaFactory
                .newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        Schema schema = null;

        input = getXsdResource();

        if (input != null) {
            source = new StreamSource(input);
            schema = schemaFactory.newSchema(source);
        }

        return schema;
    }
    
    protected char [] getXmlDoc() {
        return xmlDoc;
    }

    protected abstract InputStream getXsdResource();

    public final List<T> parse() throws ConfigurationException {
        try {
            if (validateXml()) {
                return parseXmlDoc();
            } else {
                throw new ConfigurationException("Configuration is invalid.");
            }
        } catch (SAXException e) {
            throw new ConfigurationException(e);
        }
    }
    
    protected abstract List<T> parseXmlDoc() throws ConfigurationException;
    
    private final void setXmlDoc(InputStream is) {
        InputStreamReader xmlReader = new InputStreamReader(is);
        
        try {
            xmlDoc = new char[is.available()];
            xmlReader.read(xmlDoc);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }
    
    /**
     * Validates the supplied InputStream against the spectrum-sql.xsd schema.
     * 
     * Note that Validator.validate usage in this method closes the
     * InputStream, so it will need to be reopened after validating.
     * 
     * @param reader
     * @return
     */
    private final boolean validateXml() throws SAXException {

        boolean valid = false;
        Schema schema = getSchema();

        Validator validator = schema.newValidator();
        CharArrayReader reader = new CharArrayReader(getXmlDoc());
        InputSource is = new InputSource(reader);
        Source saxSource = new SAXSource(is);
        
        try {
            validator.validate(saxSource);
            valid = true;
        } catch (IOException e) {
            // valid = false
            logger.warn(e);
        } finally {
            reader.close();
        }

        return valid;
    }
}
