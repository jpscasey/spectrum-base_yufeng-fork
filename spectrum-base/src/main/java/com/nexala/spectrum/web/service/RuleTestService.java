
/*
 * Copyright (c) Nexala Technologies 2011, All rights reserved.
 */

package com.nexala.spectrum.web.service;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.nexala.spectrum.analysis.EventSource;
import com.nexala.spectrum.analysis.bean.GenericEvent;
/**
 * <p>
 * This web service provides functionality for testing rules in the context of a
 * spectrum implementation (i.e. lser, nxea, etc.) This service should be
 * extended (implementing the abstract methods) in the spectrum implementation
 * and the URL should be added to the <test-url> param in the
 * rule-package-config.xml (which is in the repository).
 * </p>
 * 
 * Currently, this service has two functions:
 * 
 * <ol>
 * <li>Providing Vehicle Information</li>
 * <li>Testing rules in the context of a Spectrum implementation</li>
 * </ol>
 * 
 * <p>
 * For option 1, you should pass the parameter "method" with the value
 * "vehicles". There are no other parameters required for this service. The
 * vehicle list is returned in CSV format, e.g. 
 * {@code vehicle1,vehicle2,vehicle3}. Mimetype: text/csv
 * </p>
 * 
 * <p>
 * For option 2, you should pass the parameter "method" with the value "test".
 * In addition to this, you will also need to specify the following parameters:
 * 
 * <ul>
 * <li>source - the DRL source to be tested</li>
 * <li>rcount - The number of rules being tested (long)</li>
 * <li>startTime - The time to retrieve data from (long)</li>
 * <li>endTime - The time to retrieve data to (long)</li>
 * <li>clientTz - The timezone of the client, in the format [+-][HHMM], e.g.
 * +0100. May be null
 * <li>vehicle - The vehicle (or unit) to retrieve data for. This vehicle code
 * should be one of those returns by the "vehicles" method</li>
 * <li>rlimit - This is used for calculating the threshold execution time for
 * the test. This is intended to represent a typical maximum rule number in the
 * repository</li>
 * </ul>
 * 
 * The result of this service is a HTML report. Mimetype: text/html
 * </p>
 * 
 * @see TestReport
 */
@Singleton
public class RuleTestService extends HttpServlet {

    /**
     * 
     */
    private static final long serialVersionUID = -3771558212495075850L;
    
    /** Vehicles parameter, */
    
    private static final String VEHICLES_METHOD = "vehicles";
    
    private static final String PKG_PARAM = "pkg";
    
    /**
     * RuleValidationProvider's are bound to a unique package configuration
     * name.
     */
    @Inject
    private Map<String, RuleTestDataProvider<GenericEvent>> providers;
    
    /**
     * See class description
     */
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException {
        
        String method = request.getParameter("method");
        
        if (method.equalsIgnoreCase(VEHICLES_METHOD)) {
            vehicleService(request, response);
        }
    }
    
    /**
     * Go away
     */
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) {
        response.setStatus(405); // method not allowed
        response.addHeader("Allow", "POST");
    }
    

    private void vehicleService(HttpServletRequest request,
            HttpServletResponse response) throws ServletException {

        String pkg = request.getParameter(PKG_PARAM);
        response.setContentType("text/csv");

        // Send the response.
        PrintWriter out = null;
        StringBuilder sb = new StringBuilder();
        
        for (EventSource target : providers.get(pkg).getTestTargets()) {
            sb.append(target.getSourceId());
            sb.append("|");
            sb.append(target.getSourceDesc());
            sb.append(",");
        }
        
        // Remove last character..
        String vehicleCsv = sb.toString().substring(0, sb.length() - 1);
        
        try {
            out = response.getWriter();
            out.print(vehicleCsv);
        } catch (IOException e) {
            throw new ServletException(e);
        } finally {
            if (out != null) {
                out.close();
            }
        }
    }

}