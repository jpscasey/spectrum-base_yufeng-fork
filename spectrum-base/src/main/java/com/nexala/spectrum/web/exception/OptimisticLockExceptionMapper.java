package com.nexala.spectrum.web.exception;

import javax.persistence.OptimisticLockException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;


/**
 * Exception mapper for {@link OptimisticLockException}.
 * Throw an error 409 (conflit) when this error is happening.
 * 
 * @author BBaudry
 *
 */
@Provider
public class OptimisticLockExceptionMapper implements ExceptionMapper<OptimisticLockException> {

    @Override
    public Response toResponse(OptimisticLockException exception) {
        return Response.status(Status.CONFLICT).entity(exception.getMessage()).build();
    }

}
