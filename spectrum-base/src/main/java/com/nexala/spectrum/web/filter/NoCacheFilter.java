
/*
 * Copyright (c) Nexala Technologies 2011, All rights reserved.
 */

package com.nexala.spectrum.web.filter;

import java.io.IOException;
import java.util.Date;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.inject.Singleton;

/**
 * This is a simple filter which prevents caching of resources.
 * it does this by adding the following headers to the responses
 * for requests which match the filter pattern AND an optional
 * url-regex (PCRE) pattern.
 * 
 * <code><pre>
 *  Date: [current time]
 *  Expires: [current time - 1 day]
 *  Pragma: no-cache
 *  Cache-control: no-cache, no-store, must-revalidate
 * </pre></code>
 * 
 * Sample config:
 * 
 * <code><pre>
 * &lt;filter&gt;
 *   &lt;filter-name&gt;NoCacheFilter&lt;/filter-name&gt;
 *   &lt;filter-class&gt;com.nexala.spectrum.web.filter.NoCacheFilter&lt;/filter-class&gt;
 *     &lt;init-param&gt;
 *       &lt;param-name&gt;url-regex&lt;/param-name&gt;
 *       &lt;param-value&gt;^.*\.nocache\.js$&lt;/param-value&gt;
 *     &lt;/init-param&gt;
 * &lt;/filter&gt;
 * 
 * &lt;filter-mapping&gt;
 *   &lt;filter-name&gt;NoCacheFilter&lt;/filter-name&gt;
 *   &lt;url-pattern&gt;/*&lt;/url-pattern&gt;
 * &lt;/filter-mapping&gt;
 * </code></pre>
 * 
 * @author poriordan
 */
@Singleton
public class NoCacheFilter implements Filter {

    private String urlRegex;
    
    public void destroy() {}

    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain filterChain) throws IOException, ServletException {

        long now = new Date().getTime();
        
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        
        if (urlRegex == null ||
                httpRequest.getRequestURI().matches(urlRegex)) {
            
            HttpServletResponse httpResponse = (HttpServletResponse) response;
            httpResponse.setDateHeader("Date", now);

            // Expired 1 day ago
            httpResponse.setDateHeader("Expires", now - 86400000L);
            httpResponse.setHeader("Pragma", "no-cache");
            httpResponse.setHeader("Cache-control",
                    "no-cache, no-store, must-revalidate");
        }

        filterChain.doFilter(request, response);
    }

    public void init(FilterConfig conf) throws ServletException {
        urlRegex = conf.getInitParameter("url-regex");
    }
}