
/*
 * Copyright (c) Nexala Technologies 2011, All rights reserved.
 */

package com.nexala.spectrum.web.service;

/**
 * This is an immutable class, which contains details about an
 * Object (i.e. a Unit/Vehicle/Formation/etc.) The class has been
 * called Stock rather than Object so that it is not confused w/
 * java.lang.Object.
 * 
 * @author poriordan
 */
public final class Stock {
    private Long objectId;
    private String objectCode;
    
    public Stock(Long objectId, String objectCode) {
        if (objectCode == null) {
            throw new NullPointerException("ObjectId may not be null");
        }
        
        this.objectId = objectId;
        this.objectCode = objectCode;
    }

    public Long getObjectId() {
        return objectId;
    }

    public String getObjectCode() {
        return objectCode;
    }
    
    /**
     * This method will always return a string value with the format
     * objectId|objectCode. Either value can be empty. A null value will
     * will not be added. E.g. "null|90001" will not occur.
     * 
     * ObjectId=null / ObjectCode=90001 : result = "|90001"
     * ObjectId=null / ObjectCode=null  : result = "|"
     * ObjectId=1234 / ObjectCode=90001 : result = "1234|90001"
     */
    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();
        
        if (objectId != null) {
            str.append(objectId);
        }
        
        str.append("|");
        
        if (objectCode != null) {
            str.append(objectCode);
        }
        
        return str.toString();
    }
}