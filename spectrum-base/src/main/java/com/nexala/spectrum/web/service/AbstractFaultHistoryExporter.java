package com.nexala.spectrum.web.service;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import au.com.bytecode.opencsv.CSVWriter;

import com.nexala.spectrum.faulthistory.FaultHistoryRowProvider;

/**
 * Generic fault history export (as CSV) servlet for Spectrum products.
 * Requires subclasses to implement methods to: provide a list of CSV column
 * header names, convert the request parameters into a map of required search
 * parameters, and take the search parameter map and return a search result in
 * the form of a list of objects that implement FaultHistoryRowProvider.
 * 
 * @see FaultHistoryRowProvider
 * @author amartin
 *
 */
public abstract class AbstractFaultHistoryExporter extends HttpServlet {
	private static final long serialVersionUID = 4628907475216363083L;

	/**
	 * Returns the parameter identified by <code>name</code> as a Boolean. If
	 * the parameter can't be found, null is returned.
	 * 
	 * @param request		The HTTP request to take the parameter from
	 * @param name			The parameter name.
	 * @return				The parameter as a Boolean, or null otherwise.
	 */
	protected static Boolean getBooleanParameter(HttpServletRequest request,
            String name) {
        String value = request.getParameter(name);

        if (value != null && !value.equals("null")) {
            return Boolean.parseBoolean(value);
        }

        return null;
    }

	/**
	 * Returns the parameter identified by <code>name</code> as a Long. If
	 * the parameter can't be found, null is returned.
	 * 
	 * @param request		The HTTP request to take the parameter from
	 * @param name			The parameter name.
	 * @return				The parameter as a Long, or null otherwise.
	 */
    protected static Long getLongParameter(HttpServletRequest request,
    		String name) {
        String value = request.getParameter(name);

        if (value != null && !value.equals("null")) {
            return Long.parseLong(value);
        }

        return null;
    }

	/**
	 * Returns the parameter identified by <code>name</code> as a String. If
	 * the parameter can't be found, null is returned.
	 * 
	 * @param request		The HTTP request to take the parameter from
	 * @param name			The parameter name.
	 * @return				The parameter as a String, or null otherwise.
	 */
    protected static String getStringParameter(HttpServletRequest request,
            String name) {
        String value = request.getParameter(name);

        if (value != null && !value.equals("null")) {
            return value;
        }

        return null;
    }
    
    public final void doGet(HttpServletRequest request, HttpServletResponse response) 
    	throws IOException {
        OutputStream outputStream = response.getOutputStream();

        response.setContentType("text/csv");
        response.addHeader("Content-disposition",
                "attachment; filename=faults-export.csv");

        PrintWriter writer = new PrintWriter(outputStream);
        
        
        HashMap<String, Object> requestParameters = getRequestParameters(request);
        List<FaultHistoryRowProvider> faultHistory = getFaultHistory(requestParameters);
        
        CSVWriter csvWriter = new CSVWriter(writer);

        // Add the column headings.
        csvWriter.writeNext(getOutputColumnHeaderNames());
        
        for(FaultHistoryRowProvider historyItem:faultHistory) {
        	String [] columns = historyItem.getFaultHistoryRowStrings();
        	csvWriter.writeNext(columns);
        }
        
        
        csvWriter.close();
        writer.close();
        outputStream.close();
    }

    @Override
    public final void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws IOException {
        doGet(req, resp);
    }
    
    /**
     * Returns an array of strings with column heading names for the CSV
     * output.
     * 
     * @return Column header names
     */
    public abstract String [] getOutputColumnHeaderNames();
    
    /**
     * Returns the fault history as a list of beans implementing the
     * FaultHistoryRowProvider interface.
     * 
     * @param values 	The request parameters required to generate the fault
     * 					history.
     * @return			A list of fault history entries.
     */
    public abstract List<FaultHistoryRowProvider> getFaultHistory(HashMap<String, Object> values);
    
    /**
     * Returns a map of HTTP request parameter values, as the types required
     * by getFaultHistory().
     * 
     * @param request		The HttpRequest to read parameters from.
     * @return
     */
    public abstract HashMap<String, Object> getRequestParameters(HttpServletRequest request);
}
