/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;

@Singleton
public class SpectrumProperties {
    private final Logger logger = Logger.getLogger(SpectrumProperties.class);
    
    private Properties properties = new Properties();

    @Inject
    public SpectrumProperties(@Named(Spectrum.SPECTRUM_PROPERTIES_PATH_PROP) String propertiesFile) {

        InputStream is = null;
        
        try {
            ClassLoader cl = Thread.currentThread().getContextClassLoader();
            is = cl.getResourceAsStream(propertiesFile);

            if (is == null) {
                logger.warn(String.format("Spectrum properties file not found (%s).", propertiesFile));
            } else {
                properties.load(is);
                logger.warn(String.format("Spectrum properties file loaded (%s).", propertiesFile));
            }
        } catch (IOException e) {
            logger.warn(String.format("Failed to load Spectrum properties file (%s).", propertiesFile), e);
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    logger.error("Can't close spectrum.properties", e);
                }
            }
        }
    }

    public String get(String name) {
        return new Properties(properties).getProperty(name);
    }

    public String get(String name, String defaultValue) {
        return new Properties(properties).getProperty(name, defaultValue);
    }

    /**
     * Return the property as a boolean. Return true if the property value is 'true' (not case sensitive), false
     * otherwise.
     * 
     * @param name the property name
     * @return true if the property value is 'true' (not case sensitive), false otherwise.
     */
    public Boolean getAsBoolean(String name) {
        String value = get(name);
        if (value != null) {
            value = value.toLowerCase();
            value = value.trim();
        }

        return "true".equals(value);
    }

    public Long getAsLong(String name) {
        String value = get(name);
        try {
            return new Long(value);
        } catch (NumberFormatException ex) {
            logger.warn(String.format("Invalid long value '%s' for property name '%s'", value, name));
            return null;
        }
    }
}
