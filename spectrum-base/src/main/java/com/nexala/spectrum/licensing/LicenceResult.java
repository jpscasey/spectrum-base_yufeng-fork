package com.nexala.spectrum.licensing;

public final class LicenceResult {
    private final boolean allowed;
    private final String message;

    public LicenceResult(boolean allowed, String message) {
        this.allowed = allowed;
        this.message = message;
    }

    public boolean isAllowed() {
        return allowed;
    }

    public String getMessage() {
        return message;
    }
}
