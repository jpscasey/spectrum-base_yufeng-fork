/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.licensing;

public class LicensableException extends Exception {
    private static final long serialVersionUID = -1713268457798327733L;

    public LicensableException(String msg) {
        super(msg);
    }
}