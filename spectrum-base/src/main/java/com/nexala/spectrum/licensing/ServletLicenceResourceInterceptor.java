/*
 * Copyright (c) Nexala Technologies 2011, All rights reserved.
 */

package com.nexala.spectrum.licensing;

import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.apache.log4j.Logger;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.nexala.spectrum.view.Constants;

public class ServletLicenceResourceInterceptor implements MethodInterceptor {
    private static final Logger LOGGER = Logger.getLogger(ServletLicenceResourceInterceptor.class);

    @Inject
    private ActivityLogProvider logProvider;

    @Inject
    private Provider<HttpServletRequest> requestProvider;

    @Inject
    private Provider<HttpServletResponse> responseProvider;

    public Object invoke(MethodInvocation invocation) throws Throwable {
        HttpServletRequest request = requestProvider.get();
        HttpServletResponse response = responseProvider.get();

        Licence licence = Licence.get(request, response, false);

        RestrictedResource resource = invocation.getMethod().getAnnotation(RestrictedResource.class);

        if (resource == null) {
            resource = invocation.getClass().getAnnotation(RestrictedResource.class);
        }

        // Because the annotation may only appear on a method or a class. At this stage
        // the resource must be non-null.
        String[] resourceKeys = resource.value();

        // The default/empty app is not licenced, all users have access to
        if (resourceKeys.length == 1 && Constants.DEFAULT_LIC_KEY.equalsIgnoreCase(resourceKeys[0])) {
            return invocation.proceed();
        } else {
            for (String key : resourceKeys) {
                if (licence.hasResourceAccess(key)) {
                    return invocation.proceed();
                }
            }

            logProvider.logActivity(Constants.RESOURCE_RESTRICTION_LOG_TYPE, "No resource access", request);

            LOGGER.warn("User " + licence.getLoginUser().getLogin() + " does not have access to "
                    + Arrays.deepToString(resourceKeys) + " in the application " + licence.getAppName());

            response.sendRedirect(Constants.DEFAULT_VIEW);

            // stops execution
            return null;
        }
    }
}
