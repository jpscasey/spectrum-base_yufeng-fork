package com.nexala.spectrum.licensing;

import java.util.List;

import com.google.inject.Singleton;

@Singleton
public class DataLicensor {
    @RestrictedData
    public <T> List<T> getLicenced(List<T> data) {
        return data;
    }
}
