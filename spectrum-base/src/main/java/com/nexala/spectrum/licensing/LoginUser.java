package com.nexala.spectrum.licensing;

public final class LoginUser {
    private final String id;
    private final String login;
    private final String name;
    private final String logoutUrl;
    private final String accountUrl;
    private final Boolean userAdmin;
    private final String realmManagementUrl;
    private final String locale;

    private LoginUser(String id, String login, String name, String logoutUrl, String accountUrl, Boolean userAdmin,
            String realmManagementUrl, String locale) {
        this.id = id;
        this.login = login;
        this.name = name;
        this.logoutUrl = logoutUrl;
        this.accountUrl = accountUrl;
        this.userAdmin = userAdmin;
        this.realmManagementUrl = realmManagementUrl;
        this.locale = locale;

    }

    public static LoginUser get(String id, String login, String name, String logoutUrl, String accountUrl,
            Boolean userAdmin, String realmManagementUrl, String locale) {
        return new LoginUser(id, login, name, logoutUrl, accountUrl, userAdmin, realmManagementUrl, locale);
    }

    public String getId() {
        return id;
    }

    public String getLogin() {
        return login;
    }

    public String getName() {
        return name;
    }

    public String getLogoutUrl() {
        return logoutUrl;
    }

    public String getAccountUrl() {
        return accountUrl;
    }

    public Boolean getUserAdmin() {
        return userAdmin;
    }

    public String getRealmManagementUrl() {
        return realmManagementUrl;
    }

    public String getLocale() {
        return locale;
    }
}
