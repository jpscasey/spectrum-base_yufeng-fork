/*
 * Copyright (c) Nexala Technologies 2013, All rights reserved.
 */
package com.nexala.spectrum.licensing;

public interface ScreenRequiresFeature {
    String getRequiredRole();
}
