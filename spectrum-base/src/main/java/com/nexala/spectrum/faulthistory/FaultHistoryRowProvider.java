package com.nexala.spectrum.faulthistory;

public interface FaultHistoryRowProvider {
	public String [] getFaultHistoryRowStrings();
}
