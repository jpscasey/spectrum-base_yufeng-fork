/*
 * Copyright (c) Nexala Technologies 2011, All rights reserved.
 */
package com.nexala.spectrum.mail;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

import org.apache.log4j.Logger;

import com.trimble.rail.security.client.HttpClient;

/**
 * A class that retrieves e-mail templates from the e-mail repository.
 * @author Severinas Monkevicius
 */
public class EmailProvider {
    private static final Logger logger = Logger.getLogger(EmailProvider.class);
    private static final String METHOD = "GET";

    private final HttpClient httpClient;
    private final String protocol;
    private final String hostname;
    private final int port;
    private final String path;
    private final String query;
    

    /**
     * Create a new e-mail provider with the specified URL components.
     * @param httpClient the http client
     * @param protocol the protocol used to retrieve the template, e.g.
     *        <code>http</code>.
     * @param hostname the hostname used to retrieve the template, e.g.
     *        <code>localhost</code>.
     * @param port the port used to retrieve the template, e.g. <code>80</code>.
     * @param path the base path used to retrieve the template, e.g.
     *        <code>/editor/emailTemplate</code>.
     * @param query the parameter prefix used to retrieve the template, e.g.
     *        <code>path=</code>.
     * 
     */
    public EmailProvider(HttpClient httpClient, String protocol, String hostname, int port,
            String path, String query) {
        this.httpClient = httpClient;
        this.protocol = protocol;
        this.hostname = hostname;
        this.port = port;
        this.path = path;
        this.query = query;
    }

    /**
     * Returns an e-mail template as an XML string from the e-mail repository.
     * @param path the path to the e-mail template.
     * @return an e-mail template as an XML string from the e-mail repository or
     *         <code>null</code> if the e-mail cannot be found.
     */
    public String loadEmailTemplate(String emailPath) {
        String template = null;

        URI uri = null;
        String q = query + emailPath;
        try {
            uri = new URI(protocol, null, hostname, port, path, q, null);
        } catch (URISyntaxException e) {
            logger.error(String.format("Invalid URI (%s %s %d %s %s)",
                    protocol, hostname, port, path, q));
        }

        URL url = null;
        try {
            url = new URL(uri.toASCIIString());
        } catch (MalformedURLException e) {
            logger.error(String.format("Invalid URL (%s)", uri.toString()));
            return null;
        }

        String lineSeparator = System.getProperty("line.separator");

        HttpURLConnection connection = null;
        try {
            connection = (HttpURLConnection) url.openConnection();

            connection.setRequestMethod(METHOD);
            httpClient.authenticate(connection);
            connection.connect();
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    connection.getInputStream()));
            StringBuilder response = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                response.append(line).append(lineSeparator);
            }

            template = response.toString();
        } catch (ProtocolException e) {
            logger.error(String.format("Invalid HTTP method (%s)", METHOD));
        } catch (IOException e) {
            logger.error(String.format("Failed to retrieve e-mail template "
                    + "from %s", url.toString()));
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }

        return template;
    }
}
