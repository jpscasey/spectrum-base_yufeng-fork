/*
 * Copyright (c) Nexala Technologies 2011, All rights reserved.
 */
package com.nexala.spectrum.mail;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.mail.Address;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.log4j.Logger;

/**
 * A client for sending e-mails.
 * @author Severinas Monkevicius
 */
public class EmailClient {
    private static final Logger logger = Logger.getLogger(EmailClient.class);

    /**
     * The mail session used to send e-mails.
     */
    private final Session session;

    /**
     * Create a new e-mail client with the specified properties.
     * @param properties the standard Java mail properties as defined by the
     *        JavaMail specification that should be used by this e-mail client.
     */
    public EmailClient(Properties properties) {
        this.session = Session.getInstance(properties);
    }
    
    public EmailClient(Properties properties, Authenticator authenticator) {
        this.session = Session.getInstance(properties, authenticator);
    }

    /**
     * Sends an e-mail.
     * @param subject the subject of the e-mail.
     * @param body the body of the e-mail.
     * @param recipients the addresses this e-mail should be sent to.
     * @return true if the email was sent, false otherwise
     */
    public boolean sendEmail(String from, String subject, String body,
            String... recipients) {
        boolean result = true;
        try {
            MimeMessage msg = new MimeMessage(session);
            msg.setFrom();

            Address fromAddress = addressFromRecipient(from);
            if (fromAddress == null) {
                logger.warn(String.format("Failed to create "
                        + "the return e-mail address from %s", from));
                return false;
            }
            msg.setFrom(fromAddress);

            Address[] addresses = addressesFromRecipients(recipients);
            if (addresses == null || addresses.length == 0) {
                logger.warn(String.format("No valid recipients for e-mail"));
                return false;
            }
            msg.setRecipients(Message.RecipientType.TO, addresses);

            msg.setSentDate(new Date());

            msg.setSubject(subject);
            msg.setText(body);

            Transport.send(msg);

            logger.info("email sent to : "+Arrays.deepToString(recipients));
            
        } catch (Exception e) {
            logger.error("Failed to send e-mail", e);
            result = false;
        }
        return result;
    }

    /**
     * Converts an e-mail address into an {@link Address} object.
     * @param recipient the e-mail address.
     * @return an {@link Address} object created from the specified e-mail
     *         address or </code>null</code> if the address cannot be converted.
     */
    private Address addressFromRecipient(String recipient) {
        try {
            return new InternetAddress(recipient);
        } catch (AddressException e) {
            logger.warn(String.format("Invalid e-mail address (%s)", recipient));
            return null;
        }
    }

    /**
     * Converts a list of e-mail addresses to a list of {@link Address} objects.
     * @param recipients the e-mail addresses.
     * @return a list of {@link Address} objects created from the specified list
     *         of e-mail addresses. Addresses that cannot be converted are not
     *         included.
     */
    private Address[] addressesFromRecipients(String... recipients) {
        if (recipients == null) {
            return new Address[0];
        }

        List<Address> addresses = new ArrayList<Address>(recipients.length);
        for (String recipient: recipients) {
            if (recipient != null) {
                Address address = addressFromRecipient(recipient);
                if (address != null) {
                    addresses.add(address);
                }
            }
        }

        Address[] addressArray = new Address[addresses.size()];
        return addresses.toArray(addressArray);
    }
}
