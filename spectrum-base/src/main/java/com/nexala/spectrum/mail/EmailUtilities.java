/*
 * Copyright (c) Nexala Technologies 2011, All rights reserved.
 */
package com.nexala.spectrum.mail;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.Node;
import org.stringtemplate.v4.ST;

import com.nexala.spectrum.mail.beans.Email;
import com.nexala.spectrum.mail.beans.Recipient;

/**
 * Utility methods for dealing with e-mails.
 * @author Severinas Monkevicius
 */
public class EmailUtilities {
    private static final Logger logger = Logger.getLogger(EmailUtilities.class);

    /**
     * Converts and XML e-mail template into an {@link Email} object.
     * @param xml the XML e-mail template to convert.
     * @return an {@link Email} object that represents the specified XML e-mail
     *         template.
     */
    public static Email emailFromXml(String xml) {
        Email email = new Email();

        Document document = null;
        try {
            document = DocumentHelper.parseText(xml);
        } catch (DocumentException e) {
            logger.error("Failed to parse e-mail template", e);
        }

        Element root = document.getRootElement();

        Node from = root.selectSingleNode("from");
        email.setFrom(from.getText());

        List<Recipient> recipients = new ArrayList<Recipient>();
        Node to = root.selectSingleNode("to");
        @SuppressWarnings("unchecked")
        List<Node> addresses = to.selectNodes("address");
        for (Node address: addresses) {
            Node type = address.selectSingleNode("@type");
            String typeStr = type.getText();
            Recipient recipient = new Recipient();
            if (typeStr.equalsIgnoreCase("user")) {
                recipient.setType(Recipient.Type.USER);
            } else if (typeStr.equalsIgnoreCase("role")) {
                recipient.setType(Recipient.Type.ROLE);
            } else {
                logger.error(String.format("Invalid recipient type (%s)",
                        typeStr));
            }
            recipient.setName(address.getText());
            recipients.add(recipient);
        }
        email.setRecipients(recipients);

        Node subject = root.selectSingleNode("subject");
        email.setSubject(subject.getText());

        Node body = root.selectSingleNode("body");
        email.setBody(body.getText());

        return email;
    }

    /**
     * Applies e-mail templating to a piece of text.
     * @param text the text to apply templating to.
     * @param variables a map of <variableName,variableValue> to be used to templating.
     * @return the text after templating is applied.
     */
    public static String applyTemplating(String text, Map<String, String> variables) {
        ST template = new ST(text, '$', '$');
        
        String renderedText = text;
        
        if (variables != null && variables.size() > 0) {
	        for (String varName : variables.keySet()) {
	        	String varValue = variables.get(varName);
	        	template.add(varName, (varValue != null ? varValue : ""));
	        }
	        
	        renderedText = template.render();
        } else {
        	logger.error("Invalid map of variables for email templating");
        }
        
        return renderedText;
    }
}
