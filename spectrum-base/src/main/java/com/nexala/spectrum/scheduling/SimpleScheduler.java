/*
 * Copyright (c) Nexala Technologies 2011, All rights reserved.
 */
package com.nexala.spectrum.scheduling;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Module;

public class SimpleScheduler implements SimpleSchedulerMBean {
    private final Logger logger = Logger.getLogger(SimpleScheduler.class);

    private String name;
    private String taskModuleClass;
    private long initialDelay;
    private long scheduleInterval;
    private boolean startAtStartup;
    private boolean started;
    private ScheduledExecutorService scheduler = null;
    private SimpleScheduledTask task = null;

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getTaskModuleClass() {
        return taskModuleClass;
    }

    @Override
    public void setTaskModuleClass(String taskModuleClass) {
        this.taskModuleClass = taskModuleClass;
    }

    @Override
    public long getInitialDelay() {
        return initialDelay;
    }

    @Override
    public void setInitialDelay(long initialDelay) {
        this.initialDelay = initialDelay;
    }

    @Override
    public long getScheduleInterval() {
        return scheduleInterval;
    }

    @Override
    public void setScheduleInterval(long scheduleInterval) {
        this.scheduleInterval = scheduleInterval;
    }

    @Override
    public boolean getStartAtStartup() {
        return startAtStartup;
    }

    @Override
    public void setStartAtStartup(boolean startAtStartup) {
        this.startAtStartup = startAtStartup;
    }

    @Override
    public boolean getStarted() {
        return started;
    }

    @Override
    public void start() throws Exception {
        if (getStartAtStartup()) {
            startSchedule();
        }
    }

    @Override
    public void stop() {
        stopSchedule();
    }

    @Override
    public synchronized void startSchedule() {
        if (started || scheduler != null || task != null) {
            return;
        }

        logger.info(String.format("Starting simple scheduler %s", name));

        task = newScheduledTask();

        if (task != null) {
            scheduler = Executors.newSingleThreadScheduledExecutor();
            scheduler.scheduleWithFixedDelay(new Runnable() {
                @Override
                public void run() {
                    try {
                        task.execute();
                    } catch (Exception ex) {
                        logger.error("Error running simple scheduler", ex);
                    }
                }
            }, getInitialDelay(), getScheduleInterval(), TimeUnit.MILLISECONDS);

            started = true;
        }
    }

    @Override
    public synchronized void stopSchedule() {
        if (!started || scheduler == null) {
            return;
        }

        logger.info(String.format("Stopping simple scheduler %s", name));

        scheduler.shutdown();

        scheduler = null;
        task = null;
        started = false;
    }

    @Override
    public void restartSchedule() throws SchedulerException {
        stopSchedule();
        startSchedule();
    }

    protected SimpleScheduledTask newScheduledTask() {
        try {
            Module module = resolveModule(getTaskModuleClass());
            final Injector injector = Guice.createInjector(module);
            SimpleTaskFactory factory = injector.getInstance(SimpleTaskFactory.class);
            SimpleScheduledTask task = factory.getTask();

            return task;
        } catch (Exception ex) {
            logger.error("Error creating simple scheduler task module", ex);
        }

        return null;
    }

    private Module resolveModule(String className) {
        Module module = null;

        if (className != null) {
            try {
                Class<?> clazz = Class.forName(className);
                Object object = clazz.newInstance();

                if (object instanceof Module) {
                    module = (Module) object;
                } else {
                    throw new IllegalArgumentException(className + " is " + "not an instance of "
                            + Module.class.getName());
                }
            } catch (ClassNotFoundException e) {
                logger.fatal("Class " + className + " not found");
            } catch (InstantiationException e) {
                logger.fatal(className + " is not default instantiable");
            } catch (IllegalAccessException e) {
                logger.fatal(className + " is not default instantiable");
            }
        } else {
            logger.warn("Module class name not specified");
        }

        return module;
    }
}
