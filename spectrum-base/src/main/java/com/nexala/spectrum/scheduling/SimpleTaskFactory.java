package com.nexala.spectrum.scheduling;

public interface SimpleTaskFactory {
    SimpleScheduledTask getTask();
}
