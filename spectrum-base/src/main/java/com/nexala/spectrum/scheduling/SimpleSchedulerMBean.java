/*
 * Copyright (c) Nexala Technologies 2011, All rights reserved.
 */
package com.nexala.spectrum.scheduling;

public interface SimpleSchedulerMBean {
    void start() throws Exception;

    void stop();

    void startSchedule() throws SchedulerException;

    void stopSchedule() throws SchedulerException;

    void restartSchedule() throws SchedulerException;

    String getName();

    void setName(String name);

    String getTaskModuleClass();

    void setTaskModuleClass(String taskModuleClass);

    long getInitialDelay();

    void setInitialDelay(long initialDelay);

    long getScheduleInterval();

    void setScheduleInterval(long scheduleInterval);

    boolean getStartAtStartup();

    void setStartAtStartup(boolean startAtStartup);

    boolean getStarted();
}
