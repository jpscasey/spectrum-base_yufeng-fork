/*
 * Copyright (c) Nexala Technologies 2011, All rights reserved.
 */
package com.nexala.spectrum.scheduling;

import javax.ejb.Remote;

/**
 * The managed bean interface for
 * {@link com.nexala.spectrum.scheduling.Scheduler}
 * 
 * @author Severinas Monkevicius
 * @see com.nexala.spectrum.scheduling.Scheduler
 */
@Remote
public interface SchedulerMBean {
    

    /**
     * Starts the scheduler.
     * 
     * @throws SchedulerException
     *             if the scheduler failed to start.
     */
    void startSchedule() throws SchedulerException;

    /**
     * Stops the scheduler.
     * 
     * @throws SchedulerException
     *             if the scheduler failed to stop.
     */
    void stopSchedule() throws SchedulerException;

    /**
     * Restarts the scheduler.
     * 
     * @throws SchedulerException
     *             if the scheduler failed to restart.
     */
    void restartSchedule() throws SchedulerException;

    /**
     * Returns the name of the scheduler.
     * 
     * @return the name of the scheduler.
     */
    String getName();

    /**
     * Sets the name of the scheduler.
     * 
     * @param name
     *            the name of the scheduler. It should correspond to the ID of
     *            the
     *            {@link com.nexala.spectrum.monitoring.MonitoringConfiguration}
     *            that will be used to configure the scheduler.
     */
    void setName(String name);

    /**
     * Returns the fully qualified class name of the task module that will be
     * used to instantiate the class that will be performed by the scheduler.
     * 
     * @return the fully qualified class name.
     */
    String getTaskModuleClass();

    /**
     * Sets the fully qualified class name of the task module that will be used
     * to instantiate the class that will be performed by the scheduler.
     * 
     * @param taskModuleClass
     *            the fully qualified class name.
     */
    void setTaskModuleClass(String taskModuleClass);

    /**
     * Returns the date that this scheduler should start running on.
     * 
     * @return the date that this scheduler should start running on.
     */
    long getInitialDelay();

    /**
     * Sets the date that this scheduler should start running on.
     * 
     * @param startDate
     *            the date that this scheduler should start running on.
     */
    void setInitialDelay(long startDate);

    /**
     * Returns the period of the scheduler.
     * 
     * @return the period of the scheduler in milliseconds.
     */
    long getSchedulePeriod();

    /**
     * Sets the period of the scheduler.
     * 
     * @param schedulePeriod
     *            the period of the scheduler in milliseconds.
     */
    void setSchedulePeriod(long schedulePeriod);

    /**
     * Returns whether the scheduler has been started.
     * 
     * @return <code>true</code> if the scheduler has been started.
     *         <code>false</code> otherwise.
     */
    boolean getStarted();
    
    /**
     * Returns whether the scheduler will be started as soon as it is created.
     * 
     * @return <code>true</code> if the scheduler will be started as soon as it
     *         is created, <code>false</code> otherwise.
     */
    boolean getStartAtStartup();

    /**
     * Sets whether the scheduler will be started as soon as it is created.
     * 
     * @param startAtStartup
     *            <code>true</code> if the scheduler will be started as soon as
     *            it is created. <code>false</code> otherwise.
     */
    void setStartAtStartup(boolean startAtStartup);

}
