package com.nexala.spectrum.scheduling;

public interface TaskFactory {
    ScheduledTask getTask(String name);
}
