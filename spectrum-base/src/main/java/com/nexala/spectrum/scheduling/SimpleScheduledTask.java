/*
 * Copyright (c) Nexala Technologies 2011, All rights reserved.
 */
package com.nexala.spectrum.scheduling;

public interface SimpleScheduledTask {
    void execute();
}
