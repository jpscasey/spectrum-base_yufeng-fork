/*
 * Copyright (c) Nexala Technologies 2011, All rights reserved.
 */
package com.nexala.spectrum.scheduling;

import java.util.Calendar;

import org.apache.log4j.Logger;

/**
 * A class that provides common functionality for scheduled tasks. Namely this
 * includes support for tracking the scheduled execution time in cases where the
 * execution time is greater than the scheduled period and the scheduler falls
 * behind schedule.
 * @author Severinas Monkevicius
 * @see com.nexala.spectrum.scheduling.Scheduler
 * @see com.nexala.spectrum.scheduling.ScheduledTask
 */
public abstract class DefaultScheduledTask implements ScheduledTask {
    protected final Logger logger = Logger.getLogger(this.getClass());

    /**
     * The scheduled execution time for the next execution. Set only when the
     * task is executed for the first time.
     */
    private Long timestamp;

    /**
     * The period at which the task is run in milliseconds.
     */
    private long schedulePeriod;

    /**
     * Point of execution for the scheduler. The scheduler will call this method
     * when it is time to execute the scheduled task. This method calls the
     * <code>run(long timestamp)</code> method to do the actual work and passes
     * it the time when the task was originally scheduled.
     */
    public final synchronized void run() {
        // Perform the scheduled task
        try {
            run(timestamp);
        } catch (Exception e) {
            logger.error("Failed to run scheduled task", e);
        }

        // Update the timestamp for when the task is executed next time
        timestamp = nextTimestamp();
    }

    /**
     * Calculates and returns the next timestamp as defined by the schedule
     * period.
     * @return the next timestamp as defined by the schedule period.
     */
    private long nextTimestamp() {
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(timestamp);
        adjustMillis(c, schedulePeriod);
        return c.getTimeInMillis();
    }

    /**
     * {@inheritDoc}
     */
    public long getTimestamp() {
        return timestamp;
    }

    /**
     * {@inheritDoc}
     */
    public final long getSchedulePeriod() {
        return schedulePeriod;
    }

    /**
     * {@inheritDoc}
     */
    public final void setSchedulePeriod(long schedulePeriod) {
        this.schedulePeriod = schedulePeriod;
    }

    /**
     * {@inheritDoc}
     */
    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    /**
     * Adds or subtracts some milliseconds from a calendar object. Instead of
     * using the milliseconds directly, the appropriate {@link Calendar
     * calendar} fields are used, meaning that instead of adding 5500
     * milliseconds, 5 seconds and 500 milliseconds will be added. This helps
     * when dealing with periods of 24 hours or longer and daylight saving time.
     * @param calendar the callendar to add the milliseconds to.
     * @param millis the number of milliseconds to add.
     */
    public static void adjustMillis(Calendar calendar, long millis) {
        long[] periods = { 86400000, 3600000, 60000, 1000, 1 };
        int[] fields = { Calendar.DAY_OF_MONTH, Calendar.HOUR_OF_DAY,
                Calendar.MINUTE, Calendar.SECOND, Calendar.MILLISECOND };

        int multiplier = 1;
        if (millis < 0) {
            multiplier = -1;
            millis = -millis;
        }
        for (int i = 0; i < periods.length; i++) {
            long period = periods[i];
            if (millis >= period) {
                long num = millis / period;
                calendar.add(fields[i], multiplier * (int) num);
                millis %= period;
            }
        }
    }
}

