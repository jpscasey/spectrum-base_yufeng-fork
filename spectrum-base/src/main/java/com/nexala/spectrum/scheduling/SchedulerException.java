/*
 * Copyright (c) Nexala Technologies 2011, All rights reserved.
 */
package com.nexala.spectrum.scheduling;

@SuppressWarnings("serial")
public class SchedulerException extends Exception {
    public SchedulerException() {}

    public SchedulerException(String message) {
        super(message);
    }

    public SchedulerException(Throwable cause) {
        super(cause);
    }

    public SchedulerException(String message, Throwable cause) {
        super(message, cause);
    }
}
