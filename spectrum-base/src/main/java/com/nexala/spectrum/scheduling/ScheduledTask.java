/*
 * Copyright (c) Nexala Technologies 2011, All rights reserved.
 */
package com.nexala.spectrum.scheduling;

/**
 * A task that is executed periodically based on a schedule.
 * @author Severinas Monkevicius
 * @see com.nexala.spectrum.scheduling.Scheduler
 * @see com.nexala.spectrum.scheduling.DefaultScheduledTask
 */
public interface ScheduledTask extends Runnable {
    /**
     * Executes the scheduled task.
     * @param timestamp the time this scheduled task was scheduled to execute
     *        on. If the task is executed on schedule, this will be very similar
     *        to the time of the actual execution.
     */
    void run(long timestamp);

    /**
     * Performs any cleanup operations. Called once when the scheduler is
     * stopped.
     */
    void destroy();

    /**
     * Returns the current timestamp according to the schedule.
     * @return the current timestamp according to the schedule.
     */
    long getTimestamp();

    /**
     * Sets the current timestamp according to the schedule.
     * @param startTime the current timestamp according to the schedule.
     */
    void setTimestamp(long startTime);

    /**
     * Returns the period at which this task is executed.
     * @return the period at which this task is executed in milliseconds.
     */
    long getSchedulePeriod();

    /**
     * Sets the period at which this task is executed.
     * @param schedulePeriod the period at which this task is executed in
     *        milliseconds.
     */
    void setSchedulePeriod(long schedulePeriod);
    
    /**
     * Returns the thread priority for the task. This should be one of the
     * following values: [Thread.MAX_PRIORITY, Thread.MIN_PRIORITY,
     * Thread.NORM_PRIORITY]. See {@linkplain Thread} for details
     * 
     * @return
     * @see Thread
     */
    int getThreadPriority();
}
