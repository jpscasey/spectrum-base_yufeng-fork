/*
 * Copyright (c) Nexala Technologies 2011, All rights reserved.
 */
package com.nexala.spectrum.scheduling;

import java.lang.management.ManagementFactory;
import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

import javax.management.MBeanServer;
import javax.management.ObjectName;

import org.apache.log4j.Logger;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Module;

/**
 * A simple scheduler that periodically performs a single scheduled task.
 * Modelled after the JBoss scheduler and thus follows the JBoss service
 * lifecycle, but without any actual dependencies.
 * 
 * @author Severinas Monkevicius
 */
public abstract class Scheduler implements SchedulerMBean {
    private static final Logger LOGGER = Logger.getLogger(Scheduler.class);

    private String name;
    private String taskModuleClass;
    private long initialDelay;
    private long schedulePeriod;
    private boolean startAtStartup;

    private Timer timer;
    private ScheduledTask task;
    private boolean started;
    
    private ObjectName objectName;


    public void start() throws Exception {
            
        this.objectName = new ObjectName("nexala:type=" + this.name);
        MBeanServer platformMBeanServer = ManagementFactory.getPlatformMBeanServer();
        platformMBeanServer.registerMBean(this, objectName);
        
        if (startAtStartup) {
            startSchedule();
        }
        
    }

    /**
     * {@inheritDoc}
     */
    public void stop() {
        
        try {
            ManagementFactory.getPlatformMBeanServer().unregisterMBean(this.objectName);
            stopSchedule();
        } catch (Exception e) {
            LOGGER.error(String.format("Failed to stop scheduler %s", name), e);
        }
    }

    /**
     * {@inheritDoc}
     */
    public synchronized void startSchedule() throws SchedulerException {
        if (started || task != null || timer != null) {
            return;
        }

        try {
            timer = new Timer(getName());

            task = newScheduledTask();

            long currentTime = System.currentTimeMillis();
            long delay = initialDelay;
            long firstRun = currentTime + delay;

            LOGGER.info(String.format("Scheduler %1$s will start at "
                    + "%2$tF %<tT,%<tL and will run every %3$dms", name,
                    firstRun, schedulePeriod));

            task.setTimestamp(firstRun);
            task.setSchedulePeriod(schedulePeriod);

            scheduleRun(delay);

            started = true;
        } catch (Exception e) {
            LOGGER.error(String.format("Failed to start scheduler %s", name), e);
            task = null;
            timer = null;
            started = false;
        }
    }

    private void scheduleRun(long delay) {
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                runTask();
            }
        }, Math.max(delay, 0));
    }

    private synchronized void runTask() {
        if (started && task != null) {
            Thread.currentThread().setPriority(task.getThreadPriority());
            
            try {
                task.run();
            } catch (Exception e) {
                LOGGER.warn("Scheduler timer thread threw exception", e);
            } finally {
                scheduleRun(task.getTimestamp() - System.currentTimeMillis());
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    public synchronized void stopSchedule() throws SchedulerException {
        if (!started || task == null || timer == null) {
            return;
        }

        LOGGER.info(String.format("Stopping scheduler %s", name));

        if (timer != null) {
            timer.cancel();
            timer = null;
        }

        task.destroy();
        task = null;

        started = false;
    }

    /**
     * {@inheritDoc} Exactly the same as stopping and starting the service.
     */
    public void restartSchedule() throws SchedulerException {
        stopSchedule();
        startSchedule();
    }

    /**
     * Creates a new instance of the scheduled task.
     * 
     * @return a new instance of the scheduled task.
     * @throws SchedulerException
     *             if the scheduled task cannot be instantiated.
     */
    protected ScheduledTask newScheduledTask() throws SchedulerException {
        Module module = resolveModule(getTaskModuleClass());
        final Injector injector = Guice.createInjector(module);
        TaskFactory factory = injector.getInstance(TaskFactory.class);
        ScheduledTask task = factory.getTask(getName());

        return task;
    }

    /**
     * Instantiates a new Module using the full qualified class name.
     * 
     * @return a new instance of the Module.
     * @param className
     *            the full qualified class name.
     */
    private Module resolveModule(String className) {
        Module module = null;

        if (className != null) {
            try {
                Class<?> clazz = Class.forName(className);
                Object object = clazz.newInstance();

                if (object instanceof Module) {
                    module = (Module) object;
                } else {
                    throw new IllegalArgumentException(className + " is "
                            + "not an instance of " + Module.class.getName());
                }
            } catch (ClassNotFoundException e) {
                LOGGER.fatal("Class " + className + " not found");
            } catch (InstantiationException e) {
                LOGGER.fatal(className + " is not default instantiable");
            } catch (IllegalAccessException e) {
                LOGGER.fatal(className + " is not default instantiable");
            }
        } else {
            LOGGER.warn("Module class name not specified");
        }

        return module;
    }

    /**
     * Returns the number of milliseconds to wait before running the scheduler
     * for the first time.
     * 
     * @param startTime
     *            the start time.
     * @param time
     *            the current time.
     * @param period
     *            the desired execution period.
     * @return the number of milliseconds to wait before running the scheduler
     *         for the first time. When startTime is greater than time, the
     *         difference between them is returned. If startTime is less than
     *         time, the delay is calculated by determining when the next
     *         execution should fall on based on the desired execution period.
     */
    private long getDelay(long startTime, long time, long period) {
        if (startTime >= time) {
            return startTime - time;
        }

        Calendar st = Calendar.getInstance();
        st.setTimeInMillis(startTime);

        long deltaTime = period - ((time - startTime) % period);

        // If the period is greater than 1 hour, we need to take daylight
        // savings into account and adjust the delay accordingly
        // FIXME can sometimes return crap when called around 12AM - 1AM
        if (period >= 360000) {
            Calendar dt = Calendar.getInstance();
            dt.setTimeInMillis(time + deltaTime);

            int stOff = st.get(Calendar.DST_OFFSET);
            int dtOff = dt.get(Calendar.DST_OFFSET);

            if (stOff != dtOff) {
                deltaTime += stOff - dtOff;
            }
        }

        return deltaTime;
    }

    /**
     * {@inheritDoc}
     */
    public String getName() {
        return name;
    }

    /**
     * {@inheritDoc}
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * {@inheritDoc}
     */
    public String getTaskModuleClass() {
        return taskModuleClass;
    }

    /**
     * {@inheritDoc} The class is required to have a constructor that takes a
     * single String argument as it is used to pass in the name of the
     * scheduler.
     */
    public void setTaskModuleClass(String taskModuleClass) {
        this.taskModuleClass = taskModuleClass;
    }

    /**
     * {@inheritDoc}
     */
    public long getInitialDelay() {
        return initialDelay;
    }

    /**
     * {@inheritDoc}
     */
    public void setInitialDelay(long initialDelay) {
        this.initialDelay = initialDelay;
    }

    /**
     * {@inheritDoc}
     */
    public long getSchedulePeriod() {
        return schedulePeriod;
    }

    /**
     * {@inheritDoc}
     */
    public void setSchedulePeriod(long schedulePeriod) {
        this.schedulePeriod = schedulePeriod;
    }


    /**
     * {@inheritDoc}
     */
    public boolean getStarted() {
        return started;
    }
    
    
    /**
     * @see com.nexala.spectrum.scheduling.SchedulerMBean#getStartAtStartup()
     */
    public boolean getStartAtStartup() {
        return startAtStartup;
    }

    
    /**
     * @see com.nexala.spectrum.scheduling.SchedulerMBean#setStartAtStartup(boolean)
     */
    public void setStartAtStartup(boolean startAtStartup) {
        this.startAtStartup = startAtStartup;
    }

}
