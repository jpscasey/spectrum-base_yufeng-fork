package com.nexala.spectrum.loader.parser;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.loader.LoaderException;
import com.nexala.spectrum.loader.algorithm.Algorithm;
import com.nexala.spectrum.loader.beans.DataFieldValue;
import com.nexala.spectrum.loader.beans.DataRecord;
import com.nexala.spectrum.loader.beans.FieldValue;
import com.nexala.spectrum.loader.beans.FileFieldDefinition;
import com.nexala.spectrum.loader.beans.InterfaceFileData;

public class BinaryDataParser extends DataParser {

    @Override
    public List<DataRecord> parse(InterfaceFileData iFileData) throws LoaderException {
        Map<Integer, DataFieldValue> dataFieldValueMap = new HashMap<Integer, DataFieldValue>();

        for (FileFieldDefinition field : iFileData.getFileType().getFileFields()) {
            if (field.getExistenceCondition() != null) {
                if (!checkCondition(field, field.getExistenceCondition(),
                        dataFieldValueMap.get(field.getExistenceCondition().getConditionFieldId()))) {

                    continue;
                }
            }

            Algorithm algorithm = algorithms.get(field.getProcessingAlgorithm());

            DataFieldValue dependence = null;
            if (field.getDependenceFieldID() != null) {
                dependence = dataFieldValueMap.get(field.getDependenceFieldID());
            }

            FieldValue<?> fieldValue = null;
            if (field.getSource().equalsIgnoreCase(Spectrum.SOURCE_FILE_NAME)) {
                fieldValue = getFieldValue(iFileData, field, algorithm, dependence);
            } else {
                Integer addressByte = field.getAddressByte();

                if (field.getAddressByteCondition() != null) {
                    if (checkCondition(field, field.getAddressByteCondition(),
                            dataFieldValueMap.get(field.getAddressByteCondition().getConditionFieldId()))) {

                        addressByte += field.getAddressByteFactor();
                    }
                }

                if (addressByte > iFileData.getData().length) {
                    throw new LoaderException(String.format("File field id "
                            + "%s has an invalid address byte %s in the file " + "data content, file data id %s",
                            field.getId(), addressByte, iFileData.getId()));
                }

                int bytesLength = 1;
                if (field.getAddressLength() % 8 == 0) {
                    bytesLength = field.getAddressLength() / 8;
                }

                byte[] byteArraySource = Arrays
                        .copyOfRange(iFileData.getData(), addressByte, addressByte + bytesLength);

                fieldValue = algorithm.getFieldValue(field, byteArraySource, dependence);
            }

            dataFieldValueMap.put(field.getId(), new DataFieldValue(field, fieldValue));
        }

        List<DataRecord> parsedValues = new ArrayList<DataRecord>();
        parsedValues.add(new DataRecord(new ArrayList<DataFieldValue>(dataFieldValueMap.values())));

        return parsedValues;
    }
}
