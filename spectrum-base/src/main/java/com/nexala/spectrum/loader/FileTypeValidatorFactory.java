/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.loader;

import com.nexala.spectrum.loader.beans.FileType;

public interface FileTypeValidatorFactory {
    FileTypeValidator create(FileType fileType);
}