/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.loader.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.configuration.ChannelConfiguration;
import com.nexala.spectrum.configuration.ConfigurationManager;
import com.nexala.spectrum.db.QueryManager;
import com.nexala.spectrum.db.beans.ChannelConfig;
import com.nexala.spectrum.db.dao.DataAccessException;
import com.nexala.spectrum.db.dao.GenericDao;
import com.nexala.spectrum.loader.beans.DataFieldValue;
import com.nexala.spectrum.loader.beans.DataRecord;
import com.nexala.spectrum.loader.beans.FieldValue;
import com.nexala.spectrum.loader.beans.FieldValueLong;
import com.nexala.spectrum.loader.beans.FileFieldDefinition;
import com.nexala.spectrum.loader.beans.FileType;
import com.nexala.spectrum.loader.beans.InterfaceFile;
import com.nexala.spectrum.loader.beans.InterfaceFileData;
import com.nexala.spectrum.loader.processor.DataProcessor;

@Singleton
public class DataFieldValueDao extends GenericDao<DataFieldValue, Long> {
    @Inject
    private QueryManager queryManager;

    @Inject
    private ChannelConfiguration channelConfiguration;

    @Inject
    private InterfaceFileDao iFileDao;

    @Inject
    private FileTypeDao fileTypeDao;

    @Inject
    private ConfigurationManager confManager;

    public void save(InterfaceFile iFile) throws DataAccessException {
        save(iFile, null);
    }

    public void save(InterfaceFile iFile, Integer insertBatchSize) throws DataAccessException {
        if (iFile == null) {
            throw new DataAccessException("Invalid interface file to save into the database");
        }
        
        Boolean useBatch = false;

        if (!iFile.getInterfaceFileDataList().isEmpty()) {
            FileType fileType = iFile.getInterfaceFileDataList().get(0).getFileType();
            if (fileType != null) {
                useBatch = fileType.isUseBatch();
            }
        }

        if (insertBatchSize != null && insertBatchSize > 0 && useBatch) {
            saveByBatch(iFile, insertBatchSize);
        } else {
            saveAll(iFile);
        }

    }

    private void saveAll(InterfaceFile iFile) {
        Connection conn = null;
        CallableStatement cs = null;

        try {
            conn = getConnection();
            conn.setAutoCommit(false);

            for (InterfaceFileData iFileData : iFile.getInterfaceFileDataList()) {
                DataProcessor dp = iFileData.getDataProcessor();

                for (DataRecord dataRecord : iFileData.getDataRecords()) {
                    loadLookup(conn, dataRecord);

                    cs = conn.prepareCall(dp.getSqlStatement(iFileData.getFileType().getStoredProcedureName(), dataRecord, iFileData.getFileName()));

                    cs.registerOutParameter(1, Types.INTEGER);
                    cs.execute();

                    int output = cs.getInt(1);
                    if (output < 0) {
                        throw new DataAccessException(String.format("Error saving data field values into the "
                                + "database for interface file data id %s using '%s' processor, error code %s",
                                iFileData.getId(), dp.getProcessor(), output));
                    }
                }

                iFileDao.changeStatus(iFileData.getId(), confManager.getConfAsString(Spectrum.LOADER_STATUS_SUCCESS));
            }

            iFileDao.changeStatus(iFile.getId(), confManager.getConfAsString(Spectrum.LOADER_STATUS_SUCCESS));

            conn.commit();
        } catch (Exception ex) {
            if (conn != null) {
                try {
                    conn.rollback();
                } catch (SQLException ex1) {
                    logger.error("Error rolling back database changes", ex1);
                }
            }

            throw new DataAccessException("Error saving file field values", ex);
        } finally {
            if (cs != null) {
                try {
                    cs.close();
                } catch (SQLException ex) {
                    logger.error("Error closing callable statement", ex);
                }
            }

            if (conn != null) {
                try {
                    conn.setAutoCommit(true);
                } catch (SQLException ex) {
                    logger.error("Error setting auto commit to true", ex);
                }

                try {
                    conn.close();
                } catch (SQLException ex) {
                    logger.error("Error closing connection", ex);
                }
            }
        }
    }

    private void saveByBatch(InterfaceFile iFile, Integer insertBatchSize) {

        Connection conn = null;
        Statement cs = null;

        try {
            conn = getConnection();

            cs = conn.createStatement();

            int batchSize = insertBatchSize;
            int batchCount = 0;

            for (InterfaceFileData iFileData : iFile.getInterfaceFileDataList()) {
                DataProcessor dp = iFileData.getDataProcessor();

                while (iFileData.getDataRecords().size() > 0) {
                    DataRecord dataRecord = iFileData.getDataRecords().remove(0);

                    loadLookup(conn, dataRecord);

                    cs.addBatch(dp.getSqlStatement(iFileData.getFileType().getStoredProcedureName(), dataRecord, iFileData.getFileName()));

                    batchCount++;
                    if (batchCount >= batchSize) {
                        cs.executeBatch();
                        batchCount = 0;
                    }

                }
                
                cs.executeBatch();
                batchCount = 0;

                iFileDao.changeStatus(iFileData.getId(), confManager.getConfAsString(Spectrum.LOADER_STATUS_SUCCESS));
            }

            iFileDao.changeStatus(iFile.getId(), confManager.getConfAsString(Spectrum.LOADER_STATUS_SUCCESS));

        } catch (Exception ex) {
            throw new DataAccessException("Error saving file field values", ex);
        } finally {
            if (cs != null) {
                try {
                    cs.close();
                } catch (SQLException ex) {
                    logger.error("Error closing callable statement", ex);
                }
            }

            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    logger.error("Error closing connection", ex);
                }
            }
        }
    }

    private void loadLookup(Connection conn, DataRecord dataRecord) throws DataAccessException {
        CallableStatement cs = null;

        try {
            cs = conn.prepareCall(queryManager.getQuery(Spectrum.LOAD_LOOKUP_QUERY));

            for (DataFieldValue fieldData : dataRecord.getDataFieldValues()) {
                FileFieldDefinition field = fieldData.getFileFieldDefinition();

                if (Boolean.TRUE.equals(field.getLookupInsert())) {
                    ChannelConfig config = channelConfiguration.getConfig(field.getChannelId());
                    FieldValue<?> fieldValue = fieldData.getFieldValue();

                    cs.setInt(1, config.getId());
                    cs.setString(2, fieldValue.getStringValue());
                    cs.registerOutParameter(3, Types.INTEGER);
                    cs.execute();

                    fieldData.setFieldValue(new FieldValueLong(Long.valueOf(cs.getInt(3))));
                }
            }
        } catch (Exception ex) {
            throw new DataAccessException("Error loading lookup values", ex);
        } finally {
            if (cs != null) {
                try {
                    cs.close();
                } catch (SQLException ex) {
                    logger.error("Error closing 'load lookup values' callable statement", ex);
                }
            }
        }
    }
}
