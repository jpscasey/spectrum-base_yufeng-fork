package com.nexala.spectrum.loader.beans;

import java.util.List;

public class DataRecord {
    private final List<DataFieldValue> dataFieldValues;

    public DataRecord(List<DataFieldValue> dataFieldValues) {
        this.dataFieldValues = dataFieldValues;
    }

    public List<DataFieldValue> getDataFieldValues() {
        return dataFieldValues;
    }
}
