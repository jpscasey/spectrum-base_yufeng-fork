package com.nexala.spectrum.loader.algorithm;

import com.google.inject.Singleton;
import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.loader.LoaderException;
import com.nexala.spectrum.loader.beans.DataFieldValue;
import com.nexala.spectrum.loader.beans.FieldValue;
import com.nexala.spectrum.loader.beans.FieldValueDouble;
import com.nexala.spectrum.loader.beans.FieldValueLong;
import com.nexala.spectrum.loader.beans.FileFieldDefinition;

@Singleton
public class NumberAlgorithm extends Algorithm {

    private final static String INVALID = "INVALID";

    @Override
    public FieldValue<?> getFieldValue(FileFieldDefinition fileFieldDefinition, String source, DataFieldValue dependence)
            throws LoaderException {

        source = source != null ? source.trim() : source;

        // Treat "Invalid" as "NaN"
        if (source != null && source.toUpperCase().equals(INVALID)) {
            source = "NaN";
        }

        int radix = 10;

        String pattern = fileFieldDefinition.getProcessingPattern();
        if (pattern != null) {
            pattern = pattern.trim();
            if (pattern.startsWith("base")) {
                radix = Integer.parseInt(pattern.split("\\s*\\(\\s*|\\s*\\)\\s*")[1], radix);
            }
        }

        Double scalingGain = fileFieldDefinition.getScalingGain();
        scalingGain = scalingGain == null ? 1 : scalingGain;

        Long l = null;
        Double d = null;

        try {
            // Check whether it could be long
            l = Long.parseLong(source, radix);
        } catch (NumberFormatException ex) {
        }
        ;

        try {
            // Check whether it could be decimal
            d = Double.parseDouble(source);
        } catch (NumberFormatException ex) {
        }
        ;

        try {
            if (l != null) {
                // Try proceed it as long
                return new FieldValueLong((long) (l * scalingGain));
            } else {
                // Try proceed it as decimal
                return new FieldValueDouble(d * scalingGain);
            }
        } catch (NumberFormatException ex) {
            throw new LoaderException(String.format("Error executing '%s', source '%s', file "
                    + "field definition id %s", this.getClass().getName(), source, fileFieldDefinition.getId()), ex);
        }
    }

    @Override
    public FieldValue<?> getFieldValue(FileFieldDefinition fileFieldDefinition, byte[] source, DataFieldValue dependence)
            throws LoaderException {

        String pattern = fileFieldDefinition.getProcessingPattern();

        if (pattern != null) {
            if (pattern.equalsIgnoreCase(Spectrum.PATTERN_REVERSE_BYTE)
                    || pattern.equalsIgnoreCase(Spectrum.PATTERN_REVERSE_BIT)) {

                try {
                    if (pattern.equals(Spectrum.PATTERN_REVERSE_BYTE)) {
                        source = getByteReversed(source);
                    } else if (pattern.equals(Spectrum.PATTERN_REVERSE_BIT)) {
                        source = getBitReversed(source);
                    }
                } catch (Exception ex) {
                    throw new LoaderException(String.format("Error executing '%s', source '%s', "
                            + "pattern '%s' file field definition id %s", this.getClass().getName(), source, pattern,
                            fileFieldDefinition.getId()), ex);
                }
            } else {
                throw new LoaderException(String.format("Error executing '%s', invalid pattern '%s' on file "
                        + "field definition id %s", this.getClass().getName(), pattern, fileFieldDefinition.getId()));
            }
        }

        Double scalingGain = fileFieldDefinition.getScalingGain();
        try {
            if (scalingGain == null) {
                return new FieldValueLong(getUnsignedLong(source));
            } else {
                if (scalingGain % 1 == 0) {
                    return new FieldValueLong((long) (getUnsignedLong(source) * scalingGain));
                } else {
                    return new FieldValueDouble(getUnsignedLong(source) * scalingGain);
                }
            }
        } catch (Exception ex) {
            throw new LoaderException(String.format("Error executing '%s', source '%s', scaling gain "
                    + "'%s', file field definition id %s", this.getClass().getName(), source, scalingGain,
                    fileFieldDefinition.getId()), ex);
        }
    }
}