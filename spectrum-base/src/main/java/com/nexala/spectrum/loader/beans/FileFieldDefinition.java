package com.nexala.spectrum.loader.beans;

public class FileFieldDefinition {
    private int id;
    private Integer channelId;
    private String fieldType;
    private Integer fieldPosition;
    private Integer addressByte;
    private FieldCondition addressByteCondition;
    private Integer addressByteFactor;
    private Integer addressBit;
    private Integer addressLength;
    private Double scalingGain;
    private Double scalingOffset;
    private Boolean scalingInversion;
    private String source;
    private String processingAlgorithm;
    private String processingPattern;
    private String identificationTag;
    private String note;
    private FieldCondition existenceCondition;
    private Boolean lookupInsert;
    private Integer dependenceFieldID;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFieldType() {
        return fieldType;
    }

    public void setFieldType(String fieldType) {
        this.fieldType = fieldType;
    }

    public Integer getFieldPosition() {
        return fieldPosition;
    }

    public void setFieldPosition(Integer fieldPosition) {
        this.fieldPosition = fieldPosition;
    }

    public Integer getAddressByte() {
        return addressByte;
    }

    public void setAddressByte(Integer addressByte) {
        this.addressByte = addressByte;
    }

    public Integer getAddressBit() {
        return addressBit;
    }

    public void setAddressBit(Integer addressBit) {
        this.addressBit = addressBit;
    }

    public Integer getAddressLength() {
        return addressLength;
    }

    public void setAddressLength(Integer addressLength) {
        this.addressLength = addressLength;
    }

    public Double getScalingGain() {
        return scalingGain;
    }

    public void setScalingGain(Double scalingGain) {
        this.scalingGain = scalingGain;
    }

    public Double getScalingOffset() {
        return scalingOffset;
    }

    public void setScalingOffset(Double scalingOffset) {
        this.scalingOffset = scalingOffset;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getProcessingAlgorithm() {
        return processingAlgorithm;
    }

    public void setProcessingAlgorithm(String processingAlgorithm) {
        this.processingAlgorithm = processingAlgorithm;
    }

    public String getIdentificationTag() {
        return identificationTag;
    }

    public void setIdentificationTag(String identificationTag) {
        this.identificationTag = identificationTag;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Integer getChannelId() {
        return channelId;
    }

    public void setChannelId(Integer channelId) {
        this.channelId = channelId;
    }

    public Boolean getScalingInversion() {
        return scalingInversion;
    }

    public void setScalingInversion(Boolean scalingInversion) {
        this.scalingInversion = scalingInversion;
    }

    public String getProcessingPattern() {
        return processingPattern;
    }

    public void setProcessingPattern(String processingPattern) {
        this.processingPattern = processingPattern;
    }

    public FieldCondition getExistenceCondition() {
        return existenceCondition;
    }

    public void setExistenceCondition(FieldCondition existenceCondition) {
        this.existenceCondition = existenceCondition;
    }

    public Boolean getLookupInsert() {
        return lookupInsert;
    }

    public void setLookupInsert(Boolean lookupInsert) {
        this.lookupInsert = lookupInsert;
    }

    public FieldCondition getAddressByteCondition() {
        return addressByteCondition;
    }

    public void setAddressByteCondition(FieldCondition addressByteCondition) {
        this.addressByteCondition = addressByteCondition;
    }

    public Integer getAddressByteFactor() {
        return addressByteFactor;
    }

    public void setAddressByteFactor(Integer addressByteFactor) {
        this.addressByteFactor = addressByteFactor;
    }

    public Integer getDependenceFieldID() {
        return dependenceFieldID;
    }

    public void setDependenceFieldID(Integer dependenceFieldID) {
        this.dependenceFieldID = dependenceFieldID;
    }
}
