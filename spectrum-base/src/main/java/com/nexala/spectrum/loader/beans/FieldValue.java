package com.nexala.spectrum.loader.beans;

public abstract class FieldValue<T> {
    public abstract T getValue();

    public abstract void setValue(T value);

    public String getStringValue() {
        if (getValue() != null) {
            return getValue().toString();
        } else {
            return null;
        }
    }
}
