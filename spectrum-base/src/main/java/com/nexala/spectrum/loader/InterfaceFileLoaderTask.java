package com.nexala.spectrum.loader;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.google.inject.Inject;
import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.configuration.ConfigurationManager;
import com.nexala.spectrum.db.dao.DataAccessException;
import com.nexala.spectrum.loader.beans.DataRecord;
import com.nexala.spectrum.loader.beans.InterfaceFile;
import com.nexala.spectrum.loader.beans.InterfaceFileData;
import com.nexala.spectrum.loader.dao.DataFieldValueDao;
import com.nexala.spectrum.loader.dao.InterfaceFileDao;
import com.nexala.spectrum.loader.parser.DataParser;
import com.nexala.spectrum.loader.processor.DataProcessor;
import com.nexala.spectrum.scheduling.SimpleScheduledTask;

public class InterfaceFileLoaderTask implements SimpleScheduledTask {
    private final Logger logger = Logger.getLogger(InterfaceFileLoaderTask.class);

    @Inject
    private ConfigurationManager confManager;

    @Inject
    private InterfaceFileDao iFileDao;

    @Inject
    private DataFieldValueDao dataFieldValueDao;

    @Inject
    private Map<String, DataParser> dataParsers;

    @Inject
    private FileTypeValidatorFactory fileTypeValidatorFactory;

    @Inject
    private Map<String, DataProcessor> dataProcessors;

    @Override
    public void execute() {
        int filesBatchSize = confManager.getConfAsInteger(Spectrum.LOADER_FILES_BATCH_SIZE);
        List<InterfaceFile> iFiles = iFileDao.findByStatus(filesBatchSize, confManager.getConfAsString(Spectrum.LOADER_STATUS_INITIAL));

        Integer insertBatchSize = confManager.getConfAsInteger(Spectrum.LOADER_INSERT_BATCH_SIZE);

        for (InterfaceFile iFile : iFiles) {
            Throwable error = null;
            try {
                loadFile(iFile);
                dataFieldValueDao.save(iFile, insertBatchSize);
                logger.info(String.format("Interface file id %s processed successfully", iFile.getId()));
            } catch (Exception ex) { // Any exception will result in a file being set to the Error status
                error = ex;
                logger.error(String.format("Error processing interface file id %s", iFile.getId()), ex);

                try {
                    iFileDao.changeStatus(iFile.getId(), confManager.getConfAsString(Spectrum.LOADER_STATUS_ERROR));
                } catch (DataAccessException ex1) {
                    logger.error(ex1);
                }
            } finally {
                try {
                    for (InterfaceFileData iFileData : iFile.getInterfaceFileDataList()) {
                        DataProcessor processor = iFileData.getDataProcessor();
                        if (processor != null)
                            processor.postProcess(iFileData, error);
                    }
                } catch (Throwable t) {
                    logger.error("Error during post processing", t);
                }
            }
        }

        if (iFiles.size() > 0) {
            logger.info(String.format("%s interface files processed", iFiles.size()));
        }
    }

    private void loadFile(InterfaceFile iFile) throws LoaderException {
        for (InterfaceFileData iFileData : iFile.getInterfaceFileDataList()) {
            FileTypeValidator fileTypeValidator = fileTypeValidatorFactory.create(iFileData.getFileType());
            if (!fileTypeValidator.validate()) {
                throw new LoaderException(String.format("Error processing interface file id %s.\n%s", iFile.getId(),
                        fileTypeValidator.getErrorMessage()));
            }

            DataProcessor dataProcessor = dataProcessors.get(iFileData.getFileType().getProcessor());
            iFileData.setDataProcessor(dataProcessor);
            dataProcessor.preProcess(iFileData);

            DataParser parser = dataParsers.get(iFileData.getFileType().getFileDataType());
            List<DataRecord> records = parser.parse(iFileData);
            iFileData.setDataRecords(records);

            dataProcessor.process(iFileData);
        }
    }
}
