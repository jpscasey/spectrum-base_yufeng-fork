package com.nexala.spectrum.loader;

import java.util.Map;

import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.loader.algorithm.Algorithm;
import com.nexala.spectrum.loader.beans.FileFieldDefinition;
import com.nexala.spectrum.loader.beans.FileType;
import com.nexala.spectrum.loader.parser.DataParser;
import com.nexala.spectrum.loader.processor.DataProcessor;

public class FileTypeValidator {
    @Inject
    private Map<String, Algorithm> algorithms;

    @Inject
    private Map<String, DataParser> parsers;

    @Inject
    private Map<String, DataProcessor> processors;

    private final FileType fileType;

    private String errorMessage = "";

    @Inject
    public FileTypeValidator(@Assisted FileType fileType) {
        this.fileType = fileType;
    }

    public String getErrorMessage() {
        return (errorMessage != null ? String.format("Invalid file type id %s.\n", fileType.getId()) + errorMessage
                : errorMessage);
    }

    public boolean validate() {
        int problemCount = 0;

        if (parsers.get(fileType.getFileDataType()) == null) {
            errorMessage += String.format("'File Data Type' value '%s' is not valid on file field id %s.\n",
                    fileType.getFileDataType(), fileType.getId());

            problemCount++;
        }

        if (processors.get(fileType.getProcessor()) == null) {
            errorMessage += String.format("'Processor' value '%s' is not valid on file field id %s.\n",
                    fileType.getProcessor(), fileType.getId());

            problemCount++;
        }

        if (fileType.getFileDataType().equals(Spectrum.DATA_TYPE_BINARY)) {
            if (fileType.getFieldSeparator() != null) {
                errorMessage += String.format("'Field Separator' value '%s' is not valid on file type id %s.\n",
                        fileType.getFieldSeparator(), fileType.getId());

                problemCount++;
            }

            if (fileType.getTextQualifier() != null) {
                errorMessage += String.format("'Text Qualifier' value '%s' is not valid on file type id %s.\n",
                        fileType.getTextQualifier(), fileType.getId());

                problemCount++;
            }

            if (fileType.getHeaderRowsToSkip() != 0) {
                errorMessage += String.format("'Header Rows To Skip' value '%s' is not valid on file type id %s.\n",
                        fileType.getHeaderRowsToSkip(), fileType.getId());

                problemCount++;
            }
        }

        if (fileType.getFileDataType().equals(Spectrum.DATA_TYPE_TEXT)) {
            if (fileType.getFieldSeparator() == null) {
                errorMessage += String.format("'Field Separator' value '%s' is not valid on file type id %s.\n",
                        fileType.getFieldSeparator(), fileType.getId());

                problemCount++;
            }

            if (fileType.getHeaderRowsToSkip() < 0) {
                errorMessage += String.format("'Header Rows To Skip' value '%s' is not valid on file type id %s.\n",
                        fileType.getHeaderRowsToSkip(), fileType.getId());

                problemCount++;
            }
        }

        if (fileType.getStoredProcedureName().isEmpty()) {
            errorMessage += String.format("'Stored Procedure Name' value '%s' is not valid on file type id %s.\n",
                    fileType.getStoredProcedureName(), fileType.getId());

            problemCount++;
        }

        for (FileFieldDefinition field : fileType.getFileFields()) {
            if (!validate(field)) {
                return false;
            }
        }

        if (problemCount == 0) {
            return true;
        }

        return false;
    }

    private boolean validate(FileFieldDefinition field) {
        int problemCount = 0;

        if (!field.getFieldType().equals(Spectrum.FIELD_TYPE_CHANNEL)
                && !field.getFieldType().equals(Spectrum.FIELD_TYPE_IDENTIFICATION)
                && !field.getFieldType().equals(Spectrum.FIELD_TYPE_VALIDATION)) {

            errorMessage += String.format("'Field Type' value '%s' is not valid on file field id %s.\n",
                    field.getFieldType(), field.getId());

            problemCount++;
        }

        if (field.getFieldType().equals(Spectrum.FIELD_TYPE_CHANNEL)) {
            if (field.getChannelId() == null) {
                errorMessage += String.format("'Channel ID' value '%s' is not valid on file field id %s.\n",
                        field.getChannelId(), field.getId());

                problemCount++;
            } else if (field.getIdentificationTag() != null) {
                errorMessage += String.format("'Identification Tag' value '%s' is not valid on file field id %s.\n",
                        field.getIdentificationTag(), field.getId());

                problemCount++;
            }
        }

        if (field.getFieldType().equals(Spectrum.FIELD_TYPE_IDENTIFICATION)
                || field.getFieldType().equals(Spectrum.FIELD_TYPE_VALIDATION)) {

            if (field.getChannelId() != null) {
                errorMessage += String.format("'Channel ID' value '%s' is not valid on file field id %s.\n",
                        field.getChannelId(), field.getId());

                problemCount++;
            } else if (field.getIdentificationTag() == null) {
                errorMessage += String.format("'Identification Tag' value '%s' is not valid on file field id %s.\n",
                        field.getIdentificationTag(), field.getId());

                problemCount++;
            }
        }

        if (!field.getSource().equals(Spectrum.SOURCE_FILE_DATA)
                && !field.getSource().equals(Spectrum.SOURCE_FILE_NAME)) {

            errorMessage += String.format("'Source' value '%s' is not valid on file field id %s.\n", field.getSource(),
                    field.getId());

            problemCount++;
        }

        if (algorithms.get(field.getProcessingAlgorithm()) == null) {
            errorMessage += String.format("'Processing Algorithm' value '%s' is not valid on file field id %s.\n",
                    field.getProcessingAlgorithm(), field.getId());

            problemCount++;
        }

        if (fileType.getFileDataType().equals(Spectrum.DATA_TYPE_TEXT)
                || field.getSource().equals(Spectrum.SOURCE_FILE_NAME)) {

            if (field.getFieldPosition() == null || field.getFieldPosition() < 0) {
                errorMessage += String.format("'Field Position' value '%s' is not valid on file field id %s.\n",
                        field.getFieldPosition(), field.getId());

                problemCount++;
            }
        } else if (field.getSource().equals(Spectrum.SOURCE_FILE_DATA)) {
            if (field.getAddressByte() == null || field.getAddressByte() < 0) {

                errorMessage += String.format("'Address Byte' value '%s' is not valid on file field id %s.\n",
                        field.getAddressByte(), field.getId());

                problemCount++;
            }

            if (field.getAddressLength() == null || field.getAddressLength() < 1
                    || (field.getAddressLength() % 8 != 0 && field.getAddressLength() != 1)) {

                errorMessage += String.format("'Address Length' value '%s' is not valid on file field id %s.\n",
                        field.getAddressLength(), field.getId());

                problemCount++;
            }

            if (field.getProcessingAlgorithm().equals(Spectrum.ALGORITHM_BIT)) {
                if (field.getAddressBit() == null || field.getAddressBit() < 0 || field.getAddressBit() > 7) {

                    errorMessage += String.format("'Address Bit' value '%s' is not valid on file field id %s.\n",
                            field.getAddressBit(), field.getId());

                    problemCount++;
                }
            } else if (field.getAddressLength() % 8 != 0) {
                errorMessage += String.format("'Address length' value '%s' is not valid on file field id %s.\n",
                        field.getAddressLength(), field.getId());

                problemCount++;
            }
        }

        if (field.getExistenceCondition() != null) {
            int conditionCheck = 0;

            if (field.getExistenceCondition().getConditionFieldId() != null) {
                conditionCheck++;
            }
            if (field.getExistenceCondition().getConditionOperator() != null) {
                conditionCheck++;
            }
            if (field.getExistenceCondition().getConditionValue() != null) {
                conditionCheck++;
            }

            if (conditionCheck > 0 && conditionCheck != 3) {
                errorMessage += String.format(
                        "The combination of 'Condition Field ID' value '%s', 'Condition Operator' "
                                + "value '%s' and 'Condition Value' value '%s' is not valid on file field id %s.\n",
                        field.getExistenceCondition().getConditionFieldId(), field.getExistenceCondition()
                                .getConditionOperator(), field.getExistenceCondition().getConditionValue(), field
                                .getId());

                problemCount++;
            }

            if (field.getExistenceCondition().getConditionOperator() != null) {
                try {
                    Comparison.valueOf(field.getExistenceCondition().getConditionOperator());
                } catch (Exception e) {
                    errorMessage += String.format("'Condition Operator' value '%s' is not valid on file field id %s",
                            field.getExistenceCondition().getConditionOperator(), field.getId());

                    problemCount++;
                }
            }
        }

        if (field.getAddressByteCondition() != null) {
            int conditionCheck = 0;

            if (field.getAddressByteCondition().getConditionFieldId() != null) {
                conditionCheck++;
            }
            if (field.getAddressByteCondition().getConditionOperator() != null) {
                conditionCheck++;
            }
            if (field.getAddressByteCondition().getConditionValue() != null) {
                conditionCheck++;
            }
            if (field.getAddressByteFactor() != null) {
                conditionCheck++;
            }

            if (conditionCheck > 0 && conditionCheck != 4) {
                errorMessage += String
                        .format("The combination of 'Address Byte Condition Field ID' value '%s', "
                                + "'Address Byte Condition Operator' value '%s', 'Address Byte Condition Value' value '%s' and "
                                + "'Address Byte Factor' value '%s' is not valid on file field id %s.\n", field
                                .getAddressByteCondition().getConditionFieldId(), field.getAddressByteCondition()
                                .getConditionOperator(), field.getAddressByteCondition().getConditionValue(), field
                                .getAddressByteFactor(), field.getId());

                problemCount++;
            }

            if (field.getAddressByteCondition().getConditionOperator() != null) {
                try {
                    Comparison.valueOf(field.getAddressByteCondition().getConditionOperator());
                } catch (Exception e) {
                    errorMessage += String.format("'Address Byte Condition Operator' value '%s' is not "
                            + "valid on file field id %s", field.getAddressByteCondition().getConditionOperator(),
                            field.getId());

                    problemCount++;
                }
            }
        }

        if (problemCount == 0) {
            return true;
        }

        return false;
    }
}
