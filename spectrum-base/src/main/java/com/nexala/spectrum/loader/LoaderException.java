/*
 * Copyright (c) Nexala Technologies 2010, All rights reserved.
 */
package com.nexala.spectrum.loader;

public class LoaderException extends Exception {
    private static final long serialVersionUID = 3866249459167025556L;

    public LoaderException(String s) {
        super(s);
    }

    public LoaderException(Throwable t) {
        super(t);
    }

    public LoaderException(String msg, Throwable t) {
        super(msg, t);
    }
}