package com.nexala.spectrum.loader.beans;

import java.util.List;

import com.nexala.spectrum.loader.processor.DataProcessor;

public class InterfaceFileData {
    private long id;
    private FileType fileType;
    private String fileName;
    private long timestamp;
    private String status;
    private byte[] data;
    private List<DataRecord> dataRecords;
    private DataProcessor dataProcessor;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public FileType getFileType() {
        return fileType;
    }

    public void setFileType(FileType fileType) {
        this.fileType = fileType;
    }

    public String getFileName() {
        return fileName;
    }

    public String getShortFileName() {
        return fileName.substring(0, fileName.lastIndexOf("."));
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }

    public List<DataRecord> getDataRecords() {
        return dataRecords;
    }

    public void setDataRecords(List<DataRecord> dataRecords) {
        this.dataRecords = dataRecords;
    }

    public DataProcessor getDataProcessor() {
        return dataProcessor;
    }

    public void setDataProcessor(DataProcessor dataProcessor) {
        this.dataProcessor = dataProcessor;
    }
}
