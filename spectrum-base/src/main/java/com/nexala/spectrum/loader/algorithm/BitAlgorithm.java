package com.nexala.spectrum.loader.algorithm;

import java.nio.ByteBuffer;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.nexala.spectrum.loader.LoaderException;
import com.nexala.spectrum.loader.beans.DataFieldValue;
import com.nexala.spectrum.loader.beans.FieldValue;
import com.nexala.spectrum.loader.beans.FieldValueBoolean;
import com.nexala.spectrum.loader.beans.FileFieldDefinition;

@Singleton
public class BitAlgorithm extends Algorithm {

    @Inject
    private NumberAlgorithm numAlgo;

    @Override
    public FieldValueBoolean getFieldValue(FileFieldDefinition fileFieldDefinition, String source,
            DataFieldValue dependence) throws LoaderException {

        FieldValue<?> fv = numAlgo.getFieldValue(fileFieldDefinition, source, dependence);
        Long numValue = (Long) fv.getValue();
        byte[] longAsByteArr = ByteBuffer.allocate(8).putLong(numValue).array();

        return getFieldValue(fileFieldDefinition, longAsByteArr, dependence);
    }

    @Override
    public FieldValueBoolean getFieldValue(FileFieldDefinition fileFieldDefinition, byte[] source,
            DataFieldValue dependence) throws LoaderException {

        Integer addressBit = fileFieldDefinition.getAddressBit();

        if (addressBit == null) {
            throw new LoaderException(String.format("Error executing '%s', invalid address bit "
                    + "'%s' on file field definition id %s", this.getClass().getName(), addressBit,
                    fileFieldDefinition.getId()));
        }

        try {
            return new FieldValueBoolean(getBit(source, addressBit));
        } catch (Exception ex) {
            throw new LoaderException(String.format(
                    "Error executing '%s', source '%s', address bit '%s', file field definition id %s", this.getClass()
                            .getName(), source, addressBit, fileFieldDefinition.getId()), ex);
        }
    }
}