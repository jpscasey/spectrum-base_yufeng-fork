/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.loader.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.google.inject.Inject;
import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.db.JdbcRowMapper;
import com.nexala.spectrum.db.QueryManager;
import com.nexala.spectrum.db.dao.BoxedResultSet;
import com.nexala.spectrum.db.dao.BoxedResultSetFactory;
import com.nexala.spectrum.db.dao.DataAccessException;
import com.nexala.spectrum.db.dao.GenericDao;
import com.nexala.spectrum.loader.beans.FieldCondition;
import com.nexala.spectrum.loader.beans.FileFieldDefinition;
import com.nexala.spectrum.loader.beans.FileType;

public class FileFieldDefinitionDao extends GenericDao<FileFieldDefinition, Long> {
    @Inject
    private FieldFieldDefinitionMapper mapper;

    @Inject
    private QueryManager queryManager;

    private static final class FieldFieldDefinitionMapper extends JdbcRowMapper<FileFieldDefinition> {
        @Inject
        private BoxedResultSetFactory boxedRSFactory;

        @Override
        public FileFieldDefinition createRow(ResultSet results) throws SQLException {
            BoxedResultSet rs = boxedRSFactory.create(results);

            FileFieldDefinition field = new FileFieldDefinition();
            field.setId(rs.getInt(Spectrum.ID));
            field.setChannelId(rs.getInt(Spectrum.CHANNEL_ID));
            field.setFieldType(rs.getString(Spectrum.FIELD_TYPE));
            field.setFieldPosition(rs.getInt(Spectrum.FIELD_POSITION));
            field.setAddressByte(rs.getInt(Spectrum.ADDRESS_BYTE));
            field.setAddressByteFactor(rs.getInt(Spectrum.ADDRESS_BYTE_FACTOR));
            field.setAddressBit(rs.getInt(Spectrum.ADDRESS_BIT));
            field.setAddressLength(rs.getInt(Spectrum.ADDRESS_LENGTH));
            field.setScalingGain(rs.getDouble(Spectrum.SCALING_GAIN));
            field.setScalingOffset(rs.getDouble(Spectrum.SCALING_OFFSET));
            field.setScalingInversion(rs.getBoolean(Spectrum.SCALING_INVERSION));
            field.setSource(rs.getString(Spectrum.SOURCE));
            field.setProcessingAlgorithm(rs.getString(Spectrum.PROCESSING_ALGORITHM));
            field.setProcessingPattern(rs.getString(Spectrum.PROCESSING_PATTERN));
            field.setIdentificationTag(rs.getString(Spectrum.IDENTIFICATION_TAG));
            field.setNote(rs.getString(Spectrum.NOTE));
            field.setLookupInsert(rs.getBoolean(Spectrum.LOOKUP_INSERT));

            field.setExistenceCondition(getFieldCondition(rs.getInt(Spectrum.CONDITION_FIELD_ID),
                    rs.getString(Spectrum.CONDITION_OPERATOR), rs.getInt(Spectrum.CONDITION_VALUE)));

            field.setAddressByteCondition(getFieldCondition(rs.getInt(Spectrum.ADDRESS_BYTE_CONDITION_FIELD_ID),
                    rs.getString(Spectrum.ADDRESS_BYTE_CONDITION_OPERATOR),
                    rs.getInt(Spectrum.ADDRESS_BYTE_CONDITION_VALUE)));

            field.setDependenceFieldID(rs.getInt(Spectrum.DEPENDENCE_FIELD_ID));

            return field;
        }

        private FieldCondition getFieldCondition(Integer conditionFieldId, String conditionOperator,
                Integer conditionValue) {

            if (conditionFieldId != null || conditionOperator != null || conditionValue != null) {
                return new FieldCondition(conditionFieldId, conditionOperator, conditionValue);
            }

            return null;
        }
    };

    public List<FileFieldDefinition> findByFileType(FileType fileType) throws DataAccessException {
        String query = queryManager.getQuery(Spectrum.FILE_FIELD_DEFINITION_BY_FILE_TYPE_ID_QUERY);
        return findByQuery(query, mapper, fileType.getId());
    }
}
