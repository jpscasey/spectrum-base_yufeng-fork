package com.nexala.spectrum.loader.beans;

public class FieldCondition {
    private final Integer conditionFieldId;
    private final String conditionOperator;
    private final Integer conditionValue;

    public FieldCondition(Integer conditionFieldId, String conditionOperator, Integer conditionValue) {
        this.conditionFieldId = conditionFieldId;
        this.conditionOperator = conditionOperator;
        this.conditionValue = conditionValue;
    }

    public Integer getConditionFieldId() {
        return conditionFieldId;
    }

    public String getConditionOperator() {
        return conditionOperator;
    }

    public Integer getConditionValue() {
        return conditionValue;
    }
}
