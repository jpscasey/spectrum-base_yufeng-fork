package com.nexala.spectrum.loader.algorithm;

import java.math.BigDecimal;

import org.apache.log4j.Logger;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.nexala.spectrum.loader.LoaderException;
import com.nexala.spectrum.loader.beans.DataFieldValue;
import com.nexala.spectrum.loader.beans.FieldValueLong;
import com.nexala.spectrum.loader.beans.FileFieldDefinition;
import com.nexala.spectrum.utils.DateUtils;

/**
 * Algorithm for converting .Net DateTime.Ticks in UTC format to java timestamp.
 * 
 * 
 * <br>
 * <b>Notice</b> Class can be extended to work with non UTC ticks but be aware that ticks have different values in different timezones ( contrary to java timestamp ).
 * 
 * @see http://msdn.microsoft.com/en-us/library/system.datetime.ticks(v=vs.110).aspx
 * @author jana
 * 
 */
@Singleton
public class DotNetTicksAlgorithm extends Algorithm {
    @Inject
    DateUtils dateUtils;

    protected final Logger logger = Logger.getLogger(this.getClass());

    /**
     * Ticks elapsed from the DataTime.Ticks epoch which begins January 1, 0001 till the Java epoch beginning January 1, 1970, 00:00:00 GMT expressed in Ticks unit ( that is 100 hundred of
     * nanoseconds) .
     * 
     * <p>
     * Constant reproducible in C# by
     * 
     * <pre>
     * DateTime javaEpoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
     * Console.WriteLine(&quot;Ticks of Java Epoch (TICKS_AT_JAVA_EPOCH) = {0}&quot;, javaEpoch.Ticks);
     * </pre>
     * 
     * </p>
     */
    private static final long TICKS_OF_JAVA_EPOCH = 621355968000000000L;
    /**
     * 1 Tick = 100 nano seconds = 100*10^-9 ns = 10^-7 ns = 10^-4 ms => 1 ms = 10^4 Ticks.
     */
    private static final long TICKS_PER_MILLISECOND = 10000;

    @Override
    public FieldValueLong getFieldValue(FileFieldDefinition fileFieldDefinition, String source,
            DataFieldValue dependence) throws LoaderException {

        source = source != null ? source.trim() : source;
        if (source == null || source.isEmpty()) {
            return new FieldValueLong(null);
        }
        try {
            return new FieldValueLong(getJavaMillisFromTicks(source));
        } catch (Exception e) {
            String msg = String.format("Error processing %s, invalid source '%s' on field definition id %s."
                    + " Cann't convert source to Java Date.", this.getClass().getName(), source,
                    fileFieldDefinition.getId());
            logger.error(msg);
            throw new LoaderException(msg, e);
        }
    }

    /**
     * Parse String representation of .Net DateTime.Ticks in UTC.
     * 
     * .Net Ticks has .Net long data type. <b>.Net long</b> is signed 64-bit integer, so we can store it in Java long. Ticks are strictly positive numbers. They with L or l but this is similar to java
     * or in scientific notation ( as was seen in sample data files from SWT ).
     * 
     * @param ticksStr String representation of .Net DateTime.Ticks in UTC (e.g. 621355968000000000L for Java Epoch)
     * @return Returns the number representation of String object.
     * @throws LoaderException on parse error
     */
    public long parseTicks(final String ticksValue) throws LoaderException {
        long ticks = 0;
        String ticksStr = ticksValue;
        // First check whether it is Long, because BigDecimal fails with
        // detection long data type with suffix like l or L
        try {
            // First remove ending 'L' or 'l' as parseLong will not parse it
            if (ticksStr.endsWith("L") || ticksStr.endsWith("l")) {
                ticksStr = ticksStr.substring(0, ticksStr.length() - 1);
            }
            ticks = Long.parseLong(ticksStr);
        } catch (NumberFormatException e) {
            // ok it is not java long
            // Lets try whether it is not long in scientific notation and
            // without decimal part..
            try {
                BigDecimal ticksNum = new BigDecimal(ticksStr);
                ticks = ticksNum.longValueExact();
            } catch (ArithmeticException ae) {
                throwLoaderException(ticksStr, ae);
            }
        }

        // Check that number is positive
        if (ticks <= 0) {
            throwLoaderException(ticksStr, new NumberFormatException("Negative ticks."));
        }
        return ticks;
    }

    /**
     * Wrap exception into {@link LoaderException}.
     * 
     * @param ticksStr Error message
     * @param e Original exception
     * @throws LoaderException Wrapped original exception
     */
    private void throwLoaderException(String ticksStr, RuntimeException e) throws LoaderException {
        String msg = String.format("Error processing %s, invalid ticks '%s'.", this.getClass().getName(), ticksStr);
        logger.error(msg, e);
        throw new LoaderException(msg, e);
    }

    /**
     * Convert Ticks to java milliseconds.
     * 
     * @param ticksStr String representation of .Net DateTime.Ticks in UTC (e.g. 621355968000000000L for Java Epoch)
     * @return Returns the number of milliseconds since January 1, 1970, 00:00:00 GMT represented by this String object.
     * @throws LoaderException if error during parsing
     */
    public long getJavaMillisFromTicks(String ticksStr) throws LoaderException {
        Long ticks = parseTicks(ticksStr);
        return (ticks - TICKS_OF_JAVA_EPOCH) / TICKS_PER_MILLISECOND;
    }

}
