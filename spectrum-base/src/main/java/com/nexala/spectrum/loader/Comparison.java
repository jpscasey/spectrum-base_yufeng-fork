package com.nexala.spectrum.loader;

interface Compare {
    boolean compare(int i, int j);
}

public enum Comparison implements Compare {
    EQ {
        @Override
        public boolean compare(int value, int conditionValue) {
            return (value == conditionValue);
        }
    },

    NE {
        @Override
        public boolean compare(int value, int conditionValue) {
            return (value != conditionValue);
        }
    },

    LT {
        @Override
        public boolean compare(int value, int conditionValue) {
            return (value < conditionValue);
        }
    },

    LE {
        @Override
        public boolean compare(int value, int conditionValue) {
            return (value <= conditionValue);
        }
    },

    GE {
        @Override
        public boolean compare(int value, int conditionValue) {
            return (value >= conditionValue);
        }
    },

    GT {
        @Override
        public boolean compare(int value, int conditionValue) {
            return (value > conditionValue);
        }
    }
}
