package com.nexala.spectrum.loader.parser;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.google.inject.Inject;
import com.nexala.spectrum.loader.Comparison;
import com.nexala.spectrum.loader.LoaderException;
import com.nexala.spectrum.loader.algorithm.Algorithm;
import com.nexala.spectrum.loader.beans.DataFieldValue;
import com.nexala.spectrum.loader.beans.DataRecord;
import com.nexala.spectrum.loader.beans.FieldCondition;
import com.nexala.spectrum.loader.beans.FieldValue;
import com.nexala.spectrum.loader.beans.FileFieldDefinition;
import com.nexala.spectrum.loader.beans.InterfaceFileData;

public abstract class DataParser {
    protected final Logger logger = Logger.getLogger(this.getClass());

    @Inject
    protected Map<String, Algorithm> algorithms;

    public abstract List<DataRecord> parse(InterfaceFileData iFileData) throws LoaderException;

    protected String[] getFileNameFields(InterfaceFileData iFileData) {
        if (iFileData.getShortFileName() != null && iFileData.getFileType().getFileNameFieldSeparator() != null) {
            return iFileData.getShortFileName().split(iFileData.getFileType().getFileNameFieldSeparator());
        }

        return null;
    }

    protected boolean checkCondition(FileFieldDefinition field, FieldCondition fieldCondition,
            DataFieldValue conditionDataFieldValue) throws LoaderException {

        if (field == null) {
            throw new LoaderException("Invalid file field definition");
        }

        if (fieldCondition == null) {
            throw new LoaderException(String.format("Invalid field definition for file field id %s", field.getId()));
        }

        if (conditionDataFieldValue == null) {
            throw new LoaderException(String.format(
                    "Invalid condition data field value for file field id %s and field condition id %s", field.getId(),
                    fieldCondition.getConditionFieldId()));
        }

        try {
            int conditionValue = fieldCondition.getConditionValue();
            String conditionOperator = fieldCondition.getConditionOperator();
            int value = Integer.valueOf(conditionDataFieldValue.getFieldValue().getStringValue());

            if (Comparison.valueOf(conditionOperator).compare(value, conditionValue)) {
                return true;
            }
        } catch (Exception ex) {
            throw new LoaderException(String.format(
                    "Error verifying field condition for condition field id %s and file field id %s",
                    fieldCondition.getConditionFieldId(), field.getId()), ex);
        }

        return false;
    }

    protected FieldValue<?> getFieldValue(InterfaceFileData iFileData, FileFieldDefinition field, Algorithm algorithm,
            DataFieldValue dependence) throws LoaderException {

        String[] nameFields = getFileNameFields(iFileData);

        if (nameFields == null || nameFields.length == 0) {
            throw new LoaderException(String.format("File data id %s does not have a valid "
                    + "file name field value for file type id %s and file field id %s", iFileData.getId(), iFileData
                    .getFileType().getId(), field.getId()));
        } else if (field.getFieldPosition() > nameFields.length) {
            throw new LoaderException(String.format("File field id %s has an invalid field positon "
                    + "%s in the file data name, file data id %s", field.getId(), field.getFieldPosition(),
                    iFileData.getId()));
        }

        return algorithm.getFieldValue(field, nameFields[field.getFieldPosition()], dependence);
    }
}
