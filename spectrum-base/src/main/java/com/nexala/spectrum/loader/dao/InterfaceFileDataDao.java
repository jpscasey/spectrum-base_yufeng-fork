/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.loader.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.google.inject.Inject;
import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.db.JdbcRowMapper;
import com.nexala.spectrum.db.QueryManager;
import com.nexala.spectrum.db.dao.BoxedResultSet;
import com.nexala.spectrum.db.dao.BoxedResultSetFactory;
import com.nexala.spectrum.db.dao.GenericDao;
import com.nexala.spectrum.loader.beans.InterfaceFile;
import com.nexala.spectrum.loader.beans.InterfaceFileData;

public class InterfaceFileDataDao extends GenericDao<InterfaceFileData, Long> {
    @Inject
    private InterfaceFileDataMapper iFileDataMapper;

    @Inject
    private QueryManager queryManager;

    private static final class InterfaceFileDataMapper extends JdbcRowMapper<InterfaceFileData> {
        @Inject
        private BoxedResultSetFactory boxedRSFactory;

        @Inject
        private FileTypeDao fileTypeDao;

        @Override
        public InterfaceFileData createRow(ResultSet results) throws SQLException {
            BoxedResultSet rs = boxedRSFactory.create(results);

            InterfaceFileData iFileData = new InterfaceFileData();
            iFileData.setId(rs.getLong(Spectrum.ID));
            iFileData.setFileName(rs.getString(Spectrum.FILE_NAME));
            iFileData.setTimestamp(rs.getTimestamp(Spectrum.IMPORT_TIMESTAMP));
            iFileData.setStatus(rs.getString(Spectrum.STATUS));
            iFileData.setData(rs.getBytes(Spectrum.DATA));

            iFileData.setFileType(fileTypeDao.findById(rs.getInt(Spectrum.FILE_TYPE_ID)));

            return iFileData;
        }
    };

    public List<InterfaceFileData> findByInterfaceFile(InterfaceFile iFile) {
        String query = queryManager.getQuery(Spectrum.INTERFACE_FILE_DATA_BY_PARENT_ID_QUERY);
        return findByQuery(query, iFileDataMapper, iFile.getId());
    }
}
