package com.nexala.spectrum.loader.processor;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.google.inject.Inject;
import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.configuration.ChannelConfiguration;
import com.nexala.spectrum.db.beans.ChannelConfig;
import com.nexala.spectrum.db.dao.DataAccessException;
import com.nexala.spectrum.loader.beans.DataFieldValue;
import com.nexala.spectrum.loader.beans.DataRecord;
import com.nexala.spectrum.loader.beans.FieldValue;
import com.nexala.spectrum.loader.beans.FileFieldDefinition;
import com.nexala.spectrum.loader.beans.InterfaceFileData;

public abstract class DataProcessor {
    @Inject
    protected ChannelConfiguration channelConfiguration;

    public void preProcess(InterfaceFileData iFileData) throws DataAccessException {}

    public abstract void process(InterfaceFileData iFileData) throws DataAccessException;

    public void postProcess(InterfaceFileData iFileData, Throwable error) throws DataAccessException {}

    protected abstract String getProcessorName();

    public String getProcessor() {
        return getProcessorName();
    }

    public String getSqlStatement(String storedProcedurename, DataRecord dataRecord, String fileName) {
        StringBuilder sql = new StringBuilder();

        sql.append("{ ? = call ");
        sql.append(storedProcedurename);
        sql.append(" (");

        String sep = "";
        for (DataFieldValue data : dataRecord.getDataFieldValues()) {
            FileFieldDefinition field = data.getFileFieldDefinition();

            if (field.getFieldType().equalsIgnoreCase(Spectrum.FIELD_TYPE_VALIDATION)) {
                continue;
            }

            FieldValue<?> fieldValue = data.getFieldValue();

            String value = fieldValue.getStringValue();
            if (field.getProcessingAlgorithm().equalsIgnoreCase(Spectrum.ALGORITHM_TIMESTAMP)) {
                Long ts = (Long) fieldValue.getValue();
                value = "'" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date(ts)) + "'";
            } else if (field.getProcessingAlgorithm().equalsIgnoreCase(Spectrum.ALGORITHM_TIME)) {
                Long ts = (Long) fieldValue.getValue();
                value = "'" + new SimpleDateFormat("HH:mm:ss.SSS").format(new Date(ts)) + "'";
            } else if (field.getProcessingAlgorithm().equalsIgnoreCase(Spectrum.ALGORITHM_STRING)) {
                value = "'" + value + "'";
            } else if (Spectrum.ALGORITHM_DOTNET_TICKS.equalsIgnoreCase(field.getProcessingAlgorithm())) {
                Long ts = (Long) fieldValue.getValue();
                value = "'" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date(ts)) + "'";
            }

            if (field.getFieldType().equalsIgnoreCase(Spectrum.FIELD_TYPE_CHANNEL)) {
                ChannelConfig config = channelConfiguration.getConfig(field.getChannelId());
                sql.append(sep + "@" + config.getShortName() + "=" + value);
                sep = ", ";
            } else if (field.getFieldType().equalsIgnoreCase(Spectrum.FIELD_TYPE_IDENTIFICATION)) {
                sql.append(sep + "@" + field.getIdentificationTag() + "=" + value);
                sep = ", ";
            }
        }

        sql.append(") }");

        return sql.toString();
    }
}
