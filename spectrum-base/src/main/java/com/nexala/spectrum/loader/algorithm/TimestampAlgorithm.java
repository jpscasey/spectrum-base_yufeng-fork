package com.nexala.spectrum.loader.algorithm;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import com.google.inject.Singleton;
import com.nexala.spectrum.loader.LoaderException;
import com.nexala.spectrum.loader.beans.DataFieldValue;
import com.nexala.spectrum.loader.beans.FieldValueLong;
import com.nexala.spectrum.loader.beans.FileFieldDefinition;

@Singleton
public class TimestampAlgorithm extends Algorithm {
    @Override
    public FieldValueLong getFieldValue(FileFieldDefinition fileFieldDefinition, String source,
            DataFieldValue dependence) throws LoaderException {

        source = source != null ? source.trim() : source;

        String pattern = fileFieldDefinition.getProcessingPattern();

        if (pattern == null) {
            throw new LoaderException(String.format(
                    "Error executing '%s', invalid processing pattern '%s' on file field definition id %s", this
                            .getClass().getName(), pattern, fileFieldDefinition.getId()));
        }

        try {
            DateFormat sdf = new SimpleDateFormat(pattern);
            return new FieldValueLong(sdf.parse(source).getTime());
        } catch (ParseException ex) {
            throw new LoaderException(String.format("Error executing '%s', source '%s', pattern '%s', "
                    + "file field definition id %s", this.getClass().getName(), source, pattern,
                    fileFieldDefinition.getId()), ex);
        }
    }
}
