package com.nexala.spectrum.loader.parser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.loader.LoaderException;
import com.nexala.spectrum.loader.algorithm.Algorithm;
import com.nexala.spectrum.loader.beans.DataFieldValue;
import com.nexala.spectrum.loader.beans.DataRecord;
import com.nexala.spectrum.loader.beans.FieldValue;
import com.nexala.spectrum.loader.beans.FileFieldDefinition;
import com.nexala.spectrum.loader.beans.InterfaceFileData;

public class TextDataParser extends DataParser {
    @Override
    public List<DataRecord> parse(InterfaceFileData iFileData) throws LoaderException {
        List<String[]> textRecords = getTextDataRecords(iFileData);

        List<DataRecord> parsedValues = new ArrayList<DataRecord>();

        for (String[] record : textRecords) {
            Map<Integer, DataFieldValue> dataFieldValueMap = new HashMap<Integer, DataFieldValue>();

            for (FileFieldDefinition field : iFileData.getFileType().getFileFields()) {
                if (field.getExistenceCondition() != null) {
                    if (!checkCondition(field, field.getExistenceCondition(),
                            dataFieldValueMap.get(field.getExistenceCondition().getConditionFieldId()))) {

                        continue;
                    }
                }

                Algorithm algorithm = algorithms.get(field.getProcessingAlgorithm());

                DataFieldValue dependence = null;
                if (field.getDependenceFieldID() != null) {
                    dependence = dataFieldValueMap.get(field.getDependenceFieldID());
                }

                FieldValue<?> fieldValue = null;
                if (field.getSource().equalsIgnoreCase(Spectrum.SOURCE_FILE_NAME)) {
                    fieldValue = getFieldValue(iFileData, field, algorithm, dependence);
                } else {
                    if (field.getFieldPosition() > record.length) {
                        throw new LoaderException(String.format("File field id %s has an invalid field positon "
                                + "%s in the file text data, file data id %s", field.getId(), field.getFieldPosition(),
                                iFileData.getId()));
                    }

                    fieldValue = algorithm.getFieldValue(field, record[field.getFieldPosition()], dependence);
                }

                dataFieldValueMap.put(field.getId(), new DataFieldValue(field, fieldValue));
            }

            parsedValues.add(new DataRecord(new ArrayList<DataFieldValue>(dataFieldValueMap.values())));
        }

        return parsedValues;
    }

    private List<String[]> getTextDataRecords(InterfaceFileData iFileData) throws LoaderException {
        List<String[]> textFileData = new ArrayList<String[]>();

        try {
            String data = new String(iFileData.getData(), "US-ASCII");
            if (iFileData.getFileType().getTextQualifier() != null) {
                data = data.replaceAll(iFileData.getFileType().getTextQualifier(), "");
            }

            String[] records = data.split("\n");
            for (String record : records) {
                if (!record.isEmpty()) {
                    textFileData.add(record.split(iFileData.getFileType().getFieldSeparator()));
                }
            }

            if (iFileData.getFileType().getHeaderRowsToSkip() > 0) {
                for (int i = 0; i < iFileData.getFileType().getHeaderRowsToSkip(); i++) {
                    textFileData.remove(0);
                }
            }

            return textFileData;
        } catch (Exception ex) {
            throw new LoaderException(String.format("Error getting the text data records for file data id %s and "
                    + "file type id %s", iFileData.getId(), iFileData.getFileType().getId()), ex);
        }
    }
}
