package com.nexala.spectrum.loader.algorithm;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.google.inject.Singleton;
import com.nexala.spectrum.loader.LoaderException;
import com.nexala.spectrum.loader.beans.DataFieldValue;
import com.nexala.spectrum.loader.beans.FieldValue;
import com.nexala.spectrum.loader.beans.FieldValueLong;
import com.nexala.spectrum.loader.beans.FileFieldDefinition;

@Singleton
public class TimeAlgorithm extends Algorithm {
    @Override
    public FieldValue<?> getFieldValue(FileFieldDefinition fileFieldDefinition, byte[] source, DataFieldValue dependence)
            throws LoaderException {

        if (dependence == null || dependence.getFieldValue() == null) {
            throw new LoaderException(String.format(
                    "Error executing '%s', invalid dependence value 'null' on file field definition id %s", this
                            .getClass().getName(), fileFieldDefinition.getId()));
        }

        long value = getValue(fileFieldDefinition, source);

        try {
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            Date date = df.parse(df.format(new Date(Long.parseLong(dependence.getFieldValue().getStringValue()))));
            return new FieldValueLong(date.getTime() + value);
        } catch (ParseException ex) {
            throw new LoaderException(String.format("Error executing '%s', source '%s', dependence field ID '%s', "
                    + "file field definition id %s", this.getClass().getName(), source,
                    fileFieldDefinition.getDependenceFieldID(), fileFieldDefinition.getId()), ex);
        }
    }

    private long getValue(FileFieldDefinition fileFieldDefinition, byte[] source) throws LoaderException {
        /*
         * Example A byte array containing 3 values for hour, minutes and
         * seconds specifically [4, 23, 54]
         * 
         * The processing pattern in the case could be (the example could not
         * make sense, it's just an example)
         * 
         * 0=[*, 3600000]|[-, 1000];1=[*, 60000];2=[*, 1000]
         * 
         * 0=[*, 3600000],[-, 1000] -> the first byte will be multiplied by
         * 3600000 and the result will be subtracted by 1000
         * 
         * 1=[*, 60000] -> the second byte will be multiplied by 60000
         * 
         * 2=[*, 1000] -> the third byte will be multiplied by 1000
         * 
         * All the 3 resultant values are summed to produce the final time
         * value, in this case milliseconds.
         */

        String pattern = fileFieldDefinition.getProcessingPattern();

        if (pattern == null) {
            throw new LoaderException(String.format(
                    "Error executing '%s', invalid processing pattern '%s' on file field definition id %s", this
                            .getClass().getName(), pattern, fileFieldDefinition.getId()));
        }

        long value = 0;

        String[] calcs = pattern.split(";");
        for (String calc : calcs) {
            String[] parts = calc.split("=");
            int pos = Integer.parseInt(parts[0].trim());

            long posValue = source[pos];

            for (String step : parts[1].split("\\|")) {
                step = step.replace("[", "").replace("]", "");

                String[] ops = step.split(",");

                String op = ops[0].trim();
                long opValue = Long.parseLong(ops[1].trim());

                if (op.equals("*")) {
                    posValue *= opValue;
                } else if (op.equals("/")) {
                    posValue /= opValue;
                } else if (op.equals("+")) {
                    posValue += opValue;
                } else if (op.equals("-")) {
                    posValue -= opValue;
                } else {
                    throw new LoaderException(String.format(
                            "Error executing '%s', invalid processing pattern '%s' on file field "
                                    + "definition id %s, invalid operation %s", this.getClass().getName(), pattern,
                            fileFieldDefinition.getId(), op));
                }
            }

            value += posValue;
        }

        return value;
    }
}