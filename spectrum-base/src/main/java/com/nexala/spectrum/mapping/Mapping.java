/*
 * Copyright (c) Nexala Technologies 2010, All rights reserved.
 */

package com.nexala.spectrum.mapping;

public class Mapping {

    // Prevent default instantiation
    private Mapping() {}
    
    /**
     * Converts DMS (Degrees, Minutes, Seconds) to Decimal Degrees
     * 
     * @param d (Degrees)
     * @param m (Minutes)
     * @param s (Seconds)
     * @return
     */
    //public static double degreesToDecimal(double d, double m, double s) {
    //    return d + m / 60 + s / 3600;
    //}

    // need to check east / west of 0'
    // (lat_hgh / 100 + cast((cast(lat_hgh as int) % 100) as float)/60 +
    // (lat_low / 10000 * 60 / 3600)) as Latitude,
    // (lng_hgh / 100 + cast((cast(lng_hgh as int) % 100) as float)/60 +
    // (lng_low / 10000 * 60 / 3600)) as Longitude

    /**
     * FIXME - These are for RTM only converts longitude from degrees/minutes/seconds as stored in the TDS database to
     * decimal degrees.
     */
    /*public static Double getLongitude(double hgh, double low, int llhs) {

        double lng = Mapping.degreesToDecimal(
        // first digits of lat_h are degrees
                (int) hgh / 100,
                // last digits of lat_h are minutes
                // lat_h - ((int)(lat_h/100)*100),
                hgh % 100,
                // lat_l is the decimal part of the
                // minutes (not seconds)
                (low / 10000) * 60);

        try {
            // convert 'valid' -> string -> binary -> int :)
            llhs = Byte.valueOf("" + llhs, 2).intValue();

            // when the coordinate is west of
            // longitude 0, negate..
            // (Bit 3 is not 1)
            if ((llhs & 0x00000002) == 0x00000002)
                lng = -lng;
        } catch (NumberFormatException e) {
            return null;
        }

        return lng;
    }*/

    /**
     * FIXME - This is for RTM only. converts latitude from degrees/minutes/seconds as stored in the TDS database to
     * decimal degrees.
     */
    /*public static Double getLatitude(double hgh, double low, int llhs) {

        // get the DD latitude from the lat_h
        // + lat_l fields.
        double lat = Mapping.degreesToDecimal(
        // first digits of lat_h are degrees
                (int) hgh / 100,
                // last digits of lat_h are minutes
                // lat_h - ((int)(lat_h/100)*100),
                hgh % 100,
                // lat_l is the decimal part of the
                // minutes (not seconds)
                (low / 10000) * 60);

        try {
            // convert 'valid' -> string -> binary -> int :)
            llhs = Byte.valueOf("" + llhs, 2).intValue();

            // when coordinate is south of equator
            // i.e. bit 1 is 1.
            if ((llhs & 0x00000001) == 0x00000001)
                lat = -lat;
        } catch (NumberFormatException e) {
            return null;
            // logger.error("'Value' field is not a valid format.");
        }

        return lat;
    }*/

    /**
     * Gets the distance between two coordinates, using a simple distance formula, which may be up to 10% less accurate
     * than the great circle formula.
     * 
     * @param latitude1
     * @param longitude1
     * @param latitude2
     * @param longitude2
     * @return distance between two coordinates
     */
    public static double getSimpleDistance(double latitude1, double longitude1, double latitude2, double longitude2) {
        double x = 69.1 * (latitude2 - latitude1);
        double y = 53D * (longitude2 - longitude1);
        return Math.sqrt(x * x + y * y);
    }

    /**
     * Gets the distance between two coordinates, using a simple distance formula, which may be up to 10% less accurate
     * than the great circle formula. Calls getSimpleDistance(double, double, double, double) internally.
     * 
     * @param latitude1
     * @param longitude1
     * @param latitude2
     * @param longitude2
     * @return distance between two coordinates
     */
    public static final double getSimpleDistance(Coordinate a, Coordinate b) {
        return Mapping.getSimpleDistance(a.getLatitude(), a.getLongitude(), b.getLatitude(), b.getLongitude());
    }

    /**
     * Gets the distance between two points using the great circle distance formula. From benchmarks, calculating
     * distance between 100k points was shown to take ~175ms, compared to ~15ms for the simple distance formula (above)
     * 
     * @return distance between two coordinates in kilometres
     */
    public static double getGreatCircleDistance(double latitude1, double longitude1, double latitude2, double longitude2) {
        latitude1 = Math.toRadians(latitude1);
        longitude1 = Math.toRadians(longitude1);
        latitude2 = Math.toRadians(latitude2);
        longitude2 = Math.toRadians(longitude2);

        return 6371D * Math.acos(Math.sin(latitude1) * Math.sin(latitude2) + Math.cos(latitude1) * Math.cos(latitude2)
                * Math.cos(longitude2 - longitude1));
    }

    /**
     * Gets the distance between two points using the great circle distance formula. From benchmarks, calculating
     * distance between 100k points was shown to take ~175ms, compared to ~15ms for the simple distance formula (above).
     * Calls getGreatCircleDistance(double, double, double, double) internally.
     * 
     * @return distance between two coordinates in kilometres
     */
    public static double getGreatCircleDistance(Coordinate a, Coordinate b) {
        return Mapping.getGreatCircleDistance(a.getLatitude(), a.getLongitude(), b.getLatitude(), b.getLongitude());
    }

    /**
     * Gets the resolution of the map. Given the latitude and zoom level, it is possible to return the number of metres
     * per pixel at that latitude. see: http://msdn.microsoft.com/en-us/library/aa940990.aspx
     * 
     * @param latitude
     * @param zoomLevel
     * @return pixels per metre at specified latitude and zoom
     */
    public static double getMetresPerPixelBing(double latitude, int zoomLevel) {
        return 156543.04D * Math.cos(Math.toRadians(latitude)) / (Math.pow(2, zoomLevel));
    }
}
