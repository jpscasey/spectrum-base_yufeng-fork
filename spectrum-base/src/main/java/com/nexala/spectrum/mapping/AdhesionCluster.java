package com.nexala.spectrum.mapping;

import com.nexala.spectrum.view.conf.maps.AdhesionMarker;

/**
 * Cluster for WSP and Sand.
 *
 */
public class AdhesionCluster extends Cluster<AdhesionMarker> {

    private int count = 0;

    private Integer category = 0;
    private Double bearing = 0D;

    public AdhesionCluster() {

    }

    public void init(AdhesionMarker sc) {
        this.setCoordinate(sc);
        this.bearing = sc.getBearing();
        this.category = sc.getCategory();
        this.addItem(sc);
    }

    @Override
    public void addItem(AdhesionMarker sc) {
        count = count + sc.getValue();
    }

    public Integer getCount() {
        return count;
    }

    public Double getBearing() {
        return this.bearing;
    }

    /**
     * Getter for category.
     * 
     * @return the category
     */
    public Integer getCategory() {
        return category;
    }

    /**
     * @param count
     */
    public void setCount(int count) {
        this.count = count;
    }

    /**
     * Set the category.
     * 
     * @param category
     *            the category to set
     */
    public void setCategory(Integer category) {
        this.category = category;
    }

    /**
     * Set the bearing.
     * 
     * @param bearing
     *            the bearing to set
     */
    public void setBearing(Double bearing) {
        this.bearing = bearing;
    }
}
