/*
 * Copyright (c) Nexala Technologies 2010, All rights reserved.
 */

package com.nexala.spectrum.mapping;

/**
 * Coordinate
 *
 * @author Paul O'Riordan
 * @created 13 Dec 2010
 *
 */
public class Coordinate {
    private Double latitude;
    private Double longitude;

    public Coordinate() {}
    
    /**
     * Creates a Coordinate object from the specified
     * values. Either parameter may be null.
     * @param latitude
     * @param longitude
     */
    public Coordinate(Double latitude, Double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }
    
    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    /**
     * @return The latitude of the Coordinate, may be null.
     */
    public Double getLatitude() {
        return latitude;
    }

    /**
     * @return The longitude of the Coordinate, may be null.
     */
    public Double getLongitude() {
        return longitude;
    }
    
    /**
     * @return A description of the Coordinate
     * in the following format "LATITUDE, LONGITUDE"
     * where LATITUDE and LONGITUDE are double
     * values.
     */
    @Override
    public String toString() {
        
        String lat = "null";
        String lng = "null";
        
        if (latitude != null) {
            lat = latitude.toString();
        }
        
        if (longitude != null) {
            lng = longitude.toString();
        }
        
        return "(" + lat + ", " + lng + ")";
    }
}
