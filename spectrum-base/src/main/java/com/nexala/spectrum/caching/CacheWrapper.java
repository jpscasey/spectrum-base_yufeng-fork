package com.nexala.spectrum.caching;

import java.util.List;

/**
 * Wrapper for a cache.
 * 
 * @author brice
 * 
 * @param <K>
 *            type of the key
 * @param <V>
 *            type of the element
 */
public interface CacheWrapper<K, V> {

	/**
	 * Add an element to the cache.
	 * 
	 * @param key key of the element
	 * @param value the element to put in the cache
	 */
	public void put(K key, V value);
	
	/**
	 * Replace an element value in the cache.
	 * 
	 * @param key key of the element to replace
	 * @param value the element to put in the cache
	 */
	public void replace(K key, V value);
	
	/**
	 * Add an element to the cache.
	 * 
	 * @param key key of the element
	 * @param value the element to put in the cache
	 * @param timeToIdleSeconds time idle before remove
	 * @param timeToLiveSeconds time to live before remove
	 */
	public void put(K key, V value, int timeToIdleSeconds, int timeToLiveSeconds);

	/**
	 * Get the element in the cache for this key.
	 * @param key key to retrieve
	 * @return the element in the cache for this key.
	 */
	public V get(K key);
	
	
	/**
	 * Remove the element in the cache witht the provided key
	 * @param key the key to remove form the cache
	 */
	public void remove(K key);
	
	/**
	 * Return true if this key is in cache, false otherwise.
	 * @param key the key to retrieve
	 * @return true if the key is in cache, false otherwise
	 */
	public boolean isKeyInCache(K key);
	
	/**
	* Acquires the proper read lock for a given cache key
	*
	* @param key - The key that retrieves a value that you want to protect via locking
	*/
	public void acquireReadLockOnKey(Object key);
	
	/**
	* Acquires the proper write lock for a given cache key
	*
	* @param key - The key that retrieves a value that you want to protect via locking
	*/
	public void acquireWriteLockOnKey(Object key);
	
	/**
	* Release a held read lock for the passed in key
	*
	* @param key - The key that retrieves a value that you want to protect via locking
	*/
	public void releaseReadLockOnKey(Object key);
	
	/**
	* Release a held write lock for the passed in key
	*
	* @param key - The key that retrieves a value that you want to protect via locking
	*/
	public void releaseWriteLockOnKey(Object key);
	
	/**
	 * Get the cache object wich is wrapped
	 * @return the cache manager
	 */
	public Object getCache();
	
	/**
     * Get the key set of the cache
     * @return the list of keys
     */
	public List<K> getKeys();
}