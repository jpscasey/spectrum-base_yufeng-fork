package com.nexala.spectrum.caching;

import java.util.List;

import net.sf.ehcache.Ehcache;
import net.sf.ehcache.Element;

/**
 * EHCache wrapper.
 * @author brice
 *
 * @param <K> the key
 * @param <V> the value
 */
public class EhCacheWrapper<K, V> implements CacheWrapper<K, V> {
	/*private final String cacheName;
	private final CacheManager cacheManager;*/
	
	private final Ehcache cache;

	public EhCacheWrapper(final Ehcache cache) {
		this.cache = cache;
	}

	/**
	 * @see com.nexala.spectrum.caching.CacheWrapper#put(java.lang.Object, java.lang.Object)
	 */
	@Override
	public void put(final K key, final V value) {
		getCache().put(new Element(key, value));
	}
	
	/**
	 * @see com.nexala.spectrum.caching.CacheWrapper#replace(java.lang.Object, java.lang.Object)
	 */
	@Override
	public void replace(K key, V value) {
		getCache().replace(new Element(key, value));
	}
	
	@Override
	public void put(K key, V value, int timeToIdleSeconds, int timeToLiveSeconds) {
		getCache().put(new Element(key, value, timeToIdleSeconds, timeToLiveSeconds));
	}

	/**
	 * @see com.nexala.spectrum.caching.CacheWrapper#get(java.lang.Object)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public V get(final K key) {
		Element element = getCache().get(key);
		if (element != null) {
			return (V) element.getObjectValue();
		}
		return null;
	}


	/**
	 * @see com.nexala.spectrum.caching.CacheWrapper#getCache()
	 */
	@Override
	public Ehcache getCache() {
		return this.cache;
	}
	
	/**
	 * @see com.nexala.spectrum.caching.CacheWrapper#remove(java.lang.Object)
	 */
	@Override
	public void remove(K key) {
		getCache().remove(key);
	}
	
	/**
	 * @see com.nexala.spectrum.caching.CacheWrapper#isKeyInCache(java.lang.Object)
	 */
	@Override
	public boolean isKeyInCache(K key) {
		return getCache().isKeyInCache(key);
	}

	/**
	 * @see com.nexala.spectrum.caching.CacheWrapper#acquireReadLockOnKey(java.lang.Object)
	 */
	@Override
	public void acquireReadLockOnKey(Object key) {
		getCache().acquireReadLockOnKey(key);
		
	}

	/**
	 * @see com.nexala.spectrum.caching.CacheWrapper#acquireWriteLockOnKey(java.lang.Object)
	 */
	@Override
	public void acquireWriteLockOnKey(Object key) {
		getCache().acquireWriteLockOnKey(key);
		
	}

	/**
	 * @see com.nexala.spectrum.caching.CacheWrapper#releaseReadLockOnKey(java.lang.Object)
	 */
	@Override
	public void releaseReadLockOnKey(Object key) {
		getCache().releaseReadLockOnKey(key);
		
	}

	/**
	 * @see com.nexala.spectrum.caching.CacheWrapper#releaseWriteLockOnKey(java.lang.Object)
	 */
	@Override
	public void releaseWriteLockOnKey(Object key) {
		getCache().releaseWriteLockOnKey(key);
		
	}

    @Override
    public List<K> getKeys() {
        return getCache().getKeys();
    }
	
}