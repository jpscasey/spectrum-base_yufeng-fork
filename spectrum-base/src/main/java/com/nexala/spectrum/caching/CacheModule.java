package com.nexala.spectrum.caching;

import net.sf.ehcache.CacheManager;
import net.sf.ehcache.config.CacheConfiguration;
import net.sf.ehcache.config.Configuration;
import net.sf.ehcache.config.MemoryUnit;
import net.sf.ehcache.config.PersistenceConfiguration;
import net.sf.ehcache.config.PersistenceConfiguration.Strategy;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;

public class CacheModule extends AbstractModule {

	@Override
	protected void configure() {
	}

	@Provides
	@Singleton
	CacheManager provideCacheManager() {

		// FIXME should be better to add it in configuration file
		// That's the default configuration for all cache that will be created
		CacheConfiguration cacheConfig = new CacheConfiguration().timeToLiveSeconds(3600)
				.maxBytesLocalHeap(5, MemoryUnit.MEGABYTES);
		
		PersistenceConfiguration persistanceConfig = new PersistenceConfiguration();
		persistanceConfig.strategy(Strategy.NONE);
		cacheConfig.addPersistence(persistanceConfig);

		Configuration config = new Configuration();
		config.setDefaultCacheConfiguration(cacheConfig);

		CacheManager cacheManager = CacheManager.create(config);
		
		return cacheManager;
	}

}
