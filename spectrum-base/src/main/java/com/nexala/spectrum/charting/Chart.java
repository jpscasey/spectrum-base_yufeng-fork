/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.charting;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.Plot;
import org.jfree.chart.plot.XYPlot;

import com.google.inject.Inject;

public class Chart {
    
    @Inject
    protected ChartStyle style;
    
    @Inject
    protected ColorGenerator colorGenerator;
    
    /**
     * @param chart
     * @return Image data (png)
     * @throws IOException
     */
    public byte [] renderChart(int height, int width, JFreeChart chart) throws IOException {
        Plot plot = chart.getPlot();
        plot.setBackgroundPaint(style.getPlotBackgroundColor());
        
        if (plot instanceof CategoryPlot) {
            CategoryPlot cPlot = (CategoryPlot) plot;
            cPlot.setRangeGridlinePaint(style.getPlotRangeGridlineColor());
            cPlot.setDomainGridlinePaint(style.getPlotDomainGridlineColor());
        } else if (plot instanceof XYPlot) {
            XYPlot xPlot = (XYPlot) plot;
            xPlot.setRangeGridlinePaint(style.getPlotRangeGridlineColor());
            xPlot.setDomainGridlinePaint(style.getPlotDomainGridlineColor());
        } 


        chart.setBackgroundPaint(style.getBackgroundColor());
        chart.setPadding(style.getPadding());
        
        BufferedImage img = chart.createBufferedImage(width, height);
        
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ImageIO.write(img, "png", baos);
        
        byte [] imageData = baos.toByteArray();
        
        return imageData;
    }
}
