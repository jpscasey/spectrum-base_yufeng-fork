package com.nexala.spectrum.charting;

import org.jfree.data.time.RegularTimePeriod;
import org.jfree.data.time.TimeSeries;

public class DigitalTimeSeries extends TimeSeries {
    /**
     * 
     */
    private static final long serialVersionUID = -6624086264437075510L;

    private int scale;

    public DigitalTimeSeries(Comparable<?> name, int scale) {
        super(name);
        this.scale = scale;
    }

    @Override
    public void add(RegularTimePeriod time, Number value) {
        if (value == null) {
            super.addOrUpdate(time, null);
        } else {
            super.addOrUpdate(time, scale + value.doubleValue() / 2);
        }
    }

    @Override
    public void add(RegularTimePeriod time, double value) {
        super.addOrUpdate(time, scale + value / 2);
    }
}