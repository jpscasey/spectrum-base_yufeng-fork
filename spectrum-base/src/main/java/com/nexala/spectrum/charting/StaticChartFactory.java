/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.charting;

import java.util.List;
import java.util.TimeZone;

import com.google.inject.assistedinject.Assisted;
import com.nexala.spectrum.db.beans.ChannelConfig;

public interface StaticChartFactory {
    StaticChart create(
            @Assisted("width") Integer width,
            @Assisted("height") Integer height,
            @Assisted List<ChannelConfig> channels,
            @Assisted TimeZone timezone,
            @Assisted StaticChartConfig config);
}
