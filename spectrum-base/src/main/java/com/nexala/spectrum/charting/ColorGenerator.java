/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.charting;

import java.awt.Color;

import com.google.inject.ImplementedBy;


@ImplementedBy(DefaultColorGenerator.class)
public interface ColorGenerator {
    public Color next();
    public void reset();
}