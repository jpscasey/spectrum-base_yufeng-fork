package com.nexala.spectrum.charting;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.nexala.spectrum.charting.StaticChart.ChartType;

/**
 * Configuration for a static chart.
 * @author brice
 *
 */
public class StaticChartConfig {
	
	private List<Integer> channelsId;
	
	private Map<Integer, BigDecimal> scaleByChannelsId;
	
	private ChartType type;
	
	private String chartName;
	
	private String vehicleType;
	
	private Integer id;
	
	private BigDecimal lowerBound;
	
	private BigDecimal upperBound;
	
	private int ticks;

	/**
	 * Getter for the channelsId.
	 * @return the channelsId
	 */
	public List<Integer> getChannelsId() {
		return channelsId;
	}

	/**
	 * Setter for the channelsId.
	 * @param channelsId the channelsId to set
	 */
	public void setChannelsId(List<Integer> channelsId) {
		this.channelsId = channelsId;
	}

	/**
	 * Getter for the type.
	 * @return the type
	 */
	public ChartType getType() {
		return type;
	}

	/**
	 * Setter for the type.
	 * @param type the type to set
	 */
	public void setType(ChartType type) {
		this.type = type;
	}

	/**
	 * Getter for the chartName.
	 * @return the chartName
	 */
	public String getChartName() {
		return chartName;
	}

	/**
	 * Setter for the chartName.
	 * @param chartName the chartName to set
	 */
	public void setChartName(String chartName) {
		this.chartName = chartName;
	}

	/**
	 * Getter for the lowerBound.
	 * @return the lowerBound
	 */
	public BigDecimal getLowerBound() {
		return lowerBound;
	}

	/**
	 * Setter for the lowerBound.
	 * @param lowerBound the lowerBound to set
	 */
	public void setLowerBound(BigDecimal lowerBound) {
		this.lowerBound = lowerBound;
	}

	/**
	 * Getter for the upperBound.
	 * @return the upperBound
	 */
	public BigDecimal getUpperBound() {
		return upperBound;
	}

	/**
	 * Setter for the upperBound.
	 * @param upperBound the upperBound to set
	 */
	public void setUpperBound(BigDecimal upperBound) {
		this.upperBound = upperBound;
	}

	/**
	 * Getter for the scaleByChannelsId.
	 * @return the scaleByChannelsId
	 */
	public Map<Integer, BigDecimal> getScaleByChannelsId() {
		return scaleByChannelsId;
	}

	/**
	 * Setter for the scaleByChannelsId.
	 * @param scaleByChannelsId the scaleByChannelsId to set
	 */
	public void setScaleByChannelsId(Map<Integer, BigDecimal> scaleByChannelsId) {
		this.scaleByChannelsId = scaleByChannelsId;
	}

	/**
	 * Getter for the vehicleType.
	 * @return the vehicleType
	 */
	public String getVehicleType() {
		return vehicleType;
	}

	/**
	 * Setter for the vehicleType.
	 * @param vehicleType the vehicleType to set
	 */
	public void setVehicleType(String vehicleType) {
		this.vehicleType = vehicleType;
	}

	/**
	 * Getter for the id.
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * Setter for the id.
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	public int getTicks() {
		return ticks;
	}

	public void setTicks(int ticks) {
		this.ticks = ticks;
	}

	@Override
	public String toString() {
		final int maxLen = 10;
		return "StaticChartConfig [channelsId="
				+ (channelsId != null ? toString(channelsId, maxLen) : null)
				+ ", scaleByChannelsId="
				+ (scaleByChannelsId != null ? toString(
						scaleByChannelsId.entrySet(), maxLen) : null)
				+ ", type=" + type + ", chartName=" + chartName
				+ ", vehicleType=" + vehicleType + ", id=" + id
				+ ", lowerBound=" + lowerBound + ", upperBound=" + upperBound
				+ "]";
	}

	private String toString(Collection<?> collection, int maxLen) {
		StringBuilder builder = new StringBuilder();
		builder.append("[");
		int i = 0;
		for (Iterator<?> iterator = collection.iterator(); iterator.hasNext()
				&& i < maxLen; i++) {
			if (i > 0)
				builder.append(", ");
			builder.append(iterator.next());
		}
		builder.append("]");
		return builder.toString();
	}


}
