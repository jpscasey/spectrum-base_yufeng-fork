package com.nexala.spectrum.charting;

import java.awt.Color;

public class DefaultColorGenerator implements ColorGenerator {
    private Color[] colors = new Color[] {
            // new Color(0xf2, 0xf3, 0xf4), // White-263
            // new Color(0x22, 0x22, 0x22), // Black-267
            new Color(0xF3, 0xC3, 0x00), // Yellow-82
            new Color(0x87, 0x56, 0x92), // Purple-218
            new Color(0xF3, 0x84, 0x00), // Orange-48
            new Color(0xA1, 0xCA, 0xF1), // LightBlue-180
            new Color(0xBE, 0x00, 0x32), // Red-11
            new Color(0xC2, 0xB2, 0x80), // Buff-90
            new Color(0x84, 0x84, 0x82), // Gray-265
            new Color(0x00, 0x88, 0x56), // Green-139
            new Color(0xE6, 0x8F, 0xAC), // PurplishPink-247
            new Color(0x00, 0x67, 0xA5), // Blue-178
            new Color(0xF9, 0x93, 0x79), // YellowishPink-26
            new Color(0x60, 0x4E, 0x97), // Violet-207
            new Color(0xF6, 0xA6, 0x00), // OrangeYellow-66
            new Color(0xB3, 0x44, 0x6C), // PurplishRed-255
            new Color(0xDC, 0xD3, 0x00), // GreenishYellow-97
            new Color(0x88, 0x2D, 0x17), // ReddishBrown-40
            new Color(0x8D, 0xB6, 0x00), // YellowGreen-115
            new Color(0x65, 0x45, 0x22), // YellowishBrown-75
            new Color(0xE2, 0x58, 0x22), // ReddishOrange-34
            new Color(0x2B, 0x3D, 0x26)  // OliveGreen-126
    };

    private int numColors = colors.length;
    private int index = 0;

    public Color next() {
        Color next = colors[index++ % numColors];
        return new Color(next.getRed(), next.getGreen(), next.getBlue());
    }

    public void reset() {
        index = 0;
    }
}