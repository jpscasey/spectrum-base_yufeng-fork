package com.nexala.spectrum.charting;

import java.awt.Font;
import java.awt.Graphics2D;

import org.jfree.chart.block.RectangleConstraint;
import org.jfree.chart.title.TextTitle;
import org.jfree.ui.Size2D;

/**
 * A vertical text title with a fixed size.
 * @author brice
 *
 */
public class VerticalTextTitle extends TextTitle {

	private static final long serialVersionUID = 1L;
	
	
	private int fixedWidth = 30;

	public VerticalTextTitle(String text, Font font) {
		super(text, font);
	}

	@Override
	public Size2D arrange(Graphics2D g2, RectangleConstraint constraint) {
		RectangleConstraint cc = toContentConstraint(constraint);
        Size2D contentSize = null;
        
        contentSize = arrangeRR(g2, cc.getWidthRange(),
                cc.getHeightRange());
		
		return new Size2D(calculateTotalWidth(getFixedWidth()),
                calculateTotalHeight(contentSize.getHeight()));
	}

	/**
	 * Getter for the fixedWidth.
	 * @return the fixedWidth
	 */
	public int getFixedWidth() {
		return fixedWidth;
	}

	/**
	 * Setter for the fixedWidth.
	 * @param fixedWidth the fixedWidth to set
	 */
	public void setFixedWidth(int fixedWidth) {
		this.fixedWidth = fixedWidth;
	}
	
}
