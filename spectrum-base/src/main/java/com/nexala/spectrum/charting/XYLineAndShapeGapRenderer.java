package com.nexala.spectrum.charting;

import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.CrosshairState;
import org.jfree.chart.plot.PlotRenderingInfo;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYItemRendererState;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYDataset;

public class XYLineAndShapeGapRenderer extends XYLineAndShapeRenderer {

    private static final long serialVersionUID = -4160296495720509024L;

    private int slotInMilliseconds;

    public XYLineAndShapeGapRenderer(int slotInMilliseconds) {
        this.slotInMilliseconds = slotInMilliseconds;
    }

    @Override
    public void drawItem(Graphics2D g2, XYItemRendererState state, Rectangle2D dataArea, PlotRenderingInfo info,
            XYPlot plot, ValueAxis domainAxis, ValueAxis rangeAxis, XYDataset dataset, int series, int item,
            CrosshairState crosshairState, int pass) {

        if ((item == 0)
                || (dataset.getX(series, item).longValue() - dataset.getX(series, item - 1).longValue() < (slotInMilliseconds))) {
            super.drawItem(g2, state, dataArea, info, plot, domainAxis, rangeAxis, dataset, series, item,
                    crosshairState, pass);
        }
    }
}