package com.nexala.spectrum.charting;

public class StaticChartConstants {
	
	public static final int VEHICLETYPE_CL90 = 1;
	
	
	public static final int TIMEDRIFT_SECONDS = 30;
	public static final Integer THINNER_STROKE_INTERVAL = 30;
	public static final Integer DATAFORMAT_SECONDS_INTERVAL = 2;

}
