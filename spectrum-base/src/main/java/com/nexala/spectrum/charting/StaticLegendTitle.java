package com.nexala.spectrum.charting;

import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.util.List;

import org.jfree.chart.LegendItem;
import org.jfree.chart.LegendItemSource;
import org.jfree.chart.block.Arrangement;
import org.jfree.chart.block.Block;
import org.jfree.chart.block.BlockContainer;
import org.jfree.chart.block.BorderArrangement;
import org.jfree.chart.block.CenterArrangement;
import org.jfree.chart.block.LabelBlock;
import org.jfree.chart.block.LineBorder;
import org.jfree.chart.block.RectangleConstraint;
import org.jfree.chart.title.LegendGraphic;
import org.jfree.chart.title.LegendItemBlockContainer;
import org.jfree.chart.title.LegendTitle;
import org.jfree.ui.RectangleEdge;
import org.jfree.ui.RectangleInsets;
import org.jfree.ui.Size2D;
import org.jfree.ui.VerticalAlignment;
import org.jfree.util.SortOrder;

import com.nexala.spectrum.charting.StaticChart.ChartType;

public class StaticLegendTitle extends LegendTitle {

	private static final long serialVersionUID = 1L;
	
	private StaticChartConfig config = null;
	private Integer height = null;
	
	public StaticLegendTitle(List<LegendItemSource> itemSource, StaticChartConfig config, Integer height, boolean reverseLegendOrder) {
		super(itemSource.get(0));
		setSources(itemSource.toArray(new LegendItemSource[itemSource.size()]));
		
		this.config = config;
		this.height = height;
		
		init();
		
		if(reverseLegendOrder) {
		    setSortOrder(SortOrder.DESCENDING);
		}
	}	

	private void init() {
		setPosition(RectangleEdge.RIGHT);
		setVerticalAlignment(VerticalAlignment.TOP);
		setFrame(new LineBorder());
		setMargin(5, 0, 0, 0);
		setPadding(0,0,0,0);
	}
	
	@Override
	protected Block createLegendItemBlock(LegendItem item) {
        BlockContainer result = null;
        LegendGraphic lg = new LegendGraphic(item.getShape(),
        		item.getFillPaint());

        double itemHeight = calculateItemHeight();
        RectangleInsets itemPadding = calculateItemPadding(item.getDatasetIndex(),itemHeight, 2);
        
        lg.setFillPaintTransformer(item.getFillPaintTransformer());
        lg.setShapeFilled(item.isShapeFilled());
        lg.setLine(item.getLine());
        lg.setLineStroke(item.getLineStroke());
        lg.setLinePaint(item.getLinePaint());
        lg.setLineVisible(item.isLineVisible());
        lg.setShapeVisible(item.isShapeVisible());
        lg.setShapeOutlineVisible(item.isShapeOutlineVisible());
        lg.setOutlinePaint(item.getOutlinePaint());
        lg.setOutlineStroke(item.getOutlineStroke());
        lg.setPadding(itemPadding);

        LegendItemBlockContainer legendItem = new LegendItemBlockContainer(
        	new BorderArrangement(), item.getDataset(),
            item.getSeriesKey());
        lg.setShapeAnchor(getLegendItemGraphicAnchor());
        lg.setShapeLocation(getLegendItemGraphicLocation());
        legendItem.add(lg, RectangleEdge.LEFT);
        
		Font textFont = new Font(
			this.getItemFont().getFontName(), 
			this.getItemFont().getStyle(), 
			10 // Font size
		);
        
        Paint textPaint = item.getLabelPaint();
        if (textPaint == null) {
        	textPaint = StaticLegendTitle.DEFAULT_ITEM_PAINT;
        }
        RectangleInsets labelPadding = calculateItemPadding(item.getDatasetIndex(),itemHeight, 4);
        LabelBlock labelBlock = new LabelBlock(item.getLabel(), textFont,
        	textPaint);
        labelBlock.setPadding(labelPadding);
        legendItem.add(labelBlock,RectangleEdge.RIGHT);
        legendItem.setToolTipText(item.getToolTipText());
        legendItem.setURLText(item.getURLText());
        result = new TDSLegendContainer(new CenterArrangement(),itemHeight);
        result.setPadding(0,0,0,0);
        result.setMargin(0,0,0,0);
        result.add(legendItem);
        
        return result;

	}
	
	
	private RectangleInsets calculateItemPadding(int position, double itemHeight, double leftPadding) {
		double topPadding = 0;
		double bottomPadding = 0;
		if (config.getType().equals(ChartType.DIGITAL)) {
		    if (position == 0 ) {
		        topPadding = (itemHeight/2)+4;
		    }
		} else {
		    topPadding = 2;
		}
		
		return new RectangleInsets(topPadding, leftPadding, bottomPadding, 0); 
	}

	private double calculateItemHeight() {
		int numChannels = config.getChannelsId().size();
		if(config.getType().equals(ChartType.DIGITAL)){
			double graphBoxHeight = this.calculateBoxHeight();
			/* height without 5 of margins divide by the number of channel 
			 * (half a channel of margin at the top) 
			 */
			return (graphBoxHeight-5) / (numChannels+0.5);
		}else{
			int iSeriesCount = numChannels + 1;
			return (this.calculateBoxHeight() / (iSeriesCount) ) - (12/iSeriesCount) ;
		}
	}

	@Override
	public Size2D arrange(Graphics2D g2, RectangleConstraint constraint) {
		Size2D superSize = super.arrange(g2, constraint);
		superSize.height = calculateBoxHeight();
		superSize.width  = 140;
		return superSize;
	}
	
	private double calculateBoxHeight() {
		//return config.chartHeight - (config.isDigital() ?  50 : 40);
		return this.height - 20;
	}

	private class TDSLegendContainer extends BlockContainer {

		private static final long serialVersionUID = 1L;		
		private double itemHeight = 0;
		public TDSLegendContainer(Arrangement a,double height) {
			super(new BorderArrangement());
			this.itemHeight = height;
		}
		
		public Size2D arrange(Graphics2D g2, RectangleConstraint constraint) {
			Size2D superSize = super.arrange(g2, constraint);
			if(config.getType().equals(ChartType.DIGITAL))
				superSize.height = itemHeight;
			
			return superSize;
		}
	}

}
