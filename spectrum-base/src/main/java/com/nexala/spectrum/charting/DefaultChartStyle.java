package com.nexala.spectrum.charting;

import java.awt.Color;
import java.awt.Font;

import org.jfree.ui.HorizontalAlignment;
import org.jfree.ui.RectangleEdge;
import org.jfree.ui.RectangleInsets;

public class DefaultChartStyle implements ChartStyle {
    @Override
    public RectangleInsets getAxisLabelInsetsLeft() {
        return new RectangleInsets(0, 3, 0, 2);
    }
    
    @Override
    public RectangleInsets getAxisLabelInsetsRight() {
        return new RectangleInsets(0, 2, 0, 3);
    }

    @Override
    public RectangleInsets getAxisOffset() {
        return new RectangleInsets(5, 5, 5, 5);
    }

    @Override
    public Color getBackgroundColor() {
        return Color.WHITE;
    }

    @Override
    public HorizontalAlignment getLegendAlignment() {
        return HorizontalAlignment.LEFT;
    }

    @Override
    public Color getLegendBackgroundColor() {
        return Color.WHITE;
    }

    @Override
    public double getLegendBorderBottom() {
        return 0D;
    }

    @Override
    public double getLegendBorderLeft() {
        return 0D;
    }

    @Override
    public double getLegendBorderRight() {
        return 0D;
    }

    @Override
    public double getLegendBorderTop() {
        return 0D;
    }

    @Override
    public Font getLegendFont() {
        return new Font("SansSerif", Font.PLAIN, 10);
    }

    @Override
    public RectangleInsets getLegendPadding() {
        return new RectangleInsets(0, 0, 0, 0);
    }

    @Override
    public RectangleEdge getLegendPosition() {
        return RectangleEdge.RIGHT;
    }

    @Override
    public RectangleInsets getPadding() {
        return new RectangleInsets(0, 0, 0, 0);
    }

    @Override
    public Color getPlotBackgroundColor() {
    	return Color.WHITE;
    }

    @Override
    public Color getPlotDomainGridlineColor() {
        return Color.GRAY;
    }

    @Override
    public Color getPlotRangeGridlineColor() {
        return Color.GRAY;
    }
    
    public Color getGridlineColor() {
        return Color.GRAY;
    }
}