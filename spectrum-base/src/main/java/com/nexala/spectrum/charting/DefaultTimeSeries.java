package com.nexala.spectrum.charting;

import org.jfree.data.time.RegularTimePeriod;
import org.jfree.data.time.TimeSeries;

public class DefaultTimeSeries extends TimeSeries {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
     * 
     */


    public DefaultTimeSeries(Comparable<?> name) {
        super(name);
    }

    @Override
    public void add(RegularTimePeriod time, Number value) {
        if (value == null) {
            super.addOrUpdate(time, null);
        } else {
            super.addOrUpdate(time, value);
        }
    }

    @Override
    public void add(RegularTimePeriod time, double value) {
        super.addOrUpdate(time, value);
    }
}