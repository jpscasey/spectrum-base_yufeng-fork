/*

 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.charting;

import java.awt.BasicStroke;
import java.awt.Color;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.AxisLocation;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.SymbolAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.CombinedDomainXYPlot;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYItemRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.chart.renderer.xy.XYStepRenderer;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.ui.RectangleInsets;

import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.configuration.ApplicationConfiguration;
import com.nexala.spectrum.db.beans.ChannelConfig;
import com.nexala.spectrum.db.dao.charting.ChannelType;
import com.nexala.spectrum.i18n.ResourceBundleUTF8;
import com.nexala.spectrum.licensing.Licence;
import com.nexala.spectrum.rest.data.beans.DataSet;

public class DVChart extends TimeChart {

    private final CombinedDomainXYPlot plot;

    private final int height;
    private final int width;
    private final int maxDescLength;
    
    private final List<ChannelConfig> analogue = new ArrayList<ChannelConfig>();
    private final List<ChannelConfig> digital = new ArrayList<ChannelConfig>();
    private final int digitalWeight;
    private final int analogueWeight;
    private final double analogueHeight;
    private final Locale locale = Locale.UK; // FIXME
    private String channelLabelConfig;

    @Inject
    public DVChart(ApplicationConfiguration appConfig, @Assisted("width") Integer width,
            @Assisted List<ChannelConfig> channels, @Assisted TimeZone timezone) {
    	
    	super(channels, timezone);

        slotInMilliseconds = appConfig.get().getInt("r2m.dataView.dataPlotSlotInSeconds") * 1000;
        
        maxDescLength = appConfig.get().getInt("r2m.dataView.maxDescriptionLength");
        
        channelLabelConfig = "description";
        if(appConfig.get().hasPath("r2m.dataView.chartChannelLabel")) {
            channelLabelConfig = appConfig.get().getString("r2m.dataView.chartChannelLabel");
        }

        DateAxis dateAxis = new DateAxis();

        dateAxis.setTimeZone(tz);

        plot = new CombinedDomainXYPlot(dateAxis);

        for (ChannelConfig channel : channels) {
            final String id = channel.getName();
            final TimeSeries dataset;

            if (channel.getType() == ChannelType.DIGITAL) {
                digital.add(channel);
                dataset = new DigitalTimeSeries(id, digital.size() - 1);
            } else if (channel.getType() == ChannelType.ANALOG) {
                analogue.add(channel);
                dataset = new TimeSeries(id);
            } else {
                dataset = null;
            }

            if (dataset != null) {
                channelIdToDataset.put(id, dataset);
            }
        }

        int numAnalogue = (int) Math.ceil(analogue.size() / 4D);

        this.digitalWeight = 25 * digital.size() + 40;
        this.analogueWeight = 180;
        this.height = numAnalogue * analogueWeight + digitalWeight + 20; // GAP
        this.width = width;
        
        // The height of the analogue chart in pixels
        analogueHeight = (double) height * analogueWeight / (analogueWeight + digitalWeight);
    }

    private void createDigitalPlot(Licence license) {
        if (digital.size() < 1) {
            return;
        }
        ResourceBundleUTF8 bundle = new ResourceBundleUTF8(license.getUserLocale());
        // XXX styleXYPlot(plot)
        XYPlot subplot = new XYPlot();
        subplot.setRangeGridlinePaint(style.getGridlineColor());
        subplot.setDomainGridlinePaint(style.getGridlineColor());
        subplot.setRenderer(new StandardXYItemRenderer());

        XYStepRenderer renderer = slotInMilliseconds != null ? new XYStepGapRenderer(slotInMilliseconds)
                : new XYStepRenderer();
        renderer.setUseOutlinePaint(true);
        renderer.setUseFillPaint(false);
        renderer.setUseOutlinePaint(false);

        String[] axisLabels = new String[digital.size()];

        TimeSeriesCollection dataset = new TimeSeriesCollection(tz);

        int i = 0;

        for (ChannelConfig channel : digital) {
            Color color = colorGenerator.next();
            renderer.setSeriesLinesVisible(i, true);
            renderer.setSeriesShapesVisible(i, false);
            renderer.setSeriesPaint(i, color);
            renderer.setSeriesOutlinePaint(i, color);
            renderer.setSeriesStroke(i, new BasicStroke(1.0f));
            
            // use configuration to decide which field to use for label
            String channelLabel;              
            switch (channelLabelConfig) {
                case "name" :
                    channelLabel = channel.getName();
                    break;
                case "description" :
                    channelLabel = bundle.getString(channel.getName());
                    if(null== channelLabel || channelLabel.equals("")) {
                    	channelLabel= channel.getDescription();
                    }
                    break;
                default :
                    channelLabel = channel.getDescription();
            }

            TimeSeries series = channelIdToDataset.get(channel.getName());
            dataset.addSeries(series);

            subplot.setRenderer(i, renderer);
            
            axisLabels[i] = channelLabel;

            i++;
        }

        ValueAxis axis = new SymbolAxis(null, axisLabels);
        axis.setLabelFont(style.getLegendFont());
        axis.setRange(-0.5, digital.size());

        subplot.setRangeAxis(axis);
        subplot.setDataset(dataset);
        subplot.setAxisOffset(new RectangleInsets(0, 0, 0, 0));
        // plot.setDataset(dataset);
        plot.add(subplot, digitalWeight);
    }

    private void createAnaloguePlots(Licence license) {
        for (int i = 0; i < analogue.size(); i += 4) {
            XYPlot subplot = new XYPlot();
            subplot.setRangeGridlinePaint(style.getGridlineColor());
            subplot.setDomainGridlinePaint(style.getGridlineColor());
            ResourceBundleUTF8 bundle = new ResourceBundleUTF8(license.getUserLocale());
            for (int j = i; j < (i + 4) && j < analogue.size(); j++) {
                int plotSeries = j;
                int plotSeriesNum = j - i;

                ChannelConfig channel = analogue.get(plotSeries);
                Color color = colorGenerator.next();
                
                // use configuration to decide which field to use for label
                String channelLabel;              
                switch (channelLabelConfig) {
                    case "name" :
                        channelLabel = channel.getName();
                        break;
                    case "description" :
                    	 channelLabel = bundle.getString(channel.getName());
                         if(null== channelLabel || channelLabel.equals("")) {
                         	channelLabel= channel.getDescription();
                         }
                        break;
                    default :
                        channelLabel = channel.getDescription();
                }

                NumberAxis axis = new NumberAxis(channelLabel);
                if (maxDescLength == -1){
                	axis = new NumberAxis(channelLabel);
                } else if (null!=channelLabel && channelLabel.length() > maxDescLength) {
                	axis = new NumberAxis(channelLabel.substring(0, maxDescLength));
                }

                Double minValue = channel.getMinValue();
                Double maxValue = channel.getMaxValue();

                axis.setRange(minValue == null ? MIN_VALUE : minValue, maxValue == null ? MAX_VALUE : maxValue);

                axis.setAxisLinePaint(color);
                // axis.setLabelPaint(color);
                axis.setLabelFont(style.getLegendFont());
                axis.setAxisLineStroke(new BasicStroke(2.0f));

                final AxisLocation location;

                location = AxisLocation.BOTTOM_OR_LEFT;

                // Setting label insets causes the axes to
                // overflow [off the chart].
                // axis.setLabelInsets(labelInsets);

                subplot.setRangeAxisLocation(plotSeriesNum, location);
                subplot.setRangeAxis(plotSeriesNum, axis);
                subplot.mapDatasetToRangeAxis(plotSeriesNum, plotSeriesNum);
                subplot.setAxisOffset(new RectangleInsets(0, 0, 0, 0));

                // Each series requires a TimeSeriesCollection if it will
                // have its own scale..
                TimeSeriesCollection collection = new TimeSeriesCollection(channelIdToDataset.get(channel.getName()), tz);

                subplot.setDataset(plotSeriesNum, collection);

                // Each series also requires its own renderer, or colours
                // will not render correctly.
                XYLineAndShapeRenderer renderer = slotInMilliseconds != null ? new XYLineAndShapeGapRenderer(slotInMilliseconds) : new XYLineAndShapeRenderer();
                renderer.setUseFillPaint(false);
                renderer.setUseOutlinePaint(false);
                renderer.setSeriesLinesVisible(0, true);
                renderer.setSeriesShapesVisible(0, false);
                renderer.setSeriesPaint(0, color);
                renderer.setSeriesOutlinePaint(0, color);
                renderer.setSeriesStroke(0, new BasicStroke(1.0f));
                renderer.setSeriesToolTipGenerator(0, null);
                renderer.setUseOutlinePaint(true);

                subplot.setRenderer(plotSeriesNum, renderer);
            }

            plot.add(subplot, analogueWeight);
        }
    }
    
    public void init(long interval, Long endTime, Licence license) {
        if (endTime == null) {
            endTime = Calendar.getInstance(tz).getTimeInMillis();
        }

        plot.getDomainAxis().setRange(endTime - interval, endTime);
        plot.setGap(20);

        createDigitalPlot(license);
        createAnaloguePlots(license);
    }
    
    public void plot(List<DataSet> data) {
        plotData(data, locale, analogueHeight);
    }
    

    public byte[] render() throws IOException {
        JFreeChart chart = new JFreeChart(null, null, plot, false);
        return renderChart(height, width, chart);
    }

}