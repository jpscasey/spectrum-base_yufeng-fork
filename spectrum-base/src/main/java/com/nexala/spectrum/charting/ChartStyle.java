package com.nexala.spectrum.charting;

import java.awt.Color;
import java.awt.Font;

import org.jfree.ui.HorizontalAlignment;
import org.jfree.ui.RectangleEdge;
import org.jfree.ui.RectangleInsets;

import com.google.inject.ImplementedBy;

@ImplementedBy(DefaultChartStyle.class)
public interface ChartStyle {
    Color getBackgroundColor();

    HorizontalAlignment getLegendAlignment();

    Color getLegendBackgroundColor();

    double getLegendBorderBottom();

    double getLegendBorderLeft();

    double getLegendBorderRight();

    double getLegendBorderTop();

    Font getLegendFont();

    RectangleInsets getLegendPadding();

    RectangleInsets getAxisLabelInsetsLeft();

    RectangleInsets getAxisLabelInsetsRight();

    RectangleInsets getAxisOffset();

    RectangleEdge getLegendPosition();

    RectangleInsets getPadding();

    Color getPlotBackgroundColor();

    Color getPlotDomainGridlineColor();

    Color getPlotRangeGridlineColor();

    Color getGridlineColor();
}