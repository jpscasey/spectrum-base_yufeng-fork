package com.nexala.spectrum.charting;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import org.apache.log4j.Logger;
import org.jfree.data.time.Millisecond;
import org.jfree.data.time.TimeSeries;

import com.nexala.spectrum.channel.Channel;
import com.nexala.spectrum.db.beans.ChannelConfig;
import com.nexala.spectrum.db.dao.charting.ChannelType;
import com.nexala.spectrum.rest.data.beans.DataSet;

public class TimeChart extends Chart {
	
    private final Logger logger = Logger.getLogger(TimeChart.class);

    protected Map<String, TimeSeries> channelIdToDataset = new HashMap<String, TimeSeries>();
	
	protected static final double MIN_VALUE = 0;
    protected static final double MAX_VALUE = 100;
	
	private final List<ChannelConfig> channels;
	protected Integer slotInMilliseconds = null;
	protected final TimeZone tz;
	
	
	public TimeChart(List<ChannelConfig> channels, TimeZone tz) {
		this.channels = channels;
		this.tz = tz;
	}

	protected void plotData(List<DataSet> data, Locale locale, double height) {
		for (ChannelConfig conf : channels) {
			if(conf.isDisplayedAsDifference())
				this.plotDataAsDifference(conf, data, locale, height);
			else
				this.plotData(conf, data, locale, height);
        }
	}

	private void plotDataAsDifference(ChannelConfig conf, List<DataSet> data, Locale locale, double height) {
		final int id = conf.getId();
        final TimeSeries dataset = channelIdToDataset.get(conf.getName());
        double maxValue = conf.getType() == ChannelType.DIGITAL ? 1 : MAX_VALUE;
        double minValue = MIN_VALUE;

        if (conf.getMaxValue() != null) {
            maxValue = conf.getMaxValue();
        }

        if (conf.getMinValue() != null) {
            minValue = conf.getMinValue();
        }

        double pointsPerPixel = (maxValue - minValue) / height;

        Double plotValue = null;
        Double lastValue = null;
        long plotTime = -1;
        long lastTime = -1;

        int size = data.size();
        for (int i = 1; i < size; i++) {
            DataSet vehicle = data.get(i);
            DataSet prevVehicle = data.get(i-1);
            long time = vehicle.getTimestamp();
            Channel<?> channel = vehicle.getChannels().getById(id);
            Channel<?> prevChannel = prevVehicle.getChannels().getById(id);
            
            Double channelValue = channel.getValueAsDouble();
            Double channelPreviousValue = prevChannel.getValueAsDouble();

            /*
             * V1 Logic:
             * 
			 *  WHEN tb2.C_Sand > ' + @tableName + '.C_Sand + 65000
			 *	    THEN (' + @tableName + '.C_Sand + 65536 - tb2.C_Sand) * 0.666666666667
			 *      ELSE (' + @tableName + '.C_Sand - tb2.C_Sand) * 0.666666666667
			 *      
			 *  It has to be applied to all the counters, not only C_Sand
             */
            if (channel != null && prevChannel != null && channel.getValueAsDouble() != null && prevChannel.getValueAsDouble() != null){
                double value = channelValue - channelPreviousValue;
                if(channelPreviousValue > (channelValue + 65000)){
                    value += 65536;
                }

                // If there is more than one point-per-pixel, we will not
                // be able to see them on the chart, so we do some rounding
                // here to fix this.
                if (pointsPerPixel > 1) {
                    value = (int) (channelValue / pointsPerPixel) * pointsPerPixel;
                }

                // Don't add the value if it's the same as the previous
                // value and the gap between their times is short.
                if (plotValue == null || value != plotValue || (time - plotTime >= slotInMilliseconds)) {
                    // If the value has changed or there is a gap, we need to plot the end
                    // position of the previous value.
                    if ((lastTime != plotTime) || (lastTime - time >= slotInMilliseconds)) {
                        Millisecond millisecond = new Millisecond(new Date(lastTime), tz, locale);
                        if (dataset.getDataItem(millisecond) == null) {
                            dataset.add(millisecond, lastValue);
                            plotValue = value;
                            plotTime = time;
                        }
                    }

                    Millisecond millisecond = new Millisecond(new Date(time), tz, locale);
                    if (dataset.getDataItem(millisecond) == null) {
                        dataset.add(millisecond, value);
                        plotValue = value;
                        plotTime = time;
                    }
                }

                lastValue = value;
                lastTime = time;
            } else if (channel != null && prevChannel != null) {
                if (lastValue != null && lastTime != plotTime) {
                    Millisecond lstValMs = new Millisecond(new Date(lastTime), tz, locale);
                    if (dataset.getDataItem(lstValMs) == null) {
                        dataset.add(lstValMs, lastValue);
                    }
                    
                    Millisecond timeMs = new Millisecond(new Date(time), tz, locale);
                    if (dataset.getDataItem(timeMs) == null) {
                        dataset.add(timeMs, null);
                    }
                    plotTime = time;
                    plotValue = null;
                    lastValue = null;
                }
                
                lastTime = time;
            }   
        }
    if (plotTime < lastTime) {
            if (lastValue != null) {
                Millisecond timeMs = new Millisecond(new Date(lastTime), tz, locale);
                if (dataset.getDataItem(timeMs) == null) {
                    dataset.add(timeMs, lastValue);
                }
            }
        }   
    }

	private void plotData(ChannelConfig conf, List<DataSet> data, Locale locale, double height) {
        final int id = conf.getId();
        final TimeSeries dataset = channelIdToDataset.get(conf.getName());

        
        // Get the smallest plottable value, for example.. If the height
        // of the chart is 100px and the maximum value is 50. The smallest
        // visible difference will be 50 / 100 = 0.5 - so all values should
        // be rounded to 0.5

        double maxValue = conf.getType() == ChannelType.DIGITAL ? 1 : MAX_VALUE;
        double minValue = MIN_VALUE;

        if (conf.getMaxValue() != null) {
            maxValue = conf.getMaxValue();
        }

        if (conf.getMinValue() != null) {
            minValue = conf.getMinValue();
        }

        // double pointsPerPixel = 1; //(maxValue - minValue) /
        // analogueHeight;
        double pointsPerPixel = (maxValue - minValue) / height;

        Double plotValue = null;
        Double lastValue = null;
        long plotTime = -1;
        long lastTime = -1;

        // Plot the first value...
        int i = 0;
        int size = data.size();

        // Find and plot the first non-null value...
        for (i = 0; i < size; i++) {
            DataSet vehicle = data.get(i);
            long time = vehicle.getTimestamp();
            Channel<?> channel = vehicle.getChannels().getById(id);

            if (channel != null) {
                Double value = channel.getValueAsDouble();

                Millisecond timeMs = new Millisecond(new Date(time), tz, locale);
                if (dataset.getDataItem(timeMs) == null) {
                    if (pointsPerPixel <= 1 || value == null) {
                        dataset.add(timeMs, value);
                    } else {
                        dataset.add(new Millisecond(new Date(time), tz, locale), (int) (value / pointsPerPixel)
                                * pointsPerPixel);
                    }
                    
                }

                plotValue = value;
                lastValue = plotValue;
                lastTime = time;
                plotTime = time;
                break;
            }
        }

        while (++i < size) {
            final DataSet vehicle = data.get(i);
            final long time = vehicle.getTimestamp();
            final Channel<?> channel = vehicle.getChannels().getById(id);

            if (channel != null && channel.getValue() != null && channel.getValueAsDouble() != null ) {
                double channelValue = channel.getValueAsDouble();
                double rounded = channelValue;

                // If there is more than one point-per-pixel, we will not
                // be able to see them on the chart, so we do some rounding
                // here to fix this.
                if (pointsPerPixel > 1) {
                    rounded = (int) (channelValue * pointsPerPixel) / pointsPerPixel ;
                }

                // Don't add the value if it's the same as the previous
                // value and the gap between their times is short.
                if (plotValue == null || rounded != plotValue || (time - plotTime >= slotInMilliseconds)) {
                    // If the value has changed or there is a gap, we need to plot the end
                    // position of the previous value.
                    if ((lastTime != plotTime) || (lastTime - time >= slotInMilliseconds)) {
                    	Millisecond millisecond = new Millisecond(new Date(lastTime), tz, locale);
                    	if (dataset.getDataItem(millisecond) == null) {
                    		dataset.add(millisecond, lastValue);
                            plotValue = lastValue;
                            plotTime = lastTime;
                    	}
                    }

                    // TODO see the problem with addorupdate method
                    Millisecond millisecond = new Millisecond(new Date(time), tz, locale);
                    if (dataset.getDataItem(millisecond) == null) {
                        dataset.add(millisecond, rounded);
                        plotValue = rounded;
                        plotTime = time;
                    }
                }

                lastValue = rounded;
                lastTime = time;
            } else if (channel != null) {
                if (lastValue != null && lastTime != plotTime) {
                	Millisecond lstValMs = new Millisecond(new Date(lastTime), tz, locale);
                	if (dataset.getDataItem(lstValMs) == null) {
                		dataset.add(lstValMs, lastValue);
                	}
                    
                	Millisecond timeMs = new Millisecond(new Date(time), tz, locale);
                	if (dataset.getDataItem(timeMs) == null) {
                		dataset.add(timeMs, null);
                	}
                    plotTime = time;
                    plotValue = null;
                    lastValue = null;
                }

                lastTime = time;
            } 
        }

        // Ensure that we have plotted the last value for each channel,
        // This is necessary, because if the last value is the same as the
        // previous channels - we will not know when the value ended, this
        // could result in one or more records missing (depending on the
        // how many records are the same as the last one).
        if (plotTime < lastTime) {
            if (lastValue != null) {
                Millisecond timeMs = new Millisecond(new Date(lastTime), tz, locale);
                if (dataset.getDataItem(timeMs) == null) {
                    dataset.add(timeMs, lastValue);
                }
                
            }
        }
	}
	
}
