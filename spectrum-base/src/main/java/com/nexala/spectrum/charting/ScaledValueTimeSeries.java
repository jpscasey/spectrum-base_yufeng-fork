package com.nexala.spectrum.charting;

import org.apache.log4j.Logger;
import org.jfree.data.time.RegularTimePeriod;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesDataItem;

/**
 * A scaled timeserie. A scale is apply when plotting values.
 * @author brice
 *
 */
public class ScaledValueTimeSeries extends TimeSeries {

	private static final long serialVersionUID = 1L;
	
	private static final Logger LOGGER = Logger.getLogger(ScaledValueTimeSeries.class);
	
	public static final int OPERATION_MULTIPLY = 1;
    public static final int OPERATION_DIVIDE = 2;
    public static final int OPERATION_ADD = 3;
    public static final int OPERATION_SUBSTRACT = 4;

    private int operation = OPERATION_MULTIPLY;
    private double factor = 1;

    @SuppressWarnings("rawtypes")
	public ScaledValueTimeSeries(Comparable arg0) {
        super(arg0);
    }

    public ScaledValueTimeSeries(String arg0, int operation, double factor) {
        this(arg0);
        
        if (operation < 1 || operation > 4) {
            throw new IllegalArgumentException();
        }
        
        this.operation = operation;
        this.factor = factor;
    }

    @Override
    public void add(RegularTimePeriod period, double value) {
    	super.addOrUpdate(period, applyScale(value));
    }
    
    
    @Override
    public void add(RegularTimePeriod period, Number value) {
    	
    	super.addOrUpdate(period,value == null ? null : applyScale(value.doubleValue()));
    }

	/**
	 * Apply the scale to the value in parameter.
	 * @param value the value
	 * @return the scaled value
	 */
	private double applyScale(double value) {
		try {
            switch (operation) {
            case OPERATION_ADD:
                value += factor;
                break;
            case OPERATION_SUBSTRACT:
                value -= factor;
                break;
            case OPERATION_MULTIPLY:
                value *= factor;
                break;
            case OPERATION_DIVIDE:
                value /= factor;
                break;
            default:
                throw new IllegalArgumentException();
            }
        } catch (ArithmeticException e) {
            LOGGER.error("can't apply scale", e);
        }
		
		return value;
	}
}
