/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.view;

import javax.servlet.http.HttpServletRequest;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.nexala.spectrum.licensing.ActivityLogProvider;
import com.nexala.spectrum.licensing.Licence;
import com.nexala.spectrum.licensing.RestrictedResource;
import com.nexala.spectrum.view.conf.App;
import com.nexala.spectrum.view.conf.AppConfiguration;
import com.trimble.rail.security.core.RailsecConstants;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

@Singleton
public class UserAdminView extends ExternalAppTemplate {

    private static final long serialVersionUID = -7861702525454545759L;

    @Inject
    ActivityLogProvider logProvider;

    @Inject
    private AppConfiguration appConf;

    public App getView() {
        return appConf.getApp(Constants.USER_ADMINISTRATION);
    }
    
    /**
     * @see com.nexala.spectrum.view.ExternalAppTemplate#getExternalUrl()
     */
    @Override
    protected String getExternalUrl() {
        Config securityConf = ConfigFactory.load(RailsecConstants.SECURITY_CONF);
        return securityConf.getString(RailsecConstants.CONF_CLIENT_MANAGE_REALM_URI);
    }
    
    /**
     * @see com.nexala.spectrum.view.ExternalAppTemplate#hasAccess(com.nexala.spectrum.licensing.Licence)
     */
    @Override
    @RestrictedResource({ Constants.ADMINISTRATION_LIC_KEY, Constants.ADMINISTRATION_USERS_LIC_KEY })
    protected boolean hasAccess(Licence licence) {
        return true;
    }
    
    
    
    /**
     * @see com.nexala.spectrum.view.ExternalAppTemplate#logAccess(com.nexala.spectrum.licensing.ActivityLogProvider, javax.servlet.http.HttpServletRequest)
     */
    @Override
    protected void logAccess(ActivityLogProvider logProvider,
            HttpServletRequest request) {
        logProvider.logActivity(Constants.RESOURCE_ACCESS_LOG_TYPE,
                Constants.ADMINISTRATION_USERS_LIC_KEY, request);
    }

    /**
     * @see com.nexala.spectrum.view.ExternalAppTemplate#getScreenName()
     */
    @Override
    protected String getScreenName() {
        return "User Administration";
    }
}
