<html>
    <head>
        <title>Nexala R2M recover password</title>
        <meta http-equiv="X-UA-Compatible" content="IE=8" >
        
        <link rel="stylesheet" type="text/css" href="/css/nexala.lostPassword.css" />
    </head>
    
    <body class="loginpage">
        <div class="loginbox">
            <div class="loginform">
                <div class="logo"></div>
            
                <form name="spectrumLoginForm" method="POST" action="lostPassword">
                	<fieldset>
                		<span>${message}</span>
                	</fieldset>
                	<fieldset>
                    	<a href="/" class="lostPwdButton">Back</a>
                    </fieldset>
                </form>
            </div>
        </div>
    </body>
</html>