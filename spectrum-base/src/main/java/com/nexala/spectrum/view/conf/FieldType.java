package com.nexala.spectrum.view.conf;

public enum FieldType {
    LABEL,
    INPUT_TEXT,
    SIMPLE_TEXT,
    SELECT,
    MULTIPLE_SELECT,
    CHECKBOX,
    TEXT_AREA,
    INVISIBLE,
    INPUT_NUMBER,
    TABLE,
    AUTOCOMPLETE,
    DATE,
    TIME
}
