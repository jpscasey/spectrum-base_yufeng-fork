/*
 * Copyright (c) Nexala Technologies 2013, All rights reserved.
 */
package com.nexala.spectrum.view.conf;

import java.util.List;

import com.nexala.spectrum.db.beans.UserConfiguration;
import com.nexala.spectrum.licensing.Licence;
import com.nexala.spectrum.licensing.ScreenRequiresFeature;
import com.nexala.spectrum.rest.data.UserMapConfiguration;
import com.nexala.spectrum.rest.data.beans.UnitSummaryButton;
import com.nexala.spectrum.view.conf.maps.GenericMapConfiguration;
import com.nexala.spectrum.view.conf.maps.MapLicencedConfiguration;
import com.nexala.spectrum.view.conf.maps.MapMarkerConfiguration;
import com.nexala.spectrum.view.conf.maps.MapView;
import com.nexala.spectrum.view.conf.maps.TrainIcon;
import com.nexala.spectrum.view.conf.maps.TrainLabel;
import com.typesafe.config.Config;

public interface MapConfiguration extends GenericMapConfiguration,
        ScreenRequiresFeature, ScreenConfiguration {

    /**
     * Gets the default position for the map, e.g. Ireland.
     * 
     * @return
     */
    MapView getDefaultDetailView();
    
    /**
     * Return the marker configuration with this marker type.
     * @param markerType the type
     * @return the marker configuration with this marker type.
     */
    MapMarkerConfiguration getMarkerConfiguration(String markerType);
    
    /**
     * Add a marker configuration to this configuration.
     * @see MapMarkerConfiguration
     * @param markerConfiguration map marker configuration to add
     * @param config configuration of the marker grabbed from config file
     */
    void addMarkerConfiguration(MapMarkerConfiguration markerConfiguration, Config config);

    /**
     * Return true if full fleet names are shown in tabs, false if fleet codes
     * are shown.
     */
    boolean isShowFleetFullNames();
    
    /**
     * Return true if Clear and Apply buttons are shown in setting panel.
     */
    boolean isShowClearApplyButtons();
    
    /**
     * Return the list of details displayed for each train.
     */
    List<Field> getDetails();

    /**
     * Return the maximum time (milliseconds) of train inactivity after which
     * the train is not shown on the map.
     */
    Long getMaxInactiveTime();
    
    /**
     * Return offset of searchUnitPanel 
     */
    String getMapUnitSearchPanelOffset();
    

    /**
     * Return a list of icons displayed for trains according to their severity
     * status (i.e. warning, fault).
     */
    List<TrainIcon> getTrainIcons();

    /**
     * Return a list of buttons which redirect to Unit Summary tabs.
     */
    List<UnitSummaryButton> getButtons();

    /**
     * Return the configuration for trains label.
     */
    TrainLabel getTrainLabel();

    /**
     * Return an object containing the licenced configurations 
     * (stations, routes and named views).
     * @param licence the licence of the user who sent the request
     */
    MapLicencedConfiguration getMapLicencedObjects(Licence licence);
    
    /**
     * return the list of map filters
     * @return
     */
    List<MapFilter> getFilters();
    
    /**
     * Return the map configuration with the user config.
     * @return the map configuration with the user config.
     */
    UserMapConfiguration getUserMapConfiguration(Licence licence);
    
    /**
     * Return the user configuration.
     * @param licence the user licence
     * @return the user configuration
     */
    UserConfiguration getUserConfiguration(Licence licence);
    
}
