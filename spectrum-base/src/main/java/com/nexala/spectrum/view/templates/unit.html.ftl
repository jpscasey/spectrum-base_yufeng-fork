<#include "header.ftl"/>
    <div id = 'unitSummaryHeader'>
        <div id = 'left'>
        </div>
        <div id = 'right'>
            <div id = 'timeController' style = 'float:right'>
                <div id = 'playPauseButton'></div>
                <div id = 'timePicker' class = 'timePicker'>
                    <div class = "leftButton"><a>-</a></div>
                    <div class = "inputDiv"></div>
                    <div class = 'nowButton'><a data-i18n="LIVE"></a></div> 
                    <div class = "rightButton"><a>+</a></div>
                </div>
            </div>
        </div>
    </div>
    <#list screens as screen>
        <#if screen?? && (screen.screenType!"TAB") == "DIALOG">
            <div id = "us_dialog${screen.displayName?replace(" ", "")}">
                <!-- including ${screen.templateName} -->
            
                <#include "${screen.templateName}"/>
            </div>
        </#if>
    </#list>
    <div id = 'unitSummaryBody'>
        <div id="unitTabs" class = 'fullHeightTabs'>
            <ul>
                <#list screens as screen>
                    <#if screen?? && (screen.screenType!"TAB") == "TAB">
                        <li><a href = "#${screen.link!"us_tab${screen_index + 1}"}" data-i18n="${screen.displayName}"></a></li>
                    </#if>
                </#list>
            </ul>

            <#list screens as screen>
                <#if screen?? && (screen.screenType!"TAB") == "TAB">
                    <div id = "${screen.link!"us_tab${screen_index + 1}"}">
                        <!-- including ${screen.templateName} -->
                    
                        <#include "${screen.templateName}"/>
                    </div>
                </#if>
            </#list>
        </div>
    </div>
<#include "footer.ftl"/>