package com.nexala.spectrum.view.conf;

import java.util.List;
import java.util.Map;

import com.google.common.collect.ListMultimap;
import com.nexala.spectrum.db.beans.Fleet;
import com.nexala.spectrum.licensing.Licence;

/** Provides some basic configuration for fleet summary screen, for <em>all</em> fleets in the system */
public interface CommonFleetConfiguration extends ScreenConfiguration {

    boolean isShowSingleFleetTab();
    
    
    /**
     * Return a list of fleet. 
     * Index the fleet name by it id
     * @return list of fleet.
     */
    List<Fleet> getFleetList(Licence licence);

    /**
     * Return true if full fleet names are shown in tabs, false if fleet codes
     * are shown.
     */
    boolean isShowFleetFullNames();

    /**
     * @return list of fleets per database
     */
    public ListMultimap<String, String> getFleetsPerDatabase();
    
    List<Column> getColumns(String fleetId, Licence licence);
    
    List<Column> getColumns(String fleetId);

    List<FleetFilter> getFleetFilters(String fleetId);

    List<String> getColumnsUnitIdentifier(String fleetId);
    
    /**
     * Returns a map containing an entry per each fleet with the fleet Code and
     * an integer value which controls the order of the Fleet Summary tabs
     */
    Map<String, Integer> getFleetTabsOrdered();
    
    ColumnSorterBean getDefaultSortingColumnName(String fleetId, Licence licence);

    /**
     * Returns mouseOverHeaderEnabled config value from r2m.fleet.fleetsummary, true by default
     */
    boolean isMouseOverHeaderEnabled();
}
