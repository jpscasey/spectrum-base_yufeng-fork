/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.view.conf;

import java.util.HashMap;
import java.util.Map;

import com.nexala.spectrum.channel.ChannelCategory;

//TODO : add new ChannelCategory/ChannelStatus EVENT

public class ChannelDataGridStyle {
    public static final Map<ChannelCategory, String> CHANNELDATA_COMMON = new HashMap<ChannelCategory, String>() {
        private static final long serialVersionUID = 4493094068501372290L;
        {
            put(ChannelCategory.INVALID, "channeldata_purple_black");
            put(ChannelCategory.NODATA, "channeldata_white_white");
            put(ChannelCategory.NORMAL, "channeldata_green_white");
            put(ChannelCategory.ON, "channeldata_yellow_black");
            put(ChannelCategory.OFF, "channeldata_blue_black");
            put(ChannelCategory.WARNING, "channeldata_orange_white");
            put(ChannelCategory.WARNING1, "channeldata_orange_white");
            put(ChannelCategory.WARNING2, "channeldata_orange_white");
            put(ChannelCategory.FAULT, "channeldata_red_white");
            put(ChannelCategory.FAULT1, "channeldata_red_white");
            put(ChannelCategory.FAULT2, "channeldata_red_white");
        }
    };

    public static final Map<ChannelCategory, String> CHANNELDATA_CHANNELNAME = new HashMap<ChannelCategory, String>() {
        private static final long serialVersionUID = 3905701591325977178L;
        {
            put(ChannelCategory.NORMAL, "channeldata_white_black_left");
        }
    };
    
    public static final Map<ChannelCategory, String> CHANNELDATA_TIMESTAMP = new HashMap<ChannelCategory, String>() {
        private static final long serialVersionUID = 3905701591325937170L;
        {
            put(ChannelCategory.NORMAL, "channeldata_white_black");
        }
    };
}