/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.view.conf.unit;

import java.util.List;

import com.google.inject.ImplementedBy;
import com.nexala.spectrum.licensing.ScreenRequiresFeature;
import com.nexala.spectrum.view.conf.Column;
import com.nexala.spectrum.view.conf.Linkable;

@ImplementedBy(DefaultRecoveryConfiguration.class)
public interface RecoveryConfiguration extends Linkable, ScreenRequiresFeature {

    public List<Column> getFaultColumns();
}
