<!DOCTYPE html>
 <html>
    <head>
        <#if screenName??>
            <title>${title?string} - ${screenName?string}</title>   
        <#else>
            <title>${title?string}</title>
        </#if>
        
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=EDGE" >
        <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />

        <link rel = 'stylesheet' type = 'text/css' href = 'css/merge?resources=<#t>
            css/Aristo.css,<#t>
            css/uniform.aristo.css,<#t>
            css/spectrum.css,<#t>
            css/nexala.eventdetail.css,<#t>
            css/nexala.licence.css,<#t>
            css/nexala.unitselector.css,<#t>
            css/jquery.tabular.css&view=main<#t>
        '><#t>
        
        <script type = 'text/javascript' src = 'js/merge?resources=<#t>
            js/jstz.js,<#t>            
            js/nexala.hub.js,<#t>
            js/jquery-1.8.1.min.js,<#t>
            js/jquery-ui-1.8.23.custom.min.js,<#t>
            js/jquery.cookie.js,<#t>
            js/jquery.tinypubsub-0.7.js,<#t>
            js/nexala.spectrum.js,<#t>
            js/nexala.unit.stocktabs.js,<#t>
            js/nexala.util.js,<#t>
            js/nexala.data.js,<#t>
            js/nexala.eventDetail.js,<#t>
            js/nexala.eventChannelDetail.js,<#t>
            js/nexala.eventTableDetail.js,<#t>
            js/jquery.uniform.min.js,<#t>
            js/nexala.unitselector.js,<#t>
            js/jquery.form.js,<#t>
            js/jquery.format.js,<#t>
            js/jquery.textarea.js,<#t>
            js/jquery.tabular.js&view=main<#t>
        '></script><#t>
        
        <script charset="UTF-8" type = 'text/javascript' src = 'js/merge?resources=<#t>
            js/i18n/jquery.i18n.js,<#t>
            js/i18n/jquery.i18n.messagestore.js,<#t>
            js/i18n/jquery.i18n.fallbacks.js,<#t>
            js/i18n/jquery.i18n.parser.js,<#t>
            js/i18n/jquery.i18n.emitter.js,<#t>
            js/i18n/jquery.i18n.emitter.bidi.js,<#t>
            js/i18n/jquery.i18n.language.js,<#t>
            js/i18n/CLDRPluralRuleParser.js<#t>
            <#list translationFiles as translationFile>,${translationFile}</#list>&view=main<#t>
        '></script><#t>
            
        <script charset="UTF-8" type="text/javascript" src="//www.bing.com/api/maps/mapcontrol?callback=GetMap" async defer></script>

        <script type = "text/javascript">

        	var mapReady = $.Deferred();
        	function GetMap() {
        		mapReady.resolve();
    		}
        
            $(document).ready(function($) {
            
                 // handle ajax errors
                 $( document ).ajaxError(function( event, jqxhr, settings, exception ) {
                    if (jqxhr.status == 401) {
                        window.location.href = "/logout";
                    }
                 });
                
                $("select, input:checkbox, input:radio, input:file").uniform();
                
                $("div.multipleLink > a").click(function(event) {
                    event.preventDefault();
                    $(this).parent().children(".subMenu").slideToggle('up');
                });
                
                var timeoutInterval = ${timeoutInterval?c};
                if(!!timeoutInterval && parseInt(timeoutInterval)) {
                    setTimeout(function() {
                        window.location = "/logout.jsp"
                    }, parseInt(timeoutInterval)*1000);
                }
                
                $.i18n.debug = true;
                
                <#if userLanguage??>
                	$.i18n({locale: '${userLanguage}'});
                </#if>
                
                <#list languages as language>
                	<#list language.translationFiles as translationFile>
                		$.i18n().load(${translationFile.varName}, "${language.localCode}" );
                	</#list>
                </#list>
                
			    $('body').i18n();
			    
			    nx.main = new nx.Main();
		        
            });
        </script>
        
        <style type = 'text/css'>
			div.selector { font-size: 1em }
        </style>
        
        <#if head??>
            <#include "${head}"/>
        </#if>
        
        <#list includes as include>
            <#include "${include}"/>
        </#list>
    </head>
    
    <!--[if IE 8]>     <body class="ie8"> <![endif]-->  
    <!--[if IE 9]>     <body class="ie9"> <![endif]-->
    <!--[if !IE]><!--> <body><!--<![endif]-->
        <div id = 'unitSearchPanel'>
            <div class = 'peak-up'></div>
            <div class = 'unitSearchBody darkbg ui-corner-all'>
                <div class="unitSelector">
                    <input class="textBox" type="text">
                </div>
            </div>
        </div>
    
        <#if fs != true>
        
        
        <div id="header">
        <table style = 'width: 100%; height: 40px'>
        <tr>
            <td style = 'height: 100%; padding: 5px'>
                <img src = '${logo?string}' style = '${logoCss?string}'/>
            </td>
            <td style = 'text-align: right; height: 100%'>
                <div id = 'links' style = 'height: 100%'>
                    <#list menu as menuEntry>
                        <#if menuEntry.screens?size == 1>
                            <#assign app = menuEntry.screens[0]>
                            <#if app.external>
                                <#assign target = '_blank'>
                            <#else>
                                <#assign target = ''>
                            </#if>
                            <#if app.equals(active)>
                                <div class = 'link link-active'>
                                    <a href = '${app.path}' target = '${target}'><span data-i18n="${app.desc}"></span></a>
                                </div>
                            <#else>
                                <div class = 'link'>
                                    <a href = '${app.path}' target = '${target}' data-i18n="${app.desc}">
                                    <#if app.external>
                                    	<span class="ui-icon ui-icon-extlink"></span>
									</#if>
									</a>
                                </div>
                            </#if>
                        <#else>
                            <div class = 'link multipleLink'>
                                <a class="multipleLinkLabel" href = '#' data-i18n="${menuEntry.label}"></a>
                                <a class="multipleLinkIcon ui-icon ui-icon-triangle-1-s" href = '#'></a>
                                <div class="subMenu">
                                    <div class="peak-up"></div>
                                    <div class="subMenuBorder lightGreybg ">
                                        <div class="subMenuContent">
                                            <#list menuEntry.screens as app>
                                                <div class = 'link'>
                                                <#if app.external>
                                                    <#assign target = '_blank'>
                                                <#else> 
                                                    <#assign target = ''>
                                                </#if>
                                                <a href = '${app.path}' target = '${target}'><span data-i18n="${app.desc}"></span>
                                                <#if app.external>
                                                	<span class="ui-icon ui-icon-extlink"></span>
                                                </#if>
                                                </a>
                                                </div>
                                            </#list>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </#if>
                    </#list>
                   
                    <div id = 'helpButton'></div>
                    <div id = 'unitSearchButton'></div>
                    <div id = 'systemPanelButton'></div>
                </div>
            </td>
        </tr>
        </table>
        </div>
        <div id = "content">
            <div class = "eventContainer">
                <div style = 'position: relative; height: 100%'>
                    <div class='eventAccordion'>
                        
                    </div>
                </div>
            </div>
            <div id = "relativeContent">
        <#include "event_detail.ftl"/>
        </#if>
