/*
 * Copyright (c) Nexala Technologies 2013, All rights reserved.
 */
package com.nexala.spectrum.view.conf;

public enum ColumnType {
    /** Represents an analogue value in a table */
    ANALOGUE,
    DIGITAL,
    TEXT,
    
    /**
     * Represents a vehicle / train / unit number in a table. This is a text
     * column, this STOCK type is simply additional metadata to be used for
     * click handlers, etc.
     */
    STOCK,
    /** Represents a cumulative time expressed in hh:mm:ss */
    DURATION,
    GROUP,
    TIMESTAMP,
    CHECKBOX
}
