package com.nexala.spectrum.view.conf.unit;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.google.common.collect.Lists;
import com.google.inject.Inject;
import com.nexala.spectrum.configuration.ApplicationConfiguration;
import com.nexala.spectrum.view.Constants;
import com.nexala.spectrum.view.Plugin;
import com.nexala.spectrum.view.PluginFactory;
import com.nexala.spectrum.view.conf.Column;
import com.nexala.spectrum.view.conf.DataNavigationType;
import com.nexala.spectrum.view.conf.ScreenType;
import com.typesafe.config.Config;

public class DefaultRecoveryConfiguration implements RecoveryConfiguration {

    @Inject
    private PluginFactory factory; 
    
    private final List<Column> columns;
    
    @Inject
    public DefaultRecoveryConfiguration(ApplicationConfiguration appConfig) {
    	Config conf = appConfig.get().getConfig("r2m.recovery");
    	
    	this.columns = new ArrayList<Column>();
    	List<? extends Config> columns = conf.getConfigList("faultColumns");
    	
    	for (Config column : columns) {
            this.columns.add(appConfig.columnBuilder(column).build());
        }
    	
	}
    

    @Override
    public ScreenType getScreenType() {
        return ScreenType.TAB;
    }

    @Override
    public String getDisplayName() {
        return "Recovery";
    }

    @Override
    public String getTemplateName() {
        return "unit_recovery.html.ftl";
    }

    @Override
    public String getRequiredRole() {
        return Constants.UNIT_RECOVERY_LIC_KEY;
    }

    @Override
    public Plugin getParentPlugin() {
        return factory.create("nx.unit.Recovery",
        		Lists.newArrayList("js/nexala.unit.recovery.js"),
        		Lists.newArrayList("css/nexala.unit.recovery.css"));
    }

    @Override
    public List<Column> getFaultColumns() {
        return columns;
    }

    public DataNavigationType getDataNavigationType() {
        return DataNavigationType.TIME;
    }

    @Override
    public List<Plugin> getPlugins() {
    	
    	List<Plugin> result = new ArrayList<Plugin>();
    	
    	Plugin plugin = new Plugin("", Collections.<String> emptyList(),
                Collections.<String> emptyList());
    	
    	result.add(plugin);

        return result;
    }


    @Override
    public String getLink() {
        return "recoveryTab";
    }


    @Override
    public String getIconClass() {
        return "ui-icon-image";
    }
}
