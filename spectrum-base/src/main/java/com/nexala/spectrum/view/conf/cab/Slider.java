/*
 * Copyright (c) Nexala Technologies 2014, All rights reserved.
 *
 * $Author$
 * $Date$
 * $Revision$
 * $Source$
 */
package com.nexala.spectrum.view.conf.cab;

public class Slider extends Widget {

    private SliderSettings settings;

    public Slider(String domId, int x, int y, int h, int w, SliderSettings settings, String... channels) {
        super(domId, x, y, h, w, channels);
        this.settings = settings;
    }

    @Override
    public String getWidgetClassName() {
        return "nx.cab.Slider";
    }

    @Override
    public Settings getSettingsObject() {
        return settings;
    }

}
