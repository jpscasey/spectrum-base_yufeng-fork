package com.nexala.spectrum.view.conf;

/**
 * Bean holding a filterName, a operator, and a value
 * @author CIglesi
 *
 */
public class DisabledCondition {
    
    private String filterName;
    
    private String operator;
    
    private String value;

	public String getFilterName() {
		return filterName;
	}

	public void setFilterName(String filterName) {
		this.filterName = filterName;
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
