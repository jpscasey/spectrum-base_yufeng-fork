/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.view.conf.cab;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;


@JsonInclude(Include.NON_NULL)
public class ButtonSettings implements Settings {
    public static final String BLUE = "r#024cc4-#0a4b90";
    public static final String GREEN = "r#028262-#0a6f56";
    public static final String RED = "r#fd0302-#dc1616";
    public static final String YELLOW = "r#ff0-#c0c000";

    public static final String BLUE_GLOW = "#0198E1";
    public static final String GREEN_GLOW = "green";
    public static final String RED_GLOW = "red";
    public static final String YELLOW_GLOW = "yellow";
    
    private String fill;
    private String highlightFill;
    private String glowColor;
    
    public String getFill() {
        return fill;
    }

    public String getHighlightFill() {
        return highlightFill;
    }

    public ButtonSettings setFill(String fill) {
        this.fill = fill;
        return this;
    }

    public ButtonSettings setHighlightFill(String highlightFill) {
        this.highlightFill = highlightFill;
        return this;
    }
    
    public String getGlowColor() {
        return this.glowColor;
    }
    
    public ButtonSettings setGlowColor(String glowColor) {
        this.glowColor = glowColor;
        return this;
    }
}