/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.view;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.aopalliance.intercept.MethodInterceptor;
import org.apache.log4j.Logger;
import org.pac4j.j2e.filter.ApplicationLogoutFilter;

import com.google.inject.Singleton;
import com.google.inject.matcher.Matchers;
import com.google.inject.servlet.ServletModule;
import com.nexala.spectrum.licensing.DataLicensor;
import com.nexala.spectrum.licensing.RestrictedData;
import com.nexala.spectrum.licensing.RestrictedResource;
import com.nexala.spectrum.licensing.ServletLicenceResourceInterceptor;
import com.nexala.spectrum.view.conf.App;
import com.nexala.spectrum.view.conf.AppConfiguration;
import com.trimble.rail.security.servlet.filter.AuthenticationFilter;
import com.trimble.rail.security.servlet.filter.CallbackFilter;

public class SpectrumViewModule extends ServletModule {
    
    private static final Logger LOGGER = Logger.getLogger(SpectrumViewModule.class);
    
    private final AppConfiguration appConf;

    public SpectrumViewModule(AppConfiguration appConf) {
        this.appConf = appConf;
    }

    @Override
    protected void configureServlets() {
        bind(AuthenticationFilter.class).in(Singleton.class);
        Map<String, String> paramsAuth = new HashMap<>();
        paramsAuth.put("configFactory", "com.trimble.rail.security.core.ClientConfigFactory");
        filter("/*").through(AuthenticationFilter.class, paramsAuth);
        
        bind(CallbackFilter.class).in(Singleton.class);
        Map<String, String> paramsCallback = new HashMap<>();
        paramsCallback.put("configFactory", "com.trimble.rail.security.core.ClientConfigFactory");
        paramsCallback.put("defaultUrl", "/");
        paramsCallback.put("renewSession", "false");
        filter("/auth/sso.callback").through(CallbackFilter.class, paramsCallback);

        bind(ApplicationLogoutFilter.class).in(Singleton.class);
        Map<String, String> paramsLogout = new HashMap<>();
        paramsLogout.put("defaultUrl", "/");
        filter("/logout").through(ApplicationLogoutFilter.class, paramsLogout);
        
        // Resource Licence interceptor
        MethodInterceptor resourceInterceptor = new ServletLicenceResourceInterceptor();
        requestInjection(resourceInterceptor);
        bindInterceptor(Matchers.any(), Matchers.annotatedWith(RestrictedResource.class), resourceInterceptor);

        // Applications
        App defaultApp = appConf.getDefaultApp();
        //filter(defaultApp.getPath()).through(BeautifierFilter.class);
        Class<? extends TemplateServlet> view = resolveView(defaultApp.getDesc(), defaultApp.getView());
        if (view != null) {
            serve(defaultApp.getPath()).with(view);
            bind(view);
        }
        
        List<String> appPaths = new ArrayList<String>();
        for (App app : appConf.getApps()) {
            //filter("/" + app.getPath()).through(BeautifierFilter.class);
            if (!app.isExternal()) {
                LOGGER.info("Serving /" + app.getPath());
                view = resolveView(app.getDesc(), app.getView());
                if (view != null) {
                    serve("/" + app.getPath()).with(view);
                    appPaths.add("/" + app.getPath());
                    bind(view);
                }
            }
        }
        
        //filter("/*").through(DebuggingFilter.class);
        
        //filter("/").through(NoCacheFilter.class);
        
    }

    @SuppressWarnings("unchecked")
    private Class<? extends TemplateServlet> resolveView(String appName, String viewName) {
        
        Class<? extends TemplateServlet> view = null;

        if (appName == null || appName.isEmpty()) {
            LOGGER.error("The view name of the app " + appName + "is null or empty. The app will not be bound." );
        } else {
            try {
                Class<?> clazz = Class.forName(viewName);
                Object object = clazz.newInstance();
                
                if (object instanceof TemplateServlet) {
                    view = (Class<? extends TemplateServlet>) clazz;
                } else {
                    throw new IllegalArgumentException(viewName + " is not an instance of " + TemplateServlet.class.getName() 
                            + ". The app " + appName + " will not be bound.");
                }
            } catch (ClassNotFoundException ex) {
                LOGGER.error(viewName + " not found. The app " + appName + " will not be bound.");
            } catch (InstantiationException ex) {
                LOGGER.error(viewName + " is not default instantiable. The app " + appName + " will not be bound.");
            } catch (IllegalAccessException ex) {
                LOGGER.error(viewName + " is not default instantiable. The app " + appName + " will not be bound.");
            } catch (ClassCastException ex) {
                LOGGER.error(viewName + " cannot be cast to Class<? extends TemplateServlet>. The app " + appName 
                        + " will not be bound.");
            }
        }

        return view;
    }
}