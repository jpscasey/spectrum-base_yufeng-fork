/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.view.conf;

import java.util.List;

public interface ChannelDefinitionConfiguration extends ScreenConfiguration {
    
	List<Column> getColumns();
    
    List<Field> getDetails();
    
    boolean getNameAsValidationsSource();
}