<#include "header.ftl"/>

<div id="eventAnalysis">
    <div id="eventAnalysisTabs">
        <ul>
            <#list groupings as grouping>
                <li><a href="#eaTab_${grouping.code}" data-i18n="${grouping.label}"></a></li>
                <div id="eaTab_${grouping.code}"></div>
            </#list>
        </ul>
    </div>

    <div id="eaContent">
        <div id="eaReport">
            <div id="eaReportHeader">
                <div id="eaFilterButton"></div>
                <div id="eaSearchButton"></div>
                <div id="eaFilterLabel"></div>    
            </div>
            
            <div id="eaFilterDialog"></div>
            <button type="button" id="eaExpandButton" title="Expand"></button>
    
            <div id="eaReportContainer">
                <div id="eaReportContentPanel">
                    <div id="eaReportChartContent"></div>
                    <div id="eaReportMapContent"></div>
                </div>
                <div id="eaReportTablePanel"></div>
                <div id="eaReportDetailTablePanel"></div>
            </div>
        </div>
    </div>
</div>

<#include "footer.ftl"/>