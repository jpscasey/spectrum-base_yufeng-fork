/*
 * Copyright (c) Nexala Technologies 2013, All rights reserved.
 */
package com.nexala.spectrum.view.conf;

public class TimestampColumnBuilder extends ColumnBuilder {
    public TimestampColumnBuilder() {
        super();
        type(ColumnType.TIMESTAMP);
        format("{date}");
        width(100);
    }
}
