package com.nexala.spectrum.view.conf.maps;

import java.util.List;

import com.nexala.spectrum.view.conf.MouseOverText;

public class TrainLabel {

    private boolean pointer;

    private int xOffset;

    private int yOffset;
    
    private int width;
    
    private int height;

    private String title;
    
    private List<MouseOverText> description;

    public TrainLabel(boolean pointer, int xOffset, int yOffset, int width, int height) {
        this.pointer = pointer;
        this.xOffset = xOffset;
        this.yOffset = yOffset;
        this.width = width;
        this.height = height;
    }

    public boolean isPointer() {
        return pointer;
    }

    public int getxOffset() {
        return xOffset;
    }

    public int getyOffset() {
        return yOffset;
    }
    
    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<MouseOverText> getDescription() {
        return description;
    }

    public void setDescription(List<MouseOverText> description) {
        this.description = description;
    }
}
