/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.view.conf;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import com.google.common.collect.Iterables;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.configuration.ApplicationConfiguration;
import com.nexala.spectrum.i18n.ResourceBundleUTF8;
import com.nexala.spectrum.licensing.Licence;
import com.nexala.spectrum.rest.data.beans.Event;
import com.nexala.spectrum.rest.data.beans.EventDefinedFilterField;
import com.nexala.spectrum.view.Plugin;
import com.typesafe.config.Config;

@Singleton
public class DefaultEventConfiguration extends AbstractEventConfiguration {
    
    private ApplicationConfiguration config;

    private final List<Column> columns;

    private final List<Field> searchFields;
    
    private final boolean definedFiltersVisible;
    
    private final Map<String, List<EventDefinedFilterField>> definedFilters;
    
    private final boolean groupButtonVisible;
    
    private final boolean displayGroupedEventsInBlock;
    
    private final int defaultSortColumn;
    
    @Inject
    public DefaultEventConfiguration(ApplicationConfiguration applicationConfiguration) {

        this.config = applicationConfiguration;
        Config conf = applicationConfiguration.get();
        
        this.columns = new ArrayList<Column>();
        List<? extends Config> columns = conf.getConfigList("r2m.eventHistory.columns");

        for (Config column : columns) {
            this.columns.add(applicationConfiguration.columnBuilder(column).build());
        }
        
        this.searchFields = new ArrayList<Field>();
        List<? extends Config> fields = conf.getConfigList("r2m.eventHistory.filters");

        for (Config field : fields) {
            this.searchFields.add(config.buildField(field, 123));
        }
        
        this.definedFiltersVisible = conf.getBoolean("r2m.eventHistory.isDefinedFiltersVisible");
        
        this.defaultSortColumn=conf.getInt("r2m.eventHistory.defaultSortColumn");
        this.definedFilters = new HashMap<String, List<EventDefinedFilterField>>();
        List<? extends Config> filters = conf.getConfigList("r2m.eventHistory.definedFilters");
        
        for (Config filter : filters) {
        	this.definedFilters.put(filter.getString("name"), buildDefinedFilter(filter));
        }
        
        this.groupButtonVisible = conf.getBoolean("r2m.eventHistory.isGroupButtonVisible");

        this.displayGroupedEventsInBlock = conf.getBoolean("r2m.eventHistory.displayGroupedEventsInBlock");
        
    }

    @Override
    public List<Column> getColumns() {
        return columns;
    }

    @Override
    public String[] getHeaders(Licence licence) {
        List<String> headers = new ArrayList<String>();
        
        ResourceBundleUTF8 bundle = new ResourceBundleUTF8(licence.getUserLocale());
        
        for (Column column : columns) {
            if (column.isExportable()) {
                headers.add(bundle.getString(column.getHeader()));
            }
        }
        
        String [] result = Iterables.toArray(headers, String.class);
        
        return result;
    }

    @Override
    public String[] getRow(Event event, TimeZone clientTimezone) {

        List<String> values = new ArrayList<String>();

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");                 
        dateFormat.setTimeZone(clientTimezone);
        
        for (Column column : columns) {
            if (column.isExportable()) {
                Object value = event.getField(column.getName());
                String valueString = "";
                if (column.getType().equals("timestamp") && value != null) {
                    valueString = dateFormat.format((Long) value);
                } else if (value != null) {
                    valueString = value.toString();
                }
                values.add(valueString);
            }
        }
        
        String[] result = Iterables.toArray(values, String.class);
        
        return result;
    }

    @Override
    public ScreenType getScreenType() {
        return ScreenType.TAB;
    }

    @Override
    public List<Field> getAdditionalAdvancedSearchFields() {
        return searchFields;
    }
    
    // Map or bean?
    private List<EventDefinedFilterField> buildDefinedFilter(Config filter) {
    	List<EventDefinedFilterField> result = new ArrayList<EventDefinedFilterField>();
    	
    	List<? extends Config> fieldConfs = filter.getConfigList("searchFields");
    	for(Config conf : fieldConfs) {
    		EventDefinedFilterField field = new EventDefinedFilterField();
    		field.setName(conf.getString("name"));
    		field.setType(conf.getString("type"));
    		field.setValue(conf.getString("value"));
    		result.add(field);
    	}
    	
    	return result;
    }
    
    @Override
    public List<Plugin> getPlugins() {
        return config.getPlugins(Spectrum.EVENT_HISTORY_ID);
    }

    @Override
    public Plugin getParentPlugin() {
        return config.getParentPlugin(Spectrum.EVENT_HISTORY_ID);
    }

    @Override
	public boolean isDefinedFiltersVisible() {
		return definedFiltersVisible;
	}

	@Override
	public Map<String, List<EventDefinedFilterField>> getDefinedFilters() {
		return definedFilters;
	}
	
	@Override
	public boolean isGroupButtonVisible() {
	    return groupButtonVisible;
	}

	@Override
	public boolean isDisplayGroupsInBlock() {
		return displayGroupedEventsInBlock;
	}
	
	@Override
	public int getDefaultSortColumn() {
		return defaultSortColumn;
	}
	
}
