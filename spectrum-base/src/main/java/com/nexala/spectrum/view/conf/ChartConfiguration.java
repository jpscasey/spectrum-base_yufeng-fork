package com.nexala.spectrum.view.conf;

import java.util.List;
import java.util.Map;

public class ChartConfiguration {

    private Integer defaultPeriod;

    private Integer refreshPeriod;

    private List<Long> periods;
    
    private Boolean exportPDF;

    private Boolean refreshNotLive;
    
	private Boolean virtualScrollBar;
	
	private Boolean isSystemChartsIncluded;
	
	private Map<String, Boolean> chartEnabledByFleet;
	
	private LabelValue channelGroupLabelConfig;
	
	private LabelValue tableChannelLabelConfig;

    public Integer getDefaultPeriod() {
        return defaultPeriod;
    }

    public void setDefaultPeriod(Integer defaultPeriod) {
        this.defaultPeriod = defaultPeriod;
        }

    public Integer getRefreshPeriod() {
        return refreshPeriod;
    }

    public void setRefreshPeriod(Integer refreshPeriod) {
        this.refreshPeriod = refreshPeriod;
    }

    public List<Long> getPeriods() {
        return periods;
    }

    public void setPeriods(List<Long> periods) {
        this.periods = periods;
    }
    
    public Boolean getExportPDF() {
        return exportPDF;
    }
    
    public void setExportPDF(Boolean exportPDF) {
        this.exportPDF = exportPDF;
    }

    public Boolean getRefreshNotLive() {
        return refreshNotLive;
    }

    public void setRefreshNotLive(Boolean refreshNotLive) {
        this.refreshNotLive = refreshNotLive;
    }

	/**
	 * @return the virtualScrollBar
	 */
	public Boolean getVirtualScrollBar() {
		return virtualScrollBar;
	}

	/**
	 * @param virtualScrollBar the virtualScrollBar to set
	 */
	public void setVirtualScrollBar(Boolean virtualScrollBar) {
		this.virtualScrollBar = virtualScrollBar;
	}

	/**
	 * @return the isSystemChartsIncluded
	 */
	public Boolean getIsSystemChartsIncluded() {
		return isSystemChartsIncluded;
	}

	/**
	 * @param isSystemChartsIncluded the isSystemChartsIncluded to set
	 */
	public void setIsSystemChartsIncluded(Boolean isSystemChartsIncluded) {
		this.isSystemChartsIncluded = isSystemChartsIncluded;
	}

	/**
	 * @return the chartEnabledByFleet
	 */
	public Map<String, Boolean> getChartEnabledByFleet() {
		return chartEnabledByFleet;
	}

	/**
	 * @param chartEnabledByFleet the chartEnabledByFleet to set
	 */
	public void setChartEnabledByFleet(Map<String, Boolean> chartEnabledByFleet) {
		this.chartEnabledByFleet = chartEnabledByFleet;
	}

    public LabelValue getChannelGroupLabelConfig() {
        return channelGroupLabelConfig;
    }

    public void setChannelGroupLabelConfig(LabelValue channelGroupLabelConfig) {
        this.channelGroupLabelConfig = channelGroupLabelConfig;
    }
    
    public LabelValue getTableChannelLabelConfig() {
        return tableChannelLabelConfig;
    }

    public void setTableChannelLabelConfig(LabelValue tableChannelLabelConfig) {
        this.tableChannelLabelConfig = tableChannelLabelConfig;
    }

}
