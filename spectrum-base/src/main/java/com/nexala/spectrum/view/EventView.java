/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.view;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.common.collect.Lists;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.nexala.spectrum.licensing.Licence;
import com.nexala.spectrum.licensing.RestrictedResource;
import com.nexala.spectrum.view.conf.App;
import com.nexala.spectrum.view.conf.AppConfiguration;
import com.nexala.spectrum.view.conf.EventConfiguration;

@Singleton
public class EventView extends TemplateServlet {
    private static final long serialVersionUID = -2000059848090562917L;

    @Inject
    private EventConfiguration eventConfiguration;
    
    @Inject
    private AppConfiguration appConf;

    private void handleRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException{
        String unitNumber = request.getParameter("unitNumber");
        String location = request.getParameter("location");
        String vehicleNumber = request.getParameter("vehicleNumber");
        String headcode = request.getParameter("headcode");
        String description = request.getParameter("description");
        
        proceed(request, response, unitNumber, location, vehicleNumber, headcode, description);
    }
    
    private void proceed(HttpServletRequest request, HttpServletResponse response, String unitNumber, 
            String location, String vehicleNumber, String headcode, String description)
    throws ServletException{
        Licence licence = Licence.get(request, response, true);

        List<String> includes = new ArrayList<String>();
        Map<String, Object> rootMap = new HashMap<String, Object>();
        rootMap.put("head", "event_head.html.ftl");
        rootMap.put("cssIncludesList", Lists.partition(getCssIncludes(), 10));
        rootMap.put("jsIncludesList", Lists.partition(getJsIncludes(), 10));
        rootMap.put("conf", eventConfiguration);
        rootMap.put("includes", includes);
        rootMap.put("screenName", eventConfiguration.getScreenName());
        rootMap.put("unitNumber", unitNumber);
        rootMap.put("location", location);
        rootMap.put("vehicleNumber", vehicleNumber);
        rootMap.put("headcode", headcode);
        rootMap.put("description", description);
        doRender(request, response, "event.html.ftl", rootMap, licence);
    }

    @RestrictedResource(Constants.EVENT_LIC_KEY)
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException {
        handleRequest(request, response);
    }
    
    @RestrictedResource(Constants.EVENT_LIC_KEY)
    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException {
        handleRequest(request, response);
    }
    
    private List<String> getCssIncludes() {
        List<Plugin> plugins = eventConfiguration.getPlugins();
        
        List<String> cssIncludes = new ArrayList<String>();
        
        for (Plugin p : plugins) {
            cssIncludes.addAll(p.getStylesheets());
        }
        
        return new ArrayList<String>(cssIncludes);
    }
    
    private List<String> getJsIncludes() {
        List<Plugin> plugins = eventConfiguration.getPlugins();
        
        List<String> jsIncludes = new ArrayList<String>();
        
        for (Plugin p : plugins) {
            jsIncludes.addAll(p.getSources());
        }
        
        return new ArrayList<String>(jsIncludes);
    }
    
    public App getView() {
        return appConf.getApp(Constants.EVENT_HISTORY);
    }
}