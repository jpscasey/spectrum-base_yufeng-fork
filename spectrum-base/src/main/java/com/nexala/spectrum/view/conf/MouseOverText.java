/*
 * Copyright (c) Nexala Technologies 2016, All rights reserved.
 */
package com.nexala.spectrum.view.conf;

public class MouseOverText {

    private String label;

    private String value;

    private String format;

    private String constant;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public String getConstant() {
        return constant;
    }

    public void setConstant(String constant) {
        this.constant = constant;
    }
}
