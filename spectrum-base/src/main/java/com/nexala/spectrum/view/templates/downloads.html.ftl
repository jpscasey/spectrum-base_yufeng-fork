<#include "header.ftl"/>

<link rel = 'stylesheet' type = 'text/css' href = 'css/merge?resources=
    css/nexala.timepicker.css
    &view=download
'>

<script type = 'text/javascript' src = 'js/merge?resources=
    js/i18n/datepicker-nl.js,
    js/i18n/datepicker-en-GB.js,
    js/nexala.timepicker.js
    &view=download
'></script>

<div class="downloadView" >
    <div id="downloadHeader" class="downloadHeader ui-widget-header">
        <div style="display:block;">
        
            <label>Fleet</label>
            <select name="fleetField" id="fleetField" tabindex="1"></select>
        
            <label>Unit Number</label>
            <input name="unitNumberField" id="unitNumberField" type="text" class="searchInput80 ui-autocomplete-input" style="margin-left: 5px; width: 95px;" tabindex="2" autocomplete="off" role="textbox" aria-autocomplete="list" aria-haspopup="true">

    		<#if isVehicleSelector>
    
                <label class="labelSelect">Vehicle Number</label>
                <select name="vehicleNumberField" id="vehicleNumberField" class="searchInputSelect80" tabindex="3">
                        <option></option>
                </select>
            </#if>

            <label>From</label>
            <input id="startDateField" name="startDateField" class="downloadDate" type="text" tabindex="4">    

            <label>To</label>
            <input id="endDateField" name="endDateField" class="downloadDate" type="text" tabindex="5">

            <label class="labelSelect">Type</label>
                <select name="fileTypeField" id="fileTypeField" class="searchInput80" tabindex="6">
                    <option value=""></option> 
                </select>

            <div style="display:inline-block;" id="searchDownloadButton">
                <button class="ui-button ui-widget ui-state-default ui-corner-all" role="button" title="Search" aria-disabled="false" tabindex="7">
                    <span class="ui-button-text">Search</span>
                </button>
            </div>

            <div style="display:inline-block;" id="resetSearchDownloadButton">
                <button class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-primary" role="button" aria-disabled="false" title="Reset Search" tabindex="8">
                    <span class="ui-button-text">Reset</span>
                </button>
            </div>

            <div id="downloadCreate" class="downloadCreates">
                <div style="display:inline-block;">
                    <button type="button" id="createDialogDownloadButton" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-primary" title="Create Ad-Hoc Resquest" tabindex="9">
                    <span class="ui-button-text">Download Request</span>
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div id="createDownloadDialog">
        <div id="popupFilters">
        	<div class = "rightAligned">
            </div>
        </div>
    </div>
    <div id="downloadTable" >
        <table cellpadding="0" cellspacing="0" border="0" id = "events"></table>
    </div>
</div>
<#include "footer.ftl"/>