/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.view.conf.maps;

import java.util.List;

public class RouteTrack {
    private final List<Coordinate> points;

    public RouteTrack(List<Coordinate> points) {
        this.points = points;
    }

    public List<Coordinate> getPoints() {
        return points;
    }
}