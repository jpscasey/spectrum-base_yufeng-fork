/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.view.conf.xml;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.log4j.Logger;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

import com.nexala.spectrum.rest.service.ServletContextHolder;
import com.nexala.spectrum.view.conf.maps.Coordinate;
import com.nexala.spectrum.view.conf.maps.RouteTrack;

public class RouteXmlParser extends DefaultHandler {
    private static final Logger LOGGER = Logger.getLogger(RouteXmlParser.class);

    private static class RouteXmlParserHandler extends DefaultHandler {
        private static final String COORDINATES = "coordinates";

        private List<RouteTrack> routeElements = new ArrayList<RouteTrack>();
        private StringBuffer buffer = null;

        @Override
        public void characters(char ch[], int start, int length) {
            if (buffer != null) {
                buffer.append(new String(ch, start, length));
            }
        }

        @Override
        public void startElement(String uri, String localName, String qName,
                Attributes attributes) {
            if (qName.equals(COORDINATES)) {
                buffer = new StringBuffer();
            }
        }

        @Override
        public void endElement(String url, String localName, String qName) {
            if (buffer != null) {
                try {
                    List<Coordinate> coordinates = new ArrayList<Coordinate>();

                    String[] points = buffer.toString().split("\n");
                    for (String point : points) {
                        String[] tmp = point.split(",");

                        if (tmp.length >= 2) {
                            Double lon = Double.parseDouble(tmp[0]);
                            Double lat = Double.parseDouble(tmp[1]);

                            coordinates.add(new Coordinate(lat, lon));
                        }
                    }

                    routeElements.add(new RouteTrack(coordinates));
                } catch (Exception ex) {
                    LOGGER.warn("Error getting route segment");
                    ex.printStackTrace();
                }
            }

            buffer = null;
        }

        public List<RouteTrack> getRouteElements() {
            return routeElements;
        }
    }

    public static List<RouteTrack> parse(String xml) {
        try {
            SAXParser sp = SAXParserFactory.newInstance().newSAXParser();
            RouteXmlParserHandler handler = new RouteXmlParserHandler();
            ClassLoader cl = Thread.currentThread().getContextClassLoader();
            sp.parse(cl.getResourceAsStream(xml), handler);

            return handler.getRouteElements();
        } catch (Exception e) {
            LOGGER.warn("Error loading Routes file (" + xml + ")");
            e.printStackTrace();

            return new ArrayList<RouteTrack>();
        }
    }
}