package com.nexala.spectrum.view.conf.maps;

import java.util.List;

import com.google.inject.ImplementedBy;
import com.nexala.spectrum.licensing.Licence;
import com.nexala.spectrum.mapping.Cluster;
import com.nexala.spectrum.mapping.Coordinate;

/**
 * 
 * @author brice
 * 
 */
@ImplementedBy(DefaultMapMarkerProvider.class)
public interface MapMarkerProvider {

    /**
     * Return the list of markers in this area.
     * These data are used to render the marker and it popup.
     * Usually return an instance of {@link MapMarker} or {@link Cluster}
     * @see nexala.map.marker.js
     * 
     * @param upperLeft
     *            the north west corner
     * @param lowerRight
     *            the south east corner
     * @param zoom
     *            level of zoom
     * @param data
     *            additional data from the client side, could be null
     * @param markerType
     *            the type of marker (i.e. WSP)
     * @param licence
     *            the licence object    
     * @return list of markers in this area.
     */
    public List<?> getMarkerData(Coordinate upperLeft, Coordinate lowerRight,
            int zoom, Object data, String markerType, Licence licence);

    public List<String[]> generateExportReport(Coordinate topLeft, Coordinate bottomRight,
            List<Integer> categories, String markerType, Licence licence);
}
