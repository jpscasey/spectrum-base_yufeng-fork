<!-- Download view includes -->

<link rel = 'stylesheet' type = 'text/css' href = 'css/merge?resources=
	<#list cssIncludes as cssInclude>
        ${cssInclude},
    </#list>
    css/nexala.timepicker.css,
    css/nexala.download.css&view=downloads
'></script>

<script type = 'text/javascript' src = 'js/merge?resources=
	js/nexala.download.js,
	js/nexala.timepicker.js,
	<#list jsIncludes as jsInclude>
    	,${jsInclude}
    </#list>
    &view=events
'></script>

