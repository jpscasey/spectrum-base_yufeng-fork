/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.view.conf;

import java.util.List;

import com.nexala.spectrum.rest.data.beans.BeanField;

public interface DownloadConfiguration extends ScreenConfiguration {
    List<Column> getColumns();

    List<FileType> getFileTypes();
    
    List<BeanField> getFields();
    
    List<Field> getPopupFilters();
    
    List<FieldBuilder> getRuleEditorDRFilters();
    /**
     * Does the download screen have a vehicle selector
     * @return true if the download screen have a vehicle selector, false otherwise
     */
    boolean isVehicleSelector();
    
    boolean isRetrievable();
    
    int getThresholdTime();
    
    int getGapTime();
}