/*
 * Copyright (c) Nexala Technologies 2013, All rights reserved.
 */
package com.nexala.spectrum.view.conf;

import com.nexala.spectrum.licensing.ScreenRequiresFeature;
import com.nexala.spectrum.view.Plugin;

public interface DetailScreen extends ScreenRequiresFeature, HasPlugins {
    /**
     * Returns the name of the screen. This is the named, which should be displayed in the unit summary tab, e.g.
     * "Data Plots". This value should be localised
     */
    public String getDisplayName();

    /**
     * Returns the name of the template, representing this screen, e.g. unit_cab.html.ftl
     * 
     * @return
     */
    public String getTemplateName();


    /**
     * Returns the main plugin, which represents the Screen.
     * 
     * @return
     */
    public Plugin getParentPlugin();

    
    /**
     * Returns the type of this Screen.
     * @return
     */
    public ScreenType getScreenType();
    
    /**
     * Returns the navigation type between data, by time or by record
     * @return
     */
    public DataNavigationType getDataNavigationType();
}
