package com.nexala.spectrum.view.conf.maps;

import java.util.List;

public class MapMarkerLegend {

    private String id;
    
    private String label;

    private boolean exportable;

    private List<MapMarkerLegendOption> options;

    public MapMarkerLegend(String id, String label, boolean exportable, List<MapMarkerLegendOption> options) {
        this.id = id;
        this.label = label;
        this.exportable = exportable;
        this.options = options;
    }
    
    public String getId() {
        return id;
    }

    public String getLabel() {
        return label;
    }

    public boolean isExportable() {
        return exportable;
    }

    public List<MapMarkerLegendOption> getOptions() {
        return options;
    }

}
