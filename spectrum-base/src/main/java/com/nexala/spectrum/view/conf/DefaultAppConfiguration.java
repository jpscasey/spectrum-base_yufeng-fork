/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.view.conf;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.configuration.ApplicationConfiguration;
import com.nexala.spectrum.configuration.ConfigurationManager;
import com.nexala.spectrum.licensing.Licence;
import com.nexala.spectrum.view.Constants;
import com.nexala.spectrum.view.conf.i18n.Language;
import com.nexala.spectrum.view.conf.i18n.TranslationFile;
import com.typesafe.config.Config;

public class DefaultAppConfiguration implements AppConfiguration {

    @Inject
    private ConfigurationManager configManager;

    @Inject
    ApplicationConfiguration config;

    private static final App DEFAULT = new App("Default",
            Constants.DEFAULT_VIEW, "com.nexala.spectrum.view.DefaultView", 0,
            false, "", false, true, (String[]) null);

    private static final Comparator<MenuEntry> SORT_MENU_BY_POSITION = new Comparator<MenuEntry>() {
        @Override
        public int compare(MenuEntry item1, MenuEntry item2) {
            return (int) (item1.getScreens().get(0).getPosition() - item2
                    .getScreens().get(0).getPosition());
        }
    };

    @Override
    public App getDefaultApp() {
        return DEFAULT;
    }

    /**
     * Returns a list of all screens, which are applicable to the licence. Note
     * that this method calls {@link #getApps()}
     */
    @Override
    public final App[] getApps(Licence licence) {
        LicencePredicate predicate = new LicencePredicate(licence);
        ArrayList<App> apps = Lists.newArrayList(getApps());
        Iterable<App> filtered = Iterables.filter(apps, predicate);
        return Iterables.toArray(filtered, App.class);
    }

    /**
     * @see com.nexala.spectrum.view.conf.AppConfiguration#getMenu(com.nexala.spectrum.licensing.Licence)
     */
    @Override
    public final List<MenuEntry> getMenu(Licence licence) {
        List<MenuEntry> result = new ArrayList<MenuEntry>();

        for (MenuEntry current : getMenu()) {
            List<App> screens = current.getScreens(licence);

            if (screens.size() == 1) {
                App app = screens.get(0);
                result.add(new MenuEntry(app));
            } else if (screens.size() > 1) {
                App[] apps = screens.toArray(new App[screens.size()]);
                result.add(new MenuEntry(current.getLabel(), apps));
            }
        }

        return result;
    }

    /**
     * @see com.nexala.spectrum.view.conf.AppConfiguration#getBranding()
     */
    @Override
    public String getBranding() {
        return config.get().getString("r2m.branding");
    }
    
    /**
     * @see com.nexala.spectrum.view.conf.AppConfiguration#getFooter()
     */
    @Override
    public String getFooter() {
        return config.get().getString("r2m.footer");
    }
    
    /**
     * @see com.nexala.spectrum.view.conf.AppConfiguration#getBrandingStyle()
     */
    @Override
    public String getBrandingStyle() {
        return config.get().getString("r2m.brandingStyle");
    }

    /**
     * @see com.nexala.spectrum.view.conf.AppConfiguration#getTitle()
     */
    @Override
    public String getTitle() {
        return "Nexala R2M";
    }

    /**
     * @see com.nexala.spectrum.view.conf.AppConfiguration#getTimeoutInterval()
     */
    @Override
    public Integer getTimeoutInterval() {
        return configManager.getConfAsInteger(
                Spectrum.SPECTRUM_APPLICATON_TIMEOUT_INTERVAL_IN_SECONDS, 0);
    }

    /**
     * @see com.nexala.spectrum.view.conf.AppConfiguration#getTimeoutInterval()
     */
    @Override
    public App[] getApps() {

        Config conf = config.get();
        List<? extends Config> screens = conf.getConfigList("r2m.screens");
        List<App> apps = new ArrayList<App>();

        for (Config screen : screens) {
            List<String> licences = screen.getStringList("licence");

            apps.add(new App(
                    screen.getString("name"),
                    screen.getString("path"),
                    screen.getString("view"),
                    screen.getInt("position"),
                    screen.getBoolean("grouped"),
                    screen.getString("groupName"),
                    screen.getBoolean("external"),
                    screen.getBoolean("visible"),
                    licences.toArray(new String[licences.size()])));
        }

        return apps.toArray(new App[apps.size()]);
    }
    
    @Override
    public List<MenuEntry> getMenu() {
        List<MenuEntry> result = new ArrayList<MenuEntry>();

        Map<String, List<App>> groupedApps = new HashMap<String, List<App>>();

        for (App entry : getApps()) {
            if (entry.isGrouped()) {
                if (!groupedApps.containsKey(entry.getGroupName())) {
                    List<App> apps = new ArrayList<App>();
                    apps.add(entry);
                    groupedApps.put(entry.getGroupName(), apps);
                } else {
                    List<App> apps = groupedApps.get(entry.getGroupName());
                    apps.add(entry);
                    groupedApps.put(entry.getGroupName(), apps);
                }
            } else {
                result.add(new MenuEntry(entry));
            }
        }

        for (String group : groupedApps.keySet()) {
            List<App> items = groupedApps.get(group);
            result.add(new MenuEntry(group, items.toArray(new App[items.size()])));
        }

        Collections.sort(result, SORT_MENU_BY_POSITION);

        return result;
    }

    @Override
    public App getApp(String name) {
        
        for (App app : getApps()) {
            if (app.getDesc().equals(name)) {
                return app;
            }
        }
        
        return DEFAULT;
    }

    @Override
    public List<Language> getLanguages() {
        List<Language> result = new ArrayList<>();

        Config conf = config.get();
        if (conf.hasPath("r2m.translation.languages")) {
            List<? extends Config> languages = conf.getConfigList("r2m.translation.languages");
            for (Config language : languages) {
                Language current = new Language();
                current.setLocalCode(language.getString("locale"));

                List<TranslationFile> lstFiles = new ArrayList<>();
                List<? extends Config> files = language.getConfigList("files");
                for (Config file : files) {
                    TranslationFile translationFile = new TranslationFile();
                    translationFile.setPath(file.getString("path"));
                    translationFile.setVarName(file.getString("varName"));

                    lstFiles.add(translationFile);
                }
                current.setTranslationFiles(lstFiles);

                result.add(current);
            }
        }

        return result;
    }
    
    @Override
    public ExportConfiguration getExportConfig() {
    	
    	ExportConfiguration result = new ExportConfiguration();
    	Config conf = config.get();
    	if (conf.hasPath("r2m.export")) {
    		Config exportConf = conf.getConfig("r2m.export");
    		
    		if (exportConf.hasPath("separator")) {
    			result.setSeparator(exportConf.getString("separator").charAt(0));
    		}
    		
    		if (exportConf.hasPath("encoding")) {
    			result.setEncoding(exportConf.getString("encoding"));
    		}
    		
    		if (exportConf.hasPath("fileExtension")) {
    			result.setFileExtension(exportConf.getString("fileExtension"));
    		}
    	}
    	
    	return result;
    }
    
    @Override
    public Map<String, String> getHelpConfig() {
    	Config conf = config.get();
    	Map<String, String> object = new HashMap<String, String>();
    	
    	String helpPath = conf.getString("r2m.helpDocumentPath");
    	object.put("helpDocumentPath", helpPath);
        
    	String eventDetail = conf.getString("r2m.eventDetail.help");
    	object.put("eventDetail", eventDetail);
    	
    	List<? extends Config> screens = conf.getConfigList("r2m.screens");
        for (Config screen : screens) {
        	object.put(screen.getString("path"), screen.getString("help"));
        }
        return object;
    }
    
    @Override
    public String getChannelLabelConfig() {
    	String channelLabelConfig = "description";
        if(config.get().hasPath("r2m.dataView.chartChannelLabel")) {
            channelLabelConfig = config.get().getString("r2m.dataView.chartChannelLabel");
        }
        return channelLabelConfig;
    }
}