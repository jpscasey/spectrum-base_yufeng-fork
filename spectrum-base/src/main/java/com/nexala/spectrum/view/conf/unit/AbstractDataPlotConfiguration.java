/*
 * Copyright (c) Nexala Technologies 2013, All rights reserved.
 */
package com.nexala.spectrum.view.conf.unit;

import com.google.common.collect.Lists;
import com.google.inject.Inject;
import com.nexala.spectrum.view.Constants;
import com.nexala.spectrum.view.Plugin;
import com.nexala.spectrum.view.PluginFactory;
import com.nexala.spectrum.view.conf.AbstractDetailScreen;
import com.nexala.spectrum.view.conf.Linkable;

public abstract class AbstractDataPlotConfiguration extends AbstractDetailScreen implements DataPlotConfiguration, Linkable {
    
    @Inject
    private PluginFactory factory;
    
    public String getRequiredRole() {
        return Constants.UNIT_DATAPLOTS_LIC_KEY;
    }

    @Override
    public String getDisplayName() {
        return "Data View";
    }

    @Override
    public String getTemplateName() {
        return "unit_dataview.html.ftl";
    }

    @Override
    public Plugin getParentPlugin() {
        return factory.create(
                "nx.unit.DataPlots",
                Lists.newArrayList(
                        "js/jquery.fadeto.js",
                        "js/nexala.dataplots.js",
                        "js/jquery.mousewheel.js",
                        "js/jstz.js"),
                Lists.newArrayList(
                        "css/nexala.dataplots.css"));
    }
    
    @Override
    public String getLink() {
        return "dataViewTab";
    }

    @Override
    public String getIconClass() {
        return "ui-icon-image";
    }
}
