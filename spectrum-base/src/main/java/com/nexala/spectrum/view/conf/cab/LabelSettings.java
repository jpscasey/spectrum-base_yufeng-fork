/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.view.conf.cab;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class LabelSettings {
    private String anchor;
    private String color;
    private Double fontSize;
    private String fontWeight;
    private Double position;
    private Double radius;
    private String text;

    public String getAnchor() {
        return anchor;
    }

    public String getColor() {
        return color;
    }

    public Double getFontSize() {
        return fontSize;
    }

    public String getFontWeight() {
        return fontWeight;
    }

    public Double getPosition() {
        return position;
    }

    public Double getRadius() {
        return radius;
    }

    public String getText() {
        return text;
    }

    public LabelSettings setAnchor(String anchor) {
        this.anchor = anchor;
        return this;
    }

    public LabelSettings setColor(String color) {
        this.color = color;
        return this;
    }

    public LabelSettings setFontSize(Double fontSize) {
        this.fontSize = fontSize;
        return this;
    }

    public LabelSettings setFontWeight(String fontWeight) {
        this.fontWeight = fontWeight;
        return this;
    }

    public LabelSettings setPosition(Double position) {
        this.position = position;
        return this;
    }
    
    public LabelSettings setRadius(Double radius) {
        this.radius = radius;
        return this;
    }
    
    public LabelSettings setText(String text) {
        this.text = text;
        return this;
    }
}