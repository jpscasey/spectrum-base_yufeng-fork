/*
 * Copyright (c) Nexala Technologies 2016, All rights reserved.
 *
 * $Author$
 * $Date$
 * $Revision$
 * $Source$
 */
package com.nexala.spectrum.view.conf;

import java.util.ArrayList;
import java.util.List;

import com.google.inject.Inject;
import com.nexala.spectrum.configuration.ApplicationConfiguration;
import com.nexala.spectrum.rest.data.beans.BeanField;
import com.nexala.spectrum.rest.service.bean.EventFilterBean;
import com.nexala.spectrum.rest.service.bean.EventPanelBean;
import com.typesafe.config.Config;

public class DefaultCurrentEventsConfiguration implements CurrentEventsConfiguration {

    private final ApplicationConfiguration appConf;
    
    private final List<Column> summaryColumns;
    
    private final List<MouseOverText> rowMouseOver;

    private final Boolean createFault;
    
    private final Boolean createFaultRecoverButton;
    
    private final Boolean eventPanelsOnBottom;
    
    private final Boolean alwaysDisplayLiveCount;
    
    private final List<EventFilterBean> createFaultPopupFields;

    private final Integer descriptionMaxCharacters;
    
    private final Integer eventsLimit;

    private final Boolean fleetBottom;

    private final List<Column> fleetSummaryColumns;

    private final List<MouseOverText> fleetRowMouseOver;
    
    private final Boolean fleetCreateFault;
    
    private final List<EventPanelBean> eventPanels;
    
    private final List<BeanField> searchFields;

    @Inject
    public DefaultCurrentEventsConfiguration (ApplicationConfiguration applicationConfiguration) {
        
        this.appConf = applicationConfiguration;
        Config conf = applicationConfiguration.get();

        // Live and Recent Events configuration
        this.summaryColumns = new ArrayList<Column>();
        List<? extends Config> summaryColumns = conf.getConfigList("r2m.liveAndRecentPanel.columns");

        for (Config summaryColumn : summaryColumns) {
            this.summaryColumns.add(applicationConfiguration.columnBuilder(summaryColumn).build());
        }
        
        this.rowMouseOver = new ArrayList<MouseOverText>();
        List<? extends Config> mouseOverLines = conf.getConfigList("r2m.liveAndRecentPanel.rowMouseOver");
        
        for (Config line : mouseOverLines) {
            this.rowMouseOver.add(applicationConfiguration.buildMouseOver(line));
        }
        
        this.createFault = conf.getBoolean("r2m.liveAndRecentPanel.createFaultEnabled");
        this.createFaultRecoverButton = conf.getBoolean("r2m.liveAndRecentPanel.createFaultRecoverButton");
        this.eventPanelsOnBottom = conf.getBoolean("r2m.liveAndRecentPanel.eventPanelsOnBottom");
        
        this.alwaysDisplayLiveCount = conf.getBoolean("r2m.liveAndRecentPanel.alwaysDisplayLiveCount");
        
        this.createFaultPopupFields = new ArrayList<EventFilterBean>();
        List<String> inputFieldNames = new ArrayList<String>();
        List<? extends Config> popupFields = conf.getConfigList("r2m.liveAndRecentPanel.createPopupFields");
        
        for(Config inputField : popupFields) {
        	EventFilterBean inputBean = applicationConfiguration.buildEventFilter(inputField);
        	
        	this.createFaultPopupFields.add(inputBean);
        	inputFieldNames.add(inputBean.getName());
        }
        
        List<? extends Config> searchConfig = conf.getConfigList("r2m.liveAndRecentPanel.createFaultSearch");

        this.searchFields = new ArrayList<BeanField>();
        for (Config field : searchConfig) {
            BeanField fields = new BeanField();

            fields.setId(field.getString("name"));
            fields.setDbName(field.getString("db"));
            fields.setType(field.getString("type"));

            this.searchFields.add(fields);
        }
        
        this.descriptionMaxCharacters = conf.getInt("r2m.liveAndRecentPanel.descMaxCharacters");
        
        this.eventsLimit = conf.getInt("r2m.liveAndRecentPanel.eventsLimit");
        
        List<? extends Config> panelConfigs = conf.getConfigList("r2m.liveAndRecentPanel.panels");
        this.eventPanels = new ArrayList<EventPanelBean>();
        
        for(Config panelConfig : panelConfigs) {
        	EventPanelBean panelBean = buildPanel(panelConfig);
        	
        	this.eventPanels.add(panelBean);
        }

        // Live and Recent Events configuration for Fleet Summary
        Config fleetConf = conf.getConfig("r2m.liveAndRecentPanel.fleetSummary");

        this.fleetBottom = fleetConf.getBoolean("bottom");

        if (fleetConf.hasPath("columns")) {
            this.fleetSummaryColumns = new ArrayList<Column>();
            List<? extends Config> fleetSummaryColumns = fleetConf.getConfigList("columns");

            for (Config column : fleetSummaryColumns) {
                this.fleetSummaryColumns.add(applicationConfiguration.columnBuilder(column).build());
            }
        } else {
            this.fleetSummaryColumns = this.summaryColumns;
        }
        
        if (fleetConf.hasPath("rowMouseOver")) {
            this.fleetRowMouseOver = new ArrayList<MouseOverText>();
            List<? extends Config> fleetMouseOverLines = fleetConf.getConfigList("rowMouseOver");
            
            for (Config line : fleetMouseOverLines) {
                this.fleetRowMouseOver.add(applicationConfiguration.buildMouseOver(line));
            }
        } else {
            this.fleetRowMouseOver = this.rowMouseOver;
        }

        if (fleetConf.hasPath("createFaultEnabled")) {
            this.fleetCreateFault = fleetConf.getBoolean("createFaultEnabled");
        } else {
            this.fleetCreateFault = this.createFault;
        }
        
    }
    
    @Override
    public CurrentEventsPanel getConfig(Boolean fleetSummary) {
        
        List<Column> columns = new ArrayList<Column>();
        List<EventFilterBean> filters = new ArrayList<EventFilterBean>();
        List<MouseOverText> rowMouseOver = new ArrayList<MouseOverText>();
        Boolean bottom = false;
        Boolean createFault = null;
        Boolean createFaultRecoverButton = null;
        Boolean eventPanelsOnBottom = null;
        Boolean alwaysDisplayLiveCount = this.alwaysDisplayLiveCount;
        List<EventFilterBean> createFaultInput = new ArrayList<EventFilterBean>();
        List<EventPanelBean> eventPanels = new ArrayList<EventPanelBean>();
        
        if (fleetSummary) {
            columns = this.fleetSummaryColumns;
            rowMouseOver = this.fleetRowMouseOver;
            bottom = this.fleetBottom;
            createFault = this.fleetCreateFault;
            createFaultRecoverButton = this.createFaultRecoverButton;
            eventPanelsOnBottom = this.eventPanelsOnBottom;
            createFaultInput = this.createFaultPopupFields;
            eventPanels = this.eventPanels;
        } else {
            columns = this.summaryColumns;
            rowMouseOver = this.rowMouseOver;
            createFault = this.createFault;
            createFaultRecoverButton = this.createFaultRecoverButton;
            createFaultInput = this.createFaultPopupFields;
            eventPanels = this.eventPanels;
        }
        
        return new CurrentEventsPanel(columns, filters, rowMouseOver, bottom,
                this.descriptionMaxCharacters, createFault, createFaultRecoverButton, eventPanelsOnBottom, createFaultInput, eventPanels, alwaysDisplayLiveCount);
    }

    @Override
    public Integer getEventsLimit() {
        return eventsLimit;
    }
    
    private EventPanelBean buildPanel(Config panelConfig) {
    	EventPanelBean result = new EventPanelBean();
    	
    	if (panelConfig.hasPath("name")) {
    		result.setName(panelConfig.getString("name"));
    	}
    	
    	if (panelConfig.hasPath("label")) {
    		result.setLabel(panelConfig.getString("label"));
    	}
    	
    	if (panelConfig.hasPath("criteria")) {
    		List<EventFilterBean> panelCriteria = new ArrayList<EventFilterBean>();
    		List<? extends Config> panelCriteriaConf = panelConfig.getConfigList("criteria");
    		
    		for (Config criteria : panelCriteriaConf) {
    			panelCriteria.add(this.appConf.buildEventFilter(criteria));
    		}
    		
    		result.setCriteriaList(panelCriteria);
    	}
    	
    	return result;
    }

	public List<BeanField> getSearchFields() {
		return searchFields;
	}

}
