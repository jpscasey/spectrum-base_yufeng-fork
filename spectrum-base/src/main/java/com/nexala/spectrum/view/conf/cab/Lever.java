/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.view.conf.cab;

public class Lever extends Widget {
    private LeverSettings settings;

    public Lever(String domId, int x, int y, int h, int w,
            LeverSettings settings, String... channels) {
        super(domId, x, y, h, w, channels);
        this.settings = settings;
    }

    public String getWidgetClassName() {
        return "nx.cab.Lever";
    }

    public Settings getSettingsObject() {
        return settings;
    }
}