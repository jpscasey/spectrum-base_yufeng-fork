/*
 * Copyright (c) Nexala Technologies 2013, All rights reserved.
 */
package com.nexala.spectrum.view;

import java.util.List;

import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;

public class Plugin {
    private String pluginClass;
    private List<String> sources;
    private List<String> stylesheets;
    
    @Inject
    public Plugin(
            @Assisted(value = "pluginClass") String pluginClass,
            @Assisted(value = "source") List<String> sources,
            @Assisted(value = "stylesheet") List<String> stylesheets) {
        
        this.pluginClass = pluginClass;
        this.sources = sources;
        this.stylesheets = stylesheets;
    }

    public String getPluginClass() {
        return pluginClass;
    }

    public List<String> getSources() {
        return sources;
    }

    public List<String> getStylesheets() {
        return stylesheets;
    }
}
