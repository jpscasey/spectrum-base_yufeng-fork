/*
 * Copyright (c) Nexala Technologies 2016, All rights reserved.
 */
package com.nexala.spectrum.view.conf;

import com.google.inject.ImplementedBy;

/**
 * Configuration for Live & Recent Events panel.
 */
@ImplementedBy(DefaultCurrentEventsConfiguration.class)
public interface CurrentEventsConfiguration {

    /**
     * Returns the configuration for Live and Recent events panel.
     * @param fleetSummary, true if the request comes from FleetSummary screen.
     */
    CurrentEventsPanel getConfig(Boolean fleetSummary);

    /**
     * Return the maximum number of events displayed per fleet.
     */
    Integer getEventsLimit();
}
