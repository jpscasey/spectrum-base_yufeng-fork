/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.view.conf.unit;

import java.util.List;
import java.util.Map;

import com.google.inject.ImplementedBy;
import com.nexala.spectrum.db.beans.Unit;
import com.nexala.spectrum.licensing.ScreenRequiresFeature;
import com.nexala.spectrum.rest.data.beans.BeanField;
import com.nexala.spectrum.rest.data.beans.StockDesc;
import com.nexala.spectrum.rest.service.bean.UnitDetailRow;
import com.nexala.spectrum.view.conf.DetailScreen;
import com.nexala.spectrum.view.conf.Field;

@ImplementedBy(DefaultUnitDetailConfiguration.class)
public interface UnitDetailConfiguration extends DetailScreen, ScreenRequiresFeature {

    List<StockDesc> getTabs(Unit unit);
    
    int getNumberOfHeaderColumns();

    List<Field> getHeaderFields();
    
    List<UnitDetailRow> getUdPanelRows();
    
    List<BeanField> getWorkOrderFields();

    boolean isShowEventHistoryButton();
    
    String getDefaultStockTabName();
    
    String getDefaultStockTabType();
    
    String getStockTabsType();

    List<Map<String, String>> getMaximoUrls();

    List<BeanField> getRestrictionFields();
}
