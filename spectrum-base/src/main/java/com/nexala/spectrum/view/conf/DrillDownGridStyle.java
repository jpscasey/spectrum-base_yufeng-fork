/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.view.conf;

import java.util.HashMap;
import java.util.Map;

import com.nexala.spectrum.channel.ChannelCategory;

//TODO : add new ChannelCategory/ChannelStatus EVENT

public class DrillDownGridStyle {
    public static final Map<ChannelCategory, String> DRILLDOWN_COMMON = new HashMap<ChannelCategory, String>() {
        private static final long serialVersionUID = 4493094068501372290L;
        {
            put(ChannelCategory.INVALID, "drill_purple_black");
            put(ChannelCategory.NODATA, "drill_white_white");
            put(ChannelCategory.NORMAL, "drill_green_white");
            put(ChannelCategory.ON, "drill_yellow_black");
            put(ChannelCategory.OFF, "drill_blue_black");
            put(ChannelCategory.WARNING, "drill_orange_white");
            put(ChannelCategory.WARNING1, "drill_orange_white");
            put(ChannelCategory.WARNING2, "drill_orange_white");
            put(ChannelCategory.FAULT, "drill_red_white");
            put(ChannelCategory.FAULT1, "drill_red_white");
            put(ChannelCategory.FAULT2, "drill_red_white");
        }
    };

//    public static final Map<ChannelCategory, String> DRILLDOWN_UNIT_ACTIVE = new HashMap<ChannelCategory, String>() {
//        private static final long serialVersionUID = -5680375858579338411L;
//        {
//            put(ChannelCategory.ON, "fleet_yellow_black");
//            put(ChannelCategory.OFF, "fleet_white_black");
//        }
//    };
    
    public static final Map<ChannelCategory, String> DRILLDOWN_CHANNELNAME = new HashMap<ChannelCategory, String>() {
        private static final long serialVersionUID = 3905701591325977178L;
        {
            put(ChannelCategory.NORMAL, "drill_white_black_left");
        }
    };
}