/*
 * Copyright (c) Nexala Technologies 2013, All rights reserved.
 */
package com.nexala.spectrum.view.conf.maps;

public interface HasMicrosoftMapsName {
    String getName();
}
