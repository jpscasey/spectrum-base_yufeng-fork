/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.view.conf.unit;

import java.util.List;

import com.google.inject.ImplementedBy;
import com.nexala.spectrum.rest.data.beans.BeanField;
import com.nexala.spectrum.view.conf.Column;

@ImplementedBy(DefaultUnitSearchConfiguration.class)
public interface UnitSearchConfiguration {
    /** The maximum number of results returned by the unit search service */
    int getMaximumResults();
    
    /** The width of the unit search box */
    int getPanelWidth();

    List<BeanField> getSearchFields();

    List<Column> getColumns();
}