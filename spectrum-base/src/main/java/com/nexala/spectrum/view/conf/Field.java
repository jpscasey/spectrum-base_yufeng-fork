/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.view.conf;

import java.util.List;

/** This is an immutable class, which represents a column in a HTML table */
public class Field {
    /** name of the column / group, e.g. C1 [Col1] or G1 [Group 1] */
    private final String name;

    /** header for the column. XXX i18n */
    private final String label;

    /** width of the column in pixels. Null permitted. */
    private final Integer width;

    private boolean resizable;

    private final String type;

    private final String formatter;

    private final boolean visible;

    /** The licence key required to view this column */
    private final String licence;

    private final boolean reference;

    private final String referencedField;

    private final boolean editable;
    
    private final boolean mandatory;

    private final String editHandler;

    private final String editLicence;

    private final boolean linkable;

    private final List<String> fleets;
    
    private final Integer maxLength;
    
    private final Integer min;
    
    private final Integer max;
    
    private final String style;
    
    private final List<Column> columns;
    
    /**
     * List of possible values, if the field is a dropdown for example.
     */
    private final List<LabelValue> values;
    
    private final List<DropdownOption> dropdownOptions;
    
    private final List<DisabledCondition> disabledConditions;
    
    private final Boolean sameRow;
    
    private List<String> usages;
    
    private final String unit;

    public Field(FieldBuilder builder) {
        this.label = builder.getDisplayName();
        this.name = builder.getName();
        this.width = builder.getWidth();
        this.formatter = builder.getFormatter();
        this.type = builder.getType().toString().toLowerCase();
        this.visible = builder.isVisible();
        this.licence = builder.getLicence();
        this.resizable = builder.isResizable();
        this.reference = builder.isReference();
        this.referencedField = builder.getReferencedField();
        this.editable = builder.isEditable();
        this.mandatory = builder.isMandatory();
        this.editHandler = builder.getEditHandler();
        this.editLicence = builder.getEditLicence();
        this.linkable = builder.isLinkable();
        this.fleets = builder.getFleets();
        this.maxLength = builder.getMaxLength();
        this.min = builder.getMin();
        this.max = builder.getMax();
        this.style = builder.getStyle();
        this.columns = builder.getColumns();
        this.values = builder.getValues();
        this.dropdownOptions = builder.getDropdownOptions();
        this.disabledConditions = builder.getDisabledConditions();
        this.sameRow = builder.getSameRow();
        this.usages = builder.getUsages();
        this.unit = builder.getUnit();
    }

    public String getName() {
        return name;
    }

    public String getLabel() {
        return label;
    }

    public String getFormatter() {
        return formatter;
    }

    public Integer getWidth() {
        return width;
    }

    public String getType() {
        return type;
    }

    public boolean isResizable() {
        return resizable;
    }

    public boolean isVisible() {
        return visible;
    }

    public String getLicence() {
        return licence;
    }

    public boolean isReference() {
        return reference;
    }

    public String getReferencedField() {
        return referencedField;
    }

    public boolean isEditable() {
        return editable;
    }

    public boolean isMandatory() {
        return mandatory;
    }

    public String getEditHandler() {
        return editHandler;
    }

    public String getEditLicence() {
        return editLicence;
    }

    public boolean isLinkable() {
        return linkable;
    }

    public List<String> getFleets() {
        return fleets;
    }
    
    public Integer getMaxLength() {
    	return maxLength;
    }
    
    public Integer getMin() {
    	return min;
    }
    
    public Integer getMax() {
    	return max;
    }
    
    public String getStyle() {
        return style;
    }
    
    public List<Column> getColumns() {
        return columns;
    }

    public List<LabelValue> getValues() {
        return values;
    }

    public Boolean getSameRow() {
        return sameRow;
    }

    public List<DropdownOption> getDropdownOptions() {
        return dropdownOptions;
    }
    
    public List<DisabledCondition> getDisabledConditions() {
        return disabledConditions;
    }
    
    public List<String> getUsages() {
    	return usages;
    }
    
    public String getUnit() {
    	return unit;
    }
}
