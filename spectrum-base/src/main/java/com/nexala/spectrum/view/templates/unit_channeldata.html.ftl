<div id = 'cdMain'>
    <div id = "cdChannelGroups">

        <ul>
            <#list screen.channelGroups as channelGroup>
                <li><a href="#cdTab_${channelGroup.id}">${channelGroup.description}</a></li>
                <div id="cdTab_${channelGroup.id}">
                </div>
            </#list>
        </ul>
    </div>

    <div id = "cdTableContainer">
        <div id = "cdTable">
        </div>

        <div id = "cdShowHideButton">
        </div>
    </div>
</div>

