package com.nexala.spectrum.view.conf.maps;

import java.util.List;

public class StationFile {
    private final List<String> fleets;
    private final String fileName;
    private final List<Station> stationList;
    
    public StationFile(List<String> fleets, String fileName, List<Station> stationList){
         this.fleets = fleets;
         this.fileName = fileName;
         this.stationList = stationList;
        
    }
    
    public List<String> getFleets() {
        return fleets;
    }
    
    public String getFileName() {
        return fileName;
    }
    
    public List<Station> getStationList() {
        return stationList;
    }
}
