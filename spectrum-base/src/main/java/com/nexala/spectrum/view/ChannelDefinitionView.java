/*
# * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.view;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.nexala.spectrum.licensing.Licence;
import com.nexala.spectrum.licensing.RestrictedResource;
import com.nexala.spectrum.view.conf.App;
import com.nexala.spectrum.view.conf.AppConfiguration;
import com.nexala.spectrum.view.conf.ChannelDefinitionConfiguration;

@Singleton
public class ChannelDefinitionView extends TemplateServlet {
    private static final long serialVersionUID = -4589530905817739310L;

    @Inject
    private ChannelDefinitionConfiguration channelDefinitionConfiguration;

    @Inject
    private AppConfiguration appConf;

    @RestrictedResource(Constants.ADMINISTRATION_CHANNELDEFINITION_LIC_KEY)
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException {

        Licence licence = Licence.get(request, response, true);

        List<String> includes = new ArrayList<String>();
        Map<String, Object> rootMap = new HashMap<String, Object>();
        rootMap.put("head", "channeldefinition_head.html.ftl");
        rootMap.put("includes", includes);
        rootMap.put("screenName", channelDefinitionConfiguration.getScreenName());
        doRender(request, response, "channeldefinition.html.ftl", rootMap, licence);
    }
    
    public App getView() {
        return appConf.getApp(Constants.CHANNEL_DEFINITION);
    }
}
