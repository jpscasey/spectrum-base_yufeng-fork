package com.nexala.spectrum.view.conf;

import java.util.List;

import com.google.inject.ImplementedBy;

@ImplementedBy(DefaultEventGroupConfiguration.class)
public interface EventGroupConfiguration {

    List<Column> getColumns();
    
}
