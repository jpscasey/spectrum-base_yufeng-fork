package com.nexala.spectrum.view.conf.cab;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * Settings for the switch component.
 * @author brice
 *
 */
@JsonInclude(Include.NON_NULL)
public class SwitchSettings implements Settings {
    
    private Long onAngle = null;
    
    private Long offAngle = null;

    /**
     * Getter for onAngle.
     * @return the onAngle
     */
    public Long getOnAngle() {
        return onAngle;
    }

    /**
     * Setter for onAngle.
     * @param onAngle the onAngle to set
     */
    public SwitchSettings setOnAngle(Long onAngle) {
        this.onAngle = onAngle;
        return this;
    }

    /**
     * Getter for offAngle.
     * @return the offAngle
     */
    public Long getOffAngle() {
        return offAngle;
    }

    /**
     * Setter for offAngle.
     * @param offAngle the offAngle to set
     */
    public SwitchSettings setOffAngle(Long offAngle) {
        this.offAngle = offAngle;
        return this;
    }
    

}
