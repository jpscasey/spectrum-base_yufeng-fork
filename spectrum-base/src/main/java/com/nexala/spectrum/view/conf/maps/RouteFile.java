package com.nexala.spectrum.view.conf.maps;

import java.util.List;

public class RouteFile {
    private final List<String> fleets;
    private final String fileName;
    private final List<RouteTrack> routeList;
    
    public RouteFile(List<String> fleets, String fileName, List<RouteTrack> routeList){
         this.fleets = fleets;
         this.fileName = fileName;
         this.routeList = routeList;
        
    }
    
    public List<String> getFleets() {
        return fleets;
    }
    
    public String getFileName() {
        return fileName;
    }
    
    public List<RouteTrack> getRouteList() {
        return routeList;
    }
}
