/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.view.conf.cab;

public class CabDomIdGenerator {
    private String baseName = "nx_unit_cab_el";
    private int nextId = 0;
    
    public CabDomIdGenerator() {
        
    }

    public String nextId() {
        return baseName + nextId++;
    }
}
