<html>
    <head>
        <title>Nexala R2M recover password</title>
        <meta http-equiv="X-UA-Compatible" content="IE=8" >
        
        <link rel="stylesheet" type="text/css" href="/css/nexala.lostPassword.css" />
    </head>
    
    <body class="loginpage">
        <div class="loginbox">
            <div class="loginform">
                <div class="logo"></div>
            
                <form name="spectrumLoginForm" method="POST" action="lostPassword">
                    <fieldset>
                        <label for="username">Enter your email : </label>
                        <input type="text" name="email" id="email" class="logintext" value="" />
                    </fieldset>
                    <fieldset>
                    	<input type="submit" class="lostPwdButton" name="reset" value="Send Reset Email">
                    	<a href="/" class="lostPwdButton">Back</a>
                    </fieldset>
                </form>
            </div>
        </div>
        
        <script type="text/javascript" language="JavaScript">
            document.spectrumLoginForm.email.focus();
        </script>
    </body>
</html>