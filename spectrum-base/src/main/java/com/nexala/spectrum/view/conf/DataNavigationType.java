package com.nexala.spectrum.view.conf;

public enum DataNavigationType {
    TIME,
    RECORD
}
