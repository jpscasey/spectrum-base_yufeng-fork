/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.view.conf;

import java.util.List;
import java.util.Map;

import com.nexala.spectrum.channel.ChannelCategory;

/** This is an immutable class, which represents a column in a HTML table */
public class Column {
    /** name of the column / group, e.g. C1 [Col1] or G1 [Group 1] */
    private final String name;

    /** header for the column. XXX i18n */
    private final String header;

    /** The format string for the column. See jquery.format.js */
    private final String format;
    
    /** width of the column in pixels. Null permitted. */
    private final Integer width;

    /** ChannelCategory -> CSS class names */
    private final Map<ChannelCategory, String> styles;

    /**
     * Function name that will handle the click event. Empty value means
     * "do nothing". FIXME - null should mean do nothing.
     */
    private final String handler;

    private boolean resizable;
    
    private final boolean sortable;
    
    private final String sortByColumn;

    private final String type;
    
    private final boolean visible;
    
    private boolean autosize;
    
    private boolean exportable;

    /** The licence key required to view this column */
    private final String licence;
    
    private final String cssClass;

    private final String constant;
    
    private final List<MouseOverText> mouseOverContent;
    
    private final String mouseOverHeader;
    
    private final String displayField;
    
    private final List<String> fleets;

    public Column(ColumnBuilder builder) {
        if (builder.getStyles() == null) {
        //if (builder.getStyles() == null || builder.getStyles().size() < 1) {
            throw new IllegalArgumentException("No styles specified");
        }

        this.header = builder.getDisplayName();
        this.format = builder.getFormat();
        this.name = builder.getName();
        this.width = builder.getWidth();
        this.handler = builder.getHandler();
        this.sortable = builder.isSortable();
        this.sortByColumn = builder.getSortByColumn();
        this.type = builder.getType().toString().toLowerCase();
        this.visible = builder.isVisible();
        this.autosize = builder.isAutosize();
        this.styles = builder.getStyles();
        this.licence = builder.getLicence();
        this.resizable = builder.isResizable();
        this.cssClass = builder.getCssClass();
        this.exportable = builder.isExportable();
        this.constant = builder.getConstant();
        this.mouseOverContent = builder.getMouseOverContent();
        this.mouseOverHeader = builder.getMouseOverHeader();
        this.displayField = builder.getDisplayField();
        this.fleets = builder.getFleets();
    }

    public String getName() {
        return name;
    }

    public String getHeader() {
        return header;
    }

    public Integer getWidth() {
        return width;
    }

    public Map<ChannelCategory, String> getStyles() {
        return styles;
    }

    public String getFormat() {
        return format;
    }
    
    public String getHandler() {
        String handler = this.handler;
        if (this.handler == null || this.handler.trim().length() == 0) {
            handler = "";
        }
        return handler;
    }

    public boolean isSortable() {
        return sortable;
    }
    
    public String getSortByColumn() {
        return sortByColumn;
    }

    public String getType() {
        return type;
    }

    public boolean isResizable() {
        return resizable;
    }
    
    public boolean isVisible() {
        return visible;
    }
    
    public boolean isAutosize() {
        return autosize;
    }
    
    public boolean isExportable() {
        return exportable;
    }

    public String getLicence() {
        return licence;
    }
    
    public String getCssClass() {
        return cssClass;
    }

    public String getConstant() {
        return constant;
    }
    
    public List<MouseOverText> getMouseOverContent() {
        return mouseOverContent;
    }
    
    public String getMouseOverHeader() {
        return mouseOverHeader;
    }
    
    public List<String> getFleets() {
        return fleets;
    }

    public String getDisplayField() {
        return displayField;
    }
}
