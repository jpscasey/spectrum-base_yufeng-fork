/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.view.conf;

import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import com.nexala.spectrum.db.dao.EventDao;
import com.nexala.spectrum.licensing.Licence;
import com.nexala.spectrum.licensing.ScreenRequiresFeature;
import com.nexala.spectrum.rest.data.beans.Event;
import com.nexala.spectrum.rest.data.beans.EventDefinedFilterField;

public interface EventConfiguration extends DetailScreen,
        ScreenRequiresFeature, ScreenConfiguration {
    /**
     * Returns a list of columns.
     * 
     * XXX Finish doc. where is this used?
     * @return
     */
    List<Column> getColumns();
    
    /**
     * The advanced search always provides fields for searching for Events [Faults], by Event Code [Fault Code]
     * Description, Type and Category. The form also allows search by live / not live. In some projects a live fault is
     * one, which hasn't been retracted yet - by the rule containing its inverse conditions.
     * 
     * Note. The names of these fields must match the names of fields returned by the
     * {@link EventDao#searchEvents(java.util.Date, java.util.Date, String, String, List)} method
     * 
     * @return
     */
    List<Field> getAdditionalAdvancedSearchFields();

    String[] getHeaders(Licence licence);

    /**
     * Return this event as a row, formation the date using the provided timezone
     * @param event the event to transform as a row
     * @param timeZone the timezone to format the event
     * @return the event as a row
     */
    String[] getRow(Event event, TimeZone timeZone);
    
    boolean isDefinedFiltersVisible();
    
    Map<String, List<EventDefinedFilterField>> getDefinedFilters();
    
    boolean isGroupButtonVisible();
    
    boolean isDisplayGroupsInBlock();
    
    int getDefaultSortColumn();

}
