package com.nexala.spectrum.view.conf;

import java.util.List;

import com.nexala.spectrum.rest.service.bean.EventFilterBean;
import com.nexala.spectrum.rest.service.bean.EventPanelBean;

public class CurrentEventsPanel {

    private final List<Column> columns;

    private final List<MouseOverText> rowMouseOver;

    private final Boolean bottom;

    private final Integer descriptionMaxCharacters;

    private final Boolean createFault;
    
    private final Boolean createFaultRecoverButton;
    
    private final Boolean eventPanelsOnBottom;

    private final List<EventFilterBean> createFaultInput;
    
    private final List<EventPanelBean> eventPanels;
    
    private String userConfiguration;
    
    private final Boolean alwaysDisplayLiveCount;

    public CurrentEventsPanel(List<Column> columns, List<EventFilterBean> filters,
            List<MouseOverText> rowMouseOver, Boolean bottom,
            Integer descriptionMaxCharacters, Boolean createFault, Boolean createFaultRecoverButton,
            Boolean eventPanelsOnBottom,
            List<EventFilterBean> createFaultInput, List<EventPanelBean> eventPanels, Boolean alwaysDisplayLiveCount) {
        this.columns = columns;
        this.rowMouseOver = rowMouseOver;
        this.bottom = bottom;
        this.descriptionMaxCharacters = descriptionMaxCharacters;
        this.createFault = createFault;
        this.createFaultRecoverButton = createFaultRecoverButton;
        this.eventPanelsOnBottom = eventPanelsOnBottom;
        this.createFaultInput = createFaultInput;
        this.eventPanels = eventPanels;
        this.alwaysDisplayLiveCount = alwaysDisplayLiveCount;
    }

    public List<Column> getColumns() {
        return columns;
    }

    public List<MouseOverText> getRowMouseOver() {
        return rowMouseOver;
    }

    public Boolean getBottom() {
        return bottom;
    }

    public Integer getDescriptionMaxCharacters() {
        return descriptionMaxCharacters;
    }

    public Boolean getCreateFault() {
        return createFault;
    }

	public List<EventFilterBean> getCreateFaultInput() {
		return createFaultInput;
	}

	public Boolean getCreateFaultRecoverButton() {
		return createFaultRecoverButton;
	}
	
	public Boolean getEventPanelsOnBottom() {
		return eventPanelsOnBottom;
	}

	public List<EventPanelBean> getEventPanels() {
		return eventPanels;
	}

    public String getUserConfiguration() {
        return userConfiguration;
    }

    public void setUserConfiguration(String userData) {
        this.userConfiguration = userData;
    }

    public Boolean getAlwaysDisplayLiveCount() {
        return alwaysDisplayLiveCount;
    }

}
