/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.view.conf.cab;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class DialSettings {
    private Double angleEnd;
    private Double angleStart;
    private Integer animationTime;
    private String arcStrokeFill;
    private Double arcStrokeWidth;
    private Boolean dial;
    private Boolean displayValue;
    private String displayValueFontColor;
    private Integer displayValueFontSize;
    private String displayValueFontWeight;
    private Integer displayValuePosition;
    private String displayValueSuffix;
    private Double displayValueRadius;
    private String easing;
    private Double endValue;
    private String id;
    
    private Boolean displayLabel;
    private String labelFill;
    private Double labelIncrement;
    private Double labelRadius;
    private Double majorIncrement;
    private String majorStrokeFill;
    private Boolean majorStrokeLabels;
    private Double majorStrokeLength;
    private Double majorStrokeWidth;
    private Double minorIncrement;
    private String minorStrokeFill;
    private Boolean minorStrokeLabels;
    private Double minorStrokeLength;
    private Double minorStrokeWidth;
    private String pivotFill;
    
    private Boolean pointer;
    private String pointerFill;
    private String pointerStroke;
    private Double pointerRadius;

    public enum pointerTypeValues {
        DEFAULT, INTERNAL, EXTERNAL
    };
    private pointerTypeValues pointerType = pointerTypeValues.DEFAULT;
    
    private Double radius;
    private Double startValue;
    private Double lastTick;
    private Boolean labelsPerpendicular;

    public Double getAngleEnd() {
        return angleEnd;
    }

    public Double getAngleStart() {
        return angleStart;
    }

    public Integer getAnimationTime() {
        return animationTime;
    }

    public String getArcStrokeFill() {
        return arcStrokeFill;
    }

    public Double getArcStrokeWidth() {
        return arcStrokeWidth;
    }

    public Boolean getDial() {
        return dial;
    }

    public Boolean getDisplayValue() {
        return displayValue;
    }

    public String getDisplayValueFontColor() {
        return displayValueFontColor;
    }

    public Integer getDisplayValueFontSize() {
        return displayValueFontSize;
    }

    public String getDisplayValueFontWeight() {
        return displayValueFontWeight;
    }

    public Integer getDisplayValuePosition() {
        return displayValuePosition;
    }

    public Double getDisplayValueRadius() {
        return displayValueRadius;
    }

    public String getEasing() {
        return easing;
    }

    public Double getEndValue() {
        return endValue;
    }
    
    public String getId() {
        return id;
    }
    
    public String getLabelFill() {
        return labelFill;
    }

    public Double getLabelIncrement() {
        return labelIncrement;
    }

    public Double getLabelRadius() {
        return labelRadius;
    }

    public Double getMajorIncrement() {
        return majorIncrement;
    }

    public String getMajorStrokeFill() {
        return majorStrokeFill;
    }

    public Boolean getMajorStrokeLabels() {
        return majorStrokeLabels;
    }

    public Double getMajorStrokeLength() {
        return majorStrokeLength;
    }

    public Double getMajorStrokeWidth() {
        return majorStrokeWidth;
    }

    public Double getMinorIncrement() {
        return minorIncrement;
    }

    public String getMinorStrokeFill() {
        return minorStrokeFill;
    }

    public Boolean getMinorStrokeLabels() {
        return minorStrokeLabels;
    }

    public Double getMinorStrokeLength() {
        return minorStrokeLength;
    }

    public Double getMinorStrokeWidth() {
        return minorStrokeWidth;
    }

    public String getPivotFill() {
        return pivotFill;
    }

    

    public Boolean getPointer() {
        return pointer;
    }

    public String getPointerFill() {
        return pointerFill;
    }

    public String getPointerStroke() {
        return pointerStroke;
    }

    public pointerTypeValues getPointerType() {
        return pointerType;
    }

    public Double getRadius() {
        return radius;
    }

    public Double getStartValue() {
        return startValue;
    }
    
    public Double getPointerRadius() {
        return pointerRadius;
    }

    public DialSettings setAngleEnd(Double angleEnd) {
        this.angleEnd = angleEnd;
        return this;
    }

    public DialSettings setAngleStart(Double angleStart) {
        this.angleStart = angleStart;
        return this;
    }

    public DialSettings setAnimationTime(Integer animationTime) {
        this.animationTime = animationTime;
        return this;
    }

    public DialSettings setArcStrokeFill(String arcStrokeFill) {
        this.arcStrokeFill = arcStrokeFill;
        return this;
    }

    public DialSettings setArcStrokeWidth(Double arcStrokeWidth) {
        this.arcStrokeWidth = arcStrokeWidth;
        return this;
    }

    public DialSettings setDial(Boolean dial) {
        this.dial = dial;
        return this;
    }

    public DialSettings setDisplayValue(Boolean displayValue) {
        this.displayValue = displayValue;
        return this;
    }

    public DialSettings setDisplayValueFontColor(String displayValueFontColor) {
        this.displayValueFontColor = displayValueFontColor;
        return this;
    }

    public DialSettings setDisplayValueFontSize(Integer displayValueFontSize) {
        this.displayValueFontSize = displayValueFontSize;
        return this;
    }

    public DialSettings setDisplayValueFontWeight(String displayValueFontWeight) {
        this.displayValueFontWeight = displayValueFontWeight;
        return this;
    }

    public DialSettings setDisplayValuePosition(Integer displayValuePosition) {
        this.displayValuePosition = displayValuePosition;
        return this;
    }

    public DialSettings setDisplayValueRadius(Double displayValueRadius) {
        this.displayValueRadius = displayValueRadius;
        return this;
    }

    public DialSettings setEasing(String easing) {
        this.easing = easing;
        return this;
    }

    public DialSettings setEndValue(Double endValue) {
        this.endValue = endValue;
        return this;
    }
    
    public DialSettings setId(String id) {
        this.id = id;
        return this;
    }
    
    public DialSettings setLabelFill(String labelFill) {
        this.labelFill = labelFill;
        return this;
    }

    public DialSettings setLabelIncrement(Double labelIncrement) {
        this.labelIncrement = labelIncrement;
        return this;
    }

    public DialSettings setLabelRadius(Double labelRadius) {
        this.labelRadius = labelRadius;
        return this;
    }

    public DialSettings setMajorIncrement(Double majorIncrement) {
        this.majorIncrement = majorIncrement;
        return this;
    }

    public DialSettings setMajorStrokeFill(String majorStrokeFill) {
        this.majorStrokeFill = majorStrokeFill;
        return this;
    }

    public DialSettings setMajorStrokeLabels(Boolean majorStrokeLabels) {
        this.majorStrokeLabels = majorStrokeLabels;
        return this;
    }

    public DialSettings setMajorStrokeLength(Double majorStrokeLength) {
        this.majorStrokeLength = majorStrokeLength;
        return this;
    }

    public DialSettings setMajorStrokeWidth(Double majorStrokeWidth) {
        this.majorStrokeWidth = majorStrokeWidth;
        return this;
    }

    public DialSettings setMinorIncrement(Double minorIncrement) {
        this.minorIncrement = minorIncrement;
        return this;
    }

    public DialSettings setMinorStrokeFill(String minorStrokeFill) {
        this.minorStrokeFill = minorStrokeFill;
        return this;
    }

    public DialSettings setMinorStrokeLabels(Boolean minorStrokeLabels) {
        this.minorStrokeLabels = minorStrokeLabels;
        return this;
    }

    public DialSettings setMinorStrokeLength(Double minorStrokeLength) {
        this.minorStrokeLength = minorStrokeLength;
        return this;
    }

    public DialSettings setMinorStrokeWidth(Double minorStrokeWidth) {
        this.minorStrokeWidth = minorStrokeWidth;
        return this;
    }

    public DialSettings setPivotFill(String pivotFill) {
        this.pivotFill = pivotFill;
        return this;
    }

    

    public DialSettings setPointer(Boolean pointer) {
        this.pointer = pointer;
        return this;
    }

    public DialSettings setPointerFill(String pointerFill) {
        this.pointerFill = pointerFill;
        return this;
    }

    public DialSettings setPointerStroke(String pointerStroke) {
        this.pointerStroke = pointerStroke;
        return this;
    }
    
    public DialSettings setPointerType(pointerTypeValues pointerType) {
        this.pointerType = pointerType;
        return this;
    }

    public DialSettings setRadius(Double radius) {
        this.radius = radius;
        return this;
    }

    public DialSettings setStartValue(Double startValue) {
        this.startValue = startValue;
        return this;
    }

    public Double getLastTick() {
        return lastTick;
    }

    public DialSettings setLastTick(Double lastTick) {
        this.lastTick = lastTick;
        return this;
    }

    public Boolean getLabelsPerpendicular() {
        return labelsPerpendicular;
    }

    public DialSettings setLabelsPerpendicular(Boolean labelsPerpendicular) {
        this.labelsPerpendicular = labelsPerpendicular;
        return this;
    }
    
    public DialSettings setPointerRadius(Double pointerRadius) {
        this.pointerRadius = pointerRadius;
        return this;
    }

    /**
     * Getter for displayValueSuffix.
     * @return the displayValueSuffix
     */
    public String getDisplayValueSuffix() {
        return displayValueSuffix;
    }

    /**
     * Setter for displayValueSuffix.
     * @param displayValueSuffix the displayValueSuffix to set
     */
    public DialSettings setDisplayValueSuffix(String displayValueSuffix) {
        this.displayValueSuffix = displayValueSuffix;
        return this;
    }

    /**
     * Getter for displayLabel.
     * @return the displayLabel
     */
    public Boolean getDisplayLabel() {
        return displayLabel;
    }

    /**
     * Setter for displayLabel.
     * @param displayLabel the displayLabel to set
     */
    public DialSettings setDisplayLabel(Boolean displayLabel) {
        this.displayLabel = displayLabel;
        return this;
    }

}