<#include "header.ftl"/>
    <style type = 'text/css'>
    #timeController {
        top: 5px;
    }
    </style>
    <div id = 'unitSummaryHeader'>
        <div id = 'left'>
            <div id = 'unitFormationSelector'></div>
        </div>
        <div id = 'right'>
            <div id = 'timeController' style = 'float:right'>
                <div id = 'playPauseButton'></div>
                <div id = 'timePicker' class = 'timePicker'>
                    <div class = "leftButton"><a>-</a></div>
                    <div class = "inputDiv"></div>
                    <div class = 'nowButton'><a>LIVE</a></div> 
                    <div class = "rightButton"><a>+</a></div>
                </div>
            </div>
        </div>
    </div>
    <div id = 'unitSummaryBody' style = 'top: 40px'>
        <div id="unitTabs" style = 'height: 100%'>
            <ul style = 'display: none'>
                <#if conf.cabProvider??><li><a href = "#us_tab6">Cab</a></li></#if>
            </ul>
        
            <#if conf.cabProvider??>
                <div id = "us_tab6" style = 'height: 100%'>
                    <#include "unit_cab.html.ftl"/>
                </div>
            </#if>
        </div>
    </div>
<#include "footer.ftl"/>