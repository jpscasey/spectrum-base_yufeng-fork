<#include "header.ftl"/>

<div class="channelDefinitionView" >
    <div id="channelDefinitionHeader" class="ui-widget-header">
        <div style="display:block;">
            <div>
                <label data-i18n="Fleet"></label>
                <select id="fleetCombo" tabindex="1"></select>

                <label style="padding-left: 15px;" data-i18n="Keyword"></label>
                <input id="keywordSearch" type="text" tabindex="2">

                <span id="searchButtonContainer" style="padding-left: 15px;"></span>
            </div>
        </div>
    </div>

    <div class="channelDefinitionGridContainer">
        <div id="channelDefinitionGrid"></div>
    </div>

    <div id="channelDefinitionDialog" style="display: none">
        <div id="channelDefinitionAlertDialog" style="display: none">
            <p>
                <span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;">
                </span>
                <div id="channelDefinitionAlertMessage"></div>
            </p>
        </div>

        <input id="dialogFleetId" type="hidden">

        <div id="leftPanel"></div>

        <div id="rightPanel">
            <div class="detailRuleRow ruleHeader">
                <div class="smallDetailLabel" data-i18n="Rules"></div>
                <select id="dialogRuleCombo" tabindex="13"></select>
                <div id="dialogRuleButtons"></div>
            </div>

            <div id="dialogRule" class="ruleNonVisible">
                <input id="dialogRuleId" type="hidden">

                <div class="detailRuleRow">
                    <div class="smallDetailLabel" data-i18n="Name"></div>
                    <input id="dialogRuleName" type="text" tabindex="14">
                </div>

                <div class="detailRuleRow">
                    <div class="smallDetailLabel" data-i18n="Status"></div>
                    <select id="dialogRuleStatus" tabindex="15"></select>
                </div>

                <div class="detailRuleRow">
                    <div class="smallDetailLabel" data-i18n="Active"></div>
                    <input id="dialogRuleActive" type="checkbox" tabindex="16">
                </div>

                <div class="validationsHeader">
                    <span data-i18n="Validations"> </span>
                    <div id="validationAddButton"></div>
                </div>

                <div id="dialogRuleValidations" class="detailRow">
                </div>
            </div>
        </div>

        <div id="bottomPanel">
            <span id="saveButtonContainer"></span>
            <span id="cancelButtonContainer"></span>
        </div>
    </div>
</div>

<#include "footer.ftl"/>