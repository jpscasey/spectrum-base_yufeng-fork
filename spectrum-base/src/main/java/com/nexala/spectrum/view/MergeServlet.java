/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.view;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLDecoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.configuration.ConfigurationManager;

@Singleton
public class MergeServlet extends HttpServlet {

    /**
     * 
     */
    private static final long serialVersionUID = 6938197427580693363L;

    /** The regex for validating the parameter provided by the user */
    private static final Pattern VALIDATION = Pattern.compile(
            "^\\s*[^,]+\\.(js|css)(\\s*,\\s*([^,]+\\.(js|css)))*\\s*,?\\s*$");

    private static final Pattern CSS_PATTERN = Pattern
            .compile("^\\s*[^,]+\\.css(\\s*,\\s*([^,]+\\.css))*\\s*$");
    
    /** The regex for blacklisting filepaths that can be injected into the request */
    private static final Pattern PROTECTED_FILE = Pattern
            .compile("(css|js|i18n|\\/i18n|\\*?)\\/[-a-zA-Z0-9.\\/]+.(csv|xml)");
    
    @Inject
    private ConfigurationManager confManager;
    
    /**
     * The regex for splitting the resources param specified by the user.
     */
    private static final Pattern SPLITTER = Pattern.compile(",");

    private final Logger logger = Logger.getLogger(MergeServlet.class);

    private ThreadLocal<SimpleDateFormat> sdf = new ThreadLocal<SimpleDateFormat>() {
        public SimpleDateFormat get() {
            return new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z");
        }
    };
    
    private byte[] injectJs;

    private class MergedResources {
        private byte [] data;
        private long timestamp;
        
        public MergedResources(byte [] data, long lastModified) {
            this.data = data;
            this.timestamp = lastModified;
        }
        
        public byte[] getData() {
            return data;
        }
        
        public long lastModified() {
            return timestamp;
        }
    }
    
    private class ResourceCache {
        /** Maps URL-path -> cached [merged] byte stream */
        private Map<String, MergedResources> cache = new HashMap<String, MergedResources>();

        public synchronized void put(String resourcesUrl, MergedResources content) {
            cache.put(resourcesUrl, content);
        }

        /**
         * Returns the cached content for the specified URL or null if the item
         * does not exist in the cache.
         * 
         * @param resourcesUrl
         * @return
         */
        public synchronized MergedResources get(String resourcesUrl) {
            return cache.get(resourcesUrl);
        }
    }

    private ResourceCache cache = new ResourceCache();

    /**
     * 
     */
    public void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        String resources = req.getParameter("resources");

        if (resources != null) {
            resources = URLDecoder.decode(resources, "UTF-8");
        }

        boolean mergeEnabled = confManager.getConfAsBoolean(Spectrum.SPECTRUM_MERGE_RESOURCES, false);
        MergedResources resource = null;

        if (resources != null && resources.length() > 0) {
            
            // Check if any resource requested is protected
            for (String filePath : SPLITTER.split(resources)) {
                Matcher pathMatcher = PROTECTED_FILE.matcher(filePath.trim());
                if (pathMatcher.matches()) {
                    logger.warn("Protected resource requested. Access Denied.");
                    resp.setStatus(403);
                    return;
                }
            }
            Matcher matcher = VALIDATION.matcher(resources);

            resource = cache.get(resources);

            boolean modified = true;
            boolean forceRefresh = false;
            String modifiedSince = req.getHeader("if-modified-since");
            String cacheControl = req.getHeader("cache-control");
            String pragma = req.getHeader("pragma");
            
            List<String> realPaths = new ArrayList<String>();
            
            if (mergeEnabled) {
                ServletContext ctx = req.getSession().getServletContext();
                
                for (String filePath : SPLITTER.split(resources)) {
                    
                    if (filePath != null) {
                        filePath = filePath.trim();
                        
                        if (filePath.length() > 0) {
                            realPaths.add(ctx.getRealPath(filePath));
                        }
                    }
                }
            }
            
            // Client has sent If-Modified-Since header, we should check the
            // modification times of the files. And send a 304 if they have
            // not changed.
            if (mergeEnabled) {
                if (modifiedSince != null) {
                    Date ltime = null;
                    
                    try {
                        ltime = sdf.get().parse(modifiedSince);
                    } catch (ParseException e) {
                        ltime = null;
                    } catch (NumberFormatException e) {
                        ltime = null;
                    }
                    
                    if (ltime != null) {
                        long mtime = getLastModificationTime(realPaths);
                        // round it to seconds
                        mtime = mtime - mtime%1000;
                        
                        if (mtime > ltime.getTime()) {
                            modified = true;
                        } else {
                            logger.debug("Sending 304");
                            resp.setStatus(304);
                            return;
                        }
                    }
                } else if ((cacheControl != null
                        && cacheControl.indexOf("no-cache") > -1) || 
                        (pragma != null && pragma.indexOf("no-cache") > -1)) {
                    long mtime = getLastModificationTime(realPaths);
                    long cachedTime = resource == null ? 0L : resource
                            .lastModified();
                    
                    // File has been updated since a copy was cached, so 
                    // we will update the cache and resend the requested
                    // resource.
                    if (mtime > cachedTime) {
                        forceRefresh = true;
                    }
                }
            }

            // If the resource has been modified 
            if (modified || forceRefresh || (resource == null && matcher.matches())) {
                if (mergeEnabled) {
                    resource = readFiles(realPaths);
                } else {
                    List<String> userPaths = new ArrayList<String>();
                    
                    for (String filePath : SPLITTER.split(resources)) {
                        userPaths.add(filePath.trim());
                    }
                    
                    // XXX nasty, use CSS_PATTERN
                    if (resources.indexOf(".css") > -1) {
                        resource = getInjectCss(userPaths);
                    } else {
                        resource = getInjectJs(userPaths);
                    }
                }
                
                cache.put(resources, resource);
            }
            
            //if (CSS_PATTERN.matcher(resources).matches()) {
            if (resources.indexOf(".css") > -1) {
                resp.setContentType("text/css; charset=utf-8");
            } else {
                resp.setContentType("application/javascript; charset=utf-8");
            }
            
            if (resource != null) {
                if (!mergeEnabled) {
                    resp.addHeader("Cache-Control", "max-age=0");
                    resp.addHeader("Cache-Control", "no-cache");
                    resp.addHeader("Cache-Control", "no-store");
                    resp.setStatus(200);
                    // may be cached forever
                } else {
                    // Cache for 2 hours.
                    resp.addHeader("Cache-Control", "max-age=7200");
                    // This tells clients to cache, but proxies not to cache..
                    resp.addHeader("Cache-Control", "private");
                    resp.addDateHeader("Last-Modified", getLastModificationTime(realPaths));
                }
            
                OutputStream os = resp.getOutputStream();
                os.write(resource.getData());
            }
        } else {
            resp.setStatus(404);
        }
    }
    
    private MergedResources getInjectCss(List<String> filePaths) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        
        try {
            for (String filePath : filePaths) {
                if(!"".equals(filePath)) {
                    baos.write(("@import url(\"../" + filePath + "\");").getBytes());
                }
            }
            
            return new MergedResources(baos.toByteArray(), new Date().getTime());
        } finally {
            baos.close();
        }
    }

    private MergedResources getInjectJs(List<String> filePaths) throws IOException {
        if (this.injectJs == null) {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            InputStream is = this.getClass().getResourceAsStream("inject.js");

            int read = 0;
            byte[] bytes = new byte[1024];

            try {
                while ((read = is.read(bytes)) != -1) {
                    baos.write(bytes, 0, read);
                }
                
                this.injectJs = baos.toByteArray();
            } finally {
                baos.close();
                is.close();
            }
        }
        
        ByteArrayOutputStream baos = null;
        
        try {
            baos = new ByteArrayOutputStream();
            baos.write(injectJs);
            
            baos.write("\ninject(".getBytes());
            
            if (filePaths.size() > 0) {
                baos.write(("'" + filePaths.get(0) + "'").getBytes());
            }
            
            for (int i = 1; i < filePaths.size(); i++) {
                if(!"".equals(filePaths.get(i).trim())) {
                    baos.write((", '" + filePaths.get(i) + "'").getBytes());
                }
            }
            
            baos.write(");".getBytes());
            
            return new MergedResources(baos.toByteArray(), new Date().getTime());
        } finally {
            if (baos != null) {
                baos.close();
            }
        }
    }
    
    private long getLastModificationTime(List<String> filePaths)
            throws IOException {
        long lastModified = -1;
        
        // Find the latest
        for (String filePath : filePaths) {
            File file = new File(filePath);
            
            if (file.exists()) {
                long mtime = file.lastModified();
                
                if (mtime > lastModified) {
                    lastModified = mtime;
                }
            }
        }
        
        return lastModified;
    }

    private MergedResources readFiles(List<String> filePaths) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        long maxTimestamp = -1;
        
        try {
            for (String realPath : filePaths) {
                if (realPath != null && realPath.length() > 0) {
                    MergedResources file = readFile(realPath);
                    
                    byte [] fileBytes = file.getData();

                    if (file.lastModified() > maxTimestamp) {
                        maxTimestamp = file.lastModified();
                    }
                    
                    if (file.getData() != null) {
                        baos.write(fileBytes);
                        continue;
                    }
                }

                logger.error("Unable to read file: " + realPath);

                /*
                 * If the file exists, this may be caused by the
                 * containers handling of the ear/war archives, which
                 * may be unpacked to a different location (if they are
                 * deployed in their compressed forms)
                 */
                throw new IOException("Unable to read file");
            }

            return new MergedResources(baos.toByteArray(), maxTimestamp);
        } finally {
            if (baos != null)
                baos.close();
        }
    }
    
    private MergedResources readFile(String filePath) throws IOException {
        logger.debug("Reading file: " + filePath);
        
        File file = new File(filePath);

        if (file.exists()) {
            FileInputStream fis = new FileInputStream(file);
            BufferedInputStream bis = new BufferedInputStream(fis);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            

            try {
                int read = 0;
                byte[] bytes = new byte[1024];

                while ((read = bis.read(bytes)) != -1) {
                    baos.write(bytes, 0, read);
                }

                return new MergedResources(baos.toByteArray(), file.lastModified());
            } finally {
                baos.close();
                bis.close();
            }
        } else {
            return null;
        }
    }
}