package com.nexala.spectrum.view.conf;

/**
 * Screens that will be linked from other parts of the application should implement this interface
 * @author jgalvez
 */
public interface Linkable extends DetailScreen {
    String getLink();

    String getIconClass();
}
