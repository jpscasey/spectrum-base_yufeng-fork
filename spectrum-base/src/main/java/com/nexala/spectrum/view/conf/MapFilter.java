/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.view.conf;

public class MapFilter {

    private String id;
    private String label;
    private String type;
    private String operator;
    private Boolean showOnUnitLocation;
    private Boolean enableByDefault;
    private Boolean filterChecked;
    
    public MapFilter(String id, String label, String type, String operator, Boolean showOnUnitLocation, Boolean enablyByDefault, Boolean filterChecked) {
        this.id = id;
        this.label = label;
        this.type = type;
        this.operator = operator;
        this.showOnUnitLocation = showOnUnitLocation;
        this.enableByDefault = enablyByDefault;
        this.filterChecked = filterChecked;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }
    
	public Boolean getShowOnUnitLocation() {
		return showOnUnitLocation;
	}

	public void setShowOnUnitLocation(Boolean showOnUnitLocation) {
		this.showOnUnitLocation = showOnUnitLocation;
	}

	public Boolean getEnableByDefault() {
		return enableByDefault;
	}

	public void setEnableByDefault(Boolean enableByDefault) {
		this.enableByDefault = enableByDefault;
	}

	public Boolean getFilterChecked() {
	    return filterChecked;
	}
	
	public void setFilterChecked(Boolean filterChecked) {
	    this.filterChecked = filterChecked;
	}
}