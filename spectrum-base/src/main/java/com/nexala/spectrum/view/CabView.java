/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.view;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.nexala.spectrum.db.beans.Unit;
import com.nexala.spectrum.licensing.Licence;
import com.nexala.spectrum.licensing.RestrictedResource;
import com.nexala.spectrum.rest.data.UnitProvider;
import com.nexala.spectrum.rest.data.beans.UnitDesc;
import com.nexala.spectrum.view.conf.App;
import com.nexala.spectrum.view.conf.AppConfiguration;
import com.nexala.spectrum.view.conf.UnitConfiguration;

@Singleton
public class CabView extends TemplateServlet {
    
    @Inject
    private Map<String, UnitConfiguration> conf;

    @Inject
    private Map<String, UnitProvider> unitProviders;

    @Inject
    private AppConfiguration appConf;

    private static final long serialVersionUID = 7743892486241418025L;

    private void handleRequest(HttpServletRequest request,
            HttpServletResponse response) throws ServletException {

        String fleetId = request.getParameter("fleetId");
        String unitId = request.getParameter("unitId");
        UnitDesc unitDesc;

        if (unitId != null) {
            Unit unit = unitProviders.get(fleetId).searchUnit(
                    Integer.parseInt(unitId));
            unitDesc = new UnitDesc(unit.getId(), unit.getUnitNumber(), fleetId);
        } else {
            String unitNumber = request.getParameter("unitNumber");

            List<UnitDesc> unitDescs = unitProviders.get(fleetId)
                    .searchUnits(unitNumber).getUnitDescs();
            unitDesc = unitDescs.size() == 1 ? unitDescs.get(0) : null;
        }

        proceed(request, response, unitDesc, fleetId);
    }
    
    @RestrictedResource(Constants.UNIT_LIC_KEY)
    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException {
        handleRequest(request, response);
    }

    @RestrictedResource(Constants.UNIT_LIC_KEY)
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException {
        handleRequest(request, response);
    }

    private void proceed(HttpServletRequest request,
            HttpServletResponse response, UnitDesc unitDesc, String fleetId) throws ServletException {
        Licence licence = Licence.get(request, response, true);

        List<String> includes = new ArrayList<String>();
        Map<String, Object> rootMap = new HashMap<String, Object>();
        rootMap.put("head", "unit_head.html.ftl");
        rootMap.put("includes", includes);
        
        if (fleetId != null) {
            rootMap.put("screens", conf.get(fleetId).getScreens(licence));
        }
        
        rootMap.put("unit", unitDesc);
        doRender(request, response, "cab_standalone.html.ftl", rootMap, licence);
    }
    
    public App getView() {
        return appConf.getApp(Constants.UNIT_SUMMARY);
    }
}