package com.nexala.spectrum.view.conf.maps;

import java.util.List;

public class ExtraLocationDropdown {
    private String displayName;
    private List<ExtraLocation> extraLocations;
    
    public ExtraLocationDropdown(String displayName, List<ExtraLocation> extraLocations) {
		this.displayName = displayName;
		this.extraLocations = extraLocations;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public List<ExtraLocation> getExtraLocations() {
		return extraLocations;
	}

	public void setExtraLocations(List<ExtraLocation> extraLocations) {
		this.extraLocations = extraLocations;
	}
}
