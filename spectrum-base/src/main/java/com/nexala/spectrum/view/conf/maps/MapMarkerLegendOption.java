package com.nexala.spectrum.view.conf.maps;

public class MapMarkerLegendOption {

    private String id;

    private String label;

    private int category;

    private String color;

    private boolean enabled;

    private int lowerThreshold;

    private int upperThreshold;
    
    public MapMarkerLegendOption(String id, String label, int category, String color, boolean enabled,
            int lowerThreshold, int upperThreshold) {
        this.id = id;
        this.label = label;
        this.category = category;
        this.color = color;
        this.enabled = enabled;
        this.lowerThreshold = lowerThreshold;
        this.upperThreshold = upperThreshold;
    }

    public String getId() {
        return id;
    }

    public String getLabel() {
        return label;
    }

    public int getCategory() {
        return category;
    }

    public String getColor() {
        return color;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public int getLowerThreshold() {
        return lowerThreshold;
    }

    public int getUpperThreshold() {
        return upperThreshold;
    }
}
