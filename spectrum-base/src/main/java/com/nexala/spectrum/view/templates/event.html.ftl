<#include "header.ftl"/>

<link rel = 'stylesheet' type = 'text/css' href = 'css/merge?resources=
    css/nexala.timepicker.css
    &view=event
'>

<#list cssIncludesList as cssIncludes>
    <link rel = 'stylesheet' type = 'text/css' href = 'css/merge?resources=
        <#list cssIncludes as cssInclude>${cssInclude},</#list>
        &view=event
    '>
</#list>

<script type = 'text/javascript' src = 'js/merge?resources=
    js/i18n/datepicker-nl.js,
    js/i18n/datepicker-en-GB.js,
    js/nexala.timepicker.js
    &view=event
'></script>

<#list jsIncludesList as jsIncludes>
    <script type = 'text/javascript' src = 'js/merge?resources=
        <#list jsIncludes as jsInclude>${jsInclude},</#list>
        &view=event
    '></script>
</#list>

<div class="faultHistory">
    <div class='advancedSearch ui-widget-header'>
        <div id="commonSearchFields">
        </div>
		<div id="additionalSearchFields" >
		    <div class = "rightAligned">
                <span class = 'advancedExportEventsButton'></span>
            </div>
        </div>
    </div>

    <div class="eventHeader ui-widget-header">
        <div style="display:block;" id="innerHeader">
            <label data-i18n="From"></label>
            <div id="timePickerFrom" class="timePicker" style="display:inline-block;">
                <div class="inputDiv">
                </div>
            </div>
            
            <label data-i18n="To"></label>
            <div id="timePickerTo" class="timePicker" style="display:inline-block;">
                <div class="inputDiv">
                </div>
            </div>
            
            <label data-i18n="Keyword"></label>
            <input id="keyWordEventSearch" type="text" class = 'inputKeyword' tabindex="6">
            
            <div id="radioDiv" style="display:inline-block;">
                <input type="radio" id="liveYes" name="liveRadio" value="Live" tabindex="3" />
                <label for="liveYes" data-i18n="Live"></label>
                <input type="radio" id="liveNo" name="liveRadio" value="NoLive" tabindex="4" />
                <label for="liveNo" data-i18n="Not live"></label>
                <input type="radio" id="liveAll" name="liveRadio" value="All" checked="checked" tabindex="5" />
                <label for="liveAll" data-i18n="All"></label>
            </div>

            <div class = "rightAligned">
                <span class = 'groupEventsButton'></span>
                <span class = 'exportEventsButton'></span>
                <span class = 'searchEventsButton'></span>
                <span class = 'expandButton'></span>
            </div>
        </div>
    </div>
    
    <div id="faultTable">
        <table cellpadding="0" cellspacing="0" border="0" id = "events"></table>
    </div>
</div>
<#include "footer.ftl"/>