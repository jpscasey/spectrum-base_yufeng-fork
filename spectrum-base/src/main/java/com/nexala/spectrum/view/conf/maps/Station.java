/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.view.conf.maps;

public class Station  {
    private final int id;
    private final String name;
    private final String code;
    private final Coordinate coordinate;
    private final String type;
    
  
    public Station(int id, String name, String code, Coordinate coordinate, String type) {
		this.id = id;
		this.name = name;
		this.code = code;
		this.coordinate = coordinate;
		this.type = type;
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getCode() {
		return code;
	}

	public Coordinate getCoordinate() {
		return coordinate;
	}

    public String getType() {
        return type;
    }

}