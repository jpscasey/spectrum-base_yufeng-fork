package com.nexala.spectrum.view.conf;

import java.util.List;

import com.google.inject.ImplementedBy;
import com.nexala.spectrum.licensing.ScreenRequiresFeature;
import com.nexala.spectrum.rest.data.beans.Schematic;
import com.nexala.spectrum.rest.data.beans.SchematicChannel;

@ImplementedBy(DefaultSchematicConfiguration.class)
public interface SchematicConfiguration extends DetailScreen, ScreenRequiresFeature {
	
	public List<Schematic> getItems();
	
	public List<SchematicChannel> getSchematicData();
}
