/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.view.conf.cab;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.nexala.spectrum.channel.ChannelCategory;

@JsonInclude(Include.NON_NULL)
public class LedSettings implements Settings {
    
    public static final String BLUE = "#0198E1";
    public static final String YELLOW = "#FCD116";
    public static final String RED = "#CD0000";
    public static final String GREEN = "#004F00";
    
    private String onColor = "green";
    private String offColor = "red";
    private boolean onValue = true;
    
    /**
     * True if the LED should display the channel category.
     */
    private boolean channelCatDisplay = false;
    
    private Map<Integer, String> categoryColor = new HashMap<Integer, String>(); 
    
    
    public String getOnColor() {
        return onColor;
    }

    public String getOffColor() {
        return this.offColor;
    }
    
    public boolean getOnValue() {
        return onValue;
    }

    public LedSettings setOnColor(String onColor) {
        this.onColor = onColor;
        return this;
    }

    public LedSettings setOffColor(String offColor) {
        this.offColor = offColor;
        return this;
    }
    
    public LedSettings setOnValue(boolean onValue) {
        this.onValue = onValue;
        return this;
    }

    /**
     * Getter for categoryColor.
     * @return the categoryColor
     */
    public Map<Integer, String> getCategoryColor() {
        return categoryColor;
    }
    
    /**
     * Associate a channel category to a color.
     * @param category the category
     * @param color the color 
     * @return the value setting
     */
    public LedSettings setColorForChanCategory(ChannelCategory category, String color) {
        categoryColor.put(category.getSeverity(), color);
        return this;
    }

    /**
     * Getter for channelCatDisplay.
     * @return the channelCatDisplay
     */
    public boolean isChannelCatDisplay() {
        return channelCatDisplay;
    }

    /**
     * Setter for channelCatDisplay.
     * @param channelCatDisplay the channelCatDisplay to set
     */
    public LedSettings setChannelCatDisplay(boolean channelCatDisplay) {
        this.channelCatDisplay = channelCatDisplay;
        return this;
    }


}
