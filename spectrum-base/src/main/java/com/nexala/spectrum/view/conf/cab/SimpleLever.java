/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.view.conf.cab;

public class SimpleLever extends Widget {
    public SimpleLever(String domId, int x, int y, int height, int width,
            String... channels) {
        super(domId, x, y, height, width, channels);
    }

    public String getWidgetClassName() {
        return "nx.cab.SimpleLever";
    }
}