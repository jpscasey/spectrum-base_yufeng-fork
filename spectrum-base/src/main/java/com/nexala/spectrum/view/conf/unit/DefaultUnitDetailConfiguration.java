package com.nexala.spectrum.view.conf.unit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.common.collect.Lists;
import com.google.inject.Inject;
import com.nexala.spectrum.configuration.ApplicationConfiguration;
import com.nexala.spectrum.db.beans.Unit;
import com.nexala.spectrum.rest.data.beans.BeanField;
import com.nexala.spectrum.rest.data.beans.StockDesc;
import com.nexala.spectrum.rest.service.bean.EventFilterBean;
import com.nexala.spectrum.rest.service.bean.UnitDetailCell;
import com.nexala.spectrum.rest.service.bean.UnitDetailRow;
import com.nexala.spectrum.view.Constants;
import com.nexala.spectrum.view.Plugin;
import com.nexala.spectrum.view.PluginFactory;
import com.nexala.spectrum.view.conf.Column;
import com.nexala.spectrum.view.conf.DataNavigationType;
import com.nexala.spectrum.view.conf.Field;
import com.nexala.spectrum.view.conf.FieldBuilder;
import com.nexala.spectrum.view.conf.FieldType;
import com.nexala.spectrum.view.conf.MouseOverText;
import com.nexala.spectrum.view.conf.ScreenType;
import com.typesafe.config.Config;

public class DefaultUnitDetailConfiguration implements UnitDetailConfiguration {

    @Inject
    private PluginFactory factory;

    private final List<UnitDetailRow> udPanelRows;

    private final String pluginName;
    private final List<String> pluginList;
    private final List<String> pluginSheets;

    private final List<Field> headerFields;
    private final ApplicationConfiguration appConf;
    private final String defaultStockTabName;
    private final String defaultStockTabType;
    private final String stockTabsType;

    private final List<BeanField> workOrderFields;
    private final List<BeanField> restrictionFields;
    private final boolean showEventHistoryButton;
    private final List<Map<String, String>> maximoUrls;
    
    @Inject
    public DefaultUnitDetailConfiguration(ApplicationConfiguration appConfig) {

        this.appConf = appConfig;
        Config conf = appConfig.get().getConfig("r2m.unitDetail");

        Config udPanel = conf.getConfig("udPanel");

        // get header fields
        List<? extends Config> headerConfs = conf.getConfigList("headerFields");
        headerFields = new ArrayList<Field>();
        for (Config headerConf : headerConfs) {
            headerFields.add(buildField(headerConf));
        }
        udPanelRows = new ArrayList<UnitDetailRow>();

        List<? extends Config> panelRows = udPanel.getConfigList("rows");
        for (Config row : panelRows) {
            udPanelRows.add(buildRow(row));
        }
        
        workOrderFields = new ArrayList<BeanField>();
        if (conf.hasPath("workOrder")) {
            List<? extends Config> woFields = conf.getConfigList("workOrder");
            for (Config field : woFields) {
                workOrderFields.add(appConfig.buildBeanField(field));
            }
        }
        
        restrictionFields = new ArrayList<BeanField>();
        if (conf.hasPath("restrictions")) {
            List<? extends Config> restrictFields = conf.getConfigList("restrictions");
            for (Config field : restrictFields) {
                restrictionFields.add(appConfig.buildBeanField(field));
            }
        }
        
        this.showEventHistoryButton = conf.getBoolean("showEventHistoryButton");
        
        String defaultStockTabName = null;
        String defaultStockTabType = null;
        if (udPanel.hasPath("defaultStockTab")) {
            Config defaultStockTab = udPanel.getConfig("defaultStockTab");
            defaultStockTabName =  defaultStockTab.getString("displayName");
            defaultStockTabType = defaultStockTab.getString("type");
        }
        
        this.defaultStockTabName = defaultStockTabName;
        this.defaultStockTabType = defaultStockTabType;
        
        this.stockTabsType = udPanel.getString("stockTabsType");

        // get plugin js files
        if (conf.hasPath("plugins")) {
            Config pluginConf = conf.getConfig("plugins");
            pluginName = pluginConf.getString("name");
            pluginList = pluginConf.getStringList("sources");
            if (pluginConf.hasPath("stylesheets")) {
                pluginSheets = pluginConf.getStringList("stylesheets");                
            } else {
                pluginSheets = Lists.newArrayList();
            }
        } else {
            pluginName = new String();
            pluginList = Lists.newArrayList();
            pluginSheets = Lists.newArrayList();
        }
        
        maximoUrls = new ArrayList<Map<String, String>>();
        
        if (conf.hasPath("maximoURLs")) {
            List<? extends Config> maximoURLs = conf.getConfigList("maximoURLs");
            for(Config current: maximoURLs){
                String name = current.getString("maximoType");
                String url = current.getString("url");
                HashMap<String, String> data = new HashMap<String, String>();
                data.put(name, url);
                maximoUrls.add(data);
            }
        }        
    }
    
    @Override
    public String getDefaultStockTabName() {
        return defaultStockTabName;
    }
    
    @Override
    public String getDefaultStockTabType() {
        return defaultStockTabType;
    }
    
    @Override
    public String getStockTabsType() {
        return stockTabsType;
    }
    
    @Override
    public String getDisplayName() {
        return "Unit Detail";
    }
    
    @Override
    public String getTemplateName() {
        return "unit_unitdetail.html.ftl";
    }

    @Override
    public Plugin getParentPlugin() {
        return factory.create("nx.unit.UnitDetail",

                Lists.newArrayList("js/raphael-2.1.min.js", "js/nexala.raphael.js", "js/nexala.unit.unitdetail.js",
                        "js/nexala.unit.diagram.js"),
                Lists.newArrayList("css/nexala.unitdetail.css"));
    }

    @Override
    public ScreenType getScreenType() {
        return ScreenType.TAB;
    }

    @Override
    public DataNavigationType getDataNavigationType() {
        return DataNavigationType.TIME;
    }

    @Override
    public String getRequiredRole() {
        return Constants.UNIT_DETAIL_LIC_KEY;
    }

    @Override
    public List<Plugin> getPlugins() {
        return Lists.newArrayList(factory.create(pluginName, pluginList, pluginSheets));
    }

    @Override
    public List<StockDesc> getTabs(Unit unit) {
        ArrayList<StockDesc> tabVehicles = new ArrayList<StockDesc>();
        tabVehicles.add(new StockDesc(unit.getId(), "Unit", 0, null, true, "Unit"));

        return tabVehicles;
    }

    @Override
    public int getNumberOfHeaderColumns() {
        return 3;
    }

    @Override
    public List<Field> getHeaderFields() {
        return headerFields;
    }
    
    @Override
    public List<Map<String, String>> getMaximoUrls() {
        return maximoUrls;
    }

    private UnitDetailRow buildRow(Config rowConfig) {
        UnitDetailRow result = new UnitDetailRow();

        result.setPosition(rowConfig.getInt("position"));

        if (rowConfig.hasPath("cells")) {
            List<? extends Config> confCells = rowConfig.getConfigList("cells");
            List<UnitDetailCell> cellsList = new ArrayList<UnitDetailCell>();

            for (Config cell : confCells) {
                cellsList.add(buildCell(cell));
            }
            result.setCells(cellsList);
        }

        return result;
    }

    private UnitDetailCell buildCell(Config cellConfig) {
        UnitDetailCell result = new UnitDetailCell();

        result.setName(cellConfig.getString("name"));
        result.setTitle(cellConfig.getString("title"));
        result.setMode(cellConfig.getString("mode"));

        if (cellConfig.hasPath("columns")) {
            List<? extends Config> confColumns = cellConfig.getConfigList("columns");
            List<Column> columnsList = new ArrayList<Column>();

            for (Config column : confColumns) {
                columnsList.add(appConf.columnBuilder(column).build());
            }
            result.setColumns(columnsList);
        }

        if (cellConfig.hasPath("filters")) {
            List<? extends Config> confFilters = cellConfig.getConfigList("filters");
            List<EventFilterBean> filtersList = new ArrayList<EventFilterBean>();

            for (Config filter : confFilters) {
                filtersList.add(appConf.buildEventFilter(filter));
            }
            result.setFilters(filtersList);
        }
        
        if (cellConfig.hasPath("rowMouseOver")) {
            List<? extends Config> confMouseOver = cellConfig.getConfigList("rowMouseOver");
            List<MouseOverText> mouseOverLines = new ArrayList<MouseOverText>();
            
            for (Config line : confMouseOver) {
                mouseOverLines.add(appConf.buildMouseOver(line));
            }            
            result.setRowMouseOver(mouseOverLines);
        }

        return result;
    }

    private Field buildField(Config config) {
        FieldBuilder fieldBuilder = new FieldBuilder();

        if (config.hasPath("displayName")) {
            fieldBuilder.displayName(config.getString("displayName"));
        }
        if (config.hasPath("name")) {
            fieldBuilder.name(config.getString("name"));
        }
        if (config.hasPath("format")) {
            fieldBuilder.formatter(config.getString("format"));
        }
        if (config.hasPath("visible")) {
            fieldBuilder.visible(config.getBoolean("visible"));
        }
        if (config.hasPath("linkable")) {
            fieldBuilder.linkable(config.getBoolean("linkable"));
        }
        if (config.hasPath("referencedField")) {
            fieldBuilder.referencedField(config.getString("referencedField"));
        }
        if (config.hasPath("width")) {
            fieldBuilder.width(config.getInt("width"));
        }
        if (config.hasPath("fleets")) {
            fieldBuilder.fleets(config.getStringList("fleets"));
        }

        if (config.hasPath("type")) {
            String type = config.getString("type");
            switch (type.toUpperCase()) {
            case "SELECT":
                fieldBuilder.type(FieldType.SELECT);
                break;
            case "INPUT_TEXT":
                fieldBuilder.type(FieldType.INPUT_TEXT);
                break;
            case "MULTIPLE_SELECT":
                fieldBuilder.type(FieldType.MULTIPLE_SELECT);
                break;
            default:
                fieldBuilder.type(FieldType.LABEL);
            }
        }

        return fieldBuilder.build();
    }

    @Override
    public List<UnitDetailRow> getUdPanelRows() {
        return udPanelRows;
    }

	@Override
	public List<BeanField> getWorkOrderFields() {
	    return workOrderFields;
	}

	@Override
	public boolean isShowEventHistoryButton() {
		return showEventHistoryButton;
	}

    @Override
    public List<BeanField> getRestrictionFields() {
        return restrictionFields;
    }
}
