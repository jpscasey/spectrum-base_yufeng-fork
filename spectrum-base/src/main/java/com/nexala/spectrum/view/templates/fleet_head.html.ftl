<!-- fleet summary includes -->

<link rel = "stylesheet" type="text/css" href='css/merge?resources=
    css/nexala.fleet.css,
    css/nexala.timepicker.css,
    css/nexala.drilldown.css&view=fleet'>

<#list cssIncludesList as cssIncludes>
    <link rel = 'stylesheet' type = 'text/css' href = 'css/merge?resources=
        <#list cssIncludes as cssInclude>${cssInclude},</#list>
        &view=fleet
    '>
</#list>

<script type = "text/javascript" src = "js/merge?resources=
    js/nexala.fleet.fleetsummary.js,
    js/nexala.fleet.drilldown.js,
    js/i18n/datepicker-nl.js,
    js/i18n/datepicker-en-GB.js,
    js/nexala.timepicker.js,
    js/nexala.data.js&view=fleet"></script>
    
<#list jsIncludesList as jsIncludes>
    <script type = 'text/javascript' src = 'js/merge?resources=
        <#list jsIncludes as jsInclude>${jsInclude},</#list>
        &view=fleet
    '></script>
</#list>
    
<script type = 'text/javascript'>

    $(document).ready(function() {
        <#list conf.plugins as plugin>
            <#if plugin.pluginClass?has_content>
                var clazz = eval(${plugin.pluginClass});
                var plugInst = new clazz();
            </#if>
        </#list> 
    });
</script>
