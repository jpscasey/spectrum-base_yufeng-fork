package com.nexala.spectrum.view.conf.maps;

/**
 * Marker for WSP and Sand.
 *
 */
public class AdhesionMarker extends MapMarker {

    private Long timestamp;
    private Double bearing;
    private Integer category;
    private Integer value;
    
    /**
     * Default constructor
     */
    public AdhesionMarker() {
        
    }
    
    public AdhesionMarker(Double latitude, Double longitude, Long timestamp,
            Double bearing, Integer category, Integer value) {
        setTimestamp(timestamp);
        setBearing(bearing);
        setCategory(category);
        setLatitude(latitude);
        setLongitude(longitude);
        setValue(value);
    }

    /**
     * Getter for timestamp.
     * @return the timestamp
     */
    public Long getTimestamp() {
        return timestamp;
    }

    /**
     * Setter for timestamp.
     * @param timestamp the timestamp to set
     */
    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    /**
     * Getter for bearing.
     * @return the bearing
     */
    public Double getBearing() {
        return bearing;
    }

    /**
     * Setter for bearing.
     * @param bearing the bearing to set
     */
    public void setBearing(Double bearing) {
        this.bearing = bearing;
    }

    /**
     * Getter for category.
     * @return the category
     */
    public Integer getCategory() {
        return category;
    }

    /**
     * Setter for category.
     * @param category the category to set
     */
    public void setCategory(Integer category) {
        this.category = category;
    }

	/**
	 * @return the value
	 */
	public Integer getValue() {
		return value;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(Integer value) {
		this.value = value;
	}
}
