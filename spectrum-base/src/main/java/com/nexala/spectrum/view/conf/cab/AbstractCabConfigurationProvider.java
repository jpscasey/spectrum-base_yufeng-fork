/*
 * Copyright (c) Nexala Technologies 2013, All rights reserved.
 */
package com.nexala.spectrum.view.conf.cab;

import com.google.common.collect.Lists;
import com.google.inject.Inject;
import com.nexala.spectrum.licensing.RestrictedResource;
import com.nexala.spectrum.rest.service.CabConfigurationProvider;
import com.nexala.spectrum.view.Constants;
import com.nexala.spectrum.view.Plugin;
import com.nexala.spectrum.view.PluginFactory;
import com.nexala.spectrum.view.conf.AbstractDetailScreen;
import com.nexala.spectrum.view.conf.Linkable;
import com.nexala.spectrum.view.conf.ScreenType;


@RestrictedResource(Constants.UNIT_CABVIEW_LIC_KEY)
public abstract class AbstractCabConfigurationProvider extends AbstractDetailScreen implements CabConfigurationProvider, Linkable {
    @Inject
    private PluginFactory factory;
    
    @Override
    public String getRequiredRole() {
        return Constants.UNIT_CABVIEW_LIC_KEY;
    }
    
    @Override
    public String getTemplateName() {
        return "unit_cab.html.ftl";
    }
    
    @Override
    public String getDisplayName() {
        return "Cab";
    }
    
    @Override
    public Plugin getParentPlugin() {
        return factory.create(
                "nx.unit.Cab",
                Lists.newArrayList(
                    "js/raphael-2.1.min.js",
                    "js/nexala.raphael.js",
                    "js/nexala.cab.widget.js",
                    "js/nexala.cab.js",
                    "js/nexala.cab.arrow.js",
                    "js/nexala.cab.aws.js",
                    "js/nexala.cab.button.js",
                    "js/nexala.cab.caws.js",
                    "js/nexala.cab.clearbutton.js",
                    "js/nexala.cab.clock.js",
                    "js/nexala.cab.gauge.js",
                    "js/nexala.cab.indicator.js",
                    "js/nexala.cab.led.js",
                    "js/nexala.cab.lever.js",
                    "js/nexala.cab.simple-lever.js",
                    "js/nexala.cab.slider.js",
                    "js/nexala.cab.switch.js",
                    "js/nexala.cab.uncouple.js",
                    "js/nexala.cab.value.js",
                    "js/nexala.cab.timestamp.js"
                ),
                Lists.newArrayList("css/nexala.cab.css")
         );
    }
    
    @Override
    public ScreenType getScreenType() {
        return ScreenType.TAB;
    }
    
    
    /* Override this method for irish rail... :)
    @Override
    public List<String> getJsPluginClassNames() {
        return Lists.newArrayList("nx.unit.Mmi");
    }
    */
    
    @Override
    public String getLink() {
        return "cabViewTab";
    }

    @Override
    public String getIconClass() {
        return "ui-icon-key";
    }
}
