/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.view.conf.cab;

public class Button extends Widget {
    private ButtonSettings settings;
    
    public Button(String domId, int x, int y, int height, int width,
            ButtonSettings settings,
            String... channels) {
        super(domId, x, y, height, width, channels);
        this.settings = settings;
    }
    
    @Override
    public Settings getSettingsObject() {
        return settings;
    }
    
    @Override
    public String getWidgetClassName() {
        return "nx.cab.Button";
    }
}
