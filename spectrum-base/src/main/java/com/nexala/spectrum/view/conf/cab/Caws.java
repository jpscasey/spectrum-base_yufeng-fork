/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.view.conf.cab;

public class Caws extends Widget {
    public Caws(String domId, int x, int y, int h, int w, String... channels) {
        super(domId, x, y, h, w, channels);
    }

    @Override
    public String getWidgetClassName() {
        return "nx.cab.Caws";
    }
}