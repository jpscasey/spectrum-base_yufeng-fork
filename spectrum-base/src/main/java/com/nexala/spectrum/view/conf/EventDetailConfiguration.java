/*
 * Copyright (c) Nexala Technologies 2013, All rights reserved.
 */
package com.nexala.spectrum.view.conf;

import java.util.List;
import java.util.Map;

import com.google.inject.ImplementedBy;
import com.nexala.spectrum.rest.data.beans.EventDetailRenderer;
import com.nexala.spectrum.rest.data.beans.EventDetailToolbarButton;
import com.nexala.spectrum.rest.data.beans.UnitSummaryButton;
import com.nexala.spectrum.rest.service.bean.LicenceConfigurationBean;

@ImplementedBy(DefaultEventDetailConfiguration.class)
public interface EventDetailConfiguration {
    /**
     * Returns true if a map should be displayed below the event detail
     * configuration. In order for this to work the event must implement the
     * HasCoordinates interface
     */
    boolean isMapEnabled();

    /**
     * Returns a list of event status actions.
     * Ex:
     * 
     * Status P (Pending) could have the action named as "R" (Reject) or "V" (Validate)
     * P: {R, Reject}, {V, Validate}
     * 
     * Status V (Validated) could have the action named as "C" (Close)
     * V: {C, Closed}
     * 
     * The first example will enable the option to manually change the event status and 
     * the second one will enable the closure of the event.
     * 
     * The combinations of "Status" and "Actions" must match the event status and available actions.
     * 
     * Empty or null list will disable this feature.
     * 
     * @return
     */
    Map<String, Map<String, String>> getEventStatusActions();

    /**
     * Returns an object to tell the Javascript how to render the event details
     * @return event details rendering info
     */
    EventDetailRenderer getEventDetailRenderer();

    /**
     * Gets the list of status "columns" to display on the left on the event
     * detail screen. These "columns" contain information from the Event table,
     * such as the event code, timestamp and location.
     * 
     * @return
     */
    List<Field> getInfoFields();

    /**
     * Returns a map containing an entry per each fleet with the fleet ID and a
     * boolean value which controls the visibility of vehicle tabs
     */
    Map<String, Boolean> getDisplayVehicleTabs();
    
    boolean isEventDataTabEnabled();
    
    String getHeaderDisplayField();
    
	boolean isEventDataTabCommentsEnabled();
    
	boolean isMouseOverHeaderEnabled();
	
	List<MouseOverText> getHeaderMouseOver();
    
    List<Field> getDataTabFields();
    
    boolean isActionButtonsEnabled();

    List<EventDetailToolbarButton> getToolbarButtons();
	
	boolean isNavigationButtonsEnabled();

	List<UnitSummaryButton> getNavigationButtons();

	boolean isEventDataTabEventGroups();
	
	List<LicenceConfigurationBean> getLicences(); 


}
