/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.view.conf;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.inject.Inject;
import com.nexala.spectrum.configuration.ApplicationConfiguration;
import com.nexala.spectrum.rest.data.beans.EventDetailRenderer;
import com.nexala.spectrum.rest.data.beans.EventDetailToolbarButton;
import com.nexala.spectrum.rest.data.beans.UnitSummaryButton;
import com.nexala.spectrum.rest.service.bean.LicenceConfigurationBean;
import com.typesafe.config.Config;

public class DefaultEventDetailConfiguration implements EventDetailConfiguration {
	
	private final ApplicationConfiguration appConf;

    private final List<Field> fields;

    private final boolean mapEnabled;
    
    private final boolean eventDataTabEnabled;
    
    private final boolean eventDataTabCommentsEnabled;
    
    private final String headerDisplayField;
    
    private final boolean mouseOverHeaderEnabled;
    
    private final List<MouseOverText> headerMouseOver;
    
    private final Map<String, Boolean> displayVehicleTabs;
    
    private final EventDetailRenderer eventDetailRenderer;
    
    private final List<Field> eventDataTabFields;
    
    private final boolean actionButtonsEnabled;
    
    private final List<EventDetailToolbarButton> toolbarButtons;
    
    private final boolean navigationButtonsEnabled;
    
    private final List<UnitSummaryButton> navigationButtons;
    
    private final boolean eventDataTabEventGroups;
    
    private final List<LicenceConfigurationBean> eventLicences;
    
    private final boolean staticSiblings;
    
    private final boolean linkableEvents;
    
    private final boolean showSlider;
    
    private final List<Integer> periodsListEvent;
    
    private final int defaultScrollValue;

    @Inject
    public DefaultEventDetailConfiguration(ApplicationConfiguration applicationConfiguration) {
    	
    	appConf = applicationConfiguration;

        Config eventDetailConfig = applicationConfiguration.get().getConfig("r2m.eventDetail");

        List<? extends Config> fields = eventDetailConfig.getConfigList("fields");
        this.fields = new ArrayList<Field>();

        for (Config fieldConfig : fields) {
            this.fields.add(applicationConfiguration.buildField(fieldConfig, null));
        }
        
        List<? extends Config> dataTabFields = eventDetailConfig.getConfigList("dataTabFields");
        this.eventDataTabFields = new ArrayList<Field>();
        
        for (Config fieldConfig : dataTabFields) {
        	this.eventDataTabFields.add(applicationConfiguration.buildField(fieldConfig, null));
        }
        
        List<Integer> periodsList = eventDetailConfig.getIntList("periodsListEvent");
        this.periodsListEvent = new ArrayList<Integer>();
        
        for (Integer fieldConfig : periodsList) {
        	this.periodsListEvent.add(fieldConfig);
        }
        
        this.defaultScrollValue =  eventDetailConfig.getInt("defaultScrollValue");
        
		
        // Licence configuration
		List<? extends Config> licenceConfigList = eventDetailConfig.getConfigList("licences");
		this.eventLicences = new ArrayList<>(licenceConfigList.size());
		for (Config licenceConfig: licenceConfigList) {
			LicenceConfigurationBean licenceConfigBean = new LicenceConfigurationBean();
			licenceConfigBean.setPermission(licenceConfig.getString("permission"));
			licenceConfigBean.setLicenceKey(licenceConfig.getString("licenceKey"));			
			this.eventLicences.add(licenceConfigBean);
		}
        
        // toolbar configuration
        Config toolbarConfig = eventDetailConfig.getConfig("toolbar");
        
        this.actionButtonsEnabled = toolbarConfig.getBoolean("actionsEnabled");
        
        this.toolbarButtons = new ArrayList<EventDetailToolbarButton>();
        for(Config conf : toolbarConfig.getConfigList("actions")) {
        	this.toolbarButtons.add(buildToolbarButton(conf));
        }
        
        this.navigationButtonsEnabled = toolbarConfig.getBoolean("navigationEnabled");
        
        this.navigationButtons = new ArrayList<UnitSummaryButton>();
        for(Config conf : toolbarConfig.getConfigList("navigation")) {
        	this.navigationButtons.add(this.buildButton(conf));
        }

        this.mapEnabled = eventDetailConfig.getBoolean("mapEnabled");
        this.headerDisplayField = eventDetailConfig.getString("channelTable.headerDisplayField");
        this.mouseOverHeaderEnabled = eventDetailConfig.getBoolean("mouseOverHeaderEnabled");
        this.eventDataTabEnabled = eventDetailConfig.getBoolean("eventDataTabEnabled");
        this.eventDataTabCommentsEnabled = eventDetailConfig.getBoolean("eventDataTabComments");
        this.eventDataTabEventGroups = eventDetailConfig.getBoolean("eventDataTabEventGroups");
        this.linkableEvents = eventDetailConfig.getBoolean("linkableEvents");
        if (eventDetailConfig.hasPath("channelTable")) {
            List<MouseOverText> mouseOverContent = new ArrayList<MouseOverText>();
            
            Config conf = eventDetailConfig.getConfig("channelTable");
            List<? extends Config> mouseOverLines = conf.getConfigList("headerMouseOver");

            for (Config line : mouseOverLines) {
                mouseOverContent.add(applicationConfiguration.buildMouseOver(line));
            }
            headerMouseOver = mouseOverContent;
        } else {
            headerMouseOver = null;
        }
        
        //Store the value of displayVehicleTabs from project.conf (application.conf if not defined in project)
        boolean displayVehicleTabsCheck = eventDetailConfig.getBoolean("displayVehicleTabs");

        this.displayVehicleTabs = new HashMap<String, Boolean>();
        Map<String, Config> fleets = applicationConfiguration.getFleetConfigList();
        
        //If displayVehicleTabs is true, we then check if the fleet is vehicle level
        //If the fleet is vehicle level, only then we display the tabs
        for (Map.Entry<String, Config> fleetConfig : fleets.entrySet()) {
    		boolean displayVehicle = displayVehicleTabsCheck ? fleetConfig.getValue().getBoolean("vehicleLevel") : false;
            this.displayVehicleTabs.put(fleetConfig.getKey(), displayVehicle);
        }
        
        this.staticSiblings = eventDetailConfig.getBoolean("staticSiblings");
        this.showSlider = eventDetailConfig.getBoolean("showSlider");
        Config rendererConfig = eventDetailConfig.getConfig("renderer");
        String rendererName = rendererConfig.getString("name");
        String rendererFile = rendererConfig.getString("file");

        List<String> additionalCss = new ArrayList<String>();
        if (rendererConfig.hasPath("additionalCss")) {
            additionalCss = rendererConfig.getStringList("additionalCss");
        }

        List<String> additionalJs = new ArrayList<String>();
        if (rendererConfig.hasPath("additionalJs")) {
            additionalJs = rendererConfig.getStringList("additionalJs");
        }

        this.eventDetailRenderer = new EventDetailRenderer(rendererName, rendererFile, additionalCss, additionalJs);

    }
    
    
    public boolean isShowSlider() {
		return showSlider;
	}


	@Override
    public EventDetailRenderer getEventDetailRenderer() {
        return eventDetailRenderer;
    }
    
    @Override
    public List<Field> getInfoFields() {
        return fields;
    }
    
    @Override
    public boolean isMapEnabled() {
        return mapEnabled;
    }

    @Override
    public Map<String, Boolean> getDisplayVehicleTabs() {
        return displayVehicleTabs;
    }

	@Override
    public boolean isMouseOverHeaderEnabled() {
        return mouseOverHeaderEnabled;
    }

    @Override
    public Map<String, Map<String, String>> getEventStatusActions() {
        return new HashMap<String, Map<String, String>>();
    }

    private UnitSummaryButton buildButton(Config buttonConfig) {
        String id = buttonConfig.getString("id");
        String name = buttonConfig.getString("displayName");
        Boolean openUnitSummary = buttonConfig.getBoolean("openUnitSummary");

        String unitSummaryTab = null;
        if (buttonConfig.hasPath("unitSummaryTab")) {
            unitSummaryTab = buttonConfig.getString("unitSummaryTab");
        }

        String alternativeAction = null;
        if (buttonConfig.hasPath("alternativeAction")) {
            alternativeAction = buttonConfig.getString("alternativeAction");
        }

        return new UnitSummaryButton(id, name, openUnitSummary, unitSummaryTab, alternativeAction);
    }
    
    private EventDetailToolbarButton buildToolbarButton(Config btnConfig) {
    	String name = btnConfig.getString("name");
    	String displayName = btnConfig.getString("displayName");
    	Boolean opensPopup = btnConfig.getBoolean("opensPopup");
    	Integer popupHeight;
    	Integer popupWidth;
    	List<Field> fields = new ArrayList<Field>();
    	if (opensPopup) {
    	    popupHeight = btnConfig.getInt("popupHeight");
            popupWidth = btnConfig.getInt("popupWidth");
            for (Config popupField : btnConfig.getConfigList("popupFields")) {
                fields.add(appConf.buildField(popupField, null));
            }
    	} else {
    	    popupHeight = 0;
    	    popupWidth = 0;
    	}
    	Boolean isLicenced = btnConfig.getBoolean("isLicenced");
        return new EventDetailToolbarButton(name, displayName, opensPopup, popupHeight, popupWidth, isLicenced, fields);
    }
    
	@Override
	public boolean isEventDataTabEnabled() {
		return eventDataTabEnabled;
	}
	
	@Override
	public boolean isEventDataTabCommentsEnabled() {
		return eventDataTabCommentsEnabled;
	}
	
	@Override
	public boolean isEventDataTabEventGroups() {
		return eventDataTabEventGroups;
	}

	@Override
	public List<Field> getDataTabFields() {
		return eventDataTabFields;
	}
	
	@Override
	public boolean isActionButtonsEnabled() {
		return actionButtonsEnabled;
	}
	
	@Override
	public  List<EventDetailToolbarButton> getToolbarButtons() {
		return toolbarButtons;
	}
	
	@Override
	public boolean isNavigationButtonsEnabled() {
		return navigationButtonsEnabled;
	}
	
	@Override
	public List<UnitSummaryButton> getNavigationButtons() {
		return navigationButtons;
	}
	
	@Override
	public List<LicenceConfigurationBean> getLicences() {
		return eventLicences;
	}
    
	@Override
    public List<MouseOverText> getHeaderMouseOver() {
        return headerMouseOver;
    }

    public String getHeaderDisplayField() {
        return headerDisplayField;
    }
    
    public boolean isStaticSiblings() {
        return staticSiblings;
    }

    public boolean isLinkableEvents() {
        return linkableEvents;
    }


	public List<Integer> getPeriodsListEvent() {
		return periodsListEvent;
	}


	public int getDefaultScrollValue() {
		return defaultScrollValue;
	}
	
    
}
