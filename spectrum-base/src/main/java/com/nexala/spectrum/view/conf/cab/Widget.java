/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.view.conf.cab;

import java.util.HashSet;
import java.util.Set;

public class Widget extends Component {
    
    private Set<String> classNames;
    final String[] channels;

    public Widget(String domId, int x, int y, int height, int width,
            String... channels) {
        super(domId, x, y, height, width);
        classNames = new HashSet<String>();
        classNames.add("widget");
        this.channels = channels;
    }

    @Override
    public String getClassName() {
        String classNamesSt= "";
        for(String s : classNames) {
            classNamesSt += s + " ";
        }
        return classNamesSt;
    }

    public String[] getChannels() {
        return channels;
    }
    
    public Widget addClassName(String className) {
        classNames.add(className);
        return this;
    }
}