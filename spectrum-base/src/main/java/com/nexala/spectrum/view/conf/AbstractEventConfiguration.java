/*
 * Copyright (c) Nexala Technologies 2013, All rights reserved.
 */
package com.nexala.spectrum.view.conf;

import java.util.Arrays;
import java.util.Collections;

import com.nexala.spectrum.view.Constants;
import com.nexala.spectrum.view.Plugin;

public abstract class AbstractEventConfiguration extends AbstractDetailScreen implements EventConfiguration {
    
    public String getRequiredRole() {
        return Constants.UNIT_EVENT_LIC_KEY;
    }
    
    @Override
    public String getTemplateName() {
        return null; // FIXME 
    }
    
    public String getDisplayName() {
        return "Events"; // FIXME i18n
    }
    
    @Override
    public String getScreenName() {
        return "Event History";
    }
    
    public Plugin getParentPlugin() {
        Plugin plugin = new Plugin("", Arrays.asList("js/jstz.js"),
                Collections.<String> emptyList());
         
        return plugin;
    }
}
