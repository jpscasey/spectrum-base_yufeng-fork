/*
 * Copyright (c) Nexala Technologies 2013, All rights reserved.
 */
package com.nexala.spectrum.view.conf;

import java.util.ArrayList;
import java.util.List;

import com.google.inject.Inject;
import com.nexala.spectrum.configuration.ApplicationConfiguration;
import com.nexala.spectrum.licensing.Licence;
import com.nexala.spectrum.rest.service.CabConfigurationProvider;
import com.nexala.spectrum.view.conf.unit.ChannelDataConfiguration;
import com.nexala.spectrum.view.conf.unit.DataPlotConfiguration;
import com.nexala.spectrum.view.conf.unit.DefaultRecoveryConfiguration;
import com.nexala.spectrum.view.conf.unit.UnitDetailConfiguration;
import com.typesafe.config.Config;

public class DefaultUnitConfiguration extends AbstractUnitConfiguration {
   
    // Note: If any of the modules are not required, a binding may be omitted.

	private Boolean unitSummaryHeaderCombined = null;

    @Inject(optional = true)
    private UnitDetailConfiguration unitConf;

    @Inject(optional = true)
    private CabConfigurationProvider cabConf;
    
    @Inject(optional = true)
    private DefaultSchematicConfiguration schematicConf;

    @Inject(optional = true)
    private ChannelDataConfiguration channelConf;

    @Inject(optional = true)
    private EventConfiguration eventConf;

    @Inject(optional = true)
    private DataPlotConfiguration dataConf;

    @Inject(optional = true)
    private DefaultMapConfiguration mapConf;
    
    @Inject(optional = true)
    private DefaultRecoveryConfiguration recoveryConf;

    @Inject
    public DefaultUnitConfiguration(ApplicationConfiguration appConfig) {
		Config conf = appConfig.get().getConfig("r2m.unitSummary");
        if (conf.hasPath("combinedHeader")) {
        	unitSummaryHeaderCombined =  conf.getBoolean("combinedHeader");
        }
	 }
    
    @Override
    public List<DetailScreen> getScreens(Licence licence) {
        List<DetailScreen> screens = new ArrayList<DetailScreen>();
        
        if (unitConf != null && licence.hasResourceAccess(unitConf.getRequiredRole())) {
            screens.add(unitConf);
        }
        
        if (channelConf != null && licence.hasResourceAccess(channelConf.getRequiredRole())) {
            screens.add(channelConf);
        }
        
        if (eventConf != null && licence.hasResourceAccess(eventConf.getRequiredRole())) {
            screens.add(eventConf);
        }
        
        if (dataConf != null && licence.hasResourceAccess(dataConf.getRequiredRole())) {
            screens.add(dataConf);
        }
        
        if (mapConf != null && licence.hasResourceAccess(mapConf.getRequiredRole())) {
            screens.add(mapConf);
        }
        
        if (cabConf != null && licence.hasResourceAccess(cabConf.getRequiredRole())) {
            screens.add(cabConf);
        }
        
        if (recoveryConf != null && licence.hasResourceAccess(recoveryConf.getRequiredRole())) {
            screens.add(recoveryConf);
        }
        
        if (schematicConf != null && licence.hasResourceAccess(schematicConf.getRequiredRole())) {
        	screens.add(schematicConf);
        }

        return screens;
    }

    @Override
    public int getUnitSelectorWidth() {
        return 250;
    }
    
    @Override
	public Boolean isUnitSummaryHeaderCombined() {
        return unitSummaryHeaderCombined;
	}
}
