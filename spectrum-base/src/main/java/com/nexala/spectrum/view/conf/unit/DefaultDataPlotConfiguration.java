/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.view.conf.unit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.inject.Inject;
import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.configuration.ApplicationConfiguration;
import com.nexala.spectrum.configuration.ConfigurationManager;
import com.nexala.spectrum.db.beans.ChannelGroup;
import com.nexala.spectrum.view.Constants;
import com.nexala.spectrum.view.conf.ChartConfiguration;
import com.nexala.spectrum.view.conf.LabelValue;
import com.nexala.spectrum.view.conf.MouseOverText;
import com.nexala.spectrum.view.conf.ScreenType;
import com.typesafe.config.Config;

public class DefaultDataPlotConfiguration extends AbstractDataPlotConfiguration implements DataPlotConfiguration  {
	
    @Inject
    private ConfigurationManager confManager;
    
	private Map<String, Boolean> chartEnabledByFleet = new HashMap<String, Boolean>();
	
	private Boolean virtualScrollBar;
	
	private Boolean isSystemChartsIncluded;
	
	private LabelValue channelGroupLabel;
	
	private LabelValue tableChannelLabel;

	@Inject
    public DefaultDataPlotConfiguration(ApplicationConfiguration appConfig) {
		Config conf = appConfig.get().getConfig("r2m.dataView");
		
		Config fleets = conf.getConfig("fleets");
		List<String> fleetIds = new ArrayList<String>(fleets.root().unwrapped().keySet());
        for (String fleetId : fleetIds) {
        	chartEnabledByFleet.put(fleetId, fleets.getConfig(fleetId).getBoolean("isChartEnabled"));
        }
		virtualScrollBar = conf.getBoolean("virtualScrollBar");
		isSystemChartsIncluded = conf.getBoolean("isSystemChartsIncluded");
		
		Config channelGroups = conf.getConfig("channelGroups");
		channelGroupLabel = appConfig.buildLabelValue(channelGroups.getConfig("label"), null);
		
		Config tableChannel = conf.getConfig("tableChannel");
		tableChannelLabel = appConfig.buildLabelValue(tableChannel.getConfig("label"), null);
	}
	
	
	@Override
    public boolean getVirtualScrollBar() {
        return virtualScrollBar;
    }

    @Override
    public String getDisplayName() {
        // XXX i18n
        return "Data View";
    }

    @Override
    public String getTemplateName() {
        return "unit_dataview.html.ftl";
    }

    @Override
    public ScreenType getScreenType() {
        return ScreenType.TAB;
    }
    
    @Override
    public List<ChannelGroup> getChannelGroups() {
        return new ArrayList<ChannelGroup>();
    }

    @Override
    public String getRequiredRole() {
        return Constants.UNIT_DATAPLOTS_LIC_KEY;
    }

	@Override
	public boolean isSystemChartsIncluded() {
		return isSystemChartsIncluded;
	}

	@Override
	public Map<String, Boolean> getChartEnabledByFleet() {
		return chartEnabledByFleet;
	}
	
	@Override
	public LabelValue getChannelLabelConfig() {
	    return channelGroupLabel;
	}
	
	@Override
	public LabelValue getTableChannelLabelConfig() {
	    return tableChannelLabel;
	}

	public ChartConfiguration getChartConfiguration() {
		ChartConfiguration result = new ChartConfiguration();
		
		Integer refreshPeriod = confManager.getConfAsInteger(Spectrum.SPECTRUM_DATAPLOT_REFRESH_PERIOD);
        Integer defaultPeriod = confManager.getConfAsInteger(Spectrum.SPECTRUM_DATAPLOT_DEFAULT_PERIOD, 2);
        Boolean exportPDF = confManager.getConfAsBoolean(Spectrum.SPECTRUM_DATAPLOT_EXPORT_PDF, false);
        Boolean refreshNotLive = confManager.getConfAsBoolean(Spectrum.SPECTRUM_DATAPLOT_REFRESH_NOT_LIVE, false);
		
        List<Long> periods = new ArrayList<Long>();
        String periodsString = confManager.getConfAsString(Spectrum.SPECTRUM_DATAPLOT_PERIODS, 
                "1000,10000,30000,60000,120000,600000,1800000,3600000,7200000");
        String[] periodsStringSep = periodsString.split(",");
        for (int i = 0; i < periodsStringSep.length; i++) {
            periods.add(Long.parseLong(periodsStringSep[i]));
        }       

        result.setRefreshPeriod(refreshPeriod);
        result.setDefaultPeriod(defaultPeriod);
        result.setPeriods(periods);
        result.setExportPDF(exportPDF);
        result.setRefreshNotLive(refreshNotLive);
        
        return result;
	}

}
