package com.nexala.spectrum.view.conf;

import java.util.List;

public class FieldBuilder {
    private String displayName = null;

    private String licence = null;

    private String name = null;

    // This is a nasty way to apply formats..
    private String formatter = null;

    /** Set some reasonable defaults */
    private FieldType type = FieldType.LABEL;

    private boolean visible = true;

    private Integer width = 20;

    private boolean resizable;

    private boolean reference;

    private String referencedField;

    private boolean editable;
    
    private boolean mandatory;

    private String editHandler;

    private String editLicence;

    private boolean linkable = false;

    private List<String> fleets;
    
    private Integer maxLength;
    
    // min value for numeric fields
    private Integer min;

    // max value for numeric fields
    private Integer max;
    
    // style
    private String style = null;
    
    private List<Column> columns;
    
    /**
     * List of possible values, if the field is a dropdown for example.
     */
    private List<LabelValue> values;
    
    private List<DropdownOption> dropdownOptions;
    
    private List<DisabledCondition> disabledConditions;
    
    private Boolean sameRow;
    
    private List<String> usages;
    
    private String unit;
    
    public FieldBuilder() {}

    public Field build() {
        return new Field(this);
    }

    public FieldBuilder displayName(String displayName) {
        this.displayName = displayName;
        return this;
    }

    public FieldBuilder formatter(String formatter) {
        this.formatter = formatter;
        return this;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getFormatter() {
        return formatter;
    }

    public String getLicence() {
        return licence;
    }

    public String getName() {
        return name;
    }

    public FieldType getType() {
        return type;
    }

    public Integer getWidth() {
        return width;
    }

    public boolean isResizable() {
        return resizable;
    }

    public boolean isVisible() {
        return visible;
    }

    public boolean isReference() {
        return reference;
    }

    public String getReferencedField() {
        return referencedField;
    }

    public FieldBuilder licence(String licence) {
        this.licence = licence;
        return this;
    }

    public FieldBuilder name(String name) {
        this.name = name;
        return this;
    }

    public FieldBuilder resizable(boolean resizable) {
        this.resizable = resizable;
        return this;
    }

    public FieldBuilder type(FieldType type) {
        this.type = type;
        return this;
    }

    public FieldBuilder visible(boolean visible) {
        this.visible = visible;
        return this;
    }

    public FieldBuilder width(Integer width) {
        this.width = width;
        return this;
    }

    public FieldBuilder referencedField(String referencedField) {
        this.reference = true;
        this.referencedField = referencedField;
        return this;
    }

    public boolean isEditable() {
        return editable;
    }

    public boolean isMandatory() {
        return mandatory;
    }

    public void setMandatory(boolean mandatory) {
        this.mandatory = mandatory;
    }

    public String getEditHandler() {
        return editHandler;
    }

    public String getEditLicence() {
        return editLicence;
    }

    public FieldBuilder editHandler(String editHandler, String editLicence) {
        if (editHandler != null && editHandler.trim().length() > 0 && editLicence != null
                && editLicence.trim().length() > 0) {

            this.editable = true;
            this.editHandler = editHandler.trim();
            this.editLicence = editLicence.trim();
        }

        return this;
    }
    
    public FieldBuilder editable(boolean editable) {
    	this.editable = editable;
    	return this;
    }
    
    public FieldBuilder mandatory(boolean mandatory) {
        this.mandatory = mandatory;
        return this;
    }

    public boolean isLinkable() {
        return linkable;
    }

    public FieldBuilder linkable(boolean linkable) {
        this.linkable = linkable;
        return this;
    }

    public List<String> getFleets() {
        return fleets;
    }

    public FieldBuilder fleets(List<String> fleets) {
        this.fleets = fleets;
        return this;
    }
    
    public FieldBuilder maxLength(Integer maxLength) {
        this.maxLength = maxLength;
        return this;
    }
    
    public Integer getMaxLength() {
    	return maxLength;
    }
    
    public FieldBuilder min(Integer min) {
        this.min = min;
        return this;
    }
    
    public Integer getMin() {
    	return min;
    }
    
    public FieldBuilder max(Integer max) {
        this.max = max;
        return this;
    }
    
    public Integer getMax() {
    	return max;
    }
    
    public FieldBuilder style(String style) {
        this.style = style;
        return this;
    }
    
    public String getStyle() {
        return style;
    }
    
    public FieldBuilder columns(List<Column> columns) {
        this.columns = columns;
        return this;
    }
    
    public List<Column> getColumns() {
        return this.columns;
    }

    public List<LabelValue> getValues() {
        return values;
    }

    public void setValues(List<LabelValue> values) {
        this.values = values;
    }

    public Boolean getSameRow() {
        return sameRow;
    }

    public void setSameRow(Boolean sameRow) {
        this.sameRow = sameRow;
    }

    public List<DropdownOption> getDropdownOptions() {
        return dropdownOptions;
    }

    public void setDropdownOptions(List<DropdownOption> dropdownOption) {
        this.dropdownOptions = dropdownOption;
    }

	public List<DisabledCondition> getDisabledConditions() {
		return disabledConditions;
	}

	public void setDisabledConditions(List<DisabledCondition> disabledConditions) {
		this.disabledConditions = disabledConditions;
	}

	public List<String> getUsages() {
		return usages;
	}

	public void setUsages(List<String> usages) {
		this.usages = usages;
	}
	
	public String getUnit() {
		return unit;
	}
	
	public void setUnit(String unit) {
		this.unit = unit;
	}
}
