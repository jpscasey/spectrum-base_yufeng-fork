/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.view.conf.cab;

public class Gauge extends Widget {
    
    private GaugeSettings settings;
    
    public Gauge(String domId, int x, int y, int h, int w,
            GaugeSettings settings, String... channels) {
        super(domId, x, y, h, w, channels);
        this.settings = settings;
    }
    
    @Override
    public Settings getSettingsObject() {
        return settings;
    }
    
    @Override
    public String getWidgetClassName() {
        return "nx.cab.Gauge";
    }
}