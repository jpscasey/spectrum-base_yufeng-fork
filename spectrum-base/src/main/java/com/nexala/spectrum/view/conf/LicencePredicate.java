package com.nexala.spectrum.view.conf;

import com.google.common.base.Predicate;
import com.nexala.spectrum.licensing.Licence;

/** This class is used for filtering a list of Applications, by a licence */
class LicencePredicate implements Predicate<App> {
    private final Licence licence;

    LicencePredicate(Licence licence) {
        this.licence = licence;
    }

    @Override
    public boolean apply(App app) {
        final String[] licenceKeys = app.getLicenceKeys();

        if (app.isVisible() && licence != null) {
            if (licenceKeys == null || licenceKeys.length == 0) {
                return true;
            } else {
                for (String key : licenceKeys) {
                    if (licence.hasResourceAccess(key)) {
                        return true;
                    }
                }

                return false;
            }
        } else {
            return false;
        }
    }
}