/*
 * Copyright (c) Nexala Technologies 2016, All rights reserved.
 */
package com.nexala.spectrum.view.conf.maps;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.google.inject.Inject;
import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.licensing.Licence;
import com.nexala.spectrum.mapping.AdhesionCluster;
import com.nexala.spectrum.mapping.Coordinate;
import com.nexala.spectrum.mapping.Mapping;
import com.nexala.spectrum.mapping.SimpleClusterizer;
import com.nexala.spectrum.rest.data.AdhesionProvider;
import com.nexala.spectrum.rest.data.FleetProvider;
import com.nexala.spectrum.rest.data.beans.AdhesionReport;

public class DefaultMapMarkerProvider implements MapMarkerProvider {

    @Inject
    protected Map<String, AdhesionProvider> adhesionProviders;

    @Inject
    protected Map<String, FleetProvider> fleetProviders;
    
    protected SimpleDateFormat reportDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

    @Override
    public List<?> getMarkerData(Coordinate upperLeft, Coordinate lowerRight,
            int zoom, Object data, String markerType, Licence licence) {
        
        List<?> result = null;
        
        if (Spectrum.MAP_WSP_MARKER.equals(markerType) || Spectrum.MAP_SAND_MARKER.equals(markerType) || Spectrum.MAP_OHLV_MARKER.equals(markerType)) {
            Map<Integer, LinkedList<AdhesionMarker>> markers = new HashMap<Integer, LinkedList<AdhesionMarker>>();
            for (String fleetId : fleetProviders.keySet()) {
                String fleetLicence = fleetProviders.get(fleetId).getLicence(fleetId);
                if (fleetLicence == null || licence.hasResourceAccess(fleetLicence)) {
                    Map<Integer, LinkedList<AdhesionMarker>> fleetMarkers = getAdhesionMarkerData(upperLeft,
                            lowerRight, data, markerType, fleetId);
                    for (Integer cat : fleetMarkers.keySet()) {
                        LinkedList<AdhesionMarker> markersList = markers.get(cat);
                        if (markersList == null) {
                            markersList = new LinkedList<AdhesionMarker>();
                        }
                        markersList.addAll(fleetMarkers.get(cat));
                        markers.put(cat, markersList);
                    }
                }
            }
            result = clusterizeData(markers, upperLeft, zoom);
        } else {
            result = getExtraMarkerData(upperLeft, lowerRight, data, markerType);
        }

        return result;
    }
    
    @Override
    public List<String[]> generateExportReport(Coordinate topLeft, Coordinate bottomRight,
            List<Integer> categories, String markerType, Licence licence) {
        List<String[]> result = new ArrayList<String[]>();
        
        // HEADERS
        // TODO Make this configurable
        result.add(new String[] { 
                "Date Occurred", 
                "Vehicle Number",
                "Latitude", 
                "Longitude", 
                "Location" });
        
        // RECORDS
        if (Spectrum.MAP_WSP_MARKER.equals(markerType)) {
            List<AdhesionReport> wspReports = new ArrayList<AdhesionReport>();

            for (String fleetId : adhesionProviders.keySet()) {
                String fleetLicence = fleetProviders.get(fleetId).getLicence(fleetId);
                if (fleetLicence == null || licence.hasResourceAccess(fleetLicence)) {
                    wspReports.addAll(adhesionProviders.get(fleetId).getWspReport(
                            topLeft, bottomRight, categories, fleetId));
                }
            }

            for (AdhesionReport wsp : wspReports) {
                result.add(new String[] {
                        reportDateFormat.format(wsp.getTimestamp()),
                        wsp.getVehicleNumber(),
                        wsp.getLatitude().toString(),
                        wsp.getLongitude().toString(),
                        wsp.getLocationCode()
                });
            }
        }
        
        return result;
    }
    
    protected List<?> clusterizeData(Map<Integer, LinkedList<AdhesionMarker>> data, Coordinate upperLeft, int zoom) {
        List<AdhesionCluster> result = new ArrayList<AdhesionCluster>();

        double clusterSize = Mapping.getMetresPerPixelBing(upperLeft.getLatitude(), zoom) * 50;
        
        SimpleClusterizer<AdhesionCluster, AdhesionMarker> clusterizer = new SimpleClusterizer<AdhesionCluster, AdhesionMarker>(AdhesionCluster.class);

        for (LinkedList<AdhesionMarker> markers : data.values()) {
            result.addAll(clusterizer.cluster(markers, clusterSize));
        }

        return result;
    }
    
    protected Map<Integer, LinkedList<AdhesionMarker>> getAdhesionMarkerData(Coordinate upperLeft,
            Coordinate lowerRight, Object data, String markerType, String fleetId) {
        
        Map<Integer, LinkedList<AdhesionMarker>> result = new HashMap<Integer, LinkedList<AdhesionMarker>>();
        
        switch (markerType) {
        case Spectrum.MAP_WSP_MARKER:
            result = adhesionProviders.get(fleetId).getWspData(upperLeft, lowerRight, data, fleetId);
            break;
        case Spectrum.MAP_SAND_MARKER:
            result = adhesionProviders.get(fleetId).getSandData(upperLeft, lowerRight, data, fleetId);
            break;
        case Spectrum.MAP_OHLV_MARKER:
            result = adhesionProviders.get(fleetId).getOhlvData(upperLeft, lowerRight, data, fleetId);
            break;
        }

        return result;
    }
    
    protected List<?> getExtraMarkerData(Coordinate upperLeft, Coordinate lowerRight, Object data,
            String markerType) {
        return null;
    }
}
