/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.view;

public class Constants {
    public static final String APP_NAME = "SPECTRUM";
    public static final String LICENCE = "licence";

    public static final String RESOURCE_ACCESS_LOG_TYPE = "Resource Access";
    public static final String DATA_ACCESS_LOG_TYPE = "Data Access";
    public static final String RESOURCE_RESTRICTION_LOG_TYPE = "Resource Restriction";
    public static final String DATA_RESTRICTION_LOG_TYPE = "Data Restriction";

    public static final String DEFAULT_VIEW = "/";
    public static final String EVENT_VIEW = "event";
    public static final String FLEET_VIEW = "fleet";
    public static final String MAP_VIEW = "maps";
    public static final String UNIT_VIEW = "unit";
    public static final String CAB_VIEW = "cab";
    public static final String DATA_VIEW = "dataplots";
    public static final String DOWNLOAD_VIEW = "downloads";
    public static final String CHANNELDEFINITION_VIEW = "channeldefinition";
    public static final String LOSTPASSWORD_VIEW = "lostPassword";
    public static final String CHANGEPASSWORD_VIEW = "changePassword";

    public static final String ADMINISTRATION_VIEW = "admin";
    public static final String ADMINISTRATION_EDITOR_CONTEXT = "editor";
    public static final String ADMINISTRATION_ADMINISTRATION_CONTEXT = "administration";
    public static final String ADMINISTRATION_USERS_ROLES_VIEW = "adminUser";

    public static final String FLEET_SUMMARY = "Fleet Summary";
    public static final String UNIT_SUMMARY = "Unit Summary";
    public static final String FLEET_LOCATION = "Fleet Location";
    public static final String EVENT_HISTORY = "Event History";
    public static final String OPS_ANALYSIS = "Operations Analysis";
    public static final String DOWNLOADS = "Downloads";
    public static final String RULES_EDITOR = "Rules Editor";
    public static final String USER_ADMINISTRATION = "User Administration";
    public static final String CHANNEL_DEFINITION = "Channel Definition";
    public static final String EVENT_LINK = "Event Link";

    public static final String DEFAULT_LIC_KEY = "spectrum.default";
    public static final String FLEET_LIC_KEY = "spectrum.fleetsummary";
    public static final String FLEET_HEADCODE_LIC_KEY = "spectrum.fleetsummary.headcode";
    public static final String FLEET_UNITNUMBERS_LIC_KEY = "spectrum.fleetsummary.unitnumbers";
    public static final String FLEET_LASTUPDATETIME_LIC_KEY = "spectrum.fleetsummary.lastupdatetime";
    public static final String FLEET_LOCATION_LIC_KEY = "spectrum.fleetsummary.location";
    public static final String FLEET_SPEED_LIC_KEY = "spectrum.fleetsummary.speed";
    public static final String FLEET_LEADINGVEHICLE_LIC_KEY = "spectrum.fleetsummary.leadingvehicle";
    public static final String FLEET_LIVEFAULTSWARNINGSCOUNT_LIC_KEY = "spectrum.fleetsummary.livefaultswarningscount";

    public static final String UNIT_LIC_KEY = "spectrum.unitsummary";
    public static final String UNIT_CAB_LIC_KEY = "spectrum.unitsummary.cab";
    public static final String UNIT_DETAIL_LIC_KEY = "spectrum.unitsummary.detail";
    public static final String UNIT_CHANNELDATA_LIC_KEY = "spectrum.unitsummary.channeldata";
    public static final String UNIT_COMPONENTCHANNELDATA_LIC_KEY = "spectrum.unitsummary.componentchanneldata";
    public static final String UNIT_COMPONENTCHANNELHISTORY_LIC_KEY = "spectrum.unitsummary.componentchannelhistory";
    public static final String UNIT_WHEELPROFILES_LIC_KEY = "spectrum.unitsummary.wheelprofiles";
    public static final String UNIT_DATAPLOTS_LIC_KEY = "spectrum.unitsummary.dataplots";
    public static final String UNIT_CABVIEW_LIC_KEY = "spectrum.unitsummary.cabview";
    public static final String UNIT_SCHEMATIC_LIC_KEY = "spectrum.unitsummary.schematicview";
    public static final String UNIT_EVENT_LIC_KEY = "spectrum.unitsummary.eventhistory";
    public static final String UNIT_MAP_LIC_KEY = "spectrum.unitsummary.location";
    public static final String UNIT_RECOVERY_LIC_KEY = "spectrum.unitsummary.recovery";

    public static final String MAP_LIC_KEY = "spectrum.fleetlocation";
    public static final String EVENT_LIC_KEY = "spectrum.eventhistory";
    public static final String DOWNLOAD_LIC_KEY = "spectrum.downloads";
    public static final String EVENT_UPDATE_STATUS_LIC_KEY = "spectrum.event.udpatestatus";
    public static final String EVENT_ACKNOWLEDGE_LIC_KEY = "spectrum.event.acknowledge";
    public static final String EVENT_DEACTIVATE_LIC_KEY = "spectrum.event.deactivate";
    public static final String EVENT_COMMENT_LIC_KEY = "spectrum.event.comment";
    public static final String EVENT_PANEL_LIC_KEY = "spectrum.event.panel";
    public static final String EVENT_DETAIL_LIC_KEY = "spectrum.event.detail";
    public static final String CHANNEL_DEFINITION_LIC_KEY = "spectrum.channeldefinition.edit";
    
    public static final String DATA_UNITNUMBER_LIC_KEY = "spectrum.data.unitnumber";

    public static final String LOGINPAGE_KEY = "spectrum login page";

    public static final String EVENT_OPEN_DRILL_DOWN = "openDrillDown";
    public static final String EVENT_OPEN_UNIT_SUMMARY = "openUnitSummary";
    public static final String EVENT_OPEN_EVENT_DETAIL = "openEventDetail";
    public static final String RESTRICTION_OPEN_EVENT_DETAIL = "restrictionEventDetail";
    
    public static final String ADMINISTRATION_LIC_KEY = "spectrum.administration";
    public static final String ADMINISTRATION_USERS_LIC_KEY = "spectrum.administration.users";
    public static final String ADMINISTRATION_RULESEDITOR_LIC_KEY = "spectrum.administration.ruleseditor";
    public static final String ADMINISTRATION_EMAILTEMPLATES_LIC_KEY = "spectrum.administration.emailtemplates";
    public static final String ADMINISTRATION_RECOVERIESEDITOR_LIC_KEY = "spectrum.administration.recoverieseditor";
    public static final String ADMINISTRATION_FAULTCONFIG_LIC_KEY = "spectrum.administration.faultconfig";
    public static final String ADMINISTRATION_FAULTTEST_LIC_KEY = "spectrum.administration.faulttest";
    public static final String ADMINISTRATION_REPOBACKUP_LIC_KEY = "spectrum.administration.repobackup";
    public static final String ADMINISTRATION_PACKAGEUPLOAD_LIC_KEY = "spectrum.administration.packageupload";
    public static final String ADMINISTRATION_OVERRIDE_LIC_KEY = "spectrum.administration.override";

    public static final String ADMINISTRATION_CHANNELDEFINITION_LIC_KEY = "spectrum.administration.channeldefinition";
}