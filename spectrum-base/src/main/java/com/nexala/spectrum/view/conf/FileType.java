/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.view.conf;

import java.util.List;

public class FileType {

    /** Id for the fileType */
    private Integer id;

    /** name of the fileType */
    private String name;

    private List<String> vehicleTypes;

    public FileType(Integer id, String name, List<String> vehicleTypes) {
        this.id = id;
        this.name = name;
        this.vehicleTypes = vehicleTypes;
    }
    
    public FileType() {};//Created this to avoid JsonMappingException

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public final void setName(String name) {
        this.name = name;
    }

    public List<String> getVehicleTypes() {
        return vehicleTypes;
    }

    public void setVehicleTypes(List<String> vehicleTypes) {
        this.vehicleTypes = vehicleTypes;
    }
}