/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.view.conf;

import java.util.List;

import com.nexala.spectrum.rest.data.beans.Formation;

public interface DrillDownConfiguration {

    public List<Column> getColumns(String fleetId, List<Formation> formationList);
    
}
