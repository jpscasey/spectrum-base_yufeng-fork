/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.view.conf.cab;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * Setting for an indicator.
 * 
 * Indicator is black and red for 0 value, and orange and black for 1.
 * Could be inverted setting the onValue to 0.
 * 
 * @author brice
 *
 */
@JsonInclude(Include.NON_NULL)
public class IndicatorSettings implements Settings {

    private boolean onValue = true;
    
    private String onColor = "orange";
    
    private String onColorCenter = "black";
    
    private String offColor = "black";
    
    private String offColorCenter = "red";
    
    public boolean getOnValue() {
        return onValue;
    }
    
    public IndicatorSettings setOnValue(boolean onValue) {
        this.onValue = onValue;
        return this;
    }

    /**
     * Getter for onColor.
     * @return the onColor
     */
    public String getOnColor() {
        return onColor;
    }

    /**
     * Setter for onColor.
     * @param onColor the onColor to set
     */
    public IndicatorSettings setOnColor(String onColor) {
        this.onColor = onColor;
        return this;
    }

    /**
     * Getter for onColorCenter.
     * @return the onColorCenter
     */
    public String getOnColorCenter() {
        return onColorCenter;
    }

    /**
     * Setter for onColorCenter.
     * @param onColorCenter the onColorCenter to set
     */
    public IndicatorSettings setOnColorCenter(String onColorCenter) {
        this.onColorCenter = onColorCenter;
        return this;
    }

    /**
     * Getter for offColor.
     * @return the offColor
     */
    public String getOffColor() {
        return offColor;
    }

    /**
     * Setter for offColor.
     * @param offColor the offColor to set
     */
    public IndicatorSettings setOffColor(String offColor) {
        this.offColor = offColor;
        return this;
    }

    /**
     * Getter for offColorCenter.
     * @return the offColorCenter
     */
    public String getOffColorCenter() {
        return offColorCenter;
    }

    /**
     * Setter for offColorCenter.
     * @param offColorCenter the offColorCenter to set
     */
    public IndicatorSettings setOffColorCenter(String offColorCenter) {
        this.offColorCenter = offColorCenter;
        return this;
    }
}