/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.view.conf.unit;

import java.util.List;

import com.nexala.spectrum.db.beans.ChannelConfig;
import com.nexala.spectrum.db.beans.ChannelGroup;
import com.nexala.spectrum.db.beans.ChannelGroupConfig;
import com.nexala.spectrum.licensing.Licence;
import com.nexala.spectrum.licensing.ScreenRequiresFeature;
import com.nexala.spectrum.rest.data.beans.StockDesc;
import com.nexala.spectrum.view.conf.Column;
import com.nexala.spectrum.view.conf.DetailScreen;

public interface ChannelDataConfiguration extends DetailScreen, ScreenRequiresFeature {
    List<ChannelGroup> getChannelGroups();

    List<ChannelConfig> getNonGroupedChannelNames(
            ChannelGroupConfig channelGroupConfig);

    List<Column> getColumns(List<StockDesc> stockList, Licence licence);
}
