package com.nexala.spectrum.view.conf;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.nexala.spectrum.db.beans.ChannelGroup;
import com.nexala.spectrum.view.Constants;
import com.nexala.spectrum.view.Plugin;
import com.nexala.spectrum.view.conf.unit.StaticDataPlotConfiguration;

/**
 * Implementation for the static dataplot configuration.
 * @author brice
 *
 */
public class StaticDataplotConfigurationImpl implements StaticDataPlotConfiguration {

	@Override
	public String getDisplayName() {
		return "Static Data View";
	}

	@Override
	public String getTemplateName() {
		return "static_dataplots.html.ftl";
	}

	@Override
	public Plugin getParentPlugin() {
		return new Plugin("nx.unit.StaticDataplots", Arrays.asList("js/nexala.static.dataplots.js"), 
				Arrays.asList("css/nexala.static.dataplots.css"));
	}

	@Override
	public ScreenType getScreenType() {
		return ScreenType.TAB;
	}

	@Override
	public DataNavigationType getDataNavigationType() {
		return DataNavigationType.TIME;
	}

	@Override
	public String getRequiredRole() {
		return Constants.UNIT_DATAPLOTS_LIC_KEY;
	}

	@Override
	public List<Plugin> getPlugins() {
		return Collections.emptyList();
	}

	@Override
	public List<ChannelGroup> getChannelGroups() {
		return Collections.EMPTY_LIST;
	}

}
