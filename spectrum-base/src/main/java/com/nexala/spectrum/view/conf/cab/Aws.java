package com.nexala.spectrum.view.conf.cab;

public class Aws extends Widget {


    public Aws(String domId, int x, int y, int height, int width,
            String... channels) {
        super(domId, x, y, height, width, channels);
    }
    
    @Override
    public String getWidgetClassName() {
        return "nx.cab.Aws";
    }

}
