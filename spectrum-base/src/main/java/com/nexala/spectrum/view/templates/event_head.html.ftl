<!-- Event History includes -->

<script type = 'text/javascript' src = 'js/merge?resources=
    js/nexala.event.js&view=events'></script>
    
<#list jsIncludesList as jsIncludes>
    <script type = 'text/javascript' src = 'js/merge?resources=
        <#list jsIncludes as jsInclude>${jsInclude},</#list>
        &view=events
    '></script>
</#list>
    
<script type = 'text/javascript'>

    $(document).ready(function() {
        <#list conf.plugins as plugin>
            <#if plugin.pluginClass?has_content>
                var clazz = eval(${plugin.pluginClass});
                var plugInst = new clazz();
            </#if>
        </#list> 
    });
</script>

<script type = 'text/javascript'>
    $(document).ready(function() {
            nx.rest.event.eventHistoryConf(function(response) {
                
                var getQuery = false;
                
                <#if unitNumber??>
                    $('#unitNumber').val(${unitNumber});
                    var getQuery = true;
                </#if>
                
                <#if headcode??>
                    $('#headcode').val(${headcode});
                    var getQuery = true;
                </#if>
                
                <#if location??>
                    $('#location').val(${location});
                    var getQuery = true;
                </#if>
                
                <#if vehicleNumber??>
                    $('#vehicleNumber').val(${vehicleNumber});
                    var getQuery = true;
                </#if>
                
                <#if description??>
                    $('#description').val(${description});
                    var getQuery = true;
                </#if>
                
                if(getQuery == true){
                    $('button[title="Show Advanced Options"]').click();
                }            
            });
    });
</script>

<link rel = 'stylesheet' type = 'text/css' href = 'css/merge?resources=
    css/nexala.event.css&view=events'>
    
 <#list cssIncludesList as cssIncludes>
    <link rel = 'stylesheet' type = 'text/css' href = 'css/merge?resources=
        <#list cssIncludes as cssInclude>${cssInclude},</#list>
        &view=events
    '>
</#list>