/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.view.conf.unit;

import java.util.ArrayList;
import java.util.List;

import com.google.inject.Inject;
import com.nexala.spectrum.configuration.ApplicationConfiguration;
import com.nexala.spectrum.rest.data.beans.BeanField;
import com.nexala.spectrum.view.conf.Column;
import com.typesafe.config.Config;

public class DefaultUnitSearchConfiguration implements UnitSearchConfiguration {
    
    private final ApplicationConfiguration applicationConfiguration;
    
    private final List<BeanField> searchFields;
    
    private final List<Column> columns;
    
    @Inject
    public DefaultUnitSearchConfiguration(ApplicationConfiguration applicationConfiguration) {
        this.applicationConfiguration = applicationConfiguration;
        Config conf = applicationConfiguration.get().getConfig("r2m.unitSearch");

        this.searchFields = new ArrayList<BeanField>();
        this.columns = new ArrayList<Column>();
        
        List<? extends Config> searchConfig = conf.getConfigList("headers");

        for (Config header : searchConfig) {
            BeanField headers = new BeanField();

            headers.setId(header.getString("name"));
            headers.setDbName(header.getString("db"));
            headers.setType(header.getString("type"));
            this.searchFields.add(headers);
            
            Column column = applicationConfiguration.columnBuilder(header).build();
            if (column.isVisible()) {
                this.columns.add(column);  
            }
        }
    }
    
    @Override
    public List<BeanField> getSearchFields() {
        return searchFields;
    }
    
    @Override
    public List<Column> getColumns() {
        return columns;
    }
           
    @Override
    public int getMaximumResults() {
        int maximumResults = applicationConfiguration.get().getInt("r2m.unitSearch.maximumResults");
        return maximumResults;
    }
    
	@Override
	public int getPanelWidth() {
		int panelWidth = applicationConfiguration.get().getInt("r2m.unitSearch.panelWidth");
		return panelWidth;
	}
}