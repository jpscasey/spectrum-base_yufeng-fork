<!-- Event Link includes -->
    
<#list jsIncludesList as jsIncludes>
    <script type = 'text/javascript' src = 'js/merge?resources=
        <#list jsIncludes as jsInclude>${jsInclude},</#list>
        &view=events
    '></script>
</#list>
    
<script type = 'text/javascript'>

    $(document).ready(function() {

    });
</script>

<script type = 'text/javascript'>
    $(document).ready(function() {
    	var eventObj = {};
    	
    	<#if event??>
    		eventObj = JSON.parse('${event}');
    		
    		function waitForMaps() {
    		  console.log("waiting for Bing Maps...");
              if (typeof window.Microsoft !== "undefined" && typeof window.Microsoft.Maps !== "undefined") {
                  nx.event.openLink(eventObj.eventId, eventObj.fleetId, eventObj.eventIds)
              } else {
                  setTimeout(waitForMaps, 250);
              }
            }
            waitForMaps();				

    	</#if>
    
    });
</script>

    
 <#list cssIncludesList as cssIncludes>
    <link rel = 'stylesheet' type = 'text/css' href = 'css/merge?resources=
        <#list cssIncludes as cssInclude>${cssInclude},</#list>
        &view=events
    '>
</#list>