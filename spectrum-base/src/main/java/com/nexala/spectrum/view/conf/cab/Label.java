/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.view.conf.cab;

public class Label extends Widget {
    private String text;

    public Label(int x, int y, int h, int w,
            String text, String... channels) {
        super(null, x, y, h, w, channels);
        this.text = text;
    }

    public String getText() {
        return text;
    }
    
    
    @Override
    public String getWidgetClassName() {
        return "label";
    }
}