/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.view.conf.unit;

import java.util.List;
import java.util.TimeZone;

import com.google.inject.ImplementedBy;
import com.nexala.spectrum.db.beans.ChannelConfig;
import com.nexala.spectrum.licensing.Licence;
import com.nexala.spectrum.rest.data.beans.DataSet;

@ImplementedBy(DefaultDataExportConfiguration.class)
public interface DataExportConfiguration {
   
    
    /**
     * Return a row for the export document
     * @return
     */

    String[] getRow(DataSet dataSet,  List<ChannelConfig> channels, TimeZone tz);
    
    /**
     * Return a row for the header of the document
     * @return
     */
    String[] getHeader (String vehicleNumber, long startDate, long endDate, TimeZone tz, String fleetId, Licence licence);
    
    String getFileName (String vehicleNumber, long start, TimeZone tz);

    String getSourceNumber(String vehicleId, String fleetId);
}
