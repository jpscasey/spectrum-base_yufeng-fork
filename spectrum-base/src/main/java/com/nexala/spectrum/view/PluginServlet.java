/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.view;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.nexala.spectrum.licensing.Licence;
import com.nexala.spectrum.view.conf.ScreenConfiguration;

public abstract class PluginServlet extends TemplateServlet {

	private static final long serialVersionUID = 1L;

	public abstract ScreenConfiguration getConfiguration();
    
    
    protected void proceed(HttpServletRequest request, HttpServletResponse response)
            throws ServletException {

        Licence licence = Licence.get(request, response, true);
        
        Map<String, Object> rootMap = new HashMap<String, Object>();
        rootMap.put("head", getHeaderTemplate());
        rootMap.put("includes", new ArrayList<String>());
        rootMap.put("cssIncludes", getCssIncludes());
        rootMap.put("jsIncludes", getJsIncludes());
        rootMap.put("conf", getConfiguration());

        doRender(request, response, getBodyTemplate(), rootMap, licence);
    }
    
    
    /**
     * Return the template of this screen header.
     * @return the template of this screen header.
     */
    protected abstract String getHeaderTemplate(); 
    
    
    /**
     * Return the template of this screen body.
     * @return the template of this screen body.
     */
    protected abstract String getBodyTemplate();
    
    /**
     * Return all css files.
     * @return all css files
     */
    private List<String> getCssIncludes() {
    	List<String> cssIncludes = new ArrayList<String>();
    	
        for (Plugin p : getAllPlugins()) {
            cssIncludes.addAll(p.getStylesheets());
        }
        
        return cssIncludes;
    }
    
    /**
     * Return all javascript files.
     * @return all javascript files.
     */
    private List<String> getJsIncludes() {
    	List<String> jsIncludes = new ArrayList<String>();
        
        for (Plugin p : getAllPlugins()) {
            jsIncludes.addAll(p.getSources());
        }
        
        return jsIncludes;
    }
    
    /**
     * Return all the plugins of the configuration
     * @return
     */
    protected List<Plugin> getAllPlugins() {
    	List<Plugin> plugins = new ArrayList<Plugin>();
    	
    	if (getConfiguration().getParentPlugin() != null) {
    		plugins.add(getConfiguration().getParentPlugin());
    	}
    	
    	if (getConfiguration().getPlugins() != null) {
    		plugins.addAll(getConfiguration().getPlugins());	
    	}
    	
    	return plugins;
    }
    
    
}
