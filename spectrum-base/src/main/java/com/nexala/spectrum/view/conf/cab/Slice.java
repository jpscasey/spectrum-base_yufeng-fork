/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.view.conf.cab;

public class Slice {
    private double radius;
    private double startAngle;
    private double endAngle;
    private String fill;
    
    public double getRadius() {
        return radius;
    }
    
    public Slice setRadius(double radius) {
        this.radius = radius;
        return this;
    }
    
    public double getStartAngle() {
        return startAngle;
    }
    
    public Slice setStartAngle(double startAngle) {
        this.startAngle = startAngle;
        return this;
    }
    
    public double getEndAngle() {
        return endAngle;
    }
    
    public Slice setEndAngle(double endAngle) {
        this.endAngle = endAngle;
        return this;
    }
    
    public String getFill() {
        return fill;
    }
    
    public Slice setFill(String fill) {
        this.fill = fill;
        return this;
    }
}
