/*
 * Copyright (c) Nexala Technologies 2013, All rights reserved.
 */
package com.nexala.spectrum.view;

import java.util.List;

import com.google.inject.assistedinject.Assisted;

public interface PluginFactory {
    Plugin create(@Assisted(value = "pluginClass") String pluginClass,
            @Assisted(value = "source") List<String> sources,
            @Assisted(value = "stylesheet") List<String> stylesheets);
}
