/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.view.conf.maps;

import java.util.List;

public class NamedMapView extends MapView {
    private final String description;
    private final String id;
    private final List<String> fleets;
    
    public NamedMapView(String id, String description, Coordinate northWest, Coordinate southEast,
            MapType mapType, List<String> fleets) {
        super(northWest, southEast, mapType);
        this.description = description;
        this.id = id;
        this.fleets = fleets;
    }
    
    public String getDescription() {
        return description;
    }

	public String getId() {
		return id;
	}
	
	public List<String> getFleets(){
		return fleets;
	}
}
