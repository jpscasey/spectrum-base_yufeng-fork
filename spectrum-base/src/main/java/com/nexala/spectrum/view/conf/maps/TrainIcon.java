/*
 * Copyright (c) Nexala Technologies 2016, All rights reserved.
 */
package com.nexala.spectrum.view.conf.maps;

/**
 * Bean for train icon displayed on maps
 * 
 * @author Fulvio
 */
public class TrainIcon {

    private String name;

    private String image;

    private int zIndex;

    public TrainIcon(String name, String image, int zIndex) {
        this.name = name;
        this.image = image;
        this.zIndex = zIndex;
    }

    public String getName() {
        return name;
    }

    public String getImage() {
        return image;
    }

    public int getzIndex() {
        return zIndex;
    }
}
