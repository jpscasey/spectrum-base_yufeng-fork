<div id = "dvMain">
    <div id = "chartPane">
        <div class = 'vehicleTabs'>
            <ul></ul>
        </div>
    </div>

    <div id = 'channelPane'>
        <div id = 'dvConfigurations' class = 'ui-state-default ui-corner-all'>
            <div id = 'select'>
                <select>
                </select>
            </div>
            
            <div id = 'update'>
                <button id = 'dvSaveButton'></button>
                <button id = 'dvDeleteButton'></button>
            </div>
        </div>
        
		<!-- FIXME generate from javascript -->
        <div id = "dvChannelGroups">
            <div id = "dvChannelAccordion">
                <#list screen.channelGroups as channelGroup>
                    <h3><a href="#${channelGroup.id}">${channelGroup.description}</a></h3>
                    <div></div>
                </#list>
            </div>
        </div> 
        
        <div id="system-warning">
            <p>
                <span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;">
                </span>
                <span data-i18n="System charts may not be modified."></span>
            </p>
        </div>
        
        
        <div id="emptyfield-warning">
            <p>
                <span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;">
                </span>
                <span data-i18n="Please enter chart name"></span>
            </p>
        </div>
        
        <div id="config-delete">
            <p>
                <span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;">
                </span>
                <span data-i18n="This configuration will be permanently deleted. Are you sure?"></span>
            </p>
        </div>
        
        <div id="rename-config">
            <form id="renameFieldForm">
            <fieldset>
                <label for="name" data-i18n="Chart Name"></label>
                <input type="text" name="name" id="renameField"
                    class="text ui-widget-content ui-corner-all" maxlength="35"/>
            </fieldset>
            </form>
        </div>
        
        <div id="save-config">
            <form id="saveFieldForm">
            <fieldset>
                <label for="name" data-i18n="Chart Name"></label>
                <input type="text" name="name" id="saveField"
                    class="text ui-widget-content ui-corner-all" maxlength="35" />
            </fieldset>
            </form>
        </div>
    </div>
</div>
