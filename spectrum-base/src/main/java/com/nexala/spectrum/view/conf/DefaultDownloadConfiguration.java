/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.view.conf;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.google.inject.Inject;
import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.configuration.ApplicationConfiguration;
import com.nexala.spectrum.rest.data.beans.BeanField;
import com.nexala.spectrum.view.Plugin;
import com.typesafe.config.Config;

public class DefaultDownloadConfiguration implements DownloadConfiguration {

    private ApplicationConfiguration applicationConfiguration;
    
    private final List<Column> columns;

    private final List<FileType> fileTypes;
    
    private final List<BeanField> downloadFields;
    
    private final List<Field> popupFilters;
    
    private final List<FieldBuilder> ruleEditorDRFilters;
    
    private final int thresholdTime;
    
    private final int gapTime;

    @Inject
    DefaultDownloadConfiguration(ApplicationConfiguration applicationConfiguration) {
        this.applicationConfiguration = applicationConfiguration;

        Config conf = applicationConfiguration.get().getConfig("r2m.download");
        
        this.columns = new ArrayList<Column>();
        if(conf.hasPath("columns")) {
            List<? extends Config> columnConfigs = applicationConfiguration.get().getConfigList("r2m.download.columns");
            for (Config columnConfig : columnConfigs) {
                ColumnBuilder columnBuilder = applicationConfiguration.columnBuilder(columnConfig);
                this.columns.add(new Column(columnBuilder));
            }
        }
        
        this.fileTypes = new ArrayList<FileType>();
        if (conf.hasPath("fileTypes")) {
            List<? extends Config> fileTypeConfigs = applicationConfiguration.get().getConfigList("r2m.download.fileTypes");
            for (Config fileTypeConfig : fileTypeConfigs) {
                this.fileTypes.add(new FileType(fileTypeConfig.getInt("id"), fileTypeConfig.getString("fileTypeName"),
                        fileTypeConfig.getStringList("vehicleTypeList")));
            }
        }
        
        this.downloadFields = new ArrayList<BeanField>();
        if (conf.hasPath("fields")) {
            List<? extends Config> downloadConfigs = applicationConfiguration.get().getConfigList("r2m.download.fields");
            for (Config downloadConfig : downloadConfigs) {
            	BeanField field = new BeanField();
            	
            	field.setId(downloadConfig.getString("id"));
                field.setDbName(downloadConfig.getString("db"));
                field.setType(downloadConfig.getString("type"));

                this.downloadFields.add(field);
            }
        } 
        
        this.popupFilters = new ArrayList<Field>();
        this.ruleEditorDRFilters = new ArrayList<FieldBuilder>();
        if (conf.hasPath("filters")) {
            List<? extends Config> fields = applicationConfiguration.get().getConfigList("r2m.download.filters");
            for (Config field : fields) {
            	Field confField = applicationConfiguration.buildField(field, 123);
            	if (confField.getUsages() != null) {
                	if (confField.getUsages().contains("popupFilter"))
                		this.popupFilters.add(confField);

                	if (confField.getUsages().contains("ruleEditorDR"))
                		this.ruleEditorDRFilters.add(applicationConfiguration.getFieldBuilder(field, 123, null));
            	}
            }
        } 
        
        this.thresholdTime = applicationConfiguration.get().getInt("r2m.download.thresholdTime");
        
        this.gapTime = applicationConfiguration.get().getInt("r2m.download.gapTime");
    }

    public List<Column> getColumns() {
        return columns;
    }

    public List<FileType> getFileTypes() {
        return fileTypes;
    }
    
    public List<BeanField> getFields() {
        return downloadFields;
    }
    
    public List<Field> getPopupFilters() {
        return popupFilters;
    }
    
    public boolean isVehicleSelector() {
        String path = "r2m.download.VehicleSelector";

        if (applicationConfiguration.get().hasPath(path)) {
            return applicationConfiguration.get().getBoolean(path);
        }
        return false;
    }
    
    public boolean isRetrievable() {
        String path = "r2m.download.isRetrievable";

        if (applicationConfiguration.get().hasPath(path)) {
            return applicationConfiguration.get().getBoolean(path);
        }
        return false;
    }
    
	public int getThresholdTime() {
		return thresholdTime;
	}
	
	public int getGapTime() {
		return gapTime;
	}
  
    public final List<Plugin> getPlugins() {
        return applicationConfiguration.getPlugins(Spectrum.DOWNLOADS_ID);
    }
    
    public Plugin getParentPlugin() {
        Plugin plugin = new Plugin("", Collections.<String> emptyList(), Collections.<String> emptyList());
        return plugin;
    }

	@Override
	public String getScreenName() {
		return "Downloads";
	}

	public List<FieldBuilder> getRuleEditorDRFilters() {
		return ruleEditorDRFilters;
	}	
}
