/*
 * Copyright (c) Nexala Technologies 2013, All rights reserved.
 */
package com.nexala.spectrum.view.conf.maps;

import java.util.Collection;

/**
 * Map configuration for a screen containing our generic map. 
 * @author brice
 *
 */
public interface GenericMapConfiguration {
    
	/**
     * Gets the default position for the map, e.g. Ireland.
     * 
     * @return the default position for the map
     */
    MapView getDefaultView();

    
    /**
     * Return the list of map marker providers registers for this map.
     * 
     * @return the list of map marker providers 
     *          registers for this map, empty list if none.
     */
    Collection<MapMarkerConfiguration> getMapMarkersConfiguration();
    
    
    
    /**
     * Return the id of the div that contain this map.
     * @return id of the div that contain this map.
     */
    String getContainerId();
    
}
