/*
 * Copyright (c) Nexala Technologies 2013, All rights reserved.
 */
package com.nexala.spectrum.view.conf;

public class DigitalColumnBuilder extends ColumnBuilder {
    public DigitalColumnBuilder() {
        super();
        type(ColumnType.DIGITAL);
        width(20);
    }
}
