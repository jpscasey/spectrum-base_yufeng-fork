package com.nexala.spectrum.view.conf;

/**
 * Bean holding a label, a value, and a licence
 * @author BRedaha
 *
 */
public class DropdownOption {
    
    private String label;
    
    private String value;
    
    private String licence;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getLicence() {
        return licence;
    }

    public void setLicence(String licence) {
        this.licence = licence;
    }

}
