/*
 * Copyright (c) Nexala Technologies 2013, All rights reserved.
 */
package com.nexala.spectrum.view.conf;

import java.util.ArrayList;
import java.util.List;

public class GroupColumn extends Column {
    
    public GroupColumn(ColumnBuilder builder) {
        super(builder);
    }

    private List<Column> columns = new ArrayList<Column>();

    public List<Column> getColumns() {
        return columns;
    }

    public void setColumns(List<Column> columns) {
        this.columns = columns;
    }

    public void addColumn(Column column) {
        this.columns.add(column);
    }
}
