<div id="eventDetailDrillDown" class="eventDetailDrillDown">
	<div id="eventDetailToolbar"></div>
    <div id="eventDetailMargin">
        <div id="eventDetail" class="eventDetailDiv">
            <div id="eventDescription">
            </div>
            
            <div id="eventLocation">
                <div id = "eventLocationContainer">
                    <div id = "edMapBlock"></div>
                </div>
            </div>
        </div>
        <div id="eventDetailData">
        </div>
        
          <div id="eventDetailDataSlider">
        </div>
    </div>
</div>
