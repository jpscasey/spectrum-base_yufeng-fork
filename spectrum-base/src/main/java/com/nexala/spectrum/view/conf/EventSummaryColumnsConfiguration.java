/*
 * Copyright (c) Nexala Technologies 2016, All rights reserved.
 *
 * $Author$
 * $Date$
 * $Revision$
 * $Source$
 */
package com.nexala.spectrum.view.conf;

import java.util.List;

public class EventSummaryColumnsConfiguration {
    
    private List<Column> columns;
    
    private Integer descriptionMaxCharacters;
    
    public EventSummaryColumnsConfiguration(List<Column> columns, Integer descriptionMaxCharacters) {
        this.columns = columns;
        this.descriptionMaxCharacters = descriptionMaxCharacters;
    }
    
    public List<Column> getColumns() {
        return columns;
    }

    public Integer getDescriptionMaxCharacters() {
        return descriptionMaxCharacters;
    }
}
