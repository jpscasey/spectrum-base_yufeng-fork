/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.view.conf;

import java.util.List;
import java.util.Map;

import com.google.inject.ImplementedBy;
import com.nexala.spectrum.licensing.Licence;
import com.nexala.spectrum.view.conf.i18n.Language;

@ImplementedBy(DefaultAppConfiguration.class)
public interface AppConfiguration {
    /** Returns the default Application */
    App getDefaultApp();

    /**
     * Returns a list of all screens in the application, i.e. FleetSummary,
     * UnitSummary, etc,
     */
    App[] getApps();

    /**
     * Returns a list of all screens, which are applicable to the Licence
     */
    App[] getApps(Licence licence);
    
    /**
     * Return a screen with specified name, if any.
     * The name must match the one in application.conf file.  
     */
    App getApp(String name);
    
    /**
     * Return a list of all menu entries.
     * @return  a list of all menu entries
     */
    List<MenuEntry> getMenu();
    
    /**
     * Returns a list of all menu entries, which are applicable to the Licence
     * @param licence
     * @return
     */
    List<MenuEntry> getMenu(Licence licence);
    
    /**
     * Get the branding of the app.
     * @return the url of the logo image
     */
    String getBranding();
    
    /**
     * Get the css style for the branding.
     * (for padding for example)
     * @return the css style for branding
     */
    String getBrandingStyle();
 
    /**
     * Get the app title.
     * @return the title of the webapp
     */
    String getTitle();
    
    /**
     * Get timeout interval of the application.
     * @return the timeout interval in milliseconds
     */
    Integer getTimeoutInterval();
    
    /**
     * Return the list of supported languages by the app.
     * @return list of supported languages
     */
    List<Language> getLanguages();

	ExportConfiguration getExportConfig();

	Map<String, String> getHelpConfig();
	
    /**
     * Get the footer of the app.
     * @return the footer string
     */
    String getFooter();
    
    String getChannelLabelConfig();
}
