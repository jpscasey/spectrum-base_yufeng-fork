package com.nexala.spectrum.view.conf;

import java.util.ArrayList;
import java.util.List;

import com.google.inject.Inject;
import com.nexala.spectrum.configuration.ApplicationConfiguration;
import com.typesafe.config.Config;

public class DefaultEventGroupConfiguration implements EventGroupConfiguration {

    private List<Column> groupColumns;
    
    @Inject
    public DefaultEventGroupConfiguration (ApplicationConfiguration applicationConfiguration) {
        Config conf = applicationConfiguration.get();

        // Live and Recent Events configuration
        this.groupColumns = new ArrayList<Column>();
        if (conf.hasPath("r2m.eventHistory.groupColumns")) {
            for (Config groupColumn : conf.getConfigList("r2m.eventHistory.groupColumns")) {
                this.groupColumns.add(applicationConfiguration.columnBuilder(groupColumn).build());
            }   
        }
    }
    
    @Override
    public List<Column> getColumns() {
        return groupColumns;
    }

}
