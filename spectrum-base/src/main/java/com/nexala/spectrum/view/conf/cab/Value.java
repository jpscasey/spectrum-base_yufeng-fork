/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.view.conf.cab;

/**
 * A {@link Widget} that display a value and a label.
 * @author brice
 *
 */
public class Value extends Widget {

    ValueSettings settings;

    public Value(String domId, int x, int y, int h, int w,
            ValueSettings settings, String... channels) {
        super(domId, x, y, h, w, channels);
        this.settings = settings;
    }

    @Override
    public String getWidgetClassName() {
        return "nx.cab.Value";
    }
    
    @Override
    public Settings getSettingsObject() {
        return settings;
    }

}