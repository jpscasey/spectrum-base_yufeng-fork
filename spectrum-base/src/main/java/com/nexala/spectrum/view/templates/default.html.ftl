<#include "header.ftl"/>

<div style="width:100%; height:100%">
    <div style="position:absolute; top:45%; left:40%; text-align:center;">
        <div>Your user does not have access to the application</div>
        
        <input
            type = "button" 
            value = "Logout" 
            onclick = "logout()";
            class = "default-view-logout ui-button ui-widget ui-state-default ui-corner-all">
    </div>
</div>

<script type = 'text/javascript'>
	function logout(){
		nx.rest.system.user(function(user) {
            	window.location.replace(user.logoutUrl);
    	});
    }
</script>

<#include "footer.ftl"/>