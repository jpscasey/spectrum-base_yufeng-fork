<html>
    <head>
        <title>Nexala R2M recover password</title>
        <meta http-equiv="X-UA-Compatible" content="IE=8" >
        <script type="text/javascript" src="js/jquery-1.8.1.min.js"></script>
        <link rel="stylesheet" type="text/css" href="/css/nexala.lostPassword.css" />
    </head>
    
    <body class="loginpage">
        <div class="loginbox">
            <div class="loginform">
                <div class="logo"></div>
            
                <form name="spectrumLoginForm" method="POST" action="changePassword">
                	<input type="hidden" name="username" id="username" value="${username}" />
                	<input type="hidden" name="token" id="token" value="${token}" />
                    <fieldset>
                        <label for="password">New Password:</label>
                        <input type="password" name="password" id="password" class="logintext" value="" />
                    </fieldset>
                    <fieldset>
                        <label for="password">Confirm Password:</label>
                        <input type="password" name="confirmPassword" id="confirmPassword" class="logintext" value="" />
                    </fieldset>
                    <fieldset>
                    	<input type="submit" class="lostPwdButton" id="changePwd" name="change" value="Change Password">
                    </fieldset>
                </form>
                <span style='color:red; font-size: 12px' id='errorField'></span>
            </div>
        </div>
        
        <script type="text/javascript">
    		$(document).ready(function() {
    			$("#password").focus();
    			$('#changePwd').click(function(event) {
  					var pwd = $('#password').val();
  					pwd = $.trim(pwd);
  					var confirmPwd = $('#confirmPassword').val();
  					confirmPwd = $.trim(confirmPwd);
  					
  					if (pwd != confirmPwd) {
  						$('#errorField').text("The two passwords don't match");
  						event.preventDefault();
  					} else {
  						$('#errorField').text('');
  					}
  					
				});
    		});
		</script>
        
    </body>
</html>