package com.nexala.spectrum.view.conf.i18n;

/**
 * Represent a file containing translations for a language.
 * @author BBaudry
 *
 */
public class TranslationFile {
    
    private String path;
    
    private String varName;

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getVarName() {
        return varName;
    }

    public void setVarName(String varName) {
        this.varName = varName;
    }
    

}
