/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.view;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.common.collect.Lists;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.nexala.spectrum.licensing.Licence;
import com.nexala.spectrum.licensing.RestrictedResource;
import com.nexala.spectrum.view.conf.App;
import com.nexala.spectrum.view.conf.AppConfiguration;
import com.nexala.spectrum.view.conf.CommonFleetConfiguration;

@Singleton
public class FleetView extends TemplateServlet {

    private static final long serialVersionUID = -974577722081931776L;

    @Inject
    private CommonFleetConfiguration fleetSummaryScreenConfig;
    
    @Inject
    private AppConfiguration appConf;
    
    private void handleRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException {
        proceed(request, response);
    }
    
    private void proceed(HttpServletRequest request, HttpServletResponse response)
            throws ServletException {

        Licence licence = Licence.get(request, response, true);
        
        Map<String, Object> rootMap = new HashMap<String, Object>();
        rootMap.put("head", "fleet_head.html.ftl");
        rootMap.put("includes", new ArrayList<String>());
        rootMap.put("cssIncludesList", Lists.partition(getCssIncludes(), 10));
        rootMap.put("jsIncludesList", Lists.partition(getJsIncludes(), 10));
        rootMap.put("conf", fleetSummaryScreenConfig);
        rootMap.put("screenName", fleetSummaryScreenConfig.getScreenName());

        doRender(request, response, "fleet.html.ftl", rootMap, licence);
    }

    @RestrictedResource(Constants.FLEET_LIC_KEY)
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException {
        handleRequest(request, response);
    }
    
    @RestrictedResource(Constants.FLEET_LIC_KEY)
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException {
        handleRequest(request, response);
    }
    
    private List<String> getCssIncludes() {
        List<Plugin> plugins = fleetSummaryScreenConfig.getPlugins();
        
        List<String> cssIncludes = new ArrayList<String>();
        
        for (Plugin p : plugins) {
            cssIncludes.addAll(p.getStylesheets());
        }
        
        return new ArrayList<String>(cssIncludes);
    }
    
    private List<String> getJsIncludes() {
        List<Plugin> plugins = fleetSummaryScreenConfig.getPlugins();
        
        List<String> jsIncludes = new ArrayList<String>();
        
        for (Plugin p : plugins) {
            jsIncludes.addAll(p.getSources());
        }
        
        return new ArrayList<String>(jsIncludes);
    }
    
    public App getView() {
        return appConf.getApp(Constants.FLEET_SUMMARY);
    }
}
