/*
 * Copyright (c) Nexala Technologies 2013, All rights reserved.
 */
package com.nexala.spectrum.view.conf;

public class GroupColumnBuilder extends ColumnBuilder {
    
    public GroupColumnBuilder() {
        super();
        type(ColumnType.GROUP);
        width(20);
    }
    
    public GroupColumn buildGroup() {
        return new GroupColumn(this);
    }
}
