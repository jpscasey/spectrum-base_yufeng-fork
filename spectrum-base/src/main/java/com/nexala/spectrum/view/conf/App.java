/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.view.conf;

import java.util.Arrays;

/**
 * This is an immutable class, which represents a screen in Spectrum. These
 * applications are mapped to their "path" in the
 * {@link com.nexala.spectrum.view.SpectrumViewModule}
 */
public final class App {
    /** The title a tab in the spectrum header */
    private String desc;

    /** The path to the view */
    private String path;
    
    /** The licence keys required to view the App */
    private String[] licenceKeys;

    private boolean visible;
    
    /** True if the screen is implemented in a separate project, i.e. OpsAnalysis */
    private boolean external;

    /** The position in the menu */
    private int position;

    /** True if the screen is displayed in a submenu, i.e. Administration */
    private boolean grouped;

    /** True submenu name */
    private String groupName;

    /**
     * The name of the App's View, this value may be null if the path is pointing to an
     * external App, i.e. OpsAnalysis
     */
    private String view;
    
    public App(String desc, String path, String view, int position, boolean grouped, String groupName,
            boolean external, boolean visible, String ... licenceKeys) {
        if (desc == null) {
            throw new NullPointerException("desc may not be null");
        }
        
        if (path == null) {
            throw new NullPointerException("path may not be null");
        }
        
        this.desc = desc;
        this.path = path;
        this.licenceKeys = licenceKeys;
        this.view = view;
        this.visible = visible;
        this.external = external;
        this.position = position;
        this.grouped = grouped;
        this.groupName = groupName;
    }

    public String getDesc() {
        return desc;
    }

    public String getPath() {
        return path;
    }
    
    public String[] getLicenceKeys() {
        return licenceKeys;
    }
    
    public boolean isVisible() {
        return this.visible;
    }

    public boolean isExternal() {
        return this.external;
    }

    public int getPosition() {
        return this.position;
    }

    public boolean isGrouped() {
        return this.grouped;
    }

    public String getGroupName() {
        return this.groupName;
    }

    public String getView() {
        return view;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((desc == null) ? 0 : desc.hashCode());
        result = prime * result
                + ((licenceKeys == null) ? 0 : licenceKeys.hashCode());
        result = prime * result + ((path == null) ? 0 : path.hashCode());
        result = prime * result + ((view == null) ? 0 : view.hashCode());
        result = prime * result + (visible ? 1231 : 1237);
        result = prime * result + (external ? 1231 : 1237);
        result = prime * result + position;
        result = prime * result + (grouped ? 1231 : 1237);
        result = prime * result + ((groupName == null) ? 0 : groupName.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        App other = (App) obj;
        if (desc == null) {
            if (other.desc != null)
                return false;
        } else if (!desc.equals(other.desc))
            return false;
        if (licenceKeys == null) {
            if (other.licenceKeys != null)
                return false;
        } else if (!Arrays.deepEquals(licenceKeys, other.licenceKeys))
            return false;
        if (path == null) {
            if (other.path != null)
                return false;
        } else if (!path.equals(other.path))
            return false;
        if (view == null) {
            if (other.view != null)
                return false;
        } else if (!view.equals(other.view))
            return false;
        if (visible != other.visible)
            return false;
        if (external != other.external)
            return false;
        if (position != other.position)
            return false;
        if (grouped != other.grouped)
            return false;
        if (groupName == null) {
            if (other.groupName != null)
                return false;
        } else if (!groupName.equals(other.groupName))
            return false;

        return true;
    }
}
