/*
 * Copyright (c) Nexala Technologies 2013, All rights reserved.
 */
package com.nexala.spectrum.view.conf.maps;

public enum MapType implements HasMicrosoftMapsName {
    AERIAL {
        public String getName() {
            return "a";
        }
    },
    
    ROAD {
        public String getName() {
            return "r";
        }
    };
}
