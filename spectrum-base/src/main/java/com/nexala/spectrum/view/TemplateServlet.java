/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.view;

import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.inject.Inject;
import com.nexala.spectrum.licensing.Licence;
import com.nexala.spectrum.view.conf.App;
import com.nexala.spectrum.view.conf.AppConfiguration;
import com.nexala.spectrum.view.conf.i18n.Language;
import com.nexala.spectrum.view.conf.i18n.TranslationFile;
import com.nexala.spectrum.view.templates.TemplateConfiguration;

import freemarker.template.Template;
import freemarker.template.TemplateException;

public abstract class TemplateServlet extends HttpServlet {

    private static final long serialVersionUID = -5872584393725958300L;

    @Inject
    private TemplateConfiguration tmplConf;

    @Inject
    protected AppConfiguration appConf;

    public void doRender(HttpServletRequest request,
            HttpServletResponse response, String tmplName,
            Map<String, Object> rootMap, Licence licence)
            throws ServletException {

        Template tmpl;

        try {
            tmpl = tmplConf.get().getTemplate(tmplName);
        } catch (IOException e) {
            throw new ServletException(e);
        }

        response.setContentType("text/html; charset=" + tmpl.getEncoding());

        final Writer out;

        try {
            out = response.getWriter();
        } catch (IOException e) {
            throw new ServletException(e);
        }

        try {
            rootMap.put("fs", "1".equals(request.getParameter("fs")));
            rootMap.put("active", getView());
            rootMap.put("menu", appConf.getMenu(licence));
            rootMap.put("logo", appConf.getBranding());
            rootMap.put("logoCss", appConf.getBrandingStyle());
            rootMap.put("title", appConf.getTitle());
            rootMap.put("timeoutInterval", appConf.getTimeoutInterval());
            rootMap.put("languages", appConf.getLanguages());
            rootMap.put("translationFiles", getTranslationFiles(appConf.getLanguages()));
            rootMap.put("userLanguage", licence.getLoginUser().getLocale());
            rootMap.put("footer", appConf.getFooter());
            tmpl.process(rootMap, out);
        } catch (IOException e) {
            throw new ServletException(e);
        } catch (TemplateException e) {
            throw new ServletException(e);
        }

        try {
            out.close();
        } catch (IOException e) {
            throw new ServletException(e);
        }
    }
    
    /**
     * Return the list of language files.
     * @param languages
     * @return
     */
    public List<String> getTranslationFiles(List<Language> languages) {
        List<String> result = new ArrayList<>();
        
        for (Language lg : languages) {
            for (TranslationFile file : lg.getTranslationFiles()) {
                result.add(file.getPath());
            }
        }
        
        return result;
    }
    
    public abstract App getView();
}
