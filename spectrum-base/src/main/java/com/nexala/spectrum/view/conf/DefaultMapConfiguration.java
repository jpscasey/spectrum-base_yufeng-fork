/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.view.conf;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.inject.Inject;
import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.configuration.ApplicationConfiguration;
import com.nexala.spectrum.db.beans.UserConfiguration;
import com.nexala.spectrum.licensing.Licence;
import com.nexala.spectrum.rest.data.UserConfigurationProvider;
import com.nexala.spectrum.rest.data.UserMapConfiguration;
import com.nexala.spectrum.rest.data.beans.UnitSummaryButton;
import com.nexala.spectrum.view.Constants;
import com.nexala.spectrum.view.Plugin;
import com.nexala.spectrum.view.PluginFactory;
import com.nexala.spectrum.view.conf.maps.Coordinate;
import com.nexala.spectrum.view.conf.maps.ExtraLocation;
import com.nexala.spectrum.view.conf.maps.ExtraLocationDropdown;
import com.nexala.spectrum.view.conf.maps.MapLicencedConfiguration;
import com.nexala.spectrum.view.conf.maps.MapMarkerConfiguration;
import com.nexala.spectrum.view.conf.maps.MapMarkerLegend;
import com.nexala.spectrum.view.conf.maps.MapMarkerLegendOption;
import com.nexala.spectrum.view.conf.maps.MapType;
import com.nexala.spectrum.view.conf.maps.MapView;
import com.nexala.spectrum.view.conf.maps.NamedMapView;
import com.nexala.spectrum.view.conf.maps.RouteFile;
import com.nexala.spectrum.view.conf.maps.RouteTrack;
import com.nexala.spectrum.view.conf.maps.Station;
import com.nexala.spectrum.view.conf.maps.StationFile;
import com.nexala.spectrum.view.conf.maps.TrainIcon;
import com.nexala.spectrum.view.conf.maps.TrainLabel;
import com.nexala.spectrum.view.conf.xml.RouteXmlParser;
import com.nexala.spectrum.view.conf.xml.StationXmlParser;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigRenderOptions;

public class DefaultMapConfiguration extends AbstractDetailScreen implements MapConfiguration, Linkable {

	protected final PluginFactory factory;

	private final ApplicationConfiguration config;

    private final List<RouteFile> routeFiles;

	private final List<StationFile> stationFiles;

	private final List<Plugin> plugins;

	private final NamedMapView defaultView;

	private final List<NamedMapView> extraViews;
	
	private final List<ExtraLocationDropdown> extraLocation;

	private final boolean showFleetFullNames;

	private final boolean showClearApplyButtons;

	private final MapView defaultDetailView;

	private final List<Field> details;

	private final List<UnitSummaryButton> buttons;

	private final List<TrainIcon> trainIcons;

	private final TrainLabel trainLabel;

	private final Map<String, MapMarkerConfiguration> markerConfigurations;

	private final String tabName;

	private Long maxInactiveTime;
	
	private String mapUnitSearchPanelOffset;

	private List<MapFilter> filters;

	@Inject
	private RouteXmlParser routeXmlParser;

	@Inject
	private StationXmlParser stationXmlParser;

	@Inject
	private UserConfigurationProvider userConfigProvider;

	@Inject
	public DefaultMapConfiguration(PluginFactory factory, ApplicationConfiguration applicationConfiguration) {
		this.factory = factory;
		plugins = new ArrayList<Plugin>();

		this.config = applicationConfiguration;
		Config mapConf = applicationConfiguration.get().getConfig("r2m.fleetLocation");

		this.stationFiles = new ArrayList<StationFile>();
		List<? extends Config> stations = mapConf.getConfigList("stations");
		for (Config station : stations) {
		    List <String> fleets = null;
		    if (station.hasPath("fleets") ) {
		        fleets = station.getStringList("fleets");
		    }
		    String fileName = station.getString("file");
			List <Station> stationList = stationXmlParser.parse(station.getString("file"));
			StationFile file = new StationFile(fleets, fileName, stationList);
			this.stationFiles.add(file);
		}
		
		this.extraLocation = new ArrayList<ExtraLocationDropdown>();
		if (mapConf.hasPath("extraLocationDropdown")) {
			List<? extends Config> extraLocations = mapConf.getConfigList("extraLocationDropdown");
			for (Config locationConf : extraLocations) {
				String displayName = locationConf.getString("displayName");
				
				List<? extends Config> extraLocationConfs = locationConf.getConfigList("locations");
				
				List<ExtraLocation> extraLocs = new ArrayList<ExtraLocation>();
				
				for (Config extraLocationConf : extraLocationConfs) {
					String location = extraLocationConf.getString("text");
					Double latitude = extraLocationConf.getDouble("latitude");
					Double longitude = extraLocationConf.getDouble("longitude");
					
					extraLocs.add(new ExtraLocation(location, latitude, longitude));
				}
				
				extraLocation.add(new ExtraLocationDropdown(displayName, extraLocs));
			}
		}

		this.routeFiles = new ArrayList<RouteFile>();
		List<? extends Config> routes = mapConf.getConfigList("routes");
		for (Config route : routes) {
		    List <String> fleets = null;
            if (route.hasPath("fleets") ) {
                fleets = route.getStringList("fleets");
            }
		    String fileName = route.getString("file");
			List<RouteTrack> routeList = routeXmlParser.parse(route.getString("file"));
			RouteFile file = new RouteFile(fleets , fileName, routeList);
			this.routeFiles.add(file);
		}

		this.defaultView = buildNamedMapView(mapConf.getConfig("defaultView"));

		this.extraViews = new ArrayList<NamedMapView>();
		if (mapConf.hasPath("extraViews")) {
			List<? extends Config> extraViewsList = mapConf.getConfigList("extraViews");
			for (Config viewConf : extraViewsList) {
				this.extraViews.add(buildNamedMapView(viewConf));
			}
		}

		this.showFleetFullNames = mapConf.getBoolean("showFleetFullNames");

		this.showClearApplyButtons = mapConf.getBoolean("showClearApplyButtons");

		this.filters = new ArrayList<MapFilter>();
		if (mapConf.hasPath("filters")) {
			List<? extends Config> filtersConf = mapConf.getConfigList("filters");
			for (Config fc : filtersConf) {
				String id = null;
				String label = null;
				String type = null;
				String operator = null;
				Boolean showOnUnitLocation = false;
				Boolean enableByDefault = false;
				Boolean filterChecked = false;

				if (fc.hasPath("id")) {
					id = fc.getString("id");
				}
				if (fc.hasPath("label")) {
					label = fc.getString("label");
				}
				if (fc.hasPath("type")) {
					type = fc.getString("type");
				}
				if (fc.hasPath("operator")) {
					operator = fc.getString("operator");
				}
				if (fc.hasPath("showOnUnitLocation")) {
					showOnUnitLocation = fc.getBoolean("showOnUnitLocation");
				}
				if (fc.hasPath("enableByDefault")) {
					enableByDefault = fc.getBoolean("enableByDefault");
				}
				if (fc.hasPath("filterChecked")) {
				    filterChecked = fc.getBoolean("filterChecked");
				}

				this.filters.add(new MapFilter(id, label, type, operator, showOnUnitLocation, enableByDefault, filterChecked));
			}
		}

		if (mapConf.hasPath("maxInactiveTime")) {
			this.maxInactiveTime = mapConf.getLong("maxInactiveTime");
		}
		
		if (mapConf.hasPath("mapUnitSearchPanelOffset")) {
			this.mapUnitSearchPanelOffset = mapConf.getString("mapUnitSearchPanelOffset");
		}

		Config detailConf = mapConf.getConfig("details");

		this.defaultDetailView = buildMapView(detailConf.getConfig("defaultDetailView"));

		this.details = new ArrayList<Field>();
		List<? extends Config> detailsList = detailConf.getConfigList("fields");
		for (Config detail : detailsList) {
			this.details.add(config.buildField(detail, null));
		}

		this.buttons = new ArrayList<UnitSummaryButton>();
		if (detailConf.hasPath("linkButtons")) {
			List<? extends Config> buttons = detailConf.getConfigList("linkButtons");
			for (Config button : buttons) {
				this.buttons.add(buildButton(button));
			}
		}

		this.trainIcons = new ArrayList<TrainIcon>();
		if (mapConf.hasPath("trainIcon")) {
			List<? extends Config> icons = mapConf.getConfigList("trainIcon");
			for (Config icon : icons) {
				this.trainIcons.add(buildTrainIcon(icon));
			}
		}

		Config trainLabelConf = mapConf.getConfig("trainLabel");
		this.trainLabel = buildTrainLabel(trainLabelConf);

		markerConfigurations = new HashMap<String, MapMarkerConfiguration>();
		List<? extends Config> markers = mapConf.getConfigList("markers");
		for (Config marker : markers) {
			MapMarkerConfiguration markerConf = buildMarker(marker);
			addMarkerConfiguration(markerConf, marker);
		}

		this.tabName = mapConf.getString("tabName");
	}

	@Override
	public MapView getDefaultView() {
		return defaultView;
	}

	@Override
	public MapView getDefaultDetailView() {
		return defaultDetailView;
	}

	@Override
	public List<Field> getDetails() {
		return details;
	}

	@Override
	public List<UnitSummaryButton> getButtons() {
		return buttons;
	}

	@Override
	public Long getMaxInactiveTime() {
		return maxInactiveTime;
	}
	
	@Override
	public String getMapUnitSearchPanelOffset() {
		return mapUnitSearchPanelOffset;
	}

	@Override
	public List<TrainIcon> getTrainIcons() {
		return trainIcons;
	}

	@Override
	public TrainLabel getTrainLabel() {
		return trainLabel;
	}

	/**
	 * @{inheritDoc
	 */
	public String getRequiredRole() {
		return Constants.UNIT_MAP_LIC_KEY;
	}

	@Override
	public String getDisplayName() {
		return tabName; // FIXME i18n
	}

	@Override
	public String getTemplateName() {
		return "unit_map.html.ftl";
	}

	/**
	 * Return the list of map marker providers registers for this map.
	 * 
	 * @return the list of map marker providers registers for this map, empty
	 *         list if none.
	 */
	public Collection<MapMarkerConfiguration> getMapMarkersConfiguration() {
		return Collections.unmodifiableCollection(markerConfigurations.values());
	}

	@Override
	public MapMarkerConfiguration getMarkerConfiguration(String markerType) {
		return markerConfigurations.get(markerType);
	}

	@Override
	public MapLicencedConfiguration getMapLicencedObjects(Licence licence) {

		List<NamedMapView> views = new ArrayList<NamedMapView>();
		for (NamedMapView view : extraViews) {
			List<String> viewFleets = view.getFleets();
			if (viewFleets == null) {
				views.add(view);
			} else {
				for (String fleet : viewFleets) {
					String licencePath = "r2m.fleets." + fleet + ".licence";
					if (licence.hasResourceAccess(config.get().getString(licencePath))) {
						views.add(view);
						break;
					}
				}
			}
		}

		
		List<Station> stations = new ArrayList<Station>();
		List<String> stationCodes = new ArrayList<String>();
		for (StationFile file : stationFiles) {
		    if (file.getFleets() == null) {
		        stations.addAll(file.getStationList());
            } else {
                for (String fleet : file.getFleets()) {
                    String licencePath = "r2m.fleets." + fleet + ".licence";
                    if (fleet.equals(Spectrum.ALL_CODE) || !config.get().hasPath(licencePath)
                            || licence.hasResourceAccess(config.get().getString(licencePath))) {
                        for (Station newStation : file.getStationList()) {
                            if (!stationCodes.contains(newStation.getCode())) {
                                stationCodes.add(newStation.getCode());
                                stations.add(newStation);
                            }
                        }
                        break;
                    }
                }
            }
		}
		
		// Sort stations by name
		Collections.sort(stations, new Comparator<Station>() {
			@Override
			public int compare(Station s1, Station s2) {

				return s1.getName().compareTo(s2.getName());
			}
		});

		List<RouteTrack> routes = new ArrayList<RouteTrack>();
		for (RouteFile file : routeFiles) {
		    if (file.getFleets() == null) {
		        routes.addAll(file.getRouteList());
            } else {
                for (String fleet : file.getFleets()) {
                    String licencePath = "r2m.fleets." + fleet + ".licence";
                    if (fleet.equals(Spectrum.ALL_CODE) || !config.get().hasPath(licencePath)
                            || licence.hasResourceAccess(config.get().getString(licencePath))) {
                        routes.addAll(file.getRouteList());
                        break;
                    }
                }
            }
		}
		
		return new MapLicencedConfiguration(views, stations, routes, extraLocation);
	}

	@Override
	public void addMarkerConfiguration(MapMarkerConfiguration markerConfiguration, Config config) {

		markerConfigurations.put(markerConfiguration.getMarkerType(), markerConfiguration);

		List<String> jsInclude = new ArrayList<String>();
		if (config.hasPath("jsInclude")) {
			jsInclude.add(config.getString("jsInclude"));
		}

		List<String> cssInclude = new ArrayList<String>();
		if (config.hasPath("cssInclude")) {
			cssInclude.add(config.getString("cssInclude"));
		}

		Plugin plugin = factory.create("", jsInclude, cssInclude);
		plugins.add(plugin);
	}

	@Override
	public List<MapFilter> getFilters() {
		return filters;
	}

	private NamedMapView buildNamedMapView(Config config) {
		String id = config.getString("id");
		String displayName = config.getString("displayName");

		Config northWest = config.getConfig("northWest");
		Double nwLatitude = northWest.getDouble("latitude");
		Double nwLongitude = northWest.getDouble("longitude");

		Config southEast = config.getConfig("southEast");
		Double seLatitude = southEast.getDouble("latitude");
		Double seLongitude = southEast.getDouble("longitude");

		String type = config.getString("type");
		MapType mapType = null;
		switch (type.toUpperCase()) {
		case "ROAD":
			mapType = MapType.ROAD;
			break;
		case "AERIAL":
			mapType = MapType.AERIAL;
			break;
		}

		List<String> fleets = null;
		if (config.hasPath("fleets")) {
			fleets = config.getStringList("fleets");
		}

		return new NamedMapView(id, displayName, new Coordinate(nwLatitude, nwLongitude),
				new Coordinate(seLatitude, seLongitude), mapType, fleets);
	}

	private MapView buildMapView(Config config) {
		Double latitude = config.getDouble("latitude");
		Double longitude = config.getDouble("longitude");
		int zoom = config.getInt("zoom");

		String type = config.getString("type");
		MapType mapType = null;
		switch (type.toUpperCase()) {
		case "ROAD":
			mapType = MapType.ROAD;
			break;
		case "AERIAL":
			mapType = MapType.AERIAL;
			break;
		}

		return new MapView(new Coordinate(latitude, longitude), zoom, mapType);
	}

	private TrainIcon buildTrainIcon(Config config) {
		String name = config.getString("name");
		String image = config.getString("image");
		int zIndex = config.getInt("z-index");

		return new TrainIcon(name, image, zIndex);
	}

	private UnitSummaryButton buildButton(Config buttonConfig) {
		String id = buttonConfig.getString("id");
		String name = buttonConfig.getString("displayName");

		String unitSummaryTab = null;
		if (buttonConfig.hasPath("unitSummaryTab")) {
			unitSummaryTab = buttonConfig.getString("unitSummaryTab");
		}

		return new UnitSummaryButton(id, name, true, unitSummaryTab, null);
	}

	private TrainLabel buildTrainLabel(Config config) {
		boolean pointer = config.getBoolean("pointer");
		int xOffset = config.getInt("x-offset");
		int yOffset = config.getInt("y-offset");
		int width = config.getInt("width");
		int height = config.getInt("height");

		TrainLabel result = new TrainLabel(pointer, xOffset, yOffset, width, height);

		if (config.hasPath("title")) {
			result.setTitle(config.getString("title"));
		}

		if (config.hasPath("description")) {
			List<MouseOverText> description = new ArrayList<MouseOverText>();
			List<? extends Config> descList = config.getConfigList("description");
			for (Config desc : descList) {
				description.add(buildMouseOver(desc));
			}
			result.setDescription(description);
		}

		return result;
	}

	private MouseOverText buildMouseOver(Config mouseOverConf) {
		MouseOverText result = new MouseOverText();

		result.setLabel(mouseOverConf.getString("label"));

		if (mouseOverConf.hasPath("value")) {
			result.setValue(mouseOverConf.getString("value"));
		}
		if (mouseOverConf.hasPath("format")) {
			result.setFormat(mouseOverConf.getString("format"));
		}
		if (mouseOverConf.hasPath("constant")) {
			result.setConstant(mouseOverConf.getString("constant"));
		}

		return result;
	}

	private MapMarkerConfiguration buildMarker(Config config) {
		MapMarkerConfiguration result = new MapMarkerConfiguration();

		result.setMarkerType(config.getString("type"));
		result.setJsObject(config.getString("jsObject"));
		result.setSettingLabel(config.getString("label"));
		result.setEnabledByDefault(config.hasPath("enabledByDefault") ? config.getBoolean("enabledByDefault") : false);
		result.setRedrawOnViewChange(
				config.hasPath("redrawOnViewChange") ? config.getBoolean("redrawOnViewChange") : true);
		result.setzIndex(config.hasPath("z-index") ? config.getInt("z-index") : 0);

		if (config.hasPath("legend")) {
			result.setLegend(buildLegend(config.getConfig("legend")));
		}

		if (config.hasPath("config")) {
			result.setConfig(config.getConfig("config").root().render(ConfigRenderOptions.concise().setJson(true)));
		}

		if (config.hasPath("datasource")) {
			result.setDatasource(config.getString("datasource"));
		}

		return result;
	}

	private MapMarkerLegend buildLegend(Config config) {
		String id = config.getString("id");
		String label = config.getString("label");
		boolean export = config.getBoolean("export");

		List<MapMarkerLegendOption> legendOptions = new ArrayList<MapMarkerLegendOption>();
		List<? extends Config> options = config.getConfigList("options");
		for (Config opt : options) {
			legendOptions.add(buildLegendOption(opt));
		}

		return new MapMarkerLegend(id, label, export, legendOptions);
	}

	private MapMarkerLegendOption buildLegendOption(Config config) {
		String id = config.getString("id");
		String label = config.getString("label");
		int category = config.getInt("category");
		String color = config.getString("color");
		boolean enabled = config.getBoolean("enabled");
		int lowerThreshold = config.getInt("lowerThreshold");
		int upperThreshold = config.getInt("upperThreshold");

		return new MapMarkerLegendOption(id, label, category, color, enabled, lowerThreshold, upperThreshold);
	}

	@Override
	public UserMapConfiguration getUserMapConfiguration(Licence licence) {
		return new UserMapConfiguration(getUserConfiguration(licence).getData(), this);
	}

	@Override
	public UserConfiguration getUserConfiguration(Licence licence) {
		return userConfigProvider.getUserConfiguration(licence.getLoginUser().getLogin(), "mapFilters");
	}

	@Override
	public Plugin getParentPlugin() {
		return config.getParentPlugin(Spectrum.FLEET_LOCATION_ID);
	}

	/**
	 * Could not be overridden. If need to add additional plugins please use
	 * {@link DefaultMapConfiguration#getAdditionalPlugins()}
	 * 
	 * @see com.nexala.spectrum.view.conf.AbstractDetailScreen#getPlugins()
	 */
	@Override
	public final List<Plugin> getPlugins() {
		List<Plugin> result = new ArrayList<Plugin>(plugins);
		result.addAll(getAdditionalPlugins());
		return result;
	}

	/**
	 * Add any additional plugins here
	 * 
	 * @return
	 */
	public List<Plugin> getAdditionalPlugins() {
		return config.getPlugins(Spectrum.FLEET_LOCATION_ID);
	}

	/**
	 * Default map container Id is map
	 * 
	 * @see com.nexala.spectrum.view.conf.maps.GenericMapConfiguration#getContainerId()
	 */
	@Override
	public String getContainerId() {
		return "map";
	}

	@Override
	public String getScreenName() {
		return "Fleet Location";
	}

	@Override
	public boolean isShowFleetFullNames() {
		return showFleetFullNames;
	}

	@Override
	public ScreenType getScreenType() {
		return ScreenType.TAB;
	}

	@Override
	public boolean isShowClearApplyButtons() {
		return showClearApplyButtons;
	}

    @Override
    public String getLink() {
        return "unitLocationTab";
    }

    @Override
    public String getIconClass() {
        return "ui-icon-image";
    }
}
