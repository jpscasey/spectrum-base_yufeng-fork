<div id = 'mapContainer'>
    <div id ='mapToolbar' class = 'ui-widget-header'>
    </div>
    
    <div id='map'>
        <div class = 'zoomer'>
            <div class = 'zoomout ui-corner-left ui-widget-header'>&#8722;</div>
            <div class = 'zoomin ui-corner-right ui-widget-header'>&#43;</div>
        </div>
    
        <div id='mapInfoOverlay'></div>
    </div>
    
    <div id='routeMap' class = 'ui-widget' style="display : none; background-color  : white; width : 100%; position : absolute; top : 40px; bottom : 0px; overflow : auto; z-index : 2;">
        <div id='routeMapCmp' class = 'ui-widget'></div>
    </div>
</div>
