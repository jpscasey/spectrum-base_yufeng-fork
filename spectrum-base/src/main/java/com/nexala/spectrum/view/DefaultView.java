/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.view;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.inject.Singleton;
import com.nexala.spectrum.licensing.Licence;
import com.nexala.spectrum.licensing.RestrictedResource;
import com.nexala.spectrum.view.conf.App;

@Singleton
public class DefaultView extends TemplateServlet {

    private static final long serialVersionUID = -7861702525454545759L;

    @RestrictedResource(Constants.DEFAULT_LIC_KEY)
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        Licence licence = Licence.get(request, response, true);

        // if the user has a licenced app calls that app
        App[] apps = appConf.getApps(licence);
        if (apps != null && apps.length > 0) {
            response.sendRedirect(apps[0].getPath());
            return;
        }

        // if the user does not have a licenced app calls the empty/default app
        List<String> includes = new ArrayList<String>();
        Map<String, Object> rootMap = new HashMap<String, Object>();
        rootMap.put("includes", includes);
        doRender(request, response, "default.html.ftl", rootMap, licence);
    }
    
    public App getView() {
        return null;
    }
}