<div id="recovery">
    <div id = 'faultSummary'>
        <div id = "faultSummaryHeader" class = "header ui-widget-header">
            <div class = "tableTitle" data-i18n="Fault Summary"></div>
        </div>
        <div id = "faultSummaryTable"></div>     
    </div>
    <div id = 'worstCaseScenario'>
        <div class = "tableTitle" data-i18n="Worst Case Scenario"></div>
        <textarea id = "worstCaseScenarioBox" readonly></textarea>
    </div>
    <div id = 'recoveryDetails'>
        <div id = "recoveryDetailsHeader" class = "header ui-widget-header">
            <div class = "tableTitle" data-i18n="Recovery Details"></div>
        </div>
        <div id = "recoveryDetailsTable"></div>
    </div>
</div>