/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.view.conf;

import java.util.ArrayList;
import java.util.List;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.configuration.ApplicationConfiguration;
import com.nexala.spectrum.view.Plugin;
import com.typesafe.config.Config;

@Singleton
public class DefaultChannelDefinitionConfiguration implements ChannelDefinitionConfiguration {

    private final ApplicationConfiguration config;
    
    private final List<Field> details;
    
    private final List<Column> columns;
    
    private final boolean nameAsValidationsSource;
    
    @Inject
    public DefaultChannelDefinitionConfiguration(ApplicationConfiguration config) {
        this.config = config;
        
        Config channelDefinitionConf = config.get().getConfig("r2m.channelDefinition");
        
        this.columns = new ArrayList<Column>();
        List<? extends Config> columns = channelDefinitionConf.getConfigList("columns");
        for (Config column : columns) {
            this.columns.add(config.columnBuilder(column).build());
        }
        
        List<? extends Config> fields = channelDefinitionConf.getConfigList("details");
        this.details = new ArrayList<Field>();
        for (Config field : fields) {
        	details.add(config.buildField(field, null));
        }
        
        this.nameAsValidationsSource = channelDefinitionConf.getBoolean("nameAsValidationsSource");
    }

    @Override
    public List<Column> getColumns() {
        return columns;
    }

    @Override
    public List<Field> getDetails() {
    	return details;
    }
    
    @Override
    public List<Plugin> getPlugins() {
        return config.getPlugins(Spectrum.CHANNEL_DEFINITION_ID);
    }

    @Override
    public Plugin getParentPlugin() {
        return config.getParentPlugin(Spectrum.CHANNEL_DEFINITION_ID);
    }

    @Override
    public String getScreenName() {
        return "Channel Definition";
    }
    
    @Override
    public boolean getNameAsValidationsSource() {
        return nameAsValidationsSource;
    }
}