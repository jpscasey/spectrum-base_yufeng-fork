/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.view.conf;

public class FleetFilter {

    public static final String CHECKBOX_TYPE = "checkBox";
    public static final String TEXTBOX_TYPE = "textBox";

    public static final String HEADCODE_ID = "headCodeFilter";
    public static final String UNIT_ID = "unitFilter";
    public static final String LOCATION_ID = "locationFilter";
    public static final String SERVICE_ID = "serviceFilter";
    public static final String FAULT_ID = "faultFilter";

    private String id;
    private String label;
    private String type;
    private Boolean optional;
    private String[] columnIds;
    

    public FleetFilter(String id, String label, String type, Boolean optional, String... columnIds) {
        this.id = id;
        this.label = label;
        this.type = type;
        this.optional = optional;
        this.columnIds = columnIds;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String[] getColumnIds() {
        return columnIds;
    }

    public void setColumnIds(String... columnIds) {
        this.columnIds = columnIds;
    }

    public Boolean getOptional() {
        return optional;
    }

    public void setOptional(Boolean optional) {
        this.optional = optional;
    }

}