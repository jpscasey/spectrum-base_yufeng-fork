/*
 * Copyright (c) Nexala Technologies 2013, All rights reserved.
 */
package com.nexala.spectrum.view.conf;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.google.common.collect.Lists;
import com.google.inject.Inject;
import com.nexala.spectrum.configuration.ApplicationConfiguration;
import com.nexala.spectrum.licensing.RestrictedResource;
import com.nexala.spectrum.rest.data.beans.Schematic;
import com.nexala.spectrum.rest.data.beans.SchematicChannel;
import com.nexala.spectrum.view.Constants;
import com.nexala.spectrum.view.Plugin;
import com.nexala.spectrum.view.PluginFactory;
import com.typesafe.config.Config;

/**
 * Implementation of SchematicConfiguration
 * 
 * @author Simon
 *
 */

@RestrictedResource(Constants.UNIT_SCHEMATIC_LIC_KEY)
public class DefaultSchematicConfiguration extends AbstractDetailScreen implements SchematicConfiguration, Linkable {
	@Inject
	private PluginFactory factory;

	private String pluginName;
	private List<String> pluginList;

	private List<Schematic> confItems = new ArrayList<Schematic>();
	private List<SchematicChannel> confSchemViewChannels = new ArrayList<SchematicChannel>();

	@Inject
	public DefaultSchematicConfiguration() {
		ApplicationConfiguration defaultConfig = new ApplicationConfiguration();
		Config mainConfig = defaultConfig.get().getConfig("r2m.schematic");
		
		if (mainConfig.hasPath("items") && mainConfig.hasPath("schematicdata")) {
			List<? extends Config> items = mainConfig.getConfigList("items");
			List<? extends Config> schemChannels = mainConfig.getConfigList("schematicdata");

			// read from configuration file for schematic
			// all fields required by the javascript code to make schematics
			// appear in the Schematic tab
			for (Config item : items) {
				Schematic schematicBean = new Schematic();
				List<SchematicChannel> channelList = new ArrayList<SchematicChannel>();

				schematicBean.setName(item.getString("name"));
				schematicBean.setDisplayName(item.getString("displayName"));
				schematicBean.setJSClassPath(item.getString("javaScriptClassPath"));
				schematicBean.setJSConstructor(item.getString("javaScriptConstructor"));

				// create configuration for sub parameters of item object

				Config confCanvasSize = item.getConfig("canvas_size");
				Config confRaphaelSize = item.getConfig("raphael_size");
				Config confSchematicParams = item.getConfig("schematic_params");

				HashMap<String, Integer> canvasSize = new HashMap<String, Integer>();
				HashMap<String, Integer> raphaelSize = new HashMap<String, Integer>();
				HashMap<String, Double> schematicParams = new HashMap<String, Double>();

				// read parameters from configuration files and store them in
				// a temporary object

				canvasSize.put("width", confCanvasSize.getInt("width"));
				canvasSize.put("height", confCanvasSize.getInt("height"));

				raphaelSize.put("width", confRaphaelSize.getInt("width"));
				raphaelSize.put("height", confRaphaelSize.getInt("height"));

				schematicParams.put("xoffset", confSchematicParams.getDouble("xoffset"));
				schematicParams.put("yoffset", confSchematicParams.getDouble("yoffset"));
				schematicParams.put("scale", confSchematicParams.getDouble("scale"));

				// store temporary objects in schematic bean
				// which will be returned by the configuration class

				schematicBean.setCanvasSize(canvasSize);
				schematicBean.setRaphaelSize(raphaelSize);
				schematicBean.setSchematicParams(schematicParams);
				
				if (mainConfig.hasPath("schematicdata")) {
					// read from configuration file for schematic data
					// channel list is configurable for different fleets
					for (Config channel : schemChannels) {
						if (channel.getStringList("view").contains(schematicBean.getName())) {
							SchematicChannel channelTemp = new SchematicChannel();
							channelTemp.setChannelId(channel.getInt("channelId"));
							channelTemp.setName(channel.getString("name"));
							channelList.add(channelTemp);
						}
					}
					schematicBean.setChannelList(channelList);
				}
				this.confItems.add(schematicBean);
			}
			for (Config channel : schemChannels) {
				SchematicChannel channelTemp = new SchematicChannel();
				channelTemp.setName(channel.getString("name"));
				channelTemp.setChannelId(channel.getInt("channelId"));
				channelTemp.setType(channel.getString("type"));
				if (channel.hasPath("default")) {
					channelTemp.setDefaultValue(channel.getString("default"));
				}
				if (channel.hasPath("unitOfMeasurement")) {
					channelTemp.setUnitOfMeasurement(channel.getString("unitOfMeasurement"));
				}
				this.confSchemViewChannels.add(channelTemp);
			}
		}

		// store the js plugin reading from the configuration file
		if (mainConfig.hasPath("plugins")) {
			Config pluginConf = mainConfig.getConfig("plugins");
			pluginName = pluginConf.getString("name");
			pluginList = pluginConf.getStringList("sources");
		}
	}

	@Override
	public String getRequiredRole() {
		return Constants.UNIT_SCHEMATIC_LIC_KEY;
	}

	@Override
	public String getTemplateName() {
		return "unit_schematic.html.ftl";
	}

	@Override
	public String getDisplayName() {
		return "Schematic";
	}

	@Override
	public List<Plugin> getPlugins() {

		return Lists.newArrayList(factory.create(pluginName, pluginList, Lists.newArrayList()));
	}

	@Override
	public Plugin getParentPlugin() {
		return factory.create("nx.unit.Schematic",
				Lists.newArrayList("js/nexala.schematic.js", "js/raphael-2.1.min.js", "js/nexala.raphael.js"),
				Lists.newArrayList("css/nexala.schematic.css"));
	}

	@Override
	public ScreenType getScreenType() {
		return ScreenType.TAB;
	}

	@Override
	public String getLink() {
		return "schematicViewTab";
	}

	@Override
	public String getIconClass() {
		return "ui-icon-key";
	}

	@Override
	public List<Schematic> getItems() {
		return confItems;
	}

	@Override
	public List<SchematicChannel> getSchematicData() {
		return confSchemViewChannels;
	}
}
