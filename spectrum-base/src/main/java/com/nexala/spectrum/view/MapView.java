/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.view;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.common.collect.Lists;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.nexala.spectrum.licensing.Licence;
import com.nexala.spectrum.licensing.RestrictedResource;
import com.nexala.spectrum.view.conf.App;
import com.nexala.spectrum.view.conf.AppConfiguration;
import com.nexala.spectrum.view.conf.CommonFleetConfiguration;
import com.nexala.spectrum.view.conf.MapConfiguration;

@Singleton
public class MapView extends TemplateServlet {
    @Inject
    private MapConfiguration conf;
    
    @Inject
    private CommonFleetConfiguration fleetConf;

    @Inject
    private AppConfiguration appConf;

    private static final long serialVersionUID = -2329807484508819416L;

    @RestrictedResource(Constants.MAP_LIC_KEY)
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException {

        Licence licence = Licence.get(request, response, true);

        List<String> includes = new ArrayList<String>();
        Map<String, Object> rootMap = new HashMap<String, Object>();
        rootMap.put("includes", includes);
        rootMap.put("head", "maps_head.html.ftl");
        rootMap.put("conf", conf);
        rootMap.put("userConf", conf.getUserConfiguration(licence));
        rootMap.put("fleetInfoList", fleetConf.getFleetList(licence));
        rootMap.put("cssIncludesList", Lists.partition(getCssIncludes(), 10));
        rootMap.put("jsIncludesList", Lists.partition(getJsIncludes(), 10));
        rootMap.put("screenName", conf.getScreenName());
        doRender(request, response, "maps.html.ftl", rootMap, licence);
    }
    
    private List<String> getCssIncludes() {
        List<Plugin> plugins = conf.getPlugins();
        
        List<String> cssIncludes = new ArrayList<String>();
        
        for (Plugin p : plugins) {
            cssIncludes.addAll(p.getStylesheets());
        }
        
        return new ArrayList<String>(cssIncludes);
    }
    
    private List<String> getJsIncludes() {
        List<Plugin> plugins = conf.getPlugins();
        
        List<String> jsIncludes = new ArrayList<String>();
        
        for (Plugin p : plugins) {
            jsIncludes.addAll(p.getSources());
        }
        
        return jsIncludes;
    }
    
    public App getView() {
        return appConf.getApp(Constants.FLEET_LOCATION);
    }
}