/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.view;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.nexala.spectrum.licensing.Licence;
import com.nexala.spectrum.licensing.RestrictedResource;
import com.nexala.spectrum.view.conf.App;
import com.nexala.spectrum.view.conf.AppConfiguration;
import com.nexala.spectrum.view.conf.DownloadConfiguration;

@Singleton
public class DownloadView extends TemplateServlet {
    private static final long serialVersionUID = -4589530905817739310L;

    @Inject
    DownloadConfiguration downloadConfiguration;
    
    @Inject
    private AppConfiguration appConf;

    @RestrictedResource(Constants.DOWNLOAD_LIC_KEY)
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException {

        Licence licence = Licence.get(request, response, true);
        Map<String, Object> rootMap = new HashMap<String, Object>();
        rootMap.put("head", "download_head.html.ftl");
        rootMap.put("includes", Collections.emptyList());
        rootMap.put("cssIncludes", getCssIncludes());
        rootMap.put("jsIncludes", getJsIncludes());
        rootMap.put("isVehicleSelector", downloadConfiguration.isVehicleSelector());
        rootMap.put("screenName", downloadConfiguration.getScreenName());
        doRender(request, response, "downloads.html.ftl", rootMap, licence);
    }
    
    public App getView() {
        return appConf.getApp(Constants.DOWNLOADS);
    }
    
    
    private List<String> getCssIncludes() {
        List<Plugin> plugins = downloadConfiguration.getPlugins();
        
        List<String> cssIncludes = new ArrayList<String>();
        
        for (Plugin p : plugins) {
            cssIncludes.addAll(p.getStylesheets());
        }
        
        return new ArrayList<String>(cssIncludes);
    }
    
    private List<String> getJsIncludes() {
        List<Plugin> plugins = downloadConfiguration.getPlugins();
        
        List<String> jsIncludes = new ArrayList<String>();
        
        for (Plugin p : plugins) {
            jsIncludes.addAll(p.getSources());
        }
        
        return new ArrayList<String>(jsIncludes);
    }
}
