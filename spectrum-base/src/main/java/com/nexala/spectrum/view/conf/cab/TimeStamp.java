/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.view.conf.cab;

/**
 * Widget to display the date and time.
 * @author jgalvez
 *
 */
public class TimeStamp extends Widget {
    
    
    public TimeStamp(String domId, int x, int y, int h, int w, 
            String... channels) {
        super(domId, x, y, h, w, channels);
    }
    
    @Override
    public String getWidgetClassName() {
        return "nx.cab.TimeStamp";
    }
    
}