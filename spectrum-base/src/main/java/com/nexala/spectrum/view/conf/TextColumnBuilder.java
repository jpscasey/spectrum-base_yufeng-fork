/*
 * Copyright (c) Nexala Technologies 2013, All rights reserved.
 */
package com.nexala.spectrum.view.conf;

public class TextColumnBuilder extends ColumnBuilder {
    public TextColumnBuilder() {
        super();
        type(ColumnType.TEXT);
        width(40);
    }
}
