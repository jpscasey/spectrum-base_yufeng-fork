/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.view.conf.cab;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.nexala.spectrum.channel.ChannelCategory;

/**
 * Settings for a value widget.
 * @author brice
 *
 */
@JsonInclude(Include.NON_NULL)
public class ValueSettings implements Settings {
    
    public enum Position { TOP, BOTTOM, LEFT, RIGHT};
    
    public enum TextAnchor {START, MIDDLE, END};
    
    private Map<Integer, String> categoryColor = new HashMap<Integer, String>(); 

    private Position labelPosition = Position.LEFT;
    
    private TextAnchor textAnchor = TextAnchor.MIDDLE;
    
    private String labelColor = null;

    private String label = null;
    
    private String suffix = null;
    
    private String defaultColor = null;
    
    private Integer labelWidth = null;
    
    private String cssClassValue = null;
    
    private String format = null;
    
    public String getLabel() {
        return label;
    }

    public ValueSettings setLabel(String label) {
        this.label = label;
        return this;
    }
    
    /**
     * The default color for the value field.
     * @return
     */
    public String getDefaultColor() {
        return defaultColor;
    }

    /**
     * Set the default color for the value field.
     * @param valueColor
     * @return
     */
    public ValueSettings setDefaultColor(String valueColor) {
        this.defaultColor = valueColor;
        return this;
    }
    
    /**
     * Associate a channel category to a color.
     * @param category the category
     * @param color the color 
     * @return the value setting
     */
    public ValueSettings setColorForChanCategory(ChannelCategory category, String color) {
        categoryColor.put(category.getSeverity(), color);
        return this;
    }

    public ValueSettings setLabelPosition(Position pos) {
        this.labelPosition = pos;
        return this;
    }
    
    public int getLabelPosition() {
        return labelPosition.ordinal();
    }
    
    public Map<Integer, String> getColorForChanCategory() {
        return categoryColor;
    }
    
    public String getLabelColor() {
        return labelColor;
    }

    public ValueSettings setLabelColor(String labelColor) {
        this.labelColor = labelColor;
        return this;
    }

    /**
     * Getter for suffix.
     * @return the suffix
     */
    public String getSuffix() {
        return suffix;
    }

    /**
     * Setter for suffix.
     * @param suffix the suffix to set
     * @return 
     */
    public ValueSettings setSuffix(String suffix) {
        this.suffix = suffix;
        return this;
    }

    /**
     * Getter for textAnchor.
     * @return the textAnchor
     */
    public int getTextAnchor() {
        return textAnchor.ordinal();
    }

    /**
     * Setter for textAnchor.
     * @param textAnchor the textAnchor to set
     */
    public ValueSettings setTextAnchor(TextAnchor textAnchor) {
        this.textAnchor = textAnchor;
        return this;
    }

    /**
     * Getter for labelWidth.
     * @return the labelWidth
     */
    public Integer getLabelWidth() {
        return labelWidth;
    }

    /**
     * Setter for labelWidth.
     * @param labelWidth the labelWidth to set
     */
    public ValueSettings setLabelWidth(Integer labelWidth) {
        this.labelWidth = labelWidth;
        return this;
    }

    /**
     * Getter for cssClassValue.
     * @return the cssClassValue
     */
    public String getCssClassValue() {
        return cssClassValue;
    }

    /**
     * Setter for cssClassValue.
     * @param cssClassValue the cssClassValue to set
     */
    public ValueSettings setCssClassValue(String cssClassValue) {
        this.cssClassValue = cssClassValue;
        return this;
    }

	public String getFormat() {
		return format;
	}

	public ValueSettings setFormat(String format) {
		this.format = format;
		return this;
	}
    
   

}