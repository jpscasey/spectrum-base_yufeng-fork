/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.view.conf.cab;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class LeverSettings implements Settings {
    private String[] values;

    public String[] getValues() {
        return values;
    }

    public LeverSettings setValues(String[] values) {
        this.values = values;
        return this;
    }
}