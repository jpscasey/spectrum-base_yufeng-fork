<link rel = 'stylesheet' type = 'text/css' href = 'css/merge?resources=<#t>
    css/nexala.maps.css,<#t>
    css/nexala.timepicker.css,<#t>
    css/nexala.togglebutton.css<#t>
    &view=events<#t>
'/>

<#list cssIncludesList as cssIncludes>
    <link rel = 'stylesheet' type = 'text/css' href = 'css/merge?resources=<#t>
        <#list cssIncludes as cssInclude>${cssInclude},</#list><#t>
        &view=events<#t>
    '><#t>
</#list>

<script type = 'text/javascript' src = 'js/merge?resources=<#t>
    js/raphael-2.1.min.js,<#t>
    js/nexala.raphael.js,<#t>
    js/nexala.map.marker.js,<#t>
    js/nexala.maps.js,<#t>
    js/i18n/datepicker-nl.js,<#t>
    js/i18n/datepicker-en-GB.js,<#t>
    js/nexala.timepicker.js,<#t>
    js/nexala.togglebutton.js,<#t>
    js/map/nexala.generic.maps.js<#t>
    &view=maps<#t>
'></script><#t>

<#list jsIncludesList as jsIncludes><#t>
    <script type = 'text/javascript' src = 'js/merge?resources=<#t>
        <#list jsIncludes as jsInclude>${jsInclude},</#list><#t>
        &view=maps<#t>
    '></script><#t>
</#list><#t>



<script type = 'text/javascript'>
    //maps access logging
    nx.rest.logging.map();

    (function() {
        var formatStations = function(stations) {
            var result = [];
        
            for (var index in stations) {
                var station = stations[index];
                
                result.push({
                    'name': station.name,
                    'code': station.code,
                    'coordinate' : {
                        'latitude': station.coordinate.latitude,
                        'longitude': station.coordinate.longitude
                    },
                    'type': station.type
                });
            }
            
            return result;
        };
    
        var formatRoutes = function(routes) {
            var result = [];
            
            for (var index in routes) {
                var route = routes[index];
    
                var coordinates = [];
                for (var i in route.points) {
                    var coordinate = route.points[i];
                    coordinates.push({
                        'latitude': coordinate.latitude,
                        'longitude': coordinate.longitude
                    });
                }
                
                result.push({
                    'coordinate' : coordinates
                });
            }
            
            return result;
        };
        
        var formatNamedViews = function(namedViews) {
            var result = [];
            
            for (var index in namedViews) {
                var namedView = namedViews[index];
                result.push({
                    'desc': namedView.description,
                    'type': namedView.mapType,
                    'id': namedView.id,
                    'northWest': {
                        'latitude': namedView.northWest.latitude,
                        'longitude': namedView.northWest.longitude
                    },
                    'southEast': {
                        'latitude': namedView.southEast.latitude,
                        'longitude': namedView.southEast.longitude
                    }           
                });
            }
            
            return result;
        };
        
        $(document).ready(function() {
            
            <#list conf.plugins as plugin>
                <#if plugin.pluginClass?has_content>
                    var clazz = eval(${plugin.pluginClass});
                    var plugInst = new clazz();
                </#if>
            </#list> 
            
            var defaultView = {
                'desc': '${conf.defaultView.description}',
                'type': '${conf.defaultView.mapType}',
                'id': '${conf.defaultView.id}',
                'northWest': {
                    'latitude': '${conf.defaultView.northWest.latitude?string("0.000000")}', 
                    'longitude': '${conf.defaultView.northWest.longitude?string("0.000000")}'
                },
                'southEast': {
                    'latitude': '${conf.defaultView.southEast.latitude?string("0.000000")}', 
                    'longitude': '${conf.defaultView.southEast.longitude?string("0.000000")}'
                }
            };
    
            //TODO: the visible checkboxes options should come from the MapConfigurationImpl 
            var toolbarConf = {
                'container': $('#mapToolbar'),
                'route': { 'visible': true },
                'station': { 'visible': true },
                'train': { 'visible': true },
                'trainLabel': { 'visible': true },
                'defaultView': defaultView,
                'searchUnit': true,
                'fleetInfoList': [
                    <#list fleetInfoList as fleetInfo>
                        {
                            'code' : '${fleetInfo.code}',
                            'description' : '${fleetInfo.name}'
                        }
                        <#if fleetInfo_has_next>,</#if>
                    </#list>
                ],
                'fleetFullNames': ${conf.showFleetFullNames?string},
                'clearApplyButtons': ${conf.showClearApplyButtons?string},
                'mapUnitSearchPanelOffset': '${conf.mapUnitSearchPanelOffset?string}',
                'filters': [
                    <#list conf.filters as filter>
                        {
                            'id' : '${filter.id}',
                            'label' : '${filter.label}',
                            'type' : '${filter.type}',
                            'operator' : '${filter.operator}',
                            'filterChecked' : '${filter.filterChecked?string}',
                            'showOnUnitLocation' : ${filter.showOnUnitLocation?string},
                            'enableByDefault' : ${filter.enableByDefault?string}
                        }
                        <#if filter_has_next>,</#if>
                    </#list>
                ]
            };

            var mapConf = {
                //bing maps requires DOM element instead a JQuery Object
                'container': $('#${conf.containerId}')[0],
                'defaultView': defaultView,
                'mapMarkersProvider': [
                    <#list conf.mapMarkersConfiguration as mapMarker>
                    {
                        'markerType': '${mapMarker.markerType}',
                        'settingLabel': '${mapMarker.settingLabel}',
                        'jsObject': '${mapMarker.jsObject}',
                        'zIndex': '${mapMarker.zIndex}',
                        'enabledByDefault': ${mapMarker.enabledByDefault?string},
                        'redrawOnViewChange':${mapMarker.redrawOnViewChange?string}
                        <#if mapMarker.legend??>
                        ,'legend': {
                            'id': '${mapMarker.legend.id}',
                            'label': '${mapMarker.legend.label}',
                            'exportable': ${mapMarker.legend.exportable?string},
                            'options': [
                                <#list mapMarker.legend.options as opt>
                                {
                                 'id': '${opt.id}',
                                 'label': '${opt.label}',
                                 'category': '${opt.category}',
                                 'color': '${opt.color}',
                                 'enabled': ${opt.enabled?string},
                                 'lowerThreshold': '${opt.lowerThreshold?c}',
                                 'upperThreshold': '${opt.upperThreshold?c}'
                                }<#if opt_has_next>,</#if>
                                </#list>
                            ]
                        }
                        </#if>
                        <#if mapMarker.config??>,'config' : '${mapMarker.config}'</#if>
                        <#if mapMarker.datasource??>,'datasource' : '${mapMarker.datasource}'</#if>
                    }
                    <#if mapMarker_has_next>,</#if>
                    </#list>
                ],
                'trainIcons': [
                    <#list conf.trainIcons as icon>
                    {
                        'name': '${icon.name}',
                        'image': '${icon.image}',
                        'zIndex': '${icon.zIndex}'
                    }
                    <#if icon_has_next>,</#if>
                    </#list>
                ],
                'trainLabel': {
                    'pointer': ${conf.trainLabel.pointer?string},
                    'xOffset': '${conf.trainLabel.xOffset}',
                    'yOffset': '${conf.trainLabel.yOffset}',
                    'height': '${conf.trainLabel.height}',
                    'width': '${conf.trainLabel.width}'
                    <#if conf.trainLabel.title??>
                    , 'title': '${conf.trainLabel.title}'
                    </#if>
                    <#if conf.trainLabel.description??>
                    , 'description': [
                        <#list conf.trainLabel.description as desc>
                        {
                            'label': '${desc.label}'
                            <#if desc.value??>
                            , 'value': '${desc.value}'
                            </#if>
                            <#if desc.format??>
                            , 'format': '${desc.format}'
                            </#if>
                            <#if desc.constant??>
                            , 'constant': '${desc.constant}'
                            </#if>                                                       
                        }
                        <#if desc_has_next>,</#if>    
                        </#list>
                    ]
                    </#if>                                        
                },
                <#if userConf.data??>'userConf' : ${userConf.data}</#if>
            };

            var detailConf = {
                'container': $('#mapInfoOverlay'),
                'defaultView': {
                    'latitude': '${conf.defaultDetailView.center.latitude?string("0.000000")}',
                    'longitude': '${conf.defaultDetailView.center.longitude?string("0.000000")}',
                    'zoom': '${conf.defaultDetailView.zoom}'
                },
                'maxHeight': $('#map').height(),
                'detailList': [
                    <#list conf.details as detail>
                        {
                            'label': '${detail.label}',
                            'name': '${detail.name}',
                            'ref': '${detail.referencedField}'
                            <#if detail.formatter??>
                            , 'format': '${detail.formatter}'
                            </#if>
                            <#if detail.fleets??>
                            , 'fleets': [
                                <#list detail.fleets as fleet>
                                    '${fleet}'
                                    <#if fleet_has_next>,</#if>
                                </#list>
                            ]
                            </#if>                           
                        }
                        <#if detail_has_next>,</#if>
                    </#list>
                ],
                'buttons': [
                    <#list conf.buttons as button>
                        {
                            'id': '${button.id}',
                            'label': '${button.displayName}'
                            <#if button.unitSummaryTab??>
                            , 'tab': '${button.unitSummaryTab}'
                            </#if>
                        }
                        <#if button_has_next>,</#if>
                    </#list>
                ]
            };

            var frameConf = {
                'timePicker': $('#timePicker'),
                'playPauseButton': $('#playPauseButton')
            };
            
	        if (!mapConf.userConf) {
	        	localStorage.setItem("mapFilters", "{}");
	        } else {
	        	localStorage.setItem("mapFilters", mapConf.userConf);
	        }

            $.when(mapReady).done(function() {
            	var frame = new nx.maps.Frame(frameConf, toolbarConf, mapConf, detailConf, function(frm) {
	            	frm.refresh();
	            });
	
	            //loads the stations/routes
	            window.setTimeout(function() {
	                nx.rest.map.licencedConf(function(response) {
	                    var namedViews = formatNamedViews(response.namedViews);
	                    var stations = formatStations(response.stations);
	                    var routes = formatRoutes(response.routes);
	                    var extraDropdowns = response.extraLocationDropdown;
	                    
	                    frame.setNamedViews(namedViews);
	                    frame.setStations(stations);
	                    frame.setExtraDropdown(extraDropdowns);
	                    frame.setRoutes(routes);
	                });
	            }, 500);
            });
        });
    })();
</script>
