/*
 * Copyright (c) Nexala Technologies 2013, All rights reserved.
 *
 * $Author$
 * $Date$
 * $Revision$
 * $Source$
 */
package com.nexala.spectrum.view.conf;

import java.util.Collections;
import java.util.List;

import com.google.common.collect.Lists;
import com.google.inject.Inject;
import com.nexala.spectrum.view.Plugin;
import com.nexala.spectrum.view.PluginFactory;

abstract public class AbstractUnitConfiguration implements UnitConfiguration{

    @Inject
    private PluginFactory factory;
    
    @Override
    public List<Plugin> getPlugins() {
        return Lists.newArrayList(factory.create("nx.unit.summaryHeader",
                Lists.newArrayList("js/nexala.unit.summaryheader.js"), 
                Lists.newArrayList("css/nexala.unit.summaryheader.css")
            )
        );
    }

    @Override
    public Plugin getParentPlugin() {
        Plugin plugin = new Plugin("", Collections.<String> emptyList(),
                Collections.<String> emptyList());

        return plugin;
    }

    @Override
    public String getScreenName() {
        return "Unit Summary";
    }
}
