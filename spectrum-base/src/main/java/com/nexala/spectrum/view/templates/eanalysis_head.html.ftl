<!-- event analysis includes -->

<link rel = "stylesheet" type="text/css" href='css/merge?resources=
    css/base.css,
    css/clean.css,
    css/jquery.jqplot.css,
    css/nexala.timepicker.css,
    css/nexala.eanalysis.css&view=eventanalysis'>

<#list cssIncludesList as cssIncludes>
       <link rel = 'stylesheet' type = 'text/css' href = 'css/merge?resources=
        <#list cssIncludes as cssInclude>${cssInclude},</#list>
        &view=eventanalysis
    '>
</#list>
    

<script type = "text/javascript" src = "js/merge?resources=
		js/i18n/datepicker-nl.js,
		js/i18n/datepicker-en-GB.js,
		js/nexala.timepicker.js,
		js/jstz.js,
		js/jquery.jqplot.js,
        js/jqplot.canvasTextRenderer.js,
        js/jqplot.canvasAxisTickRenderer.js,
        js/jqplot.categoryAxisRenderer.min.js,
        js/jqplot.barRenderer.js,
        js/nexala.eanalysis.js,
        js/nexala.eanalysis.data.js,
        js/nexala.eanalysis.filterdialog.js,
        js/nexala.eanalysis.reporttable.js,
        js/nexala.eanalysis.reportdetailtable.js,
        js/nexala.eanalysis.reportcontentchart.js,
        js/nexala.eanalysis.reportcontentmap.js,
        js/nexala.map.marker.js,
        js/map/nexala.map.marker.route.js,
        js/map/nexala.map.marker.station.js&view=eventanalysis"></script>

<#list jsIncludesList as jsIncludes>
	    <script type = 'text/javascript' src = 'js/merge?resources=
	        <#list jsIncludes as jsInclude>${jsInclude},</#list>
	        &view=eventanalysis
	    '></script>
</#list>


<script type = 'text/javascript'>

    $(document).ready(function() {
        <#list conf.plugins as plugin>
            <#if plugin.pluginClass?has_content>
                var clazz = eval(${plugin.pluginClass});
                var plugInst = new clazz();
            </#if>
        </#list> 
    });
</script>
