package com.nexala.spectrum.view.conf;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.common.collect.Lists;
import com.google.inject.Inject;
import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.configuration.ApplicationConfiguration;
import com.nexala.spectrum.configuration.ChannelConfiguration;
import com.nexala.spectrum.db.beans.ChannelConfig;
import com.nexala.spectrum.db.beans.ChannelConfigBuilder;
import com.nexala.spectrum.db.beans.ChannelGroup;
import com.nexala.spectrum.db.beans.ChannelGroupConfig;
import com.nexala.spectrum.db.dao.charting.ChannelType;
import com.nexala.spectrum.licensing.Licence;
import com.nexala.spectrum.rest.data.beans.StockDesc;
import com.nexala.spectrum.view.Plugin;
import com.nexala.spectrum.view.PluginFactory;
import com.nexala.spectrum.view.conf.unit.ChannelDataConfiguration;
import com.typesafe.config.Config;

public class DefaultChannelDataConfiguration extends AbstractDetailScreen implements ChannelDataConfiguration {

	private final ApplicationConfiguration appConfig;
	
    private final ChannelConfiguration channelConfig;
    
    private final Config channelDataConfig;
    
    private final Config screenConfig;
    
    private final PluginFactory pluginFactory;
	
	private final String CHANNEL_DATA_CONFIG_PATH = "r2m.channelData";
	
	private final String SCREEN_CONFIG_PATH = "r2m.channelData.screen";
	
	private List<Column> channelDataColumns;
	
	private Map<Integer, List<ChannelConfig>> nonGroupedChannels = new HashMap<Integer, List<ChannelConfig>>();
	
	private final ScreenType screenType;
	
	private final String templateName;
	
	private final String screenDisplayName;
	
	private final String requiredRole;
	
	private final String pluginClass;
	
	private final String source;
	
	private final String styleSheet;
	
	@Inject
	public DefaultChannelDataConfiguration(ApplicationConfiguration appConfig, 
			ChannelConfiguration channelConfig, PluginFactory factory) {
		this.appConfig = appConfig;
		this.channelConfig = channelConfig;
		this.channelDataConfig = appConfig.get().getConfig(CHANNEL_DATA_CONFIG_PATH);
		this.pluginFactory = factory;
		
		// Read screen configurations
		this.screenConfig = appConfig.get().getConfig(SCREEN_CONFIG_PATH);
		this.screenType = getScreenType(screenConfig.getString("type"));
		this.templateName = screenConfig.getString("templateName");
		this.screenDisplayName = screenConfig.getString("displayName");
		this.requiredRole = screenConfig.getString("requireRole");
		this.pluginClass = screenConfig.getString("parentPlugin.pluginClass");
		this.source = screenConfig.getString("parentPlugin.source");
		this.styleSheet = screenConfig.getString("parentPlugin.stylesheet");
		
        if(channelDataConfig.hasPath("timestampGroupName")) {
            String groupName = channelDataConfig.getString("timestampGroupName");
            ChannelGroupConfig cgc = channelConfig.getGroup(groupName);
            if (cgc != null) {
                ChannelConfigBuilder timestampBuilder = new ChannelConfigBuilder();
                timestampBuilder.setName(Spectrum.TIMESTAMP).setAlwaysDisplayed(true).setDescription(Spectrum.TIMESTAMP)
                        .setType(ChannelType.TIMESTAMP).setFormatPattern("DD/MM/YYYY HH:mm:ss.SSS");

                ChannelConfig timestamp = new ChannelConfig(timestampBuilder);

                nonGroupedChannels.put(cgc.getId(), Arrays.asList(timestamp));
            }
        }
	}
	
	private ScreenType getScreenType(String type) {
		if (type.equals("DIALOG")) {
			return ScreenType.DIALOG;
		} else {
			return ScreenType.TAB;
		}
	}
	
	@Override
	public List<ChannelGroup> getChannelGroups() {
		return channelConfig.getVisibleGroupList();
	}
	
	@Override
	public List<ChannelConfig> getNonGroupedChannelNames(ChannelGroupConfig channelGroupConfig) {
        List<ChannelConfig> configList = nonGroupedChannels.get(channelGroupConfig.getId());
        return configList != null ? configList : new ArrayList<ChannelConfig>();
	}

	@Override
	public List<Column> getColumns(List<StockDesc> stockList, Licence licence) {
		// Column Configuration
		List<? extends Config> columns = channelDataConfig.getConfigList("columns.nonStockColumns");
		this.channelDataColumns = new ArrayList<>();
		
		// Non stock columns.
		for (Config columnConfig: columns) {
			channelDataColumns.add(appConfig.columnBuilder(columnConfig).build());
		}

		// Get stock column configuration.
		Config stockColumnConfig = channelDataConfig.getConfig("columns.stockColumn");
		for (StockDesc stock: stockList) {
			ColumnBuilder columnBuilder = appConfig.columnBuilder(stockColumnConfig);
			// set extra properties that do not come from configuration file
			columnBuilder
				.displayName(stock.getDescription())
				.name(Long.toString(stock.getId()));
			channelDataColumns.add(columnBuilder.build());
		}
		
		return channelDataColumns;
	}

	@Override
	public ScreenType getScreenType() {
		return screenType;
	}

    @Override
    public String getDisplayName() {
        return screenDisplayName;
    }

    @Override
    public String getRequiredRole() {
        return requiredRole;
    }

    @Override
    public String getTemplateName() {
        return templateName;
    }

    @Override
    public Plugin getParentPlugin() {
        return pluginFactory.create(pluginClass, Lists.newArrayList(source),
                Lists.newArrayList(styleSheet));
    }
}
