/*
 * Copyright (c) Nexala Technologies 2013, All rights reserved.
 */
package com.nexala.spectrum.view.conf;

import java.util.List;

import com.google.common.collect.Lists;
import com.nexala.spectrum.licensing.ScreenRequiresFeature;
import com.nexala.spectrum.view.Plugin;

public abstract class AbstractDetailScreen implements DetailScreen, ScreenRequiresFeature {
    
    @Override
    public Plugin getParentPlugin() {
        return null;
    }
    
    @Override
    public List<Plugin> getPlugins() {
        return Lists.newArrayList();
    }

    @Override
    public DataNavigationType getDataNavigationType() {
        return DataNavigationType.TIME;
    }
}
