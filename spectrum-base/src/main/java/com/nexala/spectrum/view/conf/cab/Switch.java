package com.nexala.spectrum.view.conf.cab;

/**
 * Switch component which permit to switch between 2 values.
 * 
 * @author brice
 *
 */
public class Switch extends Widget {
    
    private SwitchSettings settings;
    
    
    public Switch(String domId, int x, int y, int h, int w,
            SwitchSettings settings, String... channels) {
        super(domId, x, y, h, w, channels);
        this.settings = settings;
    }
    
    @Override
    public Settings getSettingsObject() {
        return settings;
    }
    
    @Override
    public String getWidgetClassName() {
        return "nx.cab.Switch";
    }

}
