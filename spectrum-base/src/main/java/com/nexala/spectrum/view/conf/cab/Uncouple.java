/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.view.conf.cab;

public class Uncouple extends Widget {
    public Uncouple(String domId, int x, int y, int h, int w, String... channels) {
        super(domId, x, y, h, w, channels);
    }

    public String getWidgetClassName() {
        return "nx.cab.Uncouple";
    }
}