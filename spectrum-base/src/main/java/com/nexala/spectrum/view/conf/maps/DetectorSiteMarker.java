package com.nexala.spectrum.view.conf.maps;

import java.util.List;

/**
 * Marker for Detectors.
 *
 */
public class DetectorSiteMarker {
    
    private Integer id;
    private String siteName;
    private String tracksBetween;
    private Double latitude;
    private Double longitude;
    private List<Detector> detectors;
    
    /**
     * Default constructor
     */
    public DetectorSiteMarker(Integer id, String siteName, String tracksBetween, Double latitude, Double longitude, List<Detector> detectors) {
        setId(id);
        setSiteName(siteName);
        setTracksBetween(tracksBetween);
        setLatitude(latitude);
        setLongitude(longitude);
        setDetectors(detectors);
    }
    
    public void setId(Integer id){
        this.id = id;
    }  
    
    public void setSiteName(String siteName){
        this.siteName = siteName;
    }  
    
    public void setTracksBetween(String tracksBetween){
        this.tracksBetween = tracksBetween;
    }  
    
    public void setLatitude(Double latitude){
        this.latitude = latitude;
    }  
    
    public void setLongitude(Double longitude){
        this.longitude = longitude;
    }  
    
    public void setDetectors(List<Detector> detectors){
        this.detectors = detectors;
    }  
    
    public Integer getId(){
        return id;
    }  
    
    public String getSiteName(){
        return siteName;
    }  
    
    public String getTracksBetween(){
        return tracksBetween;
    }  
    
    public Double getLatitude(){
        return latitude;
    }  
    
    public Double getLongitude(){
        return longitude;
    }   
    
    public List<Detector> getDetectors(){
        return detectors;
    }  
    
}
