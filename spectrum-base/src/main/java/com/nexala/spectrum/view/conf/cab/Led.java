/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.view.conf.cab;

public class Led extends Widget {
    private LedSettings settings;
    
    public Led(String domId, int x, int y, int h, int w,
            LedSettings settings, String... channels) {
        super(domId, x, y, h, w, channels);
        this.settings = settings;
    }
    
    @Override // XXX rename to getStyleName
    public String getClassName() {
        return "led";
    }
    
    @Override
    public String getWidgetClassName() {
        return "nx.cab.Led";
    }
    
    @Override
    public Settings getSettingsObject() {
        return settings;
    }
}