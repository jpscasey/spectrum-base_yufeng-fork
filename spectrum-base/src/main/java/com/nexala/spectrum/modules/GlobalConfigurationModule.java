/*
 * Copyright (c) Nexala Technologies 2013, All rights reserved.
 *
 * $Author$
 * $Date$
 * $Revision$
 * $Source$
 */
package com.nexala.spectrum.modules;

import com.google.inject.PrivateModule;
import com.google.inject.Singleton;
import com.google.inject.assistedinject.FactoryModuleBuilder;
import com.google.inject.name.Names;
import com.nexala.spectrum.charting.DVChartFactory;
import com.nexala.spectrum.configuration.ApplicationConfiguration;
import com.nexala.spectrum.configuration.SystemConfiguration;
import com.nexala.spectrum.db.dao.ConnectionProvider;
import com.nexala.spectrum.db.dao.JndiConnectionProvider;
import com.nexala.spectrum.eventanalysis.conf.EAnalysisConfiguration;
import com.nexala.spectrum.eventanalysis.conf.EAnalysisConfigurationImpl;
import com.nexala.spectrum.licensing.LicenceValidatorProvider;
import com.nexala.spectrum.rest.data.CommonChartProvider;
import com.nexala.spectrum.rest.data.CommonEventProvider;
import com.nexala.spectrum.rest.data.ConfigurationProvider;
import com.nexala.spectrum.rest.data.DefaultCommonChartProvider;
import com.nexala.spectrum.rest.data.DefaultCommonEventProvider;
import com.nexala.spectrum.rest.data.DefaultConfigurationProvider;
import com.nexala.spectrum.rest.data.DefaultEventSearchProvider;
import com.nexala.spectrum.rest.data.DefaultGlobalFleetProvider;
import com.nexala.spectrum.rest.data.DefaultRecoveryProvider;
import com.nexala.spectrum.rest.data.DefaultRouteMapProvider;
import com.nexala.spectrum.rest.data.DefaultUserConfigurationProvider;
import com.nexala.spectrum.rest.data.EventSearchProvider;
import com.nexala.spectrum.rest.data.ForgotPasswordProvider;
import com.nexala.spectrum.rest.data.GlobalFleetProvider;
import com.nexala.spectrum.rest.data.RecoveryProvider;
import com.nexala.spectrum.rest.data.RouteMapProvider;
import com.nexala.spectrum.rest.data.UserConfigurationProvider;
import com.nexala.spectrum.view.conf.AppConfiguration;
import com.nexala.spectrum.view.conf.ChannelDefinitionConfiguration;
import com.nexala.spectrum.view.conf.CommonFleetConfiguration;
import com.nexala.spectrum.view.conf.CurrentEventsConfiguration;
import com.nexala.spectrum.view.conf.DefaultAppConfiguration;
import com.nexala.spectrum.view.conf.DefaultChannelDefinitionConfiguration;
import com.nexala.spectrum.view.conf.DefaultCommonFleetConfiguration;
import com.nexala.spectrum.view.conf.DefaultCurrentEventsConfiguration;
import com.nexala.spectrum.view.conf.DefaultDownloadConfiguration;
import com.nexala.spectrum.view.conf.DefaultDrillDownConfiguration;
import com.nexala.spectrum.view.conf.DefaultEventConfiguration;
import com.nexala.spectrum.view.conf.DefaultEventDetailConfiguration;
import com.nexala.spectrum.view.conf.DefaultEventGroupConfiguration;
import com.nexala.spectrum.view.conf.DefaultMapConfiguration;
import com.nexala.spectrum.view.conf.DefaultSchematicConfiguration;
import com.nexala.spectrum.view.conf.DownloadConfiguration;
import com.nexala.spectrum.view.conf.DrillDownConfiguration;
import com.nexala.spectrum.view.conf.EventConfiguration;
import com.nexala.spectrum.view.conf.EventDetailConfiguration;
import com.nexala.spectrum.view.conf.EventGroupConfiguration;
import com.nexala.spectrum.view.conf.MapConfiguration;
import com.nexala.spectrum.view.conf.SchematicConfiguration;
import com.nexala.spectrum.view.conf.maps.DefaultMapMarkerProvider;
import com.nexala.spectrum.view.conf.maps.MapMarkerProvider;
import com.nexala.spectrum.view.conf.unit.DataExportConfiguration;
import com.nexala.spectrum.view.conf.unit.DataPlotConfiguration;
import com.nexala.spectrum.view.conf.unit.DefaultDataExportConfiguration;
import com.nexala.spectrum.view.conf.unit.DefaultDataPlotConfiguration;
import com.nexala.spectrum.view.conf.unit.DefaultRecoveryConfiguration;
import com.nexala.spectrum.view.conf.unit.DefaultUnitSearchConfiguration;
import com.nexala.spectrum.view.conf.unit.RecoveryConfiguration;
import com.nexala.spectrum.view.conf.unit.UnitSearchConfiguration;
import com.nexala.spectrum.view.templates.TemplateConfiguration;
import com.trimble.rail.security.core.UserManagementImpl;
import com.trimble.rail.security.core.api.UserManagement;

/**
 * Abstract class for the global configuration.
 * Provide the default implementations which can be override in projects
 * @author brice
 *
 */
public abstract class GlobalConfigurationModule extends PrivateModule {

    // Configurations
    protected Class<? extends AppConfiguration> supplyAppConfiguration() {
        return DefaultAppConfiguration.class;
    }

    protected Class<? extends ChannelDefinitionConfiguration> supplyChannelDefinitionConfiguration() {
        return DefaultChannelDefinitionConfiguration.class;
    }
    protected Class<? extends DataExportConfiguration> supplyDataExportConfiguration() {
        return DefaultDataExportConfiguration.class;
    }

    protected Class<? extends SchematicConfiguration> supplySchematicConfiguration() {
        return DefaultSchematicConfiguration.class;
    }
    
    protected Class<? extends DataPlotConfiguration> supplyDataPlotConfiguration() {
        return DefaultDataPlotConfiguration.class;
    }
    
    protected Class<? extends RecoveryConfiguration> supplyRecoveryConfiguration() {
        return DefaultRecoveryConfiguration.class;
    }

    protected Class<? extends EventConfiguration> supplyEventConfiguration() {
        return DefaultEventConfiguration.class;
    }

    protected Class<? extends EventDetailConfiguration> supplyEventDetailConfiguration() {
        return DefaultEventDetailConfiguration.class;
    }

    protected Class<? extends EventGroupConfiguration> supplyEventGroupConfiguration() {
        return DefaultEventGroupConfiguration.class;
    }
    
    protected Class<? extends EAnalysisConfiguration> supplyEAnalysisConfiguration() {
        return EAnalysisConfigurationImpl.class;
    }

    protected Class<? extends UnitSearchConfiguration> supplyUnitSearchConfiguration() {
        return DefaultUnitSearchConfiguration.class;
    }

    // Providers
    protected Class<? extends EventSearchProvider> supplyEventSearchProvider() {
        return DefaultEventSearchProvider.class;
    }

    protected Class<? extends ConnectionProvider> supplyConnectionProvider() {
        return JndiConnectionProvider.class;
    }

    protected Class<? extends RouteMapProvider> supplyRouteMapProvider() {
        return DefaultRouteMapProvider.class;
    }

    protected Class<? extends CommonEventProvider> supplyCommonEventProvider() {
        return DefaultCommonEventProvider.class;
    }

    protected Class<? extends GlobalFleetProvider> supplyGlobalFleetProvider() {
        return DefaultGlobalFleetProvider.class;
    }

    protected Class<? extends CommonChartProvider> supplyCommonChartProvider() {
        return DefaultCommonChartProvider.class;
    }

    private Class<? extends ConfigurationProvider> supplyConfigurationProvider() {
        return DefaultConfigurationProvider.class;
    }

    private Class<? extends ForgotPasswordProvider> supplyForgotPasswordProvider() {
        return null;
    }

    private Class<? extends ApplicationConfiguration> supplyApplicationConfiguration() {
        return ApplicationConfiguration.class;
    }
    
    protected Class<? extends DownloadConfiguration> supplyDownloadConfiguration() {
        return DefaultDownloadConfiguration.class;
    }
    
    private Class<? extends CommonFleetConfiguration> supplyCommonFleetConfiguration() {
        return DefaultCommonFleetConfiguration.class;
    }

    private Class<? extends DrillDownConfiguration> supplyDrillDownConfiguration() {
        return DefaultDrillDownConfiguration.class;
    }

    private Class<? extends CurrentEventsConfiguration> supplyLiveAndRecentEventsConfiguration() {
        return DefaultCurrentEventsConfiguration.class;
    }

    protected Class<? extends MapConfiguration> supplyMapConfiguration() {
        return DefaultMapConfiguration.class;
    }
    
    protected Class<? extends UserManagement> supplyUserManagement() {
        return UserManagementImpl.class;
    }

    protected Class<? extends MapMarkerProvider> supplyMapMarkerProvider() {
        return DefaultMapMarkerProvider.class;
    }
    
    protected Class<? extends UserConfigurationProvider> supplyUserConfigurationProvider() {
        return DefaultUserConfigurationProvider.class;
    }
    
    protected Class<? extends RecoveryProvider> supplyRecoveryProvider() {
        return DefaultRecoveryProvider.class;
    }
    
    private Class<? extends SystemConfiguration> supplySystemConfiguration() {
    	return SystemConfiguration.class;
    }
    
    abstract protected Class<? extends LicenceValidatorProvider> supplyLicenceValidatorProvider();
    abstract protected Class<? extends TemplateConfiguration> supplyTemplateConfiguration();
    abstract protected String getGlobalDatasource();

    @Override
    protected void configure() {

        bind(String.class).annotatedWith(Names.named("dataSource")).toInstance(getGlobalDatasource());
        bind(ConnectionProvider.class).to(supplyConnectionProvider()).in(Singleton.class);
        install(new FactoryModuleBuilder().build(DVChartFactory.class));

        bindAndExpose(AppConfiguration.class, supplyAppConfiguration());
        bindAndExpose(CommonFleetConfiguration.class, supplyCommonFleetConfiguration());
        bindAndExpose(ChannelDefinitionConfiguration.class, supplyChannelDefinitionConfiguration());
        bindAndExpose(DataPlotConfiguration.class, supplyDataPlotConfiguration());
        bindAndExpose(DownloadConfiguration.class, supplyDownloadConfiguration());
        bindAndExpose(RecoveryConfiguration.class, supplyRecoveryConfiguration());
        bindAndExpose(EventConfiguration.class, supplyEventConfiguration());
        bindAndExpose(DataExportConfiguration.class, supplyDataExportConfiguration());
        bindAndExpose(UnitSearchConfiguration.class, supplyUnitSearchConfiguration());
        bindAndExpose(EventDetailConfiguration.class, supplyEventDetailConfiguration());
        bindAndExpose(EventGroupConfiguration.class, supplyEventGroupConfiguration());
        bindAndExpose(MapConfiguration.class, supplyMapConfiguration());
        bindAndExpose(EAnalysisConfiguration.class, supplyEAnalysisConfiguration());
        bindAndExpose(DrillDownConfiguration.class, supplyDrillDownConfiguration());
        bindAndExpose(CurrentEventsConfiguration.class, supplyLiveAndRecentEventsConfiguration());
        bindAndExpose(SchematicConfiguration.class, supplySchematicConfiguration());
        
        bindAndExpose(RouteMapProvider.class, supplyRouteMapProvider());
        bindAndExpose(EventSearchProvider.class, supplyEventSearchProvider());
        bindAndExpose(LicenceValidatorProvider.class, supplyLicenceValidatorProvider());
        bindAndExpose(CommonEventProvider.class, supplyCommonEventProvider());
        bindAndExpose(CommonChartProvider.class, supplyCommonChartProvider());
        bindAndExpose(ConfigurationProvider.class, supplyConfigurationProvider());
        bindAndExpose(MapMarkerProvider.class, supplyMapMarkerProvider());
        bindAndExpose(UserConfigurationProvider.class, supplyUserConfigurationProvider());

        bindAndExpose(TemplateConfiguration.class, supplyTemplateConfiguration());

        bindAndExpose(GlobalFleetProvider.class, supplyGlobalFleetProvider());

        bindAndExpose(ForgotPasswordProvider.class, supplyForgotPasswordProvider());
        bindAndExpose(ApplicationConfiguration.class, supplyApplicationConfiguration());
        bindAndExpose(UserManagement.class, supplyUserManagement());
        bindAndExpose(SystemConfiguration.class, supplySystemConfiguration());
    }

    protected <T> void bindAndExpose(Class<T> iface, Class<? extends T> concrete) {
        if (iface != null && concrete != null) {
            if (!iface.equals(concrete)) {
                bind(iface).to(concrete);
            }

            bind(concrete);
            expose(iface);
        }
    }

}
