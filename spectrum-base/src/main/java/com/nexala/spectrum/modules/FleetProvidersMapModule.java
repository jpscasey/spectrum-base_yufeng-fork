/*
 * Copyright (c) Nexala Technologies 2013, All rights reserved.
 *
 * $Author$
 * $Date$
 * $Revision$
 * $Source$
 */
package com.nexala.spectrum.modules;

import java.util.Map;

import com.google.inject.AbstractModule;
import com.google.inject.Key;
import com.google.inject.TypeLiteral;
import com.google.inject.multibindings.MapBinder;
import com.nexala.spectrum.analysis.bean.GenericEvent;
import com.nexala.spectrum.analysis.data.AnalysisDataProvider;
import com.nexala.spectrum.eventanalysis.rest.EAnalysisProvider;
import com.nexala.spectrum.rest.data.AdhesionProvider;
import com.nexala.spectrum.rest.data.CabDataProvider;
import com.nexala.spectrum.rest.data.ChannelDataProvider;
import com.nexala.spectrum.rest.data.ChannelDefinitionProvider;
import com.nexala.spectrum.rest.data.ChartDataProvider;
import com.nexala.spectrum.rest.data.ChartVehicleTypeProvider;
import com.nexala.spectrum.rest.data.DetectorProvider;
import com.nexala.spectrum.rest.data.DownloadProvider;
import com.nexala.spectrum.rest.data.EventProvider;
import com.nexala.spectrum.rest.data.FaultMetaProvider;
import com.nexala.spectrum.rest.data.FleetProvider;
import com.nexala.spectrum.rest.data.GroupProvider;
import com.nexala.spectrum.rest.data.LocationProvider;
import com.nexala.spectrum.rest.data.MapProvider;
import com.nexala.spectrum.rest.data.RecoveryProvider;
import com.nexala.spectrum.rest.data.StaticChartProvider;
import com.nexala.spectrum.rest.data.StockProvider;
import com.nexala.spectrum.rest.data.SystemConfigProvider;
import com.nexala.spectrum.rest.data.UnitDetailProvider;
import com.nexala.spectrum.rest.data.UnitProvider;
import com.nexala.spectrum.rest.service.CabConfigurationProvider;
import com.nexala.spectrum.view.conf.UnitConfiguration;
import com.nexala.spectrum.view.conf.unit.UnitDetailConfiguration;
import com.nexala.spectrum.web.service.RuleTestDataProvider;

public abstract class FleetProvidersMapModule extends AbstractModule {

    @Override
    protected void configure() {
        bindMap(new TypeLiteral<UnitConfiguration>() {}, supplyUnitConfigurations());
        bindMap(new TypeLiteral<UnitDetailConfiguration>() {}, supplyUnitDetailConfigurations());
        bindMap(new TypeLiteral<CabConfigurationProvider>() {}, supplyCabConfigurationProviders());
        bindMap(new TypeLiteral<CabDataProvider>() {}, supplyCabDataProviders());
        bindMap(new TypeLiteral<ChannelDataProvider>() {}, supplyChannelDataProviders());
        bindMap(new TypeLiteral<ChannelDefinitionProvider>() {}, supplyChannelDefinitionProviders());
        bindMap(new TypeLiteral<ChartDataProvider>() {}, supplyChartDataProviders());
        bindMap(new TypeLiteral<StaticChartProvider>() {}, supplyStaticChartProviders());
        bindMap(new TypeLiteral<DownloadProvider>() {}, supplyDownloadProviders());
        bindMap(new TypeLiteral<EventProvider>() {}, supplyEventProviders());
        bindMap(new TypeLiteral<FleetProvider>() {}, supplyFleetProviders());
        bindMap(new TypeLiteral<EAnalysisProvider>() {}, supplyEAnalysisProviders());
        bindMap(new TypeLiteral<GroupProvider>() {}, supplyGroupProviders());
        bindMap(new TypeLiteral<LocationProvider>() {}, supplyLocationProviders());
        bindMap(new TypeLiteral<MapProvider>() {}, supplyMapProviders());
        bindMap(new TypeLiteral<RuleTestDataProvider<GenericEvent>>() {}, supplyRuleTestDataProviders());
        bindMap(new TypeLiteral<StockProvider>() {}, supplyStockProviders());
        bindMap(new TypeLiteral<UnitDetailProvider>() {}, supplyUnitDetailProviders());
        bindMap(new TypeLiteral<RecoveryProvider>() {}, supplyRecoveryProviders());
        bindMap(new TypeLiteral<UnitProvider>() {}, supplyUnitProviders());
        bindMap(new TypeLiteral<AnalysisDataProvider<GenericEvent>>() {}, supplyAnalysisDataProviders());
        bindMap(new TypeLiteral<AdhesionProvider>() {}, supplyAdhesionProviders());     
        bindMap(new TypeLiteral<ChartVehicleTypeProvider>() {}, supplyChartVehicleTypeProviders());
        bindMap(new TypeLiteral<FaultMetaProvider>() {}, supplyFaultMetaProviders());
        bindMap(new TypeLiteral<DetectorProvider>() {}, supplyDetectorProviders());
        bindMap(new TypeLiteral<SystemConfigProvider>() {}, supplySystemConfigProviders());
    }

    // Fleet dependent configurations
    abstract protected Map<String, Key<? extends UnitConfiguration>> supplyUnitConfigurations();
    abstract protected Map<String, Key<? extends UnitDetailConfiguration>> supplyUnitDetailConfigurations();

    // Fleet dependent providers
    abstract protected Map<String, Key<? extends CabConfigurationProvider>> supplyCabConfigurationProviders();
    abstract protected Map<String, Key<? extends CabDataProvider>> supplyCabDataProviders();
    abstract protected Map<String, Key<? extends ChannelDataProvider>> supplyChannelDataProviders();
    abstract protected Map<String, Key<? extends ChannelDefinitionProvider>> supplyChannelDefinitionProviders();
    abstract protected Map<String, Key<? extends ChartDataProvider>> supplyChartDataProviders();
    abstract protected Map<String, Key<? extends StaticChartProvider>> supplyStaticChartProviders();
    abstract protected Map<String, Key<? extends DownloadProvider>> supplyDownloadProviders();
    abstract protected Map<String, Key<? extends EventProvider>> supplyEventProviders();
    abstract protected Map<String, Key<? extends EAnalysisProvider>> supplyEAnalysisProviders();
    abstract protected Map<String, Key<? extends FleetProvider>> supplyFleetProviders();
    abstract protected Map<String, Key<? extends GroupProvider>> supplyGroupProviders();
    abstract protected Map<String, Key<? extends LocationProvider>> supplyLocationProviders();
    abstract protected Map<String, Key<? extends MapProvider>> supplyMapProviders();
    abstract protected Map<String, Key<? extends RuleTestDataProvider<GenericEvent>>> supplyRuleTestDataProviders();
    abstract protected Map<String, Key<? extends StockProvider>> supplyStockProviders();
    abstract protected Map<String, Key<? extends UnitDetailProvider>> supplyUnitDetailProviders();
    abstract protected Map<String, Key<? extends RecoveryProvider>> supplyRecoveryProviders();
    abstract protected Map<String, Key<? extends UnitProvider>> supplyUnitProviders();
    abstract protected Map<String, Key<? extends AdhesionProvider>> supplyAdhesionProviders();
    abstract protected Map<String, Key<? extends AnalysisDataProvider<GenericEvent>>> supplyAnalysisDataProviders();
    abstract protected Map<String, Key<? extends ChartVehicleTypeProvider>> supplyChartVehicleTypeProviders();
    abstract protected Map<String, Key<? extends FaultMetaProvider>> supplyFaultMetaProviders();
    abstract protected Map<String, Key<? extends DetectorProvider>> supplyDetectorProviders();
    abstract protected Map<String, Key<? extends SystemConfigProvider>> supplySystemConfigProviders();

    protected <T> void bindMap(TypeLiteral<T> providerInterface, Map<String, Key<? extends T>> providersMap) {
        MapBinder<String, T> mapBinder =
                MapBinder.newMapBinder(binder(), new TypeLiteral<String>() {}, providerInterface);

        if (providersMap == null) {
            return;
        }

        for (Map.Entry<String, Key<? extends T>> provider : providersMap.entrySet()) {
            mapBinder.addBinding(provider.getKey()).to(provider.getValue());
        }
    }
    
}
