package com.nexala.spectrum.modules;

import java.lang.annotation.Annotation;

import com.google.inject.Key;
import com.google.inject.PrivateModule;
import com.google.inject.TypeLiteral;
import com.google.inject.assistedinject.FactoryModuleBuilder;
import com.google.inject.name.Names;
import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.db.EventMapperFactory;

/**
 * An abstract fleet module.
 * @author BBaudry
 *
 */
public abstract class AbstractFleetModule extends PrivateModule {
    
    private String fleetId;
    
    private Class<? extends Annotation> fleetAnnotation;
    
    /**
     * Constructor
     * @param fleetId
     * @param fleetAnnotation
     */
    public AbstractFleetModule(String fleetId, Class<? extends Annotation> fleetAnnotation) {
        this.fleetId = fleetId;
        this.fleetAnnotation = fleetAnnotation;
        
    }
    
    @Override
    protected void configure() {
        bind(String.class).annotatedWith(Names.named(Spectrum.FLEET_ID)).toInstance(fleetId);
        install(new FactoryModuleBuilder().build(EventMapperFactory.class));
    }
    
    /**
     * Bind the interface to the concrete class and the expose it.
     * It expose the concrete class and the interface annotated with the fleet annotation.
     * @param iface the interface
     * @param concrete the concrete class to bind to this interface
     */
    protected <T> void bindAndExpose(Class<T> iface, Class<? extends T> concrete) {
        bindAndExpose(TypeLiteral.get(iface), concrete);
    }
    
    /**
     * Bind the interface to the concrete class and the expose it.
     * It expose the concrete class and the interface annotated with the fleet annotation.
     * @param iface the interface
     * @param concrete the concrete class to bind to this interface
     */
    protected <T> void bindAndExpose(TypeLiteral<T> iface, Class<? extends T> concrete) {
        Key<T> key = Key.get(iface, fleetAnnotation);
        
        bind(Key.get(iface)).to(concrete);
        bind(key).to(concrete);
        bind(concrete);
        
        expose(concrete);
        expose(key);
    }
    
}
