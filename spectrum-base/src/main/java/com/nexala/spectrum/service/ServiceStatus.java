package com.nexala.spectrum.service;

/**
 * The different statuses a {@link Service service} can have.
 * 
 * @author Severinas Monkevicius
 */
public enum ServiceStatus {
    /**
     * The service has not been started yet, or has been stopped.
     */
    IDLE,

    /**
     * The service is starting.
     */
    STARTING,

    /**
     * The service is running.
     */
    RUNNING,

    /**
     * The service is stopping.
     */
    STOPPING,

    /**
     * The service is not running and has either failed to start or failed while running.
     */
    FAILED;
}
