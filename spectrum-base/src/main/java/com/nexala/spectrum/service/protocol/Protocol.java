package com.nexala.spectrum.service.protocol;

/**
 * An interface for protocols.
 * 
 * @author Severinas Monkevicius
 */
public interface Protocol {
    /**
     * The main method for a protocol. It should cover a single unbroken communication session between server and
     * client.
     */
    void communicate();

    /**
     * The {@code close()} method is used to stop any ongoing communication. After this method is invoked, the protocol
     * should return from its {@link #communicate()} method, although this can happen after the {@code close()} method
     * completes.
     */
    void close();
}
