package com.nexala.spectrum.service.bean;

/**
 * Bean containing a chart image.
 * @author brice
 *
 */
public class StaticChartBean {
	
	private Integer chartId = null;
	
	private byte[] chartImage = null;

	/**
	 * Getter for the chartId.
	 * @return the chartId
	 */
	public Integer getChartId() {
		return chartId;
	}

	/**
	 * Setter for the chartId.
	 * @param chartId the chartId to set
	 */
	public void setChartId(Integer chartId) {
		this.chartId = chartId;
	}

	/**
	 * Getter for the chartImage.
	 * @return the chartImage
	 */
	public byte[] getChartImage() {
		return chartImage;
	}

	/**
	 * Setter for the chartImage.
	 * @param chartImage the chartImage to set
	 */
	public void setChartImage(byte[] chartImage) {
		this.chartImage = chartImage;
	}

}
