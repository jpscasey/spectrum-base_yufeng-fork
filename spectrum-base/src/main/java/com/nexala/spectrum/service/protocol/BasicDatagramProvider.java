package com.nexala.spectrum.service.protocol;

import java.io.IOException;
import java.net.DatagramSocket;

public class BasicDatagramProvider implements DatagramProvider {
    @Override
    public DatagramSocket create(HasUdpSocketConfiguration conf) throws IOException {
        return new DatagramSocket(conf.getPort());
    }
}
