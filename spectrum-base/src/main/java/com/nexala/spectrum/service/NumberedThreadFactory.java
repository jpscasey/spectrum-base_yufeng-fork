package com.nexala.spectrum.service;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

public class NumberedThreadFactory implements ThreadFactory {
    private final String name;
    private final AtomicInteger n;

    public NumberedThreadFactory(String name) {
        this.name = name;
        this.n = new AtomicInteger();
    }

    public NumberedThreadFactory() {
        this(null);
    }

    @Override
    public Thread newThread(Runnable r) {
        String tn = name == null ? Thread.currentThread().getName() : name;
        return new Thread(r, tn + "-" + n.getAndIncrement());
    }
}
