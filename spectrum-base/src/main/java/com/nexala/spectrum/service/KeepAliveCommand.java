package com.nexala.spectrum.service;

import java.util.Collection;

import org.apache.log4j.Logger;

import com.nexala.spectrum.service.protocol.ScheduledCommand;

public class KeepAliveCommand extends ScheduledCommand {
    private final Logger LOG = Logger.getLogger(KeepAliveCommand.class);

    private final Collection<? extends Service<? extends SingleThreadServiceConfig>> services;

    public KeepAliveCommand(Collection<? extends Service<? extends SingleThreadServiceConfig>> services) {
        this.services = services;
    }

    @Override
    public void execute() {
        for (Service<? extends SingleThreadServiceConfig> service : services) {
            if (service.config().isKeepAlive() && service.serviceStatus() == ServiceStatus.FAILED) {
                LOG.warn(service.name() + " - service failed, restarting");
                service.start();
            }
        }
    }
}
