package com.nexala.spectrum.service.protocol;

import java.io.IOException;
import java.net.DatagramSocket;

import com.google.inject.ImplementedBy;

@ImplementedBy(BasicDatagramProvider.class)
public interface DatagramProvider {
    public DatagramSocket create(HasUdpSocketConfiguration conf) throws IOException;
}
