package com.nexala.spectrum.service;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

import org.apache.log4j.Logger;

/**
 * A named service that executes a single command in a single thread. If the command completes normally (the status of
 * the service after it completes is {@link ServiceStatus#RUNNING}), it is set to {@link ServiceStatus#IDLE}.
 * 
 * @author Severinas Monkevicius
 */
public abstract class SingleThreadService<C extends ServiceConfig> extends AbstractService<C> {
    private static final Logger LOG = Logger.getLogger(SingleThreadService.class);
    public static final int TIMEOUT = 10000;

    /**
     * The thread factory that creates the service's thread of execution.
     */
    private final ThreadFactory threadFactory;

    /**
     * The executor that executes the service's command.
     */
    private ExecutorService executor;

    /**
     * Creates a new named service.
     * 
     * @param name the name.
     * @param config the service configuration.
     */
    public SingleThreadService(String name, C config) {
        super(name, config);

        this.threadFactory = new NamedThreadFactory(name);
    }

    /**
     * Calls {@link #onStart()} and starts executing the command returned by {@link #getCommand()}.
     */
    @Override
    protected final void doStart() throws Exception {
        beforeStart();
        onStart();
        executor = Executors.newSingleThreadExecutor(threadFactory);
        executor.execute(new CommandWrapper(getCommand()));
    }

    protected void beforeStart() {
    }

    protected abstract void onStart() throws Exception;

    /**
     * Returns the command that this service will execute. Called each time the service is started.
     * 
     * @return the command that this service will execute.
     */
    protected abstract SingleThreadCommand getCommand();

    /**
     * Calls {@link #onStop()} and terminates the command being executed.
     */
    @Override
    protected final void doStop() throws Exception {
        executor.shutdown();

        try {
            onStop();
        } finally {
            // This ensures the executor is always terminated
            ServiceUtilities.terminateExecutor(executor, TIMEOUT);
        }
    }

    protected abstract void onStop() throws Exception;

    /**
     * A wrapper for the command that this service will execute. It implements the required {@link Runnable} interface
     * and ensures that the service cleans up after itself once the command completes.
     * 
     * @author Severinas Monkevicius
     */
    private class CommandWrapper implements Runnable {
        private final SingleThreadCommand command;

        public CommandWrapper(SingleThreadCommand command) {
            this.command = command;
        }

        @Override
        public void run() {
            if (!isStartedAndRunning()) {
                return;
            }

            boolean failed = false;

            try {
                Thread.sleep(config().getInitialDelay());
                command.run();
            } catch (InterruptedException e) {
                // NOOP
            } catch (Exception e) {
                LOG.warn(name() + " - failed to run command", e);
                failed = true;
            }

            if (checkStatus(ServiceStatus.RUNNING, ServiceStatus.FAILED)) {
                executor.shutdown();

                try {
                    onStop();
                } catch (Exception e) {
                    LOG.warn(name() + " - stopped with an exception", e);
                }
            }

            if (failed) {
                setStatus(ServiceStatus.FAILED);
            } else {
                // Only set the status to IDLE if the service is still running
                // This prevents overwriting the status if the command being
                // executed failed and the status has been set to FAILED
                checkAndSetStatus(ServiceStatus.IDLE, ServiceStatus.RUNNING);
            }
        }
    }
}
