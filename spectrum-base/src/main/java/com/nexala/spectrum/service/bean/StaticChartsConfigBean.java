package com.nexala.spectrum.service.bean;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Bean used to configuration the static charts screen.
 * @author brice
 *
 */
public class StaticChartsConfigBean {
	
	private Map<String, List<Integer>> chartsIdByVehicleType = new HashMap<String, List<Integer>>();

	/**
	 * Getter for the chartsIdByVehicleType.
	 * @return the chartsIdByVehicleType
	 */
	public Map<String, List<Integer>> getChartsIdByVehicleType() {
		return chartsIdByVehicleType;
	}

	/**
	 * Setter for the chartsIdByVehicleType.
	 * @param chartsIdByVehicleType the chartsIdByVehicleType to set
	 */
	public void setChartsIdByVehicleType(
			Map<String, List<Integer>> chartsIdByVehicleType) {
		this.chartsIdByVehicleType = chartsIdByVehicleType;
	}
	
}
