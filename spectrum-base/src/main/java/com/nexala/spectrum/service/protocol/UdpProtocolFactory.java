package com.nexala.spectrum.service.protocol;

public interface UdpProtocolFactory<P extends UdpProtocol> {
    P create(byte[] message, ClientConfig context);
}
