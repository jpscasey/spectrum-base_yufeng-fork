package com.nexala.spectrum.service;

/**
 * A named service.
 * 
 * @author Severinas Monkevicius
 * @param <C> the type of configuration used by this service.
 */
public interface Service<C extends ServiceConfig> {
    /**
     * Returns the name of the service.
     * 
     * @return the name of the service.
     */
    String name();

    /**
     * Returns the service configuration.
     * 
     * @return the service configuration.
     */
    C config();

    /**
     * Starts the service. Does nothing unless the status of the service is {@link ServiceStatus#IDLE} or
     * {@link ServiceStatus#FAILED} or the service is disabled. Otherwise, after this method completes, the status of
     * the service should be either {@link ServiceStatus#RUNNING} if it started correctly, or
     * {@link ServiceStatus#FAILED} if it failed to start.
     */
    void start();

    /**
     * Stops the service. Does nothing unless the status of the service is {@link ServiceStatus#RUNNING}. Otherwise,
     * after this method completes, the status of the service should be {@link ServiceStatus#IDLE}.
     */
    void stop();

    /**
     * Returns the status of the service.
     * 
     * @return the status of the service.
     * @see ServiceStatus
     */
    ServiceStatus serviceStatus();
}
