package com.nexala.spectrum.service.protocol;

import org.apache.log4j.Logger;

/**
 * A wrapper class for commands executed by a {@link ScheduledService} that
 * catch and log any exceptions that might cause it to stop executing its
 * commands.
 * @author Severinas Monkevicius
 */
public abstract class ScheduledCommand implements Runnable {
    private static final Logger LOG = Logger.getLogger(ScheduledCommand.class);

    public abstract void execute();

    @Override
    public final void run() {
        try {
            execute();
        } catch (Exception e) {
            LOG.warn("Scheduled command executed with exception", e);
        }
    }
}
