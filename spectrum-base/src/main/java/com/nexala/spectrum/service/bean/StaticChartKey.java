package com.nexala.spectrum.service.bean;


public class StaticChartKey {
	
	private String fleetId = null;
	
	private Integer vehicleId = null;
	
	private Integer interval = null;
	
	private Long endTime = null;
	
	private String timezone = null;
	

	public StaticChartKey(String fleetId, Integer vehicleId, Integer interval, Long endTime, String timezone) {
		this.fleetId = fleetId;
		this.vehicleId = vehicleId;
		this.interval = interval;
		this.endTime = endTime;
		this.timezone = timezone;
	}
	
	/**
	 * Getter for the fleetId.
	 * @return the fleetId
	 */
	public String getFleetId() {
		return fleetId;
	}

	/**
	 * Setter for the fleetId.
	 * @param fleetId the fleetId to set
	 */
	public void setFleetId(String fleetId) {
		this.fleetId = fleetId;
	}

	/**
	 * Getter for the vehicleId.
	 * @return the vehicleId
	 */
	public Integer getVehicleId() {
		return vehicleId;
	}

	/**
	 * Setter for the vehicleId.
	 * @param vehicleId the vehicleId to set
	 */
	public void setVehicleId(Integer vehicleId) {
		this.vehicleId = vehicleId;
	}

	/**
	 * Getter for the interval.
	 * @return the interval
	 */
	public Integer getInterval() {
		return interval;
	}

	/**
	 * Setter for the interval.
	 * @param interval the interval to set
	 */
	public void setInterval(Integer interval) {
		this.interval = interval;
	}

	/**
	 * Getter for the endTime.
	 * @return the endTime
	 */
	public Long getEndTime() {
		return endTime;
	}

	/**
	 * Setter for the endTime.
	 * @param endTime the endTime to set
	 */
	public void setEndTime(Long endTime) {
		this.endTime = endTime;
	}

	/**
	 * Getter for the timezone.
	 * @return the timezone
	 */
	public String getTimezone() {
		return timezone;
	}

	/**
	 * Setter for the timezone.
	 * @param timezone the timezone to set
	 */
	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}

	/**
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((endTime == null) ? 0 : endTime.hashCode());
		result = prime * result + ((fleetId == null) ? 0 : fleetId.hashCode());
		result = prime * result
				+ ((interval == null) ? 0 : interval.hashCode());
		result = prime * result
				+ ((timezone == null) ? 0 : timezone.hashCode());
		result = prime * result
				+ ((vehicleId == null) ? 0 : vehicleId.hashCode());
		return result;
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StaticChartKey other = (StaticChartKey) obj;
		if (endTime == null) {
			if (other.endTime != null)
				return false;
		} else if (!endTime.equals(other.endTime))
			return false;
		if (fleetId == null) {
			if (other.fleetId != null)
				return false;
		} else if (!fleetId.equals(other.fleetId))
			return false;
		if (interval == null) {
			if (other.interval != null)
				return false;
		} else if (!interval.equals(other.interval))
			return false;
		if (timezone == null) {
			if (other.timezone != null)
				return false;
		} else if (!timezone.equals(other.timezone))
			return false;
		if (vehicleId == null) {
			if (other.vehicleId != null)
				return false;
		} else if (!vehicleId.equals(other.vehicleId))
			return false;
		return true;
	}


}
