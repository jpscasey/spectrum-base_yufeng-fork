package com.nexala.spectrum.service;

public interface SingleThreadServiceConfig extends ServiceConfig {
    boolean isKeepAlive();
}
