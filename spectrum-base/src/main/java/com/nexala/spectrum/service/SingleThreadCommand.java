package com.nexala.spectrum.service;

public interface SingleThreadCommand {
    void run() throws ServiceException;
}
