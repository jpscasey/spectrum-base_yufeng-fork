package com.nexala.spectrum.service.protocol;

import java.io.IOException;
import java.net.ServerSocket;

import com.google.inject.ImplementedBy;

@ImplementedBy(BasicSocketProvider.class)
public interface SocketProvider {
    public ServerSocket create(HasTcpSocketConfiguration conf) throws IOException;
}
