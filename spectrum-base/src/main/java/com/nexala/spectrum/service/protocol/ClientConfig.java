package com.nexala.spectrum.service.protocol;

/**
 * Connection details for TCP or UDP clients.
 * 
 * @author Severinas Monkevicius
 */
public interface ClientConfig {
    /**
     * The address to connect to.
     * 
     * @return the address to connect to.
     */
    String getAddress();

    /**
     * The port to connect to.
     * 
     * @return the port to connect to.
     */
    int getPort();
}
