package com.nexala.spectrum.service.protocol;

import java.util.concurrent.ScheduledExecutorService;

import com.nexala.spectrum.service.ScheduledService;
import com.nexala.spectrum.service.ServiceConfig;

/**
 * Configuration details for a {@link ScheduledService scheduled service}.
 * 
 * @author Severinas Monkevicius
 */
public interface ScheduledServiceConfig extends ServiceConfig {
    /**
     * How often the service should execute its commands.
     * 
     * @return the number of milliseconds between command executions. The actual time between two consecutive executions
     *         is affected by whether the service executes commands {@link #isFixedRate() at a fixed rate} or not.
     */
    long getDelay();

    /**
     * Whether commands should be executed at a fixed rate.
     * 
     * @return <tt>true</tt> if commands should be executed at a fixed rate; <tt>false</tt> if commands should be
     *         executed with a fixed delay.
     * @see {@link ScheduledExecutorService#scheduleAtFixedRate(Runnable, long, long, java.util.concurrent.TimeUnit)
     * @see {@link ScheduledExecutorService#scheduleWithFixedDelay(Runnable, long, long, java.util.concurrent.TimeUnit)
     */
    boolean isFixedRate();
}
