package com.nexala.spectrum.service;

class ServiceStartupThread extends Thread {
    private final Service<?> service;

    public ServiceStartupThread(Service<?> service) {
        super("start-" + service.name());

        this.service = service;
    }

    @Override
    public void run() {
        service.start();
    }
}
