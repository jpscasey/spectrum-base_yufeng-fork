package com.nexala.spectrum.service.protocol;

public class ProtocolException extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = -8650882922497654064L;

    public ProtocolException() {
        super();
    }
    
    public ProtocolException(String s) {
        super(s);
    }
    
    public ProtocolException(String s, Throwable t) {
        super(s, t);
    }
}
