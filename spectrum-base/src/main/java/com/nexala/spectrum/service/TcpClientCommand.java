package com.nexala.spectrum.service;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

import com.google.inject.Inject;
import com.nexala.spectrum.service.protocol.ClientConfig;
import com.nexala.spectrum.service.protocol.ScheduledCommand;

/**
 * A TCP client that can be executed repeatedly by a {@link ScheduledService scheduled service}.
 * 
 * @author Severinas Monkevicius
 */
public abstract class TcpClientCommand extends ScheduledCommand {
    private static final Logger LOG = Logger.getLogger(TcpClientCommand.class);

    private final ClientConfig clientConfig;

    /**
     * Creates a new TCP client that will connect to the specified server.
     * 
     * @param clientConfig the server to connect to.
     */
    @Inject
    public TcpClientCommand(ClientConfig clientConfig) {
        this.clientConfig = clientConfig;
    }

    @Override
    public void execute() {
        String address = clientConfig.getAddress();
        int port = clientConfig.getPort();

        InetAddress inetAddress = null;
        try {
            inetAddress = InetAddress.getByName(address);
        } catch (UnknownHostException e) {
            LOG.warn("Failed to resolve " + address, e);
            return;
        }

        Socket socket = null;
        try {
            socket = new Socket(inetAddress, port);
        } catch (IOException e) {
            LOG.warn("Failed to connect to " + address + ":" + port);
            return;
        }

        try {
            execute(socket);
        } catch (IOException e) {
            LOG.warn("Failed to execute TCP client command connected to " + address + ":" + port, e);
        } finally {
            IOUtils.closeQuietly(socket);
        }
    }

    /**
     * Executes this TCP client.
     * 
     * @param socket a socket connected to the specified server. Implementations should not close this socket as it is
     *        closed automatically after the method exits.
     * @throws IOException if an IO error occurs.
     */
    protected abstract void execute(Socket socket) throws IOException;
}
