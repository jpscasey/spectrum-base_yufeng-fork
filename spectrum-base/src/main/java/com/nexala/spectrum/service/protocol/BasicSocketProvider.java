package com.nexala.spectrum.service.protocol;

import java.io.IOException;
import java.net.ServerSocket;

import org.apache.log4j.Logger;

import com.nexala.spectrum.service.TcpService;

public class BasicSocketProvider implements SocketProvider {

    private final Logger LOG = Logger.getLogger(TcpService.class);
    
    @Override
    public ServerSocket create(HasTcpSocketConfiguration conf) throws IOException {
        int port = conf.getPort();
        LOG.trace(String.format("Opening TCP socket on port %d", port));
        ServerSocket ss = new ServerSocket(port);
        LOG.debug(String.format("Opened TCP socket on port %d", port));
        return ss;
    }
}
