package com.nexala.spectrum.service;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

import org.apache.log4j.Logger;

import com.nexala.spectrum.service.protocol.ClientConfig;
import com.nexala.spectrum.service.protocol.DatagramProvider;
import com.nexala.spectrum.service.protocol.HasUdpSocketConfiguration;
import com.nexala.spectrum.service.protocol.Protocol;
import com.nexala.spectrum.service.protocol.UdpProtocol;
import com.nexala.spectrum.service.protocol.UdpProtocolFactory;

/**
 * A named service that listens for connections on a UDP socket and uses a {@link UdpProtocolFactory} to handle them.
 * 
 * @author Severinas Monkevicius
 */
public class UdpService extends AbstractSocketService<DatagramSocket, HasUdpSocketConfiguration> {
    private static final Logger LOG = Logger.getLogger(UdpService.class);

    /**
     * The protocol factory that spawns protocol instances to handle incoming connections.
     */
    private final UdpProtocolFactory<?> protocolFactory;

    private final DatagramProvider dgramProvider;

    /**
     * Creates a new named service that will listen on a UCD socket defined by the specified configuration and will
     * handle incoming connections using the specified protocol factory.
     * 
     * @param name the name.
     * @param config the configuration.
     * @param protocolFactory the protocol factory.
     */
    public UdpService(String name, HasUdpSocketConfiguration config, UdpProtocolFactory<?> protocolFactory,
            DatagramProvider dgramProvider) {
        super(name, config);
        this.protocolFactory = protocolFactory;
        this.dgramProvider = dgramProvider;
    }

    /**
     * Binds a UDP socket to the port specified by the configuration.
     * 
     * @param config the configuration.
     */
    @Override
    protected DatagramSocket openSocket(HasUdpSocketConfiguration config) throws IOException {
        int port = config.getPort();
        LOG.trace(String.format("Opening UDP socket on port %d", port));
        DatagramSocket socket = dgramProvider.create(config);
        LOG.debug(String.format("Opened UDP socket on port %d", port));
        return socket;
    }

    /**
     * Accepts an incoming UDP packet on the bound UDP socket and passes it to a {@link UdpProtocol} created by the
     * protocol factory for handling.
     * 
     * @param socket the socket.
     * @param config the configuration.
     */
    @Override
    protected Protocol accept(DatagramSocket socket, HasUdpSocketConfiguration config) throws IOException {
        byte[] buf = new byte[config.getMaxPacketSize()];
        DatagramPacket packet = new DatagramPacket(buf, buf.length);
        socket.receive(packet);

        // Copy the message contents into a new byte array
        final byte[] message = new byte[packet.getLength()];
        System.arraycopy(buf, 0, message, 0, message.length);

        ClientConfig cc = new SimpleClientConfig(packet.getAddress().getHostAddress(), packet.getPort());

        return protocolFactory.create(message, cc);
    }

    /**
     * Closes the UDP socket.
     * 
     * @param socket the socket.
     */
    @Override
    protected void closeSocket(DatagramSocket socket) {
        int port = socket.getLocalPort();
        LOG.trace(String.format("Closing UDP socket on port %d", port));
        socket.close();
        LOG.debug(String.format("Closed UDP socket on port %d", port));
    }
}
