package com.nexala.spectrum.service.protocol;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

/**
 * A simple client that binds to a random available port and can send UDP messages.
 * 
 * @author Severinas Monkevicius
 */
public class DatagramSocketClient {
    /**
     * Creates a new UDP client that will bind to a random available port.
     */
    public DatagramSocketClient() {}

    public synchronized void sendMessage(byte[] message, ClientConfig clientConfig) throws ProtocolException {
        String address = clientConfig.getAddress();
        int port = clientConfig.getPort();

        DatagramSocket socket = null;
        try {
            socket = new DatagramSocket();

            InetAddress inetAddr = InetAddress.getByName(address);
            DatagramPacket messagePacket = new DatagramPacket(message, message.length, inetAddr, port);
            socket.send(messagePacket);
        } catch (SocketException e) {
            throw new ProtocolException("Failed to initialize socket", e);
        } catch (UnknownHostException e) {
            throw new ProtocolException("Failed to determine the IP address of " + address, e);
        } catch (IOException e) {
            throw new ProtocolException("Failed to write to socket", e);
        } finally {
            if (socket != null) {
                socket.close();
            }
        }
    }
}
