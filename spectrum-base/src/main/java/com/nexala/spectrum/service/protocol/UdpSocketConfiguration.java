package com.nexala.spectrum.service.protocol;

public class UdpSocketConfiguration extends AbstractSocketConfiguration implements HasUdpSocketConfiguration {

    private final int port;
    
    public UdpSocketConfiguration(int port) {
        if (port < 0 || port > 65535) {
            throw new IllegalArgumentException("Invalid port number");
        }
        
        this.port = port;
    }
    
    @Override
    public int getPort() {
        return port;
    }

    @Override
    public int getMaxPacketSize() {
        // TODO Auto-generated method stub
        return 65535; // FIXME
    }
}
