package com.nexala.spectrum.service;

public interface ServiceConfig {
    /**
     * Whether the service is enabled.
     * 
     * @return whether the service is enabled.
     */
    boolean isEnabled();

    /**
     * The number of milliseconds to wait between {@link Service#start() starting the service} and actually performing
     * the service's tasks.
     * 
     * @return number of milliseconds to wait between {@link Service#start() starting the service} and actually
     *         performing the service's tasks.
     */
    long getInitialDelay();
}
