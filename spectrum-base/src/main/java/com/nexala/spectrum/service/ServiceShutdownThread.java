package com.nexala.spectrum.service;

class ServiceShutdownThread extends Thread {
    private final Service<?> service;

    public ServiceShutdownThread(Service<?> service) {
        super("stop-" + service.name());

        this.service = service;
    }

    @Override
    public void run() {
        service.stop();
    }
}
