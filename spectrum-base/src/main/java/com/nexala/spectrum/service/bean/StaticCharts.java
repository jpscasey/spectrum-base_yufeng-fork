package com.nexala.spectrum.service.bean;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Contains statics charts that will be on the same screen
 * @author brice
 *
 */
public class StaticCharts {
	
	Map<Integer, StaticChartBean> charts = new HashMap<Integer, StaticChartBean>();

	/**
	 * Get a chart.
	 * @return the charts
	 */
	public StaticChartBean getChart(Integer chartId) {
		StaticChartBean chart = charts.get(chartId);
		return chart;
	}

	/**
	 * Setter for the charts.
	 * @param charts the charts to set
	 */
	public void setCharts(List<StaticChartBean> charts) {
		for(StaticChartBean chart : charts) {
			addChart(chart);
		}
	}
	
	/**
	 * Add a chart to the list
	 * @param chart the chart to add
	 */
	public void addChart(StaticChartBean chart) {
		this.charts.put(chart.getChartId(), chart);
	}

}
