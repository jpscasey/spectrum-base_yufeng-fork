package com.nexala.spectrum.service.protocol;

import java.net.Socket;

import com.google.inject.assistedinject.Assisted;

public interface TcpProtocolFactory<P extends TcpProtocol> {
    P create(@Assisted Socket socket);
}
