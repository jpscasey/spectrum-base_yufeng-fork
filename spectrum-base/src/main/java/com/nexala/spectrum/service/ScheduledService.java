package com.nexala.spectrum.service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;

import com.nexala.spectrum.service.protocol.ScheduledServiceConfig;

/**
 * A service that periodically executes a single command or a list of commands.
 * 
 * @author Severinas Monkevicius
 */
public class ScheduledService extends AbstractService<ScheduledServiceConfig> {
    public static final int TIMEOUT = 10000;

    private final List<Runnable> commands;
    private final ThreadFactory threadFactory;

    private ScheduledExecutorService executor;

    private ScheduledService(String name, ScheduledServiceConfig config, int commandCount) {
        super(name, config);

        this.commands = new ArrayList<Runnable>(commandCount);
        if (commandCount > 1) {
            this.threadFactory = new NumberedThreadFactory(name);
        } else {
            this.threadFactory = new NamedThreadFactory(name);
        }
    }

    /**
     * Creates a new scheduled service.
     * 
     * @param name the name of the service.
     * @param config the configuration of the service.
     * @param command the command that the service will execute.
     */
    public ScheduledService(String name, ScheduledServiceConfig config, Runnable command) {
        this(name, config, 1);

        this.commands.add(command);
    }

    /**
     * Creates a new scheduled service.
     * 
     * @param name the name of the service.
     * @param config the configuration of the service.
     * @param commands the commands that the service will execute.
     */
    public ScheduledService(String name, ScheduledServiceConfig config, List<Runnable> commands) {
        this(name, config, commands.size());

        this.commands.addAll(commands);
    }

    @Override
    protected void doStart() throws Exception {
        executor = Executors.newScheduledThreadPool(commands.size(), threadFactory);
        for (Runnable command : commands) {
            if (config().isFixedRate()) {
                executor.scheduleAtFixedRate(command, config().getInitialDelay(), config().getDelay(),
                        TimeUnit.MILLISECONDS);
            } else {
                executor.scheduleWithFixedDelay(command, config().getInitialDelay(), config().getDelay(),
                        TimeUnit.MILLISECONDS);
            }
        }
    }

    @Override
    protected void doStop() throws Exception {
        ServiceUtilities.terminateExecutor(executor, TIMEOUT);
    }
}
