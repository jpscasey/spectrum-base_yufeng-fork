package com.nexala.spectrum.service;

/**
 * An exception that indicates a generic problem with the service.
 * 
 * @author Severinas Monkevicius
 */
@SuppressWarnings("serial")
public class ServiceException extends Exception {
    public ServiceException() {
        super();
    }

    public ServiceException(String message) {
        super(message);
    }

    public ServiceException(Throwable cause) {
        super(cause);
    }

    public ServiceException(String message, Throwable cause) {
        super(message, cause);
    }
}
