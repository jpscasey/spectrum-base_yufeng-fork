package com.nexala.spectrum.service.protocol;

import com.nexala.spectrum.service.SingleThreadServiceConfig;

public interface HasSocketConfiguration extends SingleThreadServiceConfig {
    /**
     * The port to bind to.
     * @return the port to bind to.
     */
    int getPort();

    /**
     * Number of milliseconds to wait when attempting to close client
     * connections.
     * @return the number of milliseconds to wait when attempting to close
     *         client connections.
     */
    int getDisconnectTimeout();

    /**
     * The maximum number of concurrently active client connections.
     * @return the maximum number of concurrently active client connections.
     */
    int getMaxConnectionCount();
}
