package com.nexala.spectrum.service.protocol;

public interface HasUdpSocketConfiguration extends HasSocketConfiguration {
    /**
     * The maximum size of a single packet in bytes
     * @return the maximum size of a single packet.
     */
    int getMaxPacketSize();
}
