package com.nexala.spectrum.service;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

import com.nexala.spectrum.service.protocol.HasTcpSocketConfiguration;
import com.nexala.spectrum.service.protocol.Protocol;
import com.nexala.spectrum.service.protocol.SocketProvider;
import com.nexala.spectrum.service.protocol.TcpProtocol;
import com.nexala.spectrum.service.protocol.TcpProtocolFactory;

/**
 * A named service that listens for connections on a TCP socket and uses a {@link TcpProtocolFactory} to handle them.
 * 
 * @author Severinas Monkevicius
 */
public class TcpService extends AbstractSocketService<ServerSocket, HasTcpSocketConfiguration> {
    private final Logger LOG = Logger.getLogger(TcpService.class);

    private final SocketProvider socketProvider;

    /**
     * The protocol factory that spawns protocol instances to handle incoming connections.
     */
    private final TcpProtocolFactory<? extends TcpProtocol> protocolFactory;

    /**
     * Creates a new named service that will listen on a TCP socket defined by the specified configuration and will
     * handle incoming connections using the specified protocol factory.
     * 
     * @param name the name.
     * @param config the configuration.
     * @param protocolFactory the protocol factory.
     */
    public TcpService(String name, HasTcpSocketConfiguration config,
            TcpProtocolFactory<? extends TcpProtocol> protocolFactory, SocketProvider socketProvider) {
        super(name, config);

        this.protocolFactory = protocolFactory;
        this.socketProvider = socketProvider;
    }

    /**
     * Binds a TCP socket to the port specified by the configuration.
     * 
     * @param config the configuration.
     */
    @Override
    protected ServerSocket openSocket(HasTcpSocketConfiguration config) throws IOException {
        return socketProvider.create(config);
    }

    /**
     * Accepts an incoming TCP connection on the bound UDP socket and passes it to a {@link TcpProtocol} created by the
     * protocol factory for handling.
     * 
     * @param socket the socket.
     * @param config the configuration.
     */
    @Override
    protected Protocol accept(ServerSocket socket, HasTcpSocketConfiguration config) throws IOException {
        Socket cs = null;
        // Don't handle IO exceptions here, just close the newly opened
        // socket and rethrow it. Once out of this method, the socket will
        // be given to a protocol instance that will close it
        try {
            cs = socket.accept();
            cs.setSoTimeout(config.getSocketTimeout());
        } catch (IOException e) {
            IOUtils.closeQuietly(cs);
            throw e;
        }
        return protocolFactory.create(cs);
    }

    /**
     * Closes the TCP socket.
     * 
     * @param socket the socket.
     */
    @Override
    protected void closeSocket(ServerSocket socket) {
        int port = socket.getLocalPort();
        LOG.trace(String.format("Closing TCP socket on port %d", port));
        IOUtils.closeQuietly(socket);
        LOG.debug(String.format("Closed TCP socket on port %d", port));
    }
}
