package com.nexala.spectrum;

public class Spectrum {
    public static final String ENCODING = "UTF-8";

    public static final String SPECTRUM_PROPERTIES_PATH_PROP = "spectrumPropertiesPath";
    public static final String SPECTRUM_PROPERTIES_PATH = "spectrum.properties";
    public static final String SPECTRUM_BASE_DS = "java:/DefaultSpectrumDS";
    public static final String ADMINISTRATION_DS = "java:/UserAdminDS";
    public static final String SPECTRUM_BASE_SQL_XML = "spectrum-base-sql.xml";
    public static final String OP_DS = "java:/OpDb";
    public static final String ROUTES = "routes.xml";
    public static final String STATIONS = "stations.xml";
    public static final String ANALYSIS_CONFIG_PATH = "analysis-config.xml";

    public static final String DATA_ACCESS_TYPE_DENY = "DENY";
    public static final String DATA_ACCESS_WILDCARD = "*";

    // properties - configurations
    public static final String DB_TIMEZONE = "db.timezone";
    public static final String APP_WORKDIR = "app.workdir";
    public static final String SPECTRUM_APPLICATON_TIMEOUT_INTERVAL_IN_SECONDS = "spectrum.application.timeout.intervalinseconds";
    public static final String SPECTRUM_REFRESH_CONFIGURATION_INTERVAL = "spectrum.refresh.configuration.interval";
    public static final String SPECTRUM_REFRESH_VALIDATIONRULES_INTERVAL = "spectrum.refresh.validationrules.interval";
    public static final String SPECTRUM_REFRESH_LOOKUPVALUES_INTERVAL = "spectrum.refresh.lookupvalues.interval";
    public static final String SPECTRUM_REFRESH_CHANNELCONFIGURATIONS_INTERVAL = "spectrum.refresh.channelconfigurations.interval";
    public static final String SPECTRUM_REFRESH_FLEET_CACHE_INTERVAL = "spectrum.refresh.fleetcache.interval";
    public static final String SPECTRUM_REFRESH_FLEET_CACHE_EXPIRATION_INTERVAL = "spectrum.refresh.fleetcacheexpiration.interval";
    public static final String SPECTRUM_DATAPLOT_PERIODS = "spectrum.dataplot.periods";
    public static final String SPECTRUM_DATAPLOT_DEFAULT_PERIOD = "spectrum.dataplot.defaultperiod";
    public static final String SPECTRUM_DATAPLOT_REFRESH_PERIOD = "spectrum.dataplot.refreshperiod";
    public static final String SPECTRUM_DATAPLOT_EXPORT_PDF = "spectrum.dataplot.exportPDF";
    public static final String SPECTRUM_DATAPLOT_REFRESH_NOT_LIVE = "spectrum.dataplot.refreshnotlive";
    public static final String SPECTRUM_MERGE_RESOURCES = "spectrum.merge.resources";
    public static final String SPECTRUM_TEMPLATES_CACHE = "spectrum.templates.cache";
    public static final String SPECTRUM_SERVICES_CACHE = "spectrum.services.cache";
    public static final String SPECTRUM_RULE_EDITOR_ENABLE_OPTIONAL_CATEGORIES = "spectrum.rule.editor.enable.optional.categories";
    public static final String SPECTRUM_RULE_EDITOR_DISABLE_RETRACT_BUTTON = "spectrum.rule.editor.disable.retract.button";
    public static final String SPECTRUM_RULE_EDITOR_ENABLE_SEND_ALERT_BUTTON = "spectrum.rule.editor.enable.sendalert.button";
    public static final String SPECTRUM_RULE_EDITOR_ENABLE_EVENT_STREAM = "spectrum.rule.editor.enable.event.stream";
    public static final String SPECTRUM_RULE_EDITOR_ENABLE_DOWNLOAD_REQUEST_BUTTON = "spectrum.rule.editor.enable.downloadrequest.button";
    public static final String SPECTRUM_RULE_EDITOR_ENABLE_FAULT_GROUP = "spectrum.rule.editor.enable.fault.group";
    public static final String SPECTRUM_RULE_EDITOR_ENABLE_FAULT_SOURCE = "spectrum.rule.editor.enable.fault.source";
    public static final String SPECTRUM_RULE_EDITOR_BULK_TRANSFORM_ALLOWED_USER = "spectrum.rule.editor.bulk.transform.allowed.user";
    public static final String SPECTRUM_RULE_ENGINE_GAP_ENABLE = "spectrum.rule.engine.gap.enable";
    public static final String SPECTRUM_RULE_ENGINE_GAP_INTERVAL = "spectrum.rule.engine.gap.interval";
    public static final String SPECTRUM_RULE_ENGINE_EMAILS_ENABLE = "spectrum.rule.engine.emails.enable";
    public static final String SPECTRUM_RULE_ENGINE_KEEP_ACTIVE_RULE_ENABLED = "spectrum.rule.engine.keep.active.rule.enabled";
    public static final String SPECTRUM_RULE_ENGINE_RESET_RULE = "spectrum.rule.engine.reset.rule";

    public static final String SPECTRUM_EVENT_HISTORY_SEARCH_LIMIT = "spectrum.eventhistory.search.limit";
    public static final String SPECTRUM_EVENT_HISTORY_ADVANCED_EXPORT_LIMIT = "spectrum.eventhistory.advancedexport.limit";
    public static final String SPECTRUM_FAULT_SENDER_ENABLE = "spectrum.fault.sender.enable";
    public static final String SPECTRUM_FAULT_SENDER_MAX_RETRIES = "spectrum.fault.sender.max.retries";
    public static final String SPECTRUM_FAULT_SENDER_E2M_IP = "spectrum.fault.sender.e2m.ip";
    public static final String SPECTRUM_FAULT_SENDER_E2M_PATH = "spectrum.fault.sender.e2m.path";

    public static final String FEATURES_TO_SYSTEM_ROLES = "spectrum.system.roles";

    // Recovery properties
    public static final String RECOVERY_DEFAULT_URL_PATH = "/editor/recovery";
    public static final String RECOVERY_DEFAULT_URL_QUERY = "path=";
    
    // Email system properties
    public static final String EMAIL_URL_PROTOCOL = "mail.template.url.protocol";
    public static final String EMAIL_URL_HOSTNAME = "mail.template.url.hostname";
    public static final String EMAIL_URL_PORT = "mail.template.url.port";
    public static final String EMAIL_URL_PATH = "mail.template.url.path";
    public static final String EMAIL_URL_QUERY = "mail.template.url.query";
    public static final String MAIL_HOSTNAME = "mail.smtp.host";
    public static final String MAIL_PORT = "mail.smtp.port";
    public static final String MAIL_USERNAME = "mail.smtp.username";
    public static final String MAIL_PASSWORD = "mail.smtp.password";
    public static final String MAIL_STARTTLS = "mail.smtp.starttls";
    public static final String MAIL_EMAILSENDER = "mail.smtp.emailsender";
    public static final String EMAIL_TIMEZONE = "mail.template.timezone";
    public static final String EMAIL_DATE_FORMAT = "mail.template.dateformat";

    // Email default value system properties
    public static final String EMAIL_DEFAULT_URL_PROTOCOL = "http";
    public static final String EMAIL_DEFAULT_URL_HOSTNAME = "localhost";
    public static final int EMAIL_DEFAULT_URL_PORT = 80;
    public static final String EMAIL_DEFAULT_URL_PATH = "/editor/emailTemplate";
    public static final String EMAIL_DEFAULT_URL_QUERY = "path=";
    public static final String EMAIL_DEFAULT_MAIL_HOSTNAME = "127.0.0.1";
    public static final String EMAIL_DEFAULT_MAIL_PORT = "25";
    public static final Boolean EMAIL_DEFAULT_STARTTLS = false;
    public static final String EMAIL_DEFAULT_TIMEZONE = "UTC";
    public static final String EMAIL_DEFAULT_DATE_FORMAT = "dd/MM/yyyy HH:mm:ss";
    
    public static final String COLUMN_CHANNEL_NAME = "channelName";
    public static final String COLUMN_CHANNEL_DESCRIPTION = "channelDescription";
    public static final String COLUMN_ALWAYS_DISPLAYED = "alwaysDisplayed";

    // FIXME
    public static final String ALL_CODE = "all";
    public static final String ALL_EXCEPT_TEST = "allExceptTest";
    public static final String EVENTS_TYPE_FAULT = "F";
    public static final String EVENTS_TYPE_WARNING = "W";
    public static final int UNIT_DETAIL_NUMBER_OF_RECENT_EVENTS = 50;
    public static final int FLEET_SUMMARY_NUMBER_OF_RECENT_EVENTS = 20;
    public static final int PREVIOUS_RECORD = -1;
    public static final int RECORD_BY_TIME = 0;
    public static final int NEXT_RECORD = 1;

    public static final String LOCATION_BY_CODE = "locationByCode";
    public static final String LOCATION_LIST = "locationListQuery";
    
    public static final String TIMESTAMP = "TimeStamp";
    public static final String LOCATION = "Location";
    public static final String DIAGRAM = "Diagram";
    public static final String FORMATION = "formation";
    public static final String FLEET_FORMATION_ID = "FleetFormationID";
    public static final String HEADCODE = "Headcode";
    public static final String UNIT_ID = "UnitID";
    public static final String UNIT_ID_GPS = "UnitId";
    public static final String UNIT_TYPE = "UnitType";
    public static final String UNIT_POSITION = "UnitPosition";
    public static final String UNIT_NUMBER = "UnitNumber";
    public static final String UNIT_NUMBER1 = "UnitNumber1";
    public static final String UNIT_NUMBER2 = "UnitNumber2";
    public static final String UNIT_NUMBER3 = "UnitNumber3";
    public static final String UNIT_NUMBER4 = "UnitNumber4";
    public static final String UNIT_NUMBER5= "UnitNumber5";
    public static final String SERVICE_STATUS = "ServiceStatus";
    public static final String VEHICLE_ID = "VehicleID";
    public static final String VEHICLE_TYPE = "VehicleType";
    public static final String VEHICLE_NUMBER = "VehicleNumber";
    public static final String FAULT_NUMBER = "FaultNumber";
    public static final String WARNING_NUMBER = "WarningNumber";
    public static final String FLEET_ID = "FleetId";
    public static final String FLEET_CODE = "FleetCode";
    public static final String PROPERTY_NAME = "PropertyName";
    public static final String PROPERTY_VALUE = "PropertyValue";
    public static final String TRAIN_ICON = "TrainIcon";
    public static final String TRAIN_ICON_NAME = "train";
    public static final String MAP_FILTER_GROUPS = "MapFilterGroups";
    public static final String MAP_FILTER_TEXT_GROUPS = "MapFilterTextGroups";

    // SQL query keys - see sql.xml
    public static final String CHANNEL_CONFIG_QUERY = "channelConfigQuery";
    public static final String CHANNEL_CONFIG_KEYWORD_QUERY = "channelConfigKeywordQuery";
    public static final String CHANNEL_CONFIG_ID_QUERY = "channelConfigIdQuery";
    public static final String CHANNEL_CONFIG_SAVE_QUERY = "channelConfigSaveQuery";
    public static final String CHANNEL_GROUP_CONFIG_QUERY = "channelGroupConfigQuery";
    public static final String CHANNEL_GROUP_CONFIG_ID_QUERY = "channelGroupConfigIdQuery";
    public static final String CHANNEL_RULE_QUERY = "channelRuleQuery";
    public static final String CHANNEL_RULE_CHANNELID_QUERY = "channelRuleChannelIdQuery";
    public static final String CHANNEL_RULE_VALIDATION_QUERY = "channelRuleValidationQuery";
    public static final String CHANNEL_RULE_VALIDATION_CHANNELID_QUERY = "channelRuleValidationChannelIdQuery";
    public static final String CHANNEL_STATUS_QUERY = "channelStatusQuery";
    public static final String LOOKUP_VALUE_CHANNEL_QUERY = "lookupValueChannelQuery";
    public static final String STOCK_SIBLINGS = "stockSiblings";
    public static final String CONFIGURATION_SINGLE_QUERY = "configurationSingleQuery";
    public static final String CONFIGURATION_CHANGE_QUERY = "configurationChangeQuery";
    public static final String INVALID_FLEET_CHANNEL_QUERY = "getInvalidFleetChannels";
    public static final String CHANNEL_VEHICLE_TYPE_CONFIG_QUERY = "channelVehicleTypeConfigQuery";
    public static final String VEHICLE_TYPE_QUERY = "vehicleTypeQuery";
    public static final String ALL_EVENT_CHANNEL_VALUE_LATEST_QUERY = "allEventChannelValueLatest";
    public static final String ALL_EVENT_CHANNEL_VALUE_SINCE_QUERY = "allEventChannelValueSince";
    public static final String UNIT_FORMATION_BY_UNIT_ID = "unitFormationByUnitId";
    public static final String UNIT_LAST_MAINTENANCE_TIMESTAMP = "unitLastMaintTimestamp";
    public static final String UNIT_STATUS = "unitStatus";
    public static final String UNIT_BY_UNIT_NUMBER = "unitByUnitNumber";
    public static final String UNIT_TYPES = "unitTypes";
    public static final String UNITS_BY_UNIT_IDS = "unitsByUnitIds";
    public static final String ALL_UNITS_QUERY = "allUnitsQuery";
    public static final String ALL_VEHICLES = "allVehicles";
    public static final String VEHICLE_NUMBER_BY_ID = "vehicleNumberById";
    public static final String VEHICLES_FOR_UNIT_ID = "vehiclesForUnitId";
    public static final String VEHICLE_ID_BY_NUMBER = "vehicleIdByNumber";
    public static final String GET_WORK_ORDERS = "getWorkOrders";
    public static final String GET_WORK_ORDER_COMMENT = "getWorkOrderComment";
    public static final String GET_RESTRICTIONS = "getRestrictions";
    public static final String EVENT_GET_ENDTIME = "getEndTime";
    public static final String GET_STOCKS_BY_EVENT = "getStocksByEvent";
    
    // Queries required by the charting...
    public static final String CHART_QUERY = "chart";
    public static final String USER_CHART_CONFIGURATIONS = "userChartConfigurations";
    public static final String SYSTEM_CHART_CONFIGURATIONS = "systemChartConfigurations";
    public static final String INSERT_CHART_CONFIGURATION_QUERY = "insertChartConfiguration";
    public static final String UPDATE_CHART_CONFIGURATION_QUERY = "updateChartConfiguration";
    public static final String INSERT_CHART_QUERY = "insertChart";
    public static final String UPDATE_CHART_QUERY = "updateChart";
    public static final String INSERT_CHART_CHANNEL_QUERY = "insertChartChannel";
    public static final String DELETE_CHART = "deleteChart";
    public static final String DELETE_CHART_CHANNELS = "deleteChannels";
    public static final String CHART_CHANNEL_QUERY = "chartChannel";
    public static final String RENAME_CHART = "renameChart";
    public static final String CREATE_CHART = "createChart";
    public static final String INSERT_CHANNEL = "insertChannel";
    public static final String CHART_BY_ID_AND_NAME = "chartByIdAndName";
    public static final String UNIT_SEARCH_QUERY = "unitSearchQuery";
    public static final String CREATE_FAULT_SEARCH_QUERY = "createFaultSearchQuery";


    // Queries required by the events...
    public static final String CURRENT_EVENTS = "currentEvents";
    public static final String ALL_EVENTS_UNIT_QUERY = "allEventsUnitQuery";
    public static final String LIVE_EVENTS_UNIT_QUERY = "liveEventsUnitQuery";
    public static final String LIVE_EVENTS_FORMATION_QUERY = "liveEventsFormationQuery";
    public static final String LIVE_EVENTS_QUERY = "allCurrentEventsQuery";
    public static final String NOT_LIVE_EVENTS_UNIT_QUERY = "notLiveEventsUnitQuery";
    public static final String NOT_LIVE_EVENTS_FORMATION_QUERY = "notLiveEventsFormationQuery";
    public static final String ACKNOWLEDGED_EVENTS_UNIT_QUERY = "acknowledgedLiveEventsUnitQuery";
    public static final String NOT_ACKNOWLEDGED_EVENTS_UNIT_QUERY = "notAcknowledgedLiveEventsUnitQuery";
    public static final String EVENTS_SINCE = "eventsSince";
    public static final String EVENT_HISTORY_QUERY = "eventSearch";
    public static final String EVENT_HISTORY_ADVANCED_QUERY = "eventSearchAdvanced";
    public static final String EVENT_COUNT_LIVE = "eventCountLive";
    public static final String EVENT_COUNT_SINCE = "eventCountSince";
    public static final String EVENT_DETAIL = "eventDetail";
    public static final String EVENT_FORMATION = "eventFormation";
    public static final String EVENT_UPDATE = "eventUpdate";
    public static final String EVENT_ACKNOWLEDGE = "acknowledgeEvent";
    public static final String EVENT_GROUP_ACKNOWLEDGE = "acknowledgeGroup";
    public static final String EVENT_DEACTIVATE = "deactivateEvent";
    public static final String EVENT_COMMENTS = "eventComments";
    public static final String EVENT_SAVE_COMMENT = "eventSaveComment";
    public static final String LIVE_EVENTS_TIMESTAMP = "liveEventsTimestamp";
    public static final String RECENT_EVENTS_TIMESTAMP = "recentEventsTimestamp";
    public static final String CREATE_EVENT_STMT = "createFaultStmt";
    public static final String CREATE_EVENT_STMT_NOTCURRENT = "createFaultStmtNotCurrent";
    public static final String CREATE_DELAYED_EVENT_STMT = "createDelayedFaultStmt";
    public static final String RETRACT_EVENT_STMT = "retractFaultStmt";
    public static final String RETRACT_EVENTS_STMT = "retractFaultsStmt";
    public static final String RETRACT_EVENTS_TYPE_STMT = "retractFaultsTypeStmt";
    public static final String FAULT_RETRACTED_QUERY_STMT = "faultRetractedStmt";
    public static final String GET_EVENTS_BY_GROUP_ID = "eventsByGroupId";
    public static final String EVENT_INSERT_EXTERNAL_REFERENCE = "insertFaultExternalReference";
    public static final String EVENT_UPDATE_LAST_UPDATE_TIME = "eventUpdateLastUpdateTime";
    
    public static final String EVENT_CREATE_FAULT_GROUP = "createFaultGroup";
    public static final String EVENT_DELETE_FAULT_GROUP = "deleteFaultGroup";
    public static final String EVENT_ADD_FAULT_TO_GROUP = "addFaultToGroup";
    public static final String EVENT_ADD_FAULT_TO_GROUP_WITH_ROWVERSION = "addFaultToGroupWithRowVersion";
    public static final String EVENT_REMOVE_ALL_FROM_GROUP = "removeAllFaultsFromGroup";
    
    // Queries for the user configuration
    public static final String GET_USER_CONFIG_QUERY = "getUserConfigQuery";
    public static final String INSERT_USER_CONFIG_QUERY = "insertUserConfigQuery";
    public static final String UPDATE_USER_CONFIG_QUERY = "updateUserConfigQuery";

    // Download
    public static final String DOWNLOAD_CURR_DOWNLOAD_CHECK = "currDownloadCheck";
    public static final String DOWNLOAD_SEARCH_QUERY = "downloadSearchQuery";
    public static final String INSERT_DOWNLOAD_STMT = "insertDownloadStmt";
    public static Long DOWNLOAD_MINUTES_TIME_FAILED = 1440L;
    public static final String FAULT_EXPORT_FILE_NAME = "faultExportFileName";
    public static final String EVENT_ANALYSIS_FILE_NAME = "eventAnalysisFileName";

    // Analysis queries
    public static final String ANALYSIS_CONFIGURATION_BY_NAMES = "getAnalysisConfigurationByPropertyNames";

    // Rule engine queries
    public static final String RULE_ENGINE_UPDATE_CURRENT_IDS = "ruleEngineUpdateCurrentIds";
    public static final String RULE_ENGINE_GET_CURRENT_IDS = "ruleEngineGetCurrentIds";
    public static final String GET_VALID_FLEET_FORMATIONS = "getValidFleetFormations";

    // Fault Sender queries
    public static final String FAULT_SENDER_INSERT = "faultSenderInsert";
    public static final String FAULT_SENDER_GET_DATA = "faultToTransmit";
    public static final String FAULT_SENDER_UPDATE = "faultSenderUpdate";
    
    // Fault meta queries
    public static final String FAULT_META_EXTRA_FIELD_INSERT_QUERY = "createFaultMetaExtraField";
    public static final String FAULT_META_EXTRA_FIELD_GET_QUERY = "getFaultMetaExtraFields";
    public static final String FAULT_META_EXTRA_FIELD_UPDATE_QUERY = "updateFaultMetaExtraField";
    public static final String FAULT_META_EXTRA_FIELD_DELETE_QUERY = "deleteFaultMetaExtraField";
    public static final String FAULT_META_EXTRA_FIELD_PARENT_QUERY = "getParentFaultMetaExtraField";
    public static final String FAULT_META_EXTRA_FIELD_PARENT_QUERY_WITH_FLEET_CODE = "getParentFaultMetaExtraFieldWithFleetCode";
    public static final String FAULT_META_EXTRA_FIELD_CHILD_QUERY = "getChildFaultMetaExtraField";
    public static final String FAULT_META_EXTRA_FIELD_CHILD_QUERY_WITH_FLEET_CODE = "getChildFaultMetaExtraFieldWithFleetCode";
    public static final String FAULT_META_PARENT_QUERY = "getParentFaultMeta";
    public static final String FAULT_META_PARENT_QUERY_BY_FLEET_CODE = "getParentFaultMetaByFleetCode";
    public static final String FAULT_META_CHILD_QUERY = "getChildFaultMeta";  
    public static final String FAULT_META_CHILD_QUERY_BY_FLEET_CODE = "getChildFaultMetaByFleetCode";
    public static final String FAULT_META_CHILD_BY_PARENT_QUERY = "getChildByParentFaultMeta";   

    // Screens IDs
    public static final String FLEET_SUMMARY_ID = "fleetSummary";
    public static final String FLEET_LOCATION_ID = "fleetLocation";
    public static final String EVENT_HISTORY_ID = "eventHistory";
    public static final String EVENT_ANALYSIS_ID = "eventAnalysis";
    public static final String CHANNEL_DEFINITION_ID = "channelDefinition";
    public static final String DOWNLOADS_ID = "download";

    // Event fields IDs
    public static final String EVENT_ID = "id";
    public static final String EVENT_HEADCODE = "headcode";
    public static final String EVENT_UNIT_NUMBER = "unitNumber";
    public static final String EVENT_CODE = "eventCode";
    public static final String EVENT_LOCATION = "locationCode";
    public static final String EVENT_SOURCE_ID = "sourceId";
    public static final String EVENT_SOURCE_NUMBER = "sourceNumber";
    public static final String EVENT_RECOVERY_ID = "recoveryId";
    public static final String EVENT_LATITUDE = "latitude";
    public static final String EVENT_LONGITUDE = "longitude";
    public static final String EVENT_FAULT_META = "faultMetaId";
    public static final String EVENT_CURRENT = "current";
    public static final String EVENT_DELAYED = "delayed";
    public static final String EVENT_REPORTING_ONLY = "reportingOnly";
    public static final String EVENT_DESCRIPTION = "description";
    public static final String LONG_DESCRIPTION = "longDescription";
    public static final String EVENT_CATEGORY = "category";
    public static final String EVENT_CATEGORY_ID = "categoryId";
    public static final String EVENT_CREATE_TIME = "createTime";
    public static final String EVENT_END_TIME = "endTime";
    public static final String EVENT_TYPE = "type";
    public static final String EVENT_TYPE_COLOR = "typeColor";
    public static final String EVENT_TYPE_ID = "typeId";
    public static final String EVENT_ROW_VERSION = "rowVersion";
    public static final String EVENT_GROUP_ID = "faultGroupId";
    public static final String EVENT_IS_GROUP_LEAD = "isGroupLead";
    public static final String EVENT_DATE = "Date Occurred";

    // Live & Recent panel filters IDs
    public static final String EVENT_CATEGORY_FILTER = "category";
    public static final String EVENT_SEVERITY_FILTER = "severity";
    public static final String EVENT_FLEET_FILTER = "fleet";
    public static final String EVENT_UNIT_TYPE_FILTER = "unitType";
    
    // Unit Detail tables filters IDs
    public static final String UNIT_DETAIL_CATEGORY_FILTER = "categoryId";
    public static final String UNIT_DETAIL_FAULT_TYPE_FILTER = "faultTypeId";

    // Map markers types
    public static final String MAP_STATION_MARKER = "station";
    public static final String MAP_ROUTE_MARKER = "route";
    public static final String MAP_SAND_MARKER = "sand";
    public static final String MAP_WSP_MARKER = "wsp";
    public static final String MAP_MILEPOST_MARKER = "milepost";
    public static final String MAP_HABD_MARKER = "habd";
    public static final String MAP_OHLV_MARKER = "ohlv";

    // interface files data loader - constants
    public static final String DATA_TYPE_BINARY = "binary";
    public static final String DATA_TYPE_TEXT = "text";
    public static final String SOURCE_FILE_NAME = "file name";
    public static final String SOURCE_FILE_DATA = "file data";
    public static final String FIELD_TYPE_IDENTIFICATION = "identification";
    public static final String FIELD_TYPE_CHANNEL = "channel";
    public static final String FIELD_TYPE_VALIDATION = "validation";
    public static final String ALGORITHM_BIT = "Bit";
    public static final String ALGORITHM_STRING = "String";
    public static final String ALGORITHM_NUMBER = "Number";
    public static final String ALGORITHM_TIMESTAMP = "Timestamp";
    public static final String ALGORITHM_TIME = "Time";
    public static final String ALGORITHM_DOTNET_TICKS = "DotNetTicks";
    public static final String PATTERN_REVERSE_BYTE = "ReverseByte";
    public static final String PATTERN_REVERSE_BIT = "ReverseBit";
    public static final String PATTERN_BYTE_STRING = "ByteString";
    public static final String PATTERN_REVERSE_BYTE_STRING = "ReverseByteString";
    // interface files data loader - properties
    // LOADER_FILES_BATCH_SIZE: How  many files we process at a time
    public static final String LOADER_FILES_BATCH_SIZE = "loader.files.batch.size";
    // LOADER_INSERT_BATCH_SIZE: How many inserts are sent to the DB at a time
    public static final String LOADER_INSERT_BATCH_SIZE = "loader.insert.batch.size";
    public static final String LOADER_STATUS_INITIAL = "loader.status.initial";
    public static final String LOADER_STATUS_SUCCESS = "loader.status.success";
    public static final String LOADER_STATUS_ERROR = "loader.status.error";
    // interface files data loader - columns
    public static final String ID = "ID";
    public static final String FILE_TYPE_ID = "FileTypeID";
    public static final String PARENT_FILE_ID = "ParentFileID";
    public static final String FILE_NAME = "Filename";
    public static final String NAME = "Name";
    public static final String IMPORT_TIMESTAMP = "ImportTimestamp";
    public static final String STATUS = "Status";
    public static final String FILE_NAME_FIELD_SEPARATOR = "FilenameFieldSeparator";
    public static final String FILE_DATA_TYPE = "FileDataType";
    public static final String FIELD_SEPARATOR = "FieldSeparator";
    public static final String TEXT_QUALIFIER = "TextQualifier";
    public static final String PROCESSOR = "Processor";
    public static final String STORED_PROCEDURE_NAME = "storedProcedureName";
    public static final String HEADER_ROWS_TO_SKIP = "headerRowsToSkip";
    public static final String USE_BATCH = "UseBatch";
    public static final String CHANNEL_ID = "ChannelID";
    public static final String FIELD_TYPE = "FieldType";
    public static final String FIELD_POSITION = "FieldPosition";
    public static final String ADDRESS_BYTE = "AddressByte";
    public static final String ADDRESS_BYTE_CONDITION_FIELD_ID = "AddressByteConditionFieldID";
    public static final String ADDRESS_BYTE_CONDITION_OPERATOR = "AddressByteConditionOperator";
    public static final String ADDRESS_BYTE_CONDITION_VALUE = "AddressByteConditionValue";
    public static final String ADDRESS_BYTE_FACTOR = "AddressByteFactor";
    public static final String ADDRESS_BIT = "AddressBit";
    public static final String ADDRESS_LENGTH = "AddressLength";
    public static final String SCALING_GAIN = "ScalingGain";
    public static final String SCALING_OFFSET = "ScalingOffset";
    public static final String SCALING_INVERSION = "ScalingInversion";
    public static final String SOURCE = "Source";
    public static final String PROCESSING_ALGORITHM = "ProcessingAlgorithm";
    public static final String PROCESSING_PATTERN = "ProcessingPattern";
    public static final String IDENTIFICATION_TAG = "IdentificationTag";
    public static final String NOTE = "Note";
    public static final String DATA = "Data";
    public static final String CONDITION_FIELD_ID = "ConditionFieldID";
    public static final String CONDITION_OPERATOR = "ConditionOperator";
    public static final String CONDITION_VALUE = "ConditionValue";
    public static final String LOOKUP_INSERT = "LookupInsert";
    public static final String DEPENDENCE_FIELD_ID = "DependenceFieldID";

    // interface files data loader - queries
    public static final String LOAD_LOOKUP_QUERY = "loadLookupQuery";
    public static final String INTERFACE_FILE_BY_STATUS_QUERY = "interfaceFileByStatusQuery";
    public static final String INTERFACE_FILE_DATA_BY_PARENT_ID_QUERY = "interfaceFileDataByParentIdQuery";
    public static final String INTERFACE_FILE_CHANGE_STATUS_QUERY = "interfaceFileChangeStatusQuery";
    public static final String FILE_FIELD_DEFINITION_BY_FILE_TYPE_ID_QUERY = "fileFieldDefinitionByFileTypeIdQuery";
    // interface files data loader - tables
    public static final String FILE_TYPE_TABLE = "FileType";
    public static final String INTERFACE_FILE_TABLE = "InterfaceFile";

    // NCU config
    public static final String UPDATE_NCU_CONFIG_QUERY = "updateNCUConfig";
    public static final String NCU_BY_SERIAL_NO = "NcuBySerialNo";
    public static final String NCU_MODULE_CONFIG_BY_MODULE_CONFIG_ID = "NcuModuleConfigByModuleConfigId";
    public static final String NCU_CHANNEL_CONFIG_BY_NCU_TYPE_ID = "NcuChannelConfigByNcuTypeId";
    
    // AWS
    public static final String AWS_REGION = "AWS_REGION";
    
    // Database config
    public static final String DATABASE_NAMES = "DatabaseNames";
    public static final String UPDATE_CHANNEL_VALUE_AND_FAULT= "updateChannelValueAndFault";
}
