package com.nexala.spectrum.utils;

import java.util.Calendar;
import java.util.SimpleTimeZone;
import java.util.TimeZone;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.SpectrumProperties;

/**
 * An utility class for dates.
 * @author brice
 *
 */
@Singleton
public class DateUtils {
    
    @Inject
    private SpectrumProperties properties;
    
    @Inject
    private DateUtils() {

    }
    
    /**
     * Return the database timezone.
     * @return the database timezone
     */
    public TimeZone getDatabaseTimeZone(){
        return TimeZone.getTimeZone(properties
                .get(Spectrum.DB_TIMEZONE));
    }
    
    
    /**
     * Return a calendar with the database timezone.
     * @return a calendar with the database timezone.
     */
    public Calendar getDatabaseCalendar() {
        return Calendar.getInstance(getDatabaseTimeZone());
    }
    
    /**
     * Return the browser timezone. <br>
     * 
     * @param timezone the name of the timezone
     * @return the browser timezone, or the server timezone if null is passed as parameter
     */
    public static TimeZone getClientTimeZone(String timezone) {
        timezone = timezone.replaceAll("-", "/");
        TimeZone tz = null;
        if (timezone != null && !timezone.isEmpty()) {
            try {
                tz = TimeZone.getTimeZone(timezone);
            } catch (Exception e) {
                tz = Calendar.getInstance().getTimeZone();
            }
        } else {
            tz = Calendar.getInstance().getTimeZone();
        }

        return tz;
    }
    
    /**
     * This method doesn't handle summer time! 
     * Should use jstz.js on the client side to get the timezone
     * Return the browser timezone. <br>
     * The parameter is the difference between UTC time and local time, in minutes
     * and can be retrieve in javascript with <code>new Date().getTimezoneOffset()</code>
     * 
     * @param timezone the difference between UTC time and local time, in minutes
     * @return the browser timezone, or the server timezone if null is passed as parameter
     */
    @Deprecated
    public static TimeZone getClientTimeZone(Integer timezone) {
        TimeZone tz = null;
        if (timezone != null) {
            tz = new SimpleTimeZone(-timezone*60000, "clientTimeZone");
        } else {
            tz = Calendar.getInstance().getTimeZone();
        }
        
        return tz;
    }

}
