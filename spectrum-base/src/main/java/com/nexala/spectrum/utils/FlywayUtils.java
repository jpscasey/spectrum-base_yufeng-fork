package com.nexala.spectrum.utils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.flywaydb.core.Flyway;
import org.flywaydb.core.api.MigrationInfoService;
import org.flywaydb.core.api.MigrationVersion;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigException;
import com.typesafe.config.ConfigFactory;
import com.typesafe.config.ConfigValue;

public class FlywayUtils {
    
    private static final Logger LOGGER = Logger.getLogger(FlywayUtils.class);
    
    public static void doFlywayMigration(String dataSourceString, String fleetId) {
        final Config conf = ConfigFactory.load("config/r2m-flyway");
        
        try {
            Context ctx = new InitialContext();
            DataSource dataSource = (DataSource) ctx.lookup(dataSourceString);
            if (dataSource != null) {
                Flyway flyway = new Flyway();
                flyway.setDataSource(dataSource);
                
                
                // check if we need to baseline this database
                MigrationInfoService info = flyway.info();
                if (info.all() == null || info.all().length == 0 || info.current().getVersion().equals(MigrationVersion.EMPTY)) {
                    LOGGER.warn("No baseline found, attempt to baseline this db.");
                    doFlywayBaseline(dataSourceString, fleetId);
                }
                
                List<String> locations = conf.getStringList("r2m.flyway." + fleetId + ".locations");
                flyway.setLocations(locations.toArray(new String[locations.size()]));
                
                // read placeholder values from the config file
                if (conf.hasPath("r2m.flyway.placeholders")) {
                    Config placeholdersConfig = conf.getConfig("r2m.flyway.placeholders");
                    Map<String, String> placeholders = new HashMap<String, String>();
                    for (Entry<String, ConfigValue> key : placeholdersConfig.entrySet()) {
                        placeholders.put(key.getKey(), String.valueOf(key.getValue().unwrapped()));
                        LOGGER.info("Flyway placeholders: " + key.getKey() + " - " + String.valueOf(key.getValue().unwrapped()));
                    }
                    flyway.setPlaceholders(placeholders);
                }
                
                // enable OutOfOrder if the config says to
                if (conf.hasPath("r2m.flyway.outOfOrder") && conf.getBoolean("r2m.flyway.outOfOrder")) {
                    flyway.setOutOfOrder(true);
                }
                
                flyway.migrate();
            }
        } catch (NamingException e) {
            throw new RuntimeException(e);
        }
    }
    
    public static void doFlywayBaseline(String dataSourceString, String fleetId) {        
        final Config conf = ConfigFactory.load("config/r2m-flyway");
        
        String baseline = "1";
        try {
            baseline = conf.getString("r2m.flyway." + fleetId + ".baseline");
        } catch (ConfigException ce) {
            LOGGER.warn("no baseline found for " + fleetId + ", using default of 1");
        }

        try {
            Context ctx = new InitialContext();
            DataSource dataSource = (DataSource) ctx.lookup(dataSourceString);
            if (dataSource != null) {
                Flyway flyway = new Flyway();
                flyway.setDataSource(dataSource);
                
                flyway.setBaselineVersionAsString(baseline);
                
                flyway.baseline();
            }
        } catch (NamingException e) {
            throw new RuntimeException(e);
        }
    }
}
