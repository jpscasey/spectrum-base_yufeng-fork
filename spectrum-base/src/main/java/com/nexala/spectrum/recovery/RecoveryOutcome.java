package com.nexala.spectrum.recovery;

public class RecoveryOutcome extends RecoveryVertex {
	
	private Object outcomeId;
	private String name;
	private String action;
	private Integer severity;
	
	public RecoveryOutcome(Object outcomeId, String name, String action, Integer severity) {
		this.outcomeId = outcomeId;
		this.name = name;
		this.action = action;
		this.severity = severity;
	}
	
	public Object getOutcomeId() {
		return outcomeId;
	}
	public void setOutcomeId(Object outcomeId) {
		this.outcomeId = outcomeId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	
	public Integer getSeverity() {
		return this.severity;
	}
	
	public String toString() {
		return new String("Outcome id=" + outcomeId + ", name=" + name + ", action=" + action);
	}
}
