/*
 * Copyright (c) Nexala Technologies 2011, All rights reserved.
 */
package com.nexala.spectrum.recovery.workflow;

import org.apache.log4j.Logger;

import com.nexala.spectrum.recovery.RecoveryOutcome;
import com.nexala.workflow.WorkflowRequest;

/**
 * Outcome Activity for Recovery Process
 * 
 * @author Marcus O'Connell
 */
public class OutcomeActivity extends Activity { 

	private static final long serialVersionUID = -4999959263256932254L;
	
	private RecoveryOutcome outcomeConfig = null;
	private static final Logger logger = Logger.getLogger(OutcomeActivity.class.getName());
	
	public void setOutcomeConfig(RecoveryOutcome outcomeConfig) {
		this.outcomeConfig = outcomeConfig;
	}
	
	public String getName() throws Exception {
		return outcomeConfig.getName();
	}

	public RecoveryOutcome getOutcomeConfig() {
	    return this.outcomeConfig;
	}
	
	public void execute(WorkflowRequest request) throws Exception {
		
		logger.info("Executing: " + outcomeConfig);
		// Do nothing for now
		//request.complete();
	}
}
