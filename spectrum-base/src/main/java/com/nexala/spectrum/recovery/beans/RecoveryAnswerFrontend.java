package com.nexala.spectrum.recovery.beans;

public class RecoveryAnswerFrontend {
    private String answerId;
    private String answer;
    private boolean hasDataCheck;
    private String dataCheck;
    private String action;
    private String actionUrl; // dummy so that DWR doesn't blow up

    public RecoveryAnswerFrontend() {
    }

    public String getAnswerId() {
        return answerId;
    }

    public void setAnswerId(String answerId) {
        this.answerId = answerId;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public boolean isHasDataCheck() {
        return hasDataCheck;
    }

    public void setHasDataCheck(boolean hasDataCheck) {
        this.hasDataCheck = hasDataCheck;
    }

    public String getDataCheck() {
        return this.dataCheck;
    }

    public void setDataCheck(String dataCheck) {
        this.dataCheck = dataCheck;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getActionUrl() {
        return actionUrl;
    }

    public void setActionUrl(String actionUrl) {
        this.actionUrl = actionUrl;
    }
}
