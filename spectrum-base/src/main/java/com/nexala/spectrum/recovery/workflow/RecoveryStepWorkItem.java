package com.nexala.spectrum.recovery.workflow;

public class RecoveryStepWorkItem {
	
	private Object stepId;
	private Object answerId;
	private boolean worked;
	private String notes;
	
	
	public RecoveryStepWorkItem(Object stepId, Object answerId, boolean worked, String notes) {
		this.stepId = stepId;
		this.answerId = answerId;
		this.worked = worked;
		this.notes = notes;
	}

	public Object getStepId() {
		return stepId;
	}
	public void setStepId(Object stepId) {
		this.stepId = stepId;
	}
	public Object getAnswerId() {
		return answerId;
	}
	public void setAnswerId(Object answerId) {
		this.answerId = answerId;
	}
	public boolean isWorked() {
		return worked;
	}
	public void setWorked(boolean worked) {
		this.worked = worked;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	
	
}
