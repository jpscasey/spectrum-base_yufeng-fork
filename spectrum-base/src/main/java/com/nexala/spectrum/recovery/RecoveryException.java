package com.nexala.spectrum.recovery;

public class RecoveryException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public RecoveryException(String message) {
		super(message);
	}
	
	public RecoveryException(Throwable t) {
		super(t);
	}
}
