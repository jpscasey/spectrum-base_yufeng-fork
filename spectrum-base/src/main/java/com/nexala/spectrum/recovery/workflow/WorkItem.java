package com.nexala.spectrum.recovery.workflow;

public class WorkItem {
	
	private Object activityId;
	private Object answerId;
	private boolean worked;
	private String notes;
	
	/**
	 * @return the activityId
	 */
	public Object getActivityId() {
		return activityId;
	}
	
	/**
	 * @param activityId the activityId to set
	 */
	public void setActivityId(Object activityId) {
		this.activityId = activityId;
	}
	
	/**
	 * @return the answerId
	 */
	public Object getAnswerId() {
		return answerId;
	}
	
	/**
	 * @param answerId the answerId to set
	 */
	public void setAnswerId(Object answerId) {
		this.answerId = answerId;
	}
	
	public boolean getWorked() {
		return worked;
	}
	
	/**
	 * @param worked the worked to set
	 */
	public void setWorked(boolean worked) {
		this.worked = worked;
	}
	
	/**
	 * @return the notes
	 */
	public String getNotes() {
		return notes;
	}
	
	/**
	 * @param notes the notes to set
	 */
	public void setNotes(String notes) {
		this.notes = notes;
	}
}
