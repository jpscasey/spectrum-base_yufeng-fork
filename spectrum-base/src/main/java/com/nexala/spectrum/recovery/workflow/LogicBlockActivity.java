/*
 * Copyright (c) Nexala Technologies 2011, All rights reserved.
 */
package com.nexala.spectrum.recovery.workflow;

import org.apache.log4j.Logger;

import com.nexala.spectrum.recovery.RecoveryStep;
import com.nexala.workflow.WorkflowRequest;

/**
 * Recovery Logic Block Activity
 * 
 * @author Marcus O'Connell
 */
public class LogicBlockActivity extends Activity {

	
	private RecoveryStep stepConfig;
	private Logger logger = Logger.getLogger(LogicBlockActivity.class.getName());
	
	public void setRecoveryStepConfig(RecoveryStep stepConfig) {
		this.stepConfig = stepConfig;
	}
	
	public RecoveryStep getRecoveryStepConfig() {
		return this.stepConfig;
	}
	
	public String getName() throws Exception {
		return stepConfig.getName();
	}
	
	public void execute(WorkflowRequest request) throws Exception {
		
		logger.info("Executing: " + stepConfig);
		// Do nothing for now
		//request.complete();
		
	}
}
