package com.nexala.spectrum.recovery.workflow;

/**
 * Base Class for Workflow Activity
 * 
 * @author Marcus O'Connell
 *
 */
public class Activity {

	private boolean isStarting;
	private boolean isEnding;
	private Object activityId;
	
	public void setStart(boolean start) {
		this.isStarting = start;
	}

	public void setEnd(boolean end) {
		this.isEnding = end;
	}
	
	public boolean isStarting() {
		return this.isStarting;
	}
	
	public boolean isEnding() {
		return this.isEnding;
	}

	/**
	 * @return the activityId
	 */
	public Object getActivityId() {
		return activityId;
	}

	/**
	 * @param activityId the activityId to set
	 */
	public void setActivityId(Object activityId) {
		this.activityId = activityId;
	}
}
