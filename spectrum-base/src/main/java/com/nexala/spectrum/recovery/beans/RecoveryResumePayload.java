package com.nexala.spectrum.recovery.beans;

import java.util.List;

import com.nexala.spectrum.recovery.workflow.Activity;
import com.nexala.spectrum.recovery.workflow.WorkItem;

public class RecoveryResumePayload {
    private List<WorkItem> workItems;
    private List<Activity> activities;

    public RecoveryResumePayload() {

    }

    public List<WorkItem> getWorkItems() {
        return workItems;
    }

    public void setWorkItems(List<WorkItem> workItems) {
        this.workItems = workItems;
    }

    public List<Activity> getActivities() {
        return activities;
    }

    public void setActivities(List<Activity> activities) {
        this.activities = activities;
    }
}
