package com.nexala.spectrum.recovery.workflow;

import com.nexala.spectrum.recovery.RecoveryException;

public class Transition {

	Predicate p = null;

	/**
	 * @return the p
	 */
	public Predicate getPredicate() {
		return p;
	}

	/**
	 * @param p the p to set
	 */
	public void setPredicate(Predicate p) {
		this.p = p;
	}
	
	public boolean test(RecoveryContext context) throws RecoveryException {
		return p.test(context);
	}
	
	
}
