package com.nexala.spectrum.recovery;

public class RecoveryCheck {

    private Object activityId;
    private Object answerId;
    private boolean worked;

    public RecoveryCheck(Object activityId, Object answerId, boolean worked) {
        this.activityId = activityId;
        this.answerId = answerId;
        this.worked = worked;
    }

    public Object getActivityId() {
        return this.activityId;
    }

    public Object getAnswerId() {
        return answerId;
    }

    public void setAnswerId(Object answerId) {
        this.answerId = answerId;
    }

    public boolean isWorked() {
        return worked;
    }

    public void setWorked(boolean worked) {
        this.worked = worked;
    }

    public String toString() {
        return new String("RecoveryCheck answer: " + answerId + ", worked: "
                + worked);
    }
}
