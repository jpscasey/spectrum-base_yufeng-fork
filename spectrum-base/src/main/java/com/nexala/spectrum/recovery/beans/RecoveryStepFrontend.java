package com.nexala.spectrum.recovery.beans;

import java.util.List;

public class RecoveryStepFrontend {
    public final static int STEP_TYPE_OUTCOME = 1;
    public final static int STEP_TYPE_LOGIC_BLOCK = 2;

    private String stepId;
    private String question;
    private String questionUrl;
    private String outcomeAction;
    private String name;
    private List<RecoveryAnswerFrontend> answers;
    private RecoveryAnswerFrontend selectedAnswer;
    private int type;

    public RecoveryStepFrontend() {
    }

    public String getStepId() {
        return stepId;
    }

    public void setStepId(String stepId) {
        this.stepId = stepId;
    }

    public String getQuestion() {
        return question;
    }

    public String getQuestionUrl() {
        return questionUrl;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public void setQuestionUrl(String questionUrl) {
        this.questionUrl = questionUrl;
    }

    public List<RecoveryAnswerFrontend> getAnswers() {
        return answers;
    }

    public void setAnswers(List<RecoveryAnswerFrontend> answers) {
        this.answers = answers;
    }

    public RecoveryAnswerFrontend getSelectedAnswer() {
        return this.selectedAnswer;
    }

    public void setSelectedAnswer(RecoveryAnswerFrontend selectedAnswer) {
        this.selectedAnswer = selectedAnswer;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getOutcomeAction() {
        return outcomeAction;
    }

    public void setOutcomeAction(String outcomeAction) {
        this.outcomeAction = outcomeAction;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
