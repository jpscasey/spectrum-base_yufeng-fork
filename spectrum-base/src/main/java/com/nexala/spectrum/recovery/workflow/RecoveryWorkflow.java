package com.nexala.spectrum.recovery.workflow;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import com.nexala.spectrum.recovery.RecoveryException;
import com.nexala.spectrum.recovery.workflow.model.Edge;
import com.nexala.spectrum.recovery.workflow.model.Graph;
import com.nexala.spectrum.recovery.workflow.model.Vertex;


/**
 * Represents an individual executing recovery instance
 * 
 * @author Marcus O'Connell
 *
 */
public class RecoveryWorkflow {
	
	
	private String workflowName;
	private Object workflowId;

	
	// the recovery process definition
	Graph data = new Graph();
	
	// The context for this executing instance
	private RecoveryContext context = new RecoveryContext();
	
	/**
	 * @return the workflowName
	 */
	public String getName() {
		return workflowName;
	}

	/**
	 * @param workflowName the workflowName to set
	 */
	public void setName(String workflowName) {
		this.workflowName = workflowName;
	}

	/**
	 * @return the workflowId
	 */
	public Object getWorkflowId() {
		return workflowId;
	}

	/**
	 * @param workflowId the workflowId to set
	 */
	public void setWorkflowId(Object workflowId) {
		this.workflowId = workflowId;
	}

	/**
	 * Get a handle to the execution context
	 * @return
	 */
	public RecoveryContext getContext() {
		return this.context;
	}
	
	/**
	 * Starts the current workflow instance
	 * @param username
	 * @throws RecoveryException
	 */
	public void execute(String username) throws RecoveryException {
		if(getContext().isStarted() == false) {
			//getContext().startActivity(getStartingActivity().getActivityId());
			getContext().setStarted(true);
		}
	}
	
	/**
	 * Complete the currently executing activity and progress the workflow
	 */
	public void completeActivity(Object aId, WorkItem wItem) {
		getContext().completeActivity(aId, wItem);
	}
	
	/**
	 * Get the next activity to execute
	 */
	public Activity getNextActivity() throws RecoveryException {
		
		Object lastCompleted = getContext().getLastActivityCompleted();
		if(lastCompleted == null) {
			Activity startAct = getStartingActivity();
			getContext().startActivity(startAct.getActivityId());
			return startAct;
		}
		
		else {
			List<Edge> outbound = getGraph().outEdges(lastCompleted);

			for(Edge edge: outbound) {
				Transition t = (Transition)edge.getData();
				if( t.test(getContext()) ) {
					Vertex v = edge.getDestination();
					Activity a = (Activity)v.getData();
					getContext().startActivity(a.getActivityId());
					return a;
				}
			}
		}

		return null;
		
	}
	
	/**
	 * Gets the starting activity for the workflow. The workflow definition should only have
	 * one starting activity. If there are more than one, only the first will be returned.
	 * @return
	 * @throws RecoveryException
	 */
	public Activity getStartingActivity() throws RecoveryException {
		
		Collection<Vertex> allActs = getGraph().getVertices();
		for(Vertex vertex : allActs) {
			if(vertex.isStarting()) {
				return (Activity)(vertex.getData());
			}
		}
		return null;
	}
	
	/**
	 * Reverts the execution to the previously executed activity
	 * @param aId
	 */
	public void revertToActivity(Object aId) {
		getContext().revertToActivity(aId);
	}
	
	/**
	 * Find an Activity by its identifier
	 * 
	 * @param activityId
	 * @return
	 * @throws RecoveryException
	 */
	public Activity getActivityById(Object activityId)
			throws RecoveryException {
		Collection<Vertex> vertices = getGraph().getVertices();
		
		for(Vertex vertex:vertices) {
			if(vertex.getData() instanceof Activity &&
					((Activity)vertex.getData()).getActivityId()
					.equals(activityId)) {
				return (Activity)vertex.getData();
			}
		}
		
		return null;
	}
	
	/**
	 * Is the workflow still executing
	 * @return
	 */
	public boolean isCompleted() {
		return false;
	}
	
	/**
	 * Returns the id of the currently executing activity
	 * @return
	 */
	public Object getCurrentlyExecutingActivity() {
		return getContext().getCurrentlyExecuting();
	}
	
	/**
	 * Add the specified activity to the workflow definition
	 * @param id
	 * @param a
	 */
	public void addActivity(Object id, Activity a) {
		getGraph().addVertex(id, a);
		getGraph().findVertex(id).setStarting(a.isStarting());
		getGraph().findVertex(id).setEnding(a.isEnding());		
	}
	
	/**
	 * Get a reference to the activity defined by the specified activity id
	 * @param aId
	 * @return
	 */
	public Activity findActivity(Object aId) {
		
		for(Vertex vertex : getGraph().vertices()) {
			if( ((Activity)vertex.getData()).getActivityId().equals(aId) ) {
				return ((Activity)vertex.getData());
			}
		}
		
		return null;
	}
	
	/**
	 * Add the specified transition to the workflow definition
	 * @param fromId
	 * @param toId
	 * @param id
	 * @param t
	 */
	public void addTransition(Object fromId, Object toId, Object id, Transition t) {
		getGraph().addEdge(id, t, fromId, toId);
	}

	/**
	 * Returns the remaining activities from the currently executing activity
	 * @return
	 */
	public List<Activity> getRemainingActivities() throws RecoveryException {
		Object oId = getCurrentlyExecutingActivity();
		if(oId == null) {
			oId = getNextActivity().getActivityId();
		}
		
		Collection<Vertex> vertices = getGraph().getRemainingVertices(oId);
		
		List<Activity> activities = new LinkedList<Activity>();
		for(Vertex vertex : vertices) {
			activities.add(((Activity)vertex.getData()));
		}
		
		return activities;
	}
	
	/**
	 * Validate the workflow structure
	 * @return
	 */
	public boolean validate() {
		return true;
	}
	
	private Graph getGraph() {
		
		return this.data;
	}
}
