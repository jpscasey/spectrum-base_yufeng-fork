/*
 * Copyright (c) Nexala Technologies 2011, All rights reserved.
 */
package com.nexala.spectrum.recovery.workflow;

import com.nexala.spectrum.recovery.RecoveryCheck;
import com.nexala.spectrum.recovery.RecoveryException;



public class RecoveryPredicate implements Predicate {

	private RecoveryCheck predConfig;
	
	public void setPredicateConfig(RecoveryCheck predConfig) {
		this.predConfig = predConfig;
	}
	
	 public boolean test(RecoveryContext context) throws RecoveryException {
		 
		 Object aId = predConfig.getActivityId();
		 
		 WorkItem wItem = context.getActivityData(aId);
		 Object answerId = wItem.getAnswerId();
		 boolean worked = wItem.getWorked();
		 
        if (predConfig.getAnswerId().toString().equals(answerId.toString())
                && predConfig.isWorked() == worked) {
			 return true;
		 } else {
			 return false;
		 }
	 }
}
