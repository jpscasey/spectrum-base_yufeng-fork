package com.nexala.spectrum.recovery.workflow;

import java.util.HashMap;
import java.util.Map;

import com.nexala.workflow.WorkItem;
import com.nexala.workflow.WorkItemLocator;

public class RecoveryWorkItem  implements WorkItem {

	private Map<String, Object> wItemData = new HashMap<String, Object>();
	
	public Object getData(WorkItemLocator loc) throws Exception {
		
		return(wItemData.get(loc.toString()));
	}

	public void setData(WorkItemLocator loc, Object data) throws Exception {

		wItemData.put(loc.toString(), data);
	}

	
	public Object clone() {
		return null;
	}
	
	public void release() {
		// TODO Auto-generated method stub
		
	}
}
