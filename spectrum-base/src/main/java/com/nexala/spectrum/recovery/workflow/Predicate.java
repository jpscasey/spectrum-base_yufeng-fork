package com.nexala.spectrum.recovery.workflow;

import com.nexala.spectrum.recovery.RecoveryException;

public interface Predicate {
	
	public boolean test(RecoveryContext context) throws RecoveryException;

}
