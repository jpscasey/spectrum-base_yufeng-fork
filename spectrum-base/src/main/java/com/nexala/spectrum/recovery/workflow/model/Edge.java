package com.nexala.spectrum.recovery.workflow.model;


public class Edge {

	private Object id;
	private Object data;
	private Vertex destination;
	private Vertex origin;
	
	/**
	 * @return the id
	 */
	public Object getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Object id) {
		this.id = id;
	}
	/**
	 * @return the data
	 */
	public Object getData() {
		return data;
	}
	/**
	 * @param data the data to set
	 */
	public void setData(Object data) {
		this.data = data;
	}
	/**
	 * @return the inVertices
	 */
	public Vertex getDestination() {
		return destination;
	}
	/**
	 * @param inVertices the inVertices to set
	 */
	public void setOrigin(Vertex vertex) {
		this.origin = vertex;
	}
	/**
	 * @return the outVertices
	 */
	public Vertex getOrigin() {
		return origin;
	}
	/**
	 * @param outVertices the outVertices to set
	 */
	public void setDestination(Vertex vertex) {
		this.destination = vertex;
	}

	
	
}
