/*
 * Copyright (c) Nexala Technologies 2011, All rights reserved.
 */

package com.nexala.spectrum.recovery;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;

import com.nexala.spectrum.Spectrum;
import com.nexala.spectrum.configuration.ApplicationConfiguration;
import com.nexala.spectrum.recovery.workflow.Activity;
import com.nexala.spectrum.recovery.workflow.LogicBlockActivity;
import com.nexala.spectrum.recovery.workflow.OutcomeActivity;
import com.nexala.spectrum.recovery.workflow.RecoveryPredicate;
import com.nexala.spectrum.recovery.workflow.RecoveryWorkflow;
import com.nexala.spectrum.recovery.workflow.Transition;
import com.nexala.spectrum.recovery.workflow.WorkItem;
import com.trimble.rail.security.client.HttpClient;

/**
 * RecoveryManager
 * TODO doc
 * @author Marcus O'Connell
 */
public class RecoveryManager {
	
	private ApplicationConfiguration appConf;
	private static RecoveryManager instance = null;
	private static final Logger logger = Logger.getLogger(RecoveryManager.class);
	private Map<Object, RecoveryWorkflow> processes = new HashMap<Object, RecoveryWorkflow>();
	private Map<Object, String> workflowStrings = new HashMap<Object, String>();
	private Map<Object, Object> temporaryContextIds = new HashMap<Object, Object>(); // CID -> Temp CID mappings for process resumptions
	
	/**
	 * Gets a reference to singleton instance. The constructor used to create
	 * an instance (if required) will read the recovery service URL from 
	 * <pre>recoveryservice.properties</pre> on the thread context classpath.
	 * 
	 * @return RecoveryManager instance
	 */
    public static synchronized RecoveryManager instance(ApplicationConfiguration appConf) {
		if(instance == null) {
            instance = new RecoveryManager(appConf);
		}
		return instance;
	}
	
    private RecoveryManager(ApplicationConfiguration appConf) {
        logger.info("Creating new RecoveryManager instance");

        this.appConf = appConf;
    }
	
	/**
	 * Starts a new process instance.
	 * @param processId Unique identifier for the process. This is the path / name of the process definition in the JCL repository
	 * @param username The username of the person that has started the process
	 * @return unique instance Id (workflow context ID)
	 */
	public Object executeProcess(String processId, String username, HttpClient httpClient) {
	    logger.debug("Executing process [" + processId + "] for user: "  +username);

	    String server = appConf.get().getConfig("r2m").getString("r2m-url");
        
        logger.debug("Loading from repository");
        String xml = null;
        HttpURLConnection conn = null;
        try {
        	String encodedProcessId = URLEncoder.encode(processId, "UTF-8");
            URL serviceUrl = new URL(server + Spectrum.RECOVERY_DEFAULT_URL_PATH + "?" + Spectrum.RECOVERY_DEFAULT_URL_QUERY + encodedProcessId);
            
            logger.debug("Requesting recovery workflow from [" + serviceUrl + "]");
            
            conn = (HttpURLConnection) serviceUrl.openConnection(); 
            conn.setRequestMethod("GET");
            conn.setDoOutput(true);
            conn.setReadTimeout(60000);
            httpClient.authenticate(conn);
            
            InputStream is = conn.getInputStream();
            BufferedReader serviceIn = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            StringBuilder xmlBuffer = new StringBuilder();
            String temp;
            while((temp = serviceIn.readLine()) != null) {
                xmlBuffer.append(temp);
            }
            
            xml = xmlBuffer.toString();
        }
        catch(IOException e) {
            logger.error("Could not load from repository:", e);
            return null;
        }
        
        if(xml == null) {
            logger.error("Unknown error loading from repository");
            return null;
        }
        
        logger.debug("Received data: " + xml);
        logger.debug("Building workflow from definition");
        
        try{
            return executeProcessFromDefn(xml, username);
        }
        catch(RecoveryException e) {
            logger.error("Could not execute process from definition:");
            logger.error(e.getMessage());
            return null;
        }
        
	}
	
	/**
	 * Start a new process instance from the provided definition
	 * @param workflow XML format workflow definition
	 * @param username The username of the person that has started the process
	 * @return unique instance Id (workflow context ID)
	 */
	public Object executeProcessFromDefn(String workflow, String username) throws RecoveryException {
		logger.debug("Executing process for user: " + username);
		RecoveryWorkflow wf = null;
		try {
			wf = getRunnableWorkflow(workflow);
			synchronized(this) {
				processes.put(wf.getWorkflowId(), wf);
				workflowStrings.put(wf.getWorkflowId(), workflow);
			}
			
			wf.execute(username);
			
		} catch(Exception e) {
			throw new RecoveryException(e);
		}
		
		logger.debug("Workflow " + wf.getName() + " with instance id [" + wf.getWorkflowId() + "] started");
		
		return wf.getWorkflowId();
	}
	
	
	/**
	 * Gets the next recovery step in the process instance
	 * @param contextId the unique instance identifier
	 * @return
	 */
	public RecoveryVertex getNextStep(Object contextId) throws RecoveryException {
		RecoveryWorkflow workflow = getWorkflow(contextId);
		Activity a =  workflow.getNextActivity();
		if(a instanceof LogicBlockActivity) {
			return ((LogicBlockActivity)a).getRecoveryStepConfig();
		}
		else if(a instanceof OutcomeActivity) {
		    return ((OutcomeActivity)a).getOutcomeConfig();
		}
		
		return null;
		
	}
	
	/**
	 * Gets the currently executing Recovery Step for the specified Recovery instance. Returns
	 * null if the currently executing step is not a Logic Block.
	 * @param contextId
	 * @return
	 * @throws RecoveryException
	 */
	public RecoveryStep getCurrentlyExecuting(Object contextId) throws RecoveryException {
		RecoveryWorkflow workflow = getWorkflow(contextId);
		Object aId = workflow.getCurrentlyExecutingActivity();
		Activity a = workflow.findActivity(aId);
		
		if(a instanceof LogicBlockActivity) {
			return ((LogicBlockActivity)a).getRecoveryStepConfig();
		}
		
		return null;
	}
	
	/**
	 * Sets the outcome for a particular step
	 * @param contextId the unique process instance id
	 * @param answerId the answer id
	 * @param outcome whether the action worked or not
	 */
	public void setStepOutcome(Object contextId, Object answerId, boolean outcome, String notes)
	        throws RecoveryException {
		
		logger.debug("Setting outcome for:" + contextId + ", answerId=" + answerId + ", worked=" + outcome);
		
		RecoveryWorkflow workflow = getWorkflow(contextId);
		Object aId = workflow.getCurrentlyExecutingActivity();
		
		WorkItem wItem = new WorkItem();
		wItem.setActivityId(aId);
		wItem.setAnswerId(answerId);
		wItem.setWorked(outcome);
        wItem.setNotes(notes);
		
		workflow.completeActivity(aId, wItem);

	}
	
	/**
	 * Determine whether or not a process is ready for completion (i.e. whether
	 * or not the process has reached an outcome).
	 * 
	 * @param contextId				The process context identifier
	 * @return						True if ready for completion, false otherwise.
	 * @throws RecoveryException
	 */
	public boolean isReadyForCompletion(Object contextId) throws RecoveryException {
		logger.debug("Checking whether workflow " + contextId.toString() + " is ready for completion");
		
		RecoveryWorkflow workflow = getWorkflow(contextId);
		Object currentActivityId = workflow.getCurrentlyExecutingActivity();
		Activity activity = workflow.findActivity(currentActivityId);
		
		if(activity instanceof OutcomeActivity) {
			return true;
		}
		else {
			return false;
		}
	}
	
	/**
	 * Removes (deletes!) a recovery process from the recovery manager.
	 * 
	 * @param contextId				The process context identifier
	 * @throws RecoveryException
	 */
	public void removeProcess(Object contextId) throws RecoveryException {
		logger.debug("Removing recovery process " + contextId + " from manager");
		
		synchronized(this) {
			processes.remove(contextId);
			workflowStrings.remove(contextId);
		}
	}
	
	/**
	 * Returns whether or not a recovery process is active (i.e. can be
	 * resumed).
	 * 
	 * @param contextId				The process identifier.
	 * @return
	 */
	public boolean isActive(Object contextId) {
		synchronized(this) {
			return processes.containsKey(contextId);
		}
	}
	
	/**
	 * Reverts a running process instance to a previously completed recovery step.
	 * @param contextId the running instance id
	 * @param stepId the step id the process should revert to
	 */
	public void revertToStep(Object contextId, Object stepId) throws RecoveryException {
		
		RecoveryWorkflow workflow = getWorkflow(contextId);
		workflow.revertToActivity(stepId);
	}
	
	/**
	 * Records notes against a specific recovery step
	 * @param contextId the running process instance id
	 * @param stepId the process step id
	 * @param notes the text notes
	 */
	public void setRecoveryStepNotes(Object contextId, String stepId, String notes ) {
		

		//TODO: Implement
		
	}
	
	
	/**
	 * Gets the current Worst Case Scenario for the specified executing process instance
	 * @param contextId
	 * @return
	 */
	public String getWorstCaseScenario(Object contextId) throws RecoveryException {
		
		
		// default worst case severity
		Integer severity = null;
		String description = null;
		
		RecoveryWorkflow workflow = getWorkflow(contextId);
		List<Activity> activities = workflow.getRemainingActivities();
		
		for(Activity activity : activities) {
			if(activity instanceof OutcomeActivity) {
				OutcomeActivity oa = (OutcomeActivity)activity;
				RecoveryOutcome outcome = oa.getOutcomeConfig();
				if(severity == null || outcome.getSeverity() > severity) {
					severity = outcome.getSeverity();
					description = outcome.getAction();
				}
			}
		}

		return description;
	}	
	
	/**
	 * Creates a runnable workflow from an XML definition 
	 * @param workflowDefn the XML representation of the workflow
	 */
	private RecoveryWorkflow getRunnableWorkflow(String workflowDefn) throws Exception {
		
		//TODO: Add in a proper workflow Id here
		Long workflowId = System.currentTimeMillis();

		
		Document doc = new SAXBuilder().build(new StringReader(workflowDefn));
		
		
		// Create Runnable Workflow Instance
		String wfName = doc.getRootElement().getAttributeValue("name");
		RecoveryWorkflow workflow = new RecoveryWorkflow();
		workflow.setWorkflowId(workflowId);
		workflow.setName(wfName);
		
		// Create Activities
		List<Element> vertices = doc.getRootElement().getChild("vertices").getChildren("vertex");
		for(Element vertexEl: vertices) {
			
			String vertexId = vertexEl.getAttributeValue("vertex-id");
			boolean starting = new Boolean(vertexEl.getAttributeValue("starting")).booleanValue();
			boolean ending = new Boolean(vertexEl.getAttributeValue("ending")).booleanValue();
			
			Element logicBlockEl = vertexEl.getChild("activity-data").getChild("logic-block");
			if(logicBlockEl != null) {
				
				String name = logicBlockEl.getChildText("name");
				String question = logicBlockEl.getChildText("question");
				String questionUrl = logicBlockEl.getChild("question").getAttributeValue("url");
				List<Element> answersEl = logicBlockEl.getChildren("answer");
				List<RecoveryAnswer> answers = new LinkedList<RecoveryAnswer>();

				for(Element answerEl: answersEl) {
					String actionUrl;
					if(answerEl.getChild("action-text") != null) {
					    actionUrl = answerEl.getChild("action-text").getAttributeValue("url");
					}
					else {
					    actionUrl = null;
					}
					
					RecoveryAnswer ans = new RecoveryAnswer(
							answerEl.getAttributeValue("answer-id"), 
							answerEl.getChildText("answer-text"),
							false,
							answerEl.getChildText("remote-data"),
							answerEl.getChildText("action-text"),
							actionUrl
					);
					answers.add(ans);
				}
				
				// Create the Vertex & Activity Config
				LogicBlockActivity lbAct = new LogicBlockActivity();
				lbAct.setActivityId(vertexId);
				lbAct.setStart(starting);
				lbAct.setEnd(ending);
				RecoveryStep step = new RecoveryStep(vertexId, name, question, questionUrl, answers);
				lbAct.setRecoveryStepConfig(step);
				
				logger.debug("Adding Recovery Step: " + step);
				
				// Add the vertex to the graph
				//graph.insertVertex(vertexId, lbAct);
				workflow.addActivity(vertexId, lbAct);
			}
			
			Element outcomeEl = vertexEl.getChild("activity-data").getChild("outcome");
			if(outcomeEl != null) {
				
				String name = outcomeEl.getChildText("name");
				String action = outcomeEl.getChildText("outcome-action");
				Integer severity = new Integer(Integer.parseInt(outcomeEl.getChildText("severity")));
				
				// Create the Vertex and Activity Config
				RecoveryOutcome outcome = new RecoveryOutcome(vertexId, name, action, severity);
				OutcomeActivity outcomeAct = new OutcomeActivity();
				outcomeAct.setActivityId(vertexId);
				outcomeAct.setOutcomeConfig(outcome);
				outcomeAct.setStart(starting);
				outcomeAct.setEnd(ending);
				
				logger.debug("Adding Recovery Outcome: " + outcome);
	
				// Add the vertex to the graph
				//graph.insertVertex(vertexId, outcomeAct);
				workflow.addActivity(vertexId, outcomeAct);
			}
		}

		
		// Create Predicates
		List<Element> edges = doc.getRootElement().getChild("edges")
				.getChildren("edge");
		for(Element edgeEl : edges) {
		
			String edgeId = edgeEl.getAttributeValue("edge-id");
			String originId = edgeEl.getChildText("origin-vertex-id");
			String destinationId = edgeEl.getChildText("destination-vertex-id");
			
			Element predEl = edgeEl.getChild("predicate");
		
			RecoveryCheck check = new RecoveryCheck(originId, 
					predEl.getChildText("answer"),
					new Boolean(predEl.getChildText("worked")).booleanValue()
			);
			
			
			RecoveryPredicate predicate = new RecoveryPredicate();
			predicate.setPredicateConfig(check);
			
			Transition t = new Transition();
			t.setPredicate(predicate);
			
			logger.debug("Adding Recovery Predicate: Origin=" + originId
					+ ", dest=" + destinationId + ", pred=" + check);
			
			workflow.addTransition(originId, destinationId, edgeId, t);
			
		}
		
		return workflow;
	}
	
	/**
	 * Retrieves the running workflow instance
	 * @param workflowId
	 * @return
	 */
	private synchronized RecoveryWorkflow getWorkflow(Object workflowId) {
		return processes.get(workflowId);
	}
	
	/**
	 * Maps a real context ID to a temporary context ID.
	 * 
	 * @param realContextId			The real context ID
	 * @param temporaryContextId	The temporary context ID
	 */
	public synchronized void setTemporaryContextIdentifier(Object realContextId,
		Object temporaryContextId) throws RecoveryException {
		if(temporaryContextIds.containsKey(realContextId)) {
			throw new RecoveryException("A temporary context has already been associated with this real context (may have lost a thread race)");
		}
		
		temporaryContextIds.put(realContextId, temporaryContextId);
	}
	
	public synchronized Object getTemporaryContextIdentifier(Object realContextId) {
		return temporaryContextIds.get(realContextId);
	}
	
	public synchronized void removeTemporaryContextIdentifier(Object realContextId) {
		temporaryContextIds.remove(realContextId);
	}
	
	public synchronized String getWorkflowString(Object contextId) {
		return workflowStrings.get(contextId);
	}
	
	public synchronized Activity getStartingActivity(Object contextId)
			throws RecoveryException {
		return processes.get(contextId).getStartingActivity();
	}
	
	public synchronized Activity getActivityFromWorkItem(Object contextId, 
			WorkItem workItem) throws RecoveryException {
		return processes.get(contextId).
				getActivityById(workItem.getActivityId());
	}
	
	public synchronized List<WorkItem> getCompletedWorkItems(Object contextId)
			throws RecoveryException {
		return processes.get(contextId).getContext().getCompletedWorkItems();
	}
	
	public synchronized Activity getCurrentActivity(Object contextId)
			throws RecoveryException {
		return (Activity)processes.get(contextId).getActivityById(
				processes.get(contextId).getCurrentlyExecutingActivity());
	}
}

