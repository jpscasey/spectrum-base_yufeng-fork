package com.nexala.spectrum.recovery.workflow.model;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;


/**
 * Directed Graph represented by a series of vertices and edges connecting those vertices.
 * 
 * @author Marcus O'Connell
 *
 */
public class Graph {
	
	private Map<Object, Vertex> vertices;
	private Map<Object, Edge> edges;

	
	/**
	 * Constructs a new graph instance
	 */
	public Graph() {
		vertices = new LinkedHashMap<Object, Vertex>();
		edges = new LinkedHashMap<Object, Edge>();
	}
	
	/**
	 * Returns the inbound edges for the specified vertex
	 * @param vertexId
	 * @return
	 */
	public List<Edge> inEdges(Object vertexId) {
		Vertex v = vertices.get(vertexId);
		return v.getInEdges();
	}
	
	/**
	 * Returns the outbound edges for the specified vertex
	 * @param vertexId
	 * @return
	 */
	public List<Edge> outEdges(Object vertexId) {
		Vertex v = vertices.get(vertexId);
		return v.getOutEdges();
	}
	
	/**
	 * Returns all of the vertices in the graph
	 * @return
	 */
	public List<Vertex> vertices() {
		List<Vertex> v = new LinkedList<Vertex>();
		v.addAll(vertices.values());
		return v;
	}
	
	/**
	 * Returns all of the edges in the graph
	 * @return
	 */
	public List<Edge> edges() {
		List<Edge> e = new LinkedList<Edge>();
		e.addAll(edges.values());
		return e;
	}
	
	/**
	 * finds the edge for the specified edge id
	 * @param edgeId
	 * @return
	 */
	public Edge findEdge(Object edgeId) {
		return edges.get(edgeId);
	}
	
	/**
	 * Finds the vertex for the specified vertex id
	 * @param vertexId
	 * @return
	 */
	public Vertex findVertex(Object vertexId) {
		return vertices.get(vertexId);
	}
	
	/**
	 * Inserts the edge into the graph, replacing an existing edge if one exists with the same id
	 * @param edge
	 * @param originId
	 * @param destinationId
	 */
	public void insertEdge(Edge edge, Object originId, Object destinationId) {
		
		Vertex origin = findVertex(originId);
		origin.addOutEdge(edge);
		edge.setOrigin(origin);
		
		Vertex destination = findVertex(destinationId);
		destination.addInEdge(edge);
		edge.setDestination(destination);
		
		edges.put(edge.getId(), edge);
	}
	
	/**
	 * Creates a new edge and adds it to the graph
	 * @param edgeId
	 * @param data
	 * @param originId
	 * @param destinationId
	 */
	public void addEdge(Object edgeId, Object data, Object originId, Object destinationId) {
		Edge e = new Edge();
		e.setId(edgeId);
		e.setData(data);
		insertEdge(e, originId, destinationId);
	}
	
	/**
	 * Inserts the vertex into the graph
	 * @param vertex
	 */
	public void insertVertex(Vertex vertex) {

		vertices.put(vertex.getId(), vertex);
	}
	
	/**
	 * Adds a new vertex to the graph
	 * @param vertexId
	 * @param data
	 */
	public void addVertex(Object vertexId, Object data) {
		Vertex v = new Vertex();
		v.setData(data);
		v.setId(vertexId);
		insertVertex(v);
	}
	
	/**
	 * Returns all of the vertices in the graph
	 * @return
	 */
	public Collection<Vertex> getVertices() {
		return vertices.values();
	}
	
	/**
	 * Returns all of the edges in te graph
	 * @return
	 */
	public Collection<Edge> getEdges() {
		return edges.values();
	}
	
	/**
	 * Returns all possible remaining ending vertices for the specified vertex
	 * @param vertexId
	 * @return
	 */
	public Collection<Vertex> getRemainingVertices(Object vertexId) {
		
		List<Vertex> vertices = new LinkedList<Vertex>();
		
		List<Edge> outEdges = outEdges(vertexId);
		for(Edge edge : outEdges) {
			Vertex v = edge.getDestination();
			if(v.isEnding()) {
				vertices.add(v);
			} else {
				vertices.addAll(getRemainingVertices(edge.getDestination().getId()));
			}
		}
		
		return vertices;
	}
}
