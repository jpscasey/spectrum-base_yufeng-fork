package com.nexala.spectrum.recovery.workflow;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;

/**
 * Stores runtime context data for a recovery workflow process
 * 
 * @author Marcus O'Connell
 *
 */
public class RecoveryContext {

	private LinkedHashMap<Object, WorkItem> completedActivities = null;
	private Object currentlyExecuting = null;
	private Object lastCompleted = null;
	private boolean started;
	
	public RecoveryContext() {
	
		completedActivities = new LinkedHashMap<Object, WorkItem>();
	}
	
	/**
	 * Get the activity id of the last completed activity
	 * @return
	 */
	public Object getLastActivityCompleted() {
		
		return lastCompleted;
	}
	
	/**
	 * Complete the currently executing activity.
	 * @param aId
	 * @param wItem
	 */
	public void completeActivity(Object aId, WorkItem wItem) {
		completedActivities.put(aId, wItem);
		lastCompleted = aId;
		currentlyExecuting = null;
	}
	
	/**
	 * Get the execution data for the specified activity
	 * @param aId
	 * @return
	 */
	public WorkItem getActivityData(Object aId) {
		return completedActivities.get(aId);
	}
	
	/**
	 * Get the id of the currently executing activity
	 * @return
	 */
	public Object getCurrentlyExecuting() {
		return this.currentlyExecuting;
	}
	
	/**
	 * Start the activity execution
	 * @param aId
	 */
	public void startActivity(Object aId) {
		this.currentlyExecuting = aId;
	}
	
	/**
	 * TODO: Review, as it reverts to first instance of specified activity, if the activity is actually
	 * referenced in the workflow twice. 
	 * @param aId
	 */
	public void revertToActivity(Object aId) {
		
		LinkedHashMap<Object, WorkItem> newList = new LinkedHashMap<Object, WorkItem>();
		
		Set<Object> completed = completedActivities.keySet();
		for(Object activityId: completed) {
			if(! aId.equals(activityId)) {
				lastCompleted = activityId;
				newList.put(activityId, completedActivities.get(activityId));
			} else {
				completedActivities = newList;
				currentlyExecuting = aId;
				return;
			}
		}
	}

	/**
	 * @return the started
	 */
	public boolean isStarted() {
		return started;
	}

	/**
	 * @param started the started to set
	 */
	public void setStarted(boolean started) {
		this.started = started;
	}
	
	
	/**
	 * Returns an ordered list of workitems that have been completed, in order
	 * of completion.
	 * 
	 * @return		Ordered list of work items.
	 */
	public List<WorkItem> getCompletedWorkItems() {
		List<WorkItem> workItems = new ArrayList<WorkItem>();
		
		for(Object key:completedActivities.keySet()) {
			workItems.add(completedActivities.get(key));
		}
		
		return workItems;
	}
}
