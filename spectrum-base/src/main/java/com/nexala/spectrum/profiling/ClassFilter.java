/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.profiling;

public class ClassFilter {

    private Class<?> filter;
    
    public ClassFilter(Class<?> filter) {
        if (filter == null) {
            throw new NullPointerException("Class may not be null");
        }
        
        this.filter = filter;
    }
    
    public final boolean matches(Class<?> someClass) {
        if (filter.isInterface()) {
            return implementsInterface(someClass);
        } else {
            return equalsClass(someClass);
        }
    }
    
    private final boolean equalsClass(Class<?> someClass) {
        return filter == someClass;
    }
    
    /**
     * Check if the class (or one of its super classes) implements the filter
     * interface.
     * 
     * @param someClass
     * @return
     */
    private final boolean implementsInterface(Class<?> someClass) {
        Class<?>[] interfaces = someClass.getInterfaces();
        
        for (Class<?> clazz : interfaces) {
            if (filter == clazz) {
                return true;
            }
        }
        
        if (someClass.getSuperclass() != null) {
            return implementsInterface(someClass.getSuperclass());
        }
        
        return false;
    }
}