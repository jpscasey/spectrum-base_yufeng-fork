/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.profiling;

public class ClassDetail extends ReportDetail {
    protected final String className;

    public ClassDetail(String className, long time) {
        super(time);
        this.className = className;
    }

    public String getClassName() {
        return className;
    }
    
    public String toString() {
        return String.format("[ClassDetail(className=%s, timeMs=%d)]",
                className, time);
    }
}