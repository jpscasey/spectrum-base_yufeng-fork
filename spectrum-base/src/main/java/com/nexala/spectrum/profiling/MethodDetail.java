/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.profiling;

public class MethodDetail extends ClassDetail {

    private final String methodName;

    public MethodDetail(String className, String methodName, long time) {
        super(className, time);
        this.methodName = methodName;
    }

    public String getMethodName() {
        return methodName;
    }
    
    @Override
    public String toString() {
        return String.format(
                "[MethodDetail(className=%s, methodName=%s, timeMs=%d)]",
                className, methodName, time);
    }
}