/*
 * Copyright (c) Nexala Technologies 2012, All rights reserved.
 */
package com.nexala.spectrum.profiling;

import java.lang.Thread.State;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This is a statistical Profiler. It works by probing the target thread(s) at regular intervals and using a stack trace
 * to find out which method is currently executing; it is then assumed that all time since the previous "sample" has
 * been spent in this method. This information is used to generate a report containing the execution times broken down
 * by class / method. This class is Thread Safe.
 * </p>
 * 
 * <p>
 * Note that these execution times are a statistical approximation and are not exact, but they should be good enough for
 * most purposes
 * </p>
 * 
 * <p>
 * Also note that because Java stack traces contain minimal information (classname, methodname and line number),
 * overloaded methods will be returned as a single result, i.e. if there are two methods {@code exec(int)} and
 * {@code exec(int, int)}, with execution times of 100ms and 200ms respectively they will be returned as a single result
 * "exec" with an execution time of 300ms. It may be possible to workaround this problem, using the ASM bytecode library
 * </p>
 * 
 * @See http://en.wikipedia.org/wiki/Performance_analysis
 * 
 * @author poriordan
 */
public class Profiler {

    /**
     * This class is responsible for probing the target thread(s) at regular intervals, grabbing stack traces and
     * updating the report.
     * 
     * @author poriordan
     */
    private class ProfilingAgent implements Runnable {

        private final ClassFilter classFilter;

        private final Map<String, Long> lastTimes = new HashMap<String, Long>();

        private final Thread[] threads;

        ProfilingAgent(ClassFilter filter, Thread thread, Thread... otherThreads) {
            long cTime = System.currentTimeMillis();
            classFilter = filter;
            threads = new Thread[otherThreads.length + 1];
            threads[0] = thread;
            lastTimes.put(threads[0].getName(), cTime);

            for (int i = 0; i < otherThreads.length; i++) {
                threads[i + 1] = otherThreads[i];
                lastTimes.put(threads[i].getName(), cTime);
            }
        }

        private void profileThread(Thread thread) {
            long time = System.currentTimeMillis();
            // Diff between current time and last sample time for thread.
            long execTime = time - lastTimes.get(thread.getName());
            // long execTime = 1;

            StackTraceElement[] trace = thread.getStackTrace();

            if (trace.length > 0) {
                if (classFilter == null) {
                    // if (trace[0].getMethodName().equals("hashCode"))
                    // System.out.println(trace[2].getClassName() + "." + trace[2].getMethodName());

                    report.updateExecTime(trace[0], execTime);
                } else {
                    StackTraceElement match = null;

                    for (StackTraceElement el : trace) {
                        try {
                            Class<?> clazz = Class.forName(el.getClassName());

                            if (classFilter.matches(clazz)) {
                                match = el;
                                break;
                            }
                        } catch (ClassNotFoundException e) {
                            // Generated classes, sun.reflect stuff, etc. will
                            // not be found. Just skip over these.
                        }
                    }

                    // If this stack trace contains our filter class; update
                    // the execTime for the matched element.
                    if (match != null) {
                        report.updateExecTime(match, execTime);
                    }
                }
            }

            lastTimes.put(thread.getName(), time);
        }

        public final void run() {
            agentStopped = false;

            while (running) {

                // long sTime = System.currentTimeMillis();

                for (Thread thread : threads) {
                    if (thread.getState() == State.RUNNABLE) {
                        profileThread(thread);
                    } else {
                        lastTimes.put(thread.getName(), System.currentTimeMillis());
                    }
                }

                try {
                    Thread.sleep(samplingInterval);
                } catch (InterruptedException e) {
                    Thread.interrupted();
                }
            }

            agentStopped = true;
        }
    }

    /** The profiling agent thread */
    private Thread agent;

    private final Report report = new Report();

    /** Whether the profiling agent should be running or not. */
    private boolean running = false;

    /** Whether the agent has been stopped */
    private boolean agentStopped = true;

    /** How often the profiling agent should take a stack trace [milliseconds] */
    private int samplingInterval = 1;

    public Profiler(ClassFilter filter, Thread firstThread, Thread... otherThreads) {
        if (firstThread == null) {
            throw new NullPointerException("Thread may not be null");
        }

        agent = new Thread(new ProfilingAgent(filter, firstThread, otherThreads));
        agent.setDaemon(true);
    }

    public Profiler(Thread firstThread, Thread... otherThreads) {
        this(null, firstThread, otherThreads);
    }

    /**
     * Causes the ProfilingAgent [thread] to be stopped (after the current iteration), if the profiler is currently
     * running.
     */
    public synchronized Report finishProfiling() {
        if (!running) {
            throw new IllegalStateException("Profiling agent has not been started");
        }

        long cTime = System.currentTimeMillis();

        running = false;

        // After setting running to false, the Profiler blocks [max 1 second]
        // while waiting for the agent to complete.
        while (agentStopped != true && (System.currentTimeMillis() - cTime < 500)) {

            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        return report;
    }

    /**
     * Starts the ProfilingAgent [thread] if it is not already running.
     */
    public synchronized void startProfiling() {
        if (running) {
            throw new IllegalStateException("Profiling agent is already running");
        }

        running = true;
        agent.start();
    }
}
