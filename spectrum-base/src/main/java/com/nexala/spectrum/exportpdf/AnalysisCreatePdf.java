package com.nexala.spectrum.exportpdf;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletResponse;

import com.lowagie.text.DocumentException;

public class AnalysisCreatePdf {

    public static void createFile(String imageData, String dataTable,
            String type, HttpServletResponse response, String unitNumber,
            String vehicleNumber) throws IOException, DocumentException {

        PdfExportConfiguration conf = new PdfExportConfiguration();
        conf.setTitle("Data View");
        conf.setFileName("report-data-view_");
        conf.setFontSize("8px");

        StringBuilder header = new StringBuilder();
        header.append("attachment; filename=").append(conf.getFileName());
        header.append(unitNumber).append("_");
        header.append(vehicleNumber).append("_");
        Date time = new Date();
        SimpleDateFormat timeFormat = new SimpleDateFormat("yyyyMMdd_HHmmssSSS");
        String date = timeFormat.format(time);
        header.append(date).append(".pdf");
        // header.append(new Date().toString()).append(".pdf");

        AnalysisCreatePdfGeneric.createFile(imageData, dataTable, conf,
                response, header.toString(), true);
    }

}