package com.nexala.spectrum.exportpdf;

import java.io.IOException;
import java.io.OutputStream;
import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.http.HttpServletResponse;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Image;
import com.lowagie.text.Paragraph;
import com.lowagie.text.html.simpleparser.HTMLWorker;
import com.lowagie.text.html.simpleparser.StyleSheet;
import com.lowagie.text.pdf.PdfWriter;

public class AnalysisCreatePdfGeneric {

    public static void createFile(String imageData, String dataTable,
            PdfExportConfiguration conf, HttpServletResponse response)
            throws IOException, DocumentException {

        StringBuilder header = new StringBuilder();
        header.append("attachment; filename=").append(conf.getFileName());
        Date time = new Date();
        SimpleDateFormat timeFormat = new SimpleDateFormat("yyyyMMdd_HHmmssSSS");
        String date = timeFormat.format(time);
        header.append(date).append(".pdf");
        // header.append(new Date().toString()).append(".pdf");

        createFile(imageData, dataTable, conf, response, header.toString(), false);
    }

    protected static void createFile(String imageData, String dataTable,
            PdfExportConfiguration conf, HttpServletResponse response, 
            String filename, boolean tableFirst) throws IOException,
            DocumentException {

        response.setContentType("application/pdf");

        Document document = new Document();
        OutputStream os = response.getOutputStream();
        PdfWriter.getInstance(document, os);
        document.open();

        Paragraph title = new Paragraph();
        title.add(conf.getTitle());
        title.setSpacingAfter(2);
        title.setAlignment("center");
        document.add(title);

        response.addHeader("Content-disposition", filename);

        if (tableFirst) {
            createTable(dataTable, conf, document);
        }

        // image
        if (imageData != null && ! imageData.isEmpty()) {
            if (!imageData.equals("notavalaible")) {
                byte[] decodedBuffer = javax.xml.bind.DatatypeConverter
                        .parseBase64Binary(imageData);
                Image image = Image.getInstance(decodedBuffer);
                float pageWidth = document.getPageSize().getWidth();
                float imageWidth = image.getWidth() + 100;

                image.scalePercent(pageWidth * 100 / imageWidth);
                image.setAlignment(Image.MIDDLE);
                document.add(image);
            } else {
                Paragraph notavalaible = new Paragraph("Chart not avalaible");
                document.add(notavalaible);
            }
        }

        if (!tableFirst) {
            createTable(dataTable, conf, document);
        }

        document.close();
        os.close();
    }

    private static void createTable(String dataTable,
            PdfExportConfiguration conf, Document document) throws IOException,
            DocumentException {
        // table
        if (dataTable != null) {
            StringReader strReader = new StringReader(dataTable);

            StyleSheet styles = new StyleSheet();

            styles.loadStyle("serviceHeadcode", "style", "font-weight:bold;");

            styles.loadStyle("green", "bgcolor", "green");
            styles.loadStyle("red", "bgcolor", "red");
            styles.loadStyle("yellow", "bgcolor", "yellow");

            styles.loadStyle("parent", "style", "font-weight:bold;");
            styles.loadStyle("sorting_disabled", "style", "font-weight:bold;");

            styles.loadTagStyle("td", "size", conf.getFontSize());
            styles.loadTagStyle("th", "size", conf.getFontSize());
            styles.loadTagStyle("th", "align", "center");
            styles.loadTagStyle("th", "font-weight", "bold");

            ArrayList<?> p = HTMLWorker.parseToList(strReader, styles);

            Paragraph paragraph = new Paragraph();
            paragraph.add((com.lowagie.text.Element) p.get(0));
            document.add(paragraph);

            Paragraph paragraphtable = new Paragraph();
            paragraphtable.setSpacingBefore(2);
            paragraphtable.add((com.lowagie.text.Element) p.get(1));
            document.add(paragraphtable);
        }
    }

}