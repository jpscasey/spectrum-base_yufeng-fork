/*
Create a new Table method.
Part 2
This script takes 4 Parameters 
@TableName the TAble name that you are modifing
@Counter The size of the batch to be copied 
@ID the staring ID to be copied defualts to the smallest ID in the Table
@MaxID the last ID to be copied 

Copies date from Current to new Columns

*/

DECLARE @Counter int = 5000
DECLARE @TableName varchar(100) = 'ChannelValueDoor' -- Table to have rows copied
DECLARE @MinID bigint -- Change this to limit rows copied
DECLARE @MaxID  Bigint --Places a max Row to be copied 

DECLARE @sql varchar(max)
DECLARE @sqlCols varchar(max)

IF NOT EXISTS (SELECT TOP 1  name 
FROM sys.tables t
WHERE t.name = @TableName+'_old'
AND T.name = @TableName) RAISERROR ('-- No Old Table to Copy From', 20, 1) WITH LOG




IF @MaxID is null
BEGIN 
	DECLARE @sqlMax nvarchar(200) = 'SELECT @ID = Max(ID) FROM '+@TableName+'_old'

	exec sp_executesql @sqlMax, N'@ID int out', @MaxID out
END

IF @MinID is null
BEGIN 
	DECLARE @sqlMix nvarchar(200) = 'SELECT @ID = Min(ID) FROM '+@TableName+'_old'


	exec sp_executesql @sqlMix, N'@ID int out', @MinID out
END

if @MaxID is null set @MaxID =1
if @MinID is null set @MinID =1
WHILE 1=1 and (@MinID < @MaxID OR @MaxID is null)
BEgiN 
SET @SQL = 'insert into '+@TableName+' select * from '+@TableName+'_OLD where ID BETWEEN  ' + CAST(@MaxID -@Counter as varchar(10))+' AND '+ CAST(@MaxID as varchar(10))

EXEC (@sql)
IF @@ROWCOUNT =0 BREAK

SET @MaxID = @MaxID -@Counter-1

WAITFOR DELAY '00:00:01'

END

GO





