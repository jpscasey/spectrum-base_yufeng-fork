/*
For Column Add rename method.
Part 1
This script takes 1 Parameters 
@TableName the TAble name that you are modifing

This Script Creates new columns For an column of type datetime in the Table Specified.

Column is created as Column_name + _new with any default that was on that column.

*/
DECLARE @TableName varchar(max) = 'Fault'
DECLARE @ColName varchar(max) = ''
DECLARE @Alter varchar(max) = ''

DECLARE @object_id int

IF NOT EXISTS (SELECT TOP 1  c.name 
FROM sys.tables t
iNNER JOIN sys.columns c on t.object_id = c.object_id 
wHERE c.user_type_id = 61
AND T.name = @TableName) RAISERROR ('-- No Columns Need to be changed', 20, 1) WITH LOG

WHILE 1=1
BEGIN

SELECT TOP 1  @ColName = c.name , @Alter = 'ALTER TABLE ' + t.name + ' ADD '+ c.name +'_new DATETIME2(3) '  FROM sys.tables t
iNNER JOIN sys.columns c on t.object_id = c.object_id 
wHERE c.user_type_id = 61
AND T.name = @TableName
and c.name > @ColName
ORDER BY t.name, c.name

IF @@ROWCOUNT =0 BREAK
 
DECLARE @NotNULL varchar(max) = NULL
IF EXISTS (SELECT 1 FROM sys.tables t iNNER JOIN sys.columns c on t.object_id = c.object_id  WHERE c.user_type_id = 61 and c.is_nullable =0  AND T.name = @TableName and c.name = @ColName)
SET @NotNULL = 'ALTER TABLE '+@TableName+' WITH NOCHECK ADD CONSTRAINT DF_'+@ColName+'_new_NOTNULL CHECK ('+@ColName+'_new IS NOT NULL)'

SET @object_id = OBJECT_ID(@TableName)
DECLARE @DefaultAdd varchar(max) =NULL

SELECT @DefaultAdd = 'ALTER TABLE '+@TableName+' ADD CONSTRAINT ['+dc.name+'_new] DEFAULT' + dc.[definition] +' FOR ['+c.name+'_new]'

FROM sys.columns c WITH (NOWAIT)
 JOIN sys.default_constraints dc ON c.default_object_id != 0 AND c.[object_id] = dc.parent_object_id AND c.column_id = dc.parent_column_id
 where c.name = @ColName and c.object_id = @object_id

EXEC (@Alter)
EXEC (@DefaultAdd)
EXEC (@NotNULL)

END

GO