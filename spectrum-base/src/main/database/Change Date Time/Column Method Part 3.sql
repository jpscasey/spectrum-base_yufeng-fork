/*
For Column Add rename method.
Part 3
This script takes 3 Parameters 
@TableName the TAble name that you are modifing
@IndexedView OPTIONAL this is the indexed view that needs to be droped to allow changes to be made.

The third is a Check on the indexed view in the first if statement set the value to be equal to TableName

This Script Drops any indexes indexed views and defaults on datetime columns. Before renameing the columns. And then renaming default for the new Column.
Recreateing indexes and indexed views


WARNING This Script is dealing with indexes so MAy take some time.
*/

IF OBJECT_ID('tempdb..#Columns') IS NOT NULL DROP TABLE #Columns




DECLARE @TableName  varchar(max) = 'Fault'
DECLARE @object_id int


DECLARE @IndexedView varchar(max) ='VW_IX_Fault'
DECLARE @indCurr varchar(max) =''


IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS where COLUMN_NAME =(SELECT TOP 1  c.name +'_new' 
FROM sys.tables t
iNNER JOIN sys.columns c on t.object_id = c.object_id 
wHERE c.user_type_id = 61
AND T.name = @TableName)) RAISERROR ('-- New Columns do not EXIST', 20, 1) WITH LOG


DECLARE @sql varchar(max)
DECLARE @Dropsql varchar(max) =''
,@sqlIndexes varchar(max) =''


if @TableName = 'Fault'
BEGiN
	SET @indCurr =''
		WHILE 1=1
		BEGiN
			SELECT Top 1 @indCurr = i.name FROM sys.indexes i WITH (NOWAIT)
			INNER JOiN sys.views v ON v.object_id = i.object_id
					WHERE 1=1
						AND i.is_primary_key = 0
						AND v.name =@IndexedView
						AND i.name >@indCurr
						ORDER BY i.name

			IF @@ROWCOUNT =0 BREAK;
			

			SET @Dropsql = @DRopsql + '
			IF EXISTS ( SELECT * FROM sys.indexes where name ='''+@indCurr+''')
			DROP INDEX '+@indCurr+' ON [dbo].'+@IndexedView+'
			
			'
		END

		SET @Dropsql = @DRopsql +  'DROP VIEW '+@IndexedView

SELECT @sql = m.[definition] 

FROM sys.sql_modules m 
INNER JOIN sys.objects o 
    ON m.[object_id]=o.[object_id]
	INNER JOIN sys.schemas s ON s.schema_id = o.schema_id
WHERE 1=1
    AND is_ms_shipped = 0 -- to exclude system stored proc
    AND [type_desc] = 'View'
	and o.name like 'VW_IX%'
	AND o.[name] = @IndexedView



		SET @indCurr =''
		WHILE 1=1
		BEGiN
			SELECT Top 1 @indCurr = i.name FROM sys.indexes i WITH (NOWAIT)
			INNER JOiN sys.views v ON v.object_id = i.object_id
					WHERE 1=1
						AND i.is_primary_key = 0
						AND v.name =@IndexedView
						AND i.name >@indCurr
					ORDER BY i.name

			IF @@ROWCOUNT =0 BREAK;
			
			DECLARE @sql2 VARCHAR(max)

			;WITH index_column AS 
			(
				SELECT 
					  ic.[object_id]
					, ic.index_id
					, ic.is_descending_key
					, ic.is_included_column
					, c.name
				FROM sys.index_columns ic WITH (NOWAIT)
				JOIN sys.columns c WITH (NOWAIT) ON ic.[object_id] = c.[object_id] AND ic.column_id = c.column_id
  
			)
			SELECT @sql2 =' 
			CREATE' + CASE WHEN i.is_unique = 1 THEN ' UNIQUE' ELSE '' END 
                + ' '+ CASE WHeN i.type_desc = 'CLUSTERED' THEN 'CLUSTERED' ELSE 'NONCLUSTERED' END + '  INDEX [' + i.name + '] ON ' + v.name + ' (' +
                STUFF((
                SELECT ', [' + c.name + ']' + CASE WHEN c.is_descending_key = 1 THEN ' DESC' ELSE ' ASC' END
                FROM index_column c
                WHERE c.is_included_column = 0
                    AND c.index_id = i.index_id
					AND c.[object_id] = i.object_id
                FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'), 1, 2, '') + ')'  
                + ISNULL(CHAR(13) + 'INCLUDE (' + 
                    STUFF((
                    SELECT ', [' + c.name + ']'
                    FROM index_column c
                    WHERE c.is_included_column = 1
                        AND c.index_id = i.index_id
						AND c.[object_id] = i.object_id
                    FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'), 1, 2, '') + ')', '')
					+ ' WITH (PAD_INDEX = '+ CASE WHEN i.is_padded =1 THEN 'ON' ELSE 'OFF'END+
					', STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, 
ALLOW_PAGE_LOCKS = ON, FILLFACTOR = '+CASE WHEN i.fill_factor =0 then '100' ELSE Cast(i.fill_factor as varchar(4)) END+') 
'	

			FROM sys.indexes i WITH (NOWAIT)
			INNER JOiN sys.views v ON v.object_id = i.object_id
					WHERE 1=1
						AND i.is_primary_key = 0
						AND v.name =@IndexedView
						AND i.name =@indCurr

			SET @sqlIndexes = @sqlIndexes +@sql2 
		END
END




SET @object_id = OBJECT_ID(@TableName)

DECLARE @DefaultRename varchar(max) =NULL
DECLARE @DefaultDrop varchar(max) =NULL

		
SELECT  c.name 
INTO #Columns
FROM sys.tables t
iNNER JOIN sys.columns c on t.object_id = c.object_id 
wHERE c.user_type_id = 61
AND T.name = @TableName


		SELECT @DefaultDrop = STUFF(( SELECT ' ALTER TABLE '+@TableName+'  DROP CONSTRAINT '+ dc.name + ' '
		FROM sys.columns c WITH (NOWAIT)
		 JOIN sys.default_constraints dc ON c.default_object_id != 0 AND c.[object_id] = dc.parent_object_id AND c.column_id = dc.parent_column_id
		 where c.object_id = @object_id 
		 and c.name in (SELECT name FROM #Columns)
		 FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'), 1, 1,  + '') 

		SELECT @DefaultRename = STUFF(( SELECT ' EXEC sp_rename '''+dc.name+'_new'''+ dc.name+''' FOR ['+c.name+'_new] , ''OBJECT'' ' 
		FROM sys.columns c WITH (NOWAIT)
		 JOIN sys.default_constraints dc ON c.default_object_id != 0 AND c.[object_id] = dc.parent_object_id AND c.column_id = dc.parent_column_id
		 where c.object_id = @object_id 
		 and c.name in (SELECT name FROM #Columns)
		 FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'), 1, 1,  + '') 

DECLARE @IndexAdd varchar(max) =NULL
DECLARE @IndexDrop varchar(max) =NULL
;WITH index_column AS 
(
    SELECT 
          ic.[object_id]
        , ic.index_id
        , ic.is_descending_key
        , ic.is_included_column
        , c.name
		, ic.key_ordinal
    FROM sys.index_columns ic WITH (NOWAIT)
    JOIN sys.columns c WITH (NOWAIT) ON ic.[object_id] = c.[object_id] AND ic.column_id = c.column_id
    WHERE ic.[object_id] = @object_id 
)

SELECT @IndexAdd = ISNULL((
		SELECT STUFF(
	(SELECT ' 
	CREATE' + CASE WHEN i.is_unique = 1 THEN ' UNIQUE' ELSE '' END 
                + ' '+ CASE WHeN i.type_desc = 'CLUSTERED' THEN 'CLUSTERED' ELSE 'NONCLUSTERED' END + '  INDEX [' + i.name + '] ON ' + t.name + ' (' +
                STUFF((
                SELECT ', [' + c.name + ']' + CASE WHEN c.is_descending_key = 1 THEN ' DESC' ELSE ' ASC' END
                FROM index_column c
                WHERE c.is_included_column = 0
                    AND c.index_id = i.index_id
					AND c.[object_id] = i.object_id
					ORDER BY c.key_ordinal
                FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'), 1, 2, '') + ')'  
                + ISNULL(CHAR(13) + 'INCLUDE (' + 
                    STUFF((
                    SELECT ', [' + c.name + ']'
                    FROM index_column c
                    WHERE c.is_included_column = 1
                        AND c.index_id = i.index_id
						AND c.[object_id] = i.object_id
						ORDER BY c.key_ordinal
		
                    FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'), 1, 2, '') + ')', '')
						+ CASE WHEN i.filter_definition is not null then ' WHERE '+i.filter_definition ELSE '' END
					+ ' WITH (PAD_INDEX = '+ CASE WHEN i.is_padded =1 THEN 'ON' ELSE 'OFF'END+
					', STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, 
ALLOW_PAGE_LOCKS = ON'+CASE WHEN i.fill_factor =0 then '' ELSE ' , FILLFACTOR = '+Cast(i.fill_factor as varchar(4)) END  +')'
	
        FROM sys.indexes i WITH (NOWAIT)
		INNER JOIN sys.tables t on t.object_id = i.object_id
		INNER JOIN sys.index_columns ic2 WITH (NOWAIT) ON ic2.index_id = i.index_id AND ic2.[object_id] = i.object_id
		JOIN sys.columns c2 WITH (NOWAIT) ON ic2.[object_id] = c2.[object_id] AND ic2.column_id = c2.column_id
        WHERE 1=1
            AND i.is_primary_key = 0
			and c2.name in (SELECT name FROM #Columns)
			and i.object_id = @object_id
	FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'), 1, 2, '') 
	),'')




;WITH index_column AS 
(
    SELECT 
          ic.[object_id]
        , ic.index_id
        , ic.is_descending_key
        , ic.is_included_column
        , c.name
		, ic.key_ordinal
    FROM sys.index_columns ic WITH (NOWAIT)
    JOIN sys.columns c WITH (NOWAIT) ON ic.[object_id] = c.[object_id] AND ic.column_id = c.column_id
    WHERE ic.[object_id] = @object_id 
)	
SELECT @IndexDrop = ISNULL((
		SELECT STUFF(
	(SELECT distinct ' 
	DROP   INDEX [' + i.name + '] ON ' + t.name 
	
        FROM sys.indexes i WITH (NOWAIT)
		INNER JOIN sys.tables t on t.object_id = i.object_id
		INNER JOIN sys.index_columns ic2 WITH (NOWAIT) ON ic2.index_id = i.index_id AND ic2.[object_id] = i.object_id
		JOIN sys.columns c2 WITH (NOWAIT) ON ic2.[object_id] = c2.[object_id] AND ic2.column_id = c2.column_id
        WHERE 1=1
            AND i.is_primary_key = 0
			and c2.name in (SELECT name FROM #Columns)
			and i.object_id = @object_id
	FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'), 1, 2, '') 
	),'')

	DECLARE @RenameSQL varchar(max)

	SELECT @RenameSQL = STUFF(( SELECT
	' EXEC sp_rename '''+@TableName+'.'+name+''' , '''+name+'_old'',''COLUMN'' EXEC sp_rename '''+@TableName+'.'+name+'_new'' , '''+name+''',''COLUMN''  '
	FROM #Columns
	FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'), 1, 1, '') 

	
		DECLARE  @DROPCheckNull varchar(max) = STUFF(( SELECT '  ALTER TABLE '+@TableName+' DROP CONSTRAINT DF_'+c.name+'_NOTNULL'
		FROM sys.check_constraints cc
		iNNER JOiN sys.tables t on cc.parent_object_id = t.object_id
		INNER JOIN sys.columns c ON c.object_id = t.object_id and cc.parent_column_id = c.column_id
		where c.name in (SELECT name+'_new' FROM #Columns)
		and t.name =@TableName
		FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'), 1, 1, '') 

		DECLARE  @AddCheckNull varchar(max) = STUFF(( SELECT 
		' ALTER TABLE '+@TableName+' WITH NOCHECK ADD CONSTRAINT DF_'+LEFT(c.name, len(c.name)-4)+'_NOTNULL CHECK ('
		+LEFT(c.name, len(c.name)-4)+' IS NOT NULL)' 
		FROM sys.check_constraints cc
		iNNER JOiN sys.tables t on cc.parent_object_id = t.object_id
		INNER JOIN sys.columns c ON c.object_id = t.object_id and cc.parent_column_id = c.column_id
		where c.name in (SELECT name+'_new' FROM #Columns)
		and t.name = @TableName
		FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'), 1, 1, '') 



		DECLARE  @AllowNUlls varchar(max) = STUFF(( SELECT 
		'  ALTER TABLE '+@TableName+' ALTER COLUMN '+c.name+'_old datetime NULL '
		FROM sys.tables t 
		INNER JOIN sys.columns c ON c.object_id = t.object_id
		where c.name in (SELECT name FROM #Columns)
		and t.name =@TableName
		and is_nullable = 0
		FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'), 1, 1, '') 
	
--		SELECT @Dropsql , @sql,@sqlIndexes ,@IndexDrop, @IndexAdd , @DefaultDrop , @DefaultRename,@RenameSQL,@AllowNUlls,@AddCheckNull,@DROPCheckNull

		EXEC (@Dropsql) -- DROPS the indexed view for fault
		EXEC (@IndexDrop) --Drop index for old column
		EXEC (@DefaultDrop) --drop old defaults
		EXEC (@DROPCheckNull)

		EXEC (@RenameSQL)
		EXEC (@AddCheckNull)
		EXEC (@sql) -- Recreates the index view for fault
		EXEC (@sqlIndexes) -- Reindexes view
		EXEC (@DefaultRename) -- Rename the new defaults
		EXEC (@IndexAdd) -- Recreates index
		EXEC (@AllowNUlls)
		

