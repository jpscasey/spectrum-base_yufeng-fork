USE Administration

/*
delete from [UserActivity]
delete from [Permission]
delete from [UserRole]
delete from [User]
delete from [Role]
delete from [Resource]
delete from [Application] 
*/

/* add SPECTRUM application */
MERGE [Application] a 
USING(
    SELECT 
        [ApplicationName] = 'SPECTRUM'
        , [Description] = 'Spectrum Application'
) AS na
ON a.ApplicationName = na.ApplicationName
WHEN MATCHED THEN 
UPDATE SET
    Description = na.Description
WHEN NOT MATCHED THEN
INSERT (
    ApplicationName
    , Description
)
VALUES (
    na.ApplicationName
    , na.Description);

GO

/* Add SPECTRUM roles */
;MERGE [Role] r
USING (
    SELECT [RoleName] = 'USERADMIN', [Description] = 'Access to User Administration', [Priority] = 1, [SystemRole] = 1
    UNION ALL SELECT 'ADMIN', 'Access to Spectrum Administration', 2, 1
    UNION ALL SELECT 'SPECTRUM', 'Access to Spectrum Application', 3, 1
    UNION ALL SELECT 'SPECTRUM Power User', 'Power User of SPECTRUM', 4, 0
    UNION ALL SELECT 'SPECTRUM Super User', 'Super User of SPECTRUM', 5, 0
    UNION ALL SELECT 'SPECTRUM Standard User', 'Standard User of SPECTRUM', 6, 0
) AS nr
ON r.RoleName = nr.RoleName
WHEN MATCHED THEN 
UPDATE SET
    [Description] = nr.[Description]
    , [Priority]    = nr.[Priority]
    , [SystemRole]  = nr.[SystemRole]
WHEN NOT MATCHED THEN
INSERT (
    RoleName
    , [Description]
    , [Priority]
    , [SystemRole])
VALUES (
    nr.RoleName
    , nr.[Description]
    , nr.[Priority]
    , nr.[SystemRole]);

GO

/* Add Administrator user */
IF NOT EXISTS (SELECT * FROM [User] WHERE UPPER([UserName]) = 'ADMINISTRATOR')
BEGIN
    INSERT INTO [User](
        [Username],  [Password], [FirstName], [LastName], [EmailAddress], [MobileNumber], [PasswordExpires], [PasswordSetDate], [ExpiryDays], [Location], [AccountDisabled])
    VALUES('Administrator', 'E38AD214943DAAD1D64C102FAEC29DE4AFE9DA3D', 'Administrator', 'Administrator', 'administrator@nexala.com', '+1234567890', 0, '1/1/2011', 0, 'Dublin', 0);
END 

GO

/* Add Administrator to roles*/
;MERGE [UserRole] ur
USING (
    SELECT
        UserID = (SELECT [UserID] FROM [User] WHERE UPPER([UserName]) = 'ADMINISTRATOR')
        , RoleID
    FROM [Role] 
    WHERE RoleName IN ('USERADMIN','ADMIN', 'SPECTRUM', 'SPECTRUM Power User')
) AS nur 
ON ur.UserID = nur.UserID
    AND ur.RoleID = nur.RoleID
WHEN NOT MATCHED THEN
INSERT (
    UserID
    , RoleID)
VALUES (
    UserID
    , RoleID);

GO

/* gets the SPECTRUM application id */
DECLARE @applicationID int = (SELECT [ApplicationID] FROM [Application] WHERE UPPER([ApplicationName]) = 'SPECTRUM')

/* gets the SPECTRUM POWER USER role id */
DECLARE @roleID int = (SELECT [RoleID] FROM [Role] WHERE UPPER([RoleName]) = 'SPECTRUM POWER USER')

/* update feature resources */
DECLARE @SummaryOfChanges TABLE(
    Change VARCHAR(20)
    , FeatureKey VARCHAR(100)
);

;MERGE Resource AS r 
USING (
    -- Administration:
    SELECT Type = 'F', FeatureKey = 'spectrum.administration', FeatureDescription = 'Administration'
    UNION SELECT 'F', 'spectrum.administration.channeldefinition', 'Administration - Channel Definition'
    UNION SELECT 'F', 'spectrum.administration.emailtemplates', 'Administration - Email Template Editor'
    UNION SELECT 'F', 'spectrum.administration.faultconfig', 'Administration - Fault Configuration Editor'
    UNION SELECT 'F', 'spectrum.administration.faulttest', 'Administration - Fault Test'
    UNION SELECT 'F', 'spectrum.administration.packageupload', 'Administration - Package Upload'
    UNION SELECT 'F', 'spectrum.administration.recoverieseditor', 'Administration - Recoveries Editor'
    UNION SELECT 'F', 'spectrum.administration.repobackup', 'Administration - Repository Backup'
    UNION SELECT 'F', 'spectrum.administration.ruleseditor', 'Administration - Fault Rules Editor'
    UNION SELECT 'F', 'spectrum.administration.scheduledanalysis', 'Administration - Scheduled Analysis'
    UNION SELECT 'F', 'spectrum.administration.users', 'Administration - Users'
    UNION SELECT 'F', 'spectrum.eventanalysis', 'Event Analysis'
    UNION SELECT 'F', 'spectrum.event.panel', 'Events Panel'
) AS nr
ON r.FeatureKey = nr.FeatureKey
    AND r.Type = 'F'
WHEN MATCHED THEN 
UPDATE SET
    Type = nr.Type
    , FeatureDescription = nr.FeatureDescription
WHEN NOT MATCHED THEN
INSERT (
    ApplicationID
    , Type
    , FeatureKey
    , FeatureDescription)
VALUES(
    @applicationID
    , nr.Type
    , nr.FeatureKey
    , nr.FeatureDescription)
OUTPUT $action, ISNULL(inserted.FeatureKey, deleted.FeatureKey) 
INTO @SummaryOfChanges;

-- add newly added licences to Power User role
INSERT INTO Permission (
    ResourceID
    , RoleID
    , PermissionType)
SELECT 
    r.ResourceID
    , @roleID
    , 'READ'
FROM @SummaryOfChanges soc
INNER JOIN Resource r ON r.FeatureKey = soc.FeatureKey
WHERE soc.Change = 'INSERT'

GO
