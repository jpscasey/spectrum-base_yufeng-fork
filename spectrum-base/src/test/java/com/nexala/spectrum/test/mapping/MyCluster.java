/*
 * Copyright (c) Nexala Technologies 2010, All rights reserved.
 */
package com.nexala.spectrum.test.mapping;

import com.nexala.spectrum.mapping.Cluster;
import com.nexala.spectrum.mapping.Coordinate;

public class MyCluster extends Cluster<Coordinate> {

    private int count = 0;
    
    public MyCluster() {}
   
    public void init(Coordinate c) {
        this.setCoordinate(c);
        this.addItem(c);
    }
    
    public void addItem(Coordinate c) {
        count++;
    }
    
    public int getCount() {
        return count;
    }
}