package com.nexala.spectrum.test.loader;

import static org.junit.Assert.assertEquals;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import org.junit.Before;
import org.junit.Test;

import com.nexala.spectrum.loader.LoaderException;
import com.nexala.spectrum.loader.algorithm.DotNetTicksAlgorithm;
import com.nexala.spectrum.loader.beans.FileFieldDefinition;

/**
 * Test suite for algorithm {@link DotNetTicksAlgorithm}.
 * 
 * @author jana
 * 
 */
public class DotNetTicksAlgorithmTest {
    private static final String TIME_FORMAT = "yyyy-MM-dd HH:mm:ss.SSS";
    DotNetTicksAlgorithm algorithm;
    FileFieldDefinition mockFileFieldDefinition;

    @Before
    public void setup() {
        algorithm = new DotNetTicksAlgorithm();
        mockFileFieldDefinition = new FileFieldDefinition();
        mockFileFieldDefinition.setId(5);
        mockFileFieldDefinition.setFieldType("identification");
        mockFileFieldDefinition.setProcessingAlgorithm("DotNetTicks");
    }

    // /

    @Test
    public void ifTicksAreInScientificLongThanSuccessParsing() throws LoaderException {
        // Setup
        String ticksStr = "635467416405e6";
        long expectedValue = 635467416405000000l;
        // Run
        long ticks = algorithm.parseTicks(ticksStr);
        // Test
        assertEquals(expectedValue, ticks);
    }

    @Test
    public void ifTicksAreInScientificLongThanSuccessParsing2() throws LoaderException {
        // Setup
        String ticksStr = "6.35467416405E+17";
        long expectedValue = 635467416405000000l;
        // Run
        long ticks = algorithm.parseTicks(ticksStr);
        // Test
        assertEquals(expectedValue, ticks);
    }

    @Test
    public void ifTicksAreValidLongThanSuccessParsing() throws LoaderException {
        String ticksStr = "635467416405000000L";
        long expectedValue = 635467416405000000L;
        // Run
        long ticks = algorithm.parseTicks(ticksStr);
        // Test
        assertEquals(expectedValue, ticks);
    }

    @Test(expected = LoaderException.class)
    public void ifTicksAreNegativeThanParsingFailsAndThrowError() throws LoaderException {
        String ticksStr = "-635467416405000000L";
        // Run
        algorithm.parseTicks(ticksStr);
    }

    @Test(expected = LoaderException.class)
    public void ifTicksAreDecimalsWithNotLoosingLongConversionThanParsingFailsAndThrowError() throws LoaderException {
        String ticksStr = "635467416405000000.5";
        // Run
        algorithm.parseTicks(ticksStr);
    }

    // //--------------------------- Algorithm computation -------------------------

    /**
     * @throws ParseException
     * @throws LoaderException
     */
    @Test
    public void ifTicksConvertedtoJavaMillisThanReturnSameMillisLikeForJavaDate() throws ParseException,
            LoaderException {
        // Values are generated with commented script below
        // Setup
        String ticks = "635467416405000000"; // UTC
        String expectedDateStr = "2014-09-19 16:40:40.500"; // UTC
        SimpleDateFormat df = new SimpleDateFormat(TIME_FORMAT);
        df.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date expectedDate = df.parse(expectedDateStr); // UTC
        long expectedMillis = expectedDate.getTime();

        // Run
        long millis = algorithm.getJavaMillisFromTicks(ticks);
        Date date = new Date(millis);
        System.out.println(date);

        // Test
        assertEquals(expectedMillis, millis);
    }

    /**
     * @throws ParseException
     * @throws LoaderException
     */
    @Test
    public void ifValidTicksInScientificNotationThanSucessfullConversion() throws ParseException, LoaderException {
        // Values are generated with commented script below
        // Setup
        String ticks = "635467416405e6"; // UTC
        String expectedDateStr = "2014-09-19 16:40:40.500"; // UTC
        SimpleDateFormat df = new SimpleDateFormat(TIME_FORMAT);
        df.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date expectedDate = df.parse(expectedDateStr); // UTC
        long expectedMillis = expectedDate.getTime();

        // Run
        long millis = algorithm.getJavaMillisFromTicks(ticks);
        Date date = new Date(millis);
        System.out.println(date);

        // Test
        assertEquals(expectedMillis, millis);
    }

}

// C# code used for generating test values
/*
 * using System.IO; using System;
 * 
 * class Program {
 * 
 * static void inspectDate(DateTime datetime) { Console.WriteLine("Inspecting datetime {0:M/dd/yyyy h:mm:ss.fff tt} {1}", datetime, datetime.Kind);
 * 
 * Console.WriteLine("Ticks {0}", datetime.Ticks.ToString()); DateTime javaEpoch = new DateTime(1970, 1,1,0,0,0,DateTimeKind.Utc); long ticksFromJavaEpoch = datetime.Ticks - javaEpoch.Ticks; long
 * javaTimestamp = ticksFromJavaEpoch / TimeSpan.TicksPerMillisecond; Console.WriteLine("Milliseconds elapsed = {0}", javaTimestamp); }
 * 
 * static void Main() { DateTime date = new DateTime(2014, 9, 19, 16, 40, 40, 500, DateTimeKind.Utc); inspectDate(date); //DateTime date = DateTime.Now; //DateTime dateUTC =
 * TimeZoneInfo.ConvertTimeToUtc(date, TimeZoneInfo.Local); //inspectDate(dateUTC); } }
 */
