/*
 * Copyright (c) Nexala Technologies 2011, All rights reserved.
 */
package com.nexala.spectrum.test.recovery;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

import org.apache.log4j.BasicConfigurator;
import org.junit.Test;

import com.nexala.spectrum.recovery.RecoveryAnswer;
import com.nexala.spectrum.recovery.RecoveryManager;
import com.nexala.spectrum.recovery.RecoveryOutcome;
import com.nexala.spectrum.recovery.RecoveryStep;
import com.nexala.spectrum.recovery.RecoveryVertex;

/**
 * Base RecoveryManager tests
 * 
 * @author Marcus O'Connell
 *
 */
public class RecoveryTest {

	@Test
	public void testCreateRecoveryInstance() throws Exception {
		BasicConfigurator.configure();
		
		//InputStream recoveryIS = getClass().getResourceAsStream("test-recovery.xml");
	    InputStream recoveryIS = getClass().getClassLoader().getResourceAsStream("test-recovery.xml");
		String recovery = convertToString(recoveryIS);
		
        RecoveryManager mgr = RecoveryManager.instance(null);
		
		Object instId = mgr.executeProcessFromDefn(recovery, "username");
		assertNotNull(instId);
		
		RecoveryVertex step = null;
		
		assertTrue(mgr.isActive(instId));
		
		assertFalse(mgr.isReadyForCompletion(instId));
		step = mgr.getNextStep(instId);
		assertFalse(mgr.isReadyForCompletion(instId));
		
		assertNotNull(step);
		assertTrue(step instanceof RecoveryStep);
		assertEquals("QUESTION 1", ((RecoveryStep)step).getQuestion());
		
		assertFalse(mgr.isReadyForCompletion(instId));
		mgr.setStepOutcome(instId, ((RecoveryStep)step).getAnswers().get(0).getAnswerId(), false, "Notes");
		assertFalse(mgr.isReadyForCompletion(instId));
		
		assertTrue(mgr.isActive(instId));
		
		step = mgr.getNextStep(instId);
		assertEquals("QUESTION 2", ((RecoveryStep)step).getQuestion());
		
		assertFalse(mgr.isReadyForCompletion(instId));
		mgr.setStepOutcome(instId, ((RecoveryStep)step).getAnswers().get(0).getAnswerId(), true, "Notes");
		assertFalse(mgr.isReadyForCompletion(instId));
		
		step = mgr.getNextStep(instId);
		
		assertTrue(mgr.isReadyForCompletion(instId));
		assertTrue(step instanceof RecoveryOutcome);
		assertTrue(mgr.isActive(instId));
		
		mgr.removeProcess(instId);
		assertFalse(mgr.isActive(instId));
		
	}

	@Test
	public void testProcess2() throws Exception {
		BasicConfigurator.configure();
		InputStream recoveryIS = getClass().getClassLoader().getResourceAsStream("test-recovery2.xml");
		String recovery = convertToString(recoveryIS);
		
        RecoveryManager mgr = RecoveryManager.instance(null);
		
		Object instId = mgr.executeProcessFromDefn(recovery, "username");
		assertNotNull(instId);
		
		RecoveryVertex step = null;
		
		assertTrue(mgr.isActive(instId));
		assertFalse(mgr.isReadyForCompletion(instId));
		step = mgr.getNextStep(instId);
		assertFalse(mgr.isReadyForCompletion(instId));
		
		assertEquals("QUESTION 1", ((RecoveryStep)step).getQuestion());
		List<RecoveryAnswer> answers = ((RecoveryStep)step).getAnswers();
		assertEquals(3, answers.size());
		assertEquals("ANSWER 1", answers.get(0).getAnswer());
		assertEquals("ANSWER 2", answers.get(1).getAnswer());
		assertEquals("ANSWER 3", answers.get(2).getAnswer());
		
		assertFalse(mgr.isReadyForCompletion(instId));
		mgr.setStepOutcome(instId, answers.get(0).getAnswerId(), false, "Notes");
		assertFalse(mgr.isReadyForCompletion(instId));
		
		step = mgr.getNextStep(instId);
		
		assertTrue(mgr.isActive(instId));
		
		assertNotNull(step);
		assertEquals("QUESTION 2", ((RecoveryStep)step).getQuestion());
		answers = ((RecoveryStep)step).getAnswers();
		assertEquals(3, answers.size());
		assertEquals("ANSWER 4", answers.get(0).getAnswer());
		assertEquals("ANSWER 5", answers.get(1).getAnswer());
		assertEquals("ANSWER 6", answers.get(2).getAnswer());
		assertEquals("ACTION 4",answers.get(0).getAction());
		assertEquals("ACTION 5",answers.get(1).getAction());
		assertEquals("ACTION 6",answers.get(2).getAction());
		
		assertFalse(mgr.isReadyForCompletion(instId));
		mgr.setStepOutcome(instId, answers.get(1).getAnswerId(), false, "Notes");
		assertFalse(mgr.isReadyForCompletion(instId));
		
		step = mgr.getNextStep(instId);
		
		assertNotNull(step);
		
		assertEquals("QUESTION 9", ((RecoveryStep)step).getQuestion());
		answers = ((RecoveryStep)step).getAnswers();
		assertEquals(2, answers.size());
		assertEquals("ANSWER 9", answers.get(0).getAnswer());
		assertEquals("ANSWER 10", answers.get(1).getAnswer());
		assertEquals("ACTION 9",answers.get(0).getAction());
		assertEquals("ACTION 10",answers.get(1).getAction());
		assertFalse(mgr.isReadyForCompletion(instId));
		
		mgr.setStepOutcome(instId, answers.get(0).getAnswerId(), true, "Notes");
		assertFalse(mgr.isReadyForCompletion(instId));
		
		step = mgr.getNextStep(instId);
		
		assertTrue(mgr.isReadyForCompletion(instId));
		assertTrue(step instanceof RecoveryOutcome);
		assertTrue(mgr.isActive(instId));
		
		mgr.removeProcess(instId);
		assertFalse(mgr.isActive(instId));
	}
	
	@Test
	public void testRevert() throws Exception {
		
		BasicConfigurator.configure();
		InputStream recoveryIS = getClass().getClassLoader().getResourceAsStream("test-recovery2.xml");
		String recovery = convertToString(recoveryIS);
		
        RecoveryManager mgr = RecoveryManager.instance(null);
		
		Object instId = mgr.executeProcessFromDefn(recovery, "username");
		assertNotNull(instId);
		assertFalse(mgr.isReadyForCompletion(instId));
		
		RecoveryVertex step = null;
		
		step = mgr.getNextStep(instId);
		assertFalse(mgr.isReadyForCompletion(instId));
		
		Object revertStep = ((RecoveryStep)step).getStepId();
		
		assertEquals("QUESTION 1", ((RecoveryStep)step).getQuestion());
		List<RecoveryAnswer> answers = ((RecoveryStep)step).getAnswers();
		assertEquals(3, answers.size());
		
		mgr.setStepOutcome(instId, answers.get(0).getAnswerId(), false, "Notes");
		step = mgr.getNextStep(instId);
		assertFalse(mgr.isReadyForCompletion(instId));
		
		assertNotNull(step);
		assertEquals("QUESTION 2", ((RecoveryStep)step).getQuestion());
		answers = ((RecoveryStep)step).getAnswers();
		assertEquals(3, answers.size());

		mgr.setStepOutcome(instId, answers.get(1).getAnswerId(), false, "Notes");
		
		step = mgr.getNextStep(instId);
		assertFalse(mgr.isReadyForCompletion(instId));
		assertNotNull(step);
		assertEquals("QUESTION 9", ((RecoveryStep)step).getQuestion());

		mgr.revertToStep(instId, revertStep);
		assertFalse(mgr.isReadyForCompletion(instId));
		
		step = mgr.getCurrentlyExecuting(instId);
		
		assertNotNull(step);
		assertEquals("QUESTION 1", ((RecoveryStep)step).getQuestion());
		
	}
	
	@Test
	public void scaleTest() throws Exception {
		
		BasicConfigurator.configure();
		InputStream recoveryIS = getClass().getClassLoader().getResourceAsStream("test-recovery2.xml");
		String recovery = convertToString(recoveryIS);
		int counter = 10;
		
        RecoveryManager mgr = RecoveryManager.instance(null);
		
		Object[] instances = new Object[counter];
		RecoveryVertex[] currentSteps = new RecoveryVertex[counter];
		
		for(int x = 0; x < counter; x++) {
			instances[x] = mgr.executeProcessFromDefn(recovery, "username");
		}
		
		for(int x = 0; x < counter; x++) {
			currentSteps[x] = mgr.getNextStep(instances[x]);
			assertFalse(mgr.isReadyForCompletion(instances[x]));
			assertEquals("QUESTION 1", ((RecoveryStep)currentSteps[x]).getQuestion());
		}
		
		for(int x = 0; x < counter; x++) {
			mgr.setStepOutcome(instances[x], ((RecoveryStep)currentSteps[x]).getAnswers().get(0).getAnswerId(), false,
			        "Notes");
		}
		
		for(int x = 0; x < counter; x++) {
			currentSteps[x] = mgr.getNextStep(instances[x]);
			assertFalse(mgr.isReadyForCompletion(instances[x]));
			assertEquals("QUESTION 2", ((RecoveryStep)currentSteps[x]).getQuestion());
		}
		
		for(int x = 0; x < counter; x++) {
			mgr.setStepOutcome(instances[x], ((RecoveryStep)currentSteps[x]).getAnswers().get(1).getAnswerId(), false,
			        "Notes");
		}
		
		for(int x = 0; x < counter; x++) {
			currentSteps[x] = mgr.getNextStep(instances[x]);
			assertEquals("QUESTION 9", ((RecoveryStep)currentSteps[x]).getQuestion());
			assertFalse(mgr.isReadyForCompletion(instances[x]));
		}
	}
	
	@Test
	public void parallelTest() throws Exception {
		
		BasicConfigurator.configure();
		InputStream recoveryIS1 = getClass().getClassLoader().getResourceAsStream("test-recovery.xml");
		String recovery1 = convertToString(recoveryIS1);
		
		InputStream recoveryIS2 = getClass().getClassLoader().getResourceAsStream("test-recovery2.xml");
		String recovery2 = convertToString(recoveryIS2);
		
        RecoveryManager mgr = RecoveryManager.instance(null);
		
		Object instId1 = mgr.executeProcessFromDefn(recovery1, "username");
		Object instId2 = mgr.executeProcessFromDefn(recovery2, "username");
		Object instId3 = mgr.executeProcessFromDefn(recovery2, "username");
		
		assertNotNull(instId1);
		assertNotNull(instId2);
		assertNotNull(instId3);
		
		assertFalse(mgr.isReadyForCompletion(instId1));
		assertFalse(mgr.isReadyForCompletion(instId2));
		assertFalse(mgr.isReadyForCompletion(instId3));

		RecoveryVertex[] steps = new RecoveryVertex[3];
		
		steps[0] = mgr.getNextStep(instId1);
		assertFalse(mgr.isReadyForCompletion(instId1));
		assertNotNull(steps[0]);
		assertEquals("QUESTION 1", ((RecoveryStep)steps[0]).getQuestion());
		mgr.setStepOutcome(instId1, ((RecoveryStep)steps[0]).getAnswers().get(0).getAnswerId(), false, "Notes");
		
		
		
		steps[1] = mgr.getNextStep(instId2);
		assertFalse(mgr.isReadyForCompletion(instId2));
		assertEquals("QUESTION 1", ((RecoveryStep)steps[1]).getQuestion());
		
		mgr.setStepOutcome(instId2, ((RecoveryStep)steps[1]).getAnswers().get(0).getAnswerId(), false, "Notes");
		steps[1] = mgr.getNextStep(instId2);
		assertFalse(mgr.isReadyForCompletion(instId2));
		
		assertNotNull(steps[1]);
		assertEquals("QUESTION 2", ((RecoveryStep)steps[1]).getQuestion());
		assertEquals(3, ((RecoveryStep)steps[1]).getAnswers().size());
		

		mgr.setStepOutcome(instId2, ((RecoveryStep)steps[1]).getAnswers().get(1).getAnswerId(), false, "Notes");
		
		steps[1] = mgr.getNextStep(instId2);
		assertFalse(mgr.isReadyForCompletion(instId2));
		
		
		steps[2] = mgr.getNextStep(instId3);
		assertFalse(mgr.isReadyForCompletion(instId3));
		assertEquals("QUESTION 1", ((RecoveryStep)steps[2]).getQuestion());
		
		mgr.setStepOutcome(instId3, ((RecoveryStep)steps[2]).getAnswers().get(0).getAnswerId(), false, "Notes");
		steps[2] = mgr.getNextStep(instId3);
		assertFalse(mgr.isReadyForCompletion(instId3));
		
		assertNotNull(steps[2]);
		assertEquals("QUESTION 2", ((RecoveryStep)steps[2]).getQuestion());
		assertEquals(3, ((RecoveryStep)steps[2]).getAnswers().size());
		

		mgr.setStepOutcome(instId3, ((RecoveryStep)steps[2]).getAnswers().get(1).getAnswerId(), false, "Notes");
		
		steps[2] = mgr.getNextStep(instId3);
		assertFalse(mgr.isReadyForCompletion(instId3));
		
	}
	
	@Test
	public void testWorstCaseOutcome() throws Exception {
		
		BasicConfigurator.configure();
		InputStream recoveryIS = getClass().getClassLoader().getResourceAsStream("test-recovery3.xml");
		String recovery = convertToString(recoveryIS);
		
        RecoveryManager mgr = RecoveryManager.instance(null);
		
		Object instId = mgr.executeProcessFromDefn(recovery, "username");
		assertNotNull(instId);
		
		String worstcase = mgr.getWorstCaseScenario(instId);
		assertEquals("Outcome Description 1 for User", worstcase);

		RecoveryVertex step = null;
		
		step = mgr.getNextStep(instId);
		
		List<RecoveryAnswer> answers = ((RecoveryStep)step).getAnswers();
        mgr.setStepOutcome(instId, answers.get(0).getAnswerId(), false, "Notes");

		step = mgr.getNextStep(instId);
		answers = ((RecoveryStep)step).getAnswers();
		
		worstcase = mgr.getWorstCaseScenario(instId);
		assertEquals("Outcome Description 2 for User", worstcase);

        mgr.setStepOutcome(instId, answers.get(0).getAnswerId(), false, "Notes");
		//step = mgr.getNextStep(instId);
		worstcase = mgr.getWorstCaseScenario(instId);
		assertEquals("Outcome Description 3 for User", worstcase);

	}

    private String convertToString(InputStream ists) throws IOException {
       
        if (ists != null) {
            StringBuilder sb = new StringBuilder();
            String line;
 
            try {
                BufferedReader r1 = new BufferedReader(new InputStreamReader(ists, "UTF-8"));
                while ((line = r1.readLine()) != null) {
                    sb.append(line).append("\n");
                }
            } finally {
                ists.close();
            }
            return sb.toString();
        } else {       
            return "";
        }
    }
}