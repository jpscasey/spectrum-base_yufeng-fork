/*
 * Copyright (c) Nexala Technologies 2010, All rights reserved.
 */
package com.nexala.spectrum.test.mapping;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
	SimpleClusterizerTest.class
})
public class AllMappingTests {}