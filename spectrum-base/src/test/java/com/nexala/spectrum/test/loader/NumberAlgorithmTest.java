package com.nexala.spectrum.test.loader;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.math.BigInteger;

import org.junit.Before;
import org.junit.Test;

import com.nexala.spectrum.loader.LoaderException;
import com.nexala.spectrum.loader.algorithm.NumberAlgorithm;
import com.nexala.spectrum.loader.beans.FieldValue;
import com.nexala.spectrum.loader.beans.FieldValueLong;
import com.nexala.spectrum.loader.beans.FileFieldDefinition;

public class NumberAlgorithmTest {
    NumberAlgorithm algorithm;
    FileFieldDefinition mockFileFieldDefinition;

    @Before
    public void setup() {
        algorithm = new NumberAlgorithm();

        mockFileFieldDefinition = new FileFieldDefinition();
        mockFileFieldDefinition.setId(5);
        mockFileFieldDefinition.setFieldType("identification");
        mockFileFieldDefinition.setProcessingAlgorithm("Number");
    }

    private void testDecimalValue(String value) throws LoaderException {
        // Run
        FieldValue<?> fieldValue = algorithm.getFieldValue(mockFileFieldDefinition, value, null);
        // Test
        assertEquals(new Double(value), fieldValue.getValue());
    }

    @Test
    public void ifValueIsNegativeDecimalThenSuccess() throws LoaderException {
        testDecimalValue("-5.358");
    }

    @Test
    public void ifValueIsNegativeDecimalThenSuccess2() throws LoaderException {
        testDecimalValue("-.358");
    }

    @Test
    public void ifValueIsPositiveDecimalThenSuccess() throws LoaderException {
        testDecimalValue("5.358");
        testDecimalValue("500.875");
    }

    @Test
    public void ifValueIsPositiveDecimalThenSuccess2() throws LoaderException {
        testDecimalValue(".358");
    }

    @Test
    public void ifValueIsDecimalInScientificNotationReturnDouble() throws LoaderException {
        testDecimalValue("7.0E-2");
    }

    @Test
    public void ifValueIsNanThenReturnDoubleFieldWithNan() throws LoaderException {
        FieldValue<?> fieldValue = algorithm.getFieldValue(mockFileFieldDefinition, "NaN", null);
        Double.isNaN((Double) fieldValue.getValue());
        assertTrue(Double.isNaN((Double) fieldValue.getValue()));
    }

    @Test
    public void ifValueIsNonDecimalNumberThenReturnLong() throws LoaderException {
        FieldValue<?> fieldValue = algorithm.getFieldValue(mockFileFieldDefinition, "10", null);
        FieldValueLong expected = new FieldValueLong((long) 10);
        assertTrue(expected.getValue().equals(fieldValue.getValue()));

        fieldValue = algorithm.getFieldValue(mockFileFieldDefinition, "-10", null);
        expected = new FieldValueLong((long) -10);
        assertTrue(expected.getValue().equals(fieldValue.getValue()));
    }

    @Test(expected = Exception.class)
    public void ifValueIs_hi_fail() throws LoaderException {
        testDecimalValue("hi");
    }

    private byte[] longToByteArray(final long i) {
        BigInteger bigInt = BigInteger.valueOf(i);
        return bigInt.toByteArray();
    }

}
