/*
 * Copyright (c) Nexala Technologies 2010, All rights reserved.
 */

package com.nexala.spectrum.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import com.nexala.spectrum.test.basic.AllBasicTests;
import com.nexala.spectrum.test.mapping.AllMappingTests;
import com.nexala.spectrum.test.validation.AllValidationTests;
import com.nexala.spectrum.test.web.filter.AllFilterTests;

@RunWith(Suite.class)
@Suite.SuiteClasses({
    AllBasicTests.class,
    AllMappingTests.class,
    AllValidationTests.class,
    AllFilterTests.class,
})
public class BaseTests {}
