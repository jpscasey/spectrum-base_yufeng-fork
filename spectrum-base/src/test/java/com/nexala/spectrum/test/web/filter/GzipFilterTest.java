/*
 * Copyright (c) Nexala Technologies 2010, All rights reserved.
 */
package com.nexala.spectrum.test.web.filter;

public class GzipFilterTest {
//
//    private ServletTester servletTester;
//    private String baseUrl;
//
//    /**
//     * Sets up the server, deploys TestServlet under two paths /test and /gz and
//     * applies the GzipFilter to the /gz path.
//     */
//    @Before
//    public void setUp() throws Exception {
//        servletTester = new ServletTester();
//
//        servletTester.setContextPath("/");
//        servletTester.addFilter("com.nexala.spectrum.web.filter.GzipFilter", "/gz", EnumSet.of(DispatcherType.REQUEST));
//        servletTester.addServlet("com.nexala.spectrum.test.web.filter.TestServlet", "/test");
//        servletTester.addServlet("com.nexala.spectrum.test.web.filter.TestServlet", "/gz");
//
//        baseUrl = servletTester.createConnector(true);
//        servletTester.start();
//    }
//
//    /**
//     * This test tests that the TestServlet (which we will use for testing the
//     * compression later) returns a string of 1024 characters.
//     */
//    @Test
//    public void testUncompressedServlet() throws Exception {
//    	final HttpClient client = new HttpClient();
//    	client.start();
//    	client.getContentDecoderFactories().clear();
//    	
//    	final ContentResponse response = client.newRequest(baseUrl + "/" + "test").send();
//    	assertTrue(response.getStatus() == 200);
//        assertTrue(response.getContent().length == 1024);
//        
//        client.stop();
//    }
//
//    /**
//     * This test tests that when the filter intercepts the response AND the
//     * client does not support gzip compression the response contains 1024
//     * characters.
//     */
//    @Test
//    public void testClientGzipNotSupported() throws Exception {
//    	final HttpClient client = new HttpClient();
//    	client.start();
//    	client.getContentDecoderFactories().clear();
//    	
//    	final ContentResponse response = client.newRequest(baseUrl + "/" + "gz").send();
//    	
//    	assertTrue(response.getStatus() == 200);
//        assertTrue(response.getContent().length == 1024);
//        
//        client.stop();
//    }
//
//    /**
//     * This test tests that when the filter intercepts the response, the result
//     * contains less than 1024 characters and when Gunzipped contains 1024
//     * characters.
//     */
//    @Test
//    public void testGzipCompression() throws Exception {
//    	final HttpClient client = new HttpClient();
//    	client.start();
//    	client.getContentDecoderFactories().clear();
//    	
//    	final ContentResponse response = client.newRequest(baseUrl + "/" + "gz").header("Accept-Encoding", "gzip,deflate").send();
//        
//    	System.out.println(response.getMediaType());
//        assertTrue(response.getStatus() == 200);
//
//        // Seems to be a problem here w/ Jetty HttpClient
//        // Header fields are unavailable for gzipped response :-/
//
//        // Firefox returns the following:
//
//        // Content-Type: text
//        // Content-Length: 29
//        // Content-Encoding: gzip
//
//        // 200 OK
//
//        // assertTrue(contentExchange.getResponseFields() != null);
//        // assertTrue(contentExchange.getResponseFields().containsKey("Content-Encoding"));
//
//        // Content should be Gzipped, therefore the size should be
//        // significantly less than 1024 bytes.
//        assertTrue(response.getContent().length < 1024);
//
//        // Unzip the response and assert that it contains
//        // 1024 A's (0x41) ;)
//
//        ByteArrayInputStream byteStream = new ByteArrayInputStream(response.getContent());
//
//        GZIPInputStream gzipStream = new GZIPInputStream(byteStream);
//
//        int x;
//        int c = 0;
//
//        while ((x = gzipStream.read()) != -1) {
//            c++;
//            assertTrue(x == 0x41);
//        }
//
//        assertTrue(c == 1024);
//        
//        client.stop();
//    }
//
//    @After
//    public void tearDown() throws Exception {
//        servletTester.stop();
//    }
}
