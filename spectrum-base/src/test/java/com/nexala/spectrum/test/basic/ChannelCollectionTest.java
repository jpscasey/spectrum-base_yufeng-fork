/*
 * Copyright (c) Nexala Technologies 2010, All rights reserved.
 */

package com.nexala.spectrum.test.basic;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.nexala.spectrum.channel.Channel;
import com.nexala.spectrum.channel.ChannelCategory;
import com.nexala.spectrum.channel.ChannelCollection;
import com.nexala.spectrum.channel.ChannelInt;

public class ChannelCollectionTest {

    @Test
    public void testEquals() {

        ChannelInt c1 = new ChannelInt("c1", 1);
        c1.setCategory(ChannelCategory.NORMAL);

        ChannelInt c2 = new ChannelInt("c1", 1);
        c2.setCategory(ChannelCategory.WARNING1);

        if (c1.equals(c2)) {
            fail();
        }

        ChannelInt c3 = new ChannelInt("c2", 1);
        c3.setCategory(ChannelCategory.NORMAL);

        ChannelInt c4 = new ChannelInt("c2", 2);
        c4.setCategory(ChannelCategory.NORMAL);

        if (c3.equals(c4)) {
            fail();
        }

        ChannelInt c5 = new ChannelInt("c3", 4);
        c3.setCategory(ChannelCategory.WARNING);

        ChannelInt c6 = new ChannelInt("c3", 4);
        c3.setCategory(ChannelCategory.WARNING);

        if (!c5.equals(c6)) {
            fail();
        }
    }

    @Test
    public void testEnumOrder() {

        ChannelCategory c1 = ChannelCategory.NORMAL;
        ChannelCategory c2 = ChannelCategory.FAULT1;

        int r = c1.compareTo(c2);

        System.out.println("enum: " + r);

        if (r >= 0) {
            fail();
        }

        // r = c1.compareTo(null);

        System.out.println("enum: " + r);
    }
    
    @Test
    public void testGetChannelCollectionChangeSet0() {

        List<Channel<?>> channelList = new ArrayList<Channel<?>>();
        channelList.add(new ChannelInt("c1", 1));
        channelList.add(new ChannelInt("c2", 2));
        channelList.add(new ChannelInt("c3", 3));
        // dup key
        channelList.add(new ChannelInt("c3", 4));

        ChannelCollection collection = new ChannelCollection();
        collection.setChannels(channelList);

        assertTrue(collection.getChannels().size() == 3);
    }
}