/*
 * Copyright (c) Nexala Technologies 2010, All rights reserved.
 */

package com.nexala.spectrum.test.basic;

import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import com.nexala.spectrum.channel.Channel;
import com.nexala.spectrum.channel.ChannelInt;
import com.nexala.spectrum.channel.ChannelIterator;
import com.nexala.spectrum.channel.ChannelString;

public class ChannelIteratorTest {
    
    @Test
    public void testGetLiveFaults() {

	Map<String, Channel<?>> channels = new HashMap<String, Channel<?>>();
	
	channels.put("c1", new ChannelInt("c1", 1));
	channels.put("c2", new ChannelInt("c2", 1));
	channels.put("c3", new ChannelInt("c3", 1));
	channels.put("s1", new ChannelString("c3", "s-1"));
	channels.put("s2", new ChannelString("c3", "s-2"));
	channels.put("s3", new ChannelString("c3", "s-3"));
	
	ChannelIterator channelIterator = new ChannelIterator(channels);
	
	int count = 0;
	
	while (channelIterator.hasNext()) {
	    Channel<?> next = channelIterator.next();
	    System.out.println(next.getName() + " : " + next.getValue());
	    count++;
	}
	
	assertTrue(count == 6);
    }
    
    @SuppressWarnings({ "unused" })
    @Test(expected = UnsupportedOperationException.class)
    public void testGetLiveFaults2() {

	Map<String, Channel<?>> channels = new HashMap<String, Channel<?>>();
	
	channels.put("c1", new ChannelInt("c1", 1));
	channels.put("c2", new ChannelInt("c2", 1));
	channels.put("c3", new ChannelInt("c3", 1));
	channels.put("s1", new ChannelString("c3", "s-1"));
	channels.put("s2", new ChannelString("c3", "s-2"));
	channels.put("s3", new ChannelString("c3", "s-3"));
	
	ChannelIterator channelIterator = new ChannelIterator(channels);
	
	while (channelIterator.hasNext()) {
	    
	    Channel<?> next = channelIterator.next();
	    channelIterator.remove();
	}
    }
}