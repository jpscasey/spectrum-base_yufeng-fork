/*
 * Copyright (c) Nexala Technologies 2010, All rights reserved.
 */

package com.nexala.spectrum.test.mapping;

import static org.junit.Assert.assertTrue;

import java.util.LinkedList;
import java.util.List;

import org.junit.Test;

import com.nexala.spectrum.mapping.Coordinate;
import com.nexala.spectrum.mapping.SimpleClusterizer;

public class SimpleClusterizerTest {

    @Test
    public void testClustering1() {

        LinkedList<Coordinate> coordinates = new LinkedList<Coordinate>();

        coordinates.add(new Coordinate(53.28945, -6.2004)); // 4 Cluster 2
        coordinates.add(new Coordinate(53.29515, -6.2191)); // 2 Cluster 1
        coordinates.add(new Coordinate(53.29474, -6.2207)); // 1 Cluster 1
        coordinates.add(new Coordinate(53.28832, -6.1992)); // 3 Cluster 2
        coordinates.add(new Coordinate(53.28796, -6.2013)); // 5 Cluster 2

        SimpleClusterizer<MyCluster, Coordinate> sc = new SimpleClusterizer<MyCluster, Coordinate>(
                MyCluster.class);

        // clustering above coordinates to 1km, should give
        // two clusters, one containing two coords and another
        // containing 3 coords.
        List<MyCluster> clusters = sc.cluster(coordinates, 1000);

        assertTrue(clusters.size() == 2);
        assertTrue(clusters.get(0).getCount() == 3);
        assertTrue(clusters.get(1).getCount() == 2);
    }

    @Test
    public void testClustering2() {

        LinkedList<Coordinate> coordinates = new LinkedList<Coordinate>();

        coordinates.add(new Coordinate(53.29474, -6.2207)); // 1 Cluster 1
        coordinates.add(new Coordinate(53.29515, -6.2191)); // 2 Cluster 1

        coordinates.add(new Coordinate(53.28832, -6.1992)); // 3 Cluster 2
        coordinates.add(new Coordinate(53.28945, -6.2004)); // 4 Cluster 2
        coordinates.add(new Coordinate(53.28796, -6.2013)); // 5 Cluster 2

        SimpleClusterizer<MyCluster, Coordinate> sc = new SimpleClusterizer<MyCluster, Coordinate>(
                MyCluster.class);

        // clustering above coordinates to 1km, should give
        // two clusters, one containing two coords and another
        // containing 3 coords.
        List<MyCluster> clusters = sc.cluster(coordinates, 1000);

        assertTrue(clusters.size() == 2);
        assertTrue(clusters.get(0).getCount() == 2);
        assertTrue(clusters.get(1).getCount() == 3);
    }
}