# Component module

This module provide daos and caches to access the component structure, and save the measurements.

## How to save measurements

The component module support multiple measurement tables and need to be configured as following.

- Create a r2m-component-override.conf file into /src/main/resources

- for a measurement type called 'wheel', the query to insert the measurement should be added under the following path : component.queries.measurement.wheel.insert

for example :

    component: {
        queries: {
            measurement: {
                wheel: {
                    insert:"""
                        INSERT INTO WheelMeasurement(ComponentId, Timestamp, RecordInserted, Mean, Peak, RMSLow, RMSHigh, 
                        WheelFlat, OutOfRound, ValidWDD, Valid) VALUES (?,?,?,?,?,?,?,?,?,?,?)
                        """
                }
            }
        }
    }