package com.nexala.spectrum.component.db;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.sql.DataSource;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;

import com.nexala.spectrum.component.ComponentConstants;
import com.nexala.spectrum.component.Configuration;
import com.nexala.spectrum.component.db.beans.Component;

public class ComponentDao extends NamedParameterJdbcDaoSupport implements CacheableDao<Component> {
    
    private final Configuration conf;
    
    @Inject
    public ComponentDao (Configuration conf, DataSource ds) {
        this.conf = conf;
        setDataSource(ds);
    }
    
    private ResultSetExtractor<List<Component>> componentTypeExtractor = new ResultSetExtractor<List<Component>>() {
        @Override
        public List<Component> extractData(ResultSet rs) throws SQLException, DataAccessException {
            List<Component> result = new ArrayList<>();
            while (rs.next()) {
                Integer id = rs.getInt("ID");
                Integer typeId = rs.getInt("ComponentTypeId");
                Integer parentId = getInteger("ParentId", rs);
                Integer vehicleId = getInteger("VehicleId", rs);
                Integer unitId = getInteger("UnitId", rs);
                short position = rs.getShort("Position");
                
                result.add(new Component(id, typeId, parentId, vehicleId, unitId, position));
            }
            return result;
        }
        
        private Integer getInteger(String key, ResultSet rs) throws SQLException {
            Integer result = rs.getInt(key);
            if (rs.wasNull()) {
                result = null;
            }
            return result;
        }
    };
    
    /**
     * Returns an active FleetStatus object for a specific unit number
     * @param unitNumber
     * @return
     */
    @Override
    public List<Component> getAll() {
        List<Component> result = new ArrayList<>();
        
        String query = conf.getQuery(ComponentConstants.COMPONENT_GET_ALL);
        result = this.getNamedParameterJdbcTemplate().query(query, componentTypeExtractor);
        
        return result;
    }

}
