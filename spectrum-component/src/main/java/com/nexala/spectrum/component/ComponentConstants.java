package com.nexala.spectrum.component;

public class ComponentConstants {
    

    // Component queries
    public static final String COMPONENT_GET_ALL = "component.getAll";
    
    // ComponentType queries
    public static final String COMPONENT_TYPE_GET_ALL = "componentType.getAll";
    
    // VehicleSerie queries
    public static final String VEHICLE_SERIE_GET_ALL = "serie.getAll";

}
