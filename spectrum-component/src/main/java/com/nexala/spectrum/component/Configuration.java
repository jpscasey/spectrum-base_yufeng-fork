package com.nexala.spectrum.component;

import javax.inject.Singleton;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

@Singleton
public class Configuration {
    private final Config conf = ConfigFactory.load("r2m-component-override")
            .withFallback(ConfigFactory.load("r2m-component"));
    
    public Config get() {
        return conf;
    }
    
    public String getQuery(String queryId) {
        return conf.getString("component.queries." + queryId);
    }

}
