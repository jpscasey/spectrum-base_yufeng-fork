package com.nexala.spectrum.component.db.beans;

import java.util.List;

/**
 * A component measurement.
 * The list of values can contain any datatype, and need to be ordered with the same order than the measurement configuration.
 * @author BBaudry
 *
 */
public class CompMeasurement {
    
    private Integer componentId;
    
    private List<Object> values;

    public Integer getComponentId() {
        return componentId;
    }

    public void setComponentId(Integer componentId) {
        this.componentId = componentId;
    }

    public List<Object> getValues() {
        return values;
    }

    public void setValues(List<Object> values) {
        this.values = values;
    }

}
