package com.nexala.spectrum.component.db.beans;

/**
 * Bean for the componenttype table
 * @author BBaudry
 *
 */
public class ComponentType {
    
    private Integer id;
    
    private Integer level;
    
    private String code;
    
    private String type;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    
    

}
