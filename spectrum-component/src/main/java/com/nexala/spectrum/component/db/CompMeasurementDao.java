package com.nexala.spectrum.component.db;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;
import javax.sql.DataSource;

import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;

import com.nexala.spectrum.component.Configuration;
import com.nexala.spectrum.component.db.beans.CompMeasurement;

/**
 * Data access object for the component measurements.
 * 
 * @author BBaudry
 *
 */
public class CompMeasurementDao extends NamedParameterJdbcDaoSupport {

    private final Configuration conf;

    @Inject
    public CompMeasurementDao(Configuration conf, DataSource ds) {
        this.conf = conf;
        setDataSource(ds);
    }

    /**
     * Insert the component measurements in parameter.
     * 
     * @param measurements
     *            list of measurements
     */
    public void insertComponentMeasurements(List<CompMeasurement> measurements, String measurementType) {
        String query = conf.getQuery("measurement." + measurementType + ".insert");

        this.getJdbcTemplate().batchUpdate(query, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                CompMeasurement compMeasurement = measurements.get(i);
                int pos = 1;
                
                ps.setInt(pos++, compMeasurement.getComponentId());
                
                for (Object value : compMeasurement.getValues()) {
                    if (value == null) {
                        ps.setNull(pos++, Types.NULL);
                    } else if (value instanceof String) {
                        ps.setString(pos++, (String) value);
                    } else if (value instanceof Double) {
                        ps.setDouble(pos++, (Double) value);
                    } else if (value instanceof BigDecimal) {
                        ps.setBigDecimal(pos++, (BigDecimal) value);
                    } else if (value instanceof Boolean) {
                        ps.setBoolean(pos++, (Boolean) value);
                    } else if (value instanceof Date) {
                        ps.setTimestamp(pos++, new Timestamp(((Date) value).getTime()));
                    } else if (value instanceof Long) {
                        ps.setLong(pos++, (Long) value);
                    } else if (value instanceof Integer) {
                        ps.setInt(pos++, (Integer) value);
                    }
                }
                
            }

            @Override
            public int getBatchSize() {
                return measurements.size();
            }
        });
    }

}
