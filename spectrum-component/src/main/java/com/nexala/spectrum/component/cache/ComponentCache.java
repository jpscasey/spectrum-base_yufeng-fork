package com.nexala.spectrum.component.cache;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import com.nexala.spectrum.component.db.ComponentDao;
import com.nexala.spectrum.component.db.beans.Component;

/**
 * Cache of components. 
 * Only one instance per database need to be instantiate to avoid multiple loads from db.
 * @author BBaudry
 *
 */
public class ComponentCache extends Cache<Component> {
    
    protected Map<Integer, Component> componentsById;
    
    protected List<Component> treeRoots;

    @Inject
    public ComponentCache(ComponentDao dao) {
        super(dao);
    }

    protected void init() {
        
        componentsById = new HashMap<>();
        treeRoots = new ArrayList<>();
                
        // Index the component per id
        for (Component current : data) {
            componentsById.put(current.getId(), current);
        }
        
        // Iterate over the components to compute the list of childs
        for (Component current : data) {
            Integer parentId = current.getParentId();
            Component parent = componentsById.get(parentId);
            
            if (parent != null) {
                parent.addChild(current);
            } else {
                treeRoots.add(current);
            }
            
        }
    }

    /**
     * Return all the root element cached.
     * @return all the root element cached
     */
    public List<Component> getTreeRoots() {
        return treeRoots;
    }

    /**
     * Return the map of components by id.
     * @return the map of components by id
     */
    public Map<Integer, Component> getComponentsById() {
        return componentsById;
    }
    
}
