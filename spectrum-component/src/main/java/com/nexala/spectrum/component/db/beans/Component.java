package com.nexala.spectrum.component.db.beans;

import java.util.Comparator;
import java.util.TreeSet;

/**
 * Bean for the component table.
 * @author BBaudry
 *
 */
public class Component {
    
    private int id;
    
    private int componentTypeId;
    
    private Integer parentId;
    
    private Integer vehicleId;
    
    private Integer unitId;
    
    private Integer referenceId;
    
    private short position;

    /**
     * The list of childs of this component.
     * Note that it isn't set by the dao but need to be computed later on.
     */
    private TreeSet<Component> childs;
    
    private static Comparator<Component> COMPONENT_SORTER = new Comparator<Component>() {
        @Override
        public int compare(Component c1, Component c2) {
            return c1.getPosition() - c2.getPosition();
        }
    };
    
    public Component(int id, int typeId, Integer parentId, Integer vehicleId, Integer unitId, short position) {
        this.id = id;
        this.componentTypeId = typeId;
        this.parentId = parentId;
        this.vehicleId = vehicleId;
        this.unitId = unitId;
        this.position = position;
        this.childs = new TreeSet<>(COMPONENT_SORTER);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getComponentTypeId() {
        return componentTypeId;
    }

    public void setComponentTypeId(int typeId) {
        this.componentTypeId = typeId;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public Integer getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(Integer vehicleId) {
        this.vehicleId = vehicleId;
    }

    public Integer getUnitId() {
        return unitId;
    }

    public void setUnitId(Integer unitId) {
        this.unitId = unitId;
    }

    public Integer getReferenceId() {
        return referenceId;
    }

    public void setReferenceId(Integer referenceId) {
        this.referenceId = referenceId;
    }

    public short getPosition() {
        return position;
    }

    public void setPosition(short position) {
        this.position = position;
    }
    
    public TreeSet<Component> getChilds() {
        return childs;
    }
    
    /**
     * Add the child if it's not present already.
     * @param child the child to ad
     * @return true if added, false if present already
     */
    public boolean addChild(Component child) {
        return childs.add(child);
    }

    
    
    
}
